<?php $this->load->view("top_application");?>

<!-- #EndLibraryItem --><span class="banner-area"><img src="<?php echo theme_url(); ?>images/slide1.jpg" class="img-fluid d-block mx-auto"alt=""></span>
<div class="banner-area"> 
<p class="down-arrow bounce"><a href="#a1" class="scroll" title="Click Here"><img src="<?php echo theme_url(); ?>images/down-arrow.png" class="mx-auto d-block" alt=""></a></p>
<!-- banner area start --><!--<div class="fluid_container">
    <div id="Page_loader">
      <div class="AniDG"></div>
    </div>
    <div class="fluid_dg_wrap fluid_dg_charcoal_skin fluid_container" id="fluid_dg_wrap_1" >
      <div data-src="banner/slide1.jpg">
        <div class="fluid_dg_caption fadeIn">
          <div class="caption_text">
            <div class="bnr-txt3">Safe Ayurvedic Medicine</div>
            <div class="bnr-txt1">ThreeNiL</div>
            <div class="bnr-txt2">For High Fever, Malaria, Viral fever, Cough &amp; Cold</div>
            <div class="bnr-btn"><a href="contact-us.htm" title="Enquiry Now">Enquiry Now</a></div>
          </div>
        </div>
      </div>
      <div data-src="banner/slide2.jpg">
        <div class="fluid_dg_caption fadeIn">
          <div class="caption_text">
            <div class="bnr-txt3">Safe Ayurvedic Medicine</div>
            <div class="bnr-txt1">Yakrat Shodhan</div>
            <div class="bnr-txt2">For Liver Disorders</div>
            <div class="bnr-btn"><a href="contact-us.htm" title="Enquiry Now">Enquiry Now</a></div>
          </div>
        </div>
      </div>
      <div data-src="banner/slide3.jpg">
        <div class="fluid_dg_caption fadeIn">
          <div class="caption_text">
            <div class="bnr-txt3">Safe Ayurvedic Medicine</div>
            <div class="bnr-txt1">Divine-g</div>
            <div class="bnr-txt2">For Anti infection Boost Immunity</div>
            <div class="bnr-btn"><a href="contact-us.htm" title="Enquiry Now">Enquiry Now</a></div>
          </div>
        </div>
      </div>
      <div data-src="banner/slide4.jpg">
        <div class="fluid_dg_caption fadeIn">
          <div class="caption_text">
            <div class="bnr-txt3">Safe Ayurvedic Medicine</div>
            <div class="bnr-txt1">Rajomax</div>
            <div class="bnr-txt2">Rajomax Herbal Mensuration Tablets</div>
            <div class="bnr-btn"><a href="contact-us.htm" title="Enquiry Now">Enquiry Now</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>-->
  <!-- banner area end -->
  <div class="clearfix"></div>
</div>

<div class="category_section" id="a1">
  <div class="container">
  <h2>Who We Are <span></span></h2>
  <p class="cat-htext"><?php echo char_limiter($who_we_are,200); ?></p>
    <div class="hm_cate_list mt-4">
      <ul id="cate_scroll" class="owl-carousel owl-theme">
        <li class="item">
          <div class="cate_w">
            <div class="cate_img bg-1">
              <figure><img src="<?php echo theme_url(); ?>images/cat-img-1.png" class="" alt="Cardiac Wellness"></figure>
            </div>
            <div class="cate_des animated2 infinite fadeInUpDown">
              <div class="cate_ttl">Cardiac Wellness</div>
              <p class="cate_ttl-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
            </div>
          </div>
        </li>
        <li class="item">
          <div class="cate_w">
            <div class="cate_img bg-2">
              <figure><img src="<?php echo theme_url(); ?>images/cat-img-2.png" class="" alt="Immune Wellness"></figure>
            </div>
            <div class="cate_des animated3 infinite fadeInUpDown">
              <div class="cate_ttl">Immune Wellness</div>
              <p class="cate_ttl-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
            </div>
          </div>
        </li>
        <li class="item">
          <div class="cate_w">
            <div class="cate_img bg-3">
              <figure><img src="<?php echo theme_url(); ?>images/cat-img-3.png" class="" alt="Joint Wellness"></figure>
            </div>
            <div class="cate_des animated2 infinite fadeInUpDown">
              <div class="cate_ttl">Joint Wellness</div>
              <p class="cate_ttl-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
            </div>
          </div>
        </li>
        <li class="item">
          <div class="cate_w">
            <div class="cate_img bg-4">
              <figure><img src="<?php echo theme_url(); ?>images/cat-img-4.png" class="" alt="Cardiac Wellness"></figure>
            </div>
            <div class="cate_des animated3 infinite fadeInUpDown">
              <div class="cate_ttl">Cardiac Wellness</div>
              <p class="cate_ttl-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
            </div>
          </div>
        </li>
      </ul>
    </div>

  </div>
</div>
<!-- Product Section start -->    

<!--counter-->
<div class="counter">
<div class="container">
<h2>Together we can make Difference</h2>
<div id="counter">
<ul>
<li>
<p class="num counter-value" data-count="704">704 <span>+</span></p>
<p class="sub-texts">Clients</p>
</li>
<li>
<p class="num counter-value" data-count="200">200 <span>+</span></p>
<p class="sub-texts">Manufacturers</p>
</li>
<li>
<p class="num counter-value" data-count="740">740 <span>+</span></p>
<p class="sub-texts">Projects</p>
</li>
<li>
<p class="num counter-value" data-count="12">12 <span>+</span></p>
<p class="sub-texts">Countries</p>
</li>
<li>
<p class="num counter-value" data-count="746">746 <span>+</span></p>
<p class="sub-texts">Daily Deliveries</p>
</li>
</ul>
</div>    
</div> 
</div>
<!--counter end-->

<!-- overview Section start -->  
<div class="category_section">
  <div class="container">
  <h2>Overview of the Company <span></span></h2>
  <p class="cat-htext"><?php echo char_limiter($welcome_content,200); ?> </p>
    <div class="hm_cate_list mt-4">
      <div class="row">
  <div class="col-xl-3 col-lg-4">
    <div class="nav nav-pills">
      <a class="nav-link active" data-toggle="pill" href="#one"><img src="<?php echo theme_url(); ?>images/icon-1s.png" alt=""> About Us</a>
      <a class="nav-link" data-toggle="pill" href="#two"><img src="<?php echo theme_url(); ?>images/icon-1.png" alt=""> Mission</a>
      <a class="nav-link" data-toggle="pill" href="#three"><img src="<?php echo theme_url(); ?>images/icon-2.png" alt=""> Vision</a>
      <a class="nav-link" data-toggle="pill" href="#four"><img src="<?php echo theme_url(); ?>images/icon-3.png" alt=""> Directors Msg</a>
    </div>
  </div>
  <div class="col-xl-9 col-lg-8">
    <div class="tab-content">
      <div class="tab-pane fade show active" id="one">
      <h2>About Us<span></span></h2>
      <div class="text-content">
  <div class="cat-ttext"><?php echo char_limiter($aboutus_content,1000); ?></div>
</div>
<p class="mt-3"><a href="<?php echo site_url('aboutus');?>" title="Read More" class="vw_al_btn">Read More...</a></p>
  </div>
      <div class="tab-pane fade" id="two"><h2>Mission<span></span></h2>
  <div class="text-content">
  <div class="cat-ttext"><?php echo char_limiter($mission_content,1000); ?> </div>
  
</div>
<p class="mt-3"><a href="<?php echo site_url('mission');?>" title="Read More" class="vw_al_btn">Read More...</a></p>
</div>
      <div class="tab-pane fade" id="three"><h2>Vision<span></span></h2>
  <div class="text-content">
  <div class="cat-ttext"><?php echo char_limiter($vision_post,1000); ?> </div>
  
</div>
<p class="mt-3"><a href="<?php echo site_url('vision');?>" title="Read More" class="vw_al_btn">Read More...</a></p></div>
      <div class="tab-pane fade" id="four"><h2>Directors Msg<span></span></h2>
  <div class="text-content">
  <div class="cat-ttext"><?php echo char_limiter($director,1000); ?></div>
</div>
<p class="mt-3"><a href="<?php echo site_url('director-message');?>" title="Read More" class="vw_al_btn">Read More...</a></p></div>
    </div>
  </div>
</div>
    </div>

  </div>
</div>
<!-- overview Section end -->  

<!-- industries Section start -->  
<div class="category_section">
  <div class="container">
  <h2>Industries we serve <span></span></h2>
  <div class="cat-htext"><?php echo char_limiter($industries_we,500); ?> </div>
    <div class="indus">
    <h2>Guru Corporation Some of Customer Name<span></span></h2>
    <div class="clearfix"></div>
    <div class="tab_conts" id="tb1s">
	 <p class="text-center"><img src="<?php echo theme_url(); ?>images/indus-1.jpg" class="img-fluid d-block mx-auto" alt=""></p>
	  </div>
	  <div class="tab_conts tab_hides" id="tb2s">
	 <p class="text-center"><img src="<?php echo theme_url(); ?>images/indus-1.jpg" class="img-fluid d-block mx-auto" alt=""></p>
	  </div>
	  <div class="tab_conts tab_hides" id="tb3s">
	 <p class="text-center"><img src="<?php echo theme_url(); ?>images/indus-1.jpg" class="img-fluid d-block mx-auto" alt=""></p>
	  </div>
	  <div class="tab_conts tab_hides" id="tb4s">
	 <p class="text-center"><img src="<?php echo theme_url(); ?>images/indus-1.jpg" class="img-fluid d-block mx-auto" alt=""></p>
	  </div>
	  <div class="tab_conts tab_hides" id="tb5s">
	 <p class="text-center"><img src="<?php echo theme_url(); ?>images/indus-1.jpg" class="img-fluid d-block mx-auto" alt=""></p>
	  </div>
    <div class="clearfix"></div>
    <div class="tab_linkd tb_links"> 
    <a href="#tb1s" class="act" title="India"><span></span> India <img src="<?php echo theme_url(); ?>images/flag-1.jpg" alt=""></a> 
    <a href="#tb2s" title="Sri Lanka"><span></span> Sri Lanka <img src="<?php echo theme_url(); ?>images/flag-2.jpg" alt=""></a> 
    <a href="#tb3s" title="Nepal"><span></span> Nepal <img src="<?php echo theme_url(); ?>images/flag-3.png" alt=""></a> 
    <a href="#tb4s" title="Oman"><span></span> Oman <img src="<?php echo theme_url(); ?>images/flag-4.png" alt=""></a> 
    <a href="#tb5s" title="Dubai"><span></span> Dubai <img src="<?php echo theme_url(); ?>images/flag-5.png" alt=""></a> 
    </div>
	  <p class="clearfix"></p>
    </div>

  </div>
</div>
<!-- industries Section end -->  

<!-- Projects Section start -->  
<div class="projects_section">
  <div class="container">
  <h2>Projects We Undertake <span></span></h2>
<div class="row">
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated3 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-1.jpg" alt=""></p>
<p class="sec2">Road</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated2 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-2.jpg" alt=""></p>
<p class="sec2">Metor</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated3 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-3.jpg" alt=""></p>
<p class="sec2">Buildings</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated2 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-4.jpg" alt=""></p>
<p class="sec2">Rail</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated3 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-5.jpg" alt=""></p>
<p class="sec2">Airport</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="proj_we">
<p class="sec1 animated2 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-6.jpg" alt=""></p>
<p class="sec2">Others</p>
<p class="clearfix"></p>
<p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad </p>
</div>
</div>
</div>

  </div>
</div>
<!-- Projects Section end --> 

<!-- Why Section start -->  
<div class="category_section">
  <div class="container">
  <h2>Why Choose Us <span></span></h2>
  <div class="cat-htext"><?php echo char_limiter($why_choose,500); ?> </div>
    <div class="hm_cate_list mt-4">
      <ul id="why_scroll" class="owl-carousel owl-theme">
        <li class="item">
          <div class="why-box">
            <p class="why-box-image"><img src="<?php echo theme_url(); ?>images/why-1.jpg" alt=""></p>
              <div class="why-box-text">WE DELIVER tQUALITY</div>
          </div>
        </li>
        <li class="item">
          <div class="why-box">
            <p class="why-box-image"><img src="<?php echo theme_url(); ?>images/why-2.jpg" alt=""></p>
              <div class="why-box-text">ALWAYS ON TIME</div>
          </div>
        </li>
        <li class="item">
          <div class="why-box">
            <p class="why-box-image"><img src="<?php echo theme_url(); ?>images/why-3.jpg" alt=""></p>
              <div class="why-box-text">WE ARE PASIONATE</div>
          </div>
        </li>
        <li class="item">
          <div class="why-box">
            <p class="why-box-image"><img src="<?php echo theme_url(); ?>images/why-4.jpg" alt=""></p>
              <div class="why-box-text">PROFESSIONAL SERVICES</div>
          </div>
        </li>
      </ul>
    </div>

  </div>
</div>
<!-- Why Section end -->  
 
<div class="clearfix"></div><!-- #BeginLibraryItem "/Library/footer.lbi" -->

<<?php $this->load->view("bottom_application");?>