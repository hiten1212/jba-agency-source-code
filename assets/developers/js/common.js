function lookup(inputString){

	if(inputString.length == 0) {
		// Hide the suggestion box.
		$('#suggestions').hide();
		$('#autoSuggestionsList').hide();
	}else {
		$.post(site_url+"products/keyword_suggestions", {mysearchString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#suggestions').show();
				$('#autoSuggestionsList').html(data);
				$('#autoSuggestionsList').show();
			}else{
				$('#suggestions').hide();
				//$('#autoSuggestionsList').html(data);
				$('#autoSuggestionsList').hide();
			}
		});
	}
} //end

// if user clicks a suggestion, fill the text box.
function fill(thisValue) {
	$('#inputString').val(thisValue);
	setTimeout("$('#suggestions').hide();", 200);
}

function validcheckstatus(name,action,text)

{

	var chObj	=	document.getElementsByName(name);

	var result	=	false;	

	for(var i=0;i<chObj.length;i++){

	

		if(chObj[i].checked){

		  result=true;

		  break;

		}

	}

 

	if(!result){

		 alert("Please select atleast one "+text+" to "+action+".");

		 return false;

	}else if(action=='delete'){

			 if(!confirm("Are you sure you want to delete this.")){

			   return false;

			 }else{

				return true;

			 }

	}else{

		return true;

	}

}




function showloader(id)

{

	$("#"+id).after("<span id='"+id+"_loader'><img src='"+site_url+"/assets/developers/images/loader.gif'/></span>");

}





function hideloader(id)

{

	$("#"+id+"_loader").remove();

}

												

												

function load_more(base_uri,more_container,formid)

{	

  showloader(more_container);

  $("#more_loader_link"+more_container).remove();

   if(formid!='0')

   {

	   form_data=$('#'+formid).serialize();

   }

   else

   {

	   form_data=null;

   }

  $.post

	  (

		  base_uri,

		  form_data,

		  function(data)

		  { 

		  

		  

			 var dom = $(data);

			

			dom.filter('script').each(function(){

			$.globalEval(this.text || this.textContent || this.innerHTML || '');

			});

			

			var currdata = $( data ).find('#'+more_container).html(); 

			$('#'+more_container).append(currdata);

			hideloader(more_container);

		  }

	  );

}







function onclickcategory(ajax_url,response_id,category_value){

	$.ajax({type: "POST",

			url: ajax_url,

			dataType: "html",

			data: {"category_id":category_value},

			cache:false,

			success:function(data){$("#"+response_id).html(data);}    

    }); 	

}



function join_newsletter()
{
	var form = $("#chk_newsletter");
	showloader('newsletter_loder');
	$(".btn-style").attr('disabled', true);
	$.post(site_url+"pages/join_newsletter",

	$(form).serialize(),
	function(data){
		$("#refresh").click();
		$(".btn-style").attr('disabled', false);

		hideloader('newsletter_loder');
		if(data.error!=undefined){
			$(".captchaimage").val('');
			$("#my_newsletter_msg").html(data.error);
		}else{
			$("#my_newsletter_msg").html(data);
			clearForm("#chk_newsletter");
		}
		$("#verification_code").val('');
	});
	
	return false;
}





function bidnow()

{

	$(".bidnow").attr('disabled', true);

	$.post(site_url+"products/add_bid",$('#bidnowform').serialize(),

	function(data){

		if(data.error!=undefined){

			$(".bidnow").attr('disabled', false);

			$(".bid_msg").html(data.error);

		}else{		

			$("#biddata").hide();

			$(".bid_msg").html(data);

		}

	});	

	return false;

}



function bargainnow()

{

	$(".bargainnow").attr('disabled', true);

	$.post(site_url+"products/add_bargain",$('#bargainnowform').serialize(),

	function(data){

		if(data.error!=undefined){

			$(".bargainnow").attr('disabled', false);

			$(".bargain_msg").html(data.error);

		}else{		

			$("#bargaindata").hide();

			$("input[name='offer_price']").val('');

			$("textara[name='message']").val('');

			$(".bargain_msg").html(data);

		}

	});	

	return false;

}



function clearForm(frm)

{

	$(frm).find(':input').each(function() {

		switch(this.type) {

			case 'checkbox':

			case 'radio':

			$(this).attr('checked',false);

			break;

			case 'password':

			case 'select-multiple':

			case 'select-one':

			case 'text':

			case 'textarea':

			$(this).val('');

			break;

		}

	});

}



function increase_quantity(fldName, maxQty) {

	var qty=document.getElementById(fldName).value;

	if (parseInt(maxQty) <= qty) {

		alert("Total available quantity of this product is only "+maxQty+" at the moment");

	}

	else {

		qty++;

		document.getElementById(fldName).value=qty;

		document.getElementById('qty').value=qty;

	}	

}





function decrease_quantity(fldName) {

	var qty=document.getElementById(fldName).value;

	if (parseInt(qty) >1) {

		qty--;

	}

	document.getElementById(fldName).value=qty;

	document.getElementById('qty').value=qty;

}



function getstate(country_id,url){  



 var ajax_url=site_url+url; 

 $.ajax({

			type: "POST",

			url: ajax_url,

			dataType: "html",

			data: "country_id="+country_id,

			cache:false,

			success:

				function(data)

				{			

					$("#countryid").html(data);

					$("#city_id").val('');

				}				

	});  

}



function getcity(state_id,url){

	

 var ajax_url=site_url+url;

 $.ajax({

			type: "POST",

			url: ajax_url,

			dataType: "html",

			data: "state_id="+state_id,

			cache:false,

			success:

				function(data){	 				

					$("#stateid").html(data);	

				}				

	}); 

 

}



function getstate_ship(country_id,url){  



 var ajax_url=site_url+url; 

 $.ajax({

			type: "POST",

			url: ajax_url,

			dataType: "html",

			data: "country_id="+country_id,

			cache:false,

			success:

				function(data)

				{			

					$("#countryid_ship").html(data);

					$("#city_id_ship").val('');

				}				

	});  

}



function getcity_ship(state_id,url){

	

 var ajax_url=site_url+url;

 $.ajax({

			type: "POST",

			url: ajax_url,

			dataType: "html",

			data: "state_id="+state_id,

			cache:false,

			success:

				function(data){	 				

					$("#stateid_ship").html(data);	

				}				

	}); 

 

}



function multisearch(srchkey,chkname)

{			

	var arrval=new Array();

	$('[name='+chkname+']:checked').each(function(mkey,mval)

	{		

		arrval.push($(mval).val());		

	})

	

	$('#'+srchkey).val(arrval.join(","));		

	$("#srcfrm").submit();

}



function check_zip_location(prd_id){

	$(".errors_value").hide();

	var hasError = false;

	var locationVal = $('#zip_location').val();

	if(locationVal == ''){

		$('#location_error').html('<span class="red mt5 loc_err">Please enter delivery location.</span>');

		$("#zip_location").focus();

		hasError = true;

	}

	if(hasError == true) { return false; }

	else{

		$("#location_loader").show();

		$('#location_loader').html('<img src="'+site_url+'assets/developers/images/loader.gif"/>');

		term = $('input[name="zip_location"]').val();

		url= site_url+'products/ajax_search_zip_location/';

		$.post(url,{zip_location: term },

		function(data){

			$("#location_error_show").html('<span class="">'+data+'</span>');

			$("#location_loader").hide();

			$("#location_search_form").hide();

			$("#location_error").hide();			

			$(".loc_err").hide();

			$("#zip_location").val('');

		});

	}

	return false;

}



function show_location_form(){

	$("#location_search_form").show();

}

function lookup(inputString) {
	if(inputString.length == 0) {
		// Hide the suggestion box.
		$('#States_sugg').hide();
		$('#suggestions').hide();
	} else {
		// post data to our php processing page and if there is a return greater than zero
		// show the suggestions box
		$.post(site_url+"home/string_search", {mysearchString: ""+inputString+""}, function(data){
			if(data.length >0) {
		$('#States_sugg').hide();
				$('#suggestions').show();
				$('#autoSuggestionsList').html(data);
			}
		});
	}
} //end


// if user clicks a suggestion, fill the text box.
function fill(thisValue) {
	$('#inputString').val(thisValue);
	setTimeout("$('#suggestions').hide();", 200);
}

$(document).ready(function(){



 $('body').on('click', '.portfolio_item', function(){
   var photoId = jQuery(this).attr("data-photoId");

   $.ajax({
        method:'POST',
        url: site_url+"/gallery/getphoto",
        dataType: 'json',
        data: {photoId: photoId},
        beforeSend: function (f) {
          
        },
        success: function (data)
        {
          
        }
    })

 });


  $('body').on('click', '.portfolio_slider_item', function(){
   var portfolioid = jQuery(this).attr("data-portfolioid");

   $.ajax({
        method:'POST',
        url: site_url+"/gallery/category_portfolio",
        dataType: 'json',
        data: {portfolioid: portfolioid},
        beforeSend: function (f) {
          
        },
        success: function (data)
        {
          if(data.confirm==1)
          {
            window.location.href = site_url+'/gallery';	
          }
          
        }
    })

 });


$(document).ready(function(){ // When the DOM is Ready, then bind the click

$(".ctg_glry_tile_sub_list").click(function(){
 var portfolioid = jQuery(this).attr("data-portfolioid");
   $.ajax({
        method:'POST',
        url: site_url+"/gallery/category_portfolio",
        dataType: 'json',
        data: {portfolioid: portfolioid},
        beforeSend: function (f) {
          
        },
        success: function (data)
        {
          if(data.confirm==1)
          {
            window.location.href = site_url+'/gallery';	
          }
          
        }
    })

});

});

});


// Get the current scroll position
var currentScrollPosition = window.pageYOffset;

// Set the target scroll position
var targetScrollPosition = 500;

// Calculate the distance to scroll
var scrollDistance = targetScrollPosition - currentScrollPosition;

// Set the duration of the scroll animation
var scrollDuration = 1000;

// Calculate the scroll speed
var scrollSpeed = scrollDistance / scrollDuration;

// Set the starting time for the scroll animation
var startTime = null;

// Function to be called on each animation frame
function step(timestamp) {
  if (!startTime) startTime = timestamp;
  var progress = timestamp - startTime;
  window.scrollTo(0, currentScrollPosition + (scrollDistance * progress / scrollDuration));
  if (progress < scrollDuration) {
    window.requestAnimationFrame(step);
  }
}

// Start the scroll animation
window.requestAnimationFrame(step);