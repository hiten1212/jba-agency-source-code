$(".mainslider_js").owlCarousel({loop:!0,margin:0,nav:!0,animateOut:"fadeOut", autoplay:true,speed:1000,dots:!0,navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],responsive:{0:{items:1},600:{items:1},1000:{items:1}}}),

$(".innerslider").owlCarousel({loop:!0,margin:0,nav:!0,animateOut:"fadeOut",autoplay:!1,speed:500,dots:!0,navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],responsive:{0:{items:1},600:{items:1},1000:{items:1}}}),

$("#nav").affix({offset:{top:$(".header-area").height()}}),$(document).ready(function(){$(window).scroll(function(){$(this).scrollTop()>100?$(".scrollup").fadeIn():$(".scrollup").fadeOut()}),$(".scrollup").click(function(){return $("html, body").animate({scrollTop:0},600),!1})});


$(".works").owlCarousel({loop:!0,margin:40,nav:!0,animateOut:"fadeOut",autoplay:!1,speed:500,dots:!0,navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],responsive:{0:{items:1},600:{items:2},1000:{items:2}}})


$("#nav").affix({offset:{top:$(".mainheader").height()}})