-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2023 at 06:38 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sinonvhd_jbaagency`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_type` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2' COMMENT '1= super admin , 2= half_admin_right,3=low_admin_right',
  `admin_key` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `admin_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enquiry_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `litigation_days` int(11) DEFAULT NULL,
  `admin_last_login` datetime NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `company_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domestic_google_map` text COLLATE utf8_unicode_ci,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_address` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_email` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_details` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_google_map` text COLLATE utf8_unicode_ci,
  `suppliers_address` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_email` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_google_map` text COLLATE utf8_unicode_ci,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `free_shipping_order_amount` int(11) NOT NULL DEFAULT '500',
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod_amount` float(10,2) NOT NULL,
  `post_date` date NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `total_hits` bigint(20) NOT NULL DEFAULT '0',
  `facebook_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `soundcloud_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_analytics_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_web_code` text COLLATE utf8_unicode_ci,
  `google_map` text COLLATE utf8_unicode_ci,
  `footer_time` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brochure` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `whatsapp_number` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `whatsapp_text` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtuble_url` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#000',
  `portfolio_list_background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#000',
  `portfolio_detail_background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#000',
  `start_project_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyright_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer1_header` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer2_header` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer3_header` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_slogan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_on_off` tinyint(4) NOT NULL COMMENT '0-Off, 1-On',
  `banner_text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyprofile_status` tinyint(4) NOT NULL,
  `start_project_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portfolio_menu_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clients_menu_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_type`, `admin_key`, `admin_username`, `admin_password`, `admin_email`, `enquiry_email`, `litigation_days`, `admin_last_login`, `address`, `company_name`, `domestic_google_map`, `city`, `state`, `country`, `zipcode`, `international_address`, `international_email`, `international_phone`, `contact_details`, `international_google_map`, `suppliers_address`, `suppliers_email`, `suppliers_phone`, `suppliers_google_map`, `phone`, `free_shipping_order_amount`, `fax`, `contact_person`, `contact_phone`, `contact_email`, `cod_amount`, `post_date`, `status`, `total_hits`, `facebook_link`, `twitter_link`, `linkedin_link`, `google_link`, `youtube_link`, `pinterest_link`, `instagram_link`, `soundcloud_link`, `rss_link`, `google_analytics_id`, `google_web_code`, `google_map`, `footer_time`, `brochure`, `website`, `whatsapp_number`, `whatsapp_text`, `youtuble_url`, `header_video`, `header_logo`, `footer_logo`, `header_background`, `footer_background`, `portfolio_list_background`, `portfolio_detail_background`, `start_project_background`, `copyright_text`, `footer1_header`, `footer2_header`, `footer3_header`, `website_url`, `banner_slogan`, `video_on_off`, `banner_text_color`, `company_profile`, `companyprofile_status`, `start_project_text`, `portfolio_menu_text`, `clients_menu_text`, `logo_background`) VALUES
(1, '1', 'cnsgkMd4', 'admin', 'Jbaagency@2022', 'info@jbaagency.net', NULL, 20, '0000-00-00 00:00:00', 'Zone 24 - Building  20 - Street 840 - Doha,  Qatar\r\n\r\n\r\n', NULL, 'https://goo.gl/maps/rKhAoqADcg6vSJgS8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '97433377554', 0, NULL, '', NULL, '', 0.00, '0000-00-00', '1', 2041, 'https://www.facebook.com/', '', 'https://www.linkedin.com/', NULL, '', NULL, 'https://www.instagram.com/jba.agency.qatar/', NULL, NULL, '', '', NULL, NULL, 'sample-pdf.pdf', NULL, '+97433377554', 'hi', NULL, 'jba-profile.mp4', 'jba-logo-l.png', 'jba-logo-l1.png', '#000000', '#000000', '#000000', '#000000', '#eb7a24', '© Copyright JBA Agency. All rights reserved', 'Follow Us', 'Qatar', 'Get In Touch', 'www.jbaagency.net', 'Joining Brains with Art', 0, '#ffffff', 'jba-agency-profile.pdf', 0, 'Start Project', 'Portfolio', 'Clients', '#2c2810');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog_review`
--

CREATE TABLE `tbl_blog_review` (
  `review_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `poster_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `poster_email` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `blog_id` int(11) NOT NULL COMMENT 'Prod_id/Business_id',
  `blog_title` varchar(255) DEFAULT NULL,
  `posted_date` datetime DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Actice,0=Inactive'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog_review`
--

INSERT INTO `tbl_blog_review` (`review_id`, `language_id`, `poster_name`, `poster_email`, `rate`, `comment`, `blog_id`, `blog_title`, `posted_date`, `status`) VALUES
(1, 1, 'Test name', 'admin@weblink.com', NULL, 'fgbfg', 2, 'test blogs2', '2019-12-09 08:22:25', '1'),
(2, 1, 'hhh', 'yy@gmail.com', NULL, 'gnbg', 2, 'test blogs2', '2019-12-09 11:29:49', '1'),
(3, 1, 'deepak', 'test@gmail.com', NULL, 'fcbfgb', 5, 'Newsrooms2', '2019-12-30 07:13:46', '1'),
(4, 1, 'A Homeland Bird', 'test@gmail.com', NULL, 'fgbfgb', 5, 'Newsrooms2', '2019-12-30 07:14:45', '1'),
(5, 1, 'test blogs yy', 'uu@gmail.com', NULL, 'euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.\r\n\r\nSed hendrerit tortor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos him', 5, 'Newsrooms2', '2019-12-30 07:15:23', '1'),
(6, 1, 'test blogs yy', 'test@gmail.com', NULL, 'dfvdfv', 4, 'News blogs', '2020-02-05 07:06:44', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog_review_reply`
--

CREATE TABLE `tbl_blog_review_reply` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `poster_name` varchar(100) DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `blog_id` int(11) NOT NULL COMMENT 'Prod_id/Business_id',
  `review_id` int(11) DEFAULT NULL,
  `posted_date` datetime DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Actice,0=Inactive'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `news_id` int(11) NOT NULL,
  `news_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `news_image` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_tag` varchar(200) DEFAULT NULL,
  `sort_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `news_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `publisher` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `recv_date` datetime NOT NULL,
  `latest_news` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `up_date` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `news_title`, `news_image`, `friendly_url`, `alt_tag`, `sort_description`, `news_description`, `publisher`, `sort_order`, `status`, `recv_date`, `latest_news`, `up_date`) VALUES
(34, 'news title3', '', 'news-title3', '', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n\r\n<p>Sed hendrerit tortor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus ornare ornare pulvinar. Vivamus ornare faucibus sollicitudin. Sed suscipit gravida tincidunt. Duis eu luctus lacus, nec consectetur massa. Phasellus orci mauris, tristique quis dolor sit amet, porttitor fermentum mauris. Ut tempor et neque id lobortis. Cras eleifend consectetur mi, vitae congue elit rutrum at. Morbi nec risus maximus sapien dignissim euismod sit amet non neque.</p>', NULL, NULL, '1', '2020-12-02 05:18:49', '1', NULL),
(35, 'Lorem ipsum dolor sit amet, consectetur', '', 'lorem-ipsum-dolor-sit-amet-consectetur', '', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n\r\n<p>Sed hendrerit tortor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus ornare ornare pulvinar. Vivamus ornare faucibus sollicitudin. Sed suscipit gravida tincidunt. Duis eu luctus lacus, nec consectetur massa. Phasellus orci mauris, tristique quis dolor sit amet, porttitor fermentum mauris. Ut tempor et neque id lobortis. Cras eleifend consectetur mi, vitae congue elit rutrum at. Morbi nec risus maximus sapien dignissim euismod sit amet non neque.</p>', NULL, NULL, '1', '2020-12-02 05:19:35', '1', NULL),
(39, 'this is new news', '', 'this-is-new-news', '', NULL, '<p>ds afsdf dsfdsaf s</p>', NULL, NULL, '1', '2021-01-23 07:10:39', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_application_form`
--

CREATE TABLE `wl_application_form` (
  `application_id` int(11) NOT NULL,
  `name` varchar(80) DEFAULT NULL,
  `abn` varchar(50) DEFAULT NULL,
  `trading_name` varchar(100) DEFAULT NULL,
  `legal_entity` varchar(255) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `address_post_code` varchar(100) DEFAULT NULL,
  `postal_address` varchar(100) DEFAULT NULL,
  `postal_address_code` varchar(100) DEFAULT NULL,
  `office_phone` varchar(100) DEFAULT NULL,
  `fax_number` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ah_phone` varchar(100) DEFAULT NULL,
  `nature_business` varchar(100) DEFAULT NULL,
  `year_business` varchar(100) DEFAULT NULL,
  `account_contact` varchar(100) DEFAULT NULL,
  `account_phone` varchar(100) DEFAULT NULL,
  `account_email` varchar(100) DEFAULT NULL,
  `account_mobile` varchar(100) DEFAULT NULL,
  `purchase_contact` varchar(100) DEFAULT NULL,
  `mobile_number` varchar(100) DEFAULT NULL,
  `monthly_credit_req` varchar(100) DEFAULT NULL,
  `todays_date` varchar(100) DEFAULT NULL,
  `finc_turn_over_last_year` varchar(100) DEFAULT NULL,
  `finc_paid_up_capital` varchar(100) DEFAULT NULL,
  `finc_accountants` varchar(100) DEFAULT NULL,
  `finc_contact_number` varchar(100) DEFAULT NULL,
  `finc_mobile_number` varchar(100) DEFAULT NULL,
  `finc_bank_name` varchar(100) DEFAULT NULL,
  `finc_account_name` varchar(100) DEFAULT NULL,
  `finc_bsb` varchar(100) DEFAULT NULL,
  `finc_account_number` varchar(100) DEFAULT NULL,
  `finc_bank_manager_name` varchar(100) DEFAULT NULL,
  `credit_application_for` varchar(255) DEFAULT NULL,
  `guarantor_name` varchar(100) DEFAULT NULL,
  `supplier_to_supply` varchar(100) DEFAULT NULL,
  `trade_applicable` varchar(100) DEFAULT NULL,
  `guarantor_address` varchar(100) DEFAULT NULL,
  `guarantor_state` varchar(100) DEFAULT NULL,
  `guarantor_zip_code` varchar(100) DEFAULT NULL,
  `limited_customer_name` varchar(100) DEFAULT NULL,
  `limited_business_name` varchar(100) DEFAULT NULL,
  `limited_address` varchar(100) DEFAULT NULL,
  `limited_state` varchar(100) DEFAULT NULL,
  `limited_zip_code` varchar(100) DEFAULT NULL,
  `dir_account_number` varchar(100) DEFAULT NULL,
  `dir_company_name` varchar(100) DEFAULT NULL,
  `dir_abn` varchar(100) DEFAULT NULL,
  `dir_institution_name` varchar(100) DEFAULT NULL,
  `dir_address` varchar(100) DEFAULT NULL,
  `dir_account_name` varchar(100) DEFAULT NULL,
  `dir_bsb_number` varchar(100) DEFAULT NULL,
  `dir_account_dbt_number` varchar(100) DEFAULT NULL,
  `application_pdf_file` varchar(100) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Delete',
  `receive_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_application_form`
--

INSERT INTO `wl_application_form` (`application_id`, `name`, `abn`, `trading_name`, `legal_entity`, `address`, `address_post_code`, `postal_address`, `postal_address_code`, `office_phone`, `fax_number`, `email`, `ah_phone`, `nature_business`, `year_business`, `account_contact`, `account_phone`, `account_email`, `account_mobile`, `purchase_contact`, `mobile_number`, `monthly_credit_req`, `todays_date`, `finc_turn_over_last_year`, `finc_paid_up_capital`, `finc_accountants`, `finc_contact_number`, `finc_mobile_number`, `finc_bank_name`, `finc_account_name`, `finc_bsb`, `finc_account_number`, `finc_bank_manager_name`, `credit_application_for`, `guarantor_name`, `supplier_to_supply`, `trade_applicable`, `guarantor_address`, `guarantor_state`, `guarantor_zip_code`, `limited_customer_name`, `limited_business_name`, `limited_address`, `limited_state`, `limited_zip_code`, `dir_account_number`, `dir_company_name`, `dir_abn`, `dir_institution_name`, `dir_address`, `dir_account_name`, `dir_bsb_number`, `dir_account_dbt_number`, `application_pdf_file`, `status`, `receive_date`) VALUES
(4, 'manish', 'ABN456', 'weblink', 'Partnership,Pty Ltd,Trust Co.', 'sdfdsafdsa', 'dsafds', 'dsfasdfsda', 'sdfsa', 'sdaf', 'fdsafds', 'dsfs', 'dsfdsafd', 'dfas', 'dsfas', 'sdfas', 'dsfdsa', 'dsfa', 'dsfa', 'dsfas', 'fdsa', 'dfsas', '2021-09-29', 'sdfsa', 'fdsa', 'dfsaa', 'dsfa', 'dsfaa', 'dsaf', 'fdaa', 'dfa', 'dsfaas', 'dfsa', 'Credit Application,Limited Warranty', 'fsdsa', 'dfas', 'dsfafasd', 'dsfaasd', 'fadsfdsa', 'fa', 'dsfsafas', 'fdsfsa', 'fdsadsfda', 'dafsdasfdas', 'dsafdas', 'sdfsa', 'fsdfdsafds', 'dsfsafas', 'sdafdsaf', 'sadfsad', 'dsafdsaf', 'dsafdsfdsa', 'dsfasfasdfas', 'Application4.pdf', '1', '2021-09-14 10:25:39'),
(5, 'sdfsafdas', 'fsdaf safdsa', 'dsfsafdsafasd', 'Sole Trader,Partnership', 'sdfsafasd', 'sdafdsa', 'dfsasfads', 'sdaffsdaas', 'dsafasd', 'fsdafdsa', 'dsfsafads', '', 'fsdafsad', 'fsdafsad', 'f', '', 'fsd', 'fsdfsd', 'sdfsa', '', 'sdfsda', '2021-09-14', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application5.pdf', '1', '2021-09-16 06:05:37'),
(6, 'dsfasfs', 'dfsd fsdafdsa', '', 'Pty Ltd', '', '', '', '', '', 's afsdfd sfasd', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sdfa sd', 'f dsfasdfsda ', 'fsdfsd afsda', 'Application6.pdf', '1', '2021-09-16 06:12:13'),
(7, 'Brijesh Kumar', 'BN23', 'test', 'Partnership,Pty Ltd,Trust Co.', 'sda fsd', 'fdsfdsf', 'sdfsdfsad', 'sdfdsafas', 'fsdaffsdsa', 'fsdafdsa', 'safasdfsda', 'dsfd', 'dsfasds', 'fdsafas', 'dsafdsa', 'dsfdsadsafs', 'dsfsda', 'dsafasdfdsa', 'dsfas', '', 'sdfdsafa', '2021-09-14', 'sdfsa', 'fsdfs', 'fsdfds', 'sad', 'fdsasf', 'dsfsa', 'dsfsaf', 'sdfsd', 'fdsaas', 'dsfsa', 'Guarantee,Limited Warranty', 'sdfaf', 'dsfsdfsd', 'fdsfd', 'dsfds', 'fsaf', 'fdsf', 'sdfdsas', 'dsfsadf', 'dsfsdafa', 'dsffads', 'dsffdas', 'dsafsfs', 'fsdfsfds', 'fdsafda', 'sdfsafsd', 'fdsfds', 'dsfsfdsa', 'dfdsafdsa', 'dsfsafs', NULL, '1', '2021-09-17 15:13:13'),
(8, 'dsfsfsd fsda', 'dsfd sfdsa', 'fdsa fasdf sdads', 'Partnership,Pty Ltd', 'dsfsa', 'fsdfds ', 'fsdaf sdfdsa', 'dsf fsda', 'dsaf sdfsda', 'sdafsd f', 'fds f', 'a fdsa', 'fs dfsdf', 'dsaf sdf', 'dsafdsf', 'dsf sad', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sdfsf', 'dsfdsafdsa', 'dsfsf dsa', 'fsd fadsfdsa', 'fds', 'Application8.pdf', '1', '2021-09-17 16:01:44'),
(9, 'Manish', 'ABN456', 'TEst', 'Partnership,Trust Co.', 'sdfafasd', 'dsfasfs', 'dfdsfds', 'fdsfasd', 'dfsdfds', 'fdsfs', 'fsdfds', 'fdsa', 'fsdfsda', 'fdsf', 'sdafdsf', 'dsfdf', 'fsdfsa', 'fdsfds', 'dsafds', '', 'fdsfas', '2021-09-16', 'sdfsda', 'dsfsf', 'sddsfsafsd', 'dfsdsaf', 'fdsa', 'sdfds', 'sfsdf', 'dsf', 'dsfdsa', 'dsfasd', 'Credit Application,Limited Warranty', 'sdfsaf', 'asdfsdfdsf', 'dsfdsf', 'fdsa', 'dsfsd', 'dsfdsf', 'sdffdsaf', 'dsafdsfsda', 'fdsfdsa', 'fdsfsda', 'fsdafsd', 'sdfdsafs', 'dfsdfsdfsd', 'fdsfasd', 'dsfdsfds', 'fdsfdsa', 'dsfdsaf', 'sadfsdfds', 'fdsfdsa', 'Application9.pdf', '1', '2021-09-18 11:17:04'),
(10, 'Testing', 'abn', 'anb trading', '', 'Ggfgg', '110032', 'Ggfgg', '502110', '01234567890', '', 'deepika@weblinkindia.net', '01234567890', 'ghfh', 'gfgfg', 'gfgf', 'gfghf', 'gfg', 'gfg', 'gfghf', 'gfgf', 'fghf', '2021-09-18', 'dghg', 'gfgf', 'gfg', 'fdg', 'gfd', 'fgf', 'gfgf', 'fgf', 'fg', 'dgd', 'Credit Application', 'fgfgDFD', 'FGF', '', '', '', '', 'Testing', '', '', 'Select State', '110032', '', '', '', 'ghghgh', 'fgf', 'fdf', 'dfdf', 'dgfdf', 'Application10.pdf', '1', '2021-09-18 11:48:55'),
(11, 'Dimpy', 'Abn', 'Trading ', 'Partnership,Pty Ltd', 'Delhi', '111111', 'Delhi', '111111', '09876543210', '', 'dimpy@gmail.com', '', 'Businees', '7876', 'rhfkj', 'kdjfghd', 'lkvjldk', 'fjkgnkjb', 'fdjghjghb', '9876543321', '6745756', '2021-09-20', '687495678947', '6745346529365', 'v jjkg gjkfnbnb', '87586788', '85798679', 'fngn ;thtrh', 'jhgthiorhi', 'fhgoiehriog', '873764645', 'ng jhghfgjfdb', 'Credit Application', 'Name', 'dgfh gf.knbfgkn', 'fuhgithiorth', 'huibftfugh', 'Delhi', '222212', 'Dimpy', 'trading', 'rithvseoirth', 'delhi', '111111', '47564986748', 'ufghiughif', 'ughiughiu', 'jiotj thn', 'kfjopjhkpoh', 'jfhghg', 'jdflhsdhf', '7456756', 'Application11.pdf', '1', '2021-09-20 15:57:16'),
(12, 'asd', 'sdf', 'sdf', 'Sole Trader', 'sdf', '110018', 'WZ 41 A/2 Sant Garh', '110018', '09811206803', '55', 'gurpreetsingh1366@gmail.com', '55', 'fregtfd', '555', '66', '77', 'sdfds@', '5567567', '47547', '567567', 'fghfgh', '2021-09-24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application12.pdf', '1', '2021-09-22 10:58:39'),
(13, 'asd', 'sdf', 'sdf', 'Sole Trader', 'sdf', '110018', 'WZ 41 A/2 Sant Garh', '110018', '09811206803', '55', 'gurpreetsingh1366@gmail.com', '55', 'fregtfd', '555', '66', '77', 'sdfds@', '5567567', '47547', '567567', 'fghfgh', '2021-09-24', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'dghdf', 'dfg', 'dfgdf', 'Application13.pdf', '1', '2021-09-22 10:59:09'),
(14, 'ritrhu', 'rjghiru', 'oghierh', 'Pty Ltd', 'wehutv ui', '35445', 'fjghgh', 'gbshlhg', 'jfgbljq', 'jihge', 'some@gmail.com', 'rjh uioth', 'uirghut', 'uiehu', 'uwhuiwrh', 'uhuihui', 'uihiuh@gmail.com', 'iofghih', 'ughih', 'uigbhoghoei', 'tjhktj', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application14.pdf', '1', '2021-09-23 10:14:15'),
(15, 'dimpy', 'kjghkj', 'jknhklh', 'Partnership', 'jh;jh', 'jb;', 'jbnkj', 'bjkbkj', 'bnbjk', 'bkjbkj', 'some@gmail.com', 'jbjguil', 'yluig', 'hlghg', 'jhklgh', 'hljgh', 'hjghjg', 'jhghj', 'ghjlg', 'hjlgjg', 'hjlgj', '2021-09-23', 'vjhvhlv', 'hjvlhg', '678697', '657575', '76757', 'vbnvjhv', '679867', 'bjhg', '6786789', '7869676', 'Guarantee', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'bjk;kh', 'jkh;kih;oijp\'', 'bmnbkjuhliu', 'h6787968', '67867869', 'Application15.pdf', '1', '2021-09-23 10:51:25'),
(16, 'TESTING', '754744', '', '', 'fghfgh ghfgh', '110015', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Guarantee', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application16.pdf', '1', '2021-10-18 09:18:40'),
(17, 'Wayne OBrien', '21069120341', '', '', '2/23 Foster Street', '2010', '2/23 Foster Street', '2010', '450830270', '', 'wayne@waycorp.com.au', '', 'Waycorp Services Pty Ltd', 'Waycorp Services Pty Ltd', '', '', 'wayne@waycorp.com.au', '450830270', '', '', '', '', '1000', '', '', '', '', '', '', '', '', '', 'Credit Application', 'George Smith', 'Ringo Starr', '', 'Piccadiliy Lane ', '', '', 'Wayne OBrien', 'Waycorp Services Pty Ltd', '', 'NSW', '2010', '', '', '', '', '', '', '', '', 'Application17.pdf', '1', '2021-10-18 10:31:52'),
(18, 'Wayne OBrien', '21069120341', '', '', '2/23 Foster Street', '2010', '2/23 Foster Street', '2010', '450830270', '', 'wayne@waycorp.com.au', '', 'Waycorp Services Pty Ltd', 'Waycorp Services Pty Ltd', '', '', 'wayne@waycorp.com.au', '450830270', '', '', '', '', '1000', '', '', '', '', '', '', '', '', '', 'Credit Application', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application18.pdf', '1', '2021-10-18 10:53:47'),
(19, 'deepika saxena', '544545', '', '', '1', '1234', '', '', '', '', '', '', '', '', '', '', '', '01234567890', '', '', '', '', 'jhkjhk', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application19.pdf', '1', '2021-10-19 06:49:38'),
(20, 'Waycorp', '21069132550', '', 'Pty Ltd', '2/23 Foster Street', '2010', '2/23 FOSTER ST', '2010', '01300323410', '', '', '', '22642413241', '22642413241', '', '', 'wayne@waycorp.com.au', '450830270', '', '', '', '', '10000', '', '', '', '', '', '', '', '', '', '', 'George Smith', 'Ringo Starr', '', '', '', '', 'Wayne OBrien', 'Waycorp Services Pty Ltd', '', 'NSW', '2010', '1322', '', '', '', '', '', '', '', 'Application20.pdf', '1', '2021-10-21 09:08:33'),
(21, 'Kuldeep', 'test#3333', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application21.pdf', '1', '2021-10-22 11:20:59'),
(22, 'Kuldeep', 'test#3333', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application22.pdf', '1', '2021-10-22 11:41:26'),
(23, 'dinesh', 'dinu45454', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application23.pdf', '1', '2021-10-22 11:44:56'),
(24, 'durg', 'durg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application24.pdf', '1', '2021-10-22 11:47:01'),
(25, 'dipika', 'dipika', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Application25.pdf', '1', '2021-10-22 12:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_ack_signatory`
--

CREATE TABLE `wl_app_ack_signatory` (
  `id` int(11) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `ack_sig_print_name` varchar(100) DEFAULT NULL,
  `ack_sig_position` varchar(100) DEFAULT NULL,
  `ack_sig_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_ack_signatory`
--

INSERT INTO `wl_app_ack_signatory` (`id`, `app_id`, `ack_sig_print_name`, `ack_sig_position`, `ack_sig_date`) VALUES
(7, 4, 'sfsdfsa', 'sdfafas', '2021-09-27'),
(6, 4, 'sdfa', 'sdfasfa', '2021-09-24'),
(5, 4, 'sdafs', 'fsda', '2021-09-20'),
(8, 7, 'sdfsfsda', 'fsdfdsa', '2021-09-21'),
(9, 7, 'fdsfsda', 'sdfsafdas', '2021-09-13'),
(10, 9, 'sdfsafsd', 'fsdafsdfsd', '2021-10-05'),
(11, 9, 'fsdfsdffsd', 'sdfsfsad', '2021-09-14'),
(12, 10, 'gdfgg', 'dfd', '2021-09-18'),
(13, 11, 'print name', 'Position', '2021-09-20'),
(14, 17, 'Wayne O\'Brie', '', ''),
(15, 18, 'Wayne O\'Brie', '', ''),
(16, 20, 'Wayne O\'Brien', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_ack_witness`
--

CREATE TABLE `wl_app_ack_witness` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `ack_wit_print_name` varchar(100) DEFAULT NULL,
  `ack_wit_position` varchar(100) DEFAULT NULL,
  `ack_wit_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_ack_witness`
--

INSERT INTO `wl_app_ack_witness` (`id`, `app_id`, `ack_wit_print_name`, `ack_wit_position`, `ack_wit_date`) VALUES
(6, 4, 'dasfasf', 'dsfsas', '2021-09-21'),
(5, 4, 'sdafs', 'dasffdas', '2021-09-22'),
(7, 7, 'sdfsafs', 'dafsdfdas', '2021-09-28'),
(8, 9, 'dsfasfsd', 'fsdafsdfsd', '2021-09-20'),
(9, 9, 'fdsfsda', 'sdfsafasd', '2021-09-21'),
(10, 11, 'print', 'position', '2021-09-20'),
(11, 17, 'Ringo Starr', '', ''),
(12, 18, 'Ringo Starr', 'Drummer', '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_cert_guarantor`
--

CREATE TABLE `wl_app_cert_guarantor` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `per_gua_print_name` varchar(100) DEFAULT NULL,
  `per_gua_address` varchar(100) DEFAULT NULL,
  `per_gua_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_cert_guarantor`
--

INSERT INTO `wl_app_cert_guarantor` (`id`, `app_id`, `per_gua_print_name`, `per_gua_address`, `per_gua_date`) VALUES
(6, 4, 'sdfadas', 'fasdads', '2021-09-30'),
(5, 4, 'sdfad', 'dsafsafd', '2021-09-27'),
(7, 4, 'fsdfssad', 'dsfaads', '2021-09-27'),
(8, 7, 'dsfa', 'dsfas', '2021-09-21'),
(9, 7, 'sdffsa', 'fdsfds', '2021-09-20'),
(10, 9, 'dsfasfds', 'fdsfsafsd', '2021-09-21'),
(11, 9, 'fsdafs', 'fdsfsa', '2021-09-20'),
(12, 10, 'FGF', 'fdf', '2021-09-18'),
(13, 11, 'jht bfh', 'fghnfh', '2021-09-20'),
(14, 20, 'Wayne OBrien', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_cert_witness`
--

CREATE TABLE `wl_app_cert_witness` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `per_wit_print_name` varchar(100) DEFAULT NULL,
  `per_wit_address` varchar(100) DEFAULT NULL,
  `per_wit_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_cert_witness`
--

INSERT INTO `wl_app_cert_witness` (`id`, `app_id`, `per_wit_print_name`, `per_wit_address`, `per_wit_date`) VALUES
(5, 4, 'fdsaf', 'afdsafsa', '2021-10-06'),
(6, 4, 'asfd', 'dasfds', '2021-09-21'),
(7, 4, 'fdsafsad', 'dsfsaf', '2021-09-09'),
(8, 4, 'fsdafsad', 'sdfsa', '2021-09-02'),
(9, 7, 'dsfasf', 'dsafdsafa', '2021-09-21'),
(10, 9, 'sdfaf', 'sdfdsfdda', '2021-09-28'),
(11, 11, 'ftjbtj h', 'nhjiortyo', '2021-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_cirt_gua_signatory`
--

CREATE TABLE `wl_app_cirt_gua_signatory` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `cirt_gua_print_name` varchar(100) DEFAULT NULL,
  `cirt_gua_address` varchar(100) DEFAULT NULL,
  `cirt_gua_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_cirt_gua_signatory`
--

INSERT INTO `wl_app_cirt_gua_signatory` (`id`, `app_id`, `cirt_gua_print_name`, `cirt_gua_address`, `cirt_gua_date`) VALUES
(6, 4, 'sdfsa', 'dsafsafdasf', '2021-09-21'),
(5, 4, 'dsfsafdas', 'fdsafasfdsa', '2021-09-28'),
(7, 7, 'sdfdsafas', 'fsdfsda', '2021-09-22'),
(8, 7, 'dsfsa', 'fdsfas', '2021-09-06'),
(9, 9, 'dsfdsa', 'fdsafdsfds', '2021-09-21'),
(10, 9, 'fdsafds', 'sdfsafds', '2021-09-21'),
(11, 10, 'fgfg', 'gdf', ''),
(12, 11, 'tyjorjy;o', 'oituyiort', '2021-09-20'),
(13, 20, 'Wayne O\'Brien', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_cirt_gua_witness`
--

CREATE TABLE `wl_app_cirt_gua_witness` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `cirt_wit_print_name` varchar(100) DEFAULT NULL,
  `cirt_wit_address` varchar(100) DEFAULT NULL,
  `cirt_wit_date` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_cirt_gua_witness`
--

INSERT INTO `wl_app_cirt_gua_witness` (`id`, `app_id`, `cirt_wit_print_name`, `cirt_wit_address`, `cirt_wit_date`) VALUES
(5, 4, 'dsfsa', 'fsafdsa', '2021-09-20'),
(6, 4, 'fdsafas', 'ds', '2021-09-23'),
(7, 4, 'sdfafdsa', 'dsfsa', '2021-09-23'),
(8, 7, 'dsafdasfsa', 'fsdafdas', '2021-09-13'),
(9, 9, 'sdfdsf', 'dfdsfdsa', '2021-09-22'),
(10, 11, 'hjgukhk', 'fghftyjuytj', '2021-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_soft_trade`
--

CREATE TABLE `wl_app_soft_trade` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `soft_trade_name` varchar(100) DEFAULT NULL,
  `soft_trade_position` varchar(100) DEFAULT NULL,
  `soft_trade_address` varchar(255) DEFAULT NULL,
  `soft_trade_mobile` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_soft_trade`
--

INSERT INTO `wl_app_soft_trade` (`id`, `app_id`, `soft_trade_name`, `soft_trade_position`, `soft_trade_address`, `soft_trade_mobile`) VALUES
(6, 4, 'dsfa', 'sdfa', 'fdas', 'fdsa'),
(5, 4, 'sadfs', 'fa', 'dsfas', 'dsfaa'),
(7, 7, 'sdfdsa', 'dsfsa', 'dsfsafsd', 'dsfdsa'),
(8, 9, 'sdfa', 'asdfsd', 'fsdfdssd', 'dsfsa'),
(9, 10, 'Testing', 'ghg', 'Ggfgg', '01234567890'),
(10, 11, 'tvuy8uyb', 'iyntuy', 'tunb8runb unev5nuyt ', '8946757'),
(11, 12, 'dfds', 'ddsdf', 'aasdfdsf', '47567'),
(12, 13, 'dfds', 'ddsdf', 'aasdfdsf', '47567'),
(13, 16, 'deepika saxena', '', '1', '+619015300933'),
(14, 17, 'Wayne OBrien', '', '2/23 Foster Street', '450830270'),
(15, 18, 'Wayne OBrien', '', '2/23 Foster Street', '450830270'),
(16, 19, 'deepika saxena', '', '1', '+619015300933'),
(17, 20, 'Wayne OBrien', '', '2/23 Foster Street', '450830270');

-- --------------------------------------------------------

--
-- Table structure for table `wl_app_trade_reference`
--

CREATE TABLE `wl_app_trade_reference` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `trade_reference_company` varchar(100) DEFAULT NULL,
  `trade_contact` varchar(100) DEFAULT NULL,
  `trade_phone` varchar(100) DEFAULT NULL,
  `trade_email` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_app_trade_reference`
--

INSERT INTO `wl_app_trade_reference` (`id`, `app_id`, `trade_reference_company`, `trade_contact`, `trade_phone`, `trade_email`) VALUES
(4, 4, 'sdfdas', 'dfdas', 'dsfas', 'sdfsa'),
(3, 4, 'sdfdas', 'sdfas', 'dsfa', 'sdfsa'),
(5, 5, 'sdfasfdsf', 'sdfsfsd', 'fsdfdsf', 'dsfdsfsd'),
(6, 7, 'sdfsa', 'fdsfa', 'sfsdfas', 'fdssda'),
(7, 7, 'dsafsad', 'fsd', 'dsfasd', 'dsfsad'),
(8, 7, 'sdfsa', 'dsafdsa', 'sdfa', 'fdsa'),
(9, 9, 'sdafs', 'fdsafdsf', 'dsfsdfsdf', 'dfdasf'),
(10, 9, 'sa', 'fdsf', 'fdsfds', 'dsfas'),
(11, 10, 'fhghfg', 'dgfg', 'gfgfg', 'dgf'),
(12, 10, 'fgfg', 'gfgf', 'gfgf', 'gfgf'),
(13, 11, 'reference', '8675476407', '47567867', 'SJHV HGFJBFGB'),
(14, 12, 'sdfsd', '56765', '5676', 'fghgfg@'),
(15, 13, 'sdfsd', '56765', '5676', 'fghgfg@'),
(16, 15, 'bkj', 'jhkbljh', 'kjhlglh', 'jhlkgh'),
(17, 17, 'Waycorp Services Pty Ltd', '', '', 'wayne@waycorp.com.au'),
(18, 17, 'Waycorp Services Pty Ltd', '', '450830270', ''),
(19, 17, '22642413241', '', '01300323410', 'wayne@waycorp.com.au'),
(20, 18, 'Waycorp Services Pty Ltd', '', '', 'wayne@waycorp.com.au'),
(21, 18, 'Waycorp Services Pty Ltd', '', '450830270', ''),
(22, 19, 'testing', '', '', ''),
(23, 20, 'Waycorp Services Pty Ltd', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_auto_respond_mails`
--

CREATE TABLE `wl_auto_respond_mails` (
  `id` int(11) NOT NULL,
  `email_section` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `email_subject` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_auto_respond_mails`
--

INSERT INTO `wl_auto_respond_mails` (`id`, `email_section`, `email_subject`, `email_content`, `status`, `updated_on`) VALUES
(1, 'Member Registration ', 'Welcome to {site_name}', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Hi {mem_name},</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">We are happy to have you as the newest member of {site_name}!</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">This is a registration email as per the details submitted by you. You are now registered on {site_name} with the following details::</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email ID:</strong></td>\r\n   <td>{username}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Password:</strong></td>\r\n   <td>{password}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">{link} to verify your account.</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Thanks again. We hope you will visit us again soon and put these special services to work for you.<br />\r\n   Please feel free to contact us if you have any questions at all.</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Thank you.<br />\r\n   {site_name} Customer Service<br />\r\n   Email: {admin_email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\" style=\"text-align:center\">{site_name}. All right reserved.</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '2', '2016-09-26 11:42:49'),
(2, 'Forgot Password', 'Forgot Password', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Hi {mem_name},</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Your login details are as follows:</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email ID:</strong></td>\r\n   <td>{username}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>password:</strong></td>\r\n   <td>{password}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Click here to login {link}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Thank you.<br />\r\n   {site_name} Customer Service<br />\r\n   Email: {admin_email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\" style=\"text-align:center\">{site_name}. All right reserved.</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '2', '2016-07-13 12:31:11'),
(3, 'Refer To Friends', 'Refer a Friend', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:2px solid rgb(233, 233, 233); margin-top:10px; width:600px\">\r\n <tbody>\r\n  <tr>\r\n   <td style=\"text-align:left\">Hi {friend_name},</td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>{your_name} has recommended this {text}, as {your_name} thinks you would like it.<br />\r\n   <br />\r\n   To view the Deal details please click on the following link.<br />\r\n   <br />\r\n   {site_link}</p>\r\n\r\n   <p> </p>\r\n\r\n   <p>Thanks and Regards,</p>\r\n\r\n   <p>{site_name} Team</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td style=\"text-align:center\">{site_name}. All right reserved.</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '2', '2016-07-13 12:30:51'),
(4, 'Donate', 'Enquiry Received on', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear {mem_name}</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Enquiry has been submitted with following info :</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email:</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Amount:</strong></td>\r\n   <td>{amount}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>comments:</strong></td>\r\n   <td>{comments}</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><br />\r\nThank you.<br />\r\n{site_name} Customer Service<br />\r\nEmail: {admin_email}</p>', '2', '2019-08-01 14:33:47'),
(5, 'Contact Us', 'Enquiry', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear {mem_name}</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Your Enquiry has been submitted with following info :</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email:</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Mobile no.:</strong></td>\r\n   <td>{mobile}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Comments:</strong></td>\r\n   <td>{comments}</td>\r\n  </tr>\r\n  <tr>\r\n   <td>We will be in touch with you shortly.</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><br />\r\nThank you.<br />\r\n{site_name} Customer Service<br />\r\nEmail: {admin_email}</p>', '1', '2020-12-07 05:39:40'),
(6, 'Admin Registration ', 'Welcome to {site_name}', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Hi Admin,</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">A new member has registered on {site_name} with the following details:</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Name:</strong></td>\r\n   <td>{name}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email ID:</strong></td>\r\n   <td>{username}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Password:</strong></td>\r\n   <td>{password}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Thank you.<br />\r\n   {site_name} Customer Service<br />\r\n   Email: {admin_email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\" style=\"text-align:center\">{site_name}. All right reserved.</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '2', '2016-10-13 15:54:15'),
(7, 'Admin Contact Us', 'Enquiry', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear Admin</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Enquiry has been submitted with following info :</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Name:</strong></td>\r\n   <td>{mem_name}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email:</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Mobile no.:</strong></td>\r\n   <td>{mobile}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Comments:</strong></td>\r\n   <td>{comments}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><br />\r\nThank you.<br />\r\n{site_name} Customer Service<br />\r\nEmail: {admin_email}</p>', '1', '2017-11-17 07:52:25'),
(8, 'Product Quote Enquiry', 'Resume Enquiry Received on {site_name}', '<table border=\"0\" xss=\"removed\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear {mem_name}</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">{body_text}</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Product Name:</strong></td>\r\n   <td>{product_name}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Name :</strong></td>\r\n   <td>{mem_name}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email :</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Mobile Number :</strong></td>\r\n   <td>{mobile}</td>\r\n  </tr>\r\n<tr>\r\n   <td><strong>Address :</strong></td>\r\n   <td>{address}</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>Thank you.<br>\r\n{site_name} Customer Service<br>\r\nEmail: {admin_email}</p>', '1', '2022-02-03 06:14:41'),
(9, 'Career Enquiry', 'Enquiry', '<table border=\"0\" xss=removed>\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear {mem_name}</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Callback request has been submitted with following info :</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n<tr>\r\n   <td><strong>Applying for.:</strong></td>\r\n   <td>{applying_for}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email:</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Mobile No.:</strong></td>\r\n   <td>{mobile}</td>\r\n  </tr>\r\n  \r\n </tbody>\r\n</table>\r\n\r\n<p><br>\r\nThank you.<br>\r\n{site_name} Customer Service<br>\r\nEmail: {admin_email}</p>', '1', '2022-02-02 06:54:18'),
(10, 'Home Contact Us', 'Services Enquiry', '<table border=\"0\" style=\"width:100%\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"2\"><strong>Dear {mem_name}</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\">Your Enquiry has been submitted with following info :</td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Services:</strong></td>\r\n   <td>{service_type}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Email:</strong></td>\r\n   <td>{email}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Mobile no.:</strong></td>\r\n   <td>{mobile}</td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>Comments:</strong></td>\r\n   <td>{comments}</td>\r\n  </tr>\r\n  <tr>\r\n   <td>We will be in touch with you shortly.</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"2\"> </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><br />\r\nThank you.<br />\r\n{site_name} Customer Service<br />\r\nEmail: {admin_email}</p>', '1', '2020-12-07 05:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `wl_banners`
--

CREATE TABLE `wl_banners` (
  `banner_id` int(11) NOT NULL,
  `banner_position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_url` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_page` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Like : home page,category page',
  `status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1=Actice,0=Inactive',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `banner_added_date` datetime DEFAULT NULL,
  `banner_start_date` datetime DEFAULT NULL,
  `banner_end_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_banners`
--

INSERT INTO `wl_banners` (`banner_id`, `banner_position`, `banner_image`, `banner_url`, `banner_page`, `status`, `clicks`, `banner_added_date`, `banner_start_date`, `banner_end_date`) VALUES
(5, NULL, 'inner-banner2.jpg', 'http://www.google.com', 'inner_page', '1', 0, '2021-12-07 11:33:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_blogs`
--

CREATE TABLE `wl_blogs` (
  `blogs_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT 'parent_id= last level of category ',
  `category_ids` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_links` varchar(80) CHARACTER SET utf8 NOT NULL,
  `blog_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `friendly_url` varchar(110) CHARACTER SET utf8 DEFAULT NULL,
  `brochure_doc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `posted_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_description` text CHARACTER SET utf8 NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `is_latest` int(11) DEFAULT '0',
  `status` enum('0','1','2') CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `added_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `blog_viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `blog_alt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `xls_type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_blogs`
--

INSERT INTO `wl_blogs` (`blogs_id`, `category_id`, `category_ids`, `category_links`, `blog_name`, `friendly_url`, `brochure_doc`, `posted_by`, `blog_description`, `likes`, `is_latest`, `status`, `added_date`, `updated_date`, `blog_viewed`, `blog_alt`, `xls_type`, `youtube`) VALUES
(4, 0, NULL, '', 'News blogs', 'news-blogs', NULL, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n\r\n<p>Sed hendrerit tortor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus ornare ornare pulvinar. Vivamus ornare faucibus sollicitudin. Sed suscipit gravida tincidunt. Duis eu luctus lacus, nec consectetur massa. Phasellus orci mauris, tristique quis dolor sit</p>\r\n', 41, 0, '1', '2019-12-27 13:44:35', '2021-12-08 12:55:36', 0, '', '0', ''),
(5, 0, NULL, '', 'Newsrooms2', 'newsrooms2', NULL, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n\r\n<p>Sed hendrerit tortor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus ornare ornare pulvinar. Vivamus ornare faucibus sollicitudin. Sed suscipit gravida tincidunt. Duis eu luctus lacus, nec consectetur massa. Phasellus orci mauris, tristique quis dolor sit</p>\r\n', 19, 0, '1', '2019-12-27 13:45:11', '2021-12-08 12:55:14', 0, '', '0', ''),
(6, 0, NULL, '', 'fgfgfg', 'fgfgfg', NULL, '', '<p>fgfgf</p>\r\n', 2, 0, '1', '2020-01-03 10:23:05', '2021-12-08 12:55:06', 0, '', '0', ''),
(7, 0, NULL, '', 'news blog test', 'news-blog-test', NULL, '', '<p><span xss=removed>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam enim lacus, viverra non volutpat nec, vulputate a lorem. Praesent id aliquet felis. Pellentesque hendrerit, dolor nec porttitor elementum, mi nunc pharetra diam, quis consequat felis metus non odio. Aliquam dignissim ornare aliquam. Cras dignissim, nisl in posuere tempus, dui libero facilisis libero, id finibus ipsum risus non augue. Suspendisse consectetur scelerisque eleifend. Etiam at lacinia elit.</span><br xss=removed>\r\n<br xss=removed>\r\n<span xss=removed>Etiam aliquam sapien tristique, malesuada turpis a, consequat neque. Phasellus non leo efficitur, vehicula erat sit amet, eleifend leo. Integer eu vehicula erat. Sed egestas fermentum lobortis. Donec aliquet venenatis dolor, et lacinia nulla rutrum facilisis. Maecenas in tortor imperdiet, iaculis nunc ac, tristique arcu. Aliquam erat volutpat. Vestibulum aliquet velit enim.</span><br xss=removed>\r\n<br xss=removed>\r\n<span xss=removed>Aenean hendrerit dolor eget elementum auctor. Cras turpis quam, accumsan et nunc maximus, varius euismod urna. Curabitur urna ex, condimentum nec lectus at, finibus rutrum felis. Pellentesque pharetra augue ipsum, ac suscipit massa congue in. Nam ac nibh non orci suscipit condimentum et at ipsum. Proin dignissim ligula eget metus accumsan volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec sollicitudin magna eget felis hendrerit, at feugiat arcu consequat. In a felis quis sapien luctus imperdiet vel eget tellus. Praesent mattis nunc quis arcu lobortis, sed mattis orci lacinia. Quisque gravida sed massa accumsan auctor. Sed ac finibus velit. Nam a eleifend tellus. Quisque volutpat posuere nisi eget pharetra. Ut non tincidunt eros, at feugiat dolor. Sed eget ornare dolor, id tincidunt metus.</span></p>\r\n', 0, 0, '1', '2021-12-08 12:16:37', NULL, 0, 'alt blog tag', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_blogs_media`
--

CREATE TABLE `wl_blogs_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `blogs_id` int(10) UNSIGNED NOT NULL,
  `media_type` enum('photo','video','pdf') CHARACTER SET utf8 NOT NULL DEFAULT 'photo',
  `media` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_blogs_media`
--

INSERT INTO `wl_blogs_media` (`id`, `blogs_id`, `media_type`, `media`, `is_default`, `sort_order`, `date_added`) VALUES
(1, 1, 'photo', 'blog-img-2.jpg', 'Y', NULL, '2019-12-06 10:45:52'),
(2, 2, 'photo', 'blog-img-21.jpg', 'Y', NULL, '2019-12-06 10:46:46'),
(3, 3, 'photo', 'blog-det-banner.jpg', 'Y', NULL, '2019-12-06 10:47:05'),
(4, 4, 'photo', 'blog-img11.jpg', 'Y', NULL, '2021-12-08 12:55:36'),
(5, 5, 'photo', 'blog-img3.jpg', 'Y', NULL, '2021-12-08 12:55:14'),
(6, 7, 'photo', 'blog-img1.jpg', 'Y', NULL, '2021-12-08 12:16:37'),
(7, 6, 'photo', 'blog-img2.jpg', 'N', NULL, '2021-12-08 12:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `wl_brands`
--

CREATE TABLE `wl_brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `friendly_url` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_description` text COLLATE utf8_unicode_ci,
  `brand_date_added` datetime DEFAULT NULL,
  `brand_date_updated` datetime DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_featured` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Delete'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_brands`
--

INSERT INTO `wl_brands` (`brand_id`, `brand_name`, `friendly_url`, `brand_url`, `brand_image`, `brand_description`, `brand_date_added`, `brand_date_updated`, `sort_order`, `is_featured`, `status`) VALUES
(60, '', '', NULL, 'avatar1661059665_1.jpg', NULL, '2022-08-20 22:21:03', NULL, NULL, '0', '1'),
(61, '', '', '', 'avatar1661059682_1.jpg', '', '2022-08-20 22:28:07', NULL, NULL, '0', '1'),
(62, '', '', NULL, 'avatar1661059719_1.jpg', NULL, '2022-08-20 22:28:24', NULL, NULL, '0', '1'),
(63, '', '', '', 'avatar1661059735_1.jpg', '', '2022-08-20 22:29:01', NULL, NULL, '0', '1'),
(64, '', '', '', 'avatar1661059749_1.jpg', '', '2022-08-20 22:29:14', NULL, NULL, '0', '1'),
(65, '', '', NULL, 'avatar1672234973_1.jpg', NULL, '2022-08-20 22:29:30', NULL, NULL, '0', '1'),
(66, '', '', '', 'avatar1661059779_1.jpg', '', '2022-08-20 22:29:45', NULL, NULL, '0', '1'),
(67, '', '', '', 'avatar1661059797_1.jpg', '', '2022-08-20 22:30:06', NULL, NULL, '0', '1'),
(68, '', '', NULL, 'avatar1672234995_1.jpg', NULL, '2022-08-20 22:30:22', NULL, NULL, '0', '1'),
(69, '', '', '', 'avatar1661059830_1.jpg', '', '2022-08-20 22:30:36', NULL, NULL, '0', '1'),
(70, '', '', '', 'avatar1661059844_1.jpg', '', '2022-08-20 22:30:49', NULL, NULL, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wl_brouchers`
--

CREATE TABLE `wl_brouchers` (
  `broucher_id` int(11) NOT NULL,
  `broucher_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gallery_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `broucher_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `broucher_doc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `broucher_date_added` datetime DEFAULT NULL,
  `broucher_date_updated` datetime DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_featured` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Delete'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_brouchers`
--

INSERT INTO `wl_brouchers` (`broucher_id`, `broucher_name`, `gallery_file`, `friendly_url`, `broucher_url`, `broucher_doc`, `mobile_number`, `broucher_date_added`, `broucher_date_updated`, `sort_order`, `is_featured`, `status`) VALUES
(22, 'Broucher for movies', 'cat-img2.jpg', NULL, NULL, 'class-vii-annual-syllabus-2021-22-middle-unit-converted-5-11.pdf', NULL, '2020-04-09 10:40:03', NULL, NULL, '0', '1'),
(23, 'Broucher for travel', 'cat-img4.jpg', NULL, NULL, 'class-vii-annual-syllabus-2021-22-middle-unit-converted-5-12.pdf', NULL, '2020-04-13 22:10:28', NULL, NULL, '0', '1'),
(24, 'Broucher for Accessories', 'cat-img1.jpg', NULL, NULL, 'class-vii-annual-syllabus-2021-22-middle-unit-converted-51.pdf', NULL, '2020-04-13 22:11:20', NULL, NULL, '0', '1'),
(25, 'Brochure 1', '', NULL, NULL, 'class-vii-annual-syllabus-2021-22-middle-unit-converted-5-1.pdf', NULL, '2022-02-15 11:06:16', NULL, NULL, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wl_categories`
--

CREATE TABLE `wl_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_description` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_popular` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `is_fixed` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `is_featured` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `type` int(11) DEFAULT '1' COMMENT '1=Product, 2=Projects',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `meta_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_alt` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `commision_rate` float(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_categories`
--

INSERT INTO `wl_categories` (`category_id`, `category_name`, `friendly_url`, `category_image`, `category_description`, `parent_id`, `is_popular`, `is_fixed`, `is_featured`, `type`, `sort_order`, `date_added`, `date_modified`, `status`, `meta_title`, `meta_keywords`, `meta_description`, `category_alt`, `commision_rate`) VALUES
(38, 'Fire & low smoke', 'fire-low-smoke', 'cat_img3.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>\r\n', 0, '0', '0', '0', 1, 3, '2019-08-01 13:21:15', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'carpets', 0.00),
(47, 'sub categorytest', 'blind/sub-categorytest', 'cat_img12.jpg', '<p>c cxxc xc</p>\r\n', 37, '0', '0', '0', 1, 13, '2020-12-14 10:04:17', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'sub categorytest', 0.00),
(48, 'sub categorytest2', 'blind/sub-categorytest2', 'cat_img21.jpg', '<p>sub categorytest2  description</p>\r\n', 37, '0', '0', '0', 1, 14, '2020-12-14 10:04:40', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'sub categorytest2', 0.00),
(37, 'Medium Voltage', 'medium-voltage', 'cat_img11.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>\r\n', 0, '0', '0', '0', 1, 1, '2019-08-01 13:19:56', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'Blinds', 0.00),
(28, 'Wires', 'wires', 'cat_img2.jpg', '<p><span font-size:=\"\" open=\"\" text-align:=\"\" xss=\"removed\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span></p>\r\n', 0, '0', '0', '0', 1, 2, '2019-05-06 16:48:48', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'Festivals', 0.00),
(49, 'testing sub category', 'blind/testing-sub-category', 'cat_img31.jpg', '<p>sdafsf dsfas</p>\r\n', 37, '0', '0', '0', 1, 15, '2021-01-23 09:51:43', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'testing sub category', 0.00),
(50, 'Low voltage power', 'low-voltage-power', 'cat_img4.jpg', '<p>content here</p>\r\n', 0, '0', '0', '0', 1, 4, '2021-06-14 05:20:38', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'new category', 0.00),
(53, 'Carpets subcategory', 'carpet/carpets-subcategory', 'cat_img1.jpg', '<p>Carpets subcategory</p>\r\n', 38, '0', '0', '0', 1, 17, '2022-02-03 04:15:08', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'Carpets subcategory', 0.00),
(54, 'Low voltage power 2', 'low-voltage-power-2', 'cat_img41.jpg', '<p>Low voltage power 2</p>\r\n', 0, '0', '0', '0', 1, 18, '2022-02-03 09:43:36', '0000-00-00 00:00:00', '1', NULL, NULL, NULL, 'Low voltage power 2', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `wl_city`
--

CREATE TABLE `wl_city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `friendly_url` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_description` text COLLATE utf8_unicode_ci,
  `city_date_added` datetime DEFAULT NULL,
  `city_date_updated` datetime DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_featured` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Delete'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_city`
--

INSERT INTO `wl_city` (`city_id`, `city_name`, `friendly_url`, `city_url`, `city_image`, `city_description`, `city_date_added`, `city_date_updated`, `sort_order`, `is_featured`, `status`) VALUES
(25, 'test city1', 'test-city1', NULL, 'clie-1.jpg', NULL, '2019-12-30 06:00:26', NULL, NULL, '0', '1'),
(23, 'test city2', 'test-city2', NULL, 'clie-2.jpg', NULL, '2019-12-30 05:59:29', NULL, NULL, '0', '1'),
(24, 'test city3', 'test-city3', NULL, 'clie-4.jpg', NULL, '2019-12-30 06:00:12', NULL, NULL, '0', '1'),
(26, 'test city4', 'test-city4', NULL, 'clie-6.jpg', NULL, '2019-12-30 06:00:41', NULL, NULL, '0', '1'),
(28, 'test city', 'test-city', NULL, 'clie-5.jpg', NULL, '2021-01-14 07:38:16', NULL, NULL, '0', '1'),
(29, 'Noida', 'noida', '', '', '', '2021-12-06 07:01:29', NULL, NULL, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wl_cms_pages`
--

CREATE TABLE `wl_cms_pages` (
  `page_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `page_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `friendly_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `page_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `page_short_description` text COLLATE utf8_unicode_ci,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Deleted',
  `page_updated_date` datetime NOT NULL,
  `title_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_size` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_cms_pages`
--

INSERT INTO `wl_cms_pages` (`page_id`, `parent_id`, `page_name`, `friendly_url`, `page_description`, `page_short_description`, `image`, `status`, `page_updated_date`, `title_background`, `img_size`) VALUES
(1, 0, 'Who  We Are', 'aboutus', '', NULL, 'jba-who-we-are-header1.jpg', '1', '2022-11-30 18:11:29', '#96833f', '1400X400'),
(2, 0, 'Welcome to JBA', 'about-company', '<p style=\"text-align: justify;\"><span style=\"color:#FFFFFF;\"><strong><span style=\"font-size:26px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">JBA Agency is a full-service creative agency, busy designing projects &amp; building new experiences for brands. we provide marketing solutions for your businesses &amp; a range of services; event management, campaign management, photography, video shoots, 3D &amp; 2D animation, and infographics, that will allow you to target the psychographics &amp; demographics of your ideal audience, to reach them with the right content at the right time.</span></span></strong></span></p>', NULL, 'jba-who-we-are-copy.jpg', '1', '2023-01-16 08:54:58', '#ffffff', '1400X600'),
(3, 0, 'Services', 'resourses', '', NULL, 'jba-service-copy.jpg', '1', '2022-11-30 16:31:31', '#96833f', '1400X600'),
(5, 0, 'Overview of the Company', 'homemessage', '<strong>RESCAB</strong> was established as a closed joint stock company in 2008 in the Kingdom of Saudi Arabia with a capital of 370 million SAR. RESCAB is a member of Al-Abdullatif group of companies. The largest two shareholders in RESCAB are Al-Abdullatif Group Holding Company and Al-Abdullatif Industrial Investment Company.\r\n<br><br>\r\n<strong>RESCAB</strong> manufactures are distributes power cables and wires of all types and sizes and caters to the growing demand of the local market, as well as exports to other markets especially in neighboring countries.', NULL, '', '2', '2022-02-03 10:57:55', NULL, ''),
(6, 0, 'Industries we serve', 'industries-we-serve', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:32:02', NULL, ''),
(9, 0, 'Terms & Conditions', 'terms-conditions', '<div class=\"mt-3\" xss=\"removed\">\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">TERMS OF USE</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Reefwind Pty Ltd trading as &ldquo;Pilbara Powder Coatings&rdquo;&nbsp;</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings offers this site to you subject to the following terms and conditions (&quot;Terms&quot;). Your use of this site shall be your agreement to abide by each of the terms set forth below. If you do not agree with any of these terms, please do not use this site. If you have any questions about these terms, please contact us at&nbsp;<a href=\"mailto:contactus@pilbarapowdercoating.com.au\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(63, 63, 63); text-decoration-line: none; background-color: transparent;\">contactus@pilbarapowdercoating.com.au</a>.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">General Use Restrictions</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings or third parties granting rights to Pilbara Powder Coatings hold all right, title, and interest in and to the materials on this Site, which are the copyrighted work of Pilbara Powder Coatings or such third parties. Pilbara Powder Coatings grants you a limited, personal, non-exclusive and non-transferable license to use and display the materials only on your personal computer only for purposes associated with your interaction with this Pilbara Powder Coatings website. Except as stated herein, you have no right to copy, download, display, perform, reproduce, distribute, modify, edit, alter or enhance any of the materials in any manner. This limited license terminates automatically, without notice to you, if you breach any of these Terms. Upon termination, you must immediately destroy any downloaded and printed materials. You have no right, title or interest (and no copyright, trademark or other intellectual property right) in or to the Site or any materials and you agree not to &quot;frame&quot; or &quot;mirror&quot; the Site, any material contained on or accessible from this Site on any other server or Internet-based device without the advanced written authorization of Pilbara Powder Coatings. Please note that these Terms and Conditions also apply to your use of any of our smartphone apps.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings does not routinely monitor your postings to the site but reserves the right so to do. However, in our efforts to promote good citizenship within the Internet community, if Pilbara Powder Coatings becomes aware of inappropriate use of the site or any of its services, Pilbara Powder Coatings will respond in any way that, in its sole discretion, Pilbara Powder Coatings deems appropriate. You acknowledge that Pilbara Powder Coatings will have the right to report to law enforcement authorities any actions that may be considered illegal, as well as any reports it receives of such conduct. When requested, Pilbara Powder Coatings will cooperate fully with law enforcement agencies in any investigation of alleged illegal activity on the Internet.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Availability</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings and Reefwind Pty Ltd have several websites offering products, services, content and various other functionalities (collectively the &quot;Services&quot;) to specific regions worldwide. The Services offered in one region may differ from those in other regions due to availability, local or regional laws, shipment and other considerations. Pilbara Powder Coatings does not make any warranty or representation that a user in one region may obtain the services from the Pilbara Powder Coatings site in another region and Pilbara Powder Coatings may cancel a user&#39;s order or redirect a user to the site for that user&#39;s region if a user attempts to order Services offered on a site in another region.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Information Pilbara Powder Coatings publishes on the World Wide Web may contain references or cross references to Pilbara Powder Coatings products, programs and services that are not announced or available in your country. Such references do not imply that Pilbara Powder Coatings intends to announce such products, programs or services in your country. Consult your local Pilbara Powder Coatings business contact for information regarding the products, programs and services which may be available to you.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Links</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">From Pilbara Powder Coatings to Third Party Websites</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">In your use of this Pilbara Powder Coatings website, you may enter into correspondence with, purchase goods or services from, or participate in promotions of advertisers, members or sponsors of this website. Any such correspondence, advertisement, purchase or promotion, including the delivery of and the payment for goods and/or services, and any other term, condition, warranty or representation associated with such correspondence, purchase or promotion, is solely between you and the applicable third party. You agree that Pilbara Powder Coatings has no liability, obligation or responsibility for any such correspondence, purchase or promotion between you and any such third party.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">This website may be linked to other sites on the World Wide Web that are not under the control of or maintained by Pilbara Powder Coatings. Such links do not constitute an endorsement by Pilbara Powder Coatings of any such sites. You acknowledge that Pilbara Powder Coatings is providing these links to you only as a convenience, and you agree that Pilbara Powder Coatings is not responsible for the content or links displayed on such sites to which you may be linked.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">From Third Party Websites to Pilbara Powder Coatings</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">All links to Pilbara Powder Coatings websites by third parties (&ldquo;linking party&rdquo;) must be approved in writing by Pilbara Powder Coatings, except that Pilbara Powder Coatings consents to links in which the link and the pages that are activated by the link do not:</div>\r\n\r\n<ul style=\"box-sizing: border-box; margin: 15px 0px 0px 10px; padding-right: 0px; padding-left: 15px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Create frames or border environments around any page or materials on the Pilbara Powder Coatings website or use other techniques that alter in any way the visual presentation or appearance of any content within the Pilbara Powder Coatings website;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Create comparisons of Pilbara Powder Coatings products or services with competitive products or services;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Shall not replicate any materials on that Pilbara Powder Coatings website or any Pilbara Powder Coatings logo or trademark without separate writing consent by Pilbara Powder Coatings;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Misrepresent your relationship with Pilbara Powder Coatings;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Imply that Pilbara Powder Coatings approves or endorses the linking party, the linking party&rsquo;s website, or the linking party&rsquo;s service or product offerings in any way;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Present false or misleading impressions about Pilbara Powder Coatings or otherwise damage the goodwill associated with the Pilbara Powder Coatings name;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Contain content that could be construed as abusive, threatening, harassing, profane or offensive, excessively violent, or which violates or encourages others to violate any applicable law;</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Contains content that could be construed as obscene, pornographic or sexually explicit materials; and</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">Shall contain only content that is appropriate for all age groups.</li>\r\n	<li style=\"box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; border: 0px; outline: none;\">As a further condition to being permitted to link to this site, the linking party agrees that Pilbara Powder Coatings may at any time, in its sole discretion, terminate permission to link to this website. In any such event, the linking party agrees to immediately remove all links to this website.</li>\r\n</ul>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">By linking to the Pilbara Powder Coatings website, the linking party agrees to indemnify and hold Pilbara Powder Coatings safe and harmless from, and against, any and all loss, cost, damage, claims, actions, or liability arising from any such link. Pilbara Powder Coatings shall have no liability for any indirect, exemplary, incidental, punitive, special, or consequential damages with regard to the linking or use of such link. Pilbara Powder Coatings makes no warranties express or implied, with respect to such links.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">If, according to the above terms, the linking party&rsquo;s requirement to link to a&nbsp;Pilbara Powder Coatings website requires approval by Pilbara Powder Coatings, or the linking party is in any doubt about the linking party&rsquo;s need to have the link approved, please use the contact link at the foot of the Pilbara Powder Coatings.com homepage and select &lsquo;website feedback&rsquo; from the dropdown as your subject, describing your request in the question area, and a&nbsp;Pilbara Powder Coatings representative will contact you.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Disclaimer of Warranties</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Your use of this Site is at your own risk. This Site may include inaccuracies or errors that may affect the quality of the materials on the Site. The materials have not been independently verified or authenticated in whole or in part by Pilbara Powder Coatings. Pilbara Powder Coatings does not warrant the accuracy or timeliness of the materials. Pilbara Powder Coatings has no liability for any errors or omissions in the materials, whether provided by Pilbara Powder Coatings or third parties.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">THIS SITE IS PROVIDED TO YOU ON AN &quot;AS IS&quot; AND &quot;WHERE-IS&quot; BASIS, WITHOUT ANY WARRANTY. PILBARA POWDER COATINGS, FOR ITSELF AND ANY THIRD PARTY PROVIDING MATERIALS, SERVICES, OR CONTENT TO THIS WEBSITE, MAKE NO REPRESENTATIONS OR WARRANTIES IN CONNECTION WITH THE SITE INCLUDING BUT NOT LIMITED TO THE QUALITY, SUITABILITY, TRUTH, ACCURACY OR COMPLETENESS OF ANY MATERIAL, INFORMATION, PRODUCT, OR SERVICE CONTAINED ON THE SITE. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED. PILBARA POWDER COATINGS WILL NOT BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, ARISING FROM OR CONNECTED WITH THE SITE, INCLUDING BUT NOT LIMITED TO, YOUR USE OF THIS SITE OR YOUR INABILITY TO USE THE SITE, EVEN IF PILBARA POWDER COATINGS HAS PREVIOUSLY BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Exception for EU citizens: IF YOU ARE RESIDENT IN THE EUROPEAN UNION, PILBARA POWDER COATINGS DOES NOT EXCLUDE OR LIMIT ITS LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Inaccuracies or Errors</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">The descriptions, pictures and other representations of products on this site may contain inaccuracies and errors. Pilbara Powder Coatings does not make any warranty or representation with respect to the accuracy or completeness of any such information.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Furthermore, the prices and availability of products on this site may change without notice to you at any time in Pilbara Powder Coating&#39;s sole discretion.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings shall have the right to refuse or cancel any orders placed for product listed at incorrect price. Pilbara Powder Coatings shall have the right to refuse or cancel any such orders whether or not the order has been confirmed and your credit card charged. If your credit card has already been charged for the purchase and your order is cancelled, Pilbara Powder Coatings shall promptly issue a credit to your credit card account in the amount of the charge.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Local Laws; Export Control</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings controls and operates this site from its headquarters in Australia&nbsp;and makes no representation that the materials are appropriate or will be available for use in other locations. Unless otherwise explicitly stated, all marketing or promotional materials found on this Site are solely directed to individuals, companies or other entities located in Australian. If you use this site from outside Australia, you are entirely responsible for compliance with applicable local laws, including but not limited to the export and import regulations.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">The materials on this Site are subject to Australian Laws and Regulations. Diversion of such materials contrary to Australian law is prohibited. Neither the materials, nor any information acquired through the use of the Site, may be acquired for, shipped, transferred, or re-exported, directly or indirectly, to proscribed or embargoed countries or their nationals, nor may it be used for nuclear activities, chemical biological weapons, or missile projects, unless specifically authorized by the Australian Government for such purposes. You shall comply strictly with all Australian export laws and assume sole responsibility for obtaining licenses to export or re-export as may be required.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Terms of Use Revisions</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Pilbara Powder Coatings may revise these Terms at any time without notice by updating this posting. Your continued use of the Site after such modifications have been made will constitute your acceptance of such revised Terms.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Applicable Law</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Any action related to these Terms will be governed by the law of the State of Western Australia, without regard to the choice or conflicts of law provisions of any jurisdiction. You agree to submit to the jurisdiction of the courts located in the State of Western Australia, for the resolution of all disputes arising from or related to these Terms and/or your use of the Site.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; font-weight: bolder;\">Unauthorised Activities</span></div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Submissions and unauthorized use of any materials contained on this Site may violate copyright laws, trademark laws, the laws of privacy and publicity, certain communications statutes and regulations and other applicable laws and regulations. You alone are responsible for your actions or the actions of any person using your user name and/or password. As such, you shall indemnify and hold Pilbara Powder Coatings and its officers, directors, employees, affiliates, agents, licensors, and business partners harmless from and against any and all costs, damages, liabilities, and expenses (including attorneys&#39; fees) incurred in relation to, arising from, or for the purpose of avoiding, any claim or demand from a third party that your use of the Site or the use of the Site by any person using your user name and/or password (including without limitation your participation in the posting areas or your Submissions) violates any applicable law or regulation, or the rights of any third party.</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">&nbsp;</div>\r\n\r\n<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: none; color: rgb(33, 37, 41); font-family: Poppins, Arial, Helvetica, sans-serif; font-size: 14px;\">Date of Last Revision: September 2021</div>\r\n</div>\r\n', NULL, '', '2', '2021-10-14 08:49:47', NULL, ''),
(10, 0, 'Quality', 'quality', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:35:49', NULL, ''),
(11, 0, 'Privacy Policy', 'privacy-policy', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:34:25', NULL, ''),
(12, 0, 'Our team', 'our-team', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:33:50', NULL, ''),
(13, 0, 'Mission', 'mission', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:33:38', NULL, '');
INSERT INTO `wl_cms_pages` (`page_id`, `parent_id`, `page_name`, `friendly_url`, `page_description`, `page_short_description`, `image`, `status`, `page_updated_date`, `title_background`, `img_size`) VALUES
(14, 0, 'Group Of Company', 'group-of-company', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:32:21', NULL, ''),
(15, 0, 'Who We Are', 'who-we-are', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:36:08', NULL, ''),
(16, 0, 'Production Quality', 'production-quality', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-04 04:52:02', NULL, ''),
(17, 0, 'Projects', 'projects', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:35:25', NULL, ''),
(18, 0, 'Latest Blog Post', 'latest-blog-post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.', NULL, '', '2', '2021-12-09 00:00:00', NULL, ''),
(19, 0, 'Services', 'services', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:36:19', NULL, ''),
(20, 0, 'Daily LME Details', 'daily-lme-details', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:32:11', NULL, ''),
(21, 0, 'QHSE Policy', 'qhse-policy', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-04 04:51:28', NULL, ''),
(22, 0, 'Vision', 'vision', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:33:38', NULL, ''),
(23, 0, 'Director Message', 'director-message', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" style=\"width: 400px; height: 201px;\" />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br />\r\n<br />\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br />\r\n<br />\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Full-fledged engineering and construction solutions</li>\r\n	<li>State-of-the-art, well-equipped fabrication yard</li>\r\n	<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Architectural and structural steel installation services</li>\r\n	<li>Project mobilization, planning &amp; engineering services</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, '', '2', '2022-02-02 04:33:38', NULL, ''),
(26, 0, 'Contact Us', 'contactus', '<ul>\r\n <li>\r\n <div class=\"contact_area\">\r\n <div class=\"circle_sec\"> </div>\r\n\r\n <p class=\"cnt_heading\">Registered Office</p>\r\n\r\n <div class=\"sec_cnt\">33 & 33A, Rama Road, Industrial Area, Near Kirti Nagar Metro Station, New Delhi - 110015 (INDIA)</div>\r\n </div>\r\n </li>\r\n <li>\r\n <div class=\"contact_area\">\r\n <div class=\"circle_sec\"> </div>\r\n\r\n <p class=\"cnt_heading\">Phone</p>\r\n\r\n <div class=\"sec_cnt black\">+91-11-47584787<br />\r\n + 91 - 8854874587</div>\r\n </div>\r\n </li>\r\n <li>\r\n <div class=\"contact_area\">\r\n <div class=\"circle_sec\"> </div>\r\n\r\n <p class=\"cnt_heading\">Email</p>\r\n\r\n <div class=\"sec_cnt\">\r\n <p><a href=\"mailto:info@abc.com\">info@abc.com</a></p>\r\n </div>\r\n </div>\r\n </li>\r\n</ul>\r\n', NULL, '', '2', '2017-06-23 12:41:05', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_countries`
--

CREATE TABLE `wl_countries` (
  `id` int(11) NOT NULL,
  `country_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `country_temp_name` varchar(250) NOT NULL,
  `country_iso_code_2` char(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `country_iso_code_3` char(3) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `address_format_id` int(11) NOT NULL DEFAULT '0',
  `is_feature` char(1) NOT NULL DEFAULT '0' COMMENT '0=Normal,1=Featured',
  `premimum_ads_avl` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=Premimum ads not available, 1=Premimum ads available',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Deleted',
  `cont_currency` varchar(20) DEFAULT NULL,
  `TimeZone` varchar(32) NOT NULL,
  `UTC_offset` varchar(8) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_countries`
--

INSERT INTO `wl_countries` (`id`, `country_name`, `country_temp_name`, `country_iso_code_2`, `country_iso_code_3`, `address_format_id`, `is_feature`, `premimum_ads_avl`, `status`, `cont_currency`, `TimeZone`, `UTC_offset`, `created_by`) VALUES
(1, 'Afghanistan', 'afghanistan', 'AF', 'AFG', 1, '0', '0', '1', '', 'Asia/Kabul', '', 1),
(2, 'Albania', 'albania', 'AL', 'ALB', 1, '0', '1', '1', '', 'Europe/Tirane', '', 1),
(3, 'Algeria', 'algeria', 'DZ', 'DZA', 1, '0', '1', '1', '', 'Africa/Algiers', '+01:00', 1),
(4, 'American Samoa', 'americansamoa', 'AS', 'ASM', 1, '0', '1', '1', '', 'Pacific/Pago_Pago', '', 1),
(5, 'Andorra', 'andorra', 'AD', 'AND', 1, '0', '1', '1', '', 'Europe/Andorra', '', 1),
(6, 'Angola', 'angola', 'AO', 'AGO', 1, '0', '1', '1', '', 'Africa/Luanda', '+01:00', 1),
(7, 'Anguilla', 'anguilla', 'AI', 'AIA', 1, '1', '1', '1', '', 'America/Anguilla', '', 1),
(8, 'Antarctica', 'antarctica', 'AQ', 'ATA', 1, '0', '1', '1', '', 'Antarctica/Vostok', '', 1),
(9, 'Antigua and Barbuda', 'antiguaandbarbuda', 'AG', 'ATG', 1, '0', '1', '1', '', 'America/Antigua', '', 1),
(10, 'Argentina', 'argentina', 'AR', 'ARG', 1, '1', '1', '1', '', 'America/Argentina/Ushuaia', '', 1),
(11, 'Armenia', 'armenia', 'AM', 'ARM', 1, '0', '1', '1', '', 'Asia/Yerevan', '', 1),
(12, 'Aruba', 'aruba', 'AW', 'ABW', 1, '0', '1', '1', '', 'America/Aruba', '', 1),
(13, 'Australia', 'australia', 'AU', 'AUS', 1, '0', '1', '1', 'AUD', 'Australia/Sydney', '', 1),
(14, 'Austria', 'austria', 'AT', 'AUT', 5, '0', '1', '1', 'EUR', 'Europe/Vienna', '', 1),
(15, 'Azerbaijan', 'azerbaijan', 'AZ', 'AZE', 1, '0', '1', '1', '', 'Asia/Baku', '', 1),
(16, 'Bahamas', 'bahamas', 'BS', 'BHS', 1, '0', '1', '1', '', 'America/Nassau', '', 1),
(17, 'Bahrain', 'bahrain', 'BH', 'BHR', 1, '0', '1', '1', '', 'Asia/Bahrain', '', 1),
(18, 'Bangladesh', 'bangladesh', 'BD', 'BGD', 1, '0', '1', '1', '', 'Asia/Dhaka', '', 1),
(19, 'Barbados', 'barbados', 'BB', 'BRB', 1, '0', '1', '1', '', 'America/Barbados', '', 1),
(20, 'Belarus', 'belarus', 'BY', 'BLR', 1, '0', '1', '1', '', 'Europe/Minsk', '', 1),
(21, 'Belgium', 'belgium', 'BE', 'BEL', 1, '0', '1', '1', 'EUR', 'Europe/Brussels', '', 1),
(22, 'Belize', 'belize', 'BZ', 'BLZ', 1, '0', '1', '1', '', 'America/Belize', '', 1),
(23, 'Benin', 'benin', 'BJ', 'BEN', 1, '0', '1', '1', '', 'Africa/Porto-Novo', '+01:00', 1),
(24, 'Bermuda', 'bermuda', 'BM', 'BMU', 1, '0', '1', '1', '', 'Atlantic/Bermuda', '', 1),
(25, 'Bhutan', 'bhutan', 'BT', 'BTN', 1, '0', '1', '1', '', 'Asia/Thimphu', '', 1),
(26, 'Bolivia', 'bolivia', 'BO', 'BOL', 1, '0', '1', '1', '', 'America/La_Paz', '', 1),
(27, 'Bosnia and Herzegowina', 'bosniaandherzegowina', 'BA', 'BIH', 1, '0', '1', '1', '', 'Europe/Sarajevo', '', 1),
(28, 'Botswana', 'botswana', 'BW', 'BWA', 1, '0', '1', '1', '', 'Africa/Gaborone', '+02:00', 1),
(29, 'Bouvet Island', 'bouvetisland', 'BV', 'BVT', 1, '0', '1', '1', '', '', '', 1),
(30, 'Brazil', 'brazil', 'BR', 'BRA', 1, '0', '1', '1', 'BRL', 'America/Sao_Paulo', '', 1),
(31, 'British Indian Ocean Territory', 'britishindianoceanterritory', 'IO', 'IOT', 1, '0', '1', '1', '', 'Indian/Chagos', '', 1),
(32, 'Brunei Darussalam', 'bruneidarussalam', 'BN', 'BRN', 1, '0', '1', '1', '', 'Asia/Brunei', '', 1),
(33, 'Bulgaria', 'bulgaria', 'BG', 'BGR', 1, '0', '1', '1', '', 'Europe/Sofia', '', 1),
(34, 'Burkina Faso', 'burkinafaso', 'BF', 'BFA', 1, '0', '1', '1', '', 'Africa/Ouagadougou', '+00:00', 1),
(35, 'Burundi', 'burundi', 'BI', 'BDI', 1, '0', '1', '1', '', 'Africa/Bujumbura', '+02:00', 1),
(36, 'Cambodia', 'cambodia', 'KH', 'KHM', 1, '0', '1', '1', '', 'Asia/Phnom_Penh', '', 1),
(37, 'Cameroon', 'cameroon', 'CM', 'CMR', 1, '0', '1', '1', '', 'Africa/Douala', '+01:00', 1),
(38, 'Canada', 'canada', 'CA', 'CAN', 1, '0', '1', '1', 'CAD', 'America/Yellowknife', '', 1),
(39, 'Cape Verde', 'capeverde', 'CV', 'CPV', 1, '0', '1', '1', '', 'Atlantic/Cape_Verde', '', 1),
(40, 'Cayman Islands', 'caymanislands', 'KY', 'CYM', 1, '0', '1', '1', '', 'America/Cayman', '', 1),
(41, 'Central African Republic', 'centralafricanrepublic', 'CF', 'CAF', 1, '0', '1', '1', '', 'Africa/Bangui', '+01:00', 1),
(42, 'Chad', 'chad', 'TD', 'TCD', 1, '0', '1', '1', '', 'Africa/Ndjamena', '+01:00', 1),
(43, 'Chile', 'chile', 'CL', 'CHL', 1, '0', '1', '1', '', 'Pacific/Easter', '', 1),
(44, 'China', 'china', 'CN', 'CHN', 1, '1', '1', '1', 'CNY', 'Asia/Urumqi', '', 1),
(45, 'Christmas Island', 'christmasisland', 'CX', 'CXR', 1, '0', '1', '1', '', 'Indian/Christmas', '', 1),
(46, 'Cocos (Keeling) Islands', 'cocoskeelingislands', 'CC', 'CCK', 1, '0', '1', '1', '', 'Indian/Cocos', '', 1),
(47, 'Colombia', 'colombia', 'CO', 'COL', 1, '0', '1', '1', '', 'America/Bogota', '', 1),
(48, 'Comoros', 'comoros', 'KM', 'COM', 1, '0', '1', '1', '', 'Indian/Comoro', '', 1),
(49, 'Congo', 'congo', 'CG', 'COG', 1, '0', '1', '1', '', 'Africa/Brazzaville', '+01:00', 1),
(50, 'Cook Islands', 'cookislands', 'CK', 'COK', 1, '0', '1', '1', '', 'Pacific/Rarotonga', '', 1),
(51, 'Costa Rica', 'costarica', 'CR', 'CRI', 1, '0', '1', '1', '', 'America/Costa_Rica', '', 1),
(52, 'Cote D\'Ivoire', 'cotedivoire', 'CI', 'CIV', 1, '0', '1', '1', '', 'Africa/Abidjan', '+00:00', 1),
(53, 'Croatia', 'croatia', 'HR', 'HRV', 1, '0', '1', '1', '', 'Europe/Zagreb', '', 1),
(54, 'Cuba', 'cuba', 'CU', 'CUB', 1, '0', '1', '1', '', 'America/Havana', '', 1),
(55, 'Cyprus', 'cyprus', 'CY', 'CYP', 1, '0', '1', '1', '', 'Asia/Nicosia', '', 1),
(56, 'Czech Republic', 'czechrepublic', 'CZ', 'CZE', 1, '0', '1', '1', '', 'Europe/Prague', '', 1),
(57, 'Denmark', 'denmark', 'DK', 'DNK', 1, '0', '1', '1', 'DKK', 'Europe/Copenhagen', '', 1),
(58, 'Djibouti', 'djibouti', 'DJ', 'DJI', 1, '0', '1', '1', '', 'Africa/Djibouti', '+03:00', 1),
(59, 'Dominica', 'dominica', 'DM', 'DMA', 1, '0', '1', '1', '', 'America/Dominica', '', 1),
(60, 'Dominican Republic', 'dominicanrepublic', 'DO', 'DOM', 1, '0', '1', '1', '', 'America/Santo_Domingo', '', 1),
(61, 'East Timor', 'easttimor', 'TP', 'TMP', 1, '0', '1', '1', '', '', '', 1),
(62, 'Ecuador', 'ecuador', 'EC', 'ECU', 1, '0', '1', '1', '', 'Pacific/Galapagos', '', 1),
(63, 'Egypt', 'egypt', 'EG', 'EGY', 1, '0', '1', '1', 'EGP', 'Africa/Cairo', '+02:00', 1),
(64, 'El Salvador', 'elsalvador', 'SV', 'SLV', 1, '0', '1', '1', '', 'America/El_Salvador', '', 1),
(65, 'Equatorial Guinea', 'equatorialguinea', 'GQ', 'GNQ', 1, '0', '1', '1', '', 'Africa/Malabo', '+01:00', 1),
(66, 'Eritrea', 'eritrea', 'ER', 'ERI', 1, '0', '1', '1', '', 'Africa/Asmara', '+03:00', 1),
(67, 'Estonia', 'estonia', 'EE', 'EST', 1, '0', '1', '1', '', 'Europe/Tallinn', '', 1),
(68, 'Ethiopia', 'ethiopia', 'ET', 'ETH', 1, '0', '1', '1', '', 'Africa/Addis_Ababa', '+03:00', 1),
(69, 'Falkland Islands (Malvinas)', 'falklandislandsmalvinas', 'FK', 'FLK', 1, '0', '1', '1', '', 'Atlantic/Stanley', '', 1),
(70, 'Faroe Islands', 'faroeislands', 'FO', 'FRO', 1, '0', '1', '1', '', 'Atlantic/Faroe', '', 1),
(71, 'Fiji', 'fiji', 'FJ', 'FJI', 1, '0', '1', '1', 'FJD', 'Pacific/Fiji', '', 1),
(72, 'Finland', 'finland', 'FI', 'FIN', 1, '0', '1', '1', 'EUR', 'Europe/Helsinki', '', 1),
(73, 'France', 'france', 'FR', 'FRA', 1, '0', '1', '1', 'EUR', 'Europe/Paris', '', 1),
(74, 'France, Metropolitan', 'francemetropolitan', 'FX', 'FXX', 1, '0', '1', '1', '', '', '', 1),
(75, 'French Guiana', 'frenchguiana', 'GF', 'GUF', 1, '0', '1', '1', '', 'America/Cayenne', '', 1),
(76, 'French Polynesia', 'frenchpolynesia', 'PF', 'PYF', 1, '0', '1', '1', '', 'Pacific/Tahiti', '', 1),
(77, 'French Southern Territories', 'frenchsouthernterritories', 'TF', 'ATF', 1, '0', '1', '1', '', 'Indian/Kerguelen', '', 1),
(78, 'Gabon', 'gabon', 'GA', 'GAB', 1, '0', '1', '1', '', 'Africa/Libreville', '+01:00', 1),
(79, 'Gambia', 'gambia', 'GM', 'GMB', 1, '0', '1', '1', '', 'Africa/Banjul', '+00:00', 1),
(80, 'Georgia', 'georgia', 'GE', 'GEO', 1, '0', '1', '1', '', 'Asia/Tbilisi', '', 1),
(81, 'Germany', 'germany', 'DE', 'DEU', 5, '0', '1', '1', 'EUR', 'Europe/Berlin', '', 1),
(82, 'Ghana', 'ghana', 'GH', 'GHA', 1, '0', '1', '1', '', 'Africa/Accra', '+00:00', 1),
(83, 'Gibraltar', 'gibraltar', 'GI', 'GIB', 1, '0', '1', '1', '', 'Europe/Gibraltar', '', 1),
(84, 'Greece', 'greece', 'GR', 'GRC', 1, '0', '1', '1', '', 'Europe/Athens', '', 1),
(85, 'Greenland', 'greenland', 'GL', 'GRL', 1, '0', '1', '1', '', 'America/Thule', '', 1),
(86, 'Grenada', 'grenada', 'GD', 'GRD', 1, '0', '1', '1', '', 'America/Grenada', '', 1),
(87, 'Guadeloupe', 'guadeloupe', 'GP', 'GLP', 1, '0', '1', '1', '', 'America/Guadeloupe', '', 1),
(88, 'Guam', 'guam', 'GU', 'GUM', 1, '0', '1', '1', '', 'Pacific/Guam', '', 1),
(89, 'Guatemala', 'guatemala', 'GT', 'GTM', 1, '0', '1', '1', '', 'America/Guatemala', '', 1),
(90, 'Guinea', 'guinea', 'GN', 'GIN', 1, '0', '1', '1', '', 'Africa/Conakry', '+00:00', 1),
(91, 'Guinea-bissau', 'guineabissau', 'GW', 'GNB', 1, '0', '1', '1', '', 'Africa/Bissau', '+00:00', 1),
(92, 'Guyana', 'guyana', 'GY', 'GUY', 1, '0', '1', '1', '', 'America/Guyana', '', 1),
(93, 'Haiti', 'haiti', 'HT', 'HTI', 1, '0', '1', '1', '', 'America/Port-au-Prince', '', 1),
(94, 'Heard and Mc Donald Islands', 'heardandmcdonaldislands', 'HM', 'HMD', 1, '0', '1', '1', '', '', '', 1),
(95, 'Honduras', 'honduras', 'HN', 'HND', 1, '0', '1', '1', '', 'America/Tegucigalpa', '', 1),
(96, 'Hong Kong', 'hongkong', 'HK', 'HKG', 1, '0', '1', '1', 'HKD', 'Asia/Hong_Kong', '', 1),
(97, 'Hungary', 'hungary', 'HU', 'HUN', 1, '0', '1', '1', 'HUF', 'Europe/Budapest', '', 1),
(98, 'Iceland', 'iceland', 'IS', 'ISL', 1, '0', '1', '1', '', 'Atlantic/Reykjavik', '', 1),
(99, 'India', 'india', 'IN', 'IND', 1, '1', '1', '1', 'INR', 'Asia/Kolkata', '', 1),
(100, 'Indonesia', 'indonesia', 'ID', 'IDN', 1, '0', '1', '1', '', 'Asia/Pontianak', '', 1),
(101, 'Iran (Islamic Republic of)', 'iranislamicrepublicof', 'IR', 'IRN', 1, '0', '1', '1', '', 'Asia/Tehran', '', 1),
(102, 'Iraq', 'iraq', 'IQ', 'IRQ', 1, '0', '1', '1', '', 'Asia/Baghdad', '', 1),
(103, 'Ireland', 'ireland', 'IE', 'IRL', 1, '0', '1', '1', 'EUR', 'Europe/Dublin', '', 1),
(104, 'Israel', 'israel', 'IL', 'ISR', 1, '0', '1', '1', 'ILS', 'Asia/Jerusalem', '', 1),
(105, 'Italy', 'italy', 'IT', 'ITA', 1, '0', '1', '1', 'EUR', 'Europe/Rome', '', 1),
(106, 'Jamaica', 'jamaica', 'JM', 'JAM', 1, '0', '1', '1', '', 'America/Jamaica', '', 1),
(107, 'Japan', 'japan', 'JP', 'JPN', 1, '0', '1', '1', 'JPY', 'Asia/Tokyo', '', 1),
(108, 'Jordan', 'jordan', 'JO', 'JOR', 1, '0', '1', '1', '', 'Asia/Amman', '', 1),
(109, 'Kazakhstan', 'kazakhstan', 'KZ', 'KAZ', 1, '0', '1', '1', '', 'Asia/Qyzylorda', '', 1),
(110, 'Kenya', 'kenya', 'KE', 'KEN', 1, '0', '1', '1', '', 'Africa/Nairobi', '+03:00', 1),
(111, 'Kiribati', 'kiribati', 'KI', 'KIR', 1, '0', '1', '1', '', 'Pacific/Tarawa', '', 1),
(112, 'Korea, Democratic People\'s Republic of', 'koreademocraticpeoplesrepublicof', 'KP', 'PRK', 1, '0', '1', '1', '', 'Asia/Pyongyang', '', 1),
(113, 'Korea, Republic of', 'korearepublicof', 'KR', 'KOR', 1, '0', '1', '1', '', 'Asia/Seoul', '', 1),
(114, 'Kuwait', 'kuwait', 'KW', 'KWT', 1, '0', '1', '1', '', 'Asia/Kuwait', '', 1),
(115, 'Kyrgyzstan', 'kyrgyzstan', 'KG', 'KGZ', 1, '0', '1', '1', '', 'Asia/Bishkek', '', 1),
(116, 'Lao People\'s Democratic Republic', 'laopeoplesdemocraticrepublic', 'LA', 'LAO', 1, '0', '1', '1', '', 'Asia/Vientiane', '', 1),
(117, 'Latvia', 'latvia', 'LV', 'LVA', 1, '0', '1', '1', '', 'Europe/Riga', '', 1),
(118, 'Lebanon', 'lebanon', 'LB', 'LBN', 1, '0', '1', '1', '', 'Asia/Beirut', '', 1),
(119, 'Lesotho', 'lesotho', 'LS', 'LSO', 1, '0', '1', '1', '', 'Africa/Maseru', '+02:00', 1),
(120, 'Liberia', 'liberia', 'LR', 'LBR', 1, '0', '1', '1', '', 'Africa/Monrovia', '+00:00', 1),
(121, 'Libyan Arab Jamahiriya', 'libyanarabjamahiriya', 'LY', 'LBY', 1, '0', '1', '1', '', 'Africa/Tripoli', '+01:00', 1),
(122, 'Liechtenstein', 'liechtenstein', 'LI', 'LIE', 1, '0', '1', '1', '', 'Europe/Vaduz', '', 1),
(123, 'Lithuania', 'lithuania', 'LT', 'LTU', 1, '0', '1', '1', '', 'Europe/Vilnius', '', 1),
(124, 'Luxembourg', 'luxembourg', 'LU', 'LUX', 1, '0', '1', '1', '', 'Europe/Luxembourg', '', 1),
(125, 'Macau', 'macau', 'MO', 'MAC', 1, '0', '1', '1', '', 'Asia/Macau', '', 1),
(126, 'Macedonia, The Former Yugoslav Republic of', 'macedoniatheformeryugoslavrepublicof', 'MK', 'MKD', 1, '0', '1', '1', '', 'Europe/Skopje', '', 1),
(127, 'Madagascar', 'madagascar', 'MG', 'MDG', 1, '0', '1', '1', '', 'Indian/Antananarivo', '', 1),
(128, 'Malawi', 'malawi', 'MW', 'MWI', 1, '0', '1', '1', '', 'Africa/Blantyre', '+02:00', 1),
(129, 'Malaysia', 'malaysia', 'MY', 'MYS', 1, '0', '1', '1', '', 'Asia/Kuching', '', 1),
(130, 'Maldives', 'maldives', 'MV', 'MDV', 1, '0', '1', '1', '', 'Indian/Maldives', '', 1),
(131, 'Mali', 'mali', 'ML', 'MLI', 1, '0', '1', '1', '', 'Africa/Bamako', '+00:00', 1),
(132, 'Malta', 'malta', 'MT', 'MLT', 1, '0', '1', '1', '', 'Europe/Malta', '', 1),
(133, 'Marshall Islands', 'marshallislands', 'MH', 'MHL', 1, '0', '1', '1', '', 'Pacific/Majuro', '', 1),
(134, 'Martinique', 'martinique', 'MQ', 'MTQ', 1, '0', '1', '1', '', 'America/Martinique', '', 1),
(135, 'Mauritania', 'mauritania', 'MR', 'MRT', 1, '0', '1', '1', '', 'Africa/Nouakchott', '+00:00', 1),
(136, 'Mauritius', 'mauritius', 'MU', 'MUS', 1, '0', '1', '1', '', 'Indian/Mauritius', '', 1),
(137, 'Mayotte', 'mayotte', 'YT', 'MYT', 1, '0', '1', '1', '', 'Indian/Mayotte', '', 1),
(138, 'Mexico', 'mexico', 'MX', 'MEX', 1, '0', '1', '1', 'MXN', 'America/Tijuana', '', 1),
(139, 'Micronesia, Federated States of', 'micronesiafederatedstatesof', 'FM', 'FSM', 1, '0', '1', '1', '', 'Pacific/Pohnpei', '', 1),
(140, 'Moldova, Republic of', 'moldovarepublicof', 'MD', 'MDA', 1, '0', '1', '1', '', 'Europe/Chisinau', '', 1),
(141, 'Monaco', 'monaco', 'MC', 'MCO', 1, '0', '1', '1', '', 'Europe/Monaco', '', 1),
(142, 'Mongolia', 'mongolia', 'MN', 'MNG', 1, '0', '1', '1', '', 'Asia/Ulaanbaatar', '', 1),
(143, 'Montserrat', 'montserrat', 'MS', 'MSR', 1, '0', '1', '1', '', 'America/Montserrat', '', 1),
(144, 'Morocco', 'morocco', 'MA', 'MAR', 1, '0', '1', '1', '', 'Africa/Casablanca', '+00:00', 1),
(145, 'Mozambique', 'mozambique', 'MZ', 'MOZ', 1, '0', '1', '1', '', 'Africa/Maputo', '+02:00', 1),
(146, 'Myanmar', 'myanmar', 'MM', 'MMR', 1, '0', '1', '1', '', 'Asia/Rangoon', '', 1),
(147, 'Namibia', 'namibia', 'NA', 'NAM', 1, '0', '1', '1', '', 'Africa/Windhoek', '+01:00', 1),
(148, 'Nauru', 'nauru', 'NR', 'NRU', 1, '0', '1', '1', '', 'Pacific/Nauru', '', 1),
(149, 'Nepal', 'nepal', 'NP', 'NPL', 1, '0', '1', '1', '', 'Asia/Kathmandu', '', 1),
(150, 'Netherlands', 'netherlands', 'NL', 'NLD', 1, '0', '1', '1', 'EUR', 'Europe/Amsterdam', '', 1),
(151, 'Netherlands Antilles', 'netherlandsantilles', 'AN', 'ANT', 1, '0', '1', '1', '', '', '', 1),
(152, 'New Caledonia', 'newcaledonia', 'NC', 'NCL', 1, '0', '1', '1', '', 'Pacific/Noumea', '', 1),
(153, 'New Zealand', 'newzealand', 'NZ', 'NZL', 1, '0', '1', '1', 'NZD', 'Pacific/Chatham', '', 1),
(154, 'Nicaragua', 'nicaragua', 'NI', 'NIC', 1, '0', '1', '1', '', 'America/Managua', '', 1),
(155, 'Niger', 'niger', 'NE', 'NER', 1, '0', '1', '1', '', 'Africa/Niamey', '+01:00', 1),
(156, 'Nigeria', 'nigeria', 'NG', 'NGA', 1, '0', '1', '1', '', 'Africa/Lagos', '+01:00', 1),
(157, 'Niue', 'niue', 'NU', 'NIU', 1, '0', '1', '1', '', 'Pacific/Niue', '', 1),
(158, 'Norfolk Island', 'norfolkisland', 'NF', 'NFK', 1, '0', '1', '1', '', 'Pacific/Norfolk', '', 1),
(159, 'Northern Mariana Islands', 'northernmarianaislands', 'MP', 'MNP', 1, '0', '1', '1', '', 'Pacific/Saipan', '', 1),
(160, 'Norway', 'norway', 'NO', 'NOR', 1, '0', '1', '1', 'NOK', 'Europe/Oslo', '', 1),
(161, 'Oman', 'oman', 'OM', 'OMN', 1, '0', '1', '1', '', 'Asia/Muscat', '', 1),
(162, 'Pakistan', 'pakistan', 'PK', 'PAK', 1, '0', '1', '1', 'PKR', 'Asia/Karachi', '', 1),
(163, 'Palau', 'palau', 'PW', 'PLW', 1, '0', '1', '1', '', 'Pacific/Palau', '', 1),
(164, 'Panama', 'panama', 'PA', 'PAN', 1, '0', '1', '1', '', 'America/Panama', '', 1),
(165, 'Papua New Guinea', 'papuanewguinea', 'PG', 'PNG', 1, '0', '1', '1', '', 'Pacific/Port_Moresby', '', 1),
(166, 'Paraguay', 'paraguay', 'PY', 'PRY', 1, '0', '1', '1', '', 'America/Asuncion', '', 1),
(167, 'Peru', 'peru', 'PE', 'PER', 1, '0', '1', '1', '', 'America/Lima', '', 1),
(168, 'Philippines', 'philippines', 'PH', 'PHL', 1, '0', '1', '1', 'PHP', 'Asia/Manila', '', 1),
(169, 'Pitcairn', 'pitcairn', 'PN', 'PCN', 1, '0', '1', '1', '', 'Pacific/Pitcairn', '', 1),
(170, 'Poland', 'poland', 'PL', 'POL', 1, '0', '1', '1', 'PLN', 'Europe/Warsaw', '', 1),
(171, 'Portugal', 'portugal', 'PT', 'PRT', 1, '0', '1', '1', 'EUR', 'Europe/Lisbon', '', 1),
(172, 'Puerto Rico', 'puertorico', 'PR', 'PRI', 1, '0', '1', '1', '', 'America/Puerto_Rico', '', 1),
(173, 'Qatar', 'qatar', 'QA', 'QAT', 1, '0', '1', '1', '', 'Asia/Qatar', '', 1),
(174, 'Reunion', 'reunion', 'RE', 'REU', 1, '0', '1', '1', '', 'Indian/Reunion', '', 1),
(175, 'Romania', 'romania', 'RO', 'ROM', 1, '0', '1', '1', '', 'Europe/Bucharest', '', 1),
(176, 'Russian Federation', 'russianfederation', 'RU', 'RUS', 1, '0', '1', '1', 'RUB', 'Europe/Volgograd', '', 1),
(177, 'Rwanda', 'rwanda', 'RW', 'RWA', 1, '0', '1', '1', '', 'Africa/Kigali', '+02:00', 1),
(178, 'Saint Kitts and Nevis', 'saintkittsandnevis', 'KN', 'KNA', 1, '0', '1', '1', '', 'America/St_Kitts', '', 1),
(179, 'Saint Lucia', 'saintlucia', 'LC', 'LCA', 1, '0', '1', '1', '', 'America/St_Lucia', '', 1),
(180, 'Saint Vincent and the Grenadines', 'saintvincentandthegrenadines', 'VC', 'VCT', 1, '0', '1', '1', '', 'America/St_Vincent', '', 1),
(181, 'Samoa', 'samoa', 'WS', 'WSM', 1, '0', '1', '1', '', 'Pacific/Apia', '', 1),
(182, 'San Marino', 'sanmarino', 'SM', 'SMR', 1, '0', '1', '1', '', 'Europe/San_Marino', '', 1),
(183, 'Sao Tome and Principe', 'saotomeandprincipe', 'ST', 'STP', 1, '0', '1', '1', '', 'Africa/Sao_Tome', '+00:00', 1),
(184, 'Saudi Arabia', 'saudiarabia', 'SA', 'SAU', 1, '0', '1', '1', 'SAR', 'Asia/Riyadh', '', 1),
(185, 'Senegal', 'senegal', 'SN', 'SEN', 1, '0', '1', '1', '', 'Africa/Dakar', '+00:00', 1),
(186, 'Seychelles', 'seychelles', 'SC', 'SYC', 1, '0', '1', '1', '', 'Indian/Mahe', '', 1),
(187, 'Sierra Leone', 'sierraleone', 'SL', 'SLE', 1, '0', '1', '1', '', 'Africa/Freetown', '+00:00', 1),
(188, 'Singapore', 'singapore', 'SG', 'SGP', 4, '0', '1', '1', 'SGD', 'Asia/Singapore', '', 1),
(189, 'Slovakia (Slovak Republic)', 'slovakiaslovakrepublic', 'SK', 'SVK', 1, '0', '1', '1', '', 'Europe/Bratislava', '', 1),
(190, 'Slovenia', 'slovenia', 'SI', 'SVN', 1, '0', '1', '1', '', 'Europe/Ljubljana', '', 1),
(191, 'Solomon Islands', 'solomonislands', 'SB', 'SLB', 1, '0', '1', '1', '', 'Pacific/Guadalcanal', '', 1),
(192, 'Somalia', 'somalia', 'SO', 'SOM', 1, '0', '1', '1', '', 'Africa/Mogadishu', '+03:00', 1),
(193, 'South Africa', 'southafrica', 'ZA', 'ZAF', 1, '0', '1', '1', 'ZAR', 'Africa/Johannesburg', '+02:00', 1),
(194, 'South Georgia and the South Sandwich Islands', 'southgeorgiaandthesouthsandwichislands', 'GS', 'SGS', 1, '0', '1', '1', '', 'Atlantic/South_Georgia', '', 1),
(195, 'Spain', 'spain', 'ES', 'ESP', 3, '0', '1', '1', 'EUR', 'Europe/Madrid', '+01:00', 1),
(196, 'Sri Lanka', 'srilanka', 'LK', 'LKA', 1, '0', '1', '1', 'LKR', 'Asia/Colombo', '', 1),
(197, 'St. Helena', 'st.helena', 'SH', 'SHN', 1, '0', '1', '1', '', 'Atlantic/St_Helena', '', 1),
(198, 'St. Pierre and Miquelon', 'st.pierreandmiquelon', 'PM', 'SPM', 1, '0', '1', '1', '', 'America/Miquelon', '', 1),
(199, 'Sudan', 'sudan', 'SD', 'SDN', 1, '0', '1', '1', '', 'Africa/Khartoum', '+03:00', 1),
(200, 'Suriname', 'suriname', 'SR', 'SUR', 1, '0', '1', '1', '', 'America/Paramaribo', '', 1),
(201, 'Svalbard and Jan Mayen Islands', 'svalbardandjanmayenislands', 'SJ', 'SJM', 1, '0', '1', '1', '', 'Arctic/Longyearbyen', '', 1),
(202, 'Swaziland', 'swaziland', 'SZ', 'SWZ', 1, '0', '1', '1', '', 'Africa/Mbabane', '+02:00', 1),
(203, 'Sweden', 'sweden', 'SE', 'SWE', 1, '0', '1', '1', 'SEK', 'Europe/Stockholm', '', 1),
(204, 'Switzerland', 'switzerland', 'CH', 'CHE', 1, '0', '1', '1', 'CHF', 'Europe/Zurich', '', 1),
(205, 'Syrian Arab Republic', 'syrianarabrepublic', 'SY', 'SYR', 1, '0', '1', '1', '', 'Asia/Damascus', '', 1),
(206, 'Taiwan', 'taiwan', 'TW', 'TWN', 1, '0', '1', '1', 'TWD', 'Asia/Taipei', '', 1),
(207, 'Tajikistan', 'tajikistan', 'TJ', 'TJK', 1, '0', '1', '1', '', 'Asia/Dushanbe', '', 1),
(208, 'Tanzania, United Republic of', 'tanzaniaunitedrepublicof', 'TZ', 'TZA', 1, '0', '1', '1', '', 'Africa/Dar_es_Salaam', '+03:00', 1),
(209, 'Thailand', 'thailand', 'TH', 'THA', 1, '0', '1', '1', 'THB', 'Asia/Bangkok', '', 1),
(210, 'Togo', 'togo', 'TG', 'TGO', 1, '0', '1', '1', '', 'Africa/Lome', '+00:00', 1),
(211, 'Tokelau', 'tokelau', 'TK', 'TKL', 1, '0', '1', '1', '', 'Pacific/Fakaofo', '', 1),
(212, 'Tonga', 'tonga', 'TO', 'TON', 1, '0', '1', '1', '', 'Pacific/Tongatapu', '', 1),
(213, 'Trinidad and Tobago', 'trinidadandtobago', 'TT', 'TTO', 1, '0', '1', '1', '', 'America/Port_of_Spain', '', 1),
(214, 'Tunisia', 'tunisia', 'TN', 'TUN', 1, '0', '1', '1', '', 'Africa/Tunis', '+01:00', 1),
(215, 'Turkey', 'turkey', 'TR', 'TUR', 1, '0', '1', '1', 'TRY', 'Europe/Istanbul', '', 1),
(216, 'Turkmenistan', 'turkmenistan', 'TM', 'TKM', 1, '0', '1', '1', '', 'Asia/Ashgabat', '', 1),
(217, 'Turks and Caicos Islands', 'turksandcaicosislands', 'TC', 'TCA', 1, '0', '1', '1', '', 'America/Grand_Turk', '', 1),
(218, 'Tuvalu', 'tuvalu', 'TV', 'TUV', 1, '0', '1', '1', '', 'Pacific/Funafuti', '', 1),
(219, 'Uganda', 'uganda', 'UG', 'UGA', 1, '0', '1', '1', '', 'Africa/Kampala', '+03:00', 1),
(220, 'Ukraine', 'ukraine', 'UA', 'UKR', 1, '0', '1', '1', '', 'Europe/Zaporozhye', '', 1),
(221, 'United Arab Emirates', 'unitedarabemirates', 'AE', 'ARE', 1, '0', '1', '1', '', 'Asia/Dubai', '', 1),
(222, 'United Kingdom', 'unitedkingdom', 'GB', 'GBR', 1, '0', '1', '1', 'GBP', 'Europe/London', '', 1),
(223, 'United States', 'unitedstates', 'US', 'USA', 2, '0', '1', '1', 'USD', 'Pacific/Honolulu', '', 1),
(224, 'United States Minor Outlying Islands', 'unitedstatesminoroutlyingislands', 'UM', 'UMI', 1, '0', '1', '1', '', 'Pacific/Wake', '', 1),
(225, 'Uruguay', 'uruguay', 'UY', 'URY', 1, '0', '1', '1', '', 'America/Montevideo', '', 1),
(226, 'Uzbekistan', 'uzbekistan', 'UZ', 'UZB', 1, '0', '1', '1', '', 'Asia/Tashkent', '', 1),
(227, 'Vanuatu', 'vanuatu', 'VU', 'VUT', 1, '0', '1', '1', '', 'Pacific/Efate', '', 1),
(228, 'Vatican City State (Holy See)', 'vaticancitystateholysee', 'VA', 'VAT', 1, '0', '1', '1', '', 'Europe/Vatican', '', 1),
(229, 'Venezuela', 'venezuela', 'VE', 'VEN', 1, '0', '1', '1', '', 'America/Caracas', '', 1),
(230, 'Viet Nam', 'vietnam', 'VN', 'VNM', 1, '0', '1', '1', '', 'Asia/Ho_Chi_Minh', '', 1),
(231, 'Virgin Islands (British)', 'virginislandsbritish', 'VG', 'VGB', 1, '0', '1', '1', '', 'America/Tortola', '', 1),
(232, 'Virgin Islands (U.S.)', 'virginislandsu.s', 'VI', 'VIR', 1, '0', '1', '1', '', 'America/St_Thomas', '', 1),
(233, 'Wallis and Futuna Islands', 'wallisandfutunaislands', 'WF', 'WLF', 1, '0', '1', '1', '', 'Pacific/Wallis', '', 1),
(234, 'Western Sahara', 'westernsahara', 'EH', 'ESH', 1, '0', '1', '1', '', 'Africa/El_Aaiun', '+00:00', 1),
(235, 'Yemen', 'yemen', 'YE', 'YEM', 1, '0', '1', '1', '', 'Asia/Aden', '', 1),
(236, 'Yugoslavia', 'yugoslavia', 'YU', 'YUG', 1, '0', '1', '1', '', '', '', 1),
(237, 'Zaire', 'zaire', 'ZR', 'ZAR', 1, '0', '1', '1', '', '', '', 1),
(238, 'Zambia', 'zambia', 'ZM', 'ZMB', 1, '0', '1', '1', '', 'Africa/Lusaka', '+02:00', 1),
(239, 'Zimbabwe', 'zimbabwe', 'ZW', 'ZWE', 1, '0', '1', '1', '', 'Africa/Harare', '+02:00', 1),
(240, 'Tanzania', 'tanzania', NULL, NULL, 0, '0', '1', '1', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wl_currencies`
--

CREATE TABLE `wl_currencies` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `code` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `symbol_left` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `symbol_right` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `curr_image` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `decimal_place` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` float(15,8) NOT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `status` int(11) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_currencies`
--

INSERT INTO `wl_currencies` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `curr_image`, `decimal_place`, `value`, `is_default`, `status`, `date_modified`) VALUES
(1, 'US Dollars', 'USD', '$', '', '', '2', 1.00000000, 'N', 0, '2015-03-18 09:59:24'),
(2, 'Pound Sterling', 'GBP', '£', '', 'flagvNci.jpg', '2', 1.00000000, 'N', 0, '2010-10-30 09:59:24'),
(3, 'AED', 'AED', 'AED', '', '', '2', 1.00000000, 'Y', 1, '2010-10-30 09:59:24'),
(4, 'Indian Rupees', 'INR', '&amp;#8377;', '', '222sNk8.gif', '2', 1.00000000, 'N', 0, '2017-11-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wl_dynamic_pages`
--

CREATE TABLE `wl_dynamic_pages` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `page_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_description` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_fixed` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `page_alt` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_dynamic_pages`
--

INSERT INTO `wl_dynamic_pages` (`page_id`, `page_name`, `friendly_url`, `page_image`, `page_description`, `parent_id`, `is_fixed`, `sort_order`, `date_added`, `date_modified`, `status`, `page_alt`) VALUES
(1, 'Industries', 'industries', 'proj-2.jpg', '<div class=\"mt-3\" xss=\"removed\">Coatings for parts and components used in offshore environments have to fulfill very stringent requirements.</div>\r\n\r\n<div class=\"mt-3\" xss=\"removed\"><br>\r\nHigh humidity and saline air in combination with intensive UV radiation create a highly corrosive environment. At the same time the coating is exposed to extreme mechanical stress in the splash zone.</div>\r\n', 0, '0', 1, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '1', 'Industries'),
(4, 'Projects', 'projects', 'istock-533437662.png', '<div class=\"mt-3\" xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Architectural powder coatings offer sustainable building solutions. Our offering covers a broad color palette with textures and special effects available in a range from standard durable to hyper durable finishes. These enhance the beauty and longevity of buildings all over the world. The quality powder coating takes into account the climatic conditions of the country and place to offer the best durable solution.</span></span></span> </div>\r\n\r\n<div class=\"mt-3\" xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Our powder coatings can also help make create different impressions of the surface in innovative ways. For example, making A metal surface look like a wood surface and a coated surface look like that of anodized aluminum. Our powder coatings meet all the major architectural quality standards including, <strong xss=\"removed\"><a href=\"http://www.qualicoat.net/main/home.html\" xss=\"removed\">Qualicoat</a></strong> and <strong xss=\"removed\"><a href=\"http://gsb-international.de/en/\" xss=\"removed\">GSB</a></strong> as appropriate to the geographic requirements.</span></span> Our <span xss=\"removed\"><span xss=\"removed\">powder coatings are used on the following building components:</span></span></div>\r\n\r\n<ul>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Windows and doors</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Facades, curtain walling</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Sunshades and shutters</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Fencing, railings, and handrails</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Ceiling tiles</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Garage doors</span></div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div><span xss=\"removed\">Verandas, winter gardens, conservatories</span></div>\r\n </li>\r\n</ul>\r\n', 0, '0', 4, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '2', 'Projects'),
(2, 'Test Reports', 'test-reports', 'appl-pic3.jpg', '<div class=\"mt-3\" xss=\"removed\">\r\n<p xss=\"removed\">For maximum powder coating processes, thermoset materials are widely used since they offer a surface layer that is not only durable but also decorative. At Pilbara Powder Coatings, we are an Australian-based company engaged as the major provider of thermoset powder coating whose molecular weight is lesser than the thermoplastic materials and higher than the liquid coatings.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The thermoset powder coating system that we offer is a protective covering substance that is manufactured with polymer lines that are cross-linked by robust chemical bonds. Our thermoset powder coatings are making thin protective layers on the surfaces of metals. The thermoset powder coatings that we provide are high-temperature resistant, unlike other thermoplastics.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Cost-Effective Thermoset Powder Coating System</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">You can trust our thermoset powder coatings to steer clear of corrosion and all other types of degradation. The range of thermoset powder coatings delivered by us is preferred for their high level of resistance against impact, temperature, and corrosion. You can use them along with a large assortment of surface finishes, glosses, and colours for designing. Our thermoset powder coatings offer multiple advantages in terms of production, cost, and design.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The series of thermoset powder-coated we offer can be applied to both infrastructures and assets. You can count on our thermoset coatings to keep your coated substrate protected and to improve its appearance. The versatile thermoset powder coatings that you get from us can also be moulded to offer extra functionalities in varied applications. Call us right away to know more about our thermoset powder coatings or browse through our portfolio of powder coatings.</p>\r\n</div>\r\n', 0, '0', 3, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '2', 'Test Reports'),
(5, 'Urban Coatings', 'urban-coatings', 'istock-1283552835.png', '<div class=\"mt-3\" xss=\"removed\">\r\n<div class=\"intro-text\" xss=\"removed\">\r\n<p xss=\"removed\">The urban powder coatings that we offer combine the latest and newest colours with a patterned effect to render a texture illusion. The smooth-feel-like powder coatings offered by us at Pilbara Powder Coatings are manufactured using state-of-the-art polyester technology. The urban powder contains myriad shades that range from sultry eggplant to quirky teal etc.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The urban powder coatings are made to provide two simple processes to the coating while simultaneously offering a fun finish to the designer of their choice. The powder coatings we offer are known for their increasing number of exterior applications that maintain their good look for several years to come. Our series of urban powder coatings secures the top position in the present market, thanks to their range of exterior grade architectural products which are combined with functional coatings filled technology.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Colour Consistent Urban Powder Coatings</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">Our urban powder coatings are specially designed to offer long-lasting corrosion protection for all types of metals, irrespective of the environment. The powder coatings are durable and are known for their versatility. You can use the powder coatings in several applications and techniques involved for processing including electrostatic spraying, fluid bed dipping, and flock spraying.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">With our urban powder coatings, you need no primer for maintenance during its life cycle. They can be used for various application methods and are both safe and dependable. They protect galvanized and aluminium substrates. The powder coatings have ultra-resistance to sun, sand, sea, and salt. The powder coatings offer impressive impact and abrasion protection.</p>\r\n</div>\r\n</div>\r\n', 0, '0', 5, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '2', 'Urban Coatings'),
(6, 'Automotive Coatings', 'automotive-coatings', 'istock-184147044.png', '<div class=\"mt-3\" xss=\"removed\">\r\n<div xss=\"removed\">\r\n<div xss=\"removed\">\r\n<p xss=\"removed\">We at Pilbara Powder Coatings are the pioneer supplier of automotive coatings in Australia. Known for industry-leading high performance, our automotive coatings cater to some of the most reputable brands present in the automotive world. The powder coatings are high-temperature-based that are capable of withstanding temperatures as high as 1,800 degrees.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">All our automotive coatings are thermal cycling-resistant and thermal-shock resistant as well. These coatings are also great when it comes to resistance to both chemical and corrosion. They are known for their distinct and premium look and feel. The eco-friendly coatings we offer are budget-friendly and easy to apply as well.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Automotive Coatings to Protect Surfaces from Weathering</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">Our range of liquid automotive coatingsis specifically designed to be used inside the HVLP spray gun. The automotive coatings are neither dip coatings nor roll coatings or even brush coating. They can be largely incorporated in present liquid lines. It is easier for us to provide elaborate design and tailor-made features.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The series of automotive coatings we deliver are standard ceramic-based and has a reputation for resolving the old industry problems with their exclusive properties and fresh solutions. Our highest quality and high-temperature automotive coatings can deliver an aesthetically appealing premium finish. And unlike all other high-temperature powder coatings, our high-temperature and durable coatings promise long-lasting protection from corrosion during both thermal shock and thermal cycling.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Certified Automotive Coatings</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The automotive coatings also guarantee long-lasting protection against winter road salts exposure followed by dicing solutions. The automotive coatings are perfect for high-temperature parts. You can use them in headers and manifolds, exhaust tips, exhaust tubes, turbo housings, turbine pistons, and intercoolers. The thin-film properties of our powder coating enable the highest performance on applications having tight tolerance, especially when the vehicles are left open to violent environments.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">What makes our automotive coatings perfect for high-wear areas and places where other finishes quickly wear down, is their extreme durability and their resistance against abrasion, rock chipping, scratching. Call us and learn more about the application of our automotive coating.</p>\r\n</div>\r\n</div>\r\n</div>\r\n', 0, '0', 6, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '2', 'Automotive Coatings'),
(18, 'Banking', 'banking', '', '<p><img src=\"assets/designer/themes/default/images/ab.jpg\" alt=\"\" class=\"cms_pc\">Lor em Epsom dolor sit am et, consecrator diapausing el it. Anyone commode ligula get dolor. Anyone mass a. Cum sociis netsuke pen a ti bus et Magnesia dis parturient montes, nester ridiculous Mus. Done quam Felice, ultraists neck, pel lent es que mu, pretium quit, seem. Nulla con se quat mass a quit denim. Done pede jus to, frangible vel, a liquet neck, voluted get, arch. In denim jus to, ranchos ut, impurity a, venerates vitae, jus to. Null am dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br><br> Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. </p>\r\n<hr>\r\n<h2 class=\"mt-3\">What Are You Doing</h2>\r\n<p class=\"mt-2\">Lor em Epsom dolor sit am et, consecrator diapausing el it. Anyone commode ligula get dolor. Anyone mass a. Cum sociis netsuke pen a ti bus et Magnesia dis parturient montes, nester ridiculous Mus. Done quam Felice, ultraists neck, pel lent es que mu, pretium quit, seem. Nulla con se quat mass a quit denim. Done pede jus to, frangible vel, a liquet neck, voluted get, arch. In denim jus to, ranchos ut, impurity a, venerates vitae, jus to. </p>\r\n<hr>\r\n<ul>\r\n<li>Full-fledged engineering and construction solutions</li>\r\n<li>State-of-the-art, well-equipped fabrication yard</li>\r\n<li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n<ol>\r\n<li>Architectural and structural steel installation services</li>\r\n<li>Project mobilization, planning & engineering services</li>\r\n</ol>', 1, '0', 2, '2021-09-10 05:26:41', '0000-00-00 00:00:00', '1', 'Banking'),
(19, 'Retail', 'retail', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\">Lor em Epsom dolor sit am et, consecrator diapausing el it. Anyone commode ligula get dolor. Anyone mass a. Cum sociis netsuke pen a ti bus et Magnesia dis parturient montes, nester ridiculous Mus. Done quam Felice, ultraists neck, pel lent es que mu, pretium quit, seem. Nulla con se quat mass a quit denim. Done pede jus to, frangible vel, a liquet neck, voluted get, arch. In denim jus to, ranchos ut, impurity a, venerates vitae, jus to. Null am dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>\r\n\r\n<hr>\r\n<h2 class=\"mt-3\">What Are You Doing</h2>\r\n\r\n<p class=\"mt-2\">Lor em Epsom dolor sit am et, consecrator diapausing el it. Anyone commode ligula get dolor. Anyone mass a. Cum sociis netsuke pen a ti bus et Magnesia dis parturient montes, nester ridiculous Mus. Done quam Felice, ultraists neck, pel lent es que mu, pretium quit, seem. Nulla con se quat mass a quit denim. Done pede jus to, frangible vel, a liquet neck, voluted get, arch. In denim jus to, ranchos ut, impurity a, venerates vitae, jus to.</p>\r\n\r\n<hr>\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n', 1, '0', 3, '2021-09-10 05:27:01', '0000-00-00 00:00:00', '1', 'Retail'),
(3, 'Functions', 'functions', 'conver-image.png', '<div class=\"mt-3\" xss=\"removed\">\r\n<div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 gridBorder section facetSection\" xss=\"removed\">\r\n<div class=\"parsysFloat\" xss=\"removed\">\r\n<div class=\"text parbase section\" xss=\"removed\">\r\n<div class=\"component textComponent no-paging listStyle_1 desktop tablet mobile    first\" xss=\"removed\">\r\n<div xss=\"removed\">Thermoplastic powder coatings are formulated for toughness and chip resistance without a primer undercoat. They provide excellent corrosion and UV protection and can be sprayed electrostatically or fluid bed dipped. They’re tough enough for lasting performance in a wide variety of exterior applications, including outdoor furniture, light poles, sign posts, playground equipment, railings and stairways, fencing panels, bike racks, cable grids, battery trays, perforated steel and more.Specify Abcite thermoplastic powder coatings for its many benefits.</div>\r\n\r\n<ul xss=\"removed\">\r\n <li xss=\"removed\">\r\n <div>Available in white and black</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Superior corrosion resistance/salt spray results</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Superior adhesionImpact, abrasion and chemical resistance</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Suitable for fluidized bed or electrostatic spray application</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>One pass coating process/primeless application</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Environmentally friendly with zero VOCs5-year shelf life at 77°F</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Field repairable</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Soft feel</div>\r\n </li>\r\n <li xss=\"removed\">\r\n <div>Noise absorption</div>\r\n </li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"button parbase section\" xss=\"removed\"> </div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 gridBorder section facetSection\" xss=\"removed\">\r\n<div class=\"parsysFloat\" xss=\"removed\">\r\n<div class=\"flipBookReader parbase section\" xss=\"removed\">\r\n<div class=\"component flipBookReader desktop tablet mobile    first\" xss=\"removed\">\r\n<section xss=\"removed\">\r\n<article xss=\"removed\"> </article>\r\n</section>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 0, '0', 2, '2021-01-13 09:52:20', '0000-00-00 00:00:00', '1', 'Functions'),
(27, 'Sales', 'sales', '', '<div><p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br></div>\r\n\r\n<div><br></div>\r\n\r\n<div>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br></div>\r\n\r\n<div><br></div>\r\n\r\n<div>Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p></div>\r\n\r\n<div> </div>\r\n\r\n<div><p> </p></div>\r\n\r\n<div> </div>\r\n\r\n<div><ul></div>\r\n\r\n<div><li>Full-fledged engineering and construction solutions</li></div>\r\n\r\n<div><li>State-of-the-art, well-equipped fabrication yard</li></div>\r\n\r\n<div><li>Highly-skilled and experienced technical crew</li></div>\r\n\r\n<div></ul></div>\r\n\r\n<div> </div>\r\n\r\n<div><p> </p></div>\r\n\r\n<div> </div>\r\n\r\n<div><p> </p></div>\r\n\r\n<div> </div>\r\n\r\n<div><ol></div>\r\n\r\n<div><li>Architectural and structural steel installation services</li></div>\r\n\r\n<div><li>Project mobilization, planning & engineering services</li></div>\r\n\r\n<div></ol></div>\r\n\r\n<div> </div>\r\n\r\n<div><p> </p></div>\r\n', 3, '0', 11, '2022-02-15 11:52:37', '0000-00-00 00:00:00', '1', 'Sales'),
(21, 'PPA 571', 'ppa-571', '', '<div class=\"container\" xss=\"removed\">\r\n<div class=\"inner_box\" xss=\"removed\">\r\n<div class=\"cms\" xss=\"removed\">\r\n<div class=\"row\" xss=\"removed\">\r\n<div class=\"col-lg-9 no_pad pr-lg-4 order-2 order-lg-1\" xss=\"removed\">\r\n<div class=\"mt-3\" xss=\"removed\">\r\n<div class=\"mt-3\" xss=\"removed\">\r\n<div xss=\"removed\"><b xss=\"removed\">Plascoat PPA 571</b> is a glossy, highly durable plastic coating that is ideal for applications on outdoor items that are exposed to demanding urban or coastal environments. PPA 571 will not crack, chip, flake or fracture. It has a proven track record in providing long-term protection for metal without damaging the environment. With excellent impact and sand abrasion resistance as well as superior resistance to sun, salt, and sea water, PPA 571 will remain maintenance-free for decades.</div>\r\n\r\n<div xss=\"removed\"> </div>\r\n\r\n<div xss=\"removed\">From fencing,<i xss=\"removed\"> s</i>treet furniture, lamp posts, and hand rails, to automotive parts, stadium seat frames, and wire baskets, the coating can be used in diverse applications. PPA 571 has received approvals for contact with food and drinking water and is one of only a few commercially available coatings that is recognized as being DDA, ‘Warm-to-the-Touch’ compliant.</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 4, '0', 5, '2021-09-20 11:37:34', '0000-00-00 00:00:00', '1', 'PPA 571'),
(22, 'INTERPON D2525', 'interpon-d2525', '', '<div class=\"container\" xss=\"removed\">\r\n<div class=\"inner_box\" xss=\"removed\">\r\n<div class=\"cms\" xss=\"removed\">\r\n<div class=\"row\" xss=\"removed\">\r\n<div class=\"col-lg-9 no_pad pr-lg-4 order-2 order-lg-1\" xss=\"removed\">\r\n<div class=\"mt-3\" xss=\"removed\">\r\n<p xss=\"removed\">Interpon 2525 Powder coating is part of the GSB-insured range of durable powder coatings that are sold for their exemplary weathering resistance. At Pilbara Powder Coatings, our range of Interpon D2525 powder coatings deliver amazing gloss retention and can withstand colour change followed by optimum film integrity.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">We present you with an exclusive range of Interpon D2525 powder coatings in eye-catching metallic and matt effects finishes. The powder coatings that you get from us can offer an amazing alternative to anodized aluminium. Our Interpon D2525 powder coating is made to cater to the demands of the weathering needs of the top industry specifications.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Super-Durable Interpon D2525 Powder Coating</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">One of the many advantages of using our Interpon D2525 powder coating is that they enable easy and quick repair of coating damage during both the installation and fabrication process. They also have a long-term resistance and come with a guarantee. Being an eco-friendly product, they have minimum to zero environmental impact as well.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">Our TGIC-free powder coating series is specifically designed for architectural applications where both film integrity and colour retention are prioritized. We use advanced polyester technology along with high-performing and effective pigments as the formulation for our D2525 to cater to industrial standards.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\"><strong>Minimal Impact on the Environment</strong></p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">Some of the architectural aluminium applications that our Interpon D2525 is developed for are louvres, door systems, window systems, balustrades, and all other types of exterior metal features present on the commercial buildings. For our Interpon D2525 powder coatings, you can get a stack full of custom colours like satin, matt, flat matt, gloss, etc.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The Interpon D2525 has been the architect’s favorited for a long time given how they use it for a vast number of valuable and important projects. The powder coating also caters to the various weathering needs of the top industry specifications like the BS EN 12206, AAMA 2604, GSB Master, Qualicoat Class 2, etc. Contact us to know more further about our Interpon D2525 powder coating.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 4, '0', 6, '2021-09-20 11:37:44', '0000-00-00 00:00:00', '1', 'INTERPON D2525'),
(23, 'DURALLOY', 'duralloy', '', '<div class=\"container\" xss=\"removed\">\r\n<div class=\"inner_box\" xss=\"removed\">\r\n<div class=\"cms\" xss=\"removed\">\r\n<div class=\"row\" xss=\"removed\">\r\n<div class=\"col-lg-9 no_pad pr-lg-4 order-2 order-lg-1\" xss=\"removed\">\r\n<div class=\"mt-3\" xss=\"removed\">\r\n<div class=\"mt-3\" xss=\"removed\">\r\n<section class=\"grid-100 tablet-grid-100\" xss=\"removed\">\r\n<div class=\"product-content\" xss=\"removed\">\r\n<div xss=\"removed\">\r\n<div xss=\"removed\">\r\n<div xss=\"removed\">\r\n<div xss=\"removed\">\r\n<p xss=\"removed\">We take pride in being the most popular Duralloy powder coating in Australia at Pilbara Powder Coating. Our range of Duralloy is a premium series of rich and textured colour powder coatings that come with a warranty. The durable range of powder coats from Duralloy that we offer is perfect for architectural grade aluminium, on all kinds of steel like mild steel, semi-bright steel followed by black and blue steel. You can use them in mild exterior conditions as well or in general interior conditions.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The durable range of Duralloy powder coating we offer runs inclusive of all types of warranty; starting from corrosion to steel colour. You can enjoy the guaranteed performance with our range of Duralloy powder coats on pre-treated aluminium. They have brilliant colour retention and features a hard-wearing finish.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<h2 xss=\"removed\"><strong>Performance Assured Duralloy Powder Coat</strong></h2>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">The range of Duralloy powder coats we offer is primarily designed for extruded aluminium like on both door and window joinery and residential building&#39;s aluminium panel work. And though you can also our powder coat from Duralloy on stainless steel they aren&#39;t warranted. You can come across an impressive range of solid colours in our Duralloy powder coat.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">We offer Duralloy powder coats that are capable of incorporating advanced powder-coat technology combined with the newest colours. The colour range is selected from the classics and the very modern international colour trends are arranged in a way to make your colour selection convenient and easy.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\"><strong>Get 10 Year Colour Warranty with Duralloy Powder Coat</strong></p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">Our offered Duralloy powder coat is one of the most popular given the availability of its impressive colour range and its tested hard-wearing finish. The demand for their usage is also improved by the premium and highest-grade powder availability with modern weathering powder. Of all other finishes, the matt finishes are most preferred, thanks to their scratching and marking resistance.</p>\r\n\r\n<p xss=\"removed\"> </p>\r\n\r\n<p xss=\"removed\">You can expect our Duralloy powder coat to run suitable for saltwater of more than 100 meters in environments that are mild to tropical. The Duralloy powder coat also comes with a warranty on its colour integrity and film integrity. Contact us to choose your colour or understand how to maintain the powder coat.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 4, '0', 7, '2021-09-20 11:37:54', '0000-00-00 00:00:00', '1', 'DURALLOY'),
(25, 'Insurance', 'insurance', 'resicoat.jpg', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 9, '2021-09-29 14:58:15', '0000-00-00 00:00:00', '1', 'Insurance'),
(26, 'Telecom', 'telecom', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 10, '2021-10-13 05:46:42', '0000-00-00 00:00:00', '1', 'Telecom'),
(28, 'Marketing', 'marketing', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n\r\n', 3, '0', 12, '2022-02-15 11:52:55', '0000-00-00 00:00:00', '1', 'Marketing'),
(29, 'test reports 1', 'test-reports-1', '', '<p>test reports 1</p>\r\n', 2, '0', 13, '2022-02-15 11:53:40', '0000-00-00 00:00:00', '1', 'test reports 1'),
(30, 'test reports 2', 'test-reports-2', '', '<p>test reports 2</p>\r\n', 2, '0', 14, '2022-02-15 11:53:54', '0000-00-00 00:00:00', '1', 'test reports 2'),
(31, 'Healthcare', 'healthcare', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 15, '2022-02-19 08:27:28', '0000-00-00 00:00:00', '1', 'Healthcare'),
(32, 'Travel and hospitality', 'travel-and-hospitality', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 16, '2022-02-19 08:27:55', '0000-00-00 00:00:00', '1', 'Travel and hospitality');
INSERT INTO `wl_dynamic_pages` (`page_id`, `page_name`, `friendly_url`, `page_image`, `page_description`, `parent_id`, `is_fixed`, `sort_order`, `date_added`, `date_modified`, `status`, `page_alt`) VALUES
(33, 'Education', 'education', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 17, '2022-02-19 08:28:25', '0000-00-00 00:00:00', '1', 'Education'),
(34, 'Real Estate', 'real-estate', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, '0', 18, '2022-02-19 08:29:40', '0000-00-00 00:00:00', '1', 'Real Estate'),
(35, 'Finance', 'finance', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 3, '0', 19, '2022-02-19 08:32:24', '0000-00-00 00:00:00', '1', 'Finance'),
(36, 'IT', 'it', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 3, '0', 20, '2022-02-19 08:32:53', '0000-00-00 00:00:00', '1', 'IT'),
(37, 'HR Support', 'hr-support', '', '<p><img alt=\"\" class=\"cms_pc\" src=\"assets/designer/themes/default/images/ab.jpg\" xss=removed>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.<br>\r\n<br>\r\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.<br>\r\n<br>\r\nInteger ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>\r\n\r\n<p> </p>\r\n\r\n<ul>\r\n <li>Full-fledged engineering and construction solutions</li>\r\n <li>State-of-the-art, well-equipped fabrication yard</li>\r\n <li>Highly-skilled and experienced technical crew</li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>Architectural and structural steel installation services</li>\r\n <li>Project mobilization, planning & engineering services</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 3, '0', 21, '2022-02-19 08:33:19', '0000-00-00 00:00:00', '1', 'HR Support');

-- --------------------------------------------------------

--
-- Table structure for table `wl_enquiry`
--

CREATE TABLE `wl_enquiry` (
  `id` int(11) NOT NULL,
  `type` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=contact Us,2=service,3=products,4=Career',
  `product_service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_type` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(220) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requirement` text COLLATE utf8_unicode_ci,
  `amount` decimal(10,2) DEFAULT NULL,
  `apply_for` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `cv_file` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document1` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `reply_status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `receive_date` datetime NOT NULL,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `relevant_experience` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_enquiry`
--

INSERT INTO `wl_enquiry` (`id`, `type`, `product_service`, `service_type`, `email`, `first_name`, `last_name`, `company_name`, `country`, `state`, `city`, `location`, `phone_number`, `ext_number`, `mobile_number`, `fax_number`, `address`, `zipcode`, `requirement`, `amount`, `apply_for`, `message`, `cv_file`, `document1`, `document2`, `status`, `reply_status`, `receive_date`, `customers_id`, `product_id`, `relevant_experience`, `resume`) VALUES
(41, '2', NULL, 'career', 'test@gmail.com', 'fff', '', NULL, NULL, NULL, NULL, NULL, '12345678777', NULL, '', NULL, NULL, NULL, NULL, NULL, 'testing', 'cvcb', 'resume-aa1.doc', NULL, NULL, '1', 'N', '2020-12-01 14:56:09', 0, 0, NULL, NULL),
(42, '3', 'test product2', NULL, 'a@gmail.com', 'deepak', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'hhhh', NULL, NULL, NULL, '1', 'N', '2020-12-02 14:53:14', 0, 0, NULL, NULL),
(43, '3', 'BREATHING CIRCUIT BASIC', NULL, 'arunano.mca@gmail.com', 'deepak', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'xvd', NULL, NULL, NULL, '1', 'N', '2021-01-15 14:18:01', 0, 0, NULL, NULL),
(45, '3', 'test by aa', NULL, 'weblink.manish84@gmail.com', 'manish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3423423', NULL, NULL, NULL, NULL, NULL, NULL, 'dsfdsafadsf', NULL, NULL, NULL, '1', 'N', '2021-01-23 11:00:54', 0, 0, NULL, NULL),
(46, '2', NULL, 'career', 'test@gmail.com', 'dsfafdasfd', 'dsfdsfds', NULL, NULL, NULL, NULL, NULL, '43242342342', NULL, '', NULL, NULL, NULL, NULL, NULL, '', 'sdfsd afdsfas', 'test.docx', NULL, NULL, '1', 'N', '2021-01-23 11:33:22', 0, 0, NULL, NULL),
(47, '2', 'asian city for best', NULL, 'test@gmail.com', 'Manish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3423432432', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsfdsdas', NULL, NULL, NULL, '1', 'N', '2021-01-30 08:23:11', 0, 0, NULL, NULL),
(48, '2', 'asian city for best', NULL, 'weblink.manish84@gmail.com', 'sdfdsfsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4543554353', NULL, NULL, NULL, NULL, NULL, NULL, 'dsfsafdas', NULL, NULL, NULL, '1', 'N', '2021-01-30 08:24:49', 0, 0, NULL, NULL),
(49, '2', 'asian city for best', NULL, 'weblink.manish84@gmail.com', 'sdfdsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3432423', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfdsfdas', NULL, NULL, NULL, '1', 'N', '2021-01-30 08:25:30', 0, 0, NULL, NULL),
(53, '3', 'test by aa', NULL, 'admin@gmail.com', 'new enq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1235567789', NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, NULL, NULL, '1', 'N', '2021-06-14 07:17:16', 0, 0, NULL, NULL),
(55, '2', '', NULL, 'test@gmail.com', 'sdfasdfs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23213231', NULL, NULL, NULL, NULL, NULL, NULL, 'sdafsfs', NULL, NULL, NULL, '1', 'N', '2021-09-10 10:47:02', 0, 0, NULL, NULL),
(56, '2', 'testing services for humnas', NULL, 'admin@gmail.com', 'dsfafsdfdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '343242342', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfasfdsfdsa', NULL, NULL, NULL, '1', 'N', '2021-09-10 10:49:39', 0, 0, NULL, NULL),
(57, '2', 'testing services for humnas', NULL, 'admin@gmail.com', 'dgfdgsfdgsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '324242', NULL, NULL, NULL, NULL, NULL, NULL, 'dgsgfdgfsd', NULL, NULL, NULL, '1', 'N', '2021-09-10 10:52:28', 0, 0, NULL, NULL),
(59, '2', 'Abcite  545 / X45', NULL, 'dimpy@gmail.com', 'Dimpy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '09876543210', NULL, NULL, NULL, NULL, NULL, NULL, 'Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra,', NULL, NULL, NULL, '1', 'Y', '2021-09-20 13:15:16', 0, 0, NULL, NULL),
(75, '2', 'Enterprise Software Development service1', NULL, 'info@bagboxmt.com', 'subash chandra pandey', NULL, NULL, NULL, 'UTTAR PRADESH', 'GHAZIABAD', 'indirapuram', NULL, NULL, '+919711019262', NULL, 'plot no 41 gf4  shakthikhand 3  indirapuram', '201014', NULL, NULL, NULL, '', NULL, '', '', '1', 'N', '2021-12-08 11:23:20', 0, 0, NULL, NULL),
(76, '2', 'Enterprise Software Development service1', NULL, 'info@bagboxmt.com', 'subash chandra pandey', NULL, NULL, NULL, 'UTTAR PRADESH', 'GHAZIABAD', 'Delhi', NULL, NULL, '+919711019262', NULL, 'plot no 41 gf4  shakthikhand 3  indirapuram', '201014', NULL, NULL, NULL, '', NULL, '', '', '1', 'N', '2021-12-08 11:26:56', 0, 0, NULL, NULL),
(77, '2', 'Enterprise Software Development service1', NULL, 'info@bagboxmt.com', 'subash chandra pandey', NULL, NULL, NULL, 'UTTAR PRADESH', 'GHAZIABAD', 'indirapuram', NULL, NULL, '+919711019262', NULL, 'plot no 41 gf4  shakthikhand 3  indirapuram', '201014', NULL, NULL, NULL, '', NULL, 'travel_ticketing_sc5.pdf', '', '1', 'N', '2021-12-08 11:42:06', 0, 0, NULL, NULL),
(80, '4', NULL, 'Web Development', 'info@bagboxmt.com', 'subash chandra pandey', '', '', '', NULL, NULL, NULL, '', NULL, '+919711019262', NULL, NULL, NULL, NULL, NULL, NULL, 'i want it suport', NULL, NULL, NULL, '1', 'N', '2021-12-09 07:11:26', 0, 0, NULL, NULL),
(81, '2', 'Total Project Summary', NULL, 'subashvns@gmail.com', 'akshat pandey', NULL, NULL, NULL, '', '', '', NULL, NULL, '8377787823', NULL, '', '', NULL, NULL, NULL, 'this is project enquiry', NULL, '', '', '1', 'N', '2022-01-24 12:58:37', 0, 0, NULL, NULL),
(83, '4', '', NULL, 'subashvns@gmail.com', 'subash job', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9711019262', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '1', 'N', '2022-02-02 06:33:30', 0, 0, NULL, ''),
(84, '4', NULL, NULL, 'subashvns@gmail.com', 'subash job', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9711019262', NULL, NULL, NULL, NULL, NULL, 'PHP Devloper', '', NULL, NULL, NULL, '1', 'N', '2022-02-02 06:56:53', 0, 3, NULL, ''),
(85, '4', NULL, NULL, 'subashvns@gmail.com', 'subash job', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9711019262', NULL, NULL, NULL, NULL, NULL, 'Office Assistant2', '', NULL, NULL, NULL, '1', 'Y', '2022-02-02 06:59:59', 0, 2, NULL, 'resumemuath.pdf'),
(86, '3', 'test product2', NULL, 'subashvns@gmail.com', 'subash chandra pandey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+919711019262', NULL, 'plot no 41 gf4  shakthikhand 3  indirapuram', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '1', 'N', '2022-02-03 06:16:54', 0, 0, NULL, NULL),
(87, '1', NULL, NULL, 'p.matson@globalaitech.com', 'Patricia', 'Matson', '', '', NULL, NULL, NULL, '', NULL, '4153455772', NULL, NULL, NULL, NULL, NULL, NULL, 'Hi,\r\n\r\nI wanted to tell you about our new AI which is shaping the future of artificial intelligence writing. With a few words from you our AI can write video scripts, blog articles, website content, marketing material, effective social media ads and sales emails in over 26 languages - and that\'s just naming a few things!\r\n\r\nGive it free try on this link: https://bit.ly/artificialintelligencewriter\r\n\r\nI\'m confident it\'s going to save you a lot of time and help you with your content creation.\r\n\r\nBest regards,\r\nPatricia Matson\r\nGlobal AI Technologies', NULL, NULL, NULL, '1', 'N', '2022-11-24 15:46:07', 0, 0, NULL, NULL),
(88, '1', NULL, NULL, 'haytham@rocketmail.com', 'Haytham soliman', '', '', '', NULL, NULL, NULL, '', NULL, '33911265', NULL, NULL, NULL, NULL, NULL, NULL, 'Test', NULL, NULL, NULL, '1', 'N', '2023-01-20 02:55:48', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_enquiry_details`
--

CREATE TABLE `wl_enquiry_details` (
  `id` int(11) NOT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `receive_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wl_events`
--

CREATE TABLE `wl_events` (
  `events_id` int(11) NOT NULL,
  `events_title` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `events_image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_description` text COLLATE utf8_unicode_ci,
  `events_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `publisher` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `recv_date` datetime NOT NULL,
  `featured` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `latest_news` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `events_image2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `events_image3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `venue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `posted_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_events`
--

INSERT INTO `wl_events` (`events_id`, `events_title`, `friendly_url`, `events_image`, `sort_description`, `events_description`, `publisher`, `sort_order`, `status`, `recv_date`, `featured`, `latest_news`, `events_image2`, `events_image3`, `start_date`, `venue`, `posted_by`) VALUES
(1, 'News Details4', 'news-details4', 'news-detail2.jpg', NULL, '<p>News Details3News Details3News Details3News Details3News Details3News Details3News Details3</p>', NULL, NULL, '1', '2022-02-02 08:38:45', 'N', '1', NULL, NULL, '2022-02-02', NULL, ''),
(2, 'News Details1', 'news-details1', 'news-detail3.jpg', NULL, 'Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.\r\nLorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.', NULL, NULL, '1', '2022-02-02 08:40:01', 'N', '1', NULL, NULL, '2022-02-08', NULL, ''),
(3, 'News Details2', 'news-details2', 'news-detail4.jpg', NULL, '<p>Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.</p>', NULL, NULL, '1', '2022-02-02 08:40:32', 'N', '1', NULL, NULL, '2022-02-17', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `wl_faq`
--

CREATE TABLE `wl_faq` (
  `faq_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `faq_parent_id` int(11) NOT NULL DEFAULT '0',
  `faq_question` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `faq_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `faq_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_faq`
--

INSERT INTO `wl_faq` (`faq_id`, `category_id`, `faq_parent_id`, `faq_question`, `faq_answer`, `sort_order`, `status`, `faq_date_added`) VALUES
(6, 0, 0, 'How thick and hard are thermoplastic powder coatings?', 'Thermoplastic powder coating thickness can achieve thicknesses greater than 200 microns via fluidised bed application, and up to 140 microns from spray application. Plastic coatings are not generally very hard, although harder coatings can be achieved with nylon plastics.', 4, '1', '2019-05-06 15:09:12'),
(7, 0, 0, 'What colour options are available?', 'Theoretically, products can be plastic coated in almost any colour. PPC stocks green, blue, black, white, red and yellow plastics, however other colours can be ordered in large quantities if you have a large order/batch. See Plascoat PPA 571 Brochure for more colour options.', 2, '1', '2019-05-06 15:09:39'),
(8, 0, 0, 'Is thermoplastic powder coating wear resistant?', 'Thermoplastic coatings generally show very good wear resistance, although this is subject to the type of plastic used. In the event of wear, melting and resetting the plastic can repair the coating. The thermoplastic coating melts at around 150 – 200 degrees Celsius and scars on the surface can easily be healed with a hot air gun.', 3, '1', '2019-05-06 15:10:21'),
(9, 0, 0, 'What lengths of material can I powder coat?', 'PPC can powder coat almost any size up to 7meters long and up to 2.4m wide.', 1, '1', '2019-05-06 15:11:09'),
(11, 0, 0, 'Is thermoplastic powder coating corrosion and chemical resistant?', 'Thermoplastic coatings generally have excellent corrosion resistance subject to the type of plastic used. They usually stand up well to chemicals and solvents.', 5, '1', '2021-06-15 07:59:51'),
(12, 0, 0, 'Which plastic coating is best for my product?', 'As plastic coatings can be used for a range of applications, it is best to talk to one of our technical experts about which coating would be best suited to your needs. You can contact us by filling out the Enquiry Form below, or by emailing enquiry@pilbarapowdercoating.com.au.', 6, '1', '2021-09-20 12:26:13'),
(13, 0, 0, 'What is the difference between thermosetting and thermoplastic powder coating?', 'The difference between thermoplastic and thermosetting powder coatings, thermosetting powder coatings will chemically react during baking to form a polymer network that is much more resistant to a coating breakdown. Also thermosetting coatings do not re-melt when they are cooled after heat is re-applied.', 7, '1', '2021-09-20 12:26:25'),
(14, 0, 0, 'How Do I Place An Order?', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 8, '1', '2022-01-24 05:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `wl_gallery`
--

CREATE TABLE `wl_gallery` (
  `gallery_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `gallery_title` varchar(255) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `friendly_url` varchar(255) DEFAULT NULL,
  `gallery_image` varchar(255) DEFAULT NULL,
  `type` enum('1','2') DEFAULT '1' COMMENT '1=Image, 2=Video',
  `status` enum('1','0') DEFAULT '1' COMMENT '1=Actice,0=Inactive',
  `is_exclusive` int(11) DEFAULT '0',
  `is_latest` int(11) DEFAULT '0',
  `post_date` datetime DEFAULT NULL,
  `gallery_alt` varchar(255) DEFAULT NULL,
  `video_thumbnail` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_gallery`
--

INSERT INTO `wl_gallery` (`gallery_id`, `parent_id`, `gallery_title`, `description`, `sort_order`, `friendly_url`, `gallery_image`, `type`, `status`, `is_exclusive`, `is_latest`, `post_date`, `gallery_alt`, `video_thumbnail`, `video_url`) VALUES
(60, 57, 'test11111', '<p>hghfghghhgh</p>\r\n', NULL, '', 'avatar1674561520_1.jpg', '1', '1', 0, 0, '2023-01-24 11:58:48', 'test11111', NULL, NULL),
(61, 0, 'Abhishek mehta', '<p>affsdfs</p>\r\n', NULL, 'abhishek', 'dsfdsf2.jpg', '1', '1', 0, 0, '2023-01-24 12:00:48', '', NULL, NULL),
(57, 0, 'Events & Exhibitions Management', '', NULL, 'events-exhibitions-management', NULL, '1', '1', 0, 0, '2023-01-16 07:49:36', 'Events & Exhibitions Management', NULL, NULL),
(58, 0, ' Branding & Design', '', NULL, 'branding-design', NULL, '1', '1', 0, 0, '2023-01-16 07:50:58', ' Branding & Design', NULL, NULL),
(62, 61, 'Abhishek 1', '<p><span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">Sinon Tech is a mobile app development company that has helped</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">businesses establish their presence in the mobile sphere.</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">Our expert developers offer the best mobile and</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">web app development services, drawing on a decade of industry</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">experience. We deliver award-winning mobile</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">experiences across many industries after a strategic mobile application development process</span></p>\r\n', NULL, '', 'avatar1674561812_1.png', '1', '1', 0, 0, '2023-01-24 12:03:49', 'Abhishek 1', NULL, NULL),
(63, 58, 'Test123', '<p><span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">Sinon Tech is a mobile app development company that has helped</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">businesses establish their presence in the mobile sphere.</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">Our expert developers offer the best mobile and</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">web app development services, drawing on a decade of industry</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">experience. We deliver award-winning mobile</span><br style=\"box-sizing: border-box; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\" />\r\n<span style=\"color: rgb(255, 255, 255); font-family: Poppins, sans-serif; font-size: 16px; letter-spacing: 0.5px; text-align: right; background-color: rgb(201, 35, 49);\">experiences across many industries after a strategic mobile application development process</span></p>\r\n', NULL, '', 'avatar1674561863_1.jpg', '1', '1', 0, 0, '2023-01-24 12:04:36', 'Test123', NULL, NULL),
(64, 61, 'mobile app development', '<p><span style=\"color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px; letter-spacing: 0.5px;\">Sinon Tech, the mobile app development company, understands that every business is different and requires a unique solution. Our highly skilled developers, programmers, and designers work together to create web and mobile app development solutions tailored for each business. Our team has examined different platforms that have similar apps and recommended the best platform, features, launch time, and other business propositions.</span></p>\r\n', NULL, '', 'avatar1674563819_1.jpg', '1', '1', 0, 0, '2023-01-24 12:32:46', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_gallery_normal_display`
--

CREATE TABLE `wl_gallery_normal_display` (
  `id` int(11) NOT NULL,
  `album_id` int(11) DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `gallery_title` varchar(255) DEFAULT NULL,
  `friendly_url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `gallery_file` varchar(255) DEFAULT NULL,
  `gallery_image` varchar(255) DEFAULT NULL,
  `thumb_name` varchar(255) DEFAULT NULL,
  `embed_code` text,
  `type` enum('1','2') DEFAULT '1' COMMENT '1=Image, 2=Video',
  `gallery_desc` text,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('1','0') DEFAULT '1' COMMENT '1=Actice,0=Inactive',
  `is_exclusive` int(11) DEFAULT '0',
  `is_latest` int(11) DEFAULT '0',
  `post_date` datetime DEFAULT NULL,
  `language_id` int(11) DEFAULT '1' COMMENT '1=English, 2=Portguese',
  `gallery_alt` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_gallery_normal_display`
--

INSERT INTO `wl_gallery_normal_display` (`id`, `album_id`, `parent_id`, `gallery_title`, `friendly_url`, `title`, `gallery_file`, `gallery_image`, `thumb_name`, `embed_code`, `type`, `gallery_desc`, `country_id`, `status`, `is_exclusive`, `is_latest`, `post_date`, `language_id`, `gallery_alt`) VALUES
(1, 0, 0, NULL, NULL, 'image gallery 1', 'gal-6.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-01-24 07:50:39', 1, NULL),
(2, 0, 0, NULL, NULL, 'Video name 1', 'gall-img1.jpg', NULL, NULL, '&lt;iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/dx1MPawGE-E\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;', '2', NULL, 0, '1', 0, 0, '2022-02-02 09:43:29', 1, NULL),
(3, 0, 0, NULL, NULL, 'images gallery 2', 'gal-big11.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:55:33', 1, NULL),
(4, 0, 0, NULL, NULL, 'images gallery 3', 'gal-5.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:55:44', 1, NULL),
(5, 0, 0, NULL, NULL, 'images gallery 3', 'gal-4.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:55:57', 1, NULL),
(6, 0, 0, NULL, NULL, 'images gallery 4', 'gal-3.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:56:07', 1, NULL),
(7, 0, 0, NULL, NULL, 'images gallery 2', 'gal-2.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:56:52', 1, NULL),
(8, 0, 0, NULL, NULL, 'images gallery 2', 'gal-1.jpg', NULL, NULL, NULL, '1', NULL, 0, '1', 0, 0, '2022-02-02 09:57:10', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_header_images`
--

CREATE TABLE `wl_header_images` (
  `id` int(11) NOT NULL,
  `header_image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `header_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line_one` text COLLATE utf8_unicode_ci,
  `text_colour` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#FFFFFF',
  `line_two` text COLLATE utf8_unicode_ci,
  `line_three` text COLLATE utf8_unicode_ci,
  `line_four` text COLLATE utf8_unicode_ci,
  `line_five` text COLLATE utf8_unicode_ci,
  `line_six` text COLLATE utf8_unicode_ci,
  `status` enum('1','0','2') COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_header_images`
--

INSERT INTO `wl_header_images` (`id`, `header_image`, `header_url`, `line_one`, `text_colour`, `line_two`, `line_three`, `line_four`, `line_five`, `line_six`, `status`, `added_date`) VALUES
(16, 'photo7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2020-12-01 11:16:49'),
(17, 'photo4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2021-09-20 10:48:56'),
(21, 'photo3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2022-02-19 07:55:00'),
(54, 'photo7jpg1.jpg', '', NULL, '#FFFFFF', NULL, NULL, NULL, NULL, NULL, '1', '2022-11-27 04:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `wl_jobs`
--

CREATE TABLE `wl_jobs` (
  `jobs_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT 'parent_id= last level of category ',
  `category_links` varchar(80) CHARACTER SET utf8 NOT NULL,
  `job_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `friendly_url` varchar(110) CHARACTER SET utf8 DEFAULT NULL,
  `job_code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `job_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salary_range` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key_skills` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `location_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobs_description` longtext CHARACTER SET utf8 NOT NULL,
  `is_featured` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_hot` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `is_trending` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_sales` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('0','1','2') CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `job_added_date` datetime NOT NULL,
  `job_updated_date` datetime DEFAULT NULL,
  `jobs_viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `job_alt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `xls_type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_jobs`
--

INSERT INTO `wl_jobs` (`jobs_id`, `category_id`, `category_links`, `job_title`, `friendly_url`, `job_code`, `job_type`, `salary_range`, `experience`, `key_skills`, `contact_person`, `contact_number`, `contact_email`, `country_id`, `state_id`, `city_id`, `location_id`, `jobs_description`, `is_featured`, `is_new`, `is_hot`, `is_trending`, `is_sales`, `status`, `job_added_date`, `job_updated_date`, `jobs_viewed`, `job_alt`, `xls_type`, `location`) VALUES
(1, 0, '', 'Office Assistant', 'office-assistant', '', '', '25,000 - 35,000/-', '', '', '', '', '', 0, 0, 0, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', '0', '0', '0', '0', '0', '1', '2021-11-24 14:05:49', NULL, 0, '', '0', 'Mumbai'),
(2, 0, '', 'Office Assistant2', 'office-assistant2', '', '', '55,000 - 35,000/-', '', '', '', '', '', 0, 0, 0, NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; font-size: 14px; text-align: justify;\">content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their def</span></p>\r\n', '0', '0', '0', '0', '0', '1', '2021-11-29 15:23:24', NULL, 0, '', '0', 'Kirti Nager'),
(3, 0, '', 'PHP Devloper', 'php-devloper', '', '', '800000', '', '', '', '', '', 0, 0, 0, NULL, '<p><span font-size:=\"\" open=\"\" xss=removed text-align:=\"\">content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their defcontent of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their def</span></p>\r\n', '0', '0', '0', '0', '0', '1', '2022-02-02 05:53:39', NULL, 0, '', '0', 'cannought place');

-- --------------------------------------------------------

--
-- Table structure for table `wl_jobs_media`
--

CREATE TABLE `wl_jobs_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobs_id` int(10) UNSIGNED NOT NULL,
  `media_type` enum('photo','video','pdf') CHARACTER SET utf8 NOT NULL DEFAULT 'photo',
  `media` varchar(255) CHARACTER SET utf8 NOT NULL,
  `job_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `sort_order` int(11) DEFAULT NULL,
  `media_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wl_meta_tags`
--

CREATE TABLE `wl_meta_tags` (
  `meta_id` int(11) NOT NULL,
  `is_fixed` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `entity_type` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'name of controllers  ',
  `entity_id` int(11) NOT NULL DEFAULT '0',
  `page_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(460) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_meta_tags`
--

INSERT INTO `wl_meta_tags` (`meta_id`, `is_fixed`, `entity_type`, `entity_id`, `page_url`, `meta_title`, `meta_description`, `meta_keyword`, `status`) VALUES
(1, 'Y', 'home/index', 0, 'home', 'Welcome to JBA Agency', 'At Pilbara Powder Coating Australia, we are a renowned supplier of Interpon Redox Triplex powder coating. Our three-layer powder coating is a blend of primer Interpon Redox PZ rich in zinc and barrier-safeguard primer Interpon Redox Plus', 'JBA Agency', '1'),
(2, 'Y', 'pages/index/mission', 0, 'mission', 'mission', 'mission', 'mission', '1'),
(3, 'Y', 'pages/index/production-quality', 0, 'production-quality', 'production-quality', 'production-quality', 'production-quality', '1'),
(4, 'Y', 'pages/index/events-exhibitions', 0, 'events-exhibitions', 'legal-disclaimer', 'legal-disclaimer', 'legal-disclaimer', '1'),
(5, 'Y', 'pages/index/research-development', 0, 'research-development', 'research-development', 'research-development', 'research-development', '1'),
(6, 'N', 'pages/index/quality', 0, 'quality', 'quality', 'quality', 'quality', '1'),
(7, 'N', 'pages/index/privacy-policy', 0, 'privacy-policy', 'privacy policy', 'privacy policy', 'privacy policy', '1'),
(8, 'Y', 'products/index/featured', 0, 'featured-products', 'featured-products', 'featured-products', 'featured-products', '1'),
(9, 'Y', 'pages/index/our-team', 0, 'our-team', 'our-team', 'our-team', 'our-team', '1'),
(10, 'Y', 'pages/order_form', 0, 'order-form', 'order-form', 'order-form', 'order-form', '1'),
(11, 'Y', 'pages/credit_application_form', 0, 'credit-application-form', 'credit-application-form', 'credit-application-form', 'credit-application-form', '1'),
(12, 'Y', 'pages/case_studies', 0, 'case-studies', 'case-studies', 'case-studies', 'case-studies', '1'),
(15, 'Y', 'pages/index/about-company', 0, 'about-company', 'about-company', 'company', 'company', '1'),
(16, 'Y', 'pages/contactus', 0, 'contactus', 'Contact Us - Pilbara Powder Coatings', 'contactus', 'contactus', '1'),
(17, 'Y', 'pages/sitemap', 0, 'sitemap', 'sitemap', 'sitemap', 'sitemap', '1'),
(18, 'N', 'dynamic_pages/detail', 1, 'industries', 'sub page', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'quis, sapien, velit, auctor, pulvinar, tincidunt, dignissim, Nunc, elit, amet, dolor, Duis, dapibus, Nullam, Vivamus, placerat, nibh, gravida, commodo, ligula', '1'),
(19, 'N', 'dynamic_pages/detail', 4, 'projects', 'sub page', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'quis, sapien, velit, auctor, pulvinar, tincidunt, dignissim, Nunc, elit, amet, dolor, Duis, dapibus, Nullam, Vivamus, placerat, nibh, gravida, commodo, ligula', '1'),
(20, 'N', 'dynamic_pages/detail', 2, 'test-reports', 'Thermoset Powder Coating System - Pilbara Powder Coatings', 'We are an Australian-based company engaged as the major provider of thermoset powder coating. Our thermoset powder coatings are making thin protective layers on the surfaces of metals.', 'Thermoset Powder Coating System', '1'),
(21, 'N', 'dynamic_pages/detail', 5, 'urban-coatings', 'Urban Powder Coatings in Australia - Pilbara Powder Coatings', 'We offers Urban Powder Coatings in Australia. Our urban powder coatings are specially designed to offer long-lasting corrosion protection for all types of metals, irrespective of the environment.', 'Urban Powder Coatings, Urban Powder Coatings in Australia', '1'),
(22, 'N', 'dynamic_pages/detail', 6, 'automotive-coatings', 'Automotive Coatings in Australia - Pilbara Powder Coatings', 'Pilbara Powder Coatings is the pioneer supplier of automotive coatings in Australia. All our automotive coatings are thermal cycling-resistant and thermal-shock resistant as well', 'Automotive Coatings in Australia', '1'),
(23, 'N', 'dynamic_pages/detail', 3, 'functions', 'sub page', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'quis, sapien, velit, auctor, pulvinar, tincidunt, dignissim, Nunc, elit, amet, dolor, Duis, dapibus, Nullam, Vivamus, placerat, nibh, gravida, commodo, ligula', '1'),
(105, 'Y', 'pages/index/terms-conditions', 0, 'terms-conditions', 'terms-conditions', 'terms-conditions', 'terms-conditions', '1'),
(237, 'N', 'testimonials/details', 7, 'riya-7', 'riya', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea', 'dolor, dolore, cillum, fugiat, nulla, esse, pariatur, velit, aute, irure, reprehenderit, voluptate, Excepteur, occaecat, deserunt, mollit, anim, laborum, officia, culpa', '1'),
(239, 'N', 'category/index', 28, 'wires', 'Festivals', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea', 'quis, nostrud, veniam, minim, enim, exercitation, ullamco, commodo, consequat, aliquip, nisi, laboris, aliqua, magna, consectetur, adipiscing, amet, dolor, ipsum, elit', '1'),
(247, 'N', 'testimonials/details', 9, 'test-name-9', 'test name', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', 'dolore, dolor, Duis, aute, consequat, aliquip, laboris, nisi, irure, commodo, reprehenderit, fugiat, nulla, pariatu, cillum, esse, voluptate, velit, ullamco, nostrud', '1'),
(264, 'N', 'testimonials/details', 10, 'deepak-10', 'Deepak', 'rem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', 'dolor, aliquip, commodo, nisi, ullamco, exercitation, consequat, laboris, aute, velit, esse, voluptate, reprehenderit, nostrud, irure, Duis, veniam, eiusmod, tempor, elit', '1'),
(268, 'N', 'category/index', 37, 'medium-voltage', 'Test category1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor', 'dolore, labore, incididunt, magna, aliqua, minim, enim, tempor, eiusmod, dolor, ipsum, amet, consectetur, elit, adipiscing, Lorem', '1'),
(270, 'N', 'category/index', 38, 'fire-low-smoke', 'Test category2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor', 'dolore, labore, incididunt, magna, aliqua, minim, enim, tempor, eiusmod, dolor, ipsum, amet, consectetur, elit, adipiscing, Lorem', '1'),
(319, 'N', 'testimonials/details', 14, 'hhh-14', 'hhh', 'cvfvf em ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea', 'dolor, nisi, aliquip, laboris, commodo, ullamco, nostrud, exercitation, consequat, Duis, velit, esse, voluptate, reprehenderit, aute, irure, quis, veniam, elit, eiusmod', '1'),
(375, 'N', 'news/details', 32, 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'amet, neque, quis, Vivamus, auctor, sapien, consectetur, dignissim, elit, pulvinar, dolor, tincidunt, ornare, sollicitudin, mauris, Duis, faucibus, Morbi, gravida, dapibus', '1'),
(376, 'N', 'news/details', 33, 'news-title2', 'news title2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'amet, neque, quis, Vivamus, auctor, sapien, consectetur, dignissim, elit, pulvinar, dolor, tincidunt, ornare, sollicitudin, mauris, Duis, faucibus, Morbi, gravida, dapibus', '1'),
(377, 'N', 'news/details', 34, 'news-title3', 'news title3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'amet, neque, quis, Vivamus, auctor, sapien, consectetur, dignissim, elit, pulvinar, dolor, tincidunt, ornare, sollicitudin, mauris, Duis, faucibus, Morbi, gravida, dapibus', '1'),
(378, 'N', 'news/details', 35, 'lorem-ipsum-dolor-sit-amet-consectetur', 'Lorem ipsum dolor sit amet, consectetur', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'amet, neque, quis, Vivamus, auctor, sapien, consectetur, dignissim, elit, pulvinar, dolor, tincidunt, ornare, sollicitudin, mauris, Duis, faucibus, Morbi, gravida, dapibus', '1'),
(380, 'N', 'products/detail', 44, 'anesthesia-mask-with-air-cushion-reusable-disposable', 'ANESTHESIA MASK WITH AIR CUSHION REUSABLE-DISPOSABLE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'auctor, neque, Aenean, amet, quis, faucibus, Donec, turpis, sapien, magna, sollicitudin, eget, Vivamus, efficitur, aliquam, velit, purus, semper, dapibus, Nunc', '1'),
(381, 'N', 'products/detail', 45, 'breathing-circuit-basic', 'BREATHING CIRCUIT BASIC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'auctor, neque, Aenean, amet, quis, faucibus, Donec, turpis, sapien, magna, sollicitudin, eget, Vivamus, efficitur, aliquam, velit, purus, semper, dapibus, Nunc', '1'),
(383, 'N', 'products/detail', 47, 'test-product2', 'test product2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'auctor, neque, Aenean, amet, quis, faucibus, Donec, turpis, sapien, magna, sollicitudin, eget, Vivamus, efficitur, aliquam, velit, purus, semper, dapibus, Nunc', '1'),
(385, 'N', 'news/details', 36, 'test-news', 'test news', 'bb cb  f b d ff', '', '1'),
(386, 'N', 'news/details', 37, 'deepak-news', 'deepak news', 'v     cv v c c cv', '', '1'),
(387, 'N', 'category/index', 47, 'blind/sub-categorytest', 'sub categorytest', 'c cxxc xc', 'cxxc', '1'),
(388, 'N', 'category/index', 48, 'blind/sub-categorytest2', 'sub categorytest2', ' x csd', '', '1'),
(389, 'N', 'products/detail', 49, 'test-by-aa', 'test by aa', 'c vxvxv', 'vxvxv', '1'),
(390, 'N', 'products/detail', 50, 'test-product', 'gggg', 'cbvcf', 'cbvcf', '1'),
(391, 'N', 'dynamic_pages/detail', 10, 'main-page1', 'Main page1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra pulvinar neque quis vehicula. Nullam tincidunt eros neque, a efficitur velit commodo ut. Sed consequat purus nec mi vulputate ullamcorper. Mauris', 'Shopping, neque, quis, Aenean, Mauris, turpis, vehicula, offer, brands, favourite, simpler, easier, faster, risus, interdum, offers, light, Suspendisse, stylish, potenti', '1'),
(396, 'N', 'news/details', 38, 'test', 'test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting', 'text, Lorem, dummy, simply, Ipsum, industry, printing, typesetting', '1'),
(398, 'N', 'testimonials/details', 16, 'raj-16', 'raj', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea', 'quis, nostrud, veniam, minim, enim, exercitation, ullamco, commodo, consequa, aliquip, nisi, laboris, aliqua, magna, consectetur, adipisicing, amet, dolor, ipsum, elit', '1'),
(400, 'N', 'news/details', 39, 'this-is-new-news', 'this is new news', 'ds afsdf dsfdsaf s', 'dsfdsaf, afsdf', '1'),
(401, 'N', 'category/index', 49, 'blind/testing-sub-category', 'testing sub category', 'sdafsf dsfas', 'dsfas, sdafsf', '1'),
(402, 'N', 'products/detail', 51, 'kbc-news-acds', 'kbc news acds', 'dsafasdfds fsda dsaf d', 'dsaf, fsda, dsafasdfds', '1'),
(414, 'N', 'testimonials/details', 18, 'manish-18', 'Manish', 'sdfsfdsfdsdsa', 'sdfsfdsfdsdsa', '1'),
(417, 'N', 'category/index', 50, 'low-voltage-power', 'new category', 'content here', 'content', '1'),
(418, 'N', 'products/detail', 53, 'jet-fuel-oil-oilfield-equipments-and-supplies', 'Skiing', 'ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.” The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page,', 'ipsum, sentence, paragraph, text, block, looking, page, doesn\'t, laying, pages, controversy, practice, distract, layout, natural, lorem, elit, eiusmod, adipiscing, consectetur', '1'),
(419, 'N', 'dynamic_pages/detail', 18, 'banking', 'PPA 571', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'quis, sapien, velit, auctor, pulvinar, tincidunt, dignissim, Nunc, elit, amet, dolor, Duis, dapibus, Nullam, Vivamus, placerat, nibh, gravida, commodo, ligula', '1'),
(420, 'N', 'dynamic_pages/detail', 19, 'retail', 'PPA 572', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'quis, sapien, velit, auctor, pulvinar, tincidunt, dignissim, Nunc, elit, amet, dolor, Duis, dapibus, Nullam, Vivamus, placerat, nibh, gravida, commodo, ligula', '1'),
(425, 'N', 'dynamic_pages/detail', 21, 'ppa-571', 'name', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti', 'amet, consectetur, dolor, neque, elit, Vivamus, auctor, quis, sapien, mauris, magna, dignissim, tincidunt, sollicitudin, ipsum, pulvinar, Lorem, Duis, faucibus, adipiscing', '1'),
(426, 'N', 'dynamic_pages/detail', 22, 'interpon-d2525', 'Interpon D2525 Powder Coating - Pilbara Powder Coatings', 'We are providing Interpon D2525 Powder Coating in Australia. The powder coatings that you get from us can offer an amazing alternative to anodized aluminium.', 'Interpon D2525 Powder Coating', '1'),
(427, 'N', 'dynamic_pages/detail', 23, 'duralloy', 'Duralloy Powder Coat in Australia - Pilbara Powder Coatings', 'We offer Duralloy Powder Coat in Australia. You can enjoy the guaranteed performance with our range of Duralloy powder coats on pre-treated aluminium.', 'Duralloy Powder Coat in Australia', '1'),
(440, 'N', 'services/detail', 21, 'electro', 'Industrial Electro Powder Coating - Pilbara Powder Coatings', 'We are offering Industrial Electro Powder Coating in Australia. To find out more about our powder coating product range, contact us today on (08)9143 1837.', 'Industrial Electro Powder Coating', '1'),
(442, 'N', 'dynamic_pages/detail', 25, 'insurance', 'FBE Coating in Australia, Resicoat R4-ES, Fusion Bonded Epoxy Powder Coating', 'We are providing FBE Coating in Australia. The fusion-bonded epoxy powder coating that we provide at Pilbara Powder Coating is widely used to offer protection to steel pipes and pipelines and a vast array of piping bonds.', 'FBE Coating in Australia, Resicoat R4-ES, Fusion bonded epoxy powder coating', '1'),
(449, 'N', 'services/detail', 29, 'pr16', 'PR16', 'INTRODUCTION OXYPLAST PR16 is a satin-matt thermosetting powder coating based on saturated polyester resins specially selected for exterior use. It’s very good flow-out and excellent resistance to atmospheric ageing and', 'zinc, range, coating, phosphatation, Aluminium, outdoor, steel, alloys, applied, colour, powder, light, cast, appropriate, cold-rolled, metals, Ferrous, conversion, cleaning, Iron', '1'),
(451, 'Y', 'pages/index/technical-data-sheets', 0, 'technical-data-sheets', 'technical-data-sheets', 'technical-data-sheets', 'technical-data-sheets', '1'),
(452, 'N', 'dynamic_pages/detail', 26, 'telecom', 'Interpon Redox Triplex', 'The three-layer Interpon Redox Triplex system combines zincrich primer Interpon Redox PZ (cathodic protection) with the barrier-protective primer Interpon Redox Plus – finished with the Interpon topcoat of your choice.', 'protection, Interpon, substrate, Redox, steel, environments, corrosion, metal, system, primer, Triplex, isolates, corrosive, environment, water, technique, airproof, barrier, coating, Oxygen', '1'),
(453, 'N', 'city/detail', 29, 'noida', 'Noida', '', '', '1'),
(461, 'N', 'blogs/detail', 7, 'news-blog-test', 'news blog test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam enim lacus, viverra non volutpat nec, vulputate a lorem. Praesent id aliquet felis. Pellentesque hendrerit, dolor nec porttitor elementum, mi nunc pharetra', 'dolor, eget, felis, ipsum, volutpat, nunc, accumsan, eleifend, lacinia, dignissim, metus, consequat, quis, pharetra, erat, finibus, hendrerit, arcu, aliquet, auctor', '1'),
(462, 'N', 'blogs/detail', 4, 'news-blogs', 'news-blogs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam enim lacus, viverra non volutpat nec, vulputate a lorem. Praesent id aliquet felis. Pellentesque hendrerit, dolor nec porttitor elementum, mi nunc pharetra', 'dolor, eget, felis, ipsum, volutpat, nunc, accumsan, eleifend, lacinia, dignissim, metus, consequat, quis, pharetra, erat, finibus, hendrerit, arcu, aliquet, auctor', '1'),
(463, 'N', 'blogs/detail', 5, 'newsrooms2', 'newsrooms2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam enim lacus, viverra non volutpat nec, vulputate a lorem. Praesent id aliquet felis. Pellentesque hendrerit, dolor nec porttitor elementum, mi nunc pharetra', 'dolor, eget, felis, ipsum, volutpat, nunc, accumsan, eleifend, lacinia, dignissim, metus, consequat, quis, pharetra, erat, finibus, hendrerit, arcu, aliquet, auctor', '1'),
(464, 'N', 'blogs/detail', 6, 'fgfgfg', 'fgfgfg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam enim lacus, viverra non volutpat nec, vulputate a lorem. Praesent id aliquet felis. Pellentesque hendrerit, dolor nec porttitor elementum, mi nunc pharetra', 'dolor, eget, felis, ipsum, volutpat, nunc, accumsan, eleifend, lacinia, dignissim, metus, consequat, quis, pharetra, erat, finibus, hendrerit, arcu, aliquet, auctor', '1'),
(465, 'N', 'testimonials/details', 21, 'subash-chandra-pandey-21', 'subash chandra pandey', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget', 'Lorem, dolor, amet, consectetur, adipiscing, elit, Maecenas, ipsum, eget, iaculis, sagittis, justo, Integer, Nunc, ullamcorper, nibh, venenatis, finibus, blandit, orci', '1'),
(466, 'N', 'testimonials/details', 22, 'subash-chandra-pandey-22', 'subash chandra pandey', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget', 'Lorem, dolor, amet, consectetur, adipiscing, elit, Maecenas, ipsum, eget, iaculis, sagittis, justo, Integer, Nunc, ullamcorper, nibh, venenatis, finibus, blandit, orci', '1'),
(468, 'N', 'testimonials/details', 23, 'suhani-pandey-23', 'suhani pandey', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget', 'Lorem, dolor, amet, consectetur, adipiscing, elit, Maecenas, ipsum, eget, iaculis, sagittis, justo, Integer, Nunc, ullamcorper, nibh, venenatis, finibus, blandit, orci', '1'),
(469, 'N', 'testimonials/details', 24, 'suhani-pandey-24', 'suhani pandey', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget', 'Lorem, dolor, amet, consectetur, adipiscing, elit, Maecenas, ipsum, eget, iaculis, sagittis, justo, Integer, Nunc, ullamcorper, nibh, venenatis, finibus, blandit, orci', '1'),
(471, 'Y', 'pages/index\r\n', 0, 'services', 'services', 'services', 'services', '1'),
(474, 'N', 'gallery/details', 1, 'webs-application', 'Image Name', 'Image Name', 'Image, Name', '1'),
(476, 'N', 'gallery/details', 5, 'events', 'company function', 'company function', 'company, function', '1'),
(477, 'N', 'gallery/details', 6, 'media', 'project display', 'project display', 'project, display', '1'),
(478, 'N', 'gallery/details', 10, 'branding', 'blog display', 'blog display', 'blog, display', '1'),
(479, 'N', 'gallery/details', 15, 'creative', 'F10 gallery', 'F10 gallery', 'gallery', '1'),
(480, 'Y', 'pages/index/group-of-company', 0, 'group-of-company', 'company - group-of-company', 'group-of-company', 'group-of-company', '1'),
(481, 'Y', 'pages/index/resourses', 0, 'resourses', 'resourses', 'approvals', 'approvals', '1'),
(482, 'Y', 'pages/index/daily-lme-details', 0, 'daily-lme-details', 'daily-lme-details', 'daily-lme-details', 'daily-lme-details', '1'),
(484, 'Y', 'pages/index/qhse-policy', 0, 'qhse-policy', 'qhse-policy', 'qhse-policy', 'qhse-policy', '1'),
(485, 'Y', 'broucher/index', 0, 'catalog', 'catalog', 'catalog', 'catalog', '1'),
(486, 'N', 'events/details', 1, 'news-details4', 'News Details4', 'News Details3News Details3News Details3News Details3News Details3News Details3News Details3', 'News, Details', '1'),
(487, 'N', 'events/details', 2, 'news-details1', 'News Details1', 'Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam. Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem', 'Lorem, ipsum, dolor, amet, consem, ctetuering, adipisc, elit, diam', '1'),
(488, 'N', 'events/details', 3, 'news-details2', 'News Details2', 'Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.Lorem ipsum dolor sit amet consem et ctetuering adipisc elit sed diam.', 'Lorem, ipsum, dolor, amet, consem, ctetuering, adipisc, elit, diam', '1'),
(489, 'Y', 'events/index', 0, 'news-and-events', 'news-and-events', 'news-and-events', 'news-and-events', '1'),
(490, 'Y', 'image_gallery/index', 0, 'image-gallery', 'image-gallery', 'image-gallery', 'image-gallery', '1'),
(491, 'Y', 'video_gallery/index', 0, 'video-gallery', 'video-gallery', 'video-gallery', 'video-gallery', '1'),
(492, 'N', 'category/index', 53, 'carpet/carpets-subcategory', 'Carpets subcategory', 'Carpets subcategory', 'Carpets, subcategory', '1'),
(493, 'N', 'category/index', 54, 'low-voltage-power-2', 'Low voltage power 2', 'Low voltage power 2', 'voltage, power', '1'),
(494, 'Y', 'pages/index/director-message', 0, 'director-message', 'director-message', 'director-message', 'director-message', '1'),
(495, 'Y', 'pages/index/vision', 0, 'vision', 'vision', 'vision', 'vision', '1'),
(496, 'Y', 'pages/index/aboutus', 0, 'aboutus', 'aboutus', 'aboutus', 'aboutus', '1'),
(497, 'N', 'dynamic_pages/detail', 27, 'sales', 'use 1', 'use 1? page data', 'page, data', '1'),
(498, 'N', 'dynamic_pages/detail', 28, 'marketing', 'use 2', 'use 2? page data', 'page, data', '1'),
(499, 'N', 'dynamic_pages/detail', 29, 'test-reports-1', 'test reports 1', 'test reports 1', 'test, reports', '1'),
(500, 'N', 'dynamic_pages/detail', 30, 'test-reports-2', 'test reports 2', 'test reports 2', 'test, reports', '1'),
(501, 'N', 'dynamic_pages/detail', 31, 'healthcare', 'Healthcare', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(502, 'N', 'dynamic_pages/detail', 32, 'travel-and-hospitality', 'Travel and hospitality', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(503, 'N', 'dynamic_pages/detail', 33, 'education', 'Education', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(504, 'N', 'dynamic_pages/detail', 34, 'real-estate', 'Real Estate', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(505, 'N', 'dynamic_pages/detail', 35, 'finance', 'Finance', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(506, 'N', 'dynamic_pages/detail', 36, 'it', 'IT', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(507, 'N', 'dynamic_pages/detail', 37, 'hr-support', 'HR Support', '', 'imperdiet, quis, eget, ante, Donec, ipsum, Aenean, orci, arcu, vitae, posuere, nisi, ultricies, Phasellus, amet, tincidunt, faucibus, Nullam, hendrerit, libero', '1'),
(513, 'N', 'services/detail', 40, 'event-4', 'event 4', 'Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM\'s social media. assets at scale, promoting personalization, individuality, and joyful living', 'assets, scale, promoting, personalization, individuality, joyful, living, leading, OEM\'s, social, Produced, high-quality, creative, media', '1'),
(515, 'N', 'services/detail', 41, 'redox-triplex-2', 'Redox Triplex 2', 'At Pilbara Powder Coating, we excel as the leading supplier of Interpon TC. Through our broadest range of Interpon trade powder coating, we strive towards making your life both easier and simpler. The exterior durable', 'Interpon, coating, range, powder, Pilbara, exterior, simpler, easier, life, making, towards, strive, trade, Powder, broadest, Through, supplier, leading, excel, Coating', '1'),
(522, 'N', 'gallery/details', 29, 'testvikas', 'Testvikas', 'Testvikas', 'Testvikas', '1'),
(523, 'N', 'gallery/details', 32, 'hi-teat', 'Hi Teat', 'Hi Teat', 'Teat', '1'),
(525, 'N', 'gallery/details', 35, 'vikastest1', 'Vikastest1', 'Vikastest1', 'Vikastest', '1'),
(526, 'N', 'gallery/details', 38, 'tod24', 'Tod24', 'Tod24', '', '1'),
(528, 'N', 'gallery/details', 41, 'testdd', 'Testdd', 'Testdd', 'Testdd', '1'),
(541, 'N', 'service_category/index', 33, '', 'Test123', '', '', '1'),
(542, 'N', 'service_category/index', 34, '', 'Test 1234', '', '', '1'),
(543, 'N', 'service_category/index', 37, 'videography-and-photography', 'Media Advertisements, Movies, and Photography', '', '', '1'),
(544, 'N', 'services/detail', 54, 'videography-and-photography', 'Videography and Photography', 'Movies We are experienced in filming all types of events, including conferences, seminars, live stage shows, concerts, exhibitions, and fashion shows. Whether your project is a documentary, any kind of video, or a TV', 'hand, Agency, effective, shows, Movies, images, content, product, service, stunning, believe, artistic, creative, talents, prized, cool, photography, Inspiring, portfolio, imagery', '1'),
(545, 'N', 'services/detail', 55, 'exhibition', 'Exhibition', '', '', '1'),
(546, 'N', 'services/detail', 56, 'design', 'Design', '', '', '1'),
(547, 'N', 'services/detail', 57, 'application', 'Application', '', '', '1'),
(548, 'N', 'services/detail', 58, 'fit-out', 'Fit Out', '', '', '1'),
(549, 'N', 'services/detail', 59, 'photography', 'Photography', '', '', '1'),
(550, 'N', 'gallery/details', 57, 'events-exhibitions-management', 'Events & Exhibitions Management', 'Events & Exhibitions Management', 'Events, Exhibitions, Management', '1'),
(551, 'N', 'gallery/details', 58, 'branding-design', 'Branding & Design', 'Branding & Design', 'Branding, Design', '1'),
(553, 'N', 'gallery/details', 61, 'abhishek', 'Abhishek', 'Abhishek', 'Abhishek', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wl_newsletters`
--

CREATE TABLE `wl_newsletters` (
  `subscriber_id` int(11) NOT NULL,
  `subscriber_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscriber_email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1=Active,0=Deactive,2=Deleted',
  `mail_status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '1=Yes,0=No',
  `subscribe_date` datetime DEFAULT NULL,
  `mail_sent_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_newsletters`
--

INSERT INTO `wl_newsletters` (`subscriber_id`, `subscriber_name`, `subscriber_email`, `status`, `mail_status`, `subscribe_date`, `mail_sent_date`) VALUES
(23, 'mansih', 'aaaa@gmail.com', '1', '0', NULL, NULL),
(26, 'Manish', 'weblink.manish84@gmail.com', '1', '0', NULL, NULL),
(27, 'subash chandra pandey', 'info@bagboxmt.com', '1', '0', NULL, NULL),
(28, 'subash chandra pandey', 'subash@bagboxmt.com', '1', '0', NULL, NULL),
(29, 'subash chandra pandey', 'info111@bagboxmt.com', '1', '0', NULL, NULL),
(30, 'subash', 'subash@gmail.com', '1', '0', NULL, NULL),
(31, 'subash', 'subash1@gmail.com', '1', '0', NULL, NULL),
(32, '', 'mmunir_s@yahoo.com', '1', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_newsletter_groups`
--

CREATE TABLE `wl_newsletter_groups` (
  `newsletter_groups_id` int(11) NOT NULL,
  `group_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Actice,0=Inactive',
  `groups_created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wl_newsletter_group_subscriber`
--

CREATE TABLE `wl_newsletter_group_subscriber` (
  `group_subscriber_id` int(11) NOT NULL,
  `newsletter_groups_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wl_newsletter_log`
--

CREATE TABLE `wl_newsletter_log` (
  `id` int(11) NOT NULL,
  `newsletter_data` longtext NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wl_news_media`
--

CREATE TABLE `wl_news_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `color_id` bigint(20) NOT NULL DEFAULT '0',
  `color_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_id` int(10) UNSIGNED NOT NULL,
  `media_type` enum('photo','video','pdf') CHARACTER SET utf8 NOT NULL DEFAULT 'photo',
  `media` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `sort_order` int(11) DEFAULT NULL,
  `media_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_news_media`
--

INSERT INTO `wl_news_media` (`id`, `color_id`, `color_status`, `products_id`, `media_type`, `media`, `product_code`, `is_default`, `sort_order`, `media_date_added`) VALUES
(21, 0, '1', 27, 'photo', 'news_img22.jpg', NULL, 'N', NULL, '2020-09-03 08:25:23'),
(20, 0, '1', 29, 'photo', 'news-bg1.jpg', NULL, 'N', NULL, '2020-12-02 05:11:00'),
(22, 0, '1', 30, 'photo', 'pro-31.jpg', NULL, 'Y', NULL, '2020-11-10 12:48:33'),
(23, 0, '1', 31, 'photo', 'pro-21.jpg', NULL, 'Y', NULL, '2020-11-10 12:57:11'),
(24, 0, '1', 32, 'photo', 'news-bg2.jpg', NULL, 'Y', NULL, '2020-12-02 05:17:48'),
(25, 0, '1', 32, 'photo', 'news-pic1.jpg', NULL, 'N', NULL, '2020-12-02 05:17:48'),
(26, 0, '1', 32, 'photo', 'news-pic-b1.jpg', NULL, 'N', NULL, '2020-12-02 05:17:48'),
(27, 0, '1', 32, 'photo', 'news-pic-b2.jpg', NULL, 'N', NULL, '2020-12-02 05:17:48'),
(28, 0, '1', 33, 'photo', 'news-pic2.jpg', NULL, 'Y', NULL, '2020-12-02 05:18:26'),
(29, 0, '1', 33, 'photo', 'news-pic-b11.jpg', NULL, 'N', NULL, '2020-12-02 05:18:26'),
(30, 0, '1', 33, 'photo', 'news-pic-b21.jpg', NULL, 'N', NULL, '2020-12-02 05:18:26'),
(31, 0, '1', 33, 'photo', 'news-pic-b3.jpg', NULL, 'N', NULL, '2020-12-02 05:18:26'),
(32, 0, '1', 34, 'photo', 'news-pic-b24.jpg', NULL, 'Y', NULL, '2020-12-02 06:22:17'),
(35, 0, '1', 35, 'photo', 'news-pic-b32.jpg', NULL, 'Y', NULL, '2020-12-02 06:22:04'),
(38, 0, '1', 36, 'photo', 'brand23.jpg', NULL, 'Y', NULL, '2020-12-07 06:29:46'),
(39, 0, '1', 36, 'photo', 'brand21.jpg', NULL, 'N', NULL, '2020-12-07 06:29:46'),
(40, 0, '1', 37, 'photo', 'partner40.jpg', NULL, 'Y', NULL, '2020-12-07 06:30:13'),
(41, 0, '1', 37, 'photo', 'brand231.jpg', NULL, 'N', NULL, '2020-12-07 06:30:13'),
(42, 0, '1', 39, 'photo', '-.jpg', NULL, 'N', NULL, '2021-06-11 08:20:12'),
(43, 0, '1', 39, 'photo', '0a80c52e-2c3d-4ac3-8012-19e1e0313245.jpeg', NULL, 'N', NULL, '2021-06-11 08:20:12'),
(44, 0, '1', 39, 'photo', '0d9867bc-2782-42f9-830a-7b37e19b1417.png', NULL, 'N', NULL, '2021-06-11 08:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `wl_order`
--

CREATE TABLE `wl_order` (
  `order_id` int(11) NOT NULL,
  `invoice_number` varchar(30) NOT NULL,
  `first_name` varchar(80) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `address` text,
  `state` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `ship_to` varchar(80) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `po_text` varchar(80) DEFAULT NULL,
  `contact_no` varchar(30) DEFAULT NULL,
  `ship_via` varchar(80) DEFAULT NULL,
  `delivery` varchar(80) DEFAULT NULL,
  `shipping_term` varchar(80) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `shipping` varchar(50) DEFAULT NULL,
  `other` varchar(50) DEFAULT NULL,
  `comment` text,
  `sub_total` varchar(50) DEFAULT NULL,
  `total_price` varchar(50) DEFAULT NULL,
  `order_pdf_file` varchar(100) DEFAULT NULL,
  `drawing_sheet` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active,2=Delete',
  `receive_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_order`
--

INSERT INTO `wl_order` (`order_id`, `invoice_number`, `first_name`, `last_name`, `email`, `address`, `state`, `phone`, `ship_to`, `purchase_date`, `po_text`, `contact_no`, `ship_via`, `delivery`, `shipping_term`, `gst`, `shipping`, `other`, `comment`, `sub_total`, `total_price`, `order_pdf_file`, `drawing_sheet`, `status`, `receive_date`) VALUES
(1, '1', 'sdfafasd', NULL, '', 'sdffdsas', 'sdfsa', 'dsfsa', '', '0000-00-00', '', 'sdafsf', 'dsfdsfds', 'fdsafdsfdsf', NULL, 'dasfdsaf', 'sfsf', 'dsfdsa', 'dsfsafsadsa', NULL, '', NULL, NULL, '1', '2021-09-11 08:55:15'),
(2, '2', 'dsfafds', NULL, 'test@gmail.com', 'sdfasf', 'dsfasds', 'dsfdasfa', 'dsfsads', '2021-09-21', 'dsfdsaf', 'sdfdsaf', 'dsafdsa', 'fdsf', 'sdffdsa', 'sdfdas', 'sfdsf', 'fdsafd', 'dsfsafsda', NULL, 'fdsa', 'Order2.pdf', NULL, '1', '2021-09-11 11:19:17'),
(3, '3', 'Manish', NULL, 'test@gmail.com', 'delhi', 'sd fsd', 'sdfsa', 'dsf afsdfsd fsdsa', '2021-09-22', 'sdaffs', 'sdafsdaf', 'dsafsdfs', 'dfsdfsd', 'sdfdsa', 'dsfsafdas', 'dsfdsa', 'dfsas', 'sdfdsf sdfsdfsda', NULL, 'sdfasd', 'Order3.pdf', NULL, '1', '2021-09-18 11:19:24'),
(4, '4', 'Deepika Saxena', NULL, 'deepika@weblinkindia.net', 'Ggfggdsfsdf', 'delhi', '9015300933', 'test ', '2021-09-18', '56', '45454545', 'desd', '343', 'fdgfgf', '787', '65', '656', 'gghfgfgfgfgf hfhfh fhgh', NULL, '353545', 'Order4.pdf', NULL, '1', '2021-09-18 11:45:35'),
(5, '5', 'Dimpy', NULL, 'dimpy@gmail.com', 'Delhi', 'delhi', '09876543210', 'Delhi', '2021-09-20', '', 'cont', 'ship', 'Del', 'term', '500', '1000', '', '', NULL, '12500', 'Order5.pdf', NULL, '1', '2021-09-20 15:04:27'),
(6, '6', 'Dimpy', NULL, 'dimpy@gmail.com', 'Delhi', 'delhi', '09876543210', 'Delhi', '2021-09-20', 'klbndkfl', 'Contact', 'Ship', 'Deli', 'Ship', '350', '500', '150', 'Comments or Special Instructions', NULL, '8000', 'Order6.pdf', NULL, '1', '2021-09-20 15:29:17'),
(7, '7', 'testing', NULL, 'ophtp@gmail.com', 'tikjrtjh', 'jgjgj', 'gijktojh', 'opgie[rjhnm ', '2021-09-22', 'dhhfh', 'rogreog', 'eoipwe', 'pogkipr', 'hkitppk[', '500', '500', '150', '', '11000', '12500', 'Order7.pdf', NULL, '1', '2021-09-22 10:30:18'),
(8, '8', 'sdfdsafsda', NULL, 'admin@gmail.com', 'dsfdsafsda', 'sdfdas', '32423423', 'dsfdsafsda', '0000-00-00', '', 'sdf sdaf', 'f f', 'sdaf sd', 'fsddas', '10', '10', 'test', 'dsfda das', '50', '70', 'Order8.pdf', NULL, '1', '2021-09-22 10:53:23'),
(9, '9', 'test', NULL, 'skf@gmail.com', 'jgnroif', 'oigh', 'fgnfgfgn', 'tgtrtyy', '0000-00-00', '', 'ybrtyu', 'jfjutj', 'yjtj', 'ytjtyj', '20', '20', 'uru', 'jkgnbgjkbnkgnb', '90', '130', 'Order9.pdf', NULL, '1', '2021-09-22 11:46:15'),
(10, '10', 'test', NULL, 'some@gmail.com', 'trjy', 'drtunt', 'ytjt mi', 'm5776yu mi', '2021-09-24', '', 'rtby', 'nyun', 'trynru', 'tnynu', '500', '500', 'ghdfgh', 'tynbyumyu,o ou', '5000', '6000', 'Order10.pdf', NULL, '1', '2021-09-23 10:11:19'),
(11, '11', 'deepika saxena', NULL, 'deepika@weblinkindia.net', '1', 'test state', '+619015300933', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 'Order11.pdf', NULL, '1', '2021-10-18 06:03:05'),
(12, '12', 'Wayne OBrien', NULL, 'wayne@waycorp.com.au', '2/23 Foster Street', 'NSW', '450830270', 'TEst', '2021-10-18', '12233', 'Wayne', '', '', '', '', '', '', '', '1200', '', 'Order12.pdf', NULL, '1', '2021-10-18 10:30:05'),
(13, '13', 'Wayne OBrien', NULL, 'wayne@waycorp.com.au', '2/23 Foster Street', 'NSW', '450830270', '', '2021-10-18', '12233', 'Wayne', '', '', '', '', '', '', '', '', '', 'Order13.pdf', NULL, '1', '2021-10-18 10:52:53'),
(14, '14', 'Wayne OBrien', NULL, 'wayne@waycorp.com.au', '2/23 Foster Street', 'NSW', '450830270', '', '0000-00-00', '', 'Wayne', '', '', '', '', '', '', '', '', '', 'Order14.pdf', NULL, '1', '2021-10-19 06:48:52'),
(15, '15', 'deepika saxena', NULL, 'deepika@weblinkindia.net', '1', 'test state', '9015300933', 'test content here', '2021-10-21', 'PO2', '', '', '', '', '36', '50', '10', '', '360', '396', 'Order15.pdf', NULL, '1', '2021-10-20 09:39:05'),
(16, '16', 'Wayne OBrien', NULL, 'wayne@waycorp.com.au', '2/23 Foster Street', 'NSW', '450830270', '', '0000-00-00', '', '', '', '', '', '115', '0', '0', '', '1150', '1265', 'Order16.pdf', NULL, '1', '2021-10-20 09:55:10'),
(17, '17', 'deepika saxena', NULL, 'deepika@weblinkindia.net', '1', 'test state', '+619015300933', 'dgfhfh', '0000-00-00', '', '', '', '', '', '5', '10', '10', 'dgdfg fg gfdg', '50', '55', 'Order17.pdf', NULL, '1', '2021-10-20 11:39:32'),
(18, '18', 'Kuldeep', NULL, 'kuldeep.weblink@gmail.com', '302 Harshwardhan Chember, Next to Oshiwara Police Station, New Link Road, Andheri (W) Mumbai-400102', 'Delhi', '+70 17 29 38 19', '\r\nge\r\nhe\r\ne\r\nh\r\ne\r\ng\r\ne\r\ngeg\r\neg\r\ne', '2021-10-28', 'DEEP#1234', 'Dinesh', 'Fedex', 'Same day', '10AM', '1.8', '0', '0', 'rgr\r\nh\r\nrth\r\nrh\r\nrtyth', '18', '19.8', 'Order18.pdf', 'users_login.pdf', '1', '2021-10-22 10:21:28'),
(19, '19', 'Kuldeep', NULL, 'kuldeep.weblink@gmail.com', '302 Harshwardhan Chember, Next to Oshiwara Police Station, New Link Road, Andheri (W) Mumbai-400102', 'Delhi', '7017298996', '\r\nhf\r\n\r\njty', '2021-10-23', 'DEEP#1234', 'Dinesh', 'Fedex', 'Same day', '10AM', '1.8', '0', '0', 'd\r\nh\r\nh\r\nrtjy', '18', '19.8', 'Order19.pdf', 'users_login1.pdf', '1', '2021-10-22 10:32:34');

-- --------------------------------------------------------

--
-- Table structure for table `wl_orders_products`
--

CREATE TABLE `wl_orders_products` (
  `orders_products_id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `description` text,
  `m2` varchar(50) DEFAULT NULL,
  `item_price` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_orders_products`
--

INSERT INTO `wl_orders_products` (`orders_products_id`, `orders_id`, `item_name`, `description`, `m2`, `item_price`) VALUES
(1, 2, 'dsf', 'fsfs', 'fdsfds', 'fsfas'),
(2, 2, 'dsafdsf', 'fsfs', 'fdsf', 'saf'),
(3, 2, 'dfdsfsasd', 'dsfasfs', 'sdfsfdsa', 'fdsdsa'),
(4, 2, 'sdfsdafd', 'dsfdsfds', 'dfdsfds', 'sdfdsf'),
(5, 2, 'dsfsfdsa', 'sdfsds', 'sdfsd', 'fsddsfsd'),
(6, 2, 'dsfsafds', 'sdfsfds', 'dsfdas', 'fdfdsaasd'),
(7, 2, 'sdfsafdas', 'fdsgfd', 'fgfdgsd', 'fdgdfs'),
(8, 2, 'dsfasfa', 'dsfsdafa', 'dsafdsa', 'dsfasd'),
(9, 2, 'dsfa', 'sdfsda', 'sdafdsa', 'fasfd'),
(10, 2, 'dsfdsafas', 'gfs', 'fgs', 'fdgfs'),
(11, 3, 'sdfdsaf', 'dsafsdfds', 'dsafds', 'dsfasd'),
(12, 4, 'bvbv', 'des', 'm2', 'hgh'),
(13, 5, 'item', 'Description', 'M2', '5000'),
(14, 5, 'Item2', 'Description', 'M3', '6000'),
(15, 6, 'Itel', 'Desc', 'M2', '3000'),
(16, 6, 'Item2', ' Descr', 'M3', '4000'),
(17, 7, 'ofrwirp', 'okifewokfpwkip pwork', 'e;lk', '5000'),
(18, 7, 'tlbykpr u', 'ghopi yjotpk', 'orybop', '6000'),
(19, 8, '1', 'dsfasfas', '10', '50'),
(20, 9, '1', 'fd ghyhjy', 'nn', '90'),
(21, 10, '5', 'yujty muk', '7', '5000'),
(22, 12, 'Test ', 'Test Item', '100', '1200'),
(23, 13, 'Test ', 'Test Item', '200', '1200'),
(24, 14, 'Test ', '', '', ''),
(25, 15, 'a', '', '', '100'),
(26, 15, 'b', '', '', '100'),
(27, 15, 'c', '', '', '100'),
(28, 16, 'Item 1', '', '', '500'),
(29, 16, 'Item 2', '', '', '650'),
(30, 17, 'h', '', '', '10'),
(31, 17, 'h', '', '', '10'),
(32, 17, 'h', '', '', '10'),
(33, 18, 'test', 'testtest testtest testtest testtest testtest testtest testtest testtest testtest testtest testtest', '100', '18'),
(34, 19, 'test', 'testtest testtest testtest testtest testtest testtest testtest testtest testtest testtest testtest', '100', '18');

-- --------------------------------------------------------

--
-- Table structure for table `wl_portfolio`
--

CREATE TABLE `wl_portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `portfolio_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `portfolio_image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portfolio_description` longtext COLLATE utf8_unicode_ci,
  `publisher` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_portfolio`
--

INSERT INTO `wl_portfolio` (`portfolio_id`, `portfolio_title`, `friendly_url`, `portfolio_image`, `alt_tag`, `portfolio_description`, `publisher`, `sort_order`, `status`, `date_added`, `date_updated`) VALUES
(1, 'Adipic acid', 'adipic-acid', 'dl-portfolio-img.jpg', '', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, '1', '2019-03-14 12:06:11', '2019-03-14 12:10:17'),
(2, 'Iso Pthalic Acid', 'iso-pthalic-acid', 'portfolio-img4.jpg', '', '<p>Description here!!</p>\r\n', NULL, NULL, '1', '2019-03-14 12:10:51', NULL),
(3, 'Maleic Anhydride', 'maleic-anhydride', 'portfolio-img3.jpg', '', '<p>Description here</p>', NULL, NULL, '1', '2019-03-14 12:11:19', '2019-03-16 07:21:14'),
(4, 'Acrlic Acid', 'acrlic-acid', 'portfolio-img1.jpg', '', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on</p>\r\n', NULL, NULL, '1', '2019-03-20 10:29:18', NULL),
(5, 'Acid', 'acid', 'portfolio-img31.jpg', '', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on</p>\r\n', NULL, NULL, '1', '2019-03-20 10:33:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_products`
--

CREATE TABLE `wl_products` (
  `products_id` int(11) NOT NULL,
  `product_type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=Veterinary,1=Medical',
  `category_id` int(11) NOT NULL COMMENT 'parent_id= last level of category ',
  `category_ids` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `category_links` varchar(80) CHARACTER SET utf8 NOT NULL,
  `product_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `friendly_url` varchar(110) CHARACTER SET utf8 DEFAULT NULL,
  `product_code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `product_price` float(15,2) DEFAULT '0.00',
  `product_discounted_price` float(15,2) DEFAULT '0.00',
  `product_tax` float(10,2) NOT NULL DEFAULT '0.00',
  `products_description` text CHARACTER SET utf8 NOT NULL,
  `core_description` text COLLATE utf8_unicode_ci,
  `related_description` text COLLATE utf8_unicode_ci,
  `featured_product` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `hot_product` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `new_arrival` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `type` int(11) DEFAULT '1' COMMENT '1=Product, 2=Projects',
  `status` enum('0','1','2') CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `product_added_date` datetime NOT NULL,
  `product_updated_date` datetime DEFAULT NULL,
  `products_viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_alt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `low_stock_quantity` int(11) NOT NULL DEFAULT '0',
  `xls_type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `youtube_code` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_code2` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_products`
--

INSERT INTO `wl_products` (`products_id`, `product_type`, `category_id`, `category_ids`, `brand_id`, `category_links`, `product_name`, `friendly_url`, `product_code`, `product_price`, `product_discounted_price`, `product_tax`, `products_description`, `core_description`, `related_description`, `featured_product`, `hot_product`, `new_arrival`, `type`, `status`, `product_added_date`, `product_updated_date`, `products_viewed`, `product_alt`, `low_stock_quantity`, `xls_type`, `youtube_code`, `youtube_code2`) VALUES
(45, '0', 28, NULL, NULL, '28', 'BREATHING CIRCUIT BASIC', 'breathing-circuit-basic', '44', 0.00, 0.00, 0.00, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n', '', '', '0', '0', '0', 1, '1', '2020-12-02 14:11:46', '2022-01-22 10:46:13', 0, '', 0, '0', '', NULL),
(47, '0', 28, NULL, NULL, '28', 'test product2', 'test-product2', '2221', 0.00, 0.00, 0.00, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n', '', '', '1', '0', '0', 1, '1', '2020-12-02 14:12:52', '2022-02-03 05:49:15', 0, '', 0, '0', '&lt;iframe width=\"100%\" height=\"300\" src=\"https://www.youtube.com/embed/dx1MPawGE-E\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;', '&lt;iframe width=\"100%\" height=\"300\" src=\"https://www.youtube.com/embed/dx1MPawGE-E\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;'),
(44, '0', 28, NULL, NULL, '28', 'ANESTHESIA MASK WITH AIR CUSHION REUSABLE-DISPOSABLE', 'anesthesia-mask-with-air-cushion-reusable-disposable', '2222', 0.00, 0.00, 0.00, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet scelerisque tortor. Maecenas dui turpis, faucibus vel iaculis at, auctor non sapien. Donec dignissim nisl id posuere pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis facilisis sem quis elit aliquet tempus. Vivamus dapibus commodo dolor in gravida. Nunc suscipit euismod nibh a auctor. Nullam placerat mi quis ligula fermentum, vel dignissim mauris venenatis. Nunc sollicitudin velit velit, eu tincidunt sapien semper non. Curabitur quis sapien eget magna luctus tincidunt eu eu neque. Morbi dapibus pulvinar sem, non lobortis orci consectetur semper. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et. Aenean eget leo sit amet turpis efficitur aliquam. Aenean purus leo, lacinia ac metus vitae, auctor finibus erat. Donec sollicitudin sagittis libero. Vivamus ut auctor magna. Mauris vehicula lectus neque, non viverra neque faucibus et.</p>\r\n', '', '', '0', '0', '0', 1, '1', '2020-12-02 14:11:23', '2021-01-15 07:22:41', 0, '', 0, '0', '', NULL),
(49, '0', 47, NULL, NULL, '47,37', 'test by aa', 'test-by-aa', '111', 0.00, 0.00, 0.00, '<p>ds Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>\r\n\r\n<p>Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '<p>test Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>\r\n\r\n<p>Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '<p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>\r\n\r\n<p>Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '1', '0', '0', 1, '1', '2020-12-14 10:06:56', '2022-01-22 10:45:51', 0, '', 0, '0', '', NULL),
(50, '0', 47, NULL, NULL, '47,37', 'test product', 'test-product', '1212', 0.00, 0.00, 0.00, '<p>desc Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century. Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '<p>core Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>\r\n\r\n<p>Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '<p>related Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>\r\n\r\n<p>Dummy text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts. If the distribution of letters and &#39;words&#39; is random, the reader will not be distracted from making a neutral judgement on the visual impact and readability of the typefaces (typography), or the distribution of text on the page (layout or type area). For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces. Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>\r\n', '1', '0', '0', 1, '1', '2020-12-14 10:07:18', '2022-01-22 10:45:11', 0, '', 0, '0', '', NULL),
(51, '0', 47, NULL, NULL, '47,37', 'kbc news acds', 'kbc-news-acds', '343423', 0.00, 0.00, 0.00, '<p>dsafasdfds fsda dsaf d</p>\r\n', '<p>dsf fdasd</p>\r\n', '<p>dsa dsfdsfads dsf ads</p>\r\n', '1', '0', '0', 1, '1', '2021-01-23 10:33:27', '2022-01-22 10:44:48', 0, '', 0, '0', '', NULL),
(53, '0', 28, NULL, NULL, '28', 'Jet Fuel Oil, Oilfield Equipments and Supplies', 'jet-fuel-oil-oilfield-equipments-and-supplies', '678976', 0.00, 0.00, 0.00, '<p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.” The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn&#39;t distract from the layout. A practice not without controversy, laying out pages with me</p>\r\n', '', '', '1', '0', '0', 1, '1', '2021-06-14 05:29:52', '2022-01-24 05:13:55', 0, '', 0, '0', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wl_products_media`
--

CREATE TABLE `wl_products_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `color_id` bigint(20) NOT NULL DEFAULT '0',
  `color_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_id` int(10) UNSIGNED NOT NULL,
  `media_type` enum('photo','video','pdf') CHARACTER SET utf8 NOT NULL DEFAULT 'photo',
  `media` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `sort_order` int(11) DEFAULT NULL,
  `media_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_products_media`
--

INSERT INTO `wl_products_media` (`id`, `color_id`, `color_status`, `products_id`, `media_type`, `media`, `product_code`, `is_default`, `sort_order`, `media_date_added`) VALUES
(1, 0, '1', 43, 'photo', 'dl-pro1.jpg', NULL, 'N', NULL, '2020-12-02 10:21:01'),
(2, 0, '1', 43, 'photo', 'dl-pro2.jpg', NULL, 'N', NULL, '2020-12-02 10:21:01'),
(3, 0, '1', 43, 'photo', 'dl-pro3.jpg', NULL, 'N', NULL, '2020-12-02 10:21:01'),
(4, 0, '1', 43, 'photo', 'dl-pro4.jpg', NULL, 'N', NULL, '2020-12-02 10:21:01'),
(5, 0, '1', 42, 'photo', 'dl-pro11.jpg', NULL, 'N', NULL, '2020-12-02 10:21:12'),
(6, 0, '1', 42, 'photo', 'dl-pro21.jpg', NULL, 'N', NULL, '2020-12-02 10:21:12'),
(7, 0, '1', 42, 'photo', 'dl-pro31.jpg', NULL, 'N', NULL, '2020-12-02 10:21:12'),
(8, 0, '1', 41, 'photo', 'dl-pro22.jpg', NULL, 'N', NULL, '2020-12-02 10:21:27'),
(9, 0, '1', 41, 'photo', 'dl-pro32.jpg', NULL, 'N', NULL, '2020-12-02 10:21:27'),
(10, 0, '1', 40, 'photo', 'dl-pro41.jpg', NULL, 'N', NULL, '2020-12-02 10:21:41'),
(11, 0, '1', 40, 'photo', 'dl-pro33.jpg', NULL, 'N', NULL, '2020-12-02 10:21:41'),
(12, 0, '1', 40, 'photo', 'dl-pro23.jpg', NULL, 'N', NULL, '2020-12-02 10:21:41'),
(13, 0, '1', 40, 'photo', 'dl-pro12.jpg', NULL, 'N', NULL, '2020-12-02 10:21:41'),
(14, 0, '1', 40, 'photo', 'dl-pro24.jpg', NULL, 'N', NULL, '2020-12-02 10:21:41'),
(15, 0, '1', 39, 'photo', 'dl-pro25.jpg', NULL, 'N', NULL, '2020-12-02 10:21:52'),
(16, 0, '1', 39, 'photo', 'dl-pro13.jpg', NULL, 'N', NULL, '2020-12-02 10:21:52'),
(17, 0, '1', 38, 'photo', 'dl-pro26.jpg', NULL, 'N', NULL, '2020-12-02 10:22:15'),
(18, 0, '1', 38, 'photo', 'dl-pro14.jpg', NULL, 'N', NULL, '2020-12-02 10:22:15'),
(19, 0, '1', 38, 'photo', 'dm-pro1.jpg', NULL, 'N', NULL, '2020-12-02 10:22:15'),
(20, 0, '1', 38, 'photo', 'dl-pro34.jpg', NULL, 'N', NULL, '2020-12-02 10:22:15'),
(21, 0, '1', 44, 'photo', 'ds-pro1.jpg', NULL, 'Y', NULL, '2021-01-15 07:22:41'),
(22, 0, '1', 44, 'photo', 'dl-pro13.jpg', NULL, 'N', NULL, '2021-01-15 07:22:41'),
(23, 0, '1', 44, 'photo', 'dl-pro23.jpg', NULL, 'N', NULL, '2021-01-15 07:22:41'),
(24, 0, '1', 44, 'photo', 'dl-pro42.jpg', NULL, 'N', NULL, '2021-01-15 07:22:41'),
(25, 0, '1', 44, 'photo', 'dl-pro28.jpg', NULL, 'N', NULL, '2020-12-02 14:11:23'),
(26, 0, '1', 45, 'photo', 'dl-pro14.jpg', NULL, 'Y', NULL, '2021-01-15 07:22:53'),
(27, 0, '1', 45, 'photo', 'dl-pro24.jpg', NULL, 'N', NULL, '2021-01-15 07:22:53'),
(28, 0, '1', 45, 'photo', 'dl-pro33.jpg', NULL, 'N', NULL, '2021-01-15 07:22:53'),
(29, 0, '1', 46, 'photo', 'dl-pro17.jpg', NULL, 'Y', NULL, '2020-12-02 14:12:16'),
(30, 0, '1', 46, 'photo', 'dl-pro210.jpg', NULL, 'N', NULL, '2020-12-02 14:12:16'),
(31, 0, '1', 46, 'photo', 'dl-pro37.jpg', NULL, 'N', NULL, '2020-12-02 14:12:16'),
(32, 0, '1', 46, 'photo', 'dl-pro18.jpg', NULL, 'N', NULL, '2020-12-02 14:12:16'),
(33, 0, '1', 46, 'photo', 'dm-pro11.jpg', NULL, 'N', NULL, '2020-12-02 14:12:16'),
(34, 0, '1', 47, 'photo', 'dl-pro1.jpg', NULL, 'Y', NULL, '2022-02-03 05:49:15'),
(35, 0, '1', 47, 'photo', 'dl-pro2.jpg', NULL, 'N', NULL, '2022-02-03 05:49:15'),
(36, 0, '1', 47, 'photo', 'dl-pro3.jpg', NULL, 'N', NULL, '2022-02-03 05:49:15'),
(37, 0, '1', 47, 'photo', 'dl-pro4.jpg', NULL, 'N', NULL, '2022-02-03 05:49:15'),
(38, 0, '1', 47, 'photo', 'dl-pro31.jpg', NULL, 'N', NULL, '2022-02-03 05:49:15'),
(39, 0, '1', 48, 'photo', 'dl-pro44.jpg', NULL, 'Y', NULL, '2020-12-02 14:13:26'),
(40, 0, '1', 48, 'photo', 'dl-pro39.jpg', NULL, 'N', NULL, '2020-12-02 14:13:26'),
(41, 0, '1', 49, 'photo', 'pro-pic-b1.jpg', NULL, 'N', NULL, '2022-01-22 10:45:51'),
(42, 0, '1', 49, 'photo', 'pro-pic11.jpg', NULL, 'N', NULL, '2022-01-22 10:45:51'),
(43, 0, '1', 49, 'photo', 'dl-pro3.jpg', NULL, 'N', NULL, '2021-01-15 07:21:31'),
(44, 0, '1', 49, 'photo', 'dl-pro4.jpg', NULL, 'N', NULL, '2021-01-15 07:21:31'),
(45, 0, '1', 50, 'photo', 'gal-pic1.jpg', NULL, 'N', NULL, '2022-01-22 10:45:11'),
(46, 0, '1', 50, 'photo', 'pro-pic-b2.jpg', NULL, 'N', NULL, '2022-01-22 10:45:11'),
(47, 0, '1', 50, 'photo', 'dl-pro41.jpg', NULL, 'N', NULL, '2021-01-15 07:22:03'),
(48, 0, '1', 50, 'photo', 'dl-pro31.jpg', NULL, 'N', NULL, '2021-01-15 07:22:03'),
(49, 0, '1', 53, 'photo', 'pro-pic1.jpg', NULL, 'Y', NULL, '2022-01-22 10:44:18'),
(50, 0, '1', 53, 'photo', 'pro-pic2.jpg', NULL, 'N', NULL, '2022-01-22 10:44:18'),
(51, 0, '1', 53, 'photo', 'pro-pic3.jpg', NULL, 'N', NULL, '2022-01-22 10:44:18'),
(52, 0, '1', 53, 'photo', 'pro-pic4.jpg', NULL, 'N', NULL, '2022-01-22 10:44:18'),
(53, 0, '1', 51, 'photo', 'proj-pic2.jpg', NULL, 'N', NULL, '2022-01-22 10:44:48'),
(54, 0, '1', 51, 'photo', 'proj-pic21.jpg', NULL, 'N', NULL, '2022-01-22 10:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `wl_products_related`
--

CREATE TABLE `wl_products_related` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `sort_order` int(11) DEFAULT NULL,
  `related_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_products_related`
--

INSERT INTO `wl_products_related` (`id`, `product_id`, `related_id`, `status`, `sort_order`, `related_date_added`) VALUES
(22, 75, 42, '1', NULL, '2019-06-10 12:55:27'),
(23, 34, 33, '1', NULL, '2020-01-01 09:46:26'),
(24, 34, 32, '1', NULL, '2020-01-01 09:46:26'),
(25, 34, 31, '1', NULL, '2020-01-01 09:46:26'),
(26, 33, 35, '1', NULL, '2020-04-10 06:53:32'),
(27, 33, 34, '1', NULL, '2020-04-10 06:53:32'),
(28, 38, 37, '1', NULL, '2020-04-14 15:13:34'),
(29, 38, 36, '1', NULL, '2020-04-14 15:13:34'),
(30, 38, 35, '1', NULL, '2020-04-14 15:13:34'),
(31, 38, 34, '1', NULL, '2020-04-14 15:13:34'),
(32, 38, 33, '1', NULL, '2020-04-14 15:13:34'),
(33, 37, 38, '1', NULL, '2020-04-14 15:13:59'),
(34, 37, 36, '1', NULL, '2020-04-14 15:13:59'),
(35, 37, 35, '1', NULL, '2020-04-14 15:13:59'),
(36, 37, 34, '1', NULL, '2020-04-14 15:13:59'),
(37, 36, 38, '1', NULL, '2020-04-14 15:14:13'),
(38, 36, 37, '1', NULL, '2020-04-14 15:14:13'),
(39, 36, 35, '1', NULL, '2020-04-14 15:14:13'),
(40, 35, 38, '1', NULL, '2020-04-14 15:14:28'),
(41, 35, 36, '1', NULL, '2020-04-14 15:14:28'),
(42, 35, 33, '1', NULL, '2020-04-14 15:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `wl_review`
--

CREATE TABLE `wl_review` (
  `review_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `rev_type` enum('P','B') NOT NULL DEFAULT 'P' COMMENT 'P=Product, B=Business',
  `poster_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` int(11) NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `prod_id` int(11) NOT NULL COMMENT 'Prod_id/Business_id',
  `mem_id` int(11) DEFAULT NULL,
  `posted_date` datetime DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Actice,0=Inactive'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_review`
--

INSERT INTO `wl_review` (`review_id`, `language_id`, `rev_type`, `poster_name`, `email`, `rate`, `comment`, `prod_id`, `mem_id`, `posted_date`, `status`) VALUES
(1, 1, 'P', 'dsfdsafdsa', 'test@gmail.com', 2, 'sdfsddssa', 45, NULL, '2021-06-14 09:14:35', '1'),
(2, 1, 'P', 'sdfafds', 'test@gmail.com', 4, 'dsfdsdsa', 45, NULL, '2021-06-14 09:15:57', '1'),
(3, 1, 'P', 'sdfafdsa', 'test@gmial.ocmn', 1, 'sdfsafsa', 45, NULL, '2021-06-14 09:16:45', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wl_services`
--

CREATE TABLE `wl_services` (
  `service_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT 'parent_id= last level of category ',
  `sort_order` int(11) DEFAULT NULL,
  `category_links` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `service_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `visit_link` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_city` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(110) CHARACTER SET utf8 DEFAULT NULL,
  `service_code` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `service_price` float(15,2) NOT NULL DEFAULT '0.00',
  `broucher_doc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_discounted_price` float(15,2) NOT NULL DEFAULT '0.00',
  `service_description` text CHARACTER SET utf8,
  `highlights` text COLLATE utf8_unicode_ci,
  `service_offered` text COLLATE utf8_unicode_ci,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('0','1','2') CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `service_added_date` datetime DEFAULT NULL,
  `service_updated_date` datetime DEFAULT NULL,
  `service_viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `service_alt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_colour` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `description_text_colour` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_services`
--

INSERT INTO `wl_services` (`service_id`, `category_id`, `sort_order`, `category_links`, `service_name`, `visit_link`, `video_link`, `service_address`, `service_city`, `friendly_url`, `service_code`, `service_price`, `broucher_doc`, `service_discounted_price`, `service_description`, `highlights`, `service_offered`, `is_featured`, `is_home`, `status`, `service_added_date`, `service_updated_date`, `service_viewed`, `service_alt`, `text_colour`, `description_text_colour`) VALUES
(21, 15, 1, '15', '', NULL, NULL, NULL, NULL, 'electro', NULL, 0.00, 'dulux-powders-electro_product-solutions-brochure-web-08_19.pdf', 0.00, '<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">The surest way to navigate change is by being true to who you are. We&rsquo;ve always been an unusual combination of business smarts and stunningly creativity, of right-brain and left-brain thinking, of rule makers and rule breakers. It&rsquo;s precisely this kind of combination our clients need right now, and it happened by design in JBA Agency.</span></span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Advertising design is the art of persuasion embedded in storytelling to capture an audience&rsquo;s attention and inspire them to take the desired action by us.</span></span></strong></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:01:23', NULL, 0, '', '#ffffff', '#FFFFFF'),
(54, 37, 1, '37', '', NULL, NULL, NULL, NULL, 'videography-and-photography', '649919', 0.00, NULL, 0.00, '<p align=\"left\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"color:#FFD700;\"><span style=\"font-size:28px;\"><strong>Movies</strong></span></span></span></span></p>\r\n\r\n<p align=\"left\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">We are experienced in filming all types of events, including conferences, seminars, live stage shows, concerts, exhibitions, and fashion shows. Whether your project is a documentary, any kind of video, or a TV Commercial, we have the tools and the talent you need. JBA Agency is ready to break the barriers of imagination.</span></span></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:12:00', NULL, 0, '', '#ffffff', '#FFFFFF'),
(29, 27, 1, '27', '', NULL, NULL, NULL, NULL, 'pr16', NULL, 0.00, 'tds_pr16_outdoor-satin-matt-polyester.pdf', 0.00, '<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Our aim is to produce ideal architectural models, above all&nbsp;they are indispensable for architects and construction companies, using all the possibilities of technological development to achieve this goal.</span></span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">The architecture models in other words that are output in JBA Agency, therefore, conform to our principles with respect to on-time delivery and customer satisfaction, giving your projects life and realism.</span></span></strong></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:15:17', NULL, 0, '', '#ffffff', '#FFFFFF'),
(55, 14, 2, '14', '', NULL, NULL, NULL, NULL, 'exhibition', '798465', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">We provide audio equipment, lighting equipment, video screens, videography, and photography for events and exhibitions.</span></span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">We take pride in the enormous amount of planning on our part, and our production/rental professionals at JBA Agency are up for the challenge. Our goal is to perform to the highest expectations of our clients. JBA Agency has some of the most experienced professionals in the industry on our roster. Event production starts in-office, where our event planners and production salespeople begin the process long before the event date.</span></span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">JBA areas of exhibition expertise, execution, and production include exhibition and conference planning, budgeting, marketing, sales, floor management, administration, and reporting.</span></span></strong></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 07:59:44', NULL, 0, '', '#000000', '#FFFFFF'),
(56, 15, 2, '15', '', NULL, NULL, NULL, NULL, 'design', '447991', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">The surest way to navigate change is by being true to who you are. We&rsquo;ve always been an unusual combination of business smarts and stunningly creativity, of right-brain and left-brain thinking, of rule makers and rule breakers. It&rsquo;s precisely this kind of combination our clients need right now, and it happened by design in JBA Agency.</span></span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Advertising design is the art of persuasion embedded in storytelling to capture an audience&rsquo;s attention and inspire them to take the desired action by us.</span></span></strong></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:01:47', NULL, 0, '', '#000000', '#FFFFFF'),
(57, 26, 2, '26', '', NULL, NULL, NULL, NULL, 'application', '355459', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><span style=\"color:#FFD700;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:28px;\"><strong>Mobile Applications</strong></span></span></span></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">To create a fast and smooth app for any platform, we also deliver products with an outsourcing app development model. This model gives a reasonable solution to control the budget and other recourses.</span></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Our team connects conventional essential technologies and architecture with the unique logic of the app. Combined with a polished design and UI/UX an app becomes the one that reaches the goals of our customers and wins the hearts of users.</span></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">JBA Agency delivers fully functional apps customizable for every product for various platforms; iOS, Android, and Windows. Combine this with our creative talent.</span></span></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:10:14', NULL, 0, '', '#000000', '#FFFFFF'),
(58, 27, 2, '27', '', NULL, NULL, NULL, NULL, 'fit-out', '445820', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\">Above all JBA Agency specializes in providing elegant and stunning interior and exterior design.</span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\">for both residential and commercial projects. We have experienced designers with various international styles.</span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\">Our team consultants are experts in the field of luxury villas.</span></strong><strong><span style=\"font-size:22px;\"> In addition, modern modeling for home and home decoration shops</span></strong><strong><span style=\"font-size:22px;\"> offers detailed and specific concepts to make your dream home a reality.</span></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><span style=\"font-size:22px;\">We are working to represent the best Turkish and European companies and factories operating in the fields of equipment and supplies for hotels, restaurants, hospitals, apartments, and luxury villas with the latest models and materials at reasonable prices.</span></strong></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:17:44', NULL, 0, '', '#000000', '#FFFFFF'),
(59, 37, 2, '37', '', NULL, NULL, NULL, NULL, 'photography', '591132', 0.00, NULL, 0.00, '<p align=\"left\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"color:#FFD700;\"><span style=\"font-size:28px;\"><strong>Photography</strong></span></span></span></span></p>\r\n\r\n<p><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">How cool is it to get the right images to make your content/product/service look effective and stunning? We believe being artistic and creative go hand in hand, and therefore, we are prized for our talents in photography as well.</span></span></p>\r\n\r\n<p><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Inspiring and effective, the JBA Agency portfolio has imagery to suit the serious corporate campaign to the fun-loving but responsive ones too.</span></span></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:13:32', NULL, 0, '', '#000000', '#FFFFFF'),
(40, 26, 1, '26', '', NULL, NULL, NULL, NULL, 'event-4', '315802', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><span style=\"color:#FFD700;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:28px;\"><strong>Websites</strong></span></span></span></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">With ears of experience in this arena, JBA Agency has the full suite of tools to build a responsive, structured website that is creatively fulfilling and can be integrated with a payment gateway along with a host of other features.</span></span></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 08:10:25', NULL, 0, '', '#ffffff', '#FFFFFF'),
(41, 14, 1, '14', '', NULL, NULL, NULL, NULL, 'redox-triplex-2', '958975', 0.00, NULL, 0.00, '<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><strong><span style=\"font-family:arial,helvetica,sans-serif;\">We provide audio equipment, lighting equipment, video screens, videography, and photography for events and exhibitions.</span></strong></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><strong><span style=\"font-family:arial,helvetica,sans-serif;\">We take pride in the enormous amount of planning on our part, and our production/rental professionals at JBA Agency are up for the challenge. Our goal is to perform to the highest expectations of our clients. JBA Agency has some of the most experienced professionals in the industry on our roster. Event production starts in-office, where our event planners and production salespeople begin the process long before the event date.</span></strong></span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:22px;\"><strong><span style=\"font-family:arial,helvetica,sans-serif;\">JBA areas of exhibition expertise, execution, and production include exhibition and conference planning, budgeting, marketing, sales, floor management, administration, and reporting.</span></strong></span></p>\r\n', NULL, NULL, 0, 0, '1', '2023-01-16 07:57:58', NULL, 0, '', '#ffffff', '#000000');

-- --------------------------------------------------------

--
-- Table structure for table `wl_services_city`
--

CREATE TABLE `wl_services_city` (
  `id` bigint(20) NOT NULL,
  `services_id` bigint(20) NOT NULL,
  `city_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_services_city`
--

INSERT INTO `wl_services_city` (`id`, `services_id`, `city_id`) VALUES
(11, 33, 24),
(12, 33, 26),
(18, 32, 29),
(19, 32, 28),
(24, 31, 29),
(25, 31, 28),
(26, 31, 25),
(27, 35, 29);

-- --------------------------------------------------------

--
-- Table structure for table `wl_services_media`
--

CREATE TABLE `wl_services_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `media_type` enum('photo','video','pdf') CHARACTER SET utf8 NOT NULL DEFAULT 'photo',
  `media` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_default` enum('Y','N') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `media_date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_services_media`
--

INSERT INTO `wl_services_media` (`id`, `service_id`, `media_type`, `media`, `is_default`, `media_date_added`) VALUES
(1, 2, 'photo', 'ser-img13.jpg', 'Y', '2020-04-13 23:39:49'),
(2, 2, 'photo', 'ser-img7.jpg', 'N', '2020-04-10 08:17:38'),
(3, 3, 'photo', 'ser-img11.jpg', 'Y', '2020-04-13 23:39:32'),
(4, 3, 'photo', 'ser-img6.jpg', 'N', '2020-04-10 08:18:04'),
(5, 3, 'photo', 'ser-img61.jpg', 'N', '2020-04-10 08:18:04'),
(6, 4, 'photo', 'ser-img10.jpg', 'Y', '2020-04-13 23:39:14'),
(7, 4, 'photo', 'dl-pro2.jpg', 'N', '2020-04-10 08:21:31'),
(8, 4, 'photo', 'dl-pro3.jpg', 'N', '2020-04-10 08:21:31'),
(9, 4, 'photo', 'dl-pro4.jpg', 'N', '2020-04-10 08:21:31'),
(48, 5, 'photo', 'screenshot_4032.png', 'N', '2021-09-29 12:42:14'),
(47, 5, 'photo', 'screenshot_4031.png', 'N', '2021-09-29 12:42:14'),
(13, 8, 'photo', 'pro-pic-b1.jpg', 'Y', '2021-09-20 11:52:29'),
(14, 8, 'photo', 'pro-pic-b2.jpg', 'N', '2021-09-20 11:52:29'),
(15, 8, 'photo', 'pro-pic-b3.jpg', 'N', '2021-09-20 11:52:29'),
(16, 8, 'photo', 'pro-pic-b4.jpg', 'N', '2021-09-20 11:52:29'),
(17, 9, 'photo', 'pro-pic-b31.jpg', 'Y', '2021-09-20 11:53:16'),
(44, 11, 'photo', 'istock-825214258-800491.jpg', 'N', '2021-10-21 00:52:14'),
(45, 11, 'photo', 'istock-534497432-r.jpg', 'N', '2021-10-20 22:56:27'),
(49, 5, 'photo', 'screenshot_4033.png', 'N', '2021-09-29 12:42:14'),
(22, 10, 'photo', 'pro-pic-b42.jpg', 'N', '2021-09-20 12:32:32'),
(23, 10, 'photo', 'pro-pic-b33.jpg', 'N', '2021-09-20 12:32:32'),
(24, 10, 'photo', 'pro-pic-b12.jpg', 'N', '2021-09-20 12:32:32'),
(25, 10, 'photo', 'eurolacke-thermoplastic-powder-coatings-abcite-plascoat-coa282-1r.jpg', 'N', '2021-10-20 23:24:53'),
(26, 7, 'photo', 'pro-pic-b34.jpg', 'N', '2021-09-20 12:35:12'),
(27, 7, 'photo', 'pro-pic-b13.jpg', 'N', '2021-09-20 12:35:12'),
(28, 7, 'photo', 'pro-pic-b44.jpg', 'N', '2021-09-20 12:35:12'),
(29, 7, 'photo', 'pro-pic-b35.jpg', 'N', '2021-09-20 12:35:12'),
(30, 6, 'photo', 'pro-pic-b45.jpg', 'N', '2021-09-20 12:36:55'),
(31, 6, 'photo', 'pro-pic-b36.jpg', 'N', '2021-09-20 12:36:55'),
(32, 6, 'photo', 'pro-pic-b14.jpg', 'N', '2021-09-20 12:36:55'),
(33, 6, 'photo', 'pro-pic-b46.jpg', 'N', '2021-09-20 12:36:55'),
(46, 5, 'photo', 'screenshot_403.png', 'N', '2021-09-29 12:42:14'),
(35, 12, 'photo', 'creative31.jpg', 'Y', '2022-05-29 11:17:03'),
(43, 11, 'photo', 'istock-990462324r.jpg', 'N', '2021-10-20 23:07:06'),
(42, 11, 'photo', 'istock-1222016756-800491.jpg', 'N', '2021-10-21 00:48:02'),
(50, 13, 'photo', 'creative21.jpg', 'Y', '2022-05-29 11:16:49'),
(54, 14, 'photo', 'creative11.jpg', 'Y', '2022-05-29 11:16:38'),
(59, 15, 'photo', 'screenshot_4061.png', 'N', '2021-09-29 13:23:52'),
(60, 15, 'photo', 'screenshot_4062.png', 'N', '2021-09-29 13:23:52'),
(61, 15, 'photo', 'screenshot_4063.png', 'N', '2021-09-29 13:23:52'),
(62, 16, 'photo', 'creative.jpg', 'Y', '2022-05-29 11:15:55'),
(66, 17, 'photo', 'creative3.jpg', 'Y', '2022-05-29 11:15:41'),
(77, 18, 'photo', 'brands.jpg', 'Y', '2022-05-29 11:22:19'),
(78, 19, 'photo', 'brand_4.jpg', 'Y', '2022-05-29 11:22:03'),
(141, 41, 'photo', 'jba-service-event-copy.jpg', 'Y', '2022-10-15 15:50:23'),
(142, 42, 'photo', '9c841444ec7a198e3fc2da32077ea95b.jpg', 'Y', '2022-07-01 11:01:37'),
(82, 20, 'photo', 'brand_3.jpg', 'Y', '2022-05-29 11:21:50'),
(86, 21, 'photo', 'jba-service-design-copy.jpg', 'Y', '2022-10-15 15:44:48'),
(90, 22, 'photo', 'resicoat.jpg', 'N', '2021-09-29 14:53:56'),
(91, 23, 'photo', 'events1.jpg', 'Y', '2022-05-29 11:17:38'),
(95, 24, 'photo', 'events_21.jpg', 'Y', '2022-05-29 11:17:50'),
(99, 26, 'photo', 'fffffffffff.jpg', 'Y', '2022-07-01 08:03:59'),
(100, 25, 'photo', 'events_41.jpg', 'Y', '2022-05-29 11:18:07'),
(104, 27, 'photo', 'healthcare.jpg', 'Y', '2022-05-29 11:18:23'),
(108, 28, 'photo', 'whatsapp-image-2021-08-07-at-10.17.42-pm.jpeg', 'Y', '2022-09-05 19:54:29'),
(112, 29, 'photo', 'jba-service-interior-copy1.jpg', 'Y', '2022-10-15 15:44:25'),
(116, 30, 'photo', 'web123.jpg', 'Y', '2022-07-01 08:54:38'),
(123, 31, 'photo', 'dl-pro2.jpg', 'Y', '2021-12-07 06:07:37'),
(124, 31, 'photo', 'dl-pro3.jpg', 'N', '2021-12-07 06:07:37'),
(125, 31, 'photo', 'dl-pro4.jpg', 'N', '2021-12-07 06:07:37'),
(126, 31, 'photo', 'dm-pro1.jpg', 'N', '2021-12-07 06:07:37'),
(127, 32, 'photo', 'dm-pro2.jpg', 'Y', '2021-12-07 06:08:17'),
(128, 32, 'photo', 'dm-pro21.jpg', 'N', '2021-12-07 06:08:17'),
(129, 32, 'photo', 'dm-pro4.jpg', 'N', '2021-12-07 06:08:17'),
(130, 33, 'photo', 'cate_img1.jpg', 'Y', '2021-12-07 09:37:48'),
(131, 33, 'photo', 'serv-img1.png', 'N', '2021-12-07 09:37:48'),
(132, 35, 'photo', 'cate_img3.jpg', 'Y', '2021-12-07 09:43:17'),
(133, 36, 'photo', 'stock-photo-black-colour-pencil-with-outline-rocket-on-white-paper-background-creativity-inspiration-ideas-19319827191.jpg', 'Y', '2022-06-18 12:47:37'),
(134, 36, 'photo', 'proj-pic2.jpg', 'N', '2022-01-24 05:01:08'),
(135, 36, 'photo', 'proj-pic3.jpg', 'N', '2022-01-24 05:01:08'),
(136, 36, 'photo', 'proj-pic4.jpg', 'N', '2022-01-24 05:01:08'),
(137, 37, 'photo', 'events.jpg', 'Y', '2022-05-28 07:04:00'),
(138, 38, 'photo', 'events_2.jpg', 'Y', '2022-05-28 07:03:42'),
(139, 39, 'photo', 'events_3.jpg', 'Y', '2022-05-28 07:07:47'),
(140, 40, 'photo', 'jba-service-web-copy1.jpg', 'Y', '2023-01-16 08:05:56'),
(143, 43, 'photo', 'advertising.png', 'Y', '2022-08-30 22:28:51'),
(144, 44, 'photo', 'advertising1.png', 'Y', '2022-08-30 22:29:29'),
(145, 45, 'photo', 'brands.gif', 'Y', '2022-08-30 22:35:30'),
(146, 46, 'photo', 'advertising2.png', 'Y', '2022-08-31 16:43:06'),
(147, 47, 'photo', 'charity.jpg', 'Y', '2022-08-31 16:43:28'),
(148, 48, 'photo', 'recharge.png', 'Y', '2022-08-31 16:43:47'),
(149, 49, 'photo', 'advertising3.png', 'Y', '2022-08-31 17:09:38'),
(150, 50, 'photo', '45a260724f7ebf164b893eed1128d38d.jpg', 'Y', '2022-09-03 15:36:29'),
(151, 51, 'photo', '9c841444ec7a198e3fc2da32077ea95b1.jpg', 'Y', '2022-09-03 15:36:59'),
(152, 52, 'photo', 'bulk-pack-3.jpg', 'Y', '2022-09-07 05:29:44'),
(153, 53, 'photo', '1020x7072.jpg', 'Y', '2022-09-07 05:47:26'),
(154, 54, 'photo', 'jba-videography-copy.jpg', 'Y', '2022-10-15 11:43:25'),
(155, 55, 'photo', 'jba-test.jpg', 'Y', '2022-10-15 16:19:01'),
(156, 56, 'photo', 'jba-test1.jpg', 'Y', '2022-10-15 16:19:44'),
(157, 57, 'photo', 'jba-service-web-copy1.jpg', 'Y', '2023-01-16 08:07:04'),
(158, 58, 'photo', 'jba-service-interior-copy.jpg', 'Y', '2023-01-16 08:16:25'),
(159, 59, 'photo', 'jba-videography-copy1.jpg', 'Y', '2023-01-16 08:13:32'),
(160, 60, 'photo', 'rubber-world2.jpg', 'Y', '2022-12-02 10:39:13');

-- --------------------------------------------------------

--
-- Table structure for table `wl_service_categories`
--

CREATE TABLE `wl_service_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_description` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_footer` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `is_home` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `is_menu` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `is_fixed` tinyint(4) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive,2=deleted',
  `meta_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_alt` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_service_categories`
--

INSERT INTO `wl_service_categories` (`category_id`, `category_name`, `friendly_url`, `category_image`, `category_description`, `parent_id`, `is_footer`, `is_home`, `is_menu`, `is_fixed`, `sort_order`, `date_added`, `date_modified`, `status`, `meta_title`, `meta_keywords`, `meta_description`, `category_alt`) VALUES
(14, 'Events & Exhibitions Management', 'events', 'cate_img2.jpg', NULL, 0, '0', '0', '0', 1, 1, '2021-01-23 11:57:25', '2022-11-06 04:32:46', '1', NULL, NULL, NULL, 'Events & Exhibitions Management'),
(15, 'Branding & Design', 'advertising', 'cate_img3.jpg', NULL, 0, '0', '0', '0', 1, 2, '2021-01-23 11:57:42', '2022-10-15 13:32:17', '1', NULL, NULL, NULL, 'Branding & Design'),
(26, 'Websites & Applications', 'events', '', '', 0, '0', '0', '0', 1, 4, '2022-05-28 05:57:06', '2022-08-19 09:29:05', '1', NULL, NULL, NULL, 'Websites & Applications'),
(27, 'Interior Design and Fit Out', 'interior-design-and-fit-out', '', NULL, 0, '0', '0', '0', 1, 5, '2022-05-28 05:57:29', '2022-11-06 04:34:00', '1', NULL, NULL, NULL, 'Interior Design and Fit Out'),
(37, 'Media Advertisements, Movies, and Photography', 'videography-and-photography', NULL, NULL, 0, '0', '0', '0', 1, 6, '2022-10-15 10:27:21', NULL, '1', NULL, NULL, NULL, 'Media Advertisements, Movies, and Photography');

-- --------------------------------------------------------

--
-- Table structure for table `wl_sessions`
--

CREATE TABLE `wl_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wl_sessions`
--

INSERT INTO `wl_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('eb02609d6bbdc87acd8b5eb183df29a66f771de2', '212.70.113.201', 1672309917, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393834333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('770f3807f1371829feda63c2abb80ab595382355', '212.70.113.201', 1672309946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393636323b),
('0044563ab82bb6e2f51ffa25be4bde2539fe1207', '66.249.75.56', 1672309316, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393331363b),
('f31c645dd6bd6ecd667d5b00fd81e8378978af11', '212.70.113.201', 1672309843, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393834333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b737563636573737c733a33373a225265636f726420686173206265656e2075706461746564207375636365737366756c6c792e223b5f5f63695f766172737c613a313a7b733a373a2273756363657373223b733a333a226f6c64223b7d),
('57ef6ab99e3c35d8b5c2f8beabe7529a5fbb5922', '212.70.113.201', 1672309662, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393636323b),
('96f7dc39b77d006dc050b0862c593f794bd783f4', '212.70.113.201', 1672309525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393532353b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('93d64b87033ba61b198a4b64105ea320ff45b7fe', '212.70.113.201', 1672309302, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393330323b),
('78ed911cdde9829039f63b66baf43ad170e78456', '35.208.251.26', 1672308823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330383832333b),
('c30cbcbdbd0577ede9604d60150eff800d16c1ce', '66.249.75.58', 1672307561, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330373536313b),
('59f7846391c29398e0ea6270557f7ae2295ecb19', '66.249.75.60', 1672307561, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330373536313b),
('68464cb63114677ae3902564c94ed5ef7ec2a6b2', '212.70.113.201', 1672309139, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330393133393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('e86f8bf7583be0b415ee367083035b5515887387', '40.77.167.35', 1672303625, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330333632353b),
('a471ddd067b652ab3465a975753500dc1a6fc0b2', '207.46.13.121', 1672303609, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323330333630393b),
('561f645043ecefa5261e11fafaa33e138cf8f166', '66.249.75.60', 1672299738, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239393733383b),
('07d7c3bd0aa6d1b9047d8f3835e79b989e6cccc4', '66.249.75.62', 1672299730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239393732353b),
('110571a3b3fd0ff2528395c4cc1e38f46bf135d9', '66.249.75.62', 1672299725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239393732353b),
('e7b5659092cb86ea7371acf74089cc10ddd2833a', '66.249.75.60', 1672294976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239343937363b),
('6c695539f072c51a538970aaf1fd2c551494a4f6', '66.249.75.62', 1672295455, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239353435353b),
('8e7f530c1fb34924a6efd8a095f43cf08c9ebf17', '66.249.75.60', 1672297628, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239373632383b),
('d4bc30b3cd9e278ffe3f8fb43b0e18b3d6b2f0f1', '66.249.75.60', 1672295455, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239353435353b),
('e998a5b64edb7e780c24ea6efbd5664e0d8c0399', '66.249.75.60', 1672293295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323239333239353b),
('c622c14421acee52668886e00c0104569e569762', '66.249.75.60', 1672489933, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323438393933333b),
('244bf56b0470a58bcf28924ed812687230afb735', '66.249.75.60', 1672479725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323437393732353b),
('22cd4dcb9382ccbaf8e89c5580bbe6994227a46b', '66.249.75.56', 1672479725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323437393732353b),
('2f8567f4754eea478de85e91a538ab59fe3bd1ee', '51.254.199.11', 1672484294, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323438343239343b),
('8e32d9cd49c18d0e87989aee170a9cec644dbe62', '66.249.75.58', 1672487234, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323438373233343b),
('c8ffbcb6f6d072c7000e616cc2a249f354623079', '122.161.50.64', 1672489692, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323438393638383b),
('fb9aef4b1d448ff392a69827b1e1efcf97a84e28', '66.249.75.58', 1672474242, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323437343234323b),
('fb3671a2860a0b394f18e57bcc47ab5e7c1676b4', '66.249.75.60', 1672464450, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436343435303b),
('173793f3d43c6c0f3d7cc667346e543b229ebb13', '66.249.75.56', 1672468386, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436383338363b),
('b17ac301a0b8f799d9d2b92ba414f9bb60fd1a0c', '66.249.73.179', 1672464457, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436343435373b),
('9816023b7fdef0e7f79bf05450676eaf5542bf55', '66.249.75.58', 1672463474, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436333437343b),
('bbc63cd1a8515c31ca43764054b85c4320106375', '209.141.35.128', 1672461384, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436313338343b),
('c48148f5404037c495ed5aca6a7fe72fcc7dea40', '66.249.75.60', 1672458536, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323435383533363b),
('d9835f45ab717df9c34be677bde138a831371306', '209.141.49.169', 1672460766, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436303736363b),
('05484b59711279628d2722f6d44a195ce1521684', '209.141.49.169', 1672460768, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323436303736373b),
('8feec740d2c3d39510fcd64431e84ac5295d6f6b', '66.249.75.56', 1672458532, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323435383533323b),
('d67a17abe78772c101b213a6636772cad749aaaa', '54.36.148.224', 1672443113, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323434333131333b),
('9d1c37c3611edb1d1d7bab5081ed98ac127309e7', '54.36.148.186', 1672443112, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323434333131323b),
('21b370c4221925f03bcc8042fa07541b12d1e96d', '66.249.75.58', 1672441257, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323434313235373b),
('4b59fd4476203f380eaae595c14dad5e05db901d', '66.249.75.60', 1672436148, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433363134383b),
('19b0bdeee15015a06e8e1c9e1c14547aae362685', '193.231.40.159', 1672430979, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303937393b),
('ef6630fb57e1779a34339e1bd5b21a728d9776f6', '193.231.40.159', 1672430925, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303932353b),
('b1fbb27f33c58404dc5d2a2e0d28ec6e32de0876', '193.231.40.159', 1672430910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303931303b),
('1269a64d64925e3c5ff70303b672ea90e345021d', '193.231.40.159', 1672430840, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303834303b),
('83e017699a7f6daad939cb44ac0be52990918e89', '143.198.51.227', 1672430656, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303635363b),
('c91da5eee36ee165478b6f7409a8d24abf188209', '159.89.192.232', 1672430580, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303538303b),
('91f4face50d0dcff158d8ac9c64e244bf17a36e8', '5.75.234.96', 1672430376, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323433303337363b),
('d591959faff3096e6c1385bb4a5a5efa906da9a0', '66.249.75.56', 1672428072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323432383037323b),
('a35300b08d5fcfad9a5d0e3bd6268e7c3bbe558f', '173.213.85.232', 1672422940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323432323934303b),
('26156086806d784df90603400396fe3bef1831b4', '66.249.75.60', 1672421287, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323432313238373b),
('6464114ce5f86b71b9e30e14a6bee77570b6703b', '66.249.75.58', 1672419475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323431393437353b),
('81a4469dac7c5fcdd0f79909bb6c6cf36820a85d', '87.236.176.116', 1672414291, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323431343239313b),
('2673b8f0aa9bf852ddd331214c7106b88f903b72', '87.236.176.242', 1672414290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323431343239303b),
('116c92e1ba414e16b781cba3a65bf7da399915b4', '66.249.75.56', 1672409372, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323430393337323b),
('a09eead75d3429b1745e3773bae513ed3508822a', '66.249.75.58', 1672409372, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323430393337323b),
('4add9e301088d88d7c6e317553ac9c74eaa1c594', '66.249.75.56', 1672400184, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323430303138343b),
('58915f5d7b31d595d24cb4cc51c922ecdbf88fb8', '66.249.75.58', 1672394681, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323339343638313b),
('f9913eb399c56fb9b5a6da8422015f17c63126b8', '66.249.75.60', 1672382994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323338323939343b),
('7696624b6bdc35691d402a507b9d924fff4f7c12', '173.213.85.232', 1672386803, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323338363830333b),
('9189ed8541a02b3f0b585218d47ccc5424ee6597', '66.249.75.56', 1672388321, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323338383332313b),
('3cf53ddc6a122f3693b6a00fbfe7ba31b15822aa', '66.249.75.60', 1672378218, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323337383231383b),
('fdd81c3fcfba2b190cd371c5685db5b00bacb491', '66.249.75.58', 1672375804, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323337353830343b),
('63c718e15b2a061ec81731666df44da00c401ee8', '66.249.75.56', 1672373634, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323337333633343b),
('df13871258cc4193025f8d6a7a569c804e2dacb3', '66.249.75.60', 1672358121, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323335383132313b),
('4017154a045fb6fcdb65a2856777442933dd49f9', '207.46.13.106', 1672357816, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323335373831363b),
('09d8df2381a96705a4da9bc4e0770aa52c0951c7', '157.55.39.70', 1672357791, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323335373739313b),
('b96a98ad739d76a416e90a38237c7fc8825de6f9', '157.55.39.17', 1672357787, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323335373738363b),
('cbe5ec7fa92c0a9a4d4faedf2d464f8ec26a24c4', '173.213.85.232', 1672350543, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323335303534333b),
('98fd173c58d641b18df305f551d2acb19fc2cf97', '54.244.41.161', 1672348949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323334383934383b),
('ccc0f6dd0a0ec627ba1485d0080ad5195cbcd01b', '35.91.135.214', 1672348947, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323334383934373b),
('7685d3fc968faa289414ede6086fbdf9a43dab23', '66.249.75.58', 1672339318, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323333393331383b),
('288b28fef92db70a86cc8722a05efcdf6a326e22', '66.249.75.56', 1672333915, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323333333931353b),
('9f91c437857c692265972c343459b006c6345098', '66.249.75.56', 1672329262, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332393236323b),
('a310a4e7892390c5e6ed4ce626e02d658595068c', '137.226.113.44', 1672326373, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332363337333b),
('9c998664ee2cc48a8725b6072b7e83114092a33f', '66.115.130.58', 1672324449, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332343434393b),
('52d699bb4c90ccca15090700a1155ce55b030b35', '66.249.75.60', 1672322824, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332323832343b),
('13b9b6acb50755151f6d42e47c61e585a91d6d76', '151.80.67.229', 1672322597, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332323539373b),
('2f12bdf12666e74f9f22ef16c268746752cae678', '151.80.67.229', 1672322471, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332323437313b),
('d63ddd48133a72b3161f3365a92d838dcae0e3b8', '151.80.67.229', 1672322456, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332323435363b),
('d82b63f0d352a4464e1a8806edd23c248d31d146', '151.80.67.229', 1672322456, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323332323435363b),
('8841d31fedc98a2cfb558ba46806534dd9551b8c', '95.68.42.225', 1672318531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331383533313b),
('70f985488cb266336907e2b78c8f3767fcd252c5', '59.25.214.142', 1672318533, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331383533333b),
('27eaf59b511490a9b674001659b3dc2758573d5b', '154.84.132.7', 1672314303, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331343330323b),
('ffcc7160cbb33505b68a869b52e68cf990571aa5', '173.213.85.232', 1672314303, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331343330333b),
('c9a4e7daf108233c1bbe04ad32b4a7e9caf3bebb', '66.249.75.58', 1672316612, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331363631323b),
('3acc4a7543f2c4c241abe5c25a39d4501dca747e', '173.213.85.232', 1672314483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323331343438333b),
('jdbsmgfpn64g0hh273bl5dth4frh85m1', '::1', 1672730758, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323733303735383b),
('us1gdth77hqrkhnqsmllmerc6dmci8j7', '::1', 1672730791, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323733303735383b),
('kt4sdl31i5vsrn8nhpqskfepr1iq0ihf', '::1', 1672751542, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323735313335323b),
('ktke5pu7103m6uq70jh5llprtb8sutpb', '::1', 1672827023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832373032333b),
('bm2rs0os73d2ip2kn41qmr1isalr39ej', '::1', 1672827635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832373633353b),
('diko0pcapiinlb7p4uj7332k4a1bp9pm', '::1', 1672827986, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832373938363b),
('ueqtq2298qh6805vufcn5ltoqqoh8q4g', '::1', 1672828289, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832383238393b),
('ac927s4vv8ivqpk2ltdoks1nct8uc7i4', '::1', 1672828590, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832383539303b),
('r9ctcetunm6kba2i7m8i8hmd8d0fep73', '::1', 1672829017, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832393031373b),
('nblf2nn2lfj1v069n04fdcd1n6ljra67', '::1', 1672829220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832393031373b),
('3usqgu7h5ofnje1c8loi6b7i38m5do9c', '::1', 1672829778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323832393737383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('61edpalb3335d3ouhcsjbud4u79ro4im', '::1', 1672830444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323833303434343b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('u32bvjggnc1gvcic0tlef3kdctici138', '::1', 1672830798, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323833303739383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('1ll67706mhp9edrs31o51mvl5mng9lp4', '::1', 1672830810, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323833303739383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('o25j7igtb0rs1m41fatttmlonl89e9sp', '127.0.0.1', 1672830923, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323833303932303b),
('8guvqbuekiksdcoa1h661sm7kjgr10lf', '::1', 1672898720, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839383732303b),
('u9d2s3d473na7mf77kfq83qha5pp35k3', '127.0.0.1', 1672896464, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839363436343b),
('3topl82getssd9s1tjfe1nr42tgjsb11', '127.0.0.1', 1672895895, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839353839353b),
('ahl7i170ftnenorsffiv1ltp66gpl84v', '127.0.0.1', 1672897186, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839373138363b),
('9uckqsibt9acivh4mpn1p2b1vohv2vot', '127.0.0.1', 1672897583, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839373538333b),
('1tkbrbdq5fjl462hjf7j28mr85gdunv1', '127.0.0.1', 1672897903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839373930333b),
('l60c5memvmc0jmvdqnrihbnags7fo30g', '127.0.0.1', 1672898547, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839383534373b),
('kkijcond024n11ki43k9tsl1qdufr8gp', '127.0.0.1', 1672899338, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839393333383b),
('7gsaa90o65bs6nubll0q0235e174k21r', '::1', 1672899099, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839393039393b),
('lngsqla879jqmfsi09prc3t6c3dn9kt1', '::1', 1672901836, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323930313833363b),
('loth6r0hubmnkcna75pveo4vf2ums2gr', '127.0.0.1', 1672899677, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839393637373b),
('ee6dq8bmv7adolbj0lnfs0s8l2q2g5v7', '127.0.0.1', 1672900475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323930303437353b),
('vect8ugkbg8d15lm4pi72e25ekskvlfo', '127.0.0.1', 1672899780, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323839393738303b),
('n3q55tfa6c0nqir428mqoffeok45d04g', '127.0.0.1', 1672916858, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931363835383b),
('5nmmhevp9qencbgd3n1bnmvkt0krr51t', '::1', 1672902647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323930323634373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('idha3ftd8lpdurj4hikt9bth5tfimqfl', '::1', 1672906378, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323930363337383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('2gje99p07rk7hdi2924hu3h0pklbo01f', '::1', 1672906861, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323930363836313b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('enptbn445u8352360aal9vs274pf6od5', '::1', 1672910467, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931303436373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('fe28sfrfkm6vm67m5l0plplo01ca3uv9', '::1', 1672914315, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931343331353b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('l9fm19us8ml5ora1bp5rujl53k367moa', '::1', 1672916133, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931363133333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('8lctit56g7ip3l0ss2sta6gsip0pcdhq', '::1', 1672916455, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931363435353b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('7r80egdt9v69pibesf8tjcidllmciivq', '::1', 1672916764, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931363736343b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('nrs45v8d2ghrv8sqom20vsl4ea3v5n1r', '::1', 1672918369, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931383336393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('9nm6a358hgolgu97b3rsj6a38ot27nrk', '127.0.0.1', 1672922150, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932323135303b),
('rme8d61cgcdcpq7df8lfth4d9n25ikoe', '::1', 1672919403, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931393430333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('qaa2a51sofu3ndb35qpdo5fe6a2ne82h', '::1', 1672919777, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323931393737373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('cdloouohct4dbdkb7talhb2d7di5odqf', '::1', 1672921461, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932313436313b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('oui6qtj3t7ldet2ifjrjim56olesbh6t', '::1', 1672921779, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932313737393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('2bgsj7fj946cp2c25kn512ksqb70s3te', '::1', 1672922306, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932323330363b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('l7te85ce6keuhik629277q42dt2hjvdd', '127.0.0.1', 1672922151, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932323135303b),
('1ltqdmk5iqhlt6efov0s0b8e66scfr44', '::1', 1672922903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932323930333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('kglijm2l7hb9j4k7a97flu88aeut1glm', '::1', 1672922904, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323932323930333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('bc6f78ed94f5107ea4138b2e2c4116ff920a5bfa', '106.214.113.170', 1672981120, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938303835353b),
('82a66d8d456b379a03919c86e23571eb1928fb69', '52.114.32.28', 1672981143, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938313134333b),
('292c93ce55543bd60519c8e2e1a576a7d66494a5', '106.214.113.170', 1672981352, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938313330393b),
('1a6f99cc284ece2870e33894578185e8459c4321', '106.214.113.170', 1672984448, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938343434383b),
('a2c171fb841e9fc1ea3290b6dbb39abad7f7a603', '106.214.113.170', 1672984870, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938343837303b),
('83dcdf3b45b126062dd499cb15834a34cc9f88ba', '106.214.113.170', 1672985279, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938353237393b),
('113bb54c4dddb1b403e079d65c3d581f045b73b7', '106.214.113.170', 1672985851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938353835313b),
('14dded120fd22a9c56cfa240f3166e63b2fbee08', '106.214.113.170', 1672986775, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938363737343b),
('1a9515fa111d832db58c39360c24327ba7fe42d8', '106.214.113.170', 1672987120, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938373132303b),
('4be69d1cef5fd1b9501ae996a8da29dca28282e6', '106.214.113.170', 1672987793, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938373739333b),
('d0c34fc26cc1625ca0dc289f13ba8f66c49cf25d', '106.214.113.170', 1672989648, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323938393634383b),
('f204f116d154c2e2424df56b0e6f6cbd62606301', '106.214.113.170', 1672997138, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323939373133383b),
('dc469d8d1ef358e6e3f4b87cc86f465ca3684342', '106.214.113.170', 1672999565, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637323939393536353b),
('64d40b2044e916415d1c67a989671755696324df', '106.214.113.170', 1673010940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333031303934303b),
('240f63c3daed680bb9a7ff76267e89e3a0c0a83f', '106.214.113.170', 1673013073, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333031333037333b),
('42f539a62876e48610076a7615e9a64804ebb028', '106.214.113.170', 1673013904, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333031333930343b),
('363e6baa0181136060d703fcb404f0f733233901', '106.214.113.170', 1673013912, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333031333930343b),
('38377825861c9b5a89946e2ae016f70d9356622e', '87.236.176.50', 1673020956, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333032303935363b),
('45ae6f4c44636542fd126db83c1fbb6d94d471b8', '87.236.176.54', 1673020958, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333032303935383b),
('ef1195ee9b4a66bffa721287ce531b9e1d4c70ce', '157.32.123.207', 1673044693, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333034343639333b),
('4e9616d07efb440a145c447b7f4dafb12c4f672b', '183.129.153.157', 1673059446, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333035393434363b),
('6db0a94e7e74416075ed2b0418714fb340435ecc', '78.101.180.162', 1673070132, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333037303133323b),
('066f8349d9980a2aef4b4cc4f3a20f0b3f1a26ac', '87.236.176.178', 1673062437, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333036323433373b),
('c70785a7ab4524ead6edf7644af66605dddbe2fa', '87.236.176.90', 1673062439, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333036323433393b),
('11eb983829bb51337dc541aede67f06d5bf9ae52', '178.152.187.190', 1673070134, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333037303133323b),
('59049b12adf1e534b53fdcdfa03c5d75ef5814d2', '78.101.180.162', 1673153069, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333135333036393b),
('46fd52139f71717d6fd6db19e1fcfe386ddf8351', '78.101.180.162', 1673153388, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333135333338383b706f7274666f6c696f49647c733a303a22223b),
('60b418dde80745640824a4021c83ba1bb777481d', '78.101.180.162', 1673153389, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333135333338383b706f7274666f6c696f49647c733a303a22223b),
('17a035f98a61e37859dce0c98837b4ae6846f7e7', '205.210.31.45', 1673328910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333332383931303b),
('67410c610adad5ed969f1061ffa70dd1db424067', '122.170.54.174', 1673346964, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333334363936343b),
('1d6714c4c10741cdc573b3575cb3cce185304603', '122.170.54.174', 1673347351, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333334373335313b),
('8c87f343706183b12a3226306f1bd72ffaa5c434', '122.170.54.174', 1673348198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333334383139383b),
('584f88a218170919a5f7fa9427bc9b16a176254a', '122.170.54.174', 1673354136, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335343133363b),
('8747d29680209676c104a26a389cad594bdcc0e2', '122.170.54.174', 1673355501, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335353530313b),
('4170a90c0020b800e426e00e63ba58d2c8d4e8e2', '122.170.54.174', 1673357195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373139353b),
('622744b3260abbd7abf3b0f827e0f272cf1f717d', '122.170.54.174', 1673357297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373239373b),
('54e40028553bedabf66f9c29054a75e8b5380b95', '122.170.54.174', 1673357516, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373531363b),
('c8ba66367740f8865e86d70ff8ecaccfd0c2a0d9', '122.170.54.174', 1673357347, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373239373b),
('fe08053ae1e2a55007eb2aa9b593a7066074127e', '122.170.54.174', 1673357474, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373434363b),
('b5211259884faac8bbf7bf61074f6ff20a9b74a3', '122.170.54.174', 1673357518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333335373531363b),
('0014c0cecdea0012faec48f25d45f6063acbd0f0', '122.170.54.174', 1673411239, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431313233393b),
('2d26b3b6c2f8363ab60012ff670ae602bc37b4f3', '122.170.54.174', 1673411691, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431313639313b),
('a7d04a0a3938129dbfd643ed4d23e51f7f0d6db7', '122.170.54.174', 1673412619, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431323631393b),
('f6cb219a4830a9e4202b25d27011dd36630edb37', '122.170.54.174', 1673412263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431323236333b),
('4a84672efd42b8e20ab62234ced7b5512b61c940', '122.170.54.174', 1673413086, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431333038363b),
('6613db72f545115791c7d4fc40058c8db2902179', '122.170.54.174', 1673413426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431333432363b),
('85c08fb413070700414baee9263f93c9511ea2bd', '122.170.54.174', 1673413884, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431333838343b),
('f86624c95bbbe6a9888a10a0ae5b12e786259499', '122.170.54.174', 1673414187, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431343138373b),
('27b7c2e7108ab4f852c75bd1275dd43c92ca1ccf', '122.170.54.174', 1673415949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431353934393b),
('17b3c352f15fd2331549707498597a9a7fee5868', '122.170.54.174', 1673417436, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431373433363b),
('e40e7991473253afeba162ad0c51e25ffed1cbd6', '122.170.54.174', 1673417851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431373835313b),
('ee1fc7c9178dfea70f11da6f2cf7cbe3d56f5062', '122.170.54.174', 1673418260, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431383236303b),
('7de0e1fac5679ce3696a7d0f77fff22539151eb6', '122.170.54.174', 1673418563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431383536333b),
('7f5f2c1980dc012c5ea6a056ccb68b7bcebcac70', '122.170.54.174', 1673419414, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431393431343b),
('e8ab8373325dc69f2cd351d306a3a4fa1dcda213', '122.170.54.174', 1673419043, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431393030323b),
('6642c56d67d5b6f06daedcb367611d3aa232a64c', '122.170.54.174', 1673421070, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432313037303b),
('d79273f1c38cb1ce1a3ae786ed4ff2c0bc1d3195', '122.170.54.174', 1673420205, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432303230353b),
('33ebba473316f9bd33d35a58beb733ebef2b3e84', '122.170.54.174', 1673419885, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333431393838353b),
('84529144610cd3f77134a472bcad39528d30f7ad', '122.170.54.174', 1673424426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432343432363b),
('7314f24de7ee2161c92459acf9cc5bd8b1316bf4', '122.170.54.174', 1673421075, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432313037303b),
('df04c3a21b048d2df38297793ba5466775eec06c', '122.170.54.174', 1673428885, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432383838353b),
('6e42501d7aa7e2454a7b6aff9ec304af5936ffb8', '122.170.54.174', 1673425030, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432353033303b),
('10100fdc8cb43b7656e55a4513a6a8f00555f59f', '122.170.54.174', 1673425373, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432353337333b),
('b435205753901c3e4568bdc507350c2753065646', '122.170.54.174', 1673426441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432363434313b),
('0a0a0c07c0cea4a02ae0b23aee5e422554d27250', '122.170.54.174', 1673427091, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432373039313b),
('7e6ae2f9951d7c4b31305d4ac7caf1b676877c67', '122.170.54.174', 1673427099, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432373039313b),
('3a278d55175ac95b2fe8975f646a3acc6bbe24f1', '122.170.54.174', 1673429249, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432393234393b),
('55f53114360a6afa17e3086f52fa869750280757', '122.170.54.174', 1673429255, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333432393234393b),
('267dc3536a339e2d36e55baa471ba59ec73ea74d', '78.101.168.57', 1673461887, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333436313837383b),
('c6d72fd46f3995f00e74b110177ffd68062baea8', '78.101.168.57', 1673553157, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333535333135363b),
('4a39d62bf7e62c66a4f4dd096ad8592eb1dde20b', '198.235.24.174', 1673560868, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333536303836383b),
('090b9ef665afaa628c0a84a8b96719681b4feeea', '198.235.24.140', 1673604985, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333630343938353b),
('d69f2c661ce3b37acc751b9c79897ba51d78db9d', '198.235.24.162', 1673618983, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333631383938333b),
('824a01a85d430c7848f16ddbdc471646ea574f86', '198.235.24.162', 1673659939, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333635393933393b),
('a77a10f35b9fb8926ada665de5d42505d843294a', '198.235.24.58', 1673700195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333730303139353b),
('20850926c838d17053a27b9b6891e8c47cc2fa4f', '205.210.31.50', 1673719749, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333731393734393b),
('4db392ee98752b1538c368ee1bfdc81d200cf50b', '198.235.24.163', 1673786207, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333738363230363b),
('c5a5182528197ea3309ba89e4b3e07c1ed1e54be', '198.235.24.46', 1673788724, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333738383732343b),
('c46bd7e2ab8a4f7e2fcc03a74fbedd2ae5fefd13', '110.227.247.113', 1673845872, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333834353837323b),
('bd0658f7d07da6af1271dcf019b72c4621bbed05', '110.227.247.113', 1673846076, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333834353837323b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('5664bd49fc9337277536a91858832b8b337b0fda', '52.114.32.28', 1673846107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333834363130373b),
('fcbdc7d04c779420b87088de55a97df03067e75b', '110.227.247.113', 1673846174, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333834363137343b),
('33d8d3476b471705efab9481b5344ac799de7824', '37.186.42.48', 1673855071, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835353037313b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('ed1e4bdb96ddfb3b93ad6d411f777ddaf2c8fab2', '37.186.42.48', 1673855376, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835353337363b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('88f6037821113792f12da87db0655dbbf9006c05', '37.186.42.48', 1673855717, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835353731373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('9c25cc8a01a080457f7743d62f43ce3313d9608b', '37.186.42.48', 1673856030, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835363033303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('e211083d7bbcd59aef97cc82d24960b55b6e0e36', '37.186.42.48', 1673856333, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835363333333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('22dd48a6fb3745c5cae2d7c47fb4ff95b46c8bd3', '37.186.42.48', 1673856659, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835363635393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('7d46ea0e82cab02e8cc44aa5876fe82c1138933f', '37.186.42.48', 1673856985, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835363938353b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('d0a5893337090b86bed9f4680cf1fcea02a69e1f', '37.186.42.48', 1673859040, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835393034303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('d79d91316a9175d9dea94d6141132d0361f5596b', '37.186.42.48', 1673857333, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835373238343b),
('4eb18e9a407d71bba5032445be005c2802012f24', '37.186.42.48', 1673859489, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835393438393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('9784bbfe5f69f7f65ff3b749be294bece352a0a5', '37.186.42.48', 1673859490, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333835393438393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b737563636573737c733a33373a225265636f726420686173206265656e2075706461746564207375636365737366756c6c792e223b5f5f63695f766172737c613a313a7b733a373a2273756363657373223b733a333a226f6c64223b7d),
('affd61d406940d8e4c86d7ff50eb4055078e3250', '110.227.247.113', 1673866668, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333836363636383b),
('62a4f9d7d17ae1f63fd301f04a60dc01dcd87801', '110.227.247.113', 1673867858, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333836373835383b),
('ec31f94cf9134c247a6245fd3b5ec18bcfb080a5', '223.238.227.139', 1673870733, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333837303733333b),
('fd339698b460d71d1472ea7cba41bdcad2f3a735', '110.227.247.113', 1673867493, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333836373439333b),
('9070be344f83b580e6ab3addc5ef059e8f6d7433', '110.227.247.113', 1673869953, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333836393935333b),
('fb1b0e9b82630edd978d8ac3679a5786dc9899d8', '110.227.247.113', 1673870259, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333837303235393b),
('e2c6d4e6242ac11a33f1adeb04e69c5382840b6c', '110.227.247.113', 1673870700, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333837303730303b),
('ba272ccb08705e469d2aebfc22b16878c1c8ec83', '110.227.247.113', 1673870711, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333837303730303b),
('11ed53a73b9fe5834ddcc14d2699c317ded7c108', '110.227.247.113', 1673870789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333837303733333b),
('e40fdd317bf00c4a3b621d476cbcbadfd3b86d43', '205.210.31.23', 1673929926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333932393932363b),
('c9cebf3dd7963f6907b2d662c1b1c487d9ce3f0f', '198.235.24.39', 1673982989, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637333938323938393b),
('affed7e4a33d17cfc8ee35eff79be68f4819b49d', '205.210.31.15', 1674005428, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343030353432383b),
('e50a8d1e65e7fb14316a07a5a576e29a1a421c9a', '178.152.135.148', 1674007113, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343030373131333b),
('19aca5f0b3d74d52ffa9464774849fe16d8909bd', '178.152.135.148', 1674009916, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343030393931363b),
('3b240e724b24b15b47512b64053d6aba776c29f6', '78.101.168.57', 1674009945, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343030393931363b),
('cd83f6ad2c039b1b4e3eeaa58b0edd34f106af03', '198.235.24.5', 1674021222, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343032313232323b),
('a1f063518e99b1bfd6353a936ade67104034f0ae', '78.101.168.57', 1674053962, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343035333936303b),
('84e5e8cc88d32d78ab6db2f8e71d2f40eb030d6d', '27.57.81.211', 1674104902, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343130343839383b),
('ad6207de944e388d4a28cded5ee378854847d973', '27.57.81.211', 1674115531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131353533313b),
('f60f364b765b44ec09f811209b56ff06010f18e5', '27.57.81.211', 1674116896, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131363839363b),
('b0b4507029292a8ce6d22111573414d34f7c81fa', '27.57.81.211', 1674119838, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131393833383b),
('d06ce180ea1987811f3591cedfc3835a6fe7d73a', '152.58.37.40', 1674116636, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131363439353b),
('f5a8340602fc08d54cd343cc94a14a367ab0ca80', '152.58.37.40', 1674116518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131363531383b),
('6636f9c3f171fa2d4a6ee09fda5747a7c56f65fa', '152.58.37.40', 1674116520, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131363531393b),
('8609859080add1b0f797a0a78c1a9bc483e37e3d', '27.57.81.211', 1674117394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131373339343b),
('87f9a555137585514614d1de7dfa5683f2c7858a', '27.57.81.211', 1674119850, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343131393835303b),
('f6119a02ada5e4111d525710f6b2f539dabefe7d', '106.205.246.144', 1674122239, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132323233393b),
('fab968f1d04f247c0c17ee51ea8d9d809c7c39b1', '27.57.81.211', 1674124907, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343930373b),
('685cee6f26e646c2def29a06ab53f20c300aa272', '27.57.81.211', 1674120831, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132303833313b),
('41208da1a31b4df8b839e6bc9ff3091aa31afb89', '27.57.81.211', 1674121655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132313635353b),
('cbceba518cbb66984242c9002c2822a396026491', '27.57.81.211', 1674122043, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132323034333b),
('e7632a5d95c5e4e725fcf560263e9dfea562c111', '27.57.81.211', 1674123977, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333937363b),
('8f9612625aebfd0e86aba318a84fef123fca8914', '106.205.246.144', 1674123005, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333030353b),
('8a596f69adb0bce457ca555d11f0cc8f69aefe48', '106.205.246.144', 1674123093, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333039333b),
('ed3ee0e06d32dd2e582e813d675f37a404e9e4b6', '106.205.246.144', 1674125174, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353137343b),
('f7b66865544ab252eb5765377e59ec6ffd8ec0d3', '106.205.246.144', 1674123094, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333039333b),
('b8b935901109f86e3f91f2cdabf4baf969c352ac', '106.205.246.144', 1674123131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333133313b),
('42406af5d7ff8e3ccb5231d40c8ead7a36a29512', '106.205.246.144', 1674123131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333133313b),
('0ae584289607669cfba92b3e3aa9dbb161f0d75c', '106.205.246.144', 1674123132, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333133313b),
('510500a07df5acb61d417bcfbfabe5c5b8fc8f9e', '106.205.246.144', 1674123936, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333933363b),
('53ef6a85c27cb7ec832a68f68049dcf77327e68b', '106.205.246.144', 1674124080, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132333933363b),
('a094d7dc894008abc8dbc324e2ad8af88ca5e93f', '27.57.81.211', 1674124299, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343239393b),
('07f973dc9bc91a2906126b8a1c53e423f011d376', '106.205.246.144', 1674124120, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343131373b),
('28196fca18229c6b9c97dc1fdf41d252435cd199', '27.57.81.211', 1674124791, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343739313b),
('a306dc38d66411fea856040f79df00530175c97b', '106.205.246.144', 1674124366, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343336313b),
('40907f6b9a34e19d2e6befcc8bb2497040ad1d63', '27.57.81.211', 1674125360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353336303b),
('a08bca25d187b0e014955aece7b4e840a8e69c1d', '27.57.81.211', 1674124856, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343834353b),
('ad091f0320fdef73a9f1c3cbdc6d12f160997c69', '27.57.81.211', 1674124975, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132343930373b),
('f81c7fb7accd553628e60bea5393c922f3c05121', '106.205.246.144', 1674125135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353133313b),
('32ab1dfd38a04449bc6301031fabb0fd77ea059a', '27.57.81.211', 1674125516, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353531363b),
('d8073ed1f339d37d4833d3e0b846e6269d8a1365', '27.57.81.211', 1674125782, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353738323b),
('f12c0567aec93174becdd2ac50b9e6e08b4b463f', '27.57.81.211', 1674125831, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132353833313b),
('55d1618be44f071113f3c4e94d92197b144ad847', '27.57.81.211', 1674126115, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132363131353b),
('0f8adfeec8f8ec4e7b46ce3067266cc93f5f5f6f', '27.57.81.211', 1674126160, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132363136303b),
('3e1b18f68210e6ea69a5dfb0bd15ef6b728b5f61', '27.57.81.211', 1674127025, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132373032343b),
('07b83e47e09a2f4a7e517ebc4c96597304f0479b', '27.57.81.211', 1674126465, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132363436353b),
('2fd144d77b417b855059af3d15f2aa2f573af89e', '27.57.81.211', 1674127049, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132373034393b),
('a2858b17864555c450ec9aa81c1ec40fcd4df326', '27.57.81.211', 1674127494, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132373439343b),
('0a73ad3a96942e558f4da31ab2e603490a8464b2', '27.57.81.211', 1674127449, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132373434393b),
('fdc9ccbecefeb09476082fb0aabdde7b88124fbc', '27.57.81.211', 1674127977, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132373937373b),
('874cae371c51f9c434571ea5c9431b635d8163e8', '27.57.81.211', 1674128041, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132383034313b),
('7fe72371c2791581b0460308ede630f1f8b53e93', '106.205.246.144', 1674128287, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132383238373b),
('eed22441581e5aad7f545f21835a903c1dd539f6', '27.57.81.211', 1674128404, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132383430343b);
INSERT INTO `wl_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('7678c4ddb6bce6e0224b126ec12257b70da136f6', '27.57.81.211', 1674128668, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132383636383b),
('d0a86bc1fb350ebe347f6b8c5926a16a5e657d33', '27.57.81.211', 1674128714, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132383731333b),
('c5e778b3872f3541f59bb9804d4bdaf7c6a85ee7', '27.57.81.211', 1674129003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132393030333b),
('0d891d0832e4de01bafb7158da8056e0b482a88a', '27.57.81.211', 1674130563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133303536333b),
('fffc2540380c7a83b53086a0ba32f32cf9e29a31', '27.57.81.211', 1674129396, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132393339363b),
('f40b778592074144ff39786ff1093a59b16ac7a8', '27.57.81.211', 1674129322, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132393332323b),
('9bec3e697cce02e3a10fdd19bac22d970b5bd443', '27.57.81.211', 1674129642, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343132393339363b),
('60c1f1c4bef3d1a46a8e4aa64b797d8e378a4334', '27.57.81.211', 1674131080, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133313038303b),
('fb3839788035d37154f479ea896fb861ddb4d1e9', '27.57.81.211', 1674131383, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133313338333b),
('1e65a5505703cbe793eecc4c8a5192762af5131c', '27.57.81.211', 1674131945, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133313934353b),
('3f828723bf87f595236e2548e2ba5b8b978f6e01', '49.34.40.164', 1674131712, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133313730303b),
('4e2f99d822ef9ed693e1c19bb82533c1eefda4d6', '27.57.81.211', 1674132294, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133323239343b),
('730f0e646a35e265ed42b0a97521b80704dc0c78', '27.57.81.211', 1674132460, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343133323239343b),
('c6dede0f995fafd336c2375c15b4d99a85525c2c', '27.61.156.183', 1674172891, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343137323838383b),
('a7709840130608c5b0f91fe86c036de3fcd8e7e3', '27.61.156.183', 1674172924, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343137323932323b),
('413cffc15098d96e823e69358b64083fb9e35525', '78.101.168.57', 1674194810, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139343831303b6d73675f747970657c733a373a2273756363657373223b6d73675f747970655f636f6e76735f726571756573747c733a31353a22737563636573735f636f6e74656374223b),
('2351b5e5c999d15fcb5e32de746a394e5b6f5e95', '27.57.81.211', 1674190197, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139303139373b),
('509edf2ae49e89076a5f9cb47998758c7c1f9a9f', '27.57.81.211', 1674190613, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139303631333b),
('c12f2379da814c33a80462bdb3ab3b5dc1beadac', '27.57.81.211', 1674191647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139313634373b),
('4edc3d0e4e5d364d58001e58fcfea32b46a96fbd', '27.57.81.211', 1674192193, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139323139333b),
('9591ef6d70a3521e15a9233a6add1e926074d7aa', '27.57.81.211', 1674192932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139323933323b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('8bdf679b3dcb7eb091a2f565cc6deac925de71b7', '27.57.81.211', 1674193580, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139333538303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('75d204b10a59fb2df87597af8ae5cc6be390a65f', '27.57.81.211', 1674195290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139353239303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('986e9107f0cfbb7667fdc4504216f2a5a8aee8f5', '178.152.136.255', 1674194894, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139343831303b6d73675f747970657c733a373a2273756363657373223b6d73675f747970655f636f6e76735f726571756573747c733a31353a22737563636573735f636f6e74656374223b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('ceaa0dcc3b305fa5ffdcf6e51d208ad2f0c7b5e0', '27.57.81.211', 1674195637, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139353633373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('69724746cab1df3fc100d24e0846c55d8de6a92f', '27.57.81.211', 1674195975, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139353937353b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('0794fd0fdd0ca27412152cdd8420945e1c2554cd', '27.57.81.211', 1674197728, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139373732383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('50a918eeb4faf12bdd55ab6113b398fc64d1c054', '27.57.81.211', 1674198131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139383133313b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('d107c78afaac3da7ca18de13e6dbdcee5ab41b2a', '27.57.81.211', 1674198564, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343139383536343b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('5b6c347324e6c58a5db99ac1ce59596ef3d81e49', '27.57.81.211', 1674208107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230383130373b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('9f4f7adf4373a72c02543ce501f96cfbae773d6a', '205.210.31.38', 1674201119, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230313131393b),
('36369d26a339121ff0dcd23bfd29693483a2abb7', '106.222.77.20', 1674203969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230333936373b),
('22171f332e4bfe5eaa150bbb18140d1e59f18d9e', '27.61.247.228', 1674206482, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230363338313b),
('475bfc466362c0472e621c3a4fc2b97dfca17ded', '27.61.247.228', 1674206532, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230363532343b),
('d273fcc1584f0477934931dfa0571bc108d3a4a1', '27.57.81.211', 1674219918, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231393931383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('aa1265c1324b7558aded384e42391e27899f2d07', '27.57.81.211', 1674208445, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230383434353b),
('ac35a734f1f58cd47ff2df4cfd08586801c12c0b', '27.57.81.211', 1674217792, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231373739323b),
('4dbf93c693715790ea042f2ecef3bacbae6c6b86', '52.114.14.102', 1674208904, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230383930343b),
('dc41e0eb8e63772e7d6a3dbe08d0cfc3ddd0c1ac', '27.57.81.211', 1674209452, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343230393435323b),
('c7410e6576846f99417f017fd7ae7d705f40d4ff', '27.57.81.211', 1674210268, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231303236383b),
('2dbbda10cc2eebc28124ab9cdcb9a8407ba76f81', '27.57.81.211', 1674211371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231313337313b),
('bb866bd7a8c783ad00b05d7f65c6cef8732e7b2e', '27.57.81.211', 1674211371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231313337313b),
('ec0553ed37450672bb3e4fa0b0e75be186545d87', '27.57.81.211', 1674219391, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231393339313b),
('1bfed86bd321e3e963721170415d6ccf587cb227', '27.57.81.211', 1674219889, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231393838393b),
('50c27bcdbf969889a094619f8fd93b89124c0ccf', '52.114.32.28', 1674219813, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231393831333b),
('dbfc6a50e92a285e9eb179748bc565f85ba5e76c', '27.57.81.211', 1674221198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232313139383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('bc5ad5380c1daca1e8b0695b069de2d2f9de0c27', '27.57.81.211', 1674219931, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343231393931383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('3537de9b00b3d6441afcfc088dd78286bfe0dc66', '27.57.81.211', 1674221650, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232313635303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('81fc8629f063f82da175e425f985fbf634192f2d', '27.57.81.211', 1674222034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232323033343b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('aa398d6419854fea9d80da9e7a2fdd61765a75ea', '27.57.81.211', 1674222443, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232323434323b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('de19c88feae0e29124f2da0f722da0841bcf2d0c', '27.57.81.211', 1674224108, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232343130383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('46760325b47922f49336e31bc5e41f5b58318aee', '27.57.81.211', 1674223356, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232333335363b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('3f09064d934d071ec7ddc33d98408e2b7187b405', '27.57.81.211', 1674223757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232333735363b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('3133256d20db019d9cafb0ac21ff2ddb287348df', '27.57.81.211', 1674224159, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232343130383b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('2d42f08ab9a31bea23aa56daf6dd928aafa3e54b', '178.152.136.255', 1674226845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232363834353b),
('32cdabdcb78f619e20eaf7c8b325bedc5ed63585', '178.152.136.255', 1674226846, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232363834353b),
('8e5ef33629415655ce6ffb6bf3477c6936af3738', '162.222.226.140', 1674226846, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343232363834363b),
('aad2abe30a1b2f14cfb16a3f9d4730486329b464', '106.213.246.156', 1674269938, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343236393931313b),
('af24382f3a73a9762838bcb4ee5cc9cb06425bbd', '205.210.31.16', 1674286492, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343238363439323b),
('758e318cef2047aff43e64b40edf076507a2b0b5', '27.61.170.46', 1674288150, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343238383134303b),
('5741459308a54d55f5566387fbe9f28dc53c389f', '78.101.168.57', 1674289348, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343238393334363b),
('de90f17587254a70f620be9a1946e9656053f2a4', '205.210.31.166', 1674290111, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343239303131313b),
('fb38fbc1f0f0f984a2a1dee8290dc1c1a50ba5c8', '198.235.24.141', 1674290195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343239303139353b),
('01aaaa5e5e2fb2a1ba12244f43d3dded4afcf540', '198.235.24.49', 1674307493, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343330373439333b),
('599b6551296b56aafd5b2e0d960eb8057d67c011', '198.235.24.130', 1674314079, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343331343037393b),
('75a6d9cabc244b467ff9392c83dfb40d96ba3461', '198.235.24.135', 1674322645, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343332323634353b),
('d741de0e1e2ff4f66fad839284fa9f41cf4fb881', '198.235.24.10', 1674331313, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343333313331333b),
('ba36dfe4ee82ee41c21f08f9c2cede83e506b47c', '37.186.42.48', 1674393687, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343339333634363b),
('e85673c2241fdcb9eb5669fc798bcdb654aa0db4', '27.61.168.103', 1674402321, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343430323332313b),
('cbd6b17b0ff4eb2a57937fa40481135dd08ca5e4', '27.61.168.103', 1674458139, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343435383133393b),
('e3880174f23c001080752f586856da5d051cf1cc', '27.61.168.103', 1674458140, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343435383133393b),
('6e0944359e34dec18631038eafaa9f20e45084d2', '27.61.168.103', 1674493922, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343439333932303b),
('5392dcb922463f151382dc153c76aa1024e14660', '122.170.30.52', 1674564333, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536343333333b),
('bad582a7b38b7bcd211eb302f5ab303ad8650d49', '122.170.30.52', 1674559938, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343535393933383b),
('17f367e85f4b0baf4ae4dcda0d399637d3cde4bb', '122.170.30.52', 1674560291, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536303239313b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('1d921ba2811821901095285c9940ce82e1c0c3c3', '122.170.30.52', 1674561370, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536313337303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('78c382eea66401eb47a47ee1a73c8ceb41dd8a9a', '122.170.30.52', 1674561783, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536313738333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b737563636573737c733a32363a225265636f7264206164646564207375636365737366756c6c792e223b5f5f63695f766172737c613a313a7b733a373a2273756363657373223b733a333a226f6c64223b7d),
('641e6058bdf930f49b09adcc03ee3a3a803d3f55', '122.170.30.52', 1674562313, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536323331333b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('c31efab468c3eaf8c43675e2b64a46c9dd836e5c', '122.170.30.52', 1674563280, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536333238303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('2f697baf5826a2597f5876f7226f2fd361c45fe5', '122.170.30.52', 1674563612, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536333631323b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b),
('d352298e5c21280dc6ba99f9d7941579204e43ba', '122.170.30.52', 1674563960, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536333936303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('b9d31e369fd84704ac760f32f21bfeb7e762539b', '122.170.30.52', 1674564198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536333936303b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b6d73675f747970657c733a373a2273756363657373223b706f7274666f6c696f49647c733a303a22223b),
('95846f3c3f4f25352eb1de352a8a6e4426a07835', '52.114.14.71', 1674564214, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536343231343b),
('b36a3c61074ee2a7db018a191be9e44d218fe151', '122.170.30.52', 1674564382, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343536343333333b706f7274666f6c696f49647c733a303a22223b70686f746f49647c733a323a223632223b),
('3e838da3f14bd9971cae4bc59ca883a8dcd55935', '198.235.24.154', 1674574284, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343537343238343b),
('183d1089a9383cfadb0ac4b7623730e86888639b', '198.235.24.171', 1674577414, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343537373431343b),
('a4c28dd64660e82160055e3b6702c1e869b2455b', '198.235.24.31', 1674580398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343538303339373b),
('99b2935b15c134dcb5355484f7911db33fa6c1a2', '162.142.125.221', 1674584770, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343538343737303b),
('f6d90cf7a57112c5768faaf8853d9ff2d5aa9206', '162.142.125.221', 1674584771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343538343737313b),
('8785d09df37c0451b883e5a8da715b0a0d68ac91', '162.142.125.221', 1674584772, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343538343737323b),
('4d220622f8c6b7eeb03bc7b1fb31d0e071adecff', '162.142.125.221', 1674584772, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343538343737323b),
('6f16e1b26ac313fa9007a30961d951a22e85331d', '205.210.31.47', 1674617263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343631373236333b),
('5104c831640906683266f7e1120ce500f8a8a8e2', '122.170.30.52', 1674636795, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633363739353b),
('e1ec814842760ebcf0d9aca2e84c0e3269367265', '122.170.30.52', 1674637466, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633373436363b70686f746f49647c733a323a223630223b),
('b8f205ba0c3f015284b3c6d4778a8bff94b21d87', '122.170.30.52', 1674638371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383337313b70686f746f49647c733a323a223630223b),
('f362af3e704f42695f36620a57896c23d0a306e8', '122.170.30.52', 1674638935, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383933353b70686f746f49647c733a323a223630223b706f7274666f6c696f49647c733a303a22223b),
('8cd4147999cc1d6f59496da743b62ce8686c2546', '122.170.30.52', 1674638834, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383538343b706f7274666f6c696f49647c733a303a22223b),
('695705d7a5028cfc356e49d389a928cc394b6b2c', '122.170.30.52', 1674638600, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383630303b),
('5301a3f4e301ae4dd892cc87a478b8c0d8e0d7c9', '122.170.30.52', 1674638872, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383837323b),
('69a3042fe904452edb661fc8789162340358338c', '122.170.30.52', 1674638873, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383837333b),
('88806c5baf4ae51b1dc8428e90dd61fc36e1ae37', '122.170.30.52', 1674638873, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633383837333b),
('bc130a9b6bb39a00356a2cae6eb4d7e8db34c4fd', '122.170.30.52', 1674640144, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634303134343b70686f746f49647c733a323a223630223b706f7274666f6c696f49647c733a303a22223b),
('59bfd2ce72c78a81a259072dc80aad10521b5aee', '122.170.30.52', 1674639251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633393235313b70686f746f49647c733a323a223630223b706f7274666f6c696f49647c733a303a22223b),
('0d27b692f5c1032f0a9586b802c9c571d4e5f867', '122.170.30.52', 1674639776, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343633393737363b),
('c592d72b745b48115de15726f3b9ce673d75c095', '122.170.30.52', 1674640371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634303337313b),
('8cd0736dbb25b56152b1f5cc39f9e5db9e1bfd0b', '122.170.30.52', 1674640480, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634303438303b70686f746f49647c733a323a223630223b706f7274666f6c696f49647c733a303a22223b),
('5a7771d94327f21b37495559b87c05045a74ab03', '122.170.30.52', 1674640371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634303337313b),
('c9e805cbed86c63b2e09f98677978a17210c9895', '122.170.30.52', 1674641672, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634313637323b70686f746f49647c733a323a223633223b706f7274666f6c696f49647c733a303a22223b),
('e53ff1e1e20597f784f80272e02777f2e5828dc2', '122.170.30.52', 1674641997, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634313939373b70686f746f49647c733a323a223633223b706f7274666f6c696f49647c733a303a22223b),
('49374ffdffdb53c43570934ac02ab2e6b3254049', '122.170.30.52', 1674643817, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634333831373b70686f746f49647c733a323a223633223b706f7274666f6c696f49647c733a303a22223b),
('1b0deb53627d6bfceb74ac0adfef7cb496d33d5b', '122.170.30.52', 1674644466, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634343436363b70686f746f49647c733a323a223630223b706f7274666f6c696f49647c733a303a22223b),
('3719eca1cd6cc1181bf61acbd62fcc6f798391a2', '122.170.30.52', 1674644790, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634343739303b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('0f4fc44191a2860a43fa6e294649f0a6279c1b2e', '122.170.30.52', 1674645759, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634353735393b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('a3f18332e64c47e77601748647437ce91fcbb956', '122.170.30.52', 1674645329, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634353332393b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('f99622a0a0dc7c3738e0b4d9208676fd8c65453f', '122.170.30.52', 1674647692, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634373639323b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('9de1fd9413c2423e1d78f92740e878d23057d268', '37.186.42.48', 1674647796, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634373739363b706f7274666f6c696f49647c733a323a223538223b),
('e938e46f3d593aa3b582c93915761d6cce84ca66', '37.186.42.48', 1674648055, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634383035353b706f7274666f6c696f49647c733a323a223631223b),
('5f7724f8adf9c20c6fd60616fb169ec290b078a1', '122.170.30.52', 1674649582, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634393538323b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('fb9e66831daaec712a5f8c8b6233d61ddb091b50', '37.186.42.48', 1674647797, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634373739363b706f7274666f6c696f49647c733a323a223538223b),
('93a9be55c5e0f12547fc0bcbe6169dd8892a9d1d', '37.186.42.48', 1674648056, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634383035353b706f7274666f6c696f49647c733a323a223631223b),
('66a8632091a40d3db901b518378d05ac8655256b', '27.61.167.33', 1674665713, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343636353731333b),
('836fa47cf187746a39b146999c3bd8c14c93c21a', '122.170.30.52', 1674649903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634393930333b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('03a42f05acb26195580db0585440bc453e4081f1', '122.170.30.52', 1674649917, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343634393930333b70686f746f49647c733a323a223632223b706f7274666f6c696f49647c733a303a22223b),
('7e65ca738b3b55b0cdb46f96db838dde35179218', '37.186.42.48', 1674653661, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343635333636313b),
('c1a380f00a0f768be6da9498413677cf7c6f8d34', '37.186.42.48', 1674653693, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343635333638353b),
('f755edb1a94efb860ab9b383c670599ef1d52f2e', '27.61.167.33', 1674665719, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343636353731333b),
('a5cbb392965406c9e2e89130f1367926206a12aa', '78.101.168.57', 1674667331, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343636373333303b),
('1133b0bdb84da1e668768515b6d545658fe1559b', '78.101.168.57', 1674667332, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343636373333303b),
('45ca1ee642d04c758cfcb3a49bc4d0ddbb4fa050', '78.101.168.57', 1674667416, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343636373337303b),
('6ad07abf8b57c107c497c696a71d49d52663507e', '78.101.168.57', 1674708101, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343730373838393b61646d696e5f757365727c733a353a2261646d696e223b61646d5f6b65797c733a383a22636e73676b4d6434223b61646d696e5f747970657c733a313a2231223b61646d696e5f69647c733a313a2231223b61646d696e5f6c6f676765645f696e7c623a313b),
('5b08b547a6f5fe1645500ac04bacdb031e818e97', '65.155.30.101', 1674734969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343733343936343b),
('fa691eb2c4c6455670d70d04f5a354d98c9a5b8d', '78.101.168.57', 1674752218, 0x5f5f63695f6c6173745f726567656e65726174657c693a313637343735323231353b);

-- --------------------------------------------------------

--
-- Table structure for table `wl_team`
--

CREATE TABLE `wl_team` (
  `team_id` int(11) NOT NULL,
  `team_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `team_image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_description` longtext COLLATE utf8_unicode_ci,
  `sort_order` int(11) DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_team`
--

INSERT INTO `wl_team` (`team_id`, `team_title`, `friendly_url`, `team_image`, `designation`, `alt_tag`, `team_description`, `sort_order`, `status`, `date_added`, `date_updated`) VALUES
(6, 'tedt', 'tedt', 'team-img-1.jpg', 'Testing', '', '', NULL, '1', '2019-07-26 06:52:34', '2019-07-26 08:01:17'),
(7, 'Jonny', 'jonny', 'team-img-2.jpg', 'Business Consultant', '', '', NULL, '1', '2019-07-26 07:40:21', '2019-07-26 08:01:08'),
(8, 'Piter dosay', 'piter-dosay', 'team-img-3.jpg', 'Business Consultant', '', '', NULL, '1', '2019-07-26 07:40:42', '2019-07-26 08:49:18');

-- --------------------------------------------------------

--
-- Table structure for table `wl_testimonial`
--

CREATE TABLE `wl_testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poster_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonial_description` text COLLATE utf8_unicode_ci,
  `posted_date` datetime DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '1=Actice,0=Inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wl_testimonial`
--

INSERT INTO `wl_testimonial` (`testimonial_id`, `testimonial_title`, `poster_name`, `photo`, `friendly_url`, `email`, `testimonial_description`, `posted_date`, `status`) VALUES
(7, NULL, 'riya sharma', '', 'riya-7', 'riya@gmail.com', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '2019-05-06 13:11:07', '1'),
(24, NULL, 'suhani pandey', '', '', 'subashvns@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget sagittis justo. Integer iaculis ullamcorper nibh et venenatis. Nunc blandit orci ut mi porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non', '2022-01-22 15:18:46', '1'),
(10, NULL, 'Deepak', '', 'deepak-10', 'admin@gmail.com', 'rem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse rem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esserem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esserem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse', '2019-07-31 14:53:33', '1'),
(23, NULL, 'suhani pandey', '', 'suhani-pandey-23', 'subashvns@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget sagittis justo. Integer iaculis ullamcorper nibh et venenatis. Nunc blandit orci ut mi porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non', '2022-01-22 15:16:36', '1'),
(18, NULL, 'Manish', '', 'manish-18', 'weblink.manish84@gmail.com', 'sdfsfdsfdsdsa', '2021-06-11 05:24:21', '1'),
(16, NULL, 'raj', '', 'raj-16', 'pp@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequa', '2021-01-14 09:41:59', '1'),
(21, NULL, 'subash chandra pandey', 'test.jpg', 'subash-chandra-pandey-21', 'info@bagboxmt.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget sagittis justo. Integer iaculis ullamcorper nibh et venenatis. Nunc blandit orci ut mi porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non', '2021-12-09 05:01:45', '1'),
(22, NULL, 'subash chandra pandey', 'test1.jpg', 'subash-chandra-pandey-22', 'info@bagboxmt.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non neque metus. Curabitur augue mauris, aliquet sed commodo id, interdum eget est. Nam sollicitudin lorem vitae ligula rhoncus finibus. Morbi eget sagittis justo. Integer iaculis ullamcorper nibh et venenatis. Nunc blandit orci ut mi porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non', '2021-12-09 05:07:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_commentmeta`
--

CREATE TABLE `wp_d2fwj6_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_comments`
--

CREATE TABLE `wp_d2fwj6_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_comments`
--

INSERT INTO `wp_d2fwj6_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2022-07-31 05:23:39', '2022-07-31 05:23:39', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://en.gravatar.com/\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_links`
--

CREATE TABLE `wp_d2fwj6_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_options`
--

CREATE TABLE `wp_d2fwj6_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_options`
--

INSERT INTO `wp_d2fwj6_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://www.jbaagency.net/backup', 'yes'),
(2, 'home', 'https://www.jbaagency.net/backup', 'yes'),
(3, 'blogname', 'Err0r H4xor', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'pekebo3955@chimpad.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:94:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:0:{}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'twentytwentytwo', 'yes'),
(41, 'stylesheet', 'twentytwentytwo', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '53496', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'posts', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:0:{}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '0', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1674797018', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(99, 'initial_db_version', '51917', 'yes'),
(100, 'wp_d2fwj6_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(101, 'fresh_site', '1', 'yes'),
(102, 'user_count', '1', 'no'),
(103, 'widget_block', 'a:6:{i:2;a:1:{s:7:\"content\";s:19:\"<!-- wp:search /-->\";}i:3;a:1:{s:7:\"content\";s:154:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Posts</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->\";}i:4;a:1:{s:7:\"content\";s:227:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Comments</h2><!-- /wp:heading --><!-- wp:latest-comments {\"displayAvatar\":false,\"displayDate\":false,\"displayExcerpt\":false} /--></div><!-- /wp:group -->\";}i:5;a:1:{s:7:\"content\";s:146:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Archives</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->\";}i:6;a:1:{s:7:\"content\";s:150:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Categories</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:9:\"sidebar-2\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}s:13:\"array_version\";i:3;}', 'yes'),
(105, 'cron', 'a:8:{i:1659342221;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1659374621;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1659374749;a:1:{s:21:\"wp_update_user_counts\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1659417821;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1659417949;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1659417950;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1659936221;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(106, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(117, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(121, 'theme_mods_twentytwentytwo', 'a:1:{s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(124, 'recovery_keys', 'a:0:{}', 'yes'),
(125, 'https_detection_errors', 'a:0:{}', 'yes'),
(133, 'db_upgraded', '', 'yes'),
(134, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.0.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.0.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.0.1\";s:7:\"version\";s:5:\"6.0.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1659341111;s:15:\"version_checked\";s:5:\"6.0.1\";s:12:\"translations\";a:0:{}}', 'no'),
(137, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:22:\"pekebo3955@chimpad.com\";s:7:\"version\";s:5:\"6.0.1\";s:9:\"timestamp\";i:1659245033;}', 'no'),
(138, '_site_transient_timeout_browser_a0909810a6d132832e28ef6da18ec77c', '1659849949', 'no'),
(139, '_site_transient_browser_a0909810a6d132832e28ef6da18ec77c', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:9:\"103.0.0.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(140, '_site_transient_timeout_php_check_2bd835122fdc4f0e2ce94cd2e6f48f9d', '1659849950', 'no'),
(141, '_site_transient_php_check_2bd835122fdc4f0e2ce94cd2e6f48f9d', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(155, 'can_compress_scripts', '0', 'no'),
(161, 'finished_updating_comment_type', '1', 'yes'),
(169, '_site_transient_timeout_theme_roots', '1659342911', 'no'),
(170, '_site_transient_theme_roots', 'a:12:{s:5:\"astra\";s:7:\"/themes\";s:7:\"blocksy\";s:7:\"/themes\";s:4:\"crio\";s:7:\"/themes\";s:15:\"hello-elementor\";s:7:\"/themes\";s:6:\"hestia\";s:7:\"/themes\";s:4:\"neve\";s:7:\"/themes\";s:7:\"oceanwp\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";s:15:\"twentytwentytwo\";s:7:\"/themes\";s:5:\"ultra\";s:7:\"/themes\";}', 'no'),
(171, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1659341112;s:7:\"checked\";a:12:{s:5:\"astra\";s:5:\"3.7.4\";s:7:\"blocksy\";s:7:\"1.8.6.5\";s:4:\"crio\";s:5:\"2.9.4\";s:15:\"hello-elementor\";s:5:\"2.4.1\";s:6:\"hestia\";s:6:\"3.0.19\";s:4:\"neve\";s:6:\"3.0.10\";s:7:\"oceanwp\";s:5:\"3.0.7\";s:14:\"twentynineteen\";s:3:\"2.1\";s:12:\"twentytwenty\";s:3:\"1.8\";s:15:\"twentytwentyone\";s:3:\"1.4\";s:15:\"twentytwentytwo\";s:3:\"1.0\";s:5:\"ultra\";s:5:\"1.6.3\";}s:8:\"response\";a:12:{s:5:\"astra\";a:6:{s:5:\"theme\";s:5:\"astra\";s:11:\"new_version\";s:5:\"3.9.1\";s:3:\"url\";s:35:\"https://wordpress.org/themes/astra/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/theme/astra.3.9.1.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.3\";}s:7:\"blocksy\";a:6:{s:5:\"theme\";s:7:\"blocksy\";s:11:\"new_version\";s:6:\"1.8.44\";s:3:\"url\";s:37:\"https://wordpress.org/themes/blocksy/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/theme/blocksy.1.8.44.zip\";s:8:\"requires\";s:3:\"5.2\";s:12:\"requires_php\";s:3:\"7.0\";}s:4:\"crio\";a:6:{s:5:\"theme\";s:4:\"crio\";s:11:\"new_version\";s:6:\"2.16.1\";s:3:\"url\";s:34:\"https://wordpress.org/themes/crio/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/theme/crio.2.16.1.zip\";s:8:\"requires\";s:3:\"5.5\";s:12:\"requires_php\";s:3:\"5.6\";}s:15:\"hello-elementor\";a:6:{s:5:\"theme\";s:15:\"hello-elementor\";s:11:\"new_version\";s:5:\"2.6.1\";s:3:\"url\";s:45:\"https://wordpress.org/themes/hello-elementor/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/theme/hello-elementor.2.6.1.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:3:\"5.6\";}s:6:\"hestia\";a:6:{s:5:\"theme\";s:6:\"hestia\";s:11:\"new_version\";s:6:\"3.0.23\";s:3:\"url\";s:36:\"https://wordpress.org/themes/hestia/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/theme/hestia.3.0.23.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";s:5:\"5.4.0\";}s:4:\"neve\";a:6:{s:5:\"theme\";s:4:\"neve\";s:11:\"new_version\";s:5:\"3.3.4\";s:3:\"url\";s:34:\"https://wordpress.org/themes/neve/\";s:7:\"package\";s:52:\"https://downloads.wordpress.org/theme/neve.3.3.4.zip\";s:8:\"requires\";s:3:\"5.4\";s:12:\"requires_php\";s:3:\"7.0\";}s:7:\"oceanwp\";a:6:{s:5:\"theme\";s:7:\"oceanwp\";s:11:\"new_version\";s:5:\"3.3.3\";s:3:\"url\";s:37:\"https://wordpress.org/themes/oceanwp/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/theme/oceanwp.3.3.3.zip\";s:8:\"requires\";s:3:\"5.6\";s:12:\"requires_php\";s:3:\"7.2\";}s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.3.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.2.0.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.6.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}s:15:\"twentytwentytwo\";a:6:{s:5:\"theme\";s:15:\"twentytwentytwo\";s:11:\"new_version\";s:3:\"1.2\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentytwo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentytwo.1.2.zip\";s:8:\"requires\";s:3:\"5.9\";s:12:\"requires_php\";s:3:\"5.6\";}s:5:\"ultra\";a:6:{s:5:\"theme\";s:5:\"ultra\";s:11:\"new_version\";s:5:\"1.6.4\";s:3:\"url\";s:35:\"https://wordpress.org/themes/ultra/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/theme/ultra.1.6.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:6:\"5.6.20\";}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(172, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1659341112;s:8:\"response\";a:10:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"5.0\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.5.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.0\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";b:0;}s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:33:\"w.org/plugins/all-in-one-seo-pack\";s:4:\"slug\";s:19:\"all-in-one-seo-pack\";s:6:\"plugin\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:11:\"new_version\";s:7:\"4.2.3.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/all-in-one-seo-pack/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/all-in-one-seo-pack.4.2.3.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:72:\"https://ps.w.org/all-in-one-seo-pack/assets/icon-256x256.png?rev=2443290\";s:2:\"1x\";s:64:\"https://ps.w.org/all-in-one-seo-pack/assets/icon.svg?rev=2443290\";s:3:\"svg\";s:64:\"https://ps.w.org/all-in-one-seo-pack/assets/icon.svg?rev=2443290\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-1544x500.png?rev=2443290\";s:2:\"1x\";s:74:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-772x250.png?rev=2443290\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.4\";s:14:\"upgrade_notice\";s:56:\"<p>This update adds major improvements and bugfixes.</p>\";}s:50:\"google-analytics-for-wordpress/googleanalytics.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:44:\"w.org/plugins/google-analytics-for-wordpress\";s:4:\"slug\";s:30:\"google-analytics-for-wordpress\";s:6:\"plugin\";s:50:\"google-analytics-for-wordpress/googleanalytics.php\";s:11:\"new_version\";s:5:\"8.7.0\";s:3:\"url\";s:61:\"https://wordpress.org/plugins/google-analytics-for-wordpress/\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/plugin/google-analytics-for-wordpress.8.7.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:83:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon-256x256.png?rev=1598927\";s:2:\"1x\";s:75:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon.svg?rev=1598927\";s:3:\"svg\";s:75:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon.svg?rev=1598927\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:86:\"https://ps.w.org/google-analytics-for-wordpress/assets/banner-1544x500.png?rev=2159532\";s:2:\"1x\";s:85:\"https://ps.w.org/google-analytics-for-wordpress/assets/banner-772x250.png?rev=2159532\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:5:\"4.8.0\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.5\";}s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:6:\"11.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/jetpack.11.1.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=2638128\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=2638128\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=2638128\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=2653649\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=2653649\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.9\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:37:\"optinmonster/optin-monster-wp-api.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/optinmonster\";s:4:\"slug\";s:12:\"optinmonster\";s:6:\"plugin\";s:37:\"optinmonster/optin-monster-wp-api.php\";s:11:\"new_version\";s:5:\"2.8.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/optinmonster/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/optinmonster.2.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/optinmonster/assets/icon-256x256.png?rev=1145864\";s:2:\"1x\";s:65:\"https://ps.w.org/optinmonster/assets/icon-128x128.png?rev=1145864\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/optinmonster/assets/banner-1544x500.png?rev=2311621\";s:2:\"1x\";s:67:\"https://ps.w.org/optinmonster/assets/banner-772x250.png?rev=2311621\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:5:\"4.7.0\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.3\";}s:47:\"post-and-page-builder/post-and-page-builder.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:35:\"w.org/plugins/post-and-page-builder\";s:4:\"slug\";s:21:\"post-and-page-builder\";s:6:\"plugin\";s:47:\"post-and-page-builder/post-and-page-builder.php\";s:11:\"new_version\";s:6:\"1.20.1\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/post-and-page-builder/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/post-and-page-builder.1.20.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/post-and-page-builder/assets/icon-256x256.png?rev=1768477\";s:2:\"1x\";s:74:\"https://ps.w.org/post-and-page-builder/assets/icon-128x128.png?rev=1768477\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/post-and-page-builder/assets/banner-1544x500.png?rev=1768477\";s:2:\"1x\";s:76:\"https://ps.w.org/post-and-page-builder/assets/banner-772x250.png?rev=1768477\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.7\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.4\";}s:35:\"boldgrid-backup/boldgrid-backup.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:29:\"w.org/plugins/boldgrid-backup\";s:4:\"slug\";s:15:\"boldgrid-backup\";s:6:\"plugin\";s:35:\"boldgrid-backup/boldgrid-backup.php\";s:11:\"new_version\";s:6:\"1.15.2\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/boldgrid-backup/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/boldgrid-backup.1.15.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/boldgrid-backup/assets/icon-256x256.png?rev=1880952\";s:2:\"1x\";s:68:\"https://ps.w.org/boldgrid-backup/assets/icon-128x128.png?rev=1880952\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/boldgrid-backup/assets/banner-1544x500.png?rev=1880952\";s:2:\"1x\";s:70:\"https://ps.w.org/boldgrid-backup/assets/banner-772x250.png?rev=1880952\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.4\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.4\";}s:24:\"wpforms-lite/wpforms.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wpforms-lite\";s:4:\"slug\";s:12:\"wpforms-lite\";s:6:\"plugin\";s:24:\"wpforms-lite/wpforms.php\";s:11:\"new_version\";s:7:\"1.7.5.5\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wpforms-lite/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wpforms-lite.1.7.5.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:65:\"https://ps.w.org/wpforms-lite/assets/icon-256x256.png?rev=2574201\";s:2:\"1x\";s:57:\"https://ps.w.org/wpforms-lite/assets/icon.svg?rev=2574198\";s:3:\"svg\";s:57:\"https://ps.w.org/wpforms-lite/assets/icon.svg?rev=2574198\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wpforms-lite/assets/banner-1544x500.png?rev=2602491\";s:2:\"1x\";s:67:\"https://ps.w.org/wpforms-lite/assets/banner-772x250.png?rev=2602491\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/wpforms-lite/assets/banner-1544x500-rtl.png?rev=2602491\";s:2:\"1x\";s:71:\"https://ps.w.org/wpforms-lite/assets/banner-772x250-rtl.png?rev=2602491\";}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"3.5.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.3.5.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2468655\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2468655\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:6:\"5.6.20\";}s:27:\"wp-super-cache/wp-cache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/wp-super-cache\";s:4:\"slug\";s:14:\"wp-super-cache\";s:6:\"plugin\";s:27:\"wp-super-cache/wp-cache.php\";s:11:\"new_version\";s:5:\"1.7.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-super-cache/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-super-cache.1.7.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-256x256.png?rev=1095422\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-128x128.png?rev=1095422\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-super-cache/assets/banner-1544x500.png?rev=1082414\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-super-cache/assets/banner-772x250.png?rev=1082414\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.1\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:39:\"boldgrid-easy-seo/boldgrid-easy-seo.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:31:\"w.org/plugins/boldgrid-easy-seo\";s:4:\"slug\";s:17:\"boldgrid-easy-seo\";s:6:\"plugin\";s:39:\"boldgrid-easy-seo/boldgrid-easy-seo.php\";s:11:\"new_version\";s:6:\"1.6.10\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/boldgrid-easy-seo/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/boldgrid-easy-seo.1.6.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/boldgrid-easy-seo/assets/icon-256x256.png?rev=1773296\";s:2:\"1x\";s:70:\"https://ps.w.org/boldgrid-easy-seo/assets/icon-128x128.png?rev=1773296\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/boldgrid-easy-seo/assets/banner-1544x500.png?rev=1773381\";s:2:\"1x\";s:72:\"https://ps.w.org/boldgrid-easy-seo/assets/banner-772x250.png?rev=1773381\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.4\";}s:9:\"hello.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/hello-dolly/assets/banner-1544x500.jpg?rev=2645582\";s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}}s:7:\"checked\";a:19:{s:19:\"akismet/akismet.php\";s:6:\"4.1.12\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:7:\"4.1.4.4\";s:39:\"boldgrid-easy-seo/boldgrid-easy-seo.php\";s:6:\"1.6.10\";s:31:\"boldgrid-gallery/wc-gallery.php\";s:5:\"1.5.1\";s:47:\"boldgrid-inspirations/boldgrid-inspirations.php\";s:5:\"2.6.4\";s:29:\"crio-premium/crio-premium.php\";s:5:\"1.3.0\";s:25:\"dreamhost-panel-login.php\";s:5:\"1.0.0\";s:50:\"google-analytics-for-wordpress/googleanalytics.php\";s:5:\"8.1.0\";s:9:\"hello.php\";s:5:\"1.7.2\";s:19:\"jetpack/jetpack.php\";s:6:\"10.2.1\";s:37:\"optinmonster/optin-monster-wp-api.php\";s:5:\"2.6.6\";s:47:\"post-and-page-builder/post-and-page-builder.php\";s:6:\"1.14.2\";s:63:\"post-and-page-builder-premium/post-and-page-builder-premium.php\";s:5:\"1.0.5\";s:35:\"boldgrid-backup/boldgrid-backup.php\";s:7:\"1.14.13\";s:51:\"boldgrid-backup-premium/boldgrid-backup-premium.php\";s:5:\"1.5.9\";s:14:\"Update/ubh.php\";s:3:\"1.0\";s:24:\"wpforms-lite/wpforms.php\";s:5:\"1.7.0\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"3.1.0\";s:27:\"wp-super-cache/wp-cache.php\";s:5:\"1.7.4\";}}', 'no'),
(173, '_transient_health-check-site-status-result', '{\"good\":16,\"recommended\":3,\"critical\":0}', 'yes'),
(174, '_transient_timeout_global_styles_twentytwentytwo', '1659341247', 'no');
INSERT INTO `wp_d2fwj6_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(175, '_transient_global_styles_twentytwentytwo', 'body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--color--foreground: #000000;--wp--preset--color--background: #ffffff;--wp--preset--color--primary: #1a4548;--wp--preset--color--secondary: #ffe2c7;--wp--preset--color--tertiary: #F6F6F6;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--gradient--vertical-secondary-to-tertiary: linear-gradient(to bottom,var(--wp--preset--color--secondary) 0%,var(--wp--preset--color--tertiary) 100%);--wp--preset--gradient--vertical-secondary-to-background: linear-gradient(to bottom,var(--wp--preset--color--secondary) 0%,var(--wp--preset--color--background) 100%);--wp--preset--gradient--vertical-tertiary-to-background: linear-gradient(to bottom,var(--wp--preset--color--tertiary) 0%,var(--wp--preset--color--background) 100%);--wp--preset--gradient--diagonal-primary-to-foreground: linear-gradient(to bottom right,var(--wp--preset--color--primary) 0%,var(--wp--preset--color--foreground) 100%);--wp--preset--gradient--diagonal-secondary-to-background: linear-gradient(to bottom right,var(--wp--preset--color--secondary) 50%,var(--wp--preset--color--background) 50%);--wp--preset--gradient--diagonal-background-to-secondary: linear-gradient(to bottom right,var(--wp--preset--color--background) 50%,var(--wp--preset--color--secondary) 50%);--wp--preset--gradient--diagonal-tertiary-to-background: linear-gradient(to bottom right,var(--wp--preset--color--tertiary) 50%,var(--wp--preset--color--background) 50%);--wp--preset--gradient--diagonal-background-to-tertiary: linear-gradient(to bottom right,var(--wp--preset--color--background) 50%,var(--wp--preset--color--tertiary) 50%);--wp--preset--duotone--dark-grayscale: url(\'#wp-duotone-dark-grayscale\');--wp--preset--duotone--grayscale: url(\'#wp-duotone-grayscale\');--wp--preset--duotone--purple-yellow: url(\'#wp-duotone-purple-yellow\');--wp--preset--duotone--blue-red: url(\'#wp-duotone-blue-red\');--wp--preset--duotone--midnight: url(\'#wp-duotone-midnight\');--wp--preset--duotone--magenta-yellow: url(\'#wp-duotone-magenta-yellow\');--wp--preset--duotone--purple-green: url(\'#wp-duotone-purple-green\');--wp--preset--duotone--blue-orange: url(\'#wp-duotone-blue-orange\');--wp--preset--duotone--foreground-and-background: url(\'#wp-duotone-foreground-and-background\');--wp--preset--duotone--foreground-and-secondary: url(\'#wp-duotone-foreground-and-secondary\');--wp--preset--duotone--foreground-and-tertiary: url(\'#wp-duotone-foreground-and-tertiary\');--wp--preset--duotone--primary-and-background: url(\'#wp-duotone-primary-and-background\');--wp--preset--duotone--primary-and-secondary: url(\'#wp-duotone-primary-and-secondary\');--wp--preset--duotone--primary-and-tertiary: url(\'#wp-duotone-primary-and-tertiary\');--wp--preset--font-size--small: 1rem;--wp--preset--font-size--medium: 1.125rem;--wp--preset--font-size--large: 1.75rem;--wp--preset--font-size--x-large: clamp(1.75rem, 3vw, 2.25rem);--wp--preset--font-family--system-font: -apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,Oxygen-Sans,Ubuntu,Cantarell,\"Helvetica Neue\",sans-serif;--wp--preset--font-family--source-serif-pro: \"Source Serif Pro\", serif;--wp--custom--spacing--small: max(1.25rem, 5vw);--wp--custom--spacing--medium: clamp(2rem, 8vw, calc(4 * var(--wp--style--block-gap)));--wp--custom--spacing--large: clamp(4rem, 10vw, 8rem);--wp--custom--spacing--outer: var(--wp--custom--spacing--small, 1.25rem);--wp--custom--typography--font-size--huge: clamp(2.25rem, 4vw, 2.75rem);--wp--custom--typography--font-size--gigantic: clamp(2.75rem, 6vw, 3.25rem);--wp--custom--typography--font-size--colossal: clamp(3.25rem, 8vw, 6.25rem);--wp--custom--typography--line-height--tiny: 1.15;--wp--custom--typography--line-height--small: 1.2;--wp--custom--typography--line-height--medium: 1.4;--wp--custom--typography--line-height--normal: 1.6;}body { margin: 0; }body{background-color: var(--wp--preset--color--background);color: var(--wp--preset--color--foreground);font-family: var(--wp--preset--font-family--system-font);font-size: var(--wp--preset--font-size--medium);line-height: var(--wp--custom--typography--line-height--normal);--wp--style--block-gap: 1.5rem;}.wp-site-blocks > .alignleft { float: left; margin-right: 2em; }.wp-site-blocks > .alignright { float: right; margin-left: 2em; }.wp-site-blocks > .aligncenter { justify-content: center; margin-left: auto; margin-right: auto; }.wp-site-blocks > * { margin-block-start: 0; margin-block-end: 0; }.wp-site-blocks > * + * { margin-block-start: var( --wp--style--block-gap ); }h1{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--custom--typography--font-size--colossal);font-weight: 300;line-height: var(--wp--custom--typography--line-height--tiny);}h2{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--custom--typography--font-size--gigantic);font-weight: 300;line-height: var(--wp--custom--typography--line-height--small);}h3{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--custom--typography--font-size--huge);font-weight: 300;line-height: var(--wp--custom--typography--line-height--tiny);}h4{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--preset--font-size--x-large);font-weight: 300;line-height: var(--wp--custom--typography--line-height--tiny);}h5{font-family: var(--wp--preset--font-family--system-font);font-size: var(--wp--preset--font-size--medium);font-weight: 700;line-height: var(--wp--custom--typography--line-height--normal);text-transform: uppercase;}h6{font-family: var(--wp--preset--font-family--system-font);font-size: var(--wp--preset--font-size--medium);font-weight: 400;line-height: var(--wp--custom--typography--line-height--normal);text-transform: uppercase;}a{color: var(--wp--preset--color--foreground);}.wp-block-button__link{background-color: var(--wp--preset--color--primary);border-radius: 0;color: var(--wp--preset--color--background);font-size: var(--wp--preset--font-size--medium);}.wp-block-post-title{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--custom--typography--font-size--gigantic);font-weight: 300;line-height: var(--wp--custom--typography--line-height--tiny);}.wp-block-post-comments{padding-top: var(--wp--custom--spacing--small);}.wp-block-pullquote{border-width: 1px 0;}.wp-block-query-title{font-family: var(--wp--preset--font-family--source-serif-pro);font-size: var(--wp--custom--typography--font-size--gigantic);font-weight: 300;line-height: var(--wp--custom--typography--line-height--small);}.wp-block-quote{border-width: 1px;}.wp-block-site-title{font-family: var(--wp--preset--font-family--system-font);font-size: var(--wp--preset--font-size--medium);font-weight: normal;line-height: var(--wp--custom--typography--line-height--normal);}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-foreground-color{color: var(--wp--preset--color--foreground) !important;}.has-background-color{color: var(--wp--preset--color--background) !important;}.has-primary-color{color: var(--wp--preset--color--primary) !important;}.has-secondary-color{color: var(--wp--preset--color--secondary) !important;}.has-tertiary-color{color: var(--wp--preset--color--tertiary) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-foreground-background-color{background-color: var(--wp--preset--color--foreground) !important;}.has-background-background-color{background-color: var(--wp--preset--color--background) !important;}.has-primary-background-color{background-color: var(--wp--preset--color--primary) !important;}.has-secondary-background-color{background-color: var(--wp--preset--color--secondary) !important;}.has-tertiary-background-color{background-color: var(--wp--preset--color--tertiary) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-foreground-border-color{border-color: var(--wp--preset--color--foreground) !important;}.has-background-border-color{border-color: var(--wp--preset--color--background) !important;}.has-primary-border-color{border-color: var(--wp--preset--color--primary) !important;}.has-secondary-border-color{border-color: var(--wp--preset--color--secondary) !important;}.has-tertiary-border-color{border-color: var(--wp--preset--color--tertiary) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-vertical-secondary-to-tertiary-gradient-background{background: var(--wp--preset--gradient--vertical-secondary-to-tertiary) !important;}.has-vertical-secondary-to-background-gradient-background{background: var(--wp--preset--gradient--vertical-secondary-to-background) !important;}.has-vertical-tertiary-to-background-gradient-background{background: var(--wp--preset--gradient--vertical-tertiary-to-background) !important;}.has-diagonal-primary-to-foreground-gradient-background{background: var(--wp--preset--gradient--diagonal-primary-to-foreground) !important;}.has-diagonal-secondary-to-background-gradient-background{background: var(--wp--preset--gradient--diagonal-secondary-to-background) !important;}.has-diagonal-background-to-secondary-gradient-background{background: var(--wp--preset--gradient--diagonal-background-to-secondary) !important;}.has-diagonal-tertiary-to-background-gradient-background{background: var(--wp--preset--gradient--diagonal-tertiary-to-background) !important;}.has-diagonal-background-to-tertiary-gradient-background{background: var(--wp--preset--gradient--diagonal-background-to-tertiary) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}.has-system-font-font-family{font-family: var(--wp--preset--font-family--system-font) !important;}.has-source-serif-pro-font-family{font-family: var(--wp--preset--font-family--source-serif-pro) !important;}', 'no'),
(176, '_transient_timeout_global_styles_svg_filters_twentytwentytwo', '1659341247', 'no'),
(177, '_transient_global_styles_svg_filters_twentytwentytwo', '<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-dark-grayscale\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 0.49803921568627\" /><feFuncG type=\"table\" tableValues=\"0 0.49803921568627\" /><feFuncB type=\"table\" tableValues=\"0 0.49803921568627\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-grayscale\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 1\" /><feFuncG type=\"table\" tableValues=\"0 1\" /><feFuncB type=\"table\" tableValues=\"0 1\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-purple-yellow\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.54901960784314 0.98823529411765\" /><feFuncG type=\"table\" tableValues=\"0 1\" /><feFuncB type=\"table\" tableValues=\"0.71764705882353 0.25490196078431\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-blue-red\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 1\" /><feFuncG type=\"table\" tableValues=\"0 0.27843137254902\" /><feFuncB type=\"table\" tableValues=\"0.5921568627451 0.27843137254902\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-midnight\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 0\" /><feFuncG type=\"table\" tableValues=\"0 0.64705882352941\" /><feFuncB type=\"table\" tableValues=\"0 1\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-magenta-yellow\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.78039215686275 1\" /><feFuncG type=\"table\" tableValues=\"0 0.94901960784314\" /><feFuncB type=\"table\" tableValues=\"0.35294117647059 0.47058823529412\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-purple-green\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.65098039215686 0.40392156862745\" /><feFuncG type=\"table\" tableValues=\"0 1\" /><feFuncB type=\"table\" tableValues=\"0.44705882352941 0.4\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-blue-orange\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.098039215686275 1\" /><feFuncG type=\"table\" tableValues=\"0 0.66274509803922\" /><feFuncB type=\"table\" tableValues=\"0.84705882352941 0.41960784313725\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-foreground-and-background\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 1\" /><feFuncG type=\"table\" tableValues=\"0 1\" /><feFuncB type=\"table\" tableValues=\"0 1\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-foreground-and-secondary\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 1\" /><feFuncG type=\"table\" tableValues=\"0 0.88627450980392\" /><feFuncB type=\"table\" tableValues=\"0 0.78039215686275\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-foreground-and-tertiary\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0 0.96470588235294\" /><feFuncG type=\"table\" tableValues=\"0 0.96470588235294\" /><feFuncB type=\"table\" tableValues=\"0 0.96470588235294\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-primary-and-background\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.10196078431373 1\" /><feFuncG type=\"table\" tableValues=\"0.27058823529412 1\" /><feFuncB type=\"table\" tableValues=\"0.28235294117647 1\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-primary-and-secondary\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.10196078431373 1\" /><feFuncG type=\"table\" tableValues=\"0.27058823529412 0.88627450980392\" /><feFuncB type=\"table\" tableValues=\"0.28235294117647 0.78039215686275\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 0 0\" width=\"0\" height=\"0\" focusable=\"false\" role=\"none\" style=\"visibility: hidden; position: absolute; left: -9999px; overflow: hidden;\" ><defs><filter id=\"wp-duotone-primary-and-tertiary\"><feColorMatrix color-interpolation-filters=\"sRGB\" type=\"matrix\" values=\" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 \" /><feComponentTransfer color-interpolation-filters=\"sRGB\" ><feFuncR type=\"table\" tableValues=\"0.10196078431373 0.96470588235294\" /><feFuncG type=\"table\" tableValues=\"0.27058823529412 0.96470588235294\" /><feFuncB type=\"table\" tableValues=\"0.28235294117647 0.96470588235294\" /><feFuncA type=\"table\" tableValues=\"1 1\" /></feComponentTransfer><feComposite in2=\"SourceGraphic\" operator=\"in\" /></filter></defs></svg>', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_postmeta`
--

CREATE TABLE `wp_d2fwj6_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_postmeta`
--

INSERT INTO `wp_d2fwj6_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_posts`
--

CREATE TABLE `wp_d2fwj6_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_posts`
--

INSERT INTO `wp_d2fwj6_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2022-07-31 05:23:39', '2022-07-31 05:23:39', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2022-07-31 05:23:39', '2022-07-31 05:23:39', '', 0, 'https://www.jbaagency.net/backup/?p=1', 0, 'post', '', 1),
(2, 1, '2022-07-31 05:23:39', '2022-07-31 05:23:39', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"https://www.jbaagency.net/backup/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2022-07-31 05:23:39', '2022-07-31 05:23:39', '', 0, 'https://www.jbaagency.net/backup/?page_id=2', 0, 'page', '', 0),
(3, 1, '2022-07-31 05:23:39', '2022-07-31 05:23:39', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: https://www.jbaagency.net/backup.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where your data is sent</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2022-07-31 05:23:39', '2022-07-31 05:23:39', '', 0, 'https://www.jbaagency.net/backup/?page_id=3', 0, 'page', '', 0),
(4, 1, '2022-07-31 05:25:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2022-07-31 05:25:50', '0000-00-00 00:00:00', '', 0, 'https://www.jbaagency.net/backup/?p=4', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_termmeta`
--

CREATE TABLE `wp_d2fwj6_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_terms`
--

CREATE TABLE `wp_d2fwj6_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_terms`
--

INSERT INTO `wp_d2fwj6_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_term_relationships`
--

CREATE TABLE `wp_d2fwj6_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_term_relationships`
--

INSERT INTO `wp_d2fwj6_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_term_taxonomy`
--

CREATE TABLE `wp_d2fwj6_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_term_taxonomy`
--

INSERT INTO `wp_d2fwj6_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_usermeta`
--

CREATE TABLE `wp_d2fwj6_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_usermeta`
--

INSERT INTO `wp_d2fwj6_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Indoploit1337'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_d2fwj6_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_d2fwj6_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"b84784b437c73ec5197c380c66320e7e98088e771663df92f245e6bb985ed21c\";a:4:{s:10:\"expiration\";i:1659417949;s:2:\"ip\";s:15:\"103.106.167.221\";s:2:\"ua\";s:111:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\";s:5:\"login\";i:1659245149;}}'),
(17, 1, 'wp_d2fwj6_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"103.106.167.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_d2fwj6_users`
--

CREATE TABLE `wp_d2fwj6_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_d2fwj6_users`
--

INSERT INTO `wp_d2fwj6_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'Indoploit1337', '$P$B1kLU5YDU0hVVDPr1mg7cJN550LEjm0', 'indoploit1337', 'pekebo3955@chimpad.com', 'https://www.jbaagency.net/backup', '2022-07-31 05:23:39', '', 0, 'Indoploit1337');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_blog_review`
--
ALTER TABLE `tbl_blog_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `tbl_blog_review_reply`
--
ALTER TABLE `tbl_blog_review_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `wl_application_form`
--
ALTER TABLE `wl_application_form`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `wl_app_ack_signatory`
--
ALTER TABLE `wl_app_ack_signatory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_ack_witness`
--
ALTER TABLE `wl_app_ack_witness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_cert_guarantor`
--
ALTER TABLE `wl_app_cert_guarantor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_cert_witness`
--
ALTER TABLE `wl_app_cert_witness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_cirt_gua_signatory`
--
ALTER TABLE `wl_app_cirt_gua_signatory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_cirt_gua_witness`
--
ALTER TABLE `wl_app_cirt_gua_witness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_soft_trade`
--
ALTER TABLE `wl_app_soft_trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_app_trade_reference`
--
ALTER TABLE `wl_app_trade_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_auto_respond_mails`
--
ALTER TABLE `wl_auto_respond_mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_banners`
--
ALTER TABLE `wl_banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `wl_blogs`
--
ALTER TABLE `wl_blogs`
  ADD PRIMARY KEY (`blogs_id`);

--
-- Indexes for table `wl_blogs_media`
--
ALTER TABLE `wl_blogs_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_products_images_products_id` (`blogs_id`);

--
-- Indexes for table `wl_brands`
--
ALTER TABLE `wl_brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `idx_mfg_name_zen` (`brand_name`);

--
-- Indexes for table `wl_brouchers`
--
ALTER TABLE `wl_brouchers`
  ADD PRIMARY KEY (`broucher_id`),
  ADD KEY `idx_mfg_name_zen` (`broucher_name`);

--
-- Indexes for table `wl_categories`
--
ALTER TABLE `wl_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `wl_city`
--
ALTER TABLE `wl_city`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `idx_mfg_name_zen` (`city_name`);

--
-- Indexes for table `wl_cms_pages`
--
ALTER TABLE `wl_cms_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `friendly_url` (`friendly_url`);

--
-- Indexes for table `wl_countries`
--
ALTER TABLE `wl_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_COUNTRIES_NAME` (`country_name`);

--
-- Indexes for table `wl_currencies`
--
ALTER TABLE `wl_currencies`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `wl_dynamic_pages`
--
ALTER TABLE `wl_dynamic_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `wl_enquiry`
--
ALTER TABLE `wl_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_enquiry_details`
--
ALTER TABLE `wl_enquiry_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_events`
--
ALTER TABLE `wl_events`
  ADD PRIMARY KEY (`events_id`);

--
-- Indexes for table `wl_faq`
--
ALTER TABLE `wl_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `wl_gallery`
--
ALTER TABLE `wl_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `wl_gallery_normal_display`
--
ALTER TABLE `wl_gallery_normal_display`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_header_images`
--
ALTER TABLE `wl_header_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_jobs`
--
ALTER TABLE `wl_jobs`
  ADD PRIMARY KEY (`jobs_id`);

--
-- Indexes for table `wl_jobs_media`
--
ALTER TABLE `wl_jobs_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_meta_tags`
--
ALTER TABLE `wl_meta_tags`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `page_url` (`page_url`),
  ADD KEY `entity_type` (`entity_type`),
  ADD KEY `entity_id` (`entity_id`);

--
-- Indexes for table `wl_newsletters`
--
ALTER TABLE `wl_newsletters`
  ADD PRIMARY KEY (`subscriber_id`);

--
-- Indexes for table `wl_newsletter_groups`
--
ALTER TABLE `wl_newsletter_groups`
  ADD PRIMARY KEY (`newsletter_groups_id`);

--
-- Indexes for table `wl_newsletter_group_subscriber`
--
ALTER TABLE `wl_newsletter_group_subscriber`
  ADD PRIMARY KEY (`group_subscriber_id`);

--
-- Indexes for table `wl_newsletter_log`
--
ALTER TABLE `wl_newsletter_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_news_media`
--
ALTER TABLE `wl_news_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_products_images_products_id` (`products_id`);

--
-- Indexes for table `wl_order`
--
ALTER TABLE `wl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `wl_orders_products`
--
ALTER TABLE `wl_orders_products`
  ADD PRIMARY KEY (`orders_products_id`);

--
-- Indexes for table `wl_portfolio`
--
ALTER TABLE `wl_portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `wl_products`
--
ALTER TABLE `wl_products`
  ADD PRIMARY KEY (`products_id`);

--
-- Indexes for table `wl_products_media`
--
ALTER TABLE `wl_products_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_products_images_products_id` (`products_id`);

--
-- Indexes for table `wl_products_related`
--
ALTER TABLE `wl_products_related`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_review`
--
ALTER TABLE `wl_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `wl_services`
--
ALTER TABLE `wl_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `wl_services_city`
--
ALTER TABLE `wl_services_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_services_media`
--
ALTER TABLE `wl_services_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wl_service_categories`
--
ALTER TABLE `wl_service_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `wl_sessions`
--
ALTER TABLE `wl_sessions`
  ADD PRIMARY KEY (`id`,`ip_address`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `wl_team`
--
ALTER TABLE `wl_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `wl_testimonial`
--
ALTER TABLE `wl_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `wp_d2fwj6_commentmeta`
--
ALTER TABLE `wp_d2fwj6_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_d2fwj6_comments`
--
ALTER TABLE `wp_d2fwj6_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_d2fwj6_links`
--
ALTER TABLE `wp_d2fwj6_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_d2fwj6_options`
--
ALTER TABLE `wp_d2fwj6_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_d2fwj6_postmeta`
--
ALTER TABLE `wp_d2fwj6_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_d2fwj6_posts`
--
ALTER TABLE `wp_d2fwj6_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_d2fwj6_termmeta`
--
ALTER TABLE `wp_d2fwj6_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_d2fwj6_terms`
--
ALTER TABLE `wp_d2fwj6_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_d2fwj6_term_relationships`
--
ALTER TABLE `wp_d2fwj6_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_d2fwj6_term_taxonomy`
--
ALTER TABLE `wp_d2fwj6_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_d2fwj6_usermeta`
--
ALTER TABLE `wp_d2fwj6_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_d2fwj6_users`
--
ALTER TABLE `wp_d2fwj6_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_blog_review`
--
ALTER TABLE `tbl_blog_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_blog_review_reply`
--
ALTER TABLE `tbl_blog_review_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `wl_application_form`
--
ALTER TABLE `wl_application_form`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `wl_app_ack_signatory`
--
ALTER TABLE `wl_app_ack_signatory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `wl_app_ack_witness`
--
ALTER TABLE `wl_app_ack_witness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wl_app_cert_guarantor`
--
ALTER TABLE `wl_app_cert_guarantor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wl_app_cert_witness`
--
ALTER TABLE `wl_app_cert_witness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wl_app_cirt_gua_signatory`
--
ALTER TABLE `wl_app_cirt_gua_signatory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wl_app_cirt_gua_witness`
--
ALTER TABLE `wl_app_cirt_gua_witness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wl_app_soft_trade`
--
ALTER TABLE `wl_app_soft_trade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `wl_app_trade_reference`
--
ALTER TABLE `wl_app_trade_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wl_auto_respond_mails`
--
ALTER TABLE `wl_auto_respond_mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wl_banners`
--
ALTER TABLE `wl_banners`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wl_blogs`
--
ALTER TABLE `wl_blogs`
  MODIFY `blogs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wl_blogs_media`
--
ALTER TABLE `wl_blogs_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wl_brands`
--
ALTER TABLE `wl_brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `wl_brouchers`
--
ALTER TABLE `wl_brouchers`
  MODIFY `broucher_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `wl_categories`
--
ALTER TABLE `wl_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `wl_city`
--
ALTER TABLE `wl_city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `wl_cms_pages`
--
ALTER TABLE `wl_cms_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wl_countries`
--
ALTER TABLE `wl_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `wl_currencies`
--
ALTER TABLE `wl_currencies`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wl_dynamic_pages`
--
ALTER TABLE `wl_dynamic_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `wl_enquiry`
--
ALTER TABLE `wl_enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `wl_enquiry_details`
--
ALTER TABLE `wl_enquiry_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wl_events`
--
ALTER TABLE `wl_events`
  MODIFY `events_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wl_faq`
--
ALTER TABLE `wl_faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wl_gallery`
--
ALTER TABLE `wl_gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `wl_gallery_normal_display`
--
ALTER TABLE `wl_gallery_normal_display`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wl_header_images`
--
ALTER TABLE `wl_header_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `wl_jobs`
--
ALTER TABLE `wl_jobs`
  MODIFY `jobs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wl_jobs_media`
--
ALTER TABLE `wl_jobs_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wl_meta_tags`
--
ALTER TABLE `wl_meta_tags`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=554;

--
-- AUTO_INCREMENT for table `wl_newsletters`
--
ALTER TABLE `wl_newsletters`
  MODIFY `subscriber_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `wl_newsletter_groups`
--
ALTER TABLE `wl_newsletter_groups`
  MODIFY `newsletter_groups_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wl_newsletter_group_subscriber`
--
ALTER TABLE `wl_newsletter_group_subscriber`
  MODIFY `group_subscriber_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wl_newsletter_log`
--
ALTER TABLE `wl_newsletter_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wl_news_media`
--
ALTER TABLE `wl_news_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `wl_order`
--
ALTER TABLE `wl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `wl_orders_products`
--
ALTER TABLE `wl_orders_products`
  MODIFY `orders_products_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `wl_portfolio`
--
ALTER TABLE `wl_portfolio`
  MODIFY `portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wl_products`
--
ALTER TABLE `wl_products`
  MODIFY `products_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `wl_products_media`
--
ALTER TABLE `wl_products_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `wl_products_related`
--
ALTER TABLE `wl_products_related`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `wl_review`
--
ALTER TABLE `wl_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wl_services`
--
ALTER TABLE `wl_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `wl_services_city`
--
ALTER TABLE `wl_services_city`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `wl_services_media`
--
ALTER TABLE `wl_services_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `wl_service_categories`
--
ALTER TABLE `wl_service_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `wl_team`
--
ALTER TABLE `wl_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wl_testimonial`
--
ALTER TABLE `wl_testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_commentmeta`
--
ALTER TABLE `wp_d2fwj6_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_comments`
--
ALTER TABLE `wp_d2fwj6_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_links`
--
ALTER TABLE `wp_d2fwj6_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_options`
--
ALTER TABLE `wp_d2fwj6_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_postmeta`
--
ALTER TABLE `wp_d2fwj6_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_posts`
--
ALTER TABLE `wp_d2fwj6_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_termmeta`
--
ALTER TABLE `wp_d2fwj6_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_terms`
--
ALTER TABLE `wp_d2fwj6_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_term_taxonomy`
--
ALTER TABLE `wp_d2fwj6_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_usermeta`
--
ALTER TABLE `wp_d2fwj6_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `wp_d2fwj6_users`
--
ALTER TABLE `wp_d2fwj6_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
