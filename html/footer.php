<style>@charset "UTF-8"; @keyframes fadeIn {  to {    opacity: 1;  }}.fade-in {  opacity: 0;  animation: fadeIn .5s ease-in 1 forwards;}.is-paused {  animation-play-state: paused;}</style>

<!-- Start Quick Call Buttons -->
<div class="quick-call-button"></div>
<div class="call-now-button" id="draggable">
<div>
<p class="call-text"> Call Us </p>
<a href="#" id="quickcallbutton">
<div class="quick-alo-ph-circle active"></div>
<div class="quick-alo-ph-circle-fill active"></div>
<div class="quick-alo-ph-img-circle shake"></div>
</a> </div>
</div>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-12 col-sm-3">
				<div class="title_footer">
					<h3>Follow Us</h3>
				</div>
				<ul class="scoalmedia">
					<li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
					<li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
					<li><a href="#"><i class="fa fa-instagram"></i> Instagram</a></li>
					<li><a href="#"><i class="fa fa-youtube"></i> youtube</a></li>
				</ul>

				<div class="title_footer">
					<h3>Be with Us</h3>
				</div>
				<form class="newsll">
					<input type="email" name="" placeholder="Email Id">
					<button class="inerbt" type="submit"><i class="fa fa-envelope-o"></i></button>
				</form>
			</div>


			<div class="col-md-6 col-xs-12 col-sm-6">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d462560.3011806427!2d54.94754379860844!3d25.076381472700533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0xbde66e5084295162!2sDubai!5e0!3m2!1sen!2sae!4v1649762691771!5m2!1sen!2sae" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="title_footer">
							<h3>Dubai Address</h3>
						</div>
						<ul class="address_il">
							<li><a href="#"><i class="fa fa-phone"></i> +12 0112545</a></li>
							<li><a href="#"><i class="fa fa-envelope"></i> info@jbaagency.com</a></li>
							<li><a href="#"><i class="fa fa-map-marker"></i> your address goes here Dubai</a></li>
							<li><a href="#"><i class="fa fa-globe"></i> www.jbaagency.com</a></li>
						</ul>
						<div class="qrcde"><img src="qrcode.jpg"></div>
					</div>
				</div>
			</div>


			<div class="col-md-3 col-xs-12 col-sm-3">
				<div class="title_footer">
					<h3>Get in Touch</h3>
				</div>
					
				<form class="footerfro">
					<input type="text" name="" placeholder="Name">
					<input type="number" name="" placeholder="Mobile Number">
					<input type="email" name="" placeholder="Email Id">
					<textarea rows="4" placeholder="Message"></textarea>
					<button class="btn btntbn" type="submit">Submit</button>
				</form>
				
			</div>

		</div>
	</div>
</footer>
 <div class="copyrights">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12">
 				<p>© Copyright JBA Agency. All rights reserved.</p>
 			</div>
 		</div>
 	</div>
 </div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>

<script src="libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="libs/gsap/3.7.1/Draggable.min.js" ></script>
    <script src="libs/gsap/3.7.1/ScrollTrigger.min.js" ></script>
    <script src='libs/three.js/r83/three.js'></script>
	<script src='libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script src="js1/clapatwebgl.js"></script>
	<script src="js1/plugins.js"></script>
    <script src="js1/common.js"></script>
    <script src="js1/scripts.js"></script>
<script src="js/main.js"></script>

<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/gsap-latest-beta.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollToPlugin3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer1", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper1",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper1")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>



<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer2", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper2",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper2")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer3", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper3",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper3")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer4", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper4",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper4")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>

<!-- <script type="text/javascript">
	var showChar = 256;
var ellipsestext = "...";
var moretext = "See More";
var lesstext = "See Less";
$('.comments-space').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});
</script> -->


</body>
</html>

