<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title><link href="favicon.png" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@600&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/prettyphoto.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <link href="style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="homepage innerpages">

<?php include('header.php') ?>

<div class="innerlogo"><img src="images/logo.png"></div>

<div class="breadcumlisting">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Media</li>
        </ol>
      </div>
    </div>
  </div>
</div>


<div class="details_portfolio">
    <div class="container">
        <div class="row">
            <div  class="col-md-12 col-xs-12 col-sm-12">
                <div class="portfolio_div">
                   
                    <div class="portfolio-content"> 
                        <ul class="portfolio-categ filter">
                          <li class="all"><a href="#" >All</a></li>
                            <li class="cat-item-1"><a href="#">Creative</a></li>
                            <li class="cat-item-2"><a href="#">Branding</a></li>
                            <li class="cat-item-3"><a href="#">Media</a></li>
                            <li class="cat-item-4"><a href="#">Events</a></li>
                            <li class="cat-item-5"><a href="#">Branding</a></li>
                            <li class="cat-item-6"><a href="#">Webs & Application</a></li>
                          </ul>
                        <ul class="portfolio-area"> 
                           <li class="portfolio_item portfolio-item2 " data-id="id-0" data-type="cat-item-1"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port1.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-1" data-type="cat-item-2"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port2.jpg" /> 
                                    </a>
                                </div>
                            </li>

                            <li class="portfolio_item portfolio-item2 " data-id="id-3" data-type="cat-item-4"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port3.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-2" data-type="cat-item-3"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port4.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-1" data-type="cat-item-2"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port2.jpg" /> 
                                    </a>
                                </div>
                            </li>

                            <li class="portfolio_item portfolio-item2 " data-id="id-3" data-type="cat-item-4"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port3.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-2" data-type="cat-item-3"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port4.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-1" data-type="cat-item-2"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port2.jpg" /> 
                                    </a>
                                </div>
                            </li>

                            <li class="portfolio_item portfolio-item2 " data-id="id-3" data-type="cat-item-4"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port3.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-2" data-type="cat-item-3"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port4.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            <li class="portfolio_item portfolio-item2 " data-id="id-1" data-type="cat-item-2"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port2.jpg" /> 
                                    </a>
                                </div>
                            </li>

                            <li class="portfolio_item portfolio-item2 " data-id="id-3" data-type="cat-item-4"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="portfolio-inner.php" title="">
                                        <img src="images/port3.jpg" /> 
                                    </a>
                                </div>
                            </li>
                            
                        </ul>
                
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php include('footer-inner.php') ?>