<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title><link href="favicon.png" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@600&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <link href="style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="homepage">

<?php include('header.php') ?>


<div class="main-slider-top">
     <a class="navbar-brand" href="index.php"><h1><img src="images/logo.png" alt="" title=""></h1></a>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12 co-sm-12 pad-0">
                <div class="slider-area">
                    <div class="sloganmiddle">
                        <h1>Bringing Ideas To Life</h1>
                        <ul>
                            <li><a href="#">Creative</a></li>
                            <li><a href="#">Branding</a></li>
                            <li><a href="#">Media</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">web & Application</a></li>
                        </ul>
                    </div>
                    <div class="slider-fluid">
                        <div class="mainslider_js slider_dots owl-theme">
                            <div>
                                <video width="" muted="" autoplay="" loop="">
                                  <source src="slider_video.mp4" type="video/mp4">
                                  <source src="slider_video.ogg" type="video/ogg">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>

                            <div><img src="images/slider-img4.jpg"></div>
                            
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>

    

</div>


<section class="commondiv sevice" style=" padding: 100px 0px;" id="Services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Services</h3>
            </div>
        </div>
    </div>
</section>

<main>
 
    
    
    
    <div class="wrapper" id="js-wrapper">
        <ul class="stories">
      <li><a class="trigger11" href="#story1">Scene 1</a></li>
            <li><a class="trigger22" href="#story2">Scene 2</a></li>
            <li><a class="trigger33"href="#story3">Scene 3</a></li>
            <li><a class="trigger44"href="#story4">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer">
          
      
        <section class="section bg_1 combg" id="story1">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Creative</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            
    
    

                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_2 combg" id="story2">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_3 combg" id="story3">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_4 combg" id="story3">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>


  <div class="wrapper" id="js-wrapper1">
        <ul class="stories">
      <li><a class="trigger1" href="#story11">Scene 1</a></li>
            <li><a class="trigger2" href="#story22">Scene 2</a></li>
            <li><a class="trigger3"href="#story33">Scene 3</a></li>
            <li><a class="trigger3"href="#story44">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer1">
          
      
        <section class="section bg_brnad_1 combg" id="story11">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Branding</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_brnad_2 combg" id="story22">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_brnad_3 combg" id="story33">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_brnad_4 combg" id="story33">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    

<style type="text/css">


.wrapper {
  width: 100%;
  height: 100vh !important;
  overflow: hidden;
}
.wrapper .scroll-title {
  font-size: 21px;
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
  font-weight: 100;
}
.wrapper .cta {
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
}
.wrapper ul.stories {

    position: absolute;
    top: 50%;
    right: 20px;
    transform: translate(-50%, 0);
    display: block;
    z-index: 20;
    list-style: none;
    width: auto;
    padding: 0;
}
.wrapper ul.stories li {
   display: none;
}
.wrapper ul.stories li a {
    color: white;
    font-size: 0px;
    background: #f36f21;
    width: 12px;
    height: 12px;
    display: block;
    margin: 10px 0;
    border-radius: 50%;
}
.wrapper .progress-bar-container {
  max-width: 300px;
  height: 10px;
  border: #f1f0f0;
  border-radius: 10px;
  position: absolute;
  bottom: 100px;
  z-index: 20;
  display: table;
  overflow: hidden;
  width: 100%;
  background: #222;
  margin: 0 auto;
  left: 50%;
  right: 0;
  transform: translate(-50%, 0);
}
.wrapper .progress-bar-container .progress-bar {
  width: 0%;
  height: 20px;
  position: absolute;
  left: 0;
  top: 0;
  background: red;
  transition: all 0.3s ease;
}
.wrapper .sections {
  width: 400%;
  height: 100%;
}
.wrapper .sections .section {
  height: 100%;
  width: calc(100% / 4);
  float: left;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
}

.wrapper .sections .section .sectionTitle {
  font-size: 30px;
  color: #fff;
  position: absolute;
  z-index: 5;
}
</style>




  <div class="wrapper" id="js-wrapper2">
        <ul class="stories">
      <li><a class="trigger1" href="#story12">Scene 1</a></li>
            <li><a class="trigger2" href="#story23">Scene 2</a></li>
            <li><a class="trigger3"href="#story34">Scene 3</a></li>
            <li><a class="trigger3"href="#story45">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer2">
          
      
        <section class="section bg_media_1 combg" id="story12">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Media</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_media_2 combg" id="story23">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_media_3 combg" id="story34">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_media_4 combg" id="story35">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    

  <div class="wrapper" id="js-wrapper3">
        <ul class="stories">
      <li><a class="trigger1" href="#story13">Scene 1</a></li>
            <li><a class="trigger2" href="#story24">Scene 2</a></li>
            <li><a class="trigger3"href="#story35">Scene 3</a></li>
            <li><a class="trigger3"href="#story46">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer3">
          
      
        <section class="section bg_events_1 combg" id="story13">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Events</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_events_2 combg" id="story23">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_events_3 combg" id="story34">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_events_4 combg" id="story35">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    



  <div class="wrapper" id="js-wrapper4">
        <ul class="stories">
      <li><a class="trigger1" href="#story13">Scene 1</a></li>
            <li><a class="trigger2" href="#story24">Scene 2</a></li>
            <li><a class="trigger3"href="#story35">Scene 3</a></li>
            <li><a class="trigger3"href="#story46">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer4">
          
      
        <section class="section bg_webs_1 combg" id="story14">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Webs & Application</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_webs_2 combg" id="story25">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_webs_3 combg" id="story36">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_webs_4 combg" id="story37">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    










<section class="commondiv" id="Clients">
    
                <div class="heading text-center">
                    <h3>Clients</h3>
                </div>

                <div class="slider_effect">
                    <div class="width_lll">
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                    </div>
                </div>

                <div class="slider_effect">
                    <div class="width_lll_1">
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                    </div>
                </div>


                <div class="slider_effect">
                    <div class="width_lll">
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                        <div class="outlogoidv">
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                            <div class="slider_loo"><img src="images/brand2.png"></div>
                            <div class="slider_loo"><img src="images/brand3.png"></div>
                            <div class="slider_loo"><img src="images/brand4.png"></div>
                            <div class="slider_loo"><img src="images/brand5.png"></div>
                            <div class="slider_loo"><img src="images/brand6.png"></div>
                            <div class="slider_loo"><img src="images/brand1.png"></div>
                        </div>
                    </div>
                </div>
</section>


<section class="commondiv" style="background-color: #000; padding: 100px 0px;" id="Portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Portfolio</h3>
                <br>
            <br>
            <br>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="works">
                
                    <div>
                        <a href="portfolio.php" class="work_mg">
                            <img src="images/healthcare1.jpg">
                            <span>Creative</span>
                        </a>
                    </div>
                    <div>
                        <a href="portfolio.php" class="work_mg">
                            <img src="images/healthcare2.jpg">
                            <span>Media</span>
                        </a>
                    </div>
                    <div>
                        <a href="portfolio.php" class="work_mg">
                            <img src="images/healthcare.jpg">
                            <span>Application</span>
                        </a>
                    </div>
                    <div>
                        <a href="portfolio.php" class="work_mg">
                            <img src="images/healthcare4.jpg">
                            <span>Branding</span>
                        </a>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
</section>



<section class="commondiv" >
   
<div class="content-row row_padding_top light-section">
                                
 <!-- <a href="#footer" class="span_contact">
      <div class="inner_spancon"><div class="arrow_svg">
        <img src="images/arrowicon.png">
      </div>
       get in touch
    </div>
  </a>-->
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
    
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-backward secondary-font">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
     <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
</div> 
</section>

<?php include('footer.php') ?>

    
