<div class="mainheader">

<header class="header-area">
      
    <nav class="navbar" id="">

      <div class="container">

          

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#Services">Services</a></li>
            <li><a href="portfolio.php">Portfolio</a></li>
            <li><a href="#Clients">Clients</a></li>
            <li><a href="#footer">Contact</a></li>

            <li class="lat"><span class="opennav_div" onclick="openNav()">
            <div></div>  
            <div></div>  
            <div></div>  
          </span></li>

            <li class="lastchild lat"><a href="#myModal" data-toggle="modal">Start Project</a></li>

        
     
    </div>
  </div>
</nav>

</header>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times; Close Panel</a>
  <a href="index.php">Home</a>
  <a href="#">About</a>
  <button class="dropdown-btn">Services 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="#Services">Creative</a>
    <a href="#Services">Branding</a>
    <a href="#Services">Media</a>
    <a href="#Services">Events</a>
    <a href="#Services">Webs & Application</a>
  </div>

  <a href="#Clients">Clients</a>

  <button class="dropdown-btn">Portfolio 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="portfolio.php">Creative</a>
    <a href="portfolio.php">Branding</a>
    <a href="portfolio.php">Media</a>
    <a href="portfolio.php">Events</a>
    <a href="portfolio.php">Webs & Application</a>
    <a href="portfolio.php">View All</a>
  </div>
  <a href="#footer">Contact</a>
</div>



</div>





<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Inquiry Now</h4>
        </div>
        <div class="modal-body">
             <form method="post" action="inquiry.php">
             <input type="hidden" name="submit_contact" value="1">
             <input type="hidden" name="check_form" value="">
                <div class="form-group">
                  <label for="usr">Name:</label>
                  <input type="text" class="form-control" name="first_name" placeholder="Name*" required="" id="usr">
                </div>
                <div class="form-group">
                  <label for="pwd">Phone Number:</label>
                  <input type="number" onkeypress="return isNumberKey(event);" class="form-control" name="telephone" placeholder="Phone Number*" required="" id="pwd">
                </div>
                <div class="form-group">
                  <label for="pwd">Email Id:</label>
                  <input type="email" class="form-control" name="email" placeholder="Email Id*" required="" id="pwd">
                </div>

                <div class="form-group">
                     <label for="pwd">Message:</label>
                  <textarea class="form-control heightauto" style="height: 80px;" name="comments" rows="5" placeholder="Message" spellcheck="false"></textarea>
                </div>
                

            
                
                <div class="form-group">
                    <button type="snubmit" class="btn_btn m0">Submit</button>
                </div>

            </form>
        </div>
    
      </div>
    </div>
  </div>