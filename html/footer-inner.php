
<style>@charset "UTF-8"; @keyframes fadeIn {  to {    opacity: 1;  }}.fade-in {  opacity: 0;  animation: fadeIn .5s ease-in 1 forwards;}.is-paused {  animation-play-state: paused;}</style>

<!-- Start Quick Call Buttons -->
<div class="quick-call-button"></div>
<div class="call-now-button" id="draggable">
<div>
<p class="call-text"> Call Us </p>
<a href="#" id="quickcallbutton">
<div class="quick-alo-ph-circle active"></div>
<div class="quick-alo-ph-circle-fill active"></div>
<div class="quick-alo-ph-img-circle shake"></div>
</a> </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-3">
                <div class="title_footer">
                    <h3>Follow Us</h3>
                </div>
                <ul class="scoalmedia">
                    <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i> Instagram</a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i> youtube</a></li>
                </ul>

                <div class="title_footer">
                    <h3>Be with Us</h3>
                </div>
                <form class="newsll">
                    <input type="email" name="" placeholder="Email Id">
                    <button class="inerbt" type="submit"><i class="fa fa-envelope-o"></i></button>
                </form>
            </div>


            <div class="col-md-6 col-xs-12 col-sm-6">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d462560.3011806427!2d54.94754379860844!3d25.076381472700533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0xbde66e5084295162!2sDubai!5e0!3m2!1sen!2sae!4v1649762691771!5m2!1sen!2sae" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="title_footer">
                            <h3>Dubai Address</h3>
                        </div>
                        <ul class="address_il">
                            <li><a href="#"><i class="fa fa-phone"></i> +12 0112545</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@jbaagency.com</a></li>
                            <li><a href="#"><i class="fa fa-map-marker"></i> your address goes here Dubai</a></li>
                            <li><a href="#"><i class="fa fa-globe"></i> www.jbaagency.com</a></li>
                        </ul>
                        <div class="qrcde"><img src="qrcode.jpg"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-xs-12 col-sm-3">
                <div class="title_footer">
                    <h3>Get in Touch</h3>
                </div>
                    
                <form class="footerfro">
                    <input type="text" name="" placeholder="Name">
                    <input type="number" name="" placeholder="Mobile Number">
                    <input type="email" name="" placeholder="Email Id">
                    <textarea rows="4" placeholder="Message"></textarea>
                    <button class="btn btntbn" type="submit">Submit</button>
                </form>
                
            </div>

        </div>
    </div>
</footer>
 <div class="copyrights">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p>© Copyright JBA Agency. All rights reserved.</p>
            </div>
        </div>
    </div>
 </div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>


<script src="js/main.js"></script>


<script src="js/jquery.quicksand.js" type="text/javascript"></script>
<script src="js/jquery.easing.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/jquery.prettyphoto.js" type="text/javascript"></script> 


<script>



function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>



</body>
</html>

