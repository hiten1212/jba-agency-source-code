<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title><link href="favicon.png" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@600&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/prettyphoto.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style1.css">
    <link href="style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="homepage innerpages">

<?php include('header.php') ?>

<div class="innerlogo"><img src="images/logo.png"></div>

<div class="breadcumlisting">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item"><a href="portfolio.php">Portfolio</a></li>
          <li class="breadcrumb-item active" aria-current="page">Bringing Ideas</li>
        </ol>
      </div>
    </div>
  </div>
</div>


<div class="details_portfolio">
    <div class="container">
        <div class="row">
            <div  class="col-md-12 col-xs-12 col-sm-12">
                <div class="portfolio_div">
                   
                    <div class="innerslider">
                        <div>
                            <img src="images/port1.jpg">
                        </div>
                        <div>
                            <img src="images/port2.jpg">
                        </div>
                        <div>
                            <img src="images/port3.jpg">
                        </div> 
                    </div>

                    <div class="details_portfolio">
                        <h3>Bringing Ideas To Life</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer-inner.php') ?>