<style>@charset "UTF-8"; @keyframes fadeIn {  to {    opacity: 1;  }}.fade-in {  opacity: 0;  animation: fadeIn .5s ease-in 1 forwards;}.is-paused {  animation-play-state: paused;}</style>

<!-- Start Quick Call Buttons -->
<div class="quick-call-button"></div>
<div class="call-now-button" id="draggable">
<div>
<p class="call-text"> Call Us </p>
<a href="#" id="quickcallbutton">
<div class="quick-alo-ph-circle active"></div>
<div class="quick-alo-ph-circle-fill active"></div>
<div class="quick-alo-ph-img-circle shake"></div>
</a> </div>
</div>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-12 col-sm-3">
				<div class="title_footer">
					<h3>Follow Us</h3>
					
				</div>
				<?php 
//trace($this->uri->rsegments);
  
$facebook_link = $this->admin_info->facebook_link != null ? 'href="'.$this->admin_info->facebook_link.'" target="_blank"' : 'href="javascript:void(0);"';
$twitter_link = $this->admin_info->twitter_link != null ? 'href="'.$this->admin_info->twitter_link.'" target="_blank"' : 'href="javascript:void(0);"';
$google_link = $this->admin_info->google_link != null ? 'href="'.$this->admin_info->google_link.'" target="_blank"' : 'href="javascript:void(0);"';
$linkedin_link = $this->admin_info->linkedin_link != null ? 'href="'.$this->admin_info->linkedin_link.'" target="_blank"' : 'href="javascript:void(0);"';
$youtube_link = $this->admin_info->youtube_link != null ? 'href="'.$this->admin_info->youtube_link.'" target="_blank"' : 'href="javascript:void(0);"';
$instagram_link = $this->admin_info->instagram_link != null ? 'href="'.$this->admin_info->instagram_link.'" target="_blank"' : 'href="javascript:void(0);"';
$soundcloud_link = $this->admin_info->soundcloud_link != null ? 'href="'.$this->admin_info->soundcloud_link.'" target="_blank"' : 'href="javascript:void(0);"';
?>
				<ul class="scoalmedia">
					<li><a <?php echo $facebook_link;?>><i class="fa fa-facebook"></i> Facebook</a></li>
					<li><a <?php echo $twitter_link;?>><i class="fa fa-twitter"></i> twitter</a></li>
					<li><a <?php echo $instagram_link;?>><i class="fa fa-instagram"></i> Instagram</a></li>
					<li><a <?php echo $youtube_link;?>><i class="fa fa-youtube"></i> youtube</a></li>
				</ul>

				<div class="title_footer">
					<h3>Be with Us</h3>
				</div>
				<form class="newsll">
					<input type="email" name="" placeholder="Email Id">
					<button class="inerbt" type="submit"><i class="fa fa-envelope-o"></i></button>
				</form>
			</div>


			<div class="col-md-6 col-xs-12 col-sm-6">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
					<?php echo html_entity_decode($this->admin_info->domestic_google_map); ?>
						
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="title_footer">
							<h3>Dubai Address</h3>
						</div>
						<ul class="address_il">
							<li><a href="tel:<?php echo $this->admin_info->phone;?>"><i class="fa fa-phone"></i> +<?php echo $this->admin_info->phone;?></a></li>
							<li><a href="mailto:<?php echo $this->admin_info->admin_email;?>"><i class="fa fa-envelope"></i> <?php echo $this->admin_info->admin_email;?></a></li>
							<li><a href="#"><i class="fa fa-map-marker"></i> <?php echo $this->admin_info->address;?></a></li>
							<li><a href="#"><i class="fa fa-globe"></i> www.jbaagency.com</a></li>
						</ul>
						<div class="qrcde"><img src="<?php echo theme_url(); ?>images/qrcode.jpg"></div>
					</div>
				</div>
			</div>


			<div class="col-md-3 col-xs-12 col-sm-3">
				<div class="title_footer">
					<h3>Get in Touch</h3>
				</div>
					
				<form class="footerfro">
					<input type="text" name="" placeholder="Name">
					<input type="number" name="" placeholder="Mobile Number">
					<input type="email" name="" placeholder="Email Id">
					<textarea rows="4" placeholder="Message"></textarea>
					<button class="btn btntbn" type="submit">Submit</button>
				</form>
				
			</div>

		</div>
	</div>
</footer>
 <div class="copyrights">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12">
 				<p>© Copyright JBA Agency. All rights reserved.</p>
 			</div>
 		</div>
 	</div>
 </div>