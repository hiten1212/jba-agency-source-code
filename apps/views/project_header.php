<?php $this->load->helper(array('category/category'));
$this->load->model(array('gallery/gallery_model','service_category/service_category_model'));
//$this->load->model(array('dynamic_pages/dynamic_pages_model'));
		
$seg=$this->uri->segment(1); 
$seg2=$this->uri->rsegment(1); 
?>
<div class="mainheader">

<header class="header-area headerbackground" id="nav" style="background: <?php echo $this->admin_info->header_background!='' ? $this->admin_info->header_background:'#2c220e';  ?> !important">
      
    <nav class="navbar headerbackground" id=""  style="background: <?php echo $this->admin_info->header_background!='' ? $this->admin_info->header_background:'#2c220e';  ?>">

      <div class="container">

          

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url();?>">Home</a></li>
             <?php foreach($allcms_pages as $allcms_page) {?>
		    <li><a href="<?php echo base_url();?>#<?php echo $allcms_page['friendly_url']; ?>"><?php echo $allcms_page['page_name']; ?></a></li>
		   <?php } ?>            
            <li><a href="<?php echo base_url('gallery');?>"><?php echo $this->admin_info->portfolio_menu_text; ?></a></li>
            <li><a href="<?php echo base_url();?>#Clients"><?php echo $this->admin_info->clients_menu_text; ?></a></li>
            <li><a href="<?php echo base_url();?>#footer">Contact</a></li>

            <li class="lat"><span class="opennav_div" onclick="openNav()">
            <div></div>  
            <div></div>  
            <div></div>  
          </span></li>

          <?php
            $uri_segment = $this->uri->segment(1);

            if($uri_segment!='gallery')
            {
              ?>
               <li class="lastchild lat start_prjct_class"><a style="background: <?php echo $this->admin_info->start_project_background!='' ? $this->admin_info->start_project_background:'#f36f21';  ?>" href="#myModal" data-toggle="modal"><?php echo $this->admin_info->start_project_text; ?></a></li>
              <?php  
            }
          ?>
			  
			   
<?php if($this->admin_info->companyprofile_status ==0) {?>
<li class="lastchild lat"><a style="background: #000" href="<?php echo base_url();?>uploaded_files/companyprofile/<?php echo $this->admin_info->company_profile;?>" data-toggle="modal" target="_blank" download>Company Profile <i class="fa fa-sign-out " style="transform: rotate(90deg);"></i></a></li>
			  
                          <?php } ?>			  
            

        
     
    </div>
  </div>
</nav>

</header>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times; Close Panel</a>
  <a href="<?php echo base_url();?>">Home</a>
   <?php foreach($allcms_pages as $allcms_page) { 
     if($allcms_page['page_id'] == 1) { ?>
    <li><a href="<?php echo base_url();?>#<?php echo $allcms_page['friendly_url']; ?>"><?php echo $allcms_page['page_name']; ?></a></li>
   <?php }
    } ?>
  <?php 

 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
 //  $condtion_array['offset'] = 0;
 //  $condtion_array['limit'] = 6;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
    <button class="dropdown-btn">  <?php foreach($allcms_pages as $allcms_page) { 
     if($allcms_page['page_id'] == 3) { ?>
    <?php echo $allcms_page['page_name']; ?>
   <?php }
    } ?> 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
   <?php
	foreach($res as $val){
	 //$link_url = site_url($val['friendly_url']);
	 //echo $val['friendly_url'].'111111111';
	  $link_url = site_url().'#'.$val['friendly_url'];//.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
<a  href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a>
<?php }
if($total_categories >6)
{
?>
<!--<a  href="<?php //echo base_url('category');?>" title="View All &raquo;"><b>View All &raquo;</b></a>-->
<?php } ?>
   
  </div>
  <?php }?>

  <a href="<?php echo base_url();?>#Clients"><?php echo $this->admin_info->clients_menu_text; ?></a>
<?php  $condtion_array = array(
   'field' =>"gallery_id,gallery_title,parent_id,friendly_url,(SELECT COUNT(gallery_id) FROM wl_gallery AS b WHERE b.parent_id=a.gallery_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $offset = 0;
   $limit = 6;
   
   $param = array('status'=>'1','parent_id'=>'0','orderby'=>'sort_order asc');
	$res_products               = $this->gallery_model->get_gallery($limit,$offset,$param);  
  //echo_sql();
   $total_page_products	= get_found_rows();
   if($total_page_products > 0){
?>
  <button class="dropdown-btn"><?php echo $this->admin_info->portfolio_menu_text; ?>
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
   <?php
	foreach($res_products as $val){
	 $link_url = site_url($val['friendly_url']);
	 //$link_url = site_url().'#'.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
  <a  href="<?php echo $link_url;?>" title="<?php echo $val['gallery_title'];?>"><?php echo $val['gallery_title'];?></a>
<?php }
if($total_page_products >6)
{
?>
<a  href="<?php echo base_url('gallery');?>" title="View All &raquo;"><strong>View More</strong></a>
<?php } ?>
    
  </div>
   <?php } ?>
  <a href="<?php echo base_url();?>#footer">Contact</a>
	
	
<a style="background: #000" href="<?php echo base_url();?>uploaded_files/companyprofile/<?php echo $this->admin_info->company_profile;?>" data-toggle="modal" target="_blank" download>Company Profile <i class="fa fa-sign-out " style="transform: rotate(90deg);"></i></a>

<?php
 if($uri_segment!='gallery')
 {
?>
<a style="background: <?php echo $this->admin_info->start_project_background!='' ? $this->admin_info->start_project_background:'#f36f21';?>" href="#myModal" data-toggle="modal"><span style="color: #fff;"><?php echo $this->admin_info->start_project_text; ?></span></a>
<?php } ?>
</div>



</div>


 <li class="mobile_menu_click"><span class="opennav_div" onclick="openNav()">
            <div></div>  
            <div></div>  
            <div></div>  
          </span></li>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Inquiry Now</h4>
        </div>
        <div class="modal-body">
             <?php echo form_open('pages/contactus#cid');?>
             <input type="hidden" name="submit_contact" value="1">
             <input type="hidden" name="check_form" value="">
                <div class="form-group">
                  <label for="usr">Name:</label>
                  <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Name*" required="" id="usr"  value="<?php echo set_value('first_name');?>"><?php echo form_error('first_name');?>
                </div>
                <div class="form-group">
                  <label for="pwd">Phone Number:</label>
                  <input type="number" onkeypress="return isNumberKey(event);" class="form-control" name="mobile_number" id="mobile_number" placeholder="Phone Number*" required="" id="pwd"  value="<?php echo set_value('mobile_number');?>"><?php echo form_error('mobile_number');?>
                </div>
                <div class="form-group">
                  <label for="pwd">Email Id:</label>
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email Id*" required="" id="pwd"  value="<?php echo set_value('email');?>"><?php echo form_error('email');?>
                </div>

                <div class="form-group">
                     <label for="pwd">Message:</label>
                  <textarea class="form-control heightauto" style="height: 80px;" name="message" id="message"  rows="5" placeholder="Message" spellcheck="false" required=""><?php echo set_value('message');?></textarea><?php echo form_error('message');?>
                </div>
                

            
                
                <div class="form-group">
                    <button type="snubmit" class="btn_btn m0">Submit</button>
                </div>

            <?php echo form_close();?>
        </div>
    
      </div>
    </div>
  </div>