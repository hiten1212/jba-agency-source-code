<?php $this->load->helper(array('category/category'));
$this->load->model(array('gallery/gallery_model','service_category/service_category_model'));
//$this->load->model(array('dynamic_pages/dynamic_pages_model'));
		
$seg=$this->uri->segment(1); 
$seg2=$this->uri->rsegment(1); 
?>
<div class="mainheader">

<header class="header-area">
      
    <nav class="navbar" id="">

      <div class="container">

          

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url('aboutus');?>">About</a></li>
            <li><a href="<?php echo base_url();?>#Services">Services</a></li>
            <li><a href="<?php echo base_url('gallery');?>">Portfolio</a></li>
            <li><a href="<?php echo base_url();?>#Clients">Clients</a></li>
            <li><a href="<?php echo base_url();?>#footer">Contact</a></li>

            <li class="lat"><span class="opennav_div" onclick="openNav()">
            <div></div>  
            <div></div>  
            <div></div>  
          </span></li>

            <li class="lastchild lat"><a href="#myModal" data-toggle="modal">Start Project</a></li>

        
     
    </div>
  </div>
</nav>

</header>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times; Close Panel</a>
  <a href="<?php echo base_url();?>">Home</a>
  <a href="<?php echo base_url('aboutus');?>">About</a>
  <?php 

 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 6;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
  <button class="dropdown-btn">Services 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
   <?php
	foreach($res as $val){
	 //$link_url = site_url($val['friendly_url']);
	  $link_url = site_url().'#Services';//.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
<a  href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a>
<?php }
if($total_categories >6)
{
?>
<a  href="<?php echo base_url('category');?>" title="View All &raquo;"><b>View All &raquo;</b></a>
<?php } ?>
   
  </div>
  <?php }?>

  <a href="#Clients">Clients</a>
<?php  $condtion_array = array(
   'field' =>"gallery_id,gallery_title,parent_id,friendly_url,(SELECT COUNT(gallery_id) FROM wl_gallery AS b WHERE b.parent_id=a.gallery_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $offset = 0;
   $limit = 6;
   
   $param = array('status'=>'1','parent_id'=>'0');
	$res_products               = $this->gallery_model->get_gallery($limit,$offset,$param);  
  //echo_sql();
   $total_page_products	= get_found_rows();
   if($total_page_products > 0){
?>
  <button class="dropdown-btn">Portfolio 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
   <?php
	foreach($res_products as $val){
	 $link_url = site_url($val['friendly_url']);
	 //$link_url = site_url().'#'.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
  <a  href="<?php echo $link_url;?>" title="<?php echo $val['gallery_title'];?>"><?php echo $val['gallery_title'];?></a>
<?php }
if($total_page_products >6)
{
?>
<a  href="<?php echo base_url('gallery');?>" title="View All &raquo;"><strong>View More</strong></a>
<?php } ?>
    
  </div>
   <?php } ?>
  <a href="<?php echo base_url();?>#footer">Contact</a>
</div>



</div>





<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Inquiry Now</h4>
        </div>
        <div class="modal-body">
             <form method="post" action="inquiry.php">
             <input type="hidden" name="submit_contact" value="1">
             <input type="hidden" name="check_form" value="">
                <div class="form-group">
                  <label for="usr">Name:</label>
                  <input type="text" class="form-control" name="first_name" placeholder="Name*" required="" id="usr">
                </div>
                <div class="form-group">
                  <label for="pwd">Phone Number:</label>
                  <input type="number" onkeypress="return isNumberKey(event);" class="form-control" name="telephone" placeholder="Phone Number*" required="" id="pwd">
                </div>
                <div class="form-group">
                  <label for="pwd">Email Id:</label>
                  <input type="email" class="form-control" name="email" placeholder="Email Id*" required="" id="pwd">
                </div>

                <div class="form-group">
                     <label for="pwd">Message:</label>
                  <textarea class="form-control heightauto" style="height: 80px;" name="comments" rows="5" placeholder="Message" spellcheck="false"></textarea>
                </div>
                

            
                
                <div class="form-group">
                    <button type="snubmit" class="btn_btn m0">Submit</button>
                </div>

            </form>
        </div>
    
      </div>
    </div>
  </div>