<?php $this->load->view('project_footer');
// echo $this->uri->rsegment(1);
?>
<?php if($this->uri->rsegment(1)!='home'){ ?>
<script src="<?php echo theme_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo theme_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo theme_url(); ?>js/owl.carousel.min.js"></script>


<script src="<?php echo theme_url(); ?>js/main.js"></script>


<script src="<?php echo theme_url(); ?>js/jquery.quicksand.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.easing.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/script.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.prettyphoto.js" type="text/javascript"></script> 


<script>



function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>

<?php } else{?>

<?php /* <p id="back-top"><a href="#top"><span></span></a></p> */?>
<script src="<?php echo theme_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo theme_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo theme_url(); ?>js/owl.carousel.min.js"></script>

<script src="<?php echo theme_url(); ?>libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="<?php echo theme_url(); ?>libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="<?php echo theme_url(); ?>libs/gsap/3.7.1/Draggable.min.js" ></script>
    <script src="<?php echo theme_url(); ?>libs/gsap/3.7.1/ScrollTrigger.min.js" ></script>
    <script src='<?php echo theme_url(); ?>libs/three.js/r83/three.js'></script>
	<script src='<?php echo theme_url(); ?>libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
	<script src="<?php echo theme_url(); ?>js1/clapatwebgl.js"></script>
	<script src="<?php echo theme_url(); ?>js1/plugins.js"></script>
    <script src="<?php echo theme_url(); ?>js1/common.js"></script>
    <script src="<?php echo theme_url(); ?>js1/scripts.js"></script>
<script src="<?php echo theme_url(); ?>js/main.js"></script>

<script src="//s3-us-west-2.amazonaws.com/s.cdpn.io/16327/gsap-latest-beta.min.js"></script>
<script src="//s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollToPlugin3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>
<script src="<?php echo theme_url(); ?>js/debug.addIndicators.min.js"></script>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer1", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer1", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper1",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper1")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>



<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer2", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer2", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper2",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper2")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer3", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer3", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper3",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper3")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>


<script type="text/javascript">
	gsap.defaultEase = Linear.easeNone;
		var titles = document.querySelectorAll(".sectionTitle");
		var controller = new ScrollMagic.Controller();
		var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


		
		
		// create timeline
		// this could also be created in a loop
		tl.to("#js-slideContainer4", {duration: 1, xPercent: 0 }, "label1");
		tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -25 }, "label2");
		tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -50 }, "label3");
		tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
		tl.to("#js-slideContainer4", {duration: 1, xPercent: -75 }, "label4");
		tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


		new ScrollMagic.Scene({
		triggerElement: "#js-wrapper4",
		triggerHook: "onLeave",
		duration: "400%"

		})
		.setPin("#js-wrapper4")
		.setTween(tl)
		.addIndicators({
			colorTrigger: "black",
			colorStart: "black",
			colorEnd: "black"
		})
		.addTo(controller);



</script>

<script type="text/javascript">
	var showChar = 256;
var ellipsestext = "...";
var moretext = "See More";
var lesstext = "See Less";
$('.comments-space').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});
</script>
<?php } ?>
<script>
function validate_frm(obj){
	
	var flag = true;
	
	if($('#term').prop("checked") == false){
		
		$('#term_validate').html('Terms & condition field is required');
		flag = false;

	}else{
		$('#term_validate').html('');
	}
	
	if($('#contactmode1').prop("checked") == false && $('#contactmode2').prop("checked") == false && $('#contactmode3').prop("checked") == false){
		
		$('#contact_mode_validate').html('Contact mode field is required');
		flag = false;

	}else{
		$('#contact_mode_validate').html('');
	}
	
	return flag;
	
}
ajax_from_post({'form':'#enq_frm'});

function ajax_from_post(obj)
{
	
	$(obj.form).on('submit',(function(e) {		
        e.preventDefault();
		var frmObj = this;
		
		var jq_frmObj = $(frmObj);
		
		var ajax_submit_button=$("input[type='submit']");
	    var button_val=$(ajax_submit_button,frmObj).val();
		
		 if(!button_val)
		 {
			 var ajax_submit_button=$("input[type='image']");
		 }
		 	
		
			
		 $(ajax_submit_button).attr('disabled', 'disabled');		
		
          $.ajax({
            url: jq_frmObj.attr('action'),
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){ 
				
                    console.log(data);
                    data = JSON.parse(data);
				
                    if(data.st == 0)
                    {						   
						$(ajax_submit_button).removeAttr("disabled");
						
						 var ctp_url='';
												
						/*if(typeof(captcha_url)=='string')
						{	ctp_url=captcha_url;	
						}						
						else						
						{						
						 ctp_url=$("img[src*='captcha']").attr('src');						
						}						
						if(ctp_url)
						{
						
							if(ctp_url.indexOf('?')>0)
							{							
								ctp_url=ctp_url.substr(0,ctp_url.indexOf('?'));							
							}
														
							ctp_url=ctp_url+'?'+Math.random();						
							$("img[src*='captcha']").attr('src',ctp_url);						
							$('input[name=verification_code]').val('');
						}
						*/
									
                        $( ".required",frmObj ).remove();
                        var data1    = JSON.parse(data.msg); 
                        $(':input',frmObj).each(function(){                            
                                        var elementName = $(this).attr('name');        
                                        var message = data1[elementName];
                                        if(message){
                                        var element = $('<div>' + message + '</div>')
                                                        .attr({
                                                            'class' : 'required'
                                                        })
                                                        .css ({
                                                            display: 'none'
                                                        });
                                        $(this).after(element);
										 if(frmObj.id=='enq_frm'){
										/*	if($('#term').prop("checked") == false){	 
												$('#term_validate').html('Terms & condition field is required');
											}
											if($('.contactmode').prop("checked") == false){	 
											
												$('.contact_mode_validate').html('Contact mode field is required');
											}*/
										 }
                                        $(element).fadeIn();
}
                        });
                    }

                    if(data.st == 1)
                    {						 
					  $(".required").remove();
					  $(ajax_submit_button).removeAttr("disabled");					  
					  if(data.ref!='')
					  {						  
						 window.location.href=data.ref;
						 		  						   
					  }else
					  {						 
						  $('#error_msg',frmObj).html(data.msg);
						  frmObj.reset();
						  
					  } 
                      
                    }
            },
            error: function(){}             
        });
    }));
	
}

</script> 

<script type="text/javascript">
	$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
</script>

<script src="<?php echo resource_url();?>Scripts/script.int.dg.js"></script>
</body>
</html>
