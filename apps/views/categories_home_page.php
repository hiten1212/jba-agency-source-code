<?php $this->load->model(array('category/category_model','products/product_model'));
$this->load->helper(array('category/category'));

$condtion_array = array(
'field' =>"category_id, category_name, friendly_url",
'condition'=>"AND parent_id = '0' AND status='1'",
'order'=>'category_id',
'debug'=>FALSE
);

$condtion_array['offset'] = 0;
$condtion_array['limit'] = 12;
$level_0_res = $this->category_model->getcategory($condtion_array);
$level_0_res_categories = $this->category_model->total_rec_found;
$selected_category = $this->uri->rsegment(3);
$counter = 0;
if(is_array($level_0_res) && !empty($level_0_res)){
	?>
  <div class="category-col-2">
   <div class="category_bx">
    <ul class="category-slide-menu">
     <?php
     foreach($level_0_res as $val){
	     $link_url = base_url().$val['friendly_url'];
	     $category_name = strlen($val['category_name']) > 30 ? char_limiter($val['category_name'],30,'') : $val['category_name'];
	     ?>
	     <li>
	      <a href="<?php echo $link_url;?>"><?php echo $category_name;?></a>
	      <?php
	      $condtion_array = array(
	      'field' =>"category_id, category_name, friendly_url",
	      'condition'=>"AND parent_id = '".$val['category_id']."' AND status='1'",
	      'order'=>'sort_order',
	      'debug'=>FALSE
	      );
	      
	      $condtion_array['offset'] = 0;
	      $condtion_array['limit'] = 12;
	      $level_1_res = $this->category_model->getcategory($condtion_array);
	      $level_1_categories	=  $this->category_model->total_rec_found;
	      if($level_1_categories > 0){
		      ?>
		      <div class="first_menu" style="display: none;">
		       <div class="four-cols">
		        <?php
		        foreach($level_1_res as $val1){
			        $link_url_1 = base_url().$val1['friendly_url'];
			        $category_name_1 = strlen($val1['category_name']) > 30 ? char_limiter($val1['category_name'],30,'') : $val1['category_name'];
			        ?>
			        <div class="block-2">
			         <div class="submenu">
			          <p><a href="<?php echo $link_url_1;?>"><?php echo $category_name_1;?></a></p>
			          <?php
			          $condtion_array = array(
					      'field' =>"category_id, category_name, friendly_url",
					      'condition'=>"AND parent_id = '".$val1['category_id']."' AND status='1'",
					      'order'=>'sort_order',
					      'debug'=>FALSE
					      );
					      
					      $condtion_array['offset'] = 0;
					      $condtion_array['limit'] = 4;
					      $level_2_res = $this->category_model->getcategory($condtion_array);
					      $level_2_categories	=  $this->category_model->total_rec_found;
					      if($level_2_categories > 0){
						      ?>
						      <p class="subcat">
						       <?php
						       foreach($level_2_res as $val2){
							       $link_url_2 = base_url().$val2['friendly_url'];
							       $category_name_2 = strlen($val2['category_name']) > 30 ? char_limiter($val2['category_name'],30,'') : $val2['category_name'];
							       ?>
							       <a href="<?php echo $link_url_2;?>"><?php echo $category_name_2;?></a>
							       <?php
						       }?>
						      </p>
						      <?php
						      if($level_2_categories > 4){
							      ?>
							      <p><a href="<?php echo $link_url_1;?>" title="View All">View All</a></p>								
							      <?php
						      }
					      }?>
			         </div>
			        </div> 
			        <?php
		        }?>
		       </div>
		      </div>
		      <?php
	      }?>
	     </li>
	     <?php
     }?>
    </ul>
    <div class="clearfix"></div>
   </div>
  </div>
  <?php
}?>