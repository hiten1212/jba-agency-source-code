<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
$meta_rec = $this->meta_info;
if( is_array($meta_rec) && !empty($meta_rec) ){
	?>
	<title><?php echo $meta_rec['meta_title'];?></title>
	<meta name="description" content="<?php echo $meta_rec['meta_description'];?>" />
	<meta  name="keywords" content="<?php echo $meta_rec['meta_keyword'];?>" />
	<?php
}?>
<?php  $cseg=$this->uri->segment(1); 
	   $cseg2=$this->uri->segment(2);
//echo "<br>";
//echo $seg2=$this->uri->rsegment(2);
if($meta_rec['page_url']!='' && $meta_rec['page_url']!='home'){
 ?>

<link rel="canonical"  href="<?php echo base_url().$meta_rec['page_url'];?>"/>
<?php } 
elseif($cseg=='category' && $cseg2=='products_listing')
{?>
<link rel="canonical"  href="<?php echo base_url().'products';?>"/>
<?php }
elseif($cseg=='products' && $cseg2=='index')
{?>
<link rel="canonical"  href="<?php echo base_url().'products';?>"/>
<?php }
else{?>
<link rel="canonical"  href="<?php echo base_url().substr($_SERVER['REQUEST_URI'],1);?>"/>
<?php } ?>
<link  rel="icon" type="image/x-icon"  href="<?php echo base_url();?>favicon.ico"/>
<link href="<?php echo base_url(); ?>assets/developers/css/proj.css" rel="stylesheet" type="text/css" />
<script> var _siteRoot='index.html',_root='index.html';</script>
<script> var site_url = '<?php echo site_url();?>';</script>
<script> var theme_url = '<?php echo theme_url();?>';</script>
<script> var resource_url = '<?php echo resource_url(); ?>'; var gObj = {};</script>

<?php if($this->uri->rsegment(1)=='home'){
	//header home page
	 ?>
    <link href="//fonts.googleapis.com/css2?family=Dancing+Script:wght@600&display=swap" rel="stylesheet">

    <link href="//fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/x-icon" />
     <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/bootstrap.min.css">
   
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/owl.carousel.min.css">
     
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/style1.css">
    <link href="<?php echo theme_url(); ?>css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/responsive.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>swiper/css/swiper.min.css">
    <?php } else{
		//header inner page
		?>
    
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@600&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/prettyphoto.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo theme_url(); ?>/css/style1.css">
    <link href="<?php echo theme_url(); ?>css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo theme_url(); ?>css/responsive.css">
    <link href="<?php echo theme_url(); ?>css/swiper.min.css" type="text/css" rel="stylesheet">
	<?php } ?>
    
    <style>
	.headerbackground{background:<?php echo $this->admin_info->header_background!='' ? $this->admin_info->header_background:'#2c220e';  ?> !important}
	</style>
<script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/developers/js/common.js" async></script> 
 
 <?php
if ($this->admin_info->google_analytics_id!=""){
	echo '<script>'.$this->admin_info->google_web_code.'</script>';
}
if ($this->admin_info->google_analytics_id!=""){
	?>
	<script>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', '<?php echo $this->admin_info->google_analytics_id;?>']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<?php
}?>
</head>


<?php
if(isset($is_header) && $is_header==false){
}else{ 
	?>
	<body class="homepage <?php if($this->uri->rsegment(1)!='home'){ ?>innerpages<?php } ?>">
   <?php /*<div id="whatsapp"> <a href="https://api.whatsapp.com/send?phone=<?php echo $this->admin_info->whatsapp;?>&amp;text=<?php echo $this->admin_info->whatsapp_text;?>" target="_blank" title="Whatsapp">
  <div id="whatsappMain"></div>
  </a> </div>*/?>
    
    
    
	<?php 
	$data['allcms_pages']       = $allcms_pages;
	$this->load->view('project_header', $data);
		} ?>

        <style type="text/css">
            .width_lll{ padding: 30px 0px; }
            .slider_effect{ padding: 0px; }
        </style>