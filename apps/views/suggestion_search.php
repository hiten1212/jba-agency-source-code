<script type="text/javascript">
$(window).load(function(e) {
var options = {
	data: [ 
	<?php
	foreach($this->product_info as $val){
		
		?>
		{name: "<?php echo $val["product_name"];?>", desc: "", icon: ""},
		<?php
	}
	?>
		],
	getValue: "name",
	list: {
		match: {enabled:true}
	},
	template: {
		type: "custom",
		method: function(value, item) {
			return "<p class='sugg_txt'><span class='fs14 black'>" + value + "</span><br>" + item.desc+"</p><p class='cb'></p>";
		}
	}
};
$("#search-keyword").easyAutocomplete(options);
$("#search-location").easyAutocomplete(options);
});
</script>