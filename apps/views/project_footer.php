<style>@charset "UTF-8"; @keyframes fadeIn {  to {    opacity: 1;  }}.fade-in {  opacity: 0;  animation: fadeIn .5s ease-in 1 forwards;}.is-paused {  animation-play-state: paused;}</style>



<!-- Start Quick Call Buttons -->
<?php if($this->admin_info->whatsapp_number){ ?>
<div class="quick-call-button"></div>
<div class="call-now-button" id="draggable">
<a href="#" class="scrollup"><i class="fa fa-arrow-up"></i></a>
<div>
<p class="call-text"> Call Us </p>
<a href="https://api.whatsapp.com/send?phone=<?php echo trim(str_replace(" ","",str_replace("+","",$this->admin_info->whatsapp_number))); ?>&text=<?php echo $this->admin_info->whatsapp_text; ?>" id="quickcallbutton">
<div class="quick-alo-ph-circle active"></div>
<div class="quick-alo-ph-circle-fill active"></div>
<div class="quick-alo-ph-img-circle shake"></div>
</a> </div>
</div>
<?php } ?>
<footer id="footer" style="background: <?php echo $this->admin_info->footer_background!='' ? $this->admin_info->footer_background:'#000';  ?> !important">
<?php 
//trace($this->uri->rsegments);
  
$facebook_link = $this->admin_info->facebook_link != null ? 'href="'.$this->admin_info->facebook_link.'" target="_blank"' : 'href="javascript:void(0);"';
$twitter_link = $this->admin_info->twitter_link != null ? 'href="'.$this->admin_info->twitter_link.'" target="_blank"' : 'href="javascript:void(0);"';
$google_link = $this->admin_info->google_link != null ? 'href="'.$this->admin_info->google_link.'" target="_blank"' : 'href="javascript:void(0);"';
$linkedin_link = $this->admin_info->linkedin_link != null ? 'href="'.$this->admin_info->linkedin_link.'" target="_blank"' : 'href="javascript:void(0);"';
$youtube_link = $this->admin_info->youtube_link != null ? 'href="'.$this->admin_info->youtube_link.'" target="_blank"' : 'href="javascript:void(0);"';
$instagram_link = $this->admin_info->instagram_link != null ? 'href="'.$this->admin_info->instagram_link.'" target="_blank"' : 'href="javascript:void(0);"';
$soundcloud_link = $this->admin_info->soundcloud_link != null ? 'href="'.$this->admin_info->soundcloud_link.'" target="_blank"' : 'href="javascript:void(0);"';
$website_url =$this->admin_info->website_url != null ?  $this->admin_info->website_url: '';
$footer1_header =$this->admin_info->footer1_header != null ?  $this->admin_info->footer1_header: '';
$footer2_header =$this->admin_info->footer2_header != null ?  $this->admin_info->footer2_header: '';
$footer3_header =$this->admin_info->footer3_header != null ?  $this->admin_info->footer3_header: '';
$copyright_text =$this->admin_info->copyright_text != null ?  $this->admin_info->copyright_text: '';
?>	
    
    <div class="container">
		<div class="row">
			<div class="col-md-4 col-xs-12 col-sm-4">
				<div class="title_footer">
					<h3><?php echo $footer1_header; ?></h3>
					
				</div>

				<ul class="scoalmedia">
                <li><a <?php echo $instagram_link;?>><i class="fa fa-instagram"></i> Instagram</a></li>
					<li><a <?php echo $youtube_link;?>><i class="fa fa-youtube"></i> youtube</a></li>
					<li><a <?php echo $twitter_link;?>><i class="fa fa-twitter"></i> twitter</a></li>
					<li><a <?php echo $facebook_link;?>><i class="fa fa-facebook"></i> Facebook</a></li>
					
				</ul>

				
                <div  id="my_newsletter_msg" class="mt5 text-center"></div>
                <div class="newsll">
                <div class="title_footer withus">
					<h3>Be with Us</h3>
				</div>
 <?php echo form_open('pages/join_newsletter','name="newsletter" class="newsll" id="chk_newsletter" onsubmit="return join_newsletter();" '); ?>
				
					<input type="email" name="subscriber_email" id="subscriber_email" placeholder="Email Id">
					<button class="inerbt" type="submit"><i class="fa fa-envelope-o"></i></button>
				<input name="subscribe_me" type="hidden" id="saction" value="Y" />
                <?php echo form_close();?>
            </div>
			</div>


			<div class="col-md-5 col-xs-12 col-sm-5 pd50">
				<div class="row">
					
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="title_footer">
							<h3><?php echo $footer2_header; ?></h3>
						</div>
						<ul class="address_il">
							<li><a href="tel:<?php echo $this->admin_info->phone;?>"><i class="fa fa-phone"></i> +<?php echo $this->admin_info->phone;?></a></li>
							<li><a href="mailto:<?php echo $this->admin_info->admin_email;?>"><i class="fa fa-envelope"></i> <?php echo $this->admin_info->admin_email;?></a></li>
							<li><a href="#"><i class="fa fa-map-marker"></i> <?php echo $this->admin_info->address;?></a></li>
							<li><a href="#"><i class="fa fa-globe"></i> <?php echo $website_url; ?></a></li>
						</ul>
						<div class="qrcde"><img src="<?php echo theme_url(); ?>images/qrcode.jpg"></div>
                        <div class="goolemap"><span>Find Us:</span><?php echo html_entity_decode($this->admin_info->domestic_google_map); ?></div>
					</div>
				</div>
			</div>


			<div class="col-md-3 col-xs-12 col-sm-3">
				<div class="title_footer">
					<h3><?php echo $footer3_header; ?></h3>
				</div>
				<?php echo error_message(); ?> 
           <?php echo form_open('pages/enquiry','id="enq_frm" class="footerfro" role="form" onsubmit="return validate_frm(this)"');  ?>            
           <div id="error_msg"></div>	
				<form class="footerfro">
					<input type="text"  name="name" placeholder="Name" value="<?php echo set_value('name'); ?>">
					<input type="number" name="phone" placeholder="Mobile Number" value="<?php echo set_value('phone'); ?>">
					<input type="email" name="email"  placeholder="Email id" value="<?php echo set_value('email')?>">
					<textarea rows="4" placeholder="Message" name="message" placeholder="Comment"><?php echo set_value('message'); ?></textarea>
					<button class="btn btntbn" type="submit">Submit</button>
				 <?php echo form_close(); ?>
				
			</div>

		</div>
	</div>
</footer>
 <div class="copyrights">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12">
 				<p><?php echo $copyright_text; ?></p>
 			</div>
 		</div>
 	</div>
 </div>