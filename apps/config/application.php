<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['meta_title'] = 'JBA Agency';
$config['meta_keyword']	= 'JBA Agency';
$config['meta_description']	= 'JBA Agency';

$config['bottom.debug'] = 0;
$config['site.status']	= '1';
$config['site_name']	= 'JBA Agency';

$config['auth.password_min_length']	= '6';
$config['auth.password_force_numbers']	= '1';
$config['auth.password_force_symbols']	= '1';
$config['auth.password_force_mixed_case']	= '0';

$config['allow.imgage.dimension']	= '2000x2000';
$config['allow.file.size']	        = '800000'; //In KB
$config['allow.doc.size']	        = '800000'; //In KB

$config['allow_discount_option'] = 1;
$config['config.date.time']	= date('Y-m-d H:i:s');
$config['config.date']	    = date('Y-m-d');

$config['analytics_id']	    = '';
$config['per_page'] = 12;

$config['frontPageOpt']  = array($config['per_page'],2*$config['per_page'],3*$config['per_page'],4*$config['per_page'],5*$config['per_page'],6*$config['per_page'],8*$config['per_page'],10*$config['per_page'],12*$config['per_page'],15*$config['per_page']);

$config['no_record_found'] = 'No Records Found.';

$config['product_set_as_config'] = array(''=>"Product Set As",
//'new_arrival'=>'New Product',
//'hot_product'=>'Hot Product',
'featured_product'=>'Featured Product');
$config['product_unset_as_config']	= array(''=>"Product Unset As",
//'new_arrival'=>'New Product',
//'hot_product'=>'Flooring',
'featured_product'=>'Featured Product');

$config['category_set_as_config'] = array(''=>"Category Set As",
'is_featured'=>'Featured');
$config['category_unset_as_config']	= array(''=>"Category Unset As",
'is_featured'=>'Featured');


$config['gallery_set_as_config'] = array(''=>"Gallery Set As",
'is_latest'=>'Latest');
$config['gallery_unset_as_config']	= array(''=>"Gallery Unset As",
'is_latest'=>'Latest');

$config['news_set_as_config'] = array(''=>"News Set As",
'latest_news'=>'Latest News & Events');
$config['news_unset_as_config']	= array(''=>"News Unset As",
'latest_news'=>'Latest News & Events');

$config['events_set_as_config'] = array(''=>"News & Events Set As",
'latest_news'=>'Latest News & Events');
$config['events_unset_as_config']	= array(''=>"News & Events Unset As",
'latest_news'=>'Latest News & Events');


$config['service_set_as_config'] = array(''=>"Project Set As",
'is_home'=>'Home');

$config['service_unset_as_config']	= array(''=>"Project Unset As",
'is_home'=>'Home');

$config['service_category_set_as_config'] = array(''=>"Category Set As",
'is_menu'=>'Menu');

$config['service_category_unset_as_config']	= array(''=>"Category Unset As",
'is_menu'=>'Menu');

$config['user_title'] =  array(""=>"Select","Mr."=>"Mr.","Ms."=>"Ms.","Miss."=>"Miss.","Mrs."=>"Mrs.","Dr"=>"Dr","Shri"=>"Shri","Smt"=>"Smt","Madam"=>"Madam");

$config['register_thanks']            = "Thanks for registering with <site_name>. We look forward to serving you. ";

$config['register_thanks_activate']   = "Thanks for registering with <site_name>. Please Check your mail account to activate your account on the <site_name>. ";



$config['enquiry_success']              = "Your enquiry has been submitted successfully. We will revert back to you soon.";

$config['feedback_success']             = "Your Feedback has been submitted successfully. We will revert back to you soon.";

$config['product_enquiry_success']      = "Your product enquiry  has been submitted successfully.We will revert back to you soon.";

$config['product_referred_success']     = "This product has been referred to your friend successfully.";

$config['site_referred_success']        = "Site has been referred to your friend successfully.";

$config['forgot_password_success']      = "Your password has been send to your email address. Please check your email account.";



$config['exists_user_id']              = "Email id  already exists. Please use different email id.";

$config['email_not_exist']             = "Email id does not exist.";



$config['login_failed']             = "Invalid Username/Password";



$config['newsletter_subscribed']           =  "You have been subscribed successfully for our newsletter service.";

$config['newsletter_already_subscribed']   =  "This Email address already exist.";

$config['newsletter_unsubscribed']         =  "You have been unsubscribed from our newsletter service.";

$config['newsletter_not_subscribe']        =  "You are not the subscribe member of our news letter service.";

$config['newsletter_already_unsubscribed']   =  "You have already un-subscribed the newsletter service.";



$config['testimonial_post_success']     = "Thank you for your testimonial to <site_name>. Your message will be posted after review by the <site_name> team.";



$config['advertisement_request']          = "Your advertisement request has been submitted successfully. We will revert back to you soon.";

$config['myaccount_update']               = "Your account information has been updated successfully.";

$config['myaccount_password_changed']     = "Password has been changed successfully.";

$config['myaccount_password_not_match']   = "Old Password does  not match. Please try again.";

$config['member_logout']                  = "Logout successfully.";



$config['wish_list_add']               = "Product has been added to your favorite list successfully.";

$config['wish_list_delete']            = "Product has been deleted from your wishlist.";

$config['wish_list_product_exists']    = "This product already exists in your wishlist.";



$config['cart_add']                  =  "Product has been added to your Shopping Basket.";

$config['cart_quantity_update']      =  "Product quantity has been updated successfully.";

$config['cart_product_exist']        =  "Product is already exist in your cart.";

$config['cart_delete_item']          =  "Product(s) has been deleted successfully.";

$config['cart_empty']                 =  "Basket has been cleared successfully.";

$config['cart_available_quantity']   =  "Maximum available quantity  is <quantity>. You can not add  more then available Quantity.";



$config['shipping_required']         =  "Shipping selection is required.";

$config['payment_success']           =  "Your Order has been placed successfully. A confirmation email and invoice have been sent to your email id";

$config['payment_failed']            =  "Your transaction is canceled.";





$config['arr_club'] =  array(

'0'=>'R&B MONDAYS NIGHTS',

'1'=>'AFROBEATS TUESDAYS NIGHTS',

'2'=>'PERSIS WEDNESDAYS NIGHTS',

'3'=>'DESI NATION THURSDAYS NIGHTS',

'4'=>'MASSIVE FRIDAYS NIGHTS',

'5'=>'MAMACITA SATURDAYS NIGHTS'

);





$config['arr_rating'] =  array(

'1'=>'1',

'2'=>'2',

'3'=>'3',

'4'=>'4',

'5'=>'5'

);	





$config['bannersz'] =  array(

'Left Panel'=>'300x135',

'Page Bottom'=>'645x80'

);	



$config['bannersections'] = array(

'common'=>"All Pages"

);

/* KEY PAIR OF SECTION AND POSTION */

$config['banner_section_positions'] = array(

'product'=>array('Left','Bottom')



);



$config['total_product_images'] = "5";

$config['product.best.image.view'] = "( File should be .jpg, .png, .gif format and file size should not be more then 1 MB (1024 KB)) ( Best image size 600X600 )";

$config['testimonial.best.image.view'] = "File should be .jpg, .png, .gif format and file size should not be more then 1 MB (1024 KB)) ( Best image size 300X300";

