<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth
{
	public $ci;
	public function __construct()
	{
		if (!isset($this->ci))
		{
			$this->ci =& get_instance();
		}
		$this->ci->load->library('safe_encrypt');
		$this->ci->load->helper('cookie');
		$this->auth_tbl = 'wl_customers';
	}
	
	public function is_user_logged_in()
	{
		$is_logged_in = $this->ci->session->userdata('logged_in');
		
		$logged_in_username = $this->ci->session->userdata('username');
		if ($is_logged_in == TRUE)
		{
			$user_data = array(
			'user_name'=>$logged_in_username,			  
			'status'=>'1'	
			);						 
			$num = $this->ci->db->get_where($this->auth_tbl,$user_data)->num_rows();
			return ($num) ? true : false;
		}else
		{
			return false;
		}
		
	}
	
	public function is_auth_user()
	{
		if ($this->is_user_logged_in()!= TRUE)
		{
			$this->logout();
			redirect('users/login', '');
		}
	}
	
	public function update_last_login($login_data)
	{
		$data = array(
		'last_login_date'=>$login_data['current_login'],
		'current_login'=>$this->ci->config->item('config.date.time') 
		);
		$this->ci->db->where('customers_id', $this->ci->session->userdata('user_id'));
		$this->ci->db->update($this->auth_tbl, $data);
	}
	
	public function verify_user($username,$password,$status='1',$type='0')
	{
		$password = $this->ci->safe_encrypt->encode($password);
		$this->ci->db->select("customers_id,user_name,first_name,last_name,title,login_type,is_blocked,last_login_date,current_login,block_time,mobile_number",FALSE);
		$this->ci->db->where('user_name', $username);
		$this->ci->db->where('password', $password);
		if($status==1)$this->ci->db->where("status = '".$status."'");
		
		$this->ci->db->where('status <>','2');			
		$this->ci->db->where('is_verified','1');		
		$query = $this->ci->db->get($this->auth_tbl);
		
		if ($query->num_rows() == 1)
		{
			$row  = $query->row_array();
			$name = $row['first_name']." ".$row['last_name'];		
			$data = array(
			'user_id'=>$row['customers_id'],
			'name'=>ucwords($name),
			'login_type'=>$row['login_type'],
			'username'=>$row['user_name'],							
			'title'=>$row['title'],
			'first_name'=>$row['first_name'],
			'last_name'=>$row['last_name'],
			'mobile_number'=>$row['mobile_number'],
			'is_blocked'=>$row['is_blocked'],	
			'blocked_time'=>$row['block_time'],
			'logged_in' => TRUE
			);
			
			$login_data = array('current_login'=>$row['current_login']);			
			$this->ci->session->set_userdata($data);			
			$this->update_last_login($login_data);
			
			return TRUE;
		}else
		{
			return FALSE;
		}
		
	}
	
	public function verify_email($login_email_id,$status='1')
	{
		$this->ci->db->select("customers_id,user_name",FALSE);
		$this->ci->db->where('user_name', $login_email_id);
		if($status==1)$this->ci->db->where("status = '".$status."'");
		$this->ci->db->where('status <>','2');
		$query = $this->ci->db->get($this->auth_tbl);
		
		if ($query->num_rows() == 1)
		{
			$row  = $query->row_array();
			$data = array(
			'login_email_id'=>$row['user_name']
			);
			
			$this->ci->session->set_userdata($data);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function logout()
	{
		$userId = $this->ci->session->userdata('user_id');
		$this->ci->session->unset_userdata('session_id');
		
		$data = array('user_id', 'type', 'login_type', 'username', 'first_name', 'last_name', 'name', 'mkey', 'is_blocked', 'blocked_time', 'logged_in');
		$this->ci->session->unset_userdata($data);
	}
	
	public function logged_member_type()
	{
		return $this->ci->session->userdata("customer_type")==1?"vendor":"members";
	}
	
}