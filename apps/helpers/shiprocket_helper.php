<?php
if(! function_exists('get_token_sr')){
	function get_token_sr(){
		try{
			$data = array('email'=>'weblink.dinesh1@gmail.com','password'=>'111111');
			$json_data = json_encode($data);
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/auth/login",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS =>$json_data,
			CURLOPT_SSL_VERIFYHOST=>0,
			CURLOPT_SSL_VERIFYPEER=>0,
			CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json"
			),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
				 $error_msg = curl_error($curl);
				 throw new Exception($error_msg);
			}
			curl_close($curl);
			$response_obj = json_decode($response);
			$resp = array('err'=>0,'token'=>$response_obj->token);
			return $resp;
		}catch(Exception $e){
			throw $e;
		}
	}
}

if(! function_exists('get_pickup_postcode_sr')){
	function get_pickup_postcode_sr(){
		$pickup_postcode = '395010';
		return $pickup_postcode;
	}
}

if(! function_exists('get_courier_company_sr')){
	function get_courier_company_sr($params=array()){
			try{
					$token_res = get_token_sr();
					$token = $token_res['token'];
					$param_req = array(
														'pickup_postcode'=>get_pickup_postcode_sr(),
														'delivery_postcode'=>$params['delivery_postcode'],
														'cod'=>$params['cod'],
														'weight'=>$params['weight']
													);
					$qry_url = http_build_query($param_req);
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/courier/serviceability?".$qry_url,
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					 CURLOPT_SSL_VERIFYHOST=>0,
					 CURLOPT_SSL_VERIFYPEER=>0,
					  CURLOPT_CUSTOMREQUEST => "GET",
					  CURLOPT_HTTPHEADER => array(
						 "Content-Type: application/json",
						 "Authorization: Bearer $token"
					  ),
					));

					$response = curl_exec($curl);
					curl_close($curl);
					$response_obj = json_decode($response);
					
					//trace($qry_url); trace($response); trace($response_obj); die;
					
					return $response_obj;
			}catch(Exception $e){
				throw $e;
			}
	}
}

if(! function_exists('order_tracking_sr')){
	function order_tracking_sr($params=array()){
			try{
					$token_res = get_token_sr();
					$token = $token_res['token'];
					$awb_code = $params['awb_code'];
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/courier/track/awb/".$awb_code,
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_SSL_VERIFYHOST=>0,
					  CURLOPT_SSL_VERIFYPEER=>0,
					  CURLOPT_CUSTOMREQUEST => "GET",
					  CURLOPT_HTTPHEADER => array(
						 "Content-Type: application/json",
						 "Authorization: Bearer $token"
					  ),
					));

					$response = curl_exec($curl);
					curl_close($curl);
					$response_obj = json_decode($response);
					return $response_obj;
			}catch(Exception $e){
				throw $e;
			}
	}
}


if(! function_exists('api_create_order_sr')){
	function api_create_order_sr($params=array()){
			try{
					$token_res = get_token_sr();
					$token = $token_res['token'];
					$sub_total = $params['sub_total'];
					$sub_total = formatNumber($sub_total,2);
					$dimension_length_arr = array();
					$dimension_breadth_arr = array();
					$dimension_height_arr = array();
					$total_weight=0;
					$params_order_items = array();
					$order_items = !empty($params['order_items']) ? $params['order_items'] : array();
					$order_items = !is_array($order_items) ? array() : $order_items;
					foreach($order_items as $okey=>$oval){
						$total_weight+=$oval['prod_weight'];
						array_push($dimension_length_arr,$oval['prod_length']);
						array_push($dimension_breadth_arr,$oval['prod_breadth']);
						array_push($dimension_height_arr,$oval['prod_height']);
						$params_order_items[] = array(
																					'name'=>$oval['product_name'],
																					'sku'=> $oval['product_code'],
																					'units'=> $oval['quantity'],
																					'selling_price'=>$oval['product_price'],
																					'discount'=>0,
																					'tax'=> formatNumber($oval['product_tax'],2),
																					'hsn'=>$oval['product_hsn']
																				);
					}
					$total_weight_kg = $total_weight/1000;
					$is_float = $total_weight%1000;
					if($is_float > 0){
						$total_weight_kg = formatNumber($total_weight_kg,2);
					}
					$dimension_length = !empty($dimension_length_arr) ? max($dimension_length_arr)  : 0;
					$dimension_breadth = !empty($dimension_breadth_arr) ? max($dimension_breadth_arr)  : 0;
					$dimension_height = !empty($dimension_height_arr) ? max($dimension_height_arr)  : 0;
					$shipping_charges = (isset($params['shipping_charges']) ?  floatval($params['shipping_charges']) : 0);
					$shipping_charges = formatNumber($shipping_charges,2);
					$total_discount = (isset($params['total_discount']) ?  floatval($params['total_discount']) : 0);
					$total_discount = formatNumber($total_discount,2);
					$data = array('order_id'=>$params['order_id'],
													'order_date'=>$params['order_date'],
													'pickup_location'=> (!empty($params['pickup_location']) ? $params['pickup_location'] : 'Serraw'),
													'channel_id'=> (!empty($params['channel_id']) ? $params['channel_id'] : ''),
													'comment'=> (!empty($params['comment']) ? $params['comment'] : ''),
													'billing_customer_name'=> (!empty($params['billing_customer_name']) ? $params['billing_customer_name'] : ''),
													'billing_last_name'=> (!empty($params['billing_last_name']) ? $params['billing_last_name'] : ''),
													'billing_address'=> (!empty($params['billing_address']) ? $params['billing_address'] : ''),
													'billing_address_2'=> (!empty($params['billing_address_2']) ? $params['billing_address_2'] : ''),
													'billing_city'=> (!empty($params['billing_city']) ? $params['billing_city'] : ''),
													'billing_pincode'=> (!empty($params['billing_pincode']) ? $params['billing_pincode'] : ''),	
													'billing_state'=> (!empty($params['billing_state']) ? $params['billing_state'] : ''),	
													'billing_country'=> (!empty($params['billing_country']) ? $params['billing_country'] : ''),	
													'billing_email'=> (!empty($params['billing_email']) ? $params['billing_email'] : ''),	
													'billing_phone'=> (!empty($params['billing_phone']) ? str_replace("+91","",$params['billing_phone']) : ''),
													'shipping_customer_name'=> (!empty($params['shipping_customer_name']) ? $params['shipping_customer_name'] : ''),
													'shipping_last_name'=> (!empty($params['shipping_last_name']) ? $params['shipping_last_name'] : ''),
													'shipping_address'=> (!empty($params['shipping_address']) ? $params['shipping_address'] : ''),	
													'shipping_address_2'=> (!empty($params['shipping_address_2']) ? $params['shipping_address_2'] : ''),	
													'shipping_city'=> (!empty($params['shipping_city']) ? $params['shipping_city'] : ''),
													'shipping_pincode'=> (!empty($params['shipping_pincode']) ? $params['shipping_pincode'] : ''),	
													'shipping_country'=> (!empty($params['shipping_country']) ? $params['shipping_country'] : ''),	
													'shipping_state'=> (!empty($params['shipping_state']) ? $params['shipping_state'] : ''),
													'shipping_email'=> (!empty($params['shipping_email']) ? $params['shipping_email'] : ''),	
													'shipping_phone'=> (!empty($params['shipping_phone']) ? str_replace("+91","",$params['shipping_phone']) : ''),
													'payment_method'=>(!empty($params['payment_method']) ? $params['payment_method'] : ''),
													'shipping_is_billing'=>(int) (isset($params['shipping_is_billing']) ?  $params['shipping_is_billing'] : false),
													'order_items'=>$params_order_items,
													'sub_total'=> $sub_total,
													'shipping_charges'=> $shipping_charges,
													'total_discount'=>$total_discount,
												  'length'=> $dimension_length,
												  'breadth'=> $dimension_breadth,
												  'height'=>$dimension_height,
												  'weight'=>$total_weight_kg
													);
					//trace($data);
					//die;
					$json_data = json_encode($data);
					$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_URL => 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_SSL_VERIFYHOST=>0,
					CURLOPT_SSL_VERIFYPEER=>0,
					CURLOPT_POSTFIELDS =>$json_data,
					CURLOPT_HTTPHEADER => array(
						 "Content-Type: application/json",
						 "Authorization: Bearer $token"
					  )
					));

					$response = curl_exec($curl);
					curl_close($curl);
					$response_obj = json_decode($response);
					return $response_obj;
			}catch(Exception $e){
				throw $e;
			}
	}
}


if(! function_exists('generate_awb_sr')){
	function generate_awb_sr($params=array()){
			try{
					$token_res = get_token_sr();
					$token = $token_res['token'];
					$data = array('shipment_id'=>$params['shipment_id'],
													'courier_id'=>$params['courier_id']
													);
					$json_data = json_encode($data);
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => 'https://apiv2.shiprocket.in/v1/external/courier/assign/awb',
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => '',
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_SSL_VERIFYHOST=>0,
					  CURLOPT_SSL_VERIFYPEER=>0,
					  CURLOPT_CUSTOMREQUEST => 'POST',
					  CURLOPT_POSTFIELDS =>$json_data,
					  CURLOPT_HTTPHEADER => array(
						 "Content-Type: application/json",
						 "Authorization: Bearer $token"
					  ),
					));

					$response = curl_exec($curl);
					curl_close($curl);
					$response_obj = json_decode($response);
					return $response_obj;
			}catch(Exception $e){
				throw $e;
			}
	}
}

if(! function_exists('cancel_order_sr')){
	function cancel_order_sr($params=array()){
			try{
					$token_res = get_token_sr();
					$token = $token_res['token'];
					$data = array('ids'=>$params['sr_order_ids']);
					$json_data = json_encode($data);
					$curl = curl_init();
					curl_setopt_array($curl, array(
						  CURLOPT_URL => 'https://apiv2.shiprocket.in/v1/external/orders/cancel',
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => '',
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 0,
						  CURLOPT_FOLLOWLOCATION => true,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => 'POST',
						 CURLOPT_SSL_VERIFYHOST=>0,
						 CURLOPT_SSL_VERIFYPEER=>0,
						  CURLOPT_POSTFIELDS =>$json_data,
						  CURLOPT_HTTPHEADER => array(
							  "Content-Type: application/json",
								"Authorization: Bearer $token"
						  ),
					));
					$response = curl_exec($curl);
					curl_close($curl);
					$response_obj = json_decode($response);
					return $response_obj;
			}catch(Exception $e){
				throw $e;
			}
	}
}

if(! function_exists('create_order_sr')){
	function create_order_sr($params=array()){
		try{
			$CI = &get_instance();
			$CI->load->model(array('order/order_model'));
			$order_id=$params['order_id'];
			$order_res = $CI->order_model->get_order_master($order_id);
			if(!is_array($order_res) || empty($order_res)){
				throw new Exception("ERR:400  Not a valid order");
			}
			$orddetail = $CI->order_model->get_order_detail($order_id);
			if(!is_array($orddetail) || empty($orddetail)){
				new Exception("ERR:401 Not a valid order");
			}
			//$sub_total = $order_res['total_amount'] - $order_res['coupon_discount_amount'] + $order_res['shipping_amount'];
			$sub_total = $order_res['total_amount'] - $order_res['coupon_discount_amount'];
			$total_discount = $order_res['coupon_discount_amount'];
			$params = array(
													'order_id'=>$order_res['order_id'],
													'order_date'=>$order_res['order_received_date'],
													'comment'=>(!empty($params['comment']) ? $params['comment'] : ''),
													'billing_customer_name'=>$order_res['shipping_name'],
													'billing_address'=>$order_res['shipping_address'],
													'billing_city'=>$order_res['shipping_city'],
													'billing_pincode'=>$order_res['shipping_zipcode'],
													'billing_state'=>$order_res['shipping_state'],
													'billing_country'=>$order_res['shipping_country'],
													'billing_email'=> $order_res['email'],
													'billing_phone'=> $order_res['shipping_mobile'],
													'shipping_customer_name'=>$order_res['shipping_name'],
													'shipping_address'=>$order_res['shipping_address'],
													'shipping_city'=>$order_res['shipping_city'],
													'shipping_pincode'=>$order_res['shipping_zipcode'],
													'shipping_state'=>$order_res['shipping_state'],
													'shipping_country'=>$order_res['shipping_country'],
													'shipping_email'=> $order_res['email'],
													'shipping_phone'=> $order_res['shipping_mobile'],
													'shipping_is_billing'=>false,
													'payment_method'=>$params['payment_method'],
													'shipping_charges'=>$order_res['shipping_amount'],
													'total_discount'=>$total_discount,
													'sub_total'=>$sub_total,
													'order_items'=>$orddetail
												);
			$res_shipment = api_create_order_sr($params);
			if(is_object($res_shipment)){
					$res_shipment_arr = (array) $res_shipment;
					$res_shipment_encoded = serialize($res_shipment_arr);
					$data  = array('sr_create_order'=>$res_shipment_encoded);
					$where = "order_id = '".$order_res['order_id']."' ";
					$CI->order_model->safe_update('wl_order',$data,$where,FALSE);
			}
			if(isset($res_shipment->shipment_id)){
					$courier_dtls = unserialize($order_res['shipment_carrier_data']);
					$courier_id = $courier_dtls['courier_company_id'];
					$params_awb = array('shipment_id'=>$res_shipment->shipment_id,'courier_id'=>$courier_id);
					$res_awb = generate_awb_sr($params_awb);	
					$awb_code='';
					if(is_object($res_awb)){
							$awb_code=@ $res_awb->response->data->awb_code;
							if($awb_code!=''){
								$res_awb_encoded = serialize($res_awb);
								$data  = array('sr_awb_created'=>$res_awb_encoded);
								$data['awb_code'] = $awb_code;
								$where = "order_id = '".$order_res['order_id']."' ";
								$CI->order_model->safe_update('wl_order',$data,$where,FALSE);
							}
					}
			}
			//trace($res_shipment);
			//trace($res_awb);
			$ret_arr = array('err'=>0,'res_shipment'=>$res_shipment,'res_awb'=>$res_awb);
			return $ret_arr;
		}catch(Exception $e){
			throw $e;
		}
	}
}