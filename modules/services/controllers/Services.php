<?php
class Services extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('service_category/service_category_model','services/services_model','faq/faq_model'));
		$this->load->helper(array('services/services','service_category/service_category','query_string'));
		$this->load->library(array('Dmailer'));
		$this->form_validation->set_error_delimiters("<div class='red fs10'>","</div>");
	}

	public function index()
	{
		$this->page_section_ct = 'services';
		$offset                 = (int) $this->input->post('offset');
		$curr_symbol = display_symbol();
		$posted_data = is_array($this->input->post()) ? array_filter($this->input->post()) : array();

		if($this->input->post()){
			$posted_data= array_filter($this->input->post());
			unset($posted_data['submit']);
		}
//trace($this->input->post());
		$condition               = array();
		$cat_res = '';
		//$record_per_page        = (int) $this->input->post('per_page');
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');

		 $category_id = $this->input->post('category_id') ? (int) $this->input->post('category_id') : $this->uri->rsegment(3);

		$page_segment           = find_paging_segment();

		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');

		$base_url      = ( $category_id!='' ) ?   "services/index/$category_id/pg/" : "services/index/pg/";

		$condition['status']     = '1';

		$condition['orderby']     = 'service_id asc';

		$page_title             = "Projects List";

		$where                   = "wlp.status ='1' ";
		if($this->uri->rsegment(3)=="featured"){
			$where.=" AND is_featured = '1'";
			//$condition['where'] = "featured_service = '1'";
			$page_title = 'Featured Projects';
		}

		if($this->uri->rsegment(3)=="new"){
			$where.=" AND is_new = '1'";
			$page_title = 'New Arrivals';
		}
		$city_ids     =  $this->input->post('citys',TRUE);
		$city_id            = (int)$this->input->get_post('city_id',TRUE);
		$category_id     = (int) $this->input->post('category_id',TRUE);
		$categories     =  $this->input->post('categories',TRUE);
		$keyword			=   trim($this->input->post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		if($category_id!='' )
		{
			$condition['category_id'] = $category_id;
		  //$where.=" AND wlp.category_id = '".$category_id."'";
		}
		if($categories!='' )
			{
				$condition['categories'] = $categories;
			  //$where.=" AND wlp.category_id = '".$category_id."'";
			}
		if($keyword){
			$condition['keyword'] = $keyword;
			$where.=" AND (wlp.service_name LIKE '%".$keyword."%' )";
		}

		if( is_array($posted_data) && !empty($posted_data ))
		{
			$category_id            = (int) $this->input->post('category_id',TRUE);
			$prange                 =  $this->input->post('amount',TRUE);
			$prange = str_replace("$curr_symbol","",$prange);
			//trace($prange);
			if($category_id!='' )
			{
				$condition['category_id'] = $category_id;
				$cat_res = get_db_single_row('wl_service_categories','*'," AND category_id='$category_id'");
				$page_title.=" (".$cat_res['category_name'].")";
			}
			if($city_id )
			{
				$condition['city_id'] = $city_id;
			}
			if($city_ids )
			{
				$condition['city_ids'] = $city_ids;
			}

			if($prange!='' )
			{
				$pranges = explode(',',$prange);
				$where.=" AND (";
				foreach ($pranges as $prange){

					if( strstr($prange,">" ))
					{
						$arr_price=explode(">",$prange);
						if(is_numeric($arr_price[1]))
						{
							$where.=" ( IF(wlp.service_discounted_price>0, wlp.service_discounted_price,wlp.service_price) > '".$arr_price[1]."' ) OR ";
						}
					}
					if(strstr($prange,"<"))
					{
						$arr_price=explode("<",$prange);
						if(is_numeric($arr_price[1]))
						{
							$where.=" ( IF(wlp.service_discounted_price>0 , wlp.service_discounted_price,wlp.service_price) < '".$arr_price[1]."' ) OR ";
						}
					}
					if(strstr($prange,"-"))
					{
						$arr_price=explode("-",$prange);
						$min_price_range = trim($arr_price[0]);
						$max_val = count($arr_price) -1;
						//$max_price_range = trim($arr_price[$max_val]);
						$max_price_range = trim($arr_price[1]);
						if(is_numeric($min_price_range) && is_numeric($max_price_range)){
							$where.=" ( IF(wlp.service_discounted_price>0 , wlp.service_discounted_price,wlp.service_price) > '".$min_price_range."' AND ( IF(wlp.service_discounted_price>0 , wlp.service_discounted_price,wlp.service_price) < '".$max_price_range."' ) ) OR ";
						}
					}
				}
				$where=substr($where,0,-4);
				$where.=" )";
			}
		}

		$condition['where'] = $where;
		$res_array               =  $this->services_model->get_services($config['per_page'],$offset,$condition);
		//echo_sql();
		$config['total_rows'] = $data['totalProduct']	=  get_found_rows();

	  	$data['frm_url'] = $base_url;

		$data['record_per_page'] = $record_per_page;
		$data['heading_title'] = $page_title;
		$data['res']           = $res_array;
		$data['cat_res'] = $cat_res;

		if($this->input->is_ajax_request())
		{
			$this->load->view('services/service_data',$data);
		}
		else
		{
			$this->load->view('services/view_services_listing',$data);
		}

	}	

	public function detail()
	{
		$this->meta_info['entity_id'];
		
		
		$data['unq_section'] = "service";
		$serviceId = (int) $this->meta_info['entity_id'];
		$option = array('serviceid'=>$serviceId);

		$res =  $this->services_model->get_services(1,0,$option);
		
		$data['service_city']=$service_city=$this->services_model->get_service_city($res['service_id']);
		
		$parent=get_parent_categories($res['category_id']);
		
		 $category_links = get_parent_categories($res['category_id'],"AND status!='2'","category_id,parent_id");
			$category_links = array_keys($category_links);
			$category_links = implode(",",$category_links);
		//echo $category_links;
		//faq data fetch
		//trace($category_links);
		$param = array('status'=>'1','category_id_in'=>$category_links);
		$res_faq_array               = $this->faq_model->get_faq(100,0,$param);
		$config['total_faq'] = $data['totalfaq']	= get_found_rows();
		$data['res_faq_array']=$res_faq_array;
		
		
		
		if($this->input->post('action')=='Submit') {
			$this->send_enquiry($res);
		}	


		if(is_array($res)&& !empty($res) )
		{
			$data['title'] = "services";
			$data['res']       = $res;
			$media_res         = $this->services_model->get_service_media(5,0,array('serviceid'=>$res['service_id']));
			$data['media_res'] = $media_res;
			
			$data['category_name'] = get_db_field_value('wl_categories','category_name'," AND category_id='".$res['category_id']."' ");
			$this->load->view('services/view_service_details',$data);

		}else
		{
			redirect('category', '');
		}

	}
	
	public function send_enquiry($res) {
		
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		
		 $product_id = (int)$this->uri->segment(3,0);	//$res['service_id'];	
		 $data['product_id'] = $product_id;
		
		$product_name="";
		if($product_id > 0){
			$product_res =  get_db_single_row('wl_services','service_name, service_code, friendly_url',array('service_id'=>$product_id));
			$product_name=$product_res['service_name'];
			$data['product_name'] = $product_name;
		}
		
		$this->form_validation->set_rules('product_name', 'Service Name', 'trim|required'); 
		$this->form_validation->set_rules('first_name', 'Full Name', 'trim|alpha|required|max_length[80]'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'trim|required|min_length[5]|max_length[25]|callback_valid_mobile[mobile_number]');	
		//$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[80]');
		//$this->form_validation->set_rules('state', 'State', 'trim|required|max_length[80]');
		//$this->form_validation->set_rules('location', 'Location', 'trim|required|max_length[80]');
		//$this->form_validation->set_rules('zipcode', 'Pin Code', 'trim|required|max_length[80]');
	//$this->form_validation->set_rules('document1','document1',"file_required|file_allowed_type[document]");	
	//$this->form_validation->set_rules('document2','document2',"file_required|file_allowed_type[document]");
	
	$this->form_validation->set_rules('document1','document1',"file_allowed_type[document]");	
	$this->form_validation->set_rules('document2','document2',"file_allowed_type[document]");	
			 
		//$this->form_validation->set_rules('description', 'Enquiry', 'trim|required|max_length[8500]');
		//$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[contact]');

	$data['page_error'] = "";
	
	 
    if($this->form_validation->run()==TRUE){
		
		
		$uploaded_file1 = "";
				$filepath1='';
				
				$uploaded_file2 = "";
				$filepath2='';
				
				if( !empty($_FILES) && $_FILES['document1']['name']!='' )
				{			  
					$this->load->library('upload');	
						
					$uploaded_data =  $this->upload->my_upload('document1','documentes');
				
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
						$uploaded_file1 = $uploaded_data['upload_data']['file_name'];
						$filepath1 = UPLOAD_DIR.'/documentes/'.$uploaded_file2;
					
					}		
					
				}	
				
			    if( !empty($_FILES) && $_FILES['document2']['name']!='' )
				{			  
					$this->load->library('upload');	
						
					$uploaded_data =  $this->upload->my_upload('document2','documentes');
				
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
						$uploaded_file2 = $uploaded_data['upload_data']['file_name'];
						$filepath2 = UPLOAD_DIR.'/documentes/'.$uploaded_file2;
					
					}		
					
				}
			
			
		
			$posted_data=array(
			'first_name'    => $this->input->post('first_name'),
			'type'           => '2',
			'email'         => $this->input->post('email'),
			'zipcode'  => $this->input->post('zipcode'),
			'address'  => $this->input->post('address'),
			'state'  => $this->input->post('state'),
			'city'  => $this->input->post('city'),
			'location'  => $this->input->post('location'),
			'mobile_number'  => $this->input->post('mobile_number'),
			'document1'       => $uploaded_file1,
			'document2'       => $uploaded_file2,
			'message'       => $this->input->post('description'),			
			'product_service'    => $product_name,
			'receive_date'     =>$this->config->item('config.date.time')
			);

			$posted_data = $this->security->xss_clean($posted_data);
			$this->services_model->safe_insert('wl_enquiry',$posted_data,FALSE);	
			// Send  mail to user
			$content    =  get_content('wl_auto_respond_mails','8');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$name = ucwords($this->input->post('first_name'));
			$body			=	str_replace('{mem_name}',$name,$body);
			$body 			=   str_replace('{body_text}', 'You have placed an enquiry and details are given below.', $body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			//$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			
			$body			=	str_replace('{product_name}',$product_res['product_name'],$body);
			$body			=	str_replace('{comments}',$this->input->post('description'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			if($filepath1){
					$mail_conf['attachment']=$filepath1;
				}
				if($filepath2){
					$mail_conf['attachment']=$filepath2;
				}			
		
			$this->dmailer->mail_notify($mail_conf);
			// End send  mail to user
			// Send  mail to admin
			$body       =  $content->email_content;
			$name = 'Admin';
			$body			=	str_replace('{mem_name}',ucwords($this->input->post('first_name')),$body);
			$body 			=   str_replace('{body_text}', 'You have received an enquiry and details are given below.', $body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			//$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			
			$body			=	str_replace('{product_name}',$product_res['product_name'],$body);
			$body			=	str_replace('{comments}',$this->input->post('description'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			
			if($filepath1){
					$mail_conf['attachment']=$filepath1;
				}
				if($filepath2){
					$mail_conf['attachment']=$filepath2;
				}
			//trace($mail_conf);
			//exit;
			$this->dmailer->mail_notify($mail_conf);
			// End send  mail to admin
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully.');
			
			//$link_url = base_url().'services/send_enquiry/'.$product_id;
			//$link_url = base_url().$product_res['friendly_url'];
			//redirect($link_url, '');
			//redirect('pages/thanks', '');
			redirect('services/send_enquiry/'.$product_id, '');
			//$this->load->view('pages/view_refer_to_friend',$data);
		}
		
		    
		//$link_url = base_url().$product_res['friendly_url'];
	
	  //redirect($link_url."#enquiry", '');
	  $data['is_header'] = FALSE;
    $this->load->view('services/view_send_enquiry',$data);
	
  }
  
  public function valid_mobile($field)

	{

		

		//$mobile = $this->input->post('mobile_number');

		if($field!=''){

		$mobile = $field;

		

		$this->form_validation->set_message('valid_mobile', 'The %s field may only contain number,+,-.');

		

		return ( ! preg_match("/^([0-9\-\+])+$/i", $mobile)) ? FALSE : TRUE;

		

		}



	}

	public function keyword_suggestions()
	{
		$data = array();
		$data['result'] = array();
		$inputString = trim($this->input->get_post('mysearchString'));
		
		if($inputString!="")
		{
			/*$keywords = explode(" ",$inputString);
			$search_cond='(';
			foreach($keywords as $kwd){
				$search_cond.="(wlp.service_name LIKE '%".$kwd."%' OR wlp.service_code LIKE '%".$kwd."%') AND ";
			}
			$search_cond=substr($search_cond,0,-5);
			*/
			$search_cond="(wlp.service_name LIKE '%".$inputString."%' ) AND wlp.status ='1'";
			
			$param = array();
			//$param['fields'] = "wlp.service_name";
			$param['where'] = $search_cond; //"wlp.search_keywords LIKE '%".$inputString."%' AND wlp.status ='1'";
			$result = $this->services_model->get_services(10,0,$param,'service_id,category_id,service_name,service_price,service_discounted_price');
			//echo_sql();exit;
			$data['result'] = $result;
		}
		
		$data['keyword'] = $inputString;		
		$this->load->view('services/keyword_suggestions',$data);
	}

	public function enquiry()
	{
		$service_id = (int) $this->uri->segment(3);
		if($service_id > 0){
			$services_result    = get_db_single_row('wl_services', 'service_name, service_id, friendly_url', " AND service_id = '".$service_id."' AND status = '1'");
			@$service_name      = $services_result['service_name'];
			@$service_id    = $services_result['service_id'];
			@$friendly_url    = $services_result['friendly_url'];
		}else{
			$service_name = '';
			$service_id = '0';
			$friendly_url = '';
		}

		$this->form_validation->set_rules('service_name', 'service Name','trim|required|max_length[100]');
		$this->form_validation->set_rules('first_name', 'Name','trim|alpha|required|max_length[40]');
		$this->form_validation->set_rules('email','Email ID','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('mobile_number','Mobile Number.','trim|required|max_length[20]');
		$this->form_validation->set_rules('message','Message','trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code');

		if($this->form_validation->run()==TRUE){
			$posted_data=array(
			'service_type'      => 'Service',
			'product_id'      => $service_id,
			'product_service'    => $this->input->post('service_name'),
			'first_name'    => $this->input->post('first_name'),
			'last_name'    => '',
			'email'         => $this->input->post('email'),
			'phone_number'  => '',
			'mobile_number' => $this->input->post('mobile_number'),
			'address'       => '',
			'country'       => '',
			'company_name'       => '',
			'message'       => $this->input->post('message'),
			'type'          => '2',
			'status'        => '1',
			'receive_date'  => $this->config->item('config.date.time')
			);
			//trace($posted_data); die;
			$posted_data = $this->security->xss_clean($posted_data);
			$this->services_model->safe_insert('wl_enquiry',$posted_data,FALSE);

			/* Send  mail to user */
			$content    =  get_content('wl_auto_respond_mails','6');
			$subject    =  $content->email_subject.' '.$this->config->item('config.date.time');

			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$mobile		=	($this->input->post('mobile_number') != '') ? $this->input->post('mobile_number'): 'N/A';
			$name = ucwords($this->input->post('first_name'));
			$sender_name = ucwords($this->input->post('first_name'));

			$body		=	str_replace('{mem_name}',$name,$body);
			$body		=	str_replace('{email}',$this->input->post('email'),$body);
			$body		=	str_replace('{mobile_number}',$mobile,$body);
			$body		=	str_replace('{service_name}',$this->input->post('service_name'),$body);
			$body			=	str_replace('{comments}',nl2br($this->input->post('message')),$body);
			$body			=	str_replace('{sender_name}',$sender_name,$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);

			//trace($mail_conf);
			//exit;
			$this->dmailer->mail_notify($mail_conf);

			/* End send  mail to user */
			/* Send  mail to admin */

			$body       =  $content->email_content;
			$name 		= 	'Admin';

			$body		=	str_replace('{mem_name}',$name,$body);
			$body		=	str_replace('{email}',$this->input->post('email'),$body);
			$body		=	str_replace('{mobile_number}',$mobile,$body);
			$body		=	str_replace('{service_name}',$this->input->post('service_name'),$body);
			$body			=	str_replace('{comments}',nl2br($this->input->post('message')),$body);
			$body			=	str_replace('{sender_name}',$sender_name,$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->input->post('email'),
			'from_name'=> $name,
			'body_part'=>$body
			);

			$this->dmailer->mail_notify($mail_conf);

			/* End send  mail to admin */

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your enquiry has been sent successfully. We will get back to you soon.');
			redirect_top($friendly_url,'refresh');
			exit;
		}

		$data['service_name'] = $service_name;
		$data['service_id'] = $service_id;
		$data['friendly_url'] = $friendly_url;

		$data['title'] = "Send Enquiry";
		$this->load->view('view_enquiry',$data);

	}
	
	public function download_pdf()
	{		
		$pId=(int)$this->uri->segment(3,0);		
		if($pId > 0){			
			$pdf_name='broucher_doc';
			$file=get_db_field_value('wl_services', $pdf_name, array("service_id"=>$pId));
			if($file !="" && @file_exists(UPLOAD_DIR."/services/".$file)){
				$this->load->helper('download');
				$data = file_get_contents(UPLOAD_DIR."/services/".$file);
				$name = $file;
				force_download($name, $data);
			}else{
				redirect('broucher', '');
			}
		}else{
			redirect('broucher', '');
		}
	}

}