<?php
if ( ! function_exists('you_save'))
{	
	function you_save($price,$discount_price)
	{  
		
		if($price!='' && $discount_price!='')
		{
			$you_save = ($price*$discount_price/100);
			return $you_save;		
		}
		
	}
}

function sort_by_price($id,$name='sortbyprice'){
	$CI=CI();
	$sortbyprice=$CI->input->get_post("sortbyprice");
	?>
	<select name="<?php echo $name?>" id=<?php echo $id;?>>
	 <option value="">Price</option>
	 <option value="lth" <?php echo $sortbyprice=="lth"?' selected="selected"':""?>>Low to High</option>
	 <option value="htl" <?php echo $sortbyprice=="htl"?' selected="selected"':""?>>High to Low</option>
	</select>
	<?php 
}

function sort_by_name($id,$name='sortbyname'){
	$CI=CI();
	$sortbyname=$CI->input->get_post("sortbyname");
	?>
	<select name="<?php echo $name?>" id=<?php echo $id;?>>
	 <option value="">Name</option>
	 <option value="az" <?php echo $sortbyname=="az"?' selected="selected"':""?>>A to Z</option>
	 <option value="za" <?php echo $sortbyname=="za"?' selected="selected"':""?>>Z to A</option>
	</select>
	<?php 
}

if ( ! function_exists('rating_html'))
{
	function rating_html($rating,$max_rating,$img_arr=array())
	{
	  if(!is_array($img_arr))
	  {
		$img_arr = array();
	  }
	  if(!array_key_exists('glow',$img_arr))
	  {
		$img_arr['glow'] = '<img alt="" class="ml2" src="'.theme_url().'images/star-1.png">';
	  }
	  if(!array_key_exists('fade',$img_arr))
	  {
		$img_arr['fade'] = '<img alt="" class="ml2" src="'.theme_url().'images/star-2.png">';
	  }
	  $rating = ceil($rating);
	  $rating = $rating > $max_rating ? $max_rfating : $rating;
	  $var = "";
	  $nostar = $max_rating - $rating;
	  
	  for($jx=1;$jx<=$rating;$jx++)
	  {
		$var.=$img_arr['glow'];
	  }

	  for($jx=1;$jx<=$nostar;$jx++)
	  {
		$var.=$img_arr['fade'];
	  }

	  return $var;
	}
}

if ( ! function_exists('service_overall_rating'))
{
	function service_overall_rating($service_id,$entity_type)
	{
		$CI = CI();
		$res = $CI->db->query("SELECT AVG(rate) as rating FROM  wl_review WHERE prod_id ='".$service_id."' AND status ='1' AND rev_type='P' ")->row();
		return $res->rating;
	}
} 