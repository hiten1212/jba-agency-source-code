<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Services_model extends MY_Model
{

	public function get_services($limit='10',$offset='0',$param=array())
	{
//trace($param);
		$category_id		=   @$param['category_id'];
		$categories		=   @$param['categories'];
		$brand_id		=   @$param['brand_id'];
		$city_id		=   @$param['city_id'];
		$city_ids		=   @$param['city_ids'];
		$status			    =   @$param['status'];
		$is_home			    =   @$param['is_home'];
		$serviceid			=   @$param['serviceid'];
		$orderby			=	@$param['orderby'];
		$orderbyo			=	@$param['orderbyo'];
		$orderbyn			=	@$param['orderbyn'];
		$where			    =	@$param['where'];
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		//trace($param);

		if($where!='')
		{
			$this->db->where($where);

		}
		if($category_id!='')
		{
			$this->db->where("FIND_IN_SET( '".$category_id."',wlp.category_links )");
		}
		if($categories!='')
				{
					$this->db->where("FIND_IN_SET( '".$categories."',wlp.category_links )");
				}
		if($serviceid!='')
		{
			$this->db->where("wlp.service_id  ","$serviceid");
		}
		
		if($brand_id!='')
		{
			$this->db->where("wlp.brand_id  ","$brand_id");
		}

		if($status!='')
		{
			$this->db->where("wlp.status","$status");
		}
		if($is_home!='')
		{
			$this->db->where("wlp.is_home",'1');
		}

		$search_prod_type=$this->input->get_post('search_prod_type',TRUE);

		if($search_prod_type=='is_featured')
		{
			$this->db->where("wlp.is_featured",'1');
		}
		if($search_prod_type=='is_new')
		{
			$this->db->where("wlp.is_new",'1');
		}
		
		if($keyword!='')
		{
			$this->db->where("(wlp.service_name LIKE '%".$keyword."%' OR wlp.service_code LIKE '%".$keyword."%' )");
		}
		if($orderbyo!=""){
			$this->db->order_by('sort_order ','asc');
		}
		if($this->input->get_post("sortbyprice")!=""){
			$orderby = $this->input->get_post("sortbyprice");
		}
		if($this->input->get_post("sortbyname")!=""){
			$orderbyn = $this->input->get_post("sortbyname");
		}

		if($orderby!='')
		{
			$orderby=$this->db->escape_str($orderby);
			if($orderby=="lth"){
				$this->db->order_by('price ','asc');
			}
			if($orderby=="htl"){
				$this->db->order_by('price ','desc');
			}
		}

		if($orderbyn!='')
		{
			$orderbyn=$this->db->escape_str($orderbyn);
			if($orderbyn=="az"){
				$this->db->order_by('service_name ','asc');
			}
			if($orderbyn=="za"){
				$this->db->order_by('service_name ','desc');
			}
		}

		if(empty($orderby) && empty($orderbyn))
		{
			$this->db->order_by('wlp.service_id ','desc');
		}

	  $this->db->group_by("wlp.service_id");
	  if($limit!="")
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wlp.*,wlp.status as service_status,wlpm.media,wlpm.media_type,wlpm.is_default, IF(service_discounted_price = 0,service_price,service_discounted_price ) as price,wlp.text_colour',FALSE);
		$this->db->from('wl_services as wlp');
		$this->db->where('wlp.status !=','2');
		
		$this->db->join('wl_services_media AS wlpm','wlp.service_id=wlpm.service_id','left');
		if($city_id!=''){
			$this->db->join('wl_services_city AS wls','wlp.service_id=wls.services_id AND wls.city_id='.$city_id);
		}
		if($city_ids!=''){
			$this->db->join('wl_services_city AS wls','wlp.service_id=wls.services_id AND wls.city_id IN ('.$city_ids.')');
		}

		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? @$result[0]: $result;
		return $result;

	}

	public function get_service_media($limit='4',$offset='0',$param=array())
	{

		$default			    =   @$param['default'];
		$serviceid			    =   @$param['serviceid'];
		$media_type			=   @$param['media_type'];
		if( is_array($param) && !empty($param) )
		{
			$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE);
			$this->db->limit($limit,$offset);
			$this->db->from('wl_services_media');
			$this->db->where('service_id',$serviceid);

			if($default!='')
			{
				$this->db->where('is_default',$default);
			}
			if($media_type!='')
			{
				$this->db->where('media_type',$media_type);
			}

			$q=$this->db->get();
			//echo_sql();
			$result = $q->result_array();
			$result = ($limit=='1') ? $result[0]: $result;
			return $result;

		 }

	}

	public function related_services_added($serviceId,$limit='NULL',$start='NULL')
	{
		$res_data =  array();
		$condtion = ($serviceId!='') ? "status ='1' AND service_id = '$serviceId' ":"status ='1'";
		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"id DESC",
													'limit'=>$limit,
													'start'=>$start,
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_services_related',$fetch_config);
		if( is_array($result) && !empty($result) )
		{
			foreach ($result as $val )
			{
				$res_data[$val['id']] =$val['related_id'];
			}
		}
		return $res_data;
	}

	public function update_viewed($id,$counter=0)
	{
	  $id = (int) $id;
	  if($id>0)
	  {
		  $posted_data = array(
					'services_viewed'=>($counter+1)
				 );

		  $where = "service_id = '".$id."'";
		  $this->category_model->safe_update('wl_services',$posted_data,$where,FALSE);
	  }

	}

	public function get_related_services($serviceId)
	{
		$condtion = (!empty($serviceId)) ? "status !='2'  AND service_id NOT IN(".implode(",",$serviceId). ")" :"status !='2'";
		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"service_id DESC",
													'limit'=>'NULL',
													'start'=>'NULL',
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_services',$fetch_config);
		return $result;
	}

	public function related_services($serviceId,$start='NULL',$limit='NULL')
	{
		$res_data =  array();
		$condtion = ($serviceId!='') ? "status ='1' AND service_id = '$serviceId' ":"status ='1'";
		$fetch_config = array(
		'condition'=>$condtion,
		'order'=>"RAND()",
		'limit'=>$limit,
		'start'=>$start,
		'debug'=>FALSE,
		'return_type'=>"array"
		);

		$result = $this->findAll('wl_services_related',$fetch_config);
		if( is_array($result) && !empty($result) )
		{
			foreach ($result as $val )
			{
				$res_data[$val['id']] = $this->get_services(1,0, array('serviceid'=>$val['related_id']));
			}
		}
		$res_data = array_filter($res_data);
		return $res_data;
	}

	public function get_shipping_methods()
	{
		$condtion = "status =1";
		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"shipping_id DESC",
													'debug'=>FALSE,
													'return_type'=>"array"
													);
		$result = $this->findAll('wl_shipping',$fetch_config);
		return $result;
	}

	public function get_service_categories()
	{

		$this->db->select('wc.category_id, wc.category_name');
		$this->db->from('wl_categories as wc');
		$this->db->join('wl_services as wp','wp.category_id=wc.category_id');
		$this->db->where('wp.status !=','2');
		$this->db->where('wc.status !=','2');
		$this->db->order_by('wc.category_name asc');
		$this->db->group_by('wp.category_id');
		$q=$this->db->get();
		return $result = $q->result_array();

	}
	
	public function get_service_by_id($id)
	{
		$id = (int) $id;
		if($id>0)
		{
			$this->db->select('*');
			$this->db->where('service_id',$id);
			$q=$this->db->get();
			return $result = $q->result_array();

		}
	}
	
	public function get_service_city($id)
	{
		$id = (int) $id;
		if($id>0)
		{

			$this->db->select('ws.city_id, ws.city_name');
			$this->db->from('wl_services_city as wps');
			$this->db->join('wl_city as ws','ws.city_id=wps.city_id');
			//$this->db->join('wl_product_stock as wpst','wpst.product_size=ws.size_id');
			$this->db->where('wps.services_id',$id)->group_by('wps.city_id')->order_by('wps.city_id desc');
			$q=$this->db->get();
			//echo $this->db->last_query();
			return $result = $q->result_array();

		}
	}
	
	
}