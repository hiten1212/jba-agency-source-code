<?php
$this->load->model(array('services/services_model'));
$where = "wlp.status ='1' AND wlp.is_home='1'";
$param = array('where'=>$where,'orderby'=>'wlp.service_id DESC');
$res_array = $this->services_model->get_services(3,0,$param);

$welcome_content = get_db_field_value('wl_cms_pages','page_description'," AND page_id='6'");

if(is_array($res_array) && !empty($res_array))
{
	?>
	<section class="business">
  <p class="bus-line"></p>
  <p class="bus-head"></p>
  <div class="container">
    <div class="business-main">
      <p class="our-business">our business</p>
      <div class="business-cont"><?php echo char_limiter(strip_tags($welcome_content),899);?></div>
      
	  <?php
	  $m=1;
	    foreach($res_array as $val)
	    {
		    $link_url = site_url($val['friendly_url']);
		    $serviceImage=get_image('services',$val["media"],'495','380');
		    ?>
	  <div class="bus-block-<?php echo $m;?>">
        <div class="bus-img">
          <p class="number"><?php echo $m;?></p>
          <p class="bus-gradi"></p>
          <figure><a href="<?php echo $link_url;?>"><img src="<?php echo $serviceImage;?>" alt="<?php echo $val['service_alt'];?>"/></a></figure>
        </div>
        <div class="cont-block">
          <p class="head"><a href="<?php echo $link_url;?>"><?php echo char_limiter($val['service_name'],20);?> </a></p>
          <p class="cont"><?php echo char_limiter($val['service_description'],400);?></p>
        </div>
      </div>	  
      <?php $m++;}?>
      <p class="clearfix"></p>
      <p class="view_alls"><a href="<?php echo site_url('service_category');?>" title="View All Business">View All Business</a></p>
      <p class="clearfix"></p>
    </div>
  </div>
</section>	
	<?php
}?>