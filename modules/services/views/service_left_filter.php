<?php echo form_open('services','id="srcfrm" name="srcfrm"');?>

<input type="hidden" name="citys" id="city" value="<?php echo $this->input->post('citys',TRUE);?>" />
<?php /*?><input type="hidden" name="colors" id="color" value="<?php echo $this->input->post('colors',TRUE);?>" /><?php */?>
<input type="hidden" name="categories" id="category" value="<?php echo $this->input->post('categories',TRUE);?>" />
<?php /*?><input type="hidden" name="brands" id="brand" value="<?php echo $this->input->post('brands',TRUE);?>" />
<input type="hidden" name="materials" id="material" value="<?php echo $this->input->post('materials');?>" /><?php */?>
<?php
if($this->uri->rsegment(1)=="category" && $this->uri->rsegment(2)=="index" ){
	?>
	<input type="hidden" name="category_id" id="category_id" value="<?php echo $this->uri->rsegment(3);?>" />
	<?php
}else{
	?>
	<input type="hidden" name="category_id" id="category_id" value="<?php echo $this->input->get_post('category_id');?>" />
	<?php
}

if($this->uri->rsegment(3)=="new"){
	?>
	<input type="hidden" name="new" id="new" value="<?php echo $this->uri->rsegment(3);?>" />
	<?php
}else{
	?>
	<input type="hidden" name="new" id="new" value="<?php echo $this->input->get_post('new');?>" />
	<?php
}
if($this->uri->rsegment(3)=="featured"){
	?>
	<input type="hidden" name="featured" id="featured" value="<?php echo $this->uri->rsegment(3);?>" />
	<?php
}else{
	?>
	<input type="hidden" name="featured" id="featured" value="<?php echo $this->input->get_post('featured');?>" />
	<?php
}?>
<input type="hidden" name="keyword2" id="keyword2" value="<?php echo $this->input->get_post('keyword2');?>" />
<?php echo form_close();
$curr_symbol = display_symbol();
 
 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 100;
   $res_left = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
?>
<div class="col-12 col-lg-3">
<div class="list_left">
<p class="fs11 float-right clear_all"><a href="javascript:void(0)" class="uu clear_all">Clear All</a></p>
<h2 class="fs14 d-none d-lg-block"><b>Filter Results</b></h2>
<h2 class="d-md-block d-lg-none showhide hand">Filter Results <i class="fas fa-bars"></i><p class="clearfix"></p></h2>

<div class="filter_dis">
<div class="flter_bx">
<div class="filt_hed">Categories</div>
<div class="list_area">
<!--<p class="left_attribute1" data-toggle="collapse" data-target="#cat01">
<a href="javascript:void(0)">Category Name</a></p>-->
<?php if(!empty($res_left) && is_array($res_left)){
	$cat_array=explode(",",$this->input->post("categories",TRUE));
	?>
<div class=" subcate_list collapse show" style="">
<div class="scroll_bar2" id="style2">
<?php $cj=1;
	 foreach($res_left as $key=>$val){
		 $sel=@in_array($val["category_id"],$cat_array)?'checked="checked"':"";
		  if($cj==1) { $act_cls='class="mt3"'; $clss="mt5"; } else { $act_cls=''; $clss="mt2";}	
		
		 ?>
<div><span class="cust_check"><input name="category_array" <?php echo $sel;?> value="<?php echo $val['category_id'];?>" onclick="multisearch('category','category_array');" class="mr-2 cat" type="checkbox"><?php echo $val['category_name'];?></span></div>
<?php $cj++;
	 }?> 

</div>
</div>
<?php } ?>
<!-- services category -->
<?php  $this->load->model("city/city_model");
$cityres=$this->city_model->get_citys(array("condition"=>" AND status ='1'","order"=>"city_name ASC"));
if(!empty($cityres) && is_array($cityres)){
   $city_array=explode(",",$this->input->post("citys",TRUE));
   //trace($city_array);
   ?>
<div class="filt_hed mt-3">City</div>
<div class=" subcate_list collapse show" style="">
<div class="scroll_bar2" id="style2">
 <?php
   foreach($cityres as $key=>$val){
	  
	   $sel=@in_array($val["city_id"],$city_array)?'checked="checked"':"";
	   ?>
<div><span class="cust_check"><input name="city_array" type="checkbox" <?php echo $sel;?> value="<?php echo $val['city_id'];?>" onclick="multisearch('city','city_array');"  class="sz mr-2"><?php echo $val['city_name'];?></span></div>
<?php } ?>

</div>
</div>
<!-- city listing -->
<?php } ?>

</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(e) {
	jQuery(':checkbox, :radio').on('click',function(){
		jQuery('#srcfrm').submit();
	});
	jQuery('.clear_color').on('click',function(){
		jQuery('.col').attr('checked', false);
		jQuery('#color').val('');
		jQuery('#srcfrm').submit();
	});
	jQuery('.clear_size').on('click',function(){
		jQuery('.sz').attr('checked', false);
		jQuery('#city').val('');
		jQuery('#srcfrm').submit();
	});
	jQuery('.clear_brand').on('click',function(){
		jQuery('.bd').attr('checked', false);
		jQuery('#brand').val('');
		jQuery('#srcfrm').submit();
	});
	jQuery('.clear_price').on('click',function(){
		jQuery('.pr').attr('checked', false);
		jQuery('#amount').val('');
		jQuery('#srcfrm').submit();
	});
});
</script>