<?php $this->load->view("top_application"); ?>
<?php //$this->load->view('banner/inner_page_top_banner');
$segment=3;
@$catid = (int) $cat_res['category_id'];
if($catid){
	echo category_breadcrumbs($catid,$segment);
}else{	
	echo navigation_breadcrumb($heading_title);	
}?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   }</script>



<!-- MIDDLE STARTS -->
<div class="container">
<div class="cms">
<h1><?php echo $heading_title;?></h1>
<?php
	  if(is_array($res) && !empty($res) )
      {
	 echo form_open("",'id="myform" method="post" ');?>
	      <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page',TRUE);?>">
	      <input type="hidden" name="offset" value="0">
	      <input type="hidden" name="categories" id="category" value="<?php echo $this->input->post('categories',TRUE);?>" />
	      <?php
	      if($this->uri->rsegment(1)=="category" && $this->uri->rsegment(2)=="index" )
	      {
		      ?>
		      <input type="hidden" name="category_id" id="category_id" value="<?php echo $this->uri->rsegment(3);?>" />
		      <?php
	      }
	      else
	      {
		      ?>
		      <input type="hidden" name="category_id" id="category_id" value="<?php echo $this->input->get_post('category_id',TRUE);?>" />
		      <?php
	      }?>
	      <input type="hidden" name="keyword2" id="keyword2" value="<?php echo $this->input->get_post('keyword2',TRUE);?>" />
          
           <?php echo form_close();?>

<div class="row" id="prodListingContainer">


<?php $data = array('res'=>$res);
	        $this->load->view('services/service_data',$data);?> 

<?php if($totalProduct > $record_per_page){?>
<p class="text-center mt-3 ac dn" id="loadingdiv" ><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p>
<?php }?>
</div>		  

<div class="clearfix"></div>
<?php
	}else{	?>
<div style="text-align:center; margin-left:100px;" ><strong><?php echo $this->config->item('no_record_found');?></strong></div>
	<?php } ?>


</div>



</div>
</div>
<div class="clearfix"></div>
<!-- MIDDLE ENDS -->


<script>
jQuery(document).ready(function(e) {
  jQuery('[id ^="per_page"]').on('change',function(){
		$("[id ^='per_page'] option[value=" + jQuery(this).val() + "]").attr('selected', 'selected');
		jQuery("input[name='per_page']","#myform").val($(this).val());
		jQuery('#myform').submit();
	});
	
	jQuery('.clear_all').on('click',function(){
		jQuery('.bd, .pr, .sz, .col, .cat').attr('checked', false);
		jQuery('#brand').val('');
		jQuery('#color').val('');
		jQuery('#city').val('');
		jQuery('#amount').val('');
		jQuery('#category').val('');
		jQuery('#srcfrm').submit();
	});
	
});

jQuery('[id ^="display_order_name"]').on('change',function(){
 	$("[id ^='display_order_name'] option[value='" + jQuery(this).val() + "']").attr('selected', 'selected');
 	 $(':hidden[name="offset"]').val(0);
	jQuery('#myform').submit();
});
</script>

<script>
    $(function () {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-plus-circle').removeClass('fa-minus-circle');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa-minus-circle').removeClass('fa-plus-circle');
            }
            e.stopPropagation();
        });
    });
    </script>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>