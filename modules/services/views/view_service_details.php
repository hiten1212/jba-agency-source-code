<?php $this->load->view("top_application");
//$this->load->view('banner/inner_page_top_banner');
echo category_breadcrumbs($res['category_id'],'',$res['service_name'])?>

<!-- MIDDLE STARTS -->
<div class="container">
<div class="cms text-center">

<div class="offset-lg-2 col-lg-8">
<h1><?php echo $res['service_name'];?></h1>

<p class="mt-3 text-center"><a data-fancybox data-type="iframe" data-src="<?php echo site_url('services/send_enquiry/'.$res['service_id']);?>" href="javascript:void(0);" class="pop2 btn btn-danger">Send Enquiry</a></p>
<?php
   $thumbc['width']=500;
   $thumbc['height']=419;
   $thumbc['source_path']=UPLOAD_DIR.'/services/';
   if(is_array($media_res) && !empty($media_res)){
	   ?>
<div id="owl-details" class="owl-carousel owl-theme mt-3">
<?php
	  foreach($media_res as $v){
		  $thumbc['org_image']=$v['media'];
		  Image_thumb($thumbc,'R');
		  $cache_file="thumb_".$thumbc['width']."_".$thumbc['height']."_".$thumbc['org_image'];
		  $catch_img_url=thumb_cache_url().$cache_file;
		  ?>
<div class="item p-1"><img src="<?php echo get_image('services',$v['media'],790,535,'R');?>" class="w-100 radius-10" alt=""></div>
	  <?php } ?>
</div>
	  <?php } ?>

</div>
<?php if(!empty($res['service_description'])){?>
<div class="mt-4"><b class="text-uppercase fs16">Description</b><br>
<?php echo $res['service_description'];?></div>
<?php } ?>

</div>
</div>
<!-- MIDDLE ENDS -->
<div class="clearfix"></div>

<?php $this->load->view("bottom_application");?>