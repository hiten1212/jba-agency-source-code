<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_model extends MY_Model
 {

		 
	 public function get_news($limit='10',$offset='0',$param=array())
	 {		
		
		$status			    =   @$param['status'];
		$orderby		    =   @$param['orderby'];	
		$latest_news		    =   @$param['latest_news'];
		$where		        =   @$param['where'];	
		$jwhere			    =	@$param['jwhere'];
		
		$keyword = $this->db->escape_str($this->input->get_post('keyword',TRUE));
			
	    if($status!='')
		{
			$this->db->where("wlp.status",$status);
		}
		 if($latest_news!='')
		{
			$this->db->where("wlp.latest_news",$latest_news);
		}
		
	    if($where!='')
		{
			$this->db->where($where);
		}
		
		
		if($keyword!='')
		{
						
			$this->db->where("(wlp.news_title LIKE '%".$keyword."%' OR wlp.publisher LIKE '%".$keyword."%' )");
				
		}
		if($orderby!='')
		{
			 $this->db->order_by($orderby);
			
		}else
		{
		  $this->db->order_by('wlp.news_id','desc');
		}
		
		$this->db->group_by("wlp.news_id");
		$this->db->limit($limit,$offset);
		if($this->uri->segment(1)=='sitepanel')
		{
			$rev_cond='';
		}
		else
		{
			$rev_cond='status=1 AND ';
		}
		
		$this->db->select('SQL_CALC_FOUND_ROWS wlp.*,wlp.status as product_status,wlpm.media,wlpm.media_type,wlpm.is_default',FALSE);		
		$this->db->from('tbl_news as wlp');
		$this->db->where('wlp.status !=','2');
		
		$this->db->join('wl_news_media AS wlpm','wlp.news_id=wlpm.products_id','left');
		
		$q=$this->db->get();
		//echo_sql();
		//exit;
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
				
	}
		  
	public function get_news_media($limit='4',$offset='0',$param=array())
    {

		 $default			    =   @$param['default'];
		 $productid			    =   @$param['productid'];
		 $media_type			=   @$param['media_type'];
		 $where                 =   @$param['where'];
		 //$color_id=$this->uri->segment('6');
		 if( is_array($param) && !empty($param) )
		 {
			$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE);
			if($limit)
			$this->db->limit($limit,$offset);
			$this->db->from('wl_news_media');
			$this->db->where('products_id',$productid);

			if($default!='')
			{
				$this->db->where('is_default',$default);
			}
			if($media_type!='')
			{
				$this->db->where('media_type',$media_type);
			}
						
			if($where!=''){			
				$this->db->where($where);			
			}
			
			$this->db->order_by('id ASC');
			
			$q=$this->db->get();
			//echo_sql();
			$result = $q->result_array();
			$result = ($limit=='1') ? $result[0]: $result;
			return $result;

		 }

	}
	 
}
?>