<?php
class News extends Public_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('news/news_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'news';
	}

	public function index()
	{
		 $this->page_section_ct = 'news';
		   
		// $this->config->set_item('per_page','2');
		 
		//$record_per_page        = (int) $this->input->post('per_page');	
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');	
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$base_url      =  "news/index/pg/";
		
		$from_date  = $this->db->escape_str(trim($this->input->get_post('from_date',TRUE)));
		$to_date    = $this->db->escape_str(trim($this->input->get_post('to_date',TRUE)));
		//exit;
		$condition ='';
		
		if($from_date!='' ||  $to_date!='')
		{
			$condition_date=array();
			$condition.=" AND (";
			
			if($from_date!='')
			{
				$condition_date[] = "DATE(wlp.recv_date) >='$from_date'";
			}
			if($to_date!='')
			{
				$condition_date[] ="DATE(wlp.recv_date) <='$to_date'";
			}
			$condition.=implode(" AND ",$condition_date)." )";
		}
		
		$param 	= array("where"=>"wlp.status = '1' $condition ");
				
		//$param = array('status'=>'1');
		$res_array     = $this->news_model->get_news($config['per_page'],$offset,$param);
       //echo_sql();
				
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct'] = $total_rows;
		$data['record_per_page'] = $config['per_page'];
		$data['title'] = 'News';
		$data['res'] = $res_array; 		
		$data['base_url'] = $base_url;
		$data['frm_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE)
		{
		  $this->load->view('news/load_news_view',$data);
		}
		else
		{
		  $this->load->view('news/view_news',$data);
		}
		   
			
	}		
	
	public function details()
	{	
		$this->page_section_ct = 'news';
	
		//$id = (int) $this->uri->rsegment(3);	
		$id= (int) $this->meta_info['entity_id'];	
		$param     = array('wlp.status'=>'1','where'=>"wlp.news_id ='$id' ");	
		$res       = $this->news_model->get_news(1,0,$param);	
		
		if(is_array($res) && !empty($res))
		{			
			$data['title'] = 'News ';
		    			
			$data['res']       = $res;
			$media_res         = $this->news_model->get_news_media(4,0,array('productid'=>$res['news_id']));
			
			$data['media_res'] = $media_res;
						
		    $this->load->view('news/news_details_view',$data);
		
			
		}else
		{
			redirect('news', ''); 
			
		}
		
	}

	public function download_pdf()
	{
	  $id = (int) $this->uri->segment(3);

	  $param     = array('status'=>'1','where'=>"news_id ='$id' ");	

	  $res_topic       = $this->news_model->get_news(1,0,$param);	
		  

	  if(is_array($res_topic) && !empty($res_topic))
	  {
		if($res_topic['news_pdf']!='' && file_exists(UPLOAD_DIR."/news/pdf/".$res_topic['news_pdf']))
		{
			$this->load->helper('download');
			$data = file_get_contents(UPLOAD_DIR."/news/pdf/".$res_topic['news_pdf']);
			$name = $res_topic['news_pdf'];
			force_download($name, $data); 
		}
	  }
	  
	}
	
	
}


/* End of file pages.php */

?>
