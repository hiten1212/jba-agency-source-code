<?php $this->load->view("top_application");
if($res['alt_tag']){
		$alt_tag=ucfirst($res['alt_tag']);
}else{
	$alt_tag=ucfirst($res['news_title']);
}
     $dateval= getDateFormat($res['recv_date'],1);
	   $date1=explode(',',$dateval);
	  $date2=explode(' ',$date1[0]);
?>
<?php navigation_breadcrumb($res['news_title'],array('News & Media'=>'news'));?>


<div class="container">
<div class="cms">
<div class="row">
<?php
   $thumbc['width']=420;
   $thumbc['height']=420;
   $thumbc['source_path']=UPLOAD_DIR.'/news/';
   if(is_array($media_res) && !empty($media_res)){	   ?>
<div class="col-lg-4 no_pad">
	<div id="owl-media" class="owl-carousel owl-theme">
	<?php
	foreach($media_res as $v)
	{
		$thumbc['org_image']=$v['media'];
		Image_thumb($thumbc,'R');
		$cache_file="thumb_".$thumbc['width']."_".$thumbc['height']."_".$thumbc['org_image'];
		$catch_img_url=thumb_cache_url().$cache_file;
		?>
      <div class="item"><img src="<?php echo get_image('news',$v['media'],420,420,'R');?>" class="w-100 radius-10" alt=""></div>
	  <?php }?>
</div>
</div>
<?php }?>
<div class="col-lg-8 no_pad pl-lg-5 mb-4">
<h1><?php echo $res['news_title'];?></h1>
<p class="fs13 mt-1"><?php echo getDateFormat($res['recv_date'],1);?></p>
	<p class="mt-4 mb-3 bb2"></p>
<div class="follow_sec"> <a href="javascript:void(0)" title="Facebook" style="background:#3a5897;"><span class="fab fa-facebook-f"></span></a> <a href="javascript:void(0)" title="Twitter" style="background:#1ea1f3;"><span class="fab fa-twitter"></span></a> <a href="javascript:void(0)" title="Instagram" style="background:#cb2028;"><span class="fab fa-instagram"></span></a> <a href="javascript:void(0)" title="Youtube" style="background:#cf2567;"><span class="fab fa-youtube"></span></a> <a href="javascript:void(0)" title="Linkedin" style="background:#007bb6;"><span class="fab fa-linkedin-in"></span></a></div>
<hr>
<div class="mt-3 fs15"><?php echo $res['news_description'];?></div>
</div>

</div>

</div>
</div>
<?php $this->load->view("bottom_application");?>