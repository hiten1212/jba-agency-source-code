<?php
$this->load->model(array('news/news_model'));
$param         = array('status'=>'1','latest_news'=>'1');	
$res_array     = $this->news_model->get_news(4,0,$param);

if(is_array($res_array) && !empty($res_array))
{
?>
<div class="categories_areas">
  <div class="container">
    <p class="heading mb-2">Latest News</p>
    <!--<p class="courses_txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>-->
    <div id="owl-news" class="owl-carousel owl-theme mt-3">
      <?php
		  foreach($res_array as $val)
		  {
			  $news_links = site_url($val['friendly_url']);
			  
			  if($val['alt_tag']){
				  $alt_tag=escape_chars(ucfirst($val['alt_tag']));
			  }else{
				  $alt_tag=escape_chars(ucfirst($val['news_title']));
			  }
				  $post_date=@explode(" ",getDateFormat($val["recv_date"],10));
	  ?>
	  <div class="item">
        <div class="news_box">
          <p class="news_pic"><span><a href="<?php echo $news_links;?>" title="<?php echo escape_chars(ucfirst($val['news_title']));?>"><img src="<?php echo get_image('news',$val['media'],'166','115','AR') ?>" alt="<?php echo $alt_tag?>"></a></span></p>
          <p class="news_date"><?php echo getDateFormat($val["recv_date"],1);?></p>
          <p class="news_title"><a href="<?php echo $news_links;?>" title="<?php echo escape_chars(ucfirst($val['news_title']));?>"><?php echo char_limiter($val['news_title'],70); ?></a></p>
          <p class="clearfix"></p>
        </div>
      </div>
       <?php } ?>      
    </div>
    <p class="mt-4 text-center"><a href="<?php echo site_url('news');;?>" class="viewmore_btn">See More News</a></p>
  </div>
</div>
 <?php
}
?>
