<?php
if(is_array($res) && !empty($res) )
{ 
  foreach($res as $val)
  {    
	$news_links = site_url($val['friendly_url']);	
	if($val['alt_tag']){
		$alt_tag=ucfirst($val['alt_tag']);
	}else{
		$alt_tag=ucfirst($val['news_title']);
	}
	 $dateval= getDateFormat($val['recv_date'],1);
	   $date1=explode(',',$dateval);
	  $date2=explode(' ',$date1[0]);
		//$date3=explode(' ',$date1[1]);
	  //trace($date2[0]);		
  ?>  
  <div class="col-md-6 col-xl-4 no_pad listpager">
<div class="news_box">
<p class="news_pic"><span><a href="<?php echo $news_links;?>" title="<?php echo $val['news_title']; ?>"><img src="<?php echo get_image('news',$val['media'],'166','115','AR') ?>" alt="<?php echo $alt_tag?>"></a></span></p>
<p class="news_date"><?php echo getDateFormat($val['recv_date'],1);?></p>
<p class="news_title"><a href="<?php echo $news_links;?>" title="<?php echo $val['news_title']; ?>"><?php echo char_limiter($val['news_description'],90); ?></a></p>
<p class="clearfix"></p>
</div>
</div>        
  <?php
 }
}
?>