<?php $this->load->view("top_application");?>
<?php navigation_breadcrumb('News & Media');?>

<script type="text/javascript">function serialize_form() { return $('#ord_frm').serialize();   } </script>
<div class="container">
<div class="cms">
<h1 class="float-md-left">News / Media</h1>


<?php echo form_open("",'id="ord_frm"  name="ord_frm" method="post" ');?>
<input type="hidden" name="per_page" value="<?php echo $this->input->post('per_page');?>">
<input type="hidden" name="offset" id="offset" value="0">
<input type="hidden" name="from_date" value="<?php echo $this->input->post('from_date');?>">
<input type="hidden" name="end_date" value="<?php echo $this->input->post('end_date',TRUE);?>" />
  
  <p class="float-md-right">
	Date Range : From <input name="from_date" id="from_date" type="text" class="p-2 pt-1 pb-1 radius-5 start_date1" readonly="readonly" value="<?php echo set_value('from_date');?>"> To <input name="to_date" id="to_date" type="text" class="p-2 pt-1 pb-1 radius-5 end_date1" readonly="readonly" value="<?php echo set_value('to_date');?>"> <input name="submit" type="submit" value="Submit" class="btn btn-success mr-2"> 
	<?php   
	 if($this->input->get_post('from_date')!='' || $this->input->get_post('to_date')!='' ){ 
		    echo anchor("news/",'<span>Clear</span>','class="btn btn-danger btn-sm radius-3"');
		 } 
	?>
	</p>
	<p class="clearfix"></p>
  
  <?php echo form_close();?>
  
<?php if(is_array($res) && !empty($res)){ 
$data['res'] = $res;?>
	
	<div class="row" id="prodListingContainer">

      <?php $this->load->view('news/load_news_view',$data);?>
	  
   <div class="clearfix"></div>
<?php if($totalProduct > $record_per_page){?>
<div class="text-center" id="loadingdiv"><img src="<?php echo theme_url()?>images/loader.gif" alt=""></div>
<?php } ?>    
</div>
<?php
	}else{
	 echo '<div class="mt50 mb30 text-center ac"><strong>No record found !</strong></div>';
	}
	?>
</div>
</div>

 
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php

//$default_date = date('Y-m-d',strtotime(date('Y-m-d',time())."-1835 days")); //'2013-01-01';
$default_date = '2019-06-12';
$posted_start_date = $this->input->post('from_date');
?>
<script type="text/javascript">
$(document).ready(function(){
	$('[id ^="per_page"]').on('change',function(){
		$("[id ^='per_page'] option[value=" + jQuery(this).val() + "]").attr('selected', 'selected');
		jQuery("input[name='per_page']","#ord_frm").val($(this).val());
		$(':hidden[name="end_date"]','#ord_frm').val($(this).val());
		$('#ord_frm').submit();
	});
	$('.btn_sbt2').on('click',function(e){
		e.preventDefault();
		$start_date = $('.start_date1:eq(0)').val();
		$end_date = $('.end_date1:eq(0)').val();
		$start_date = $start_date=='From' ? '' : $start_date;
		$end_date = $end_date=='To' ? '' : $end_date;
		$(':hidden[name="keyword"]','#ord_frm').val($('input[type="text"][name="keyword"]').val());
		$(':hidden[name="start_date"]','#ord_frm').val($start_date);
		$(':hidden[name="end_date"]','#ord_frm').val($end_date);
		$("#ord_frm").submit();
	});

	$('.start_date,.end_date').on('click',function(e){
		e.preventDefault();
		cls = $(this).hasClass('start_date') ? 'start_date1' : 'end_date1';
		$('.'+cls+':eq(0)').focus();
	});

		$(".start_date1").datepicker({
			showOn: "focus",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			defaultDate: 'y',
			buttonText:'',
			minDate:'<?php echo $default_date;?>' ,
			maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())));?>',
			yearRange: "c-100:c+100",
			buttonImageOnly: true,
			onSelect: function(dateText, inst) {
				$('.start_date1').val(dateText);
				$( ".end_date1").datepicker("option",{
					minDate:dateText ,
					maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())));?>',
				});
			}		
	});

		$(".end_date1").datepicker({
			showOn: "focus",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			defaultDate: 'y',
			buttonText:'',
			minDate:'<?php echo $posted_start_date!='' ? $posted_start_date :  $default_date;?>' ,
			maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())));?>',
			yearRange: "c-100:c+100",
			buttonImageOnly: true,
			onSelect: function(dateText, inst) {
				$('.end_date1').val(dateText);
			}
		});
	});
</script>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>