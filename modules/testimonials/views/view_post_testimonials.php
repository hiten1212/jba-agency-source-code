<div class="bb pt-1 pb-1">
<div class="container">
<?php 		//echo validation_message();
		echo error_message();?>
<p class="text-uppercase fs16 weight600 text-center shownext"><a href="#">Post Testimonial <i class="fas fa-chevron-down ml-1"></i></a></p>
<div class="offset-lg-1 col-lg-10 post_testi_box <?php echo ($this->input->post('post')=='Post') ? 'dn' : 'dn'?>" style="<?php echo ($this->input->post('post')=='Post') ? 'display: block;' : 'display: none;'?>">
<?php 

echo  form_open_multipart('testimonials#tis','class="form-horizontal tab_hider"');?>
<div class="row">
<div class="col-md-6 p-1"><input name="poster_name" type="text" class="" placeholder="Name *" required value="<?php echo set_value('poster_name');?>" ><?php echo form_error('poster_name');?>
</div>
<div class="col-md-6 p-1"><input name="email" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Invalid email address" required  class="" placeholder="E-mail *" value="<?php echo set_value('email');?>" ><?php echo form_error('email');?></div>
<?php /* <div class="col-md-6 p-1"><input name="photo" type="file" class="" placeholder="photo *">
<?php echo form_error('photo');?></div>*/ ?>
<div class="col-12 p-1">
  <textarea name="testimonial_description" required cols="40" rows="4" class="" placeholder="Description *"><?php echo set_value('testimonial_description');?></textarea><?php echo form_error('testimonial_description');?>
</div>
<div class="col-12 p-1 text-center"><input required name="verification_code" id="verification_code" type="text" class="" placeholder="Enter Code *" style="width:120px"><span class="mt-1"><img src="<?php echo site_url('captcha/normal'); ?>" class="mw_98 ml5" alt="" id="captchaimage1" style="margin-left:5px;"> <a href="javascript:viod(0);" title="Change Verification Code"> <img src="<?php echo theme_url();?>images/refresh.png" alt="Refresh" onclick="document.getElementById('captchaimage1').src='<?php echo site_url('captcha/normal');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="ml-1" ></a></span>
<?php echo form_error('verification_code');?>
</div>
<div class="col-12 p-1 text-center"><input type="submit" name="post" value="Post" class="btn btn-danger"></div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>