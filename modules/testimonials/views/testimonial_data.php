<?php
if(is_array($res) && !empty($res) ){	
	foreach($res as $val){
		$photo=get_image('testimonial',$val['photo'],'203','203');	
	   //$photo = $val['photo']?$val['photo']:"no_image.png";
$link_url = site_url($val['friendly_url']);			   
	?>
	
	<div class="testi_box mt-4 listpager">
<i class="fas fa-quote-left testi_quote"></i>
<i class="fas fa-quote-left testi_quote2"></i>
<p class="testi_name"><?php echo ucwords($val['poster_name']);?><br><span></span></p>
<p class="mt-1"><?php echo char_limiter($val['testimonial_description'],420); ?></p>
<p class="mt-2 fs13 red text-uppercase"><a href="<?php echo $link_url;?>">Read More</a></p>

<p class="clearfix"></p>
</div>
   <?php
	}

}?>