<?php $this->load->view("top_application");
echo navigation_breadcrumb("Testimonials");  ?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>



<!-- MIDDLE STARTS -->
<?php $this->load->view("testimonials/view_post_testimonials");

?>

<div class="testi_area pt-1">
<div class="container">
<div class="cms">
<h1 class="text-center">Testimonials</h1>

<?php
if(is_array($res) && !empty($res) ){
   echo form_open("",'id="myform" method="post" ');?>
   <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
   <input type="hidden" name="offset" value="0">
   <?php echo form_close();?>
   
   <div id="prodListingContainer">
<?php $data = array('res'=>$res);
     $this->load->view('testimonials/testimonial_data',$data);?>
</div>

<?php if($totalProduct > $record_per_page){?>
<div class="mt-3 text-center" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></div>
<?php }?> 

</div>
<?php }else{  ?>
<div class="mt20 mb20 text-center"><strong>No record found !</strong></div>
   <?php
}?>

</div>
</div>
</div>
<!-- MIDDLE ENDS -->

<div class="clearfix"></div>


<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application"); ?>