<?php $this->load->model(array('testimonials/testimonial_model'));
$param         = array('status'=>'1','orderby'=>'testimonial_id DESC');
$res_array     = $this->testimonial_model->get_testimonial(3,0,$param);

if(is_array($res_array) && !empty($res_array)){	?>
  <div class="testimonial_area">
  <div class="container">
    <p class="heading mb-2">Our Testimonials</p>
   <!-- <p class="courses_txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>-->
    <div id="owl-testimonials" class="owl-carousel owl-theme mt-4">
      <?php
	   $p=1;
	   $para='';
	   foreach($res_array as $val){
		   $link_url=site_url($val['friendly_url']);
		   $act_cls = $p==1 ? 'active' : '';
		   $b=$p-1;   ?>
	  <div class="item">
        <div class="testi_box">
          <div class="testi_img">
            <p><span><img src="<?php echo theme_url();?>images/user-img.jpg" alt="Darshan Rawat"></span></p>
          </div>
          <p class="testi_name"><?php echo $val['poster_name']; ?></p>
          <p class="clearfix"></p>
          <p class="testi_desc"><?php echo char_limiter($val['testimonial_description'],200);?></p>
        </div>
      </div>
      <?php } ?>
      
    </div>
	<?php if(count($res_array)>2) {?>
    <p class="mt-3 text-center"><a href="<?php echo site_url('testimonials');?>" class="viewmore_btn" title="View More Testimonilas">View More Testimonilas</a></p>
	<?php }?>
  </div>
</div>  
<?php  } ?>