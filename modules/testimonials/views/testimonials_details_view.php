<?php $this->load->view("top_application");
//echo navigation_breadcrumb("Testimonials"); 
$link_url = site_url('testimonials');	
 ?>
<?php navigation_breadcrumb($res['poster_name'],array($title=>$link_url));?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>

<!-- MIDDLE STARTS -->
<?php $this->load->view("testimonials/view_post_testimonials");

?>

<div class="testi_area pt-1">
<div class="container">
<div class="cms">
<h1 class="text-center"><?php echo $title;?></h1>

<div class="testi_box mt-4">
<i class="fas fa-quote-left testi_quote"></i>
<i class="fas fa-quote-left testi_quote2"></i>
<p class="testi_name"><?php echo ucwords($res['poster_name']);?><br><span></span></p>
<div class="mt-1"><?php echo $res['testimonial_description']; ?></div>
</div>


</div>
</div>
</div>
<!-- MIDDLE ENDS -->


<div class="clearfix"></div>


<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application"); ?>