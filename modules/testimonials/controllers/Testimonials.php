<?php
class Testimonials extends Public_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->model(array('testimonials/testimonial_model'));
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'testimonial';

	}
	public function index(){

		$record_per_page         = (int) $this->input->post('per_page');
		$page_segment            =  find_paging_segment();

		$config['per_page']	     =  ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');	
		$offset                 = (int) $this->input->post('offset');
		$base_url                =   "testimonials/index/pg/";
		$param = array('status'=>'1');
		$res_array              = $this->testimonial_model->get_testimonial($config['per_page'],$offset,$param);

		$config['total_rows'] = $data['totalProduct']	= get_found_rows();

		//$data['page_links']      = front_pagination("$base_url",$config['total_rows'],$config['per_page'],$page_segment);

		//----------------------------POST TESTIMONAILS---------------------------------

		if($this->input->post('post')=='Post') {
			//trace($this->input->post());exit;
			$this->post();
		}
		
		$data['record_per_page'] = $config['per_page'];
		$data['title'] = 'Testimonials';
		$data['res'] = $res_array;

		$data['frm_url'] = $base_url;

		if($this->input->is_ajax_request())
		{
			$this->load->view('testimonials/testimonial_data',$data);
		}
		else{
			$this->load->view('testimonials/view_testimonials',$data);
		}	

		//$this->load->view('testimonials/view_testimonials',$data);
	}

	public function post(){		

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');		

		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('photo','Photo',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

		$this->form_validation->set_rules('poster_name','Name','trim|required|alpha|max_length[30]');
		//$this->form_validation->set_rules('testimonial_title','Profession','trim|required|alpha|max_length[40]');
		$this->form_validation->set_rules('testimonial_description','Description','trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code');
		$redirect_url = "testimonials/details";
		
		if($this->form_validation->run()==TRUE){	  

		    $uploaded_file = "";

			if( !empty($_FILES) && $_FILES['photo']['name']!='' ){

				$this->load->library('upload');

				$uploaded_data =  $this->upload->my_upload('photo','testimonial');

				if( is_array($uploaded_data)  && !empty($uploaded_data) ){

					$uploaded_file = $uploaded_data['upload_data']['file_name'];

				}
			}
			

		  $posted_data=array(
		  'testimonial_title'      => $this->input->post('testimonial_title',TRUE),
		  'poster_name'             => $this->input->post('poster_name'),
		  'email'                   => $this->input->post('email'),
		  'photo'=>$uploaded_file,
		  'testimonial_description' => $this->input->post('testimonial_description'),
		  'posted_date'            =>$this->config->item('config.date.time')
		  );

		 

		  $testimonialid=  $this->testimonial_model->safe_insert('wl_testimonial',$posted_data,FALSE);

		  $posted_friendly_url = $this->input->post('poster_name').'-'.$testimonialid;

		  $this->cbk_friendly_url = seo_url_title($posted_friendly_url);		  



		  $posted_data_update=array(

		  'friendly_url'      => $this->cbk_friendly_url

		  );	  



		  $posted_data = $this->security->xss_clean($posted_data);

		  $where = "testimonial_id = '".$testimonialid."'";

		  $this->testimonial_model->safe_update('wl_testimonial',$posted_data_update,$where,FALSE);		  



		  if($testimonialid>0) {

				$meta_array  = array(

				'entity_type'=>$redirect_url,

				'entity_id'=>$testimonialid,

				'page_url'=>$this->cbk_friendly_url,

				'meta_title'=>get_text($this->input->post('poster_name'),80),

				'meta_description'=>get_text($this->input->post('testimonial_description')),

				'meta_keyword'=>get_keywords($this->input->post('testimonial_description'))

				);

				create_meta($meta_array);

			}		



			$message = $this->config->item('testimonial_post_success');

			$message = str_replace('<site_name>',$this->config->item('site_name'),$message);

			

			$this->session->set_userdata(array('msg_type'=>'success'));			

			$this->session->set_flashdata('success',$message);			

			redirect('testimonials', '');



	  }



		//$this->load->view('testimonials/view_post_testimonials');



	}







	public function details(){

		$id = (int) $this->uri->rsegment(3);
		$param     = array('status'=>'1','where'=>"testimonial_id ='$id' ");
		$res       = $this->testimonial_model->get_testimonial(1,0,$param);

		if(is_array($res) && !empty($res)){

			$data['title'] = 'Testimonials';
			$data['res'] = $res[0];
			$this->load->view('testimonials/testimonials_details_view',$data);
		}else{
			redirect('testimonials', '');
		}
	}











}







/* End of file pages.php */



?>