<?php
class Portfolio extends Public_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('portfolio/portfolio_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'portfolio';
	}

	public function index(){

		 $this->page_section_ct = 'portfolio'; 
		// $this->config->set_item('per_page','2');
		$record_per_page        = (int) $this->input->post('per_page');
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$base_url      =  "portfolio";
		$param = array('status'=>'1');
		$res_array              = $this->portfolio_model->get_portfolio($config['per_page'],$offset,$param);
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct'] = $total_rows;
		$data['record_per_page'] = $config['per_page'];
		$data['title'] = 'Blogs';
		$data['res'] = $res_array;
		$data['base_url'] = $base_url;
		$data['frm_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE){
		  $this->load->view('portfolio/load_portfolio_view',$data);
		}
		else{
		  $this->load->view('portfolio/view_portfolio',$data);
		}
	}		

	public function details(){

		$this->page_section_ct = 'portfolio';
		//$id = (int) $this->uri->rsegment(3);
		$id= (int) $this->meta_info['entity_id'];
		$param     = array('wl_portfolio.status'=>'1','where'=>"wl_portfolio.portfolio_id ='$id' ");
		$res       = $this->portfolio_model->get_portfolio(1,0,$param);
		if(is_array($res) && !empty($res)){

			$data['title'] = 'Blogs';
		    $data['res'] = $res;
		    $this->load->view('portfolio/portfolio_details_view',$data);	
		}else{
			redirect('portfolio', ''); 

		}

	}

	public function download_pdf(){
		
	  $id = (int) $this->uri->segment(3);
	  $param     = array('status'=>'1','where'=>"portfolio_id ='$id' ");
	  $res_topic       = $this->portfolio_model->get_portfolio(1,0,$param);	

	  if(is_array($res_topic) && !empty($res_topic)){

		if($res_topic['portfolio_pdf']!='' && file_exists(UPLOAD_DIR."/portfolio/pdf/".$res_topic['portfolio_pdf']))
		{
			$this->load->helper('download');
			$data = file_get_contents(UPLOAD_DIR."/portfolio/pdf/".$res_topic['portfolio_pdf']);
			$name = $res_topic['portfolio_pdf'];
			force_download($name, $data); 
		}
	  }
	}


}

/* End of file pages.php */



