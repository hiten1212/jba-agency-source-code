<?php $this->load->view("top_application");

if($res['alt_tag']){

	$alt_tag=ucfirst($res['alt_tag']);

}else{

	$alt_tag=ucfirst($res['portfolio_title']);

}

navigation_breadcrumb($res['portfolio_title'],array('Our Portfolio'=>'portfolio'));

?>

<div class="container">

	<div class="mid_area">

        <div class="portfolio_dtl_area">

            <div class="portfolio_dtl_thm rel"><figure><img src="<?php echo get_image('portfolio',$res['portfolio_image'],'600','600','R') ?>" alt="<?php echo $res['portfolio_title'];?>" title="<?php echo $res['portfolio_title'];?>"></figure></div>

        </div>

        <div class="portfolio_dtl_contents">

    		<h1><?php echo ucfirst($res['portfolio_title']);?></h1>

            <div class="fs16 lh20px"><?php echo $res['portfolio_description'];?></div>

        </div>

        <div class="clearfix"></div>

	</div>

</div>

<?php $this->load->view("bottom_application");?>