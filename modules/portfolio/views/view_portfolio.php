<?php $this->load->view("top_application");?>

<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   } </script>

<?php navigation_breadcrumb('Our Portfolio');?>

<div class="container">

    <div class="mid_area">

        <div>

        	<h1>Our Portfolio</h1>

            <?php echo form_open("",'id="myform" method="post" ');?>

            <input type="hidden" name="per_page" value="<?php echo $this->config->item('per_page');?>">

            <input type="hidden" name="offset" id="pg_offset" value="0">

            <?php echo form_close();?>

              <div class="pro_list">

              <ul  id="prodListingContainer">

              <?php

                if(is_array($res) && !empty($res)){ $data['res'] = $res;

                ?>

                <?php $this->load->view('portfolio/load_portfolio_view',$data);?> 

                </ul>

                </div>

                <?php

                }else{

                 echo '<div class="mt-3 text-center"><strong>No record found !</strong></div>';

                }

                ?>            

            <p class="clearfix"></p>            

        </div>

    </div>

</div>

<?php $this->load->view("ajax_paging");

$this->load->view("bottom_application");?>