<?php
if(is_array($res) && !empty($res) ){ 
  foreach($res as $val){  
	$portfolio_links = site_url($val['friendly_url']);
	if($val['alt_tag']){
		$alt_tag=ucfirst($val['alt_tag']);
	}else{
		$alt_tag=ucfirst($val['portfolio_title']);
	}	
  ?>
  <li class="listpager">
    <div class="portfolio_area">
        <div class="portfolio_thm rel"><figure><a href="<?php echo $portfolio_links;?>" title="<?php echo $val['portfolio_title']; ?>"><img src="<?php echo get_image('portfolio',$val['portfolio_image'],'292','292','R') ?>" alt="" class="img-responsive"/></a></figure></div>
        <p class="portfolio_title"><a href="<?php echo $portfolio_links;?>" title="<?php echo $val['portfolio_title']; ?>"><?php echo char_limiter($val["portfolio_title"],250,'...');?></a></p>
        <p class="text-center mt-2"><a href="<?php echo $portfolio_links;?>" class="btn btn-danger btn-danger2 btn-danger3">View Details</a></p>
    </div>
 </li> 
  <?php
 }
}
?>