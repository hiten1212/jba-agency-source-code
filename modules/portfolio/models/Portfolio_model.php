<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Portfolio_model extends MY_Model{ 

	 public function get_portfolio($limit='10',$offset='0',$param=array()){

		$status			    =   @$param['status'];
		$orderby		    =   @$param['orderby'];	
		$latest_portfolio		    =   @$param['latest_portfolio'];
		$where		        =   @$param['where'];

		$keyword = $this->db->escape_str($this->input->get_post('keyword',TRUE));

	    if($status!=''){
			$this->db->where("wl_portfolio.status",$status);
		}

		if($latest_portfolio!=''){
			$this->db->where("wl_portfolio.latest_portfolio",$latest_portfolio);
		}

	    if($where!=''){
			$this->db->where($where);
		}

		if($keyword!=''){
			$this->db->where("(wl_portfolio.portfolio_title LIKE '%".$keyword."%' OR wl_portfolio.publisher LIKE '%".$keyword."%' )");
		}

		if($orderby!=''){
			 $this->db->order_by($orderby);
		}else{
		  $this->db->order_by('wl_portfolio.portfolio_id','desc');		
		}

		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wl_portfolio.*',FALSE);
		$this->db->from('wl_portfolio');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;	
	}

}


?>