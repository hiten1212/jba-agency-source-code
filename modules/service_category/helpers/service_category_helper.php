<?php
if ( ! function_exists('has_child'))
{
	function has_child($catid,$condtion="AND status='1'")
	{
		$ci = CI();
		$sql="SELECT category_id FROM wl_service_categories WHERE parent_id=$catid $condtion ";
		$query = $ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		return $num_rows >= 1 ? TRUE : FALSE;
	}
}

if ( ! function_exists('get_child_categories'))
{
	function get_child_categories($parent='0',$condtion="AND status='1' ", $fields='SQL_CALC_FOUND_ROWS*')
	{
		$parent = (int) $parent;
		$ci     = CI();
		$output        = array();
		$sql           = "SELECT  $fields FROM wl_service_categories WHERE parent_id=$parent $condtion  ";
		$query         = $ci->db->query($sql);
		$num_rows      =  $query->num_rows();
		
		if ( $num_rows > 0)
		{
			foreach( $query->result_array() as $row )
			{
				$output[$row['category_id']]['parent'] = $row;
				$output[$row['category_id']]['child'] = array();
				
				if ( has_child($row['category_id'] ))
				{
					$output[$row['category_id']]['child'] = get_child_categories($row['category_id'], $condtion, $fields);
				}
			}
		}
		return $output;
	}
}

if ( ! function_exists('get_parent_categories'))
{
	function get_parent_categories($category_id,$condtion="AND status!='2'", $fields='*')
	{
		$category_id   = (int) $category_id;
		$ci            = CI();
		$output        = array();
		$sql           = "SELECT $fields FROM wl_service_categories WHERE category_id=$category_id $condtion  ";
		$query         = $ci->db->query($sql);
		$num_rows      =  $query->num_rows();
		
		if ( $num_rows > 0)
		{
			foreach( $query->result_array() as $row )
			{
				$parent_id =  $row['parent_id'];
				$output[$row['category_id']] = $row;
			
				while( $parent_id>0 )
				{
					$sql           = "SELECT $fields FROM wl_service_categories WHERE category_id=$parent_id $condtion  ";
					$query         = $ci->db->query($sql);
					$num_rows      =  $query->num_rows();
					
					if ( $num_rows > 0)
					{
						foreach( $query->result_array() as $row )
						{
							$parent_id = $row['parent_id'];
							$output[$row['category_id']] = $row;
						}
					}else
					{
						$parent_id = 0;
					}
				}
			}
		}
		return $output;
	}

}

if ( ! function_exists('get_nested_dropdown_menu'))
{
	function get_nested_dropdown_menu($parent,$selectId="",$pad="|__",$category_chain="",$allowed_catids='')
	{
		$ci = CI();
		$selId =( !is_array( $selectId ) ) ? array() : $selectId;
		$var="";
		if($parent>0)
		{
			$category_chain.="~".$parent;
		}
		
		$sql="SELECT * FROM wl_service_categories WHERE parent_id=$parent AND status!='2' ";
		if($ci->show_active_rec_only === TRUE)
		{
			$sql.=" AND status='1'";
			$has_child_cond = " AND status='1'";
		}else
		{
			$has_child_cond = " AND status!='2'";
		}
		if($allowed_catids!='')
		{
			$sql .=" and category_id IN($allowed_catids)";
		}
		
		$query=$ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		
		if ($num_rows > 0  )
		{
			foreach( $query->result_array() as $row )
			{
				$category_name=ucfirst(strtolower($row['category_name']));
				if ( has_child($row['category_id'],$has_child_cond) )
				{
					$var .= '<optgroup label="'.$pad.'&nbsp;'.$category_name.($row['status']==0 ? ' (Inactive)':'').'" >'.$category_name;
					$var .= get_nested_dropdown_menu($row['category_id'],$selId,'&nbsp;&nbsp;&nbsp;'.$pad,$category_chain);
					$var .= '</optgroup>';
				}else
				{
					$category_chain1 = $category_chain."~".$row['category_id'];
					$category_chain1 = ltrim($category_chain1,"~");
					$sel=( in_array($row['category_id'],$selId) ) ? "selected='selected'" : "";
					$var .= '<option value="'.$row['category_id'].'" '.$sel.' >'.$pad.$category_name.($row['status']==0 ? ' (Inactive)':'').'  </option>';
				}
			}
		}
		return $var;
	}
}

if ( ! function_exists('get_root_level_services_categories_dropdown'))
{
	function get_root_level_services_categories_dropdown($parent,$selectId="")
	{
		$ci = CI();
		$selId =( $selectId!="" ) ? $selectId : "";
		$var="";
		echo $sql="SELECT * FROM wl_service_categories WHERE parent_id=$parent AND status='1' ORDER BY sort_order ASC ";
		$query=$ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		
		if ($num_rows > 0  )
		{
			foreach( $query->result_array() as $row )
			{
				$category_name=ucwords(strtolower($row['category_name']));
				$sel=( $selectId==$row['category_id'] ) ? "selected='selected'" : "";
				$var .= '<option value="'.$row['category_id'].'" '.$sel.'>'.$category_name.'  </option>';
			}
		}
		return $var;
	}
}

if ( ! function_exists('count_category'))
{
	function count_category($condtion='')
	{
		$ci = CI();
		$condtion = "status !='2' ".$condtion;
		$sql="SELECT COUNT(category_id)  AS total_subcategories FROM wl_service_categories WHERE $condtion ";
		$query=$ci->db->query($sql)->row_array();
		return  $query['total_subcategories'];
	}
}

if ( ! function_exists('count_services'))
{
	function count_services($condtion='')
	{
		$ci = CI();
		$condtion = "status !='2' ".$condtion;
		$sql="SELECT COUNT(service_id)  AS total_services FROM wl_services WHERE $condtion ";
		$query=$ci->db->query($sql)->row_array();
		return  $query['total_services'];
	}
}

if ( ! function_exists('category_breadcrumb'))
{
	function category_breadcrumb($catid,$segment='')
	{
		$link_cat=array();
		$ci = CI();
		$sql="SELECT category_name,category_id,parent_id,friendly_url
		FROM wl_service_categories WHERE category_id='$catid' AND status='1' ";
		$query=$ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		$segment      = $ci->uri->rsegment($segment,0);

		if ($num_rows > 0)
		{
			foreach( $query->result_array() as $row )
			{
				if ( has_child( $row['parent_id'] ) )
				{
					//$condtion_service   =  "AND category_id='".$row['category_id']."'";
					//$service_count      = count_services($condtion_service);
					$link_url = site_url($row['friendly_url']);

					if( $segment!='' && ( $row['category_id']==$segment ) )
					{
						//$link_cat[]='<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="dib"><span itemprop="title">'.$row['category_name'].'</span></div>';
						$link_cat[]='<li class="breadcrumb-item active">'.$row['category_name'].'</li>';
					}else
					{
						//$link_cat[]='<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="dib"><span itemprop="url"><a href="'.$link_url.'">'.$row['category_name'].'</a></span></div> > ';
						$link_cat[]='<li class="breadcrumb-item"><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';
					}
					$link_cat[] = category_breadcrumb($row['parent_id'],$segment);
				}else
				{
					//$link_url = base_url()."".$row['friendly_url'];
					//$link_cat[]='<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="dib"><span itemprop="url"><a href="'.$link_url.'">'.$row['category_name'].'</a></span></div> > ';
					$link_cat[]='<li class="breadcrumb-item"><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';
				}
			}
		}else
		{
			//$link_url = base_url()."service_category";
			//$link_cat[]='<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="dib"><span itemprop="url"><a href="'.$link_url.'">Category</a></span></div> > ';
			//$link_cat[]='<li class="breadcrumb-item"><a href="'.$link_url.'">Categories</a></li>';
		}

		$link_cat = array_reverse($link_cat);
		$category_links=implode($link_cat);
		return $category_links;

	}

}

if ( ! function_exists('category_breadcrumbs'))
{
	function category_breadcrumbs($catid,$segment='',$heading='')
	{
		$category_links=category_breadcrumb($catid,$segment);
		$navigation_content='<div class="clearfix"></div>
		<div class="breadcrumb_outer">
<div class="container">
<ol class="breadcrumb">

		<li class="breadcrumb-item"><a href="'.base_url().'">Home</a></li>'.$category_links;
		if($heading!=''){
			$navigation_content.='<li class="breadcrumb-item active">'.$heading.'</li>';
		}
		$navigation_content.='
		</ol>
		</div>
		</div>';

		return $navigation_content;
	}
}

if ( ! function_exists('get_category_name'))
{
	function get_category_name($catid)
	{
		$ci = CI();
		$sql="SELECT category_name FROM wl_service_categories WHERE category_id='$catid' ";
		$query=$ci->db->query($sql);

		$num_rows     =  $query->num_rows();
		if($num_rows>0)
		{
			return $query->row_array();
		}
	}
}

if (!function_exists('get_category_chain'))
{
	function get_category_chain($catid)
	{
		$CI = & get_instance();
		$array = '';
		$var = '';
		$sql1 = $CI->db->query("select category_id,parent_id,category_name from wl_service_categories where category_id='$catid' and status='1'");
		$res = $sql1->row_array();
		$flag = 0;
		$catparent = $catid;
		while ($flag != 1)
		{
			$sql2 = $CI->db->query("select category_id,parent_id,category_name from wl_service_categories where category_id='$catparent' and status='1'");
			$record = $sql2->row_array();
			
			if ($record['parent_id'] != 0)
			{
				$catparent = $record['parent_id'];
				$array.=$record['category_name'] . "~";
			}else
			{
				if ($record['category_id'] != "")
				{
					$array.=$record['category_name'] . "~";
				}
				$flag = 1;
			}
		}
		$arr1 = explode("~", $array);
		$arr = array_reverse($arr1);
		for ($i = 0; $i < count($arr); $i++)
		{
			if ($arr[$i] != '')
			{
				$var .=$arr[$i] . "&raquo;";
			}
		}
		
		return $var;
	}
}