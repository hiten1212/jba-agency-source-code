<?php 
 $condtion_array = array(
   'field' =>"*,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 7;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
<div class="prod_area">
<div class="container position-relative">
<div id="owl-featured" class="owl-carousel owl-theme">

<?php
	foreach($res as $val){
	 $link_url = site_url($val['friendly_url']);
	 $cateImage=get_image('category',$val["category_image"],'285','175');
	 ?>
<div class="item">
<div class="pro_box">
<p class="pro_pic"><img src="<?php echo theme_url();?>images/crnr2.png" alt="<?php echo $val['category_alt'];?>" class="pro_crnr"><span><a href="<?php echo $link_url;?>"><img src="<?php echo $cateImage;?>" alt="<?php echo $val['category_alt'];?>"></a></span></p>
<div class="pro_cont">
<img src="<?php echo theme_url();?>images/crnr.png" alt="Corner" class="pro_crnr">
<p class="pro_title black"><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></p>
<p class="pro_desc"><?php echo char_limiter($val['category_description'],100);?></p>
<p class="text-right mt-1 weight700 maroon roboto"><a href="<?php echo $link_url;?>" title="Read More">Read More</a></p>
</div>
<img src="<?php echo theme_url();?>images/pro-hov-img.png" alt="Corner" class="pro_hov_img">
</div>
</div>
<?php }?>
</div>
<?php if($total_categories<=4){?>
<div class="owl-dots"><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot active"><span></span></button></div>
<?php }?>
<p class="prod_cate_link roboto"><a href="<?php echo site_url('service_category');?>" title="Product Categories">Product Categories</a></p>

</div>
</div>
<?php }?>