<?php $this->load->view("top_application"); ?>
<?php //$this->load->view('banner/inner_page_top_banner');?>
<?php 
$cat_name=$heading_title;
if($cat_id){
	$cat_name=ucwords($parentres['category_name']);
}?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>
<?php $segment=3; if($cat_id){ echo category_breadcrumbs($cat_id,$segment); }else{ echo navigation_breadcrumb($heading_title); }?>

<div class="container">
<div class="cms">

<h1>Categories</h1>

<?php
if(is_array($res) && !empty($res) ){
	echo form_open("",'id="myform" method="post" ');?>
  <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
     <input type="hidden" name="offset" value="0">
     <?php echo form_close();?>
	 
<div class="row" id="prodListingContainer">
<?php $data = array('res'=>$res);
      $this->load->view('service_category/category_data',$data);?>
      
</div>
<?php 

if($totalProduct > $record_per_page){?>
<div class="text-center mt-3" id="loadingdiv"><img src="<?php echo theme_url() ?>images/loader.gif" width="100" alt=""></div>
<?php }?>
<div class="clearfix"></div>
<?php
	}else{
	   ?>
	   <div class="clearfix"></div>
	   <p class="mt-3 text-center"><strong><?php echo $this->config->item('no_record_found');?></strong></p>
	   <?php
	}?>


</div>
</div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>