<?php
if(is_array($res) && !empty($res) ){
	$counter=0;
	foreach($res as $val)
	{
		$link_url = base_url().$val['friendly_url'];
		$cateImage=get_image('category',$val["category_image"],'295','195');
		?>
<div class="col-6 col-lg-4 col-xl-3 p-1 listpager">
<div class="proj_box">
<p class="proj_pic"><span><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><img src="<?php echo $cateImage;?>" alt="<?php echo $val['category_alt'];?>"></a></span></p>
<div class="pro_cont">
<p class="proj_title black"><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></p>
</div>
</div>
</div>

<?php
	}
}?>