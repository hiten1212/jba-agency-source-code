<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $this->config->item('site_name');?></title>
<link rel="shortcut icon" href="<?php echo base_url() ?>favicon.ico">
<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.10.2/css/all.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="//fonts.googleapis.com/css?family=Montserrat:wght@300,400,500,600,700,800,900&display=swap" rel="stylesheet">
<strong></strong>
<script type="text/javascript">var _siteRoot='index.html',_root='index.html';</script>
<script type="text/javascript" > var site_url = '<?php echo site_url();?>';</script>
<script type="text/javascript" > var theme_url = '<?php echo theme_url();?>';</script>
<script type="text/javascript" > var resource_url = '<?php echo resource_url(); ?>';</script>

<link rel="stylesheet" href="<?php echo theme_url();?>css/conditional.css">
</head>
<body style="padding:10px;">
<div class="vid-det">
<?php
 $youtubeid=get_Youtube_Id($res['embed_code']); 
 //trace($youtubeid);
 $embed_code = $youtubeid;
  
 $embed_link = "https://www.youtube.com/embed/";
   if (strstr($embed_code, "https://youtu.be/")) {
	   $embed = str_replace("https://youtu.be/","",$embed_code);
   }else{
	   $embed = str_replace("https://www.youtube.com/watch?v=","",$embed_code);
   }
   $embed_link.=$embed; 
 ?>
 <iframe width="100%" height="315" src="<?php echo $embed_link;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display:block;line-height:0;"></iframe>
</div>
  
</div>
</body>
</html>