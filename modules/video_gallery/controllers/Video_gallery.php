<?php
class Video_gallery extends Public_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('video_gallery/video_gallery_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'video_gallery';
	}

	public function index(){
		
		$this->page_section_ct = 'video_gallery';	 
		//$record_per_page   = (int) $this->input->post('per_page');
		$record_per_page    = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');		
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');	
		$base_url      =  "video_gallery";
		
								
		$param = array('status'=>'1','type'=>'2');		
		$res_array              = $this->video_gallery_model->get_video_gallery($config['per_page'],$offset,$param);		
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct']= $total_rows;
		
		$data['record_per_page'] = $record_per_page;
		$data['title'] = 'Video Gallery';
		$data['res'] = $res_array; 		
		$data['base_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE)
		{
		  $this->load->view('video_gallery/load_gallery_view',$data);
		}
		else
		{
		  $this->load->view('video_gallery/view_gallery',$data);
		}	   
			
	}
	
	public function picl(){
		
		$this->page_section_ct = 'video_gallery';	 
		$record_per_page        = (int) $this->input->post('per_page');		
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');	
		$base_url      =  "video_gallery/picl";
						
		$param = array('status'=>'1','type'=>'2','video_type'=>'PICL');
		$res_array              = $this->video_gallery_model->get_video_gallery($config['per_page'],$offset,$param);		
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $total_rows;
		
		$data['title'] = 'PICL Video Gallery';
		$data['res'] = $res_array; 		
		$data['base_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE)
		{
		  $this->load->view('video_gallery/load_gallery_view',$data);
		}
		else
		{
		  $this->load->view('video_gallery/view_gallery',$data);
		}	   
			
	}		
	
	public function playvideo(){
				
		$Id  = (int) $this->uri->segment(3);
		$query=$this->db->query("SELECT * FROM wl_gallery WHERE id='".$Id."'");
		$row_founds = $query->num_rows();
		if($row_founds>0) 
		{
			$data['res']= $query->row_array();
		}
		
		$this->load->view('video_gallery/play_video',$data);
			
    }
	
	public function detail()
	{	
		$this->page_section_ct = 'video_gallery';
	
		 $id = (int) $this->uri->rsegment(3);	
		//$id= (int) $this->meta_info['entity_id'];	
		$param     = array('status'=>'1','where'=>"id ='$id' ");	
		$res       = $this->video_gallery_model->get_video_gallery(1,0,$param);	
		
		if(is_array($res) && !empty($res))
		{			
			$data['title'] = 'Video Gallery ';
		    $data['res'] = $res; 			
		    $this->load->view('video_gallery/gallery_details_view',$data);
		
			
		}else
		{
			redirect('video_gallery', ''); 
			
		}
		
	}	
	
}


/* End of file pages.php */

?>
