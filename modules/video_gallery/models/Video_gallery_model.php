<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Video_gallery_model extends MY_Model
 {

		 
	 public function get_video_gallery($limit='10',$offset='0',$param=array()){		
		
		$status			    =   @$param['status'];
		$orderby		    =   @$param['orderby'];
		$video_type		=   @$param['video_type'];
		$country_id			    =   @$param['country_id'];
		$where		        =   @$param['where'];
		$id			=   @$param['id'];
		$type			    =   @$param['type'];	
		
		$keyword = $this->db->escape_str($this->input->get_post('keyword',TRUE));
		
		if($country_id!='')
		{
			$this->db->where("country_id","$country_id");
		}
		
		if($video_type!='')
		{
			$this->db->where("video_type","$video_type");
		}
		if($type!='')
		{
			$this->db->where("type","$type");
		}
			
	    if($status!='')
		{
			$this->db->where("status","$status");
		}
		
		if($id!='')
		{
			$this->db->where("id","$id");
		}
				
	    if($where!='')
		{
			$this->db->where($where);
		}
		
		if($keyword!='')
		{
						
			$this->db->where("(title LIKE '%".$keyword."%' )");
		}
		if($orderby!='')
		{
			 $this->db->order_by($orderby);
		}
		else
		{
		  	$this->db->order_by('id','desc');
		}
		
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS*',FALSE);
		$this->db->from('wl_gallery');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? $result[0]: $result;	
		return $result;
				
	}	
	 
}
?>