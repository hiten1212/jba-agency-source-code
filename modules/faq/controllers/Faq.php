<?php
class Faq extends Public_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('faq/faq_model'));
	}

	public function index()
	{		
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');
		$parent_segment         = (int) $this->uri->segment(3);
		$page_segment           = find_paging_segment();
		$offset                 = (int) $this->input->post('offset');
		//$offset                 =  (int) $this->uri->segment($page_segment,0);
		$parent_id              = ( $parent_segment > 0 ) ?  $parent_segment : '0';
		$base_url               = ( $parent_segment > 0 ) ?  "faq/index/$parent_id/pg/" : "faq/index/pg/";

		$data['record_per_page'] = $record_per_page;
		$param = array('status'=>'1');
		$res_array               = $this->faq_model->get_faq($record_per_page,$offset,$param);
		$config['total_rows'] = $data['totalProduct']	= get_found_rows();

		$data['frm_url'] = $base_url;
		//$data['page_links']      = front_pagination("$base_url",$config['total_rows'],$config['per_page'],$page_segment);

		$data['page_heading'] = "FAQ's";
		$data['res'] = $res_array;
		//$this->load->view('view_faq',$data);
		if($this->input->is_ajax_request())		
		{
			$this->load->view('faq/faq_data',$data);
		}
		else
		{
			$this->load->view('faq/view_faq',$data);
		}
	}
}?>