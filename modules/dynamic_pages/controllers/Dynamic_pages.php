<?php
class Dynamic_pages extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('dynamic_pages/dynamic_pages'));
		$this->load->model(array('dynamic_pages/dynamic_pages_model'));

	}


	public function index()
	{
		$data['title'] = "Main Page";
		//$record_per_page        = (int) $this->input->post('per_page');
		
		$cond_array = array(
		'field' =>"*",
		'condition'=>"AND parent_id = '0' AND status='1' ",
		'limit'=>'1',
		'offset'=>0,
		'order'=>'sort_order',
		'debug'=>FALSE
		);

		$res              =  $this->dynamic_pages_model->getpages($cond_array);
		$res = $res[0];
		//trace($res);

		$condition = "";
		$page_title = 'Page Creator';
		
		$condtion_array = array(
		'field' =>"*,( SELECT COUNT(page_id) FROM wl_dynamic_pages AS b
		WHERE b.parent_id=a.page_id ) AS total_subcategories",
		'condition'=>"AND parent_id = '0' AND status='1' ",
		'limit'=>'',
		'offset'=>$offset	,
		'order'=>'sort_order',
		'debug'=>FALSE
		);

		$res_array              =  $this->dynamic_pages_model->getpages($condtion_array);
		
		$data['heading_title'] = (count($res)>0)?$res['page_name']:$page_title;
		$data['cat_res_array'] = $res_array;
		$data['cat_id']=$res['page_id'];
		$data['res'] = $res;
		
		

        $this->load->view('dynamic_pages/view_page_detail',$data);

	}
	
	public function listing()
	{
		$data['heading_title'] = "Application";
		$this->load->view('dynamic_pages/view_page_list',$data);
	}

	public function detail()
	{
		$data['title'] = "Main Page";
		
		$page_id = (int) $this->meta_info['entity_id'];

		
		$res = get_db_single_row('wl_dynamic_pages','*'," AND page_id='$page_id'");
		

		$condtion_array = array(
		'field' =>"*,( SELECT COUNT(page_id) FROM wl_dynamic_pages AS b
		WHERE b.parent_id=a.page_id ) AS total_subcategories",
		'condition'=>"AND parent_id = '0' AND status='1' ",
		'limit'=>'',
		'order'=>'sort_order',
		'offset'=>0,
		'debug'=>FALSE
		);

		$res_array              =  $this->dynamic_pages_model->getpages($condtion_array);

		$data['heading_title'] = $res['page_name'];
		$data['cat_res_array'] = $res_array;
		$data['res'] = $res;
		$data['cat_id']=$page_id;
			
		$this->load->view('dynamic_pages/view_page_detail',$data);
			
		

	}


}

