<?php
$this->load->helper(array('dynamic_pages/dynamic_pages'));
$this->load->model(array('dynamic_pages/dynamic_pages_model'));
$cond_array = array(
'field' =>"*",
'condition'=>"AND parent_id = '0' AND status='1' ",
'limit'=>'6',
'offset'=>0,
'order'=>'sort_order',
'debug'=>FALSE
);

$res_array =  $this->dynamic_pages_model->getpages($cond_array); 
if(is_array($res_array) && !empty($res_array)){?>
<div class="appl_area">
<div class="container">
<p class="wel_title">What We Can Do For You</p>
<div class="float-md-left">
<p class="heading mt-4">Featured Applications</p>
<p class="mt-1"><img src="<?php echo theme_url();?>images/heading-border2.png" alt=""></p>
</div>
<p class="float-md-right mt-1 mb-4 mb-md-0 mt-md-4"><a href="<?php echo site_url('dynamic_pages/listing');?>" class="readmore" title="View More">View More</a></p>
<p class="clearfix"></p>
<div id="owl-applications" class="owl-carousel owl-theme mt-2">

<?php foreach($res_array as $key=>$val){
$link_url = site_url($val['friendly_url']);
$page_name = strlen($val['page_name']) > 35 ? char_limiter($val['page_name'],35,'..') : $val['page_name'];
?>
<div class="item">
<div class="appl_box">
<img src="<?php echo theme_url();?>images/crnr-appl2.png" alt="Corner" class="pro_crnr d-none d-md-block">
<div class="appl_pic">
<img src="<?php echo theme_url();?>images/crnr-appl.png" alt="Corner" class="pro_crnr d-none d-md-block">
<span><a href="<?php echo $link_url;?>" title="<?php echo $val['page_name'];?>"><img src="<?php echo get_image('dynamic_page_image',$val['page_image'],390,240,'R');?>" alt="<?php echo $page_name;?>"></a></span></div>
<div class="appl_cont">
<p class="appl_name white"><a href="<?php echo $link_url;?>" title="<?php echo $val['page_name'];?>"><?php echo $page_name;?></a></p>
<p class="appl_arr white"><a href="<?php echo $link_url;?>" title="<?php echo $val['page_name'];?>"><i class="fas fa-arrow-right"></i></a></p>
</div>
<img src="<?php echo theme_url();?>images/heading-border.png" alt="" class="pro_hov_img">
</div>
</div>
<?php }?>
</div>
</div>
</div>
<?php  }?>