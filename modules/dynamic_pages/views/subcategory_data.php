<?php
if(is_array($res) && !empty($res) ){
	foreach($res as $val){
		$link_url = base_url().$val['friendly_url'];
		$cateImage=get_image('category',$val["category_image"],'255','255');
		?>
        <li class="listpager">
            <div class="cate_w">
              <div class="cate_img">
                <figure><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><img src="<?php echo $cateImage;?>" alt="<?php echo $val['category_alt'];?>"></a></figure>
              </div>
              <div class="cate_ttl"><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></div>
            </div>
          </li>

		<?php
	}
}?>