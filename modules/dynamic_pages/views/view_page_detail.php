<?php $this->load->view("top_application"); ?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php 
if($res['parent_id']>0)
{
  $get_details = $this->db->query("SELECT friendly_url,page_name FROM wl_dynamic_pages 
  										  WHERE page_id='".$res['parent_id']."'")->row_array(); 
										  
	//echo navigation_breadcrumb($heading_title,array('Application'=>'dynamic_pages/listing',$get_details['page_name']=>$get_details['friendly_url']));	
echo navigation_breadcrumb($heading_title,array($get_details['page_name']=>$get_details['friendly_url']));	 	
}
else
{
	//echo navigation_breadcrumb($heading_title,array('Application'=>'dynamic_pages/listing'));
	echo navigation_breadcrumb($heading_title,array());
}
$this->load->model(array('dynamic_pages/dynamic_pages_model'));
 ?>
<!-- MIDDLE STARTS -->
<div class="container">
<div class="mid_area">
<div class="cms_area">
      <div class="row">
        <div class="col-lg-3 sidebar no_pad"> 
          <div id="MainMenu">
            <p class="sidebar-hed">Page Creater</p>
            <div class="list-group panel">
<?php 
$page_id = ($res['parent_id']>0) ? $res['parent_id'] : $res['page_id'];
$condtion_array = array(
'field' =>"*,( SELECT COUNT(page_id) FROM wl_dynamic_pages AS b
WHERE b.parent_id=a.page_id ) AS total_subcategories",
'condition'=>"AND parent_id = '".$page_id."' AND status='1' ",
'limit'=>'15',
'order'=>'sort_order',
'offset'=>0,
'debug'=>FALSE
);

$res_array  =  $this->dynamic_pages_model->getpages($condtion_array);
		if(is_array($res_array) && !empty($res_array)){ 
?>			
           <?php
	 foreach($res_array as $val){
		$link_url = site_url($val['friendly_url']);
		$page_name = strlen($val['page_name']) > 25 ? char_limiter($val['page_name'],25,'..') : $val['page_name'];												
	 ?>
	<a href="<?php echo $link_url; ?>" class="list-group-item " data-parent="#MainMenu" title="<?php echo $page_name;?>"><?php echo $page_name;?></a>
<?php } ?>
		<?php } ?>
              </div>
          </div>
        </div>
        <div class="col-lg-9">
          <h1 class="mb20"><?php echo $heading_title;?></h1>
          <div><?php echo $res['page_description'];?></div>
        </div>
      </div>
    </div>
</div>
</div>
<!-- MIDDLE ENDS -->

<?php $this->load->view("bottom_application");?>