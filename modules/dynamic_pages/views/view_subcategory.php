<?php $this->load->view("top_application");
$segment=3; if($cat_id){ echo category_breadcrumbs($cat_id,$segment); }else{ echo navigation_breadcrumb("Categories"); }
$cat_name="Category";
if($cat_id){
	$cat_name=ucwords($parentres['category_name']);
}?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>
<div class="container">
  <div class="mid_area">
    <div class="cms_area">
      <h1><?php echo $cat_name;?></h1>
      <div class="pro_cat">
      <?php
 if(is_array($res) && !empty($res) ){
	 echo form_open("",'id="myform" method="post" ');?>
	 <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
	 <input type="hidden" name="offset" value="0">
	 <?php echo form_close();?>
     <div id="my_data">
        <ul id="prodListingContainer">
           <?php $data = array('res'=>$res);
            $this->load->view('category/subcategory_data',$data);?>    
        </ul>
         <div class="clearfix"></div>
	  <?php if($totalProduct > $record_per_page){?>
	  <div class="mt-4 mb-4 ac" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></div>
	  <?php }?>
	 </div>
      <?php
 }else{
	 ?>
	 <div class="mt-5 mb-5 ac b"><strong>No record found !</strong></div>
	 <?php
 }?>
      </div>
      
      <div class="p10 border1 mb10 mt5 hidden-xs hidden-sm">
        <div class="thm1 float-left mr15">
          <figure><img src="<?php echo get_image('category',$parentres["category_image"],'60','60');?>" alt="<?php echo $parentres['category_alt'];?>"> </figure>
        </div>
        <p class="i fs14 gray"> <?php echo $parentres['category_description'];?> </p>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>