<?php $this->load->view("top_application"); ?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb($heading_title);
$this->load->model(array('dynamic_pages/dynamic_pages_model'));
 ?>
<!-- MIDDLE STARTS -->
<div class="container">
<div class="inner_box">
<div class="cms">
<h1>Applications</h1>
<?php 
$condtion_array = array(
'field' =>"*,( SELECT COUNT(page_id) FROM wl_dynamic_pages AS b
WHERE b.parent_id=a.page_id ) AS total_subcategories",
'condition'=>"AND parent_id = '0' AND status='1' ",
'limit'=>'15',
'order'=>'sort_order',
'offset'=>0,
'debug'=>FALSE
);

$res_array  =  $this->dynamic_pages_model->getpages($condtion_array);
		if(is_array($res_array) && !empty($res_array)){ 
?>
<div class="appl_links">
<div class="row">
<?php
	 foreach($res_array as $val){
		$link_url = site_url($val['friendly_url']);
		$page_name = strlen($val['page_name']) > 40 ? char_limiter($val['page_name'],40,'..') : $val['page_name'];												
	 ?>
	<div class="col-lg-4 no_pad"><a href="<?php echo $link_url; ?>" title="<?php echo $page_name;?>"><?php echo $page_name;?></a></div>
<?php } ?>	

</div>
</div>
<?php }?>
</div>
</div>
</div>
<!-- MIDDLE ENDS -->

<?php $this->load->view("bottom_application");?>