<?php
if ( ! function_exists('has_child_pages'))
{
	function has_child_pages($catid,$condtion="AND status='1'")
	{
		$ci = CI();
		$sql="SELECT page_id FROM wl_dynamic_pages WHERE parent_id=$catid $condtion ";
		$query = $ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		return $num_rows >= 1 ? TRUE : FALSE;
	}
}

if ( ! function_exists('get_child_pages'))
{
	function get_child_pages($parent='0',$condtion="AND status='1' ", $fields='SQL_CALC_FOUND_ROWS*')
	{
		$parent = (int) $parent;
		$ci     = CI();
		$output        = array();
		$sql           = "SELECT  $fields FROM wl_dynamic_pages WHERE parent_id=$parent $condtion  ";
		$query         = $ci->db->query($sql);
		$num_rows      =  $query->num_rows();
		
		if ( $num_rows > 0)
		{
			foreach( $query->result_array() as $row )
			{
				$output[$row['page_id']]['parent'] = $row;
				$output[$row['page_id']]['child'] = array();
				
				if ( has_child($row['page_id'] ))
				{
					$output[$row['page_id']]['child'] = get_child_categories($row['page_id'], $condtion, $fields);
				}
			}
		}
		return $output;
	}
}



if ( ! function_exists('count_dynamic_page'))
{
	function count_dynamic_page($condtion='')
	{
		$ci = CI();
		$condtion = "status !='2' ".$condtion;
		$sql="SELECT COUNT(page_id)  AS total_subcategories FROM wl_dynamic_pages WHERE $condtion ";
		$query=$ci->db->query($sql)->row_array();
		return  $query['total_subcategories'];
	}
}


if ( ! function_exists('get_page_name'))
{
	function get_page_name($catid)
	{
		$ci = CI();
		$sql="SELECT page_name FROM wl_dynamic_pages WHERE category_id='$catid' ";
		$query=$ci->db->query($sql);

		$num_rows     =  $query->num_rows();
		if($num_rows>0)
		{
			return $query->row_array();
		}
	}
}

if (!function_exists('get_page_level')) {

    function get_page_level($category_id) {
        $category_id = (int) $category_id;
        $ci = CI();
        $level = 1;
		$flag = true;
		while($flag)
		{
				$sql = "SELECT parent_id FROM wl_dynamic_pages WHERE page_id ='".$category_id."'";
				$query = $ci->db->query($sql);
				$row = $query->row_array();
				if($row['parent_id']>0)
				{
					$level++;
					$category_id = $row['parent_id'];	
					
					
				}
				else{
					$flag = false;
				}

		}

        return $level;
    }

}

 
	/*  if($parent_id =='' || $parent_id == 0)
	  {
		$level = 0;  
	  }
	  else{
		 $level = get_category_level($parent_id);
	  }
	 // echo $level;
	  if($level<2)
	  { }*/

