<?php
if(is_array($res) && !empty($res) )
{ 
  foreach($res as $val)
  {    
	$events_links = site_url($val['friendly_url']);	
	if($val['alt_tag']){
		$alt_tag=ucfirst($val['alt_tag']);
	}else{
		$alt_tag=ucfirst($val['events_title']);
	}
	 $dateval= getDateFormat($val['start_date'],1);
	 //trace($dateval);
	 //$date=explode(' ',$dateval);
	 //$date1=$date[0];
	 //$date2=$date[1];
	 //$date3=$date[2];
	  	
  ?>
  <li class="listpager">
<div class="news_out">
<div class="nws_img">
<figure><a href="<?php echo $events_links;?>"><img src="<?php echo get_image('news',$val['events_image'],'261','171','AR'); ?>" alt="" class="img-fluid"/></a></figure>
</div>
<div class="nws_rgt_sect">
<p class="dat"><i class="far fa-calendar-alt"></i><?php echo $dateval;?></p>
<p class="nws_titl"><a href="<?php echo $news_links;?>"><?php echo char_limiter($val['events_title'],50); ?></a></p>
<p class="nws_cont"><?php echo char_limiter($val['events_description'],550);?></p>
<p class="nws_butt"><a href="<?php echo $events_links;?>" title="View Details">View Details</a></p>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
  
  
  
  
  <?php
 }
}
?>