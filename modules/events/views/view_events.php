<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('News &amp; Events');?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   } </script>
<div class="clearfix"></div>
<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1>News &amp; Events</h1>
<div class="w-100 mt-3 mb-4">

<?php echo form_open("",'id="myform" method="post" ');?>
        <input type="hidden" name="per_page" value="<?php echo $this->config->item('per_page');?>">
        <input type="hidden" name="offset" id="pg_offset" value="0">
        <?php echo form_close();?>
        <?php
		  if(is_array($res) && !empty($res)){ $data['res'] = $res;
		  ?>
			 <ul class="float" id="prodListingContainer">			
				 <?php $this->load->view('events/load_events_view',$data);?> 
                        
			</ul>
             <div class="clearfix"></div>  
<?php if($totalProduct > $record_per_page){?>
	   <p class="mt20 mb20 text-center" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p>
	   <?php 
	   }  
        
            }else{
             echo '<div class="text-center"><strong>No record found !</strong></div>';
            }
            ?> 





</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>