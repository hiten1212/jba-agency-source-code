<?php
$this->load->model(array('events/events_model'));
$param         = array('status'=>'1','latest_news'=>'1');	
$res_array     = $this->events_model->get_events(12,0,$param);

if(is_array($res_array) && !empty($res_array))
{
?>
<div class="nws_sect_out">
<div class="container">
<h2>Our News &amp; Events</h2>
<div class="w-100 mt-3 mb-3">
<div id="news_scroll" class="owl-carousel owl-theme">
 <?php
  foreach($res_array as $val)
  {
	  $news_links = site_url($val['friendly_url']);
	  
	  if($val['alt_tag']){
		  $alt_tag=escape_chars(ucfirst($val['alt_tag']));
	  }else{
		  $alt_tag=escape_chars(ucfirst($val['events_title']));
	  }
		  $post_date= getDateFormat($val['start_date'],1);;
	  ?>
<div class="item">
<div class="news_out">
<div class="nws_img">
<figure><a href="<?php echo $news_links;?>"><img src="<?php echo get_image('news',$val['events_image'],'261','171','AR'); ?>" alt="" class="img-fluid"/></a></figure>
</div>
<div class="nws_rgt_sect">
<p class="dat"><i class="far fa-calendar-alt"></i><?php echo getDateFormat($val['start_date'],1);?></p>
<p class="nws_titl"><a href="<?php echo $news_links;?>"><?php echo char_limiter($val['events_title'],50); ?></a></p>
<p class="nws_cont"><?php echo char_limiter($val['events_description'],300); ?></p>
<p class="nws_butt"><a href="<?php echo $news_links;?>" title="View Details">View Details</a></p>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<!-- end -->




</div>
</div>
<div class="clearfix"></div>
<p class="text-center"><a href="<?php echo site_url('news-and-events');?>" title="View More" class="vew_mor1">View More</a></p>
<div class="clearfix"></div>
</div>
</div>



 <?php
}
?>
