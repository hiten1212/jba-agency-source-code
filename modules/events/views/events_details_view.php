<?php $this->load->view("top_application");
if($res['alt_tag']){
	$alt_tag=ucfirst($res['alt_tag']);
}else{
	$alt_tag=ucfirst($res['events_title']);
}
$dateval= getDateFormat($res['start_date'],1);
//trace($dateval);
$date=explode(' ',$dateval);
$date1=$date[0];
$date2=$date[1];
$date3=$date[2];
?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php navigation_breadcrumb($res['events_title'],array('News & Events'=>'news-and-events'));?>
<div class="clearfix"></div>
<div class="container">
<div class="mid_area">
<div class="cms_area">
<div class="w-100 mt-3 mb-4">
<div class="row">


<div class="col-lg-5">
<div class="nws-det-area">
<div class="nws-bx">
<figure><img src="<?php echo get_image('news',$res['events_image'],'500','328','AR');?>" alt="" class="img-fluid"/></figure>
</div>
</div>
</div>
<!-- end -->


<div class="col-lg-7">
<div class="nws_det_txt">
<h1><?php echo $res['events_title'];?></h1>
<p class="dat"><i class="far fa-calendar-alt"></i><?php echo $dateval;?></p>
<div class="nws_cont1"><?php echo $res['events_description'];?></div>
</div>
</div>
<!-- end -->


</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<?php $this->load->view("bottom_application");?>