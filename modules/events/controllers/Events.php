<?php
class Events extends Public_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('events/events_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'events';
	}

	public function index()
	{
		 $this->page_section_ct = 'events';
		   
		// $this->config->set_item('per_page','2');
		 
		$record_per_page        = (int) $this->input->post('per_page');		
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');	
		$base_url      =  "events";
				
		$param = array('status'=>'1');
		$res_array              = $this->events_model->get_events($config['per_page'],$offset,$param);		
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct'] = $total_rows;
		$data['record_per_page'] = $config['per_page'];
		$data['title'] = 'Events';
		$data['res'] = $res_array; 		
		$data['base_url'] = $base_url;
		$data['frm_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE)
		{
		  $this->load->view('events/load_events_view',$data);
		}
		else
		{
		  $this->load->view('events/view_events',$data);
		}
		   
			
	}		
	
	public function details()
	{	
		$this->page_section_ct = 'events';
	
		//$id = (int) $this->uri->rsegment(3);	
		$id = (int) $this->meta_info['entity_id'];	
		$param     = array('wl_events.status'=>'1','where'=>"wl_events.events_id ='$id' ");	
		$res       = $this->events_model->get_events(1,0,$param);
				
		if(is_array($res) && !empty($res))
		{			
			$data['title'] = 'News';
		    $data['res'] = $res; 
			
			$param2     = array('wl_events.status'=>'1','where'=>"wl_events.events_id !='$id' ");	
			$recnt       = $this->events_model->get_events(15,0,$param2);
			$data['recnt'] = $recnt;					
		    $this->load->view('events/events_details_view',$data);			
		}else
		{
			redirect('events', ''); 
			
		}
		
	}

	public function download_pdf()
	{
	  $id = (int) $this->uri->segment(3);

	  $param     = array('status'=>'1','where'=>"events_id ='$id' ");	

	  $res_topic       = $this->events_model->get_events(1,0,$param);	
		  

	  if(is_array($res_topic) && !empty($res_topic))
	  {
		if($res_topic['events_pdf']!='' && file_exists(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']))
		{
			$this->load->helper('download');
			$data = file_get_contents(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']);
			$name = $res_topic['events_pdf'];
			force_download($name, $data); 
		}
	  }
	  
	}
	
	
}


/* End of file pages.php */

?>
