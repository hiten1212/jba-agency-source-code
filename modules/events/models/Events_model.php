<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Events_model extends MY_Model
 {

		 
	 public function get_events($limit='10',$offset='0',$param=array())
	 {		
		
		$status			    =   @$param['status'];
		$orderby		    =   @$param['orderby'];	
		$latest_events		    =   @$param['latest_events'];
		$where		        =   @$param['where'];	
		
		$keyword = $this->db->escape_str($this->input->get_post('keyword',TRUE));
		$from_date = $this->db->escape_str(trim($this->input->get_post('from_date',TRUE)));
		$to_date   = $this->db->escape_str(trim($this->input->get_post('to_date',TRUE)));
			
	    if($from_date!='' ||  $to_date!=''){
			 $condition_date=array();
			 $condition.="  (";
			 if($from_date!=''){
				 $condition_date[] = "DATE(start_date)>='$from_date'";
			 }
			 if($to_date!=''){
				 $condition_date[] ="DATE(start_date)<='$to_date'";
			 }
			 $condition.=implode(" AND ",$condition_date)." )";
			// $where=$condition;
		}
		
		if($status!='')
		{
			$this->db->where("wl_events.status",$status);
		}
		 if($latest_events!='')
		{
			$this->db->where("wl_events.latest_events",$latest_events);
		}
		
	    if($where!='')
		{
			$this->db->where($where);
		}
		
		if($condition!='')
		{
			$this->db->where($condition);
		}
		
		
		if($keyword!='')
		{
						
			$this->db->where("(wl_events.events_title LIKE '%".$keyword."%' OR wl_events.publisher LIKE '%".$keyword."%' )");
				
		}
		if($orderby!='')
		{
			 $this->db->order_by($orderby);
			
		}else if($this->input->get_post("sortbyname")!=""){
			$orderbyn = $this->input->get_post("sortbyname");
		}
		else
		{
		  $this->db->order_by('wl_events.events_id','desc');
		}
		
		if($orderbyn!='')
		{
			$orderbyn=$this->db->escape_str($orderbyn);
			if($orderbyn=="az"){
				$this->db->order_by('wl_events.events_title','asc');
			}
			if($orderbyn=="za"){
				$this->db->order_by('wl_events.events_title','desc');
			}
		}
		
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wl_events.*',FALSE);		
		$this->db->from('wl_events');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
				
	}
		  
	public function get_events_media($limit='4',$offset='0',$param=array()) 
	{ 
		$default			    =   @$param['default']; 
		$projectid			    =   @$param['eventsid']; 
		$media_type				=   @$param['media_type']; 
		//$color_id=$this->uri->segment('6'); 
		if( is_array($param) && !empty($param) ) 
		{ 
			$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE); 
			$this->db->limit($limit,$offset); 
			$this->db->from('wl_events_media'); 
			$this->db->where('events_id',$projectid); 
			if($default!='') 
			{ 
				$this->db->where('is_default',$default); 
			} 
			if($media_type!='') 
			{ 
				$this->db->where('media_type',$media_type); 
			}   
			$q=$this->db->get(); 
			//echo_sql(); 
			$result = $q->result_array(); 
			$result = ($limit=='1') ? @$result[0]: $result; 
			return $result; 
		 } 
	} 
	 
}
?>