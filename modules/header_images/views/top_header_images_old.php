<?php
$this->load->helper(array('string','text'));
$header_images_array = array();
$sql = "SELECT * FROM wl_header_images WHERE header_image!='' AND status='1' ORDER BY  RAND() LIMIT 10";
$query = $this->db->query($sql);
if($query->num_rows() > 0){
	$data_result = $query->result_array();
	foreach($data_result as $val){
		if($val['header_image']!='' && file_exists(UPLOAD_DIR."/header_images/".$val['header_image'])){
			array_push($header_images_array,$val);
		}
	}
}
if(is_array($header_images_array) && !empty($header_images_array)){	?>


<!-- banner area start -->
<div class="container" style=" border-radius:10px; overflow:hidden; position:relative; margin-top:5px;">

<div id="Page_loader"><div class="AniDG"></div></div>
<div class="fluid_dg_wrap fluid_dg_charcoal_skin fluid_container" id="fluid_dg_wrap_1" >
  <?php
	   $counter=1;
	   foreach($header_images_array as $key=>$val){  ?>
	   
	   <div data-src="<?php echo get_image('header_images',$val['header_image'],'1230','464','R');?>" data-link="<?php echo $val['header_url']; ?>" data-target="_blank">
<div class="fluid_dg_caption moveFromLeft"><div>
<?php  if(!empty($val['line_one'])){ ?> 
            <div class="bnr-txt1"><?php echo $val['line_one'];?></div>
			<?php }?>
			  <?php  if(!empty($val['line_two'])){ ?> 
            <div class="bnr-txt2"><?php echo $val['line_two'];?></div>
			<?php }?>
			 <?php  if(!empty($val['line_three'])){ ?> 
			<div class="bnr-txt3"><?php echo $val['line_three'];?></div>
<?php }?>
</div>
</div>
</div>
		<?php }?>    
  </div>
</div>
	
<?php }
else { ?>
<div class="container" style=" border-radius:10px; overflow:hidden; position:relative; margin-top:5px;">
<div id="Page_loader"><div class="AniDG"></div></div>
<div class="fluid_dg_wrap fluid_dg_charcoal_skin fluid_container" id="fluid_dg_wrap_1" >
<div data-src="<?php echo resource_url();?>banner/slide3.jpg">
<div class="fluid_dg_caption moveFromLeft"><div>
<div class="bnr-txt1">Thermoplastic <br> Powder Coatings</div>
<div class="bnr-txt2">Dulux Fluroset</div>
<div class="bnr-txt3">Ultra durable fluoropolymer thermosetting powder. It is ideal for use on pre treated architectural grade aluminium , mild steel, bright/semi bright steel, black steel and blue steel with an approved powder coating primer system In mild, tropical and severe exterior conditigons</div>
</div>
</div>
</div>
</div>

</div>
<?php } ?>

