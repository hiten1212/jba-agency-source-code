<?php
$this->load->helper(array('string','text'));
$header_images_array = array();
$sql = "SELECT * FROM wl_header_images WHERE header_image!='' AND status='1' ORDER BY  RAND() LIMIT 10";
$query = $this->db->query($sql);
if($query->num_rows() > 0){
	$data_result = $query->result_array();
	foreach($data_result as $val){
		if($val['header_image']!='' && file_exists(UPLOAD_DIR."/header_images/".$val['header_image'])){
			array_push($header_images_array,$val);
		}
	}
}
?>
<script>
	
	$(document).ready(function() {
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		if(width < 768) {
			$('.windowimg').hide();
			$('.mobileimg').show();
			}else{
			$('.mobileimg').hide();
			$('.windowimg').show();
			}
	
	
	
});
</script>

<?php
if(is_array($header_images_array) && !empty($header_images_array))
{	?>

<!--Banner-->
<div class="mainslider_js slider_dots owl-theme">
<?php
	$counter=1;
	foreach($header_images_array as $key=>$val){  ?>
	<div class="uniqueA">
		<img class="windowimg" src="<?php echo get_image('header_images',$val['header_image'],'1400','600','R');?>">
		<img class="mobileimg" src="<?php echo get_image('header_images',$val['header_image'],'350','700','R');?>">	
	</div>
	
	<?php echo $counter; $counter++;} ?>
<!-- end -->
<?php }
else { ?>
<div><img src="<?php echo theme_url(); ?>images/slider-img4.jpg"></div>
                            <div><img src="<?php echo theme_url(); ?>images/slider-img3.jpg"></div>
                            <div><img src="<?php echo theme_url(); ?>images/slider-img2.jpg"></div>
                            <div><img src="<?php echo theme_url(); ?>images/slider-img1.jpg"></div>
<!-- end -->
<?php } ?>
</div>
<!-- banner area -->
 
<!-- <div class="clearfix"></div> -->

