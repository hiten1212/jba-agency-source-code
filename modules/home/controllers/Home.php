<?php
class Home extends Public_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('pages/pages_model','brand/brand_model','gallery/gallery_model','service_category/service_category_model','services/services_model'));
		//$this->load->helper(array('dynamic_pages/dynamic_pages','banner'));
		$this->load->library(array('Dmailer'));
		$this->form_validation->set_error_delimiters("<div class='red fs12'>","</div>");
		//create_listing_page_meta();
	}

	public function index(){
		$data['page_title']            = "";
		$data['page_keyword']          = "";
		$data['page_description']      = "";
		$offset                 =  $this->uri->segment(2,0);
		$product_limit_featured = 8;
		$product_limit_hot = 6;
		$product_limit_new = 8;

				
		$welcome_content = get_db_field_value('wl_cms_pages','page_description'," AND friendly_url='homemessage' AND status='1'");
		$data['welcome_content']       = $welcome_content;
		
		
		$config['per_page']	     =  '200';

		$offset                 = (int) $this->input->post('offset');
		
		$param= array('status'=>'1');

		$brand_array   = $this->brand_model->get_brands($param,$offset,$config['per_page']);

		//echo_sql();
		$config['totalbrand_rows'] = $data['totalProduct']	= get_found_rows();
		$data['brand_array']       = $brand_array;
		
		$param = array('status'=>'1','type'=>'1','parent_id'=>'0','orderby'=>'sort_order asc');
		$gallery_array               = $this->gallery_model->get_gallery($config['per_page'],$offset,$param);	
		$config['totalgallery_rows'] = $data['total_rec'] = get_found_rows();
		$data['gallery_array']       = $gallery_array;
		
		if($this->input->post('action')=='Submit'){
			$this->contactus();
		}	

                $allcms_pages = $this->pages_model->get_allcmspages();
                $data['allcms_pages']       = $allcms_pages;
		
		$this->load->view('home',$data);

	}
	
	public function contactus() {
		
		$this->form_validation->set_rules('first_name','Your Name','trim|alpha|required|max_length[30]');
		$this->form_validation->set_rules('last_name','Last Name','trim|alpha|max_length[30]');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('mobile_number','Mobile Number','trim|required|numeric|min_length[5]|max_length[25]');
		$this->form_validation->set_rules('service_type','Service Name','trim|max_length[3000]');
		//$this->form_validation->set_rules('company_name','Company','trim|max_length[200]');
		$this->form_validation->set_rules('message','About Your Project','trim|required|max_length[8500]');
		//$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[contact]');
		$data['page_error'] = "";

		if($this->form_validation->run()==TRUE){

			$posted_data=array(
			'type'  =>'4',
			'service_type'  =>$this->input->post('service_type'),
			'first_name'    => $this->input->post('first_name'),
			'last_name'     => $this->input->post('last_name'),
			'email'         => $this->input->post('email'),
			'phone_number'  => $this->input->post('phone_number'),
			'mobile_number'  => $this->input->post('mobile_number'),
			'company_name'  => $this->input->post('company_name'),
			'country'		=> '',
			'message'       => $this->input->post('message'),
			'receive_date'     =>$this->config->item('config.date.time')
			);
			$posted_data = $this->security->xss_clean($posted_data);
			$this->home_model->safe_insert('wl_enquiry',$posted_data,FALSE);
			/* Send  mail to user */
			$content    =  get_content('wl_auto_respond_mails','10');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$name = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{service_type}',$this->input->post('service_type'),$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);
			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to user */
			/* Send  mail to admin */
			$content    =  get_content('wl_auto_respond_mails','10');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$mname = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
			$body			=	str_replace('{mem_name}',$mname,$body);
			$body			=	str_replace('{service_type}',$this->input->post('service_type'),$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);
			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to admin */
			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_userdata(array('msg_type_convs_request'=>'success_contect'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully. We will get back to you soon.');
			redirect('#sid', '');
		}else{
			$posted_data_str=$this->input->post('action');
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}
		}
		//$friendly_url = $this->uri->segment(2)==''?$this->uri->segment(1):$this->uri->segment(2);
								
		//$data['page_heading'] = "Domestic";
		//$this->load->view('pages/domestic',$data);
		
	}
	
	public function get_likes(){	

		 $blogs_id=(int) $_POST['blog_id'];
				
		$likecount=get_db_field_value("wl_blogs","likes"," AND blogs_id='".$blogs_id."' "); 
				
		$likecount=$likecount+1;
		$posted_data=array('likes' => $likecount);	
		
		$where = "blogs_id = '".$blogs_id."'";				
		$this->blog_model->safe_update('wl_blogs',$posted_data,$where,false);
		
		$likesRes=get_db_field_value("wl_blogs","likes"," AND blogs_id='".$blogs_id."' ");
		
		echo $likesRes;
		
	}


	public function contact(){

		$this->form_validation->set_rules('first_name','Name','trim|alpha|required|max_length[30]');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('message','Message','trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code');

		$data['page_error'] = "";

		if($this->form_validation->run()==TRUE){

			$posted_data=array(
			'first_name'    => $this->input->post('first_name'),
			'last_name'     => $this->input->post('last_name'),
			'email'         => $this->input->post('email'),
			'phone_number'  => $this->input->post('phone_number'),
			'mobile_number'  => $this->input->post('mobile_number'),
			'company_name'  => '',
			'country'		=> '',
			'message'       => $this->input->post('message'),
			'receive_date'     =>$this->config->item('config.date.time')
			);

			$posted_data = $this->security->xss_clean($posted_data);
			$this->home_model->safe_insert('wl_enquiry',$posted_data,FALSE);
			
			/********* Send  mail to admin ***********/
			$fullname=$this->input->post('first_name')." ".$this->input->post('last_name');
			$content    =  get_content('wl_auto_respond_mails','7');
			$body       =  $content->email_content;
			$body			=	str_replace('{body_text}','You have received an enquiry and details are given below.',$body);
			$body			=	str_replace('{mem_name}',$fullname,$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);

			if($this->input->post('mobile_number')!=''){

				$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			}else{
				$body			=	str_replace('{mobile}','',$body);
				$body			=	str_replace('Mobile no.:','',$body);
			}
			
			if($this->input->post('phone_number')!=''){

				$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			}else{
				$body			=	str_replace('{phone}','',$body);
				$body			=	str_replace('Phone no.:','',$body);
			}
			
			if($this->input->post('message')!=''){

				$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			}else{
				$body			=	str_replace('{comments}','',$body);
				$body			=	str_replace('Request Details :','',$body);
			}

			$body			=	str_replace('{admin_email}',$this->admin_info->enquiry_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$mail_conf =  array(
							'subject'=>"Enquiry from ".$fullname." ",
							'to_email'=>$this->admin_info->enquiry_email,
							'from_email'=>$this->input->post('email'),
							'from_name'=>$fullname,
							'body_part'=>$body
							);

			$this->dmailer->mail_notify($mail_conf);
			/* End Send  mail to admin */
			/********* Send  mail to user ***********/
			$content    =  get_content('wl_auto_respond_mails','5');
			$body       =  $content->email_content;
			$body			=	str_replace('{mem_name}',$fullname,$body);
			$body			=	str_replace('{body_text}','You have placed an enquiry and details are given below.',$body);
			$body			=	str_replace('{mem_name}',$fullname,$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);

			if($this->input->post('mobile_number')!=''){

				$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			}else{
				$body			=	str_replace('{mobile}','',$body);
				$body			=	str_replace('Mobile no.:','',$body);			
			}

			if($this->input->post('phone_number')!=''){

				$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);

			}else{
				$body			=	str_replace('{phone}','',$body);
				$body			=	str_replace('Phone no.:','',$body);
			}
			if($this->input->post('message')!=''){

				$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			}else{
				$body			=	str_replace('{comments}','',$body);
				$body			=	str_replace('Request Details :','',$body);
			}
			$body			=	str_replace('{admin_email}',$this->admin_info->enquiry_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			
			$mail_conf =  array(
							'subject'=>"Enquiry placed at ".$this->config->item('site_name')." ",
							'to_email'=>$this->input->post('email'),
							'from_email'=>$this->admin_info->enquiry_email,
							'from_name'=>$this->config->item('site_name'),
							'body_part'=>$body
							);
			//trace($mail_conf);
			//exit;
			$this->dmailer->mail_notify($mail_conf);

			/* End Send  mail to user */
			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_userdata(array('msg_type_convs_request'=>'success_contect'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully. We will get back to you soon.');
			redirect('pages/thanks', '');
		}else{
			$posted_data_str=$this->input->post('action');
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}
		}

	}
	
	public function string_search()
		{
	$mysearchString = $this->input->post('mysearchString');
	// Check the user has typed something in our input box.
	if(isset($mysearchString)) {
		//$mysearchString = $db->real_escape_string($mysearchString);
		
		// Is the string length greater than 0?
		
		if(strlen($mysearchString) >0) {
			
			// Now we have the string the user entered, we want to
			// be able to use this to search in our database
			// so we use the percentage as the wildcare and use a variable 
			// in the query.
			
			$query = $this->db->query("SELECT product_name 
								 FROM wl_products 
								 WHERE product_name 
								 LIKE '%$mysearchString%' 
								 AND status = '1'
								 LIMIT 10")->result_array(); // limits our results list to 20.
								 
			
			if($query) {
				
				// so while there are results from the query
				// we loop through the results and fill out our list items
				
				foreach($query as $result) {
					
					$productName  = htmlentities($result['product_name']);
					
					$productName  = html_entity_decode($result['product_name'],ENT_QUOTES);
					 
					//$productName  =  mysql_real_escape_string($productName);
					
					// create a list item, but also listen for the user clicking 
					// the result so we can fill the text box.
					echo '<li onClick="fill(\''.$productName.'\');">'.$result['product_name'].'</li>';
				}
			} else {
				echo 'No Records Found!';
			}
		} else {
			// Dont do anything.
		} 
	} else {
		echo 'Access denied.';
	}
}

	public function opener(){
	$this->load->view('opener','');
	}


}