<?php 
 $data['allcms_pages']       = $allcms_pages;
$this->load->view("top_application" , $data);?>
<?php //$this->load->view('header_images/top_header_images');?>

<div class="main-slider-top">
 <?php
         if($this->admin_info->header_logo!='' && file_exists(UPLOAD_DIR."/logo/".$this->admin_info->header_logo)){
             ?>
             <a class="navbar-brand" href="<?php echo site_url();?>"><div class="innerlogo"  style="background: <?php echo $this->admin_info->logo_background!='' ? $this->admin_info->logo_background:'';  ?>"><img src="<?php echo base_url().'uploaded_files/logo/'.$this->admin_info->header_logo;?>" alt="" title=""></div></a>
         <?php } else{?>
     <a class="navbar-brand" href="<?php echo site_url();?>"><h1><img src="<?php echo theme_url(); ?>images/logo.png" alt="" title=""></h1></a>
         <?php } ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12 co-sm-12 pad-0">
                <div class="slider-area">
                    <div class="sloganmiddle">
                        <h1 style="color: <?php echo$this->admin_info->banner_text_color; ?>"><?php echo $this->admin_info->banner_slogan;?></h1>
                        <?php 

 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
  // $condtion_array['offset'] = 0;
  // $condtion_array['limit'] = 6;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories    =  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
                        <ul>
                            <?php
    foreach($res as $val){
     //$link_url = site_url($val['friendly_url']);
      //$link_url = site_url().'#Services';//.$val['friendly_url'];
      $link_url = site_url().'#'.$val['friendly_url'];
     $total_subcategories = $val['total_subcategories'];
     ?>
<li><a  href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></li>
<?php }
if($total_categories >6)
{
?>
<!--<li><a  href="<?php //echo base_url('category');?>" title="View All &raquo;"><b>View All &raquo;</b></a></li>-->
<?php } ?>
                        </ul>
   <?php } ?>
                    </div>
                    <div class="slider-fluid">
                        
                            <?php
                            if($this->admin_info->video_on_off == 1) { ?>
                            <div class="mainslider_js slider_dots owl-theme">
                                <div>
                                    <video width="" muted="" autoplay="" loop="">
                                    <?php /*<source src="<?php echo theme_url(); ?>images/slider_video.mp4" type="video/mp4">*/?>
                                    <?php if($this->admin_info->header_video!=''){?>
                                    <source src="<?php echo base_url().'uploaded_files/logo/'.$this->admin_info->header_video;?>">
                                    <?php }
                                    else{
                                        ?>
                                    <source src="<?php echo theme_url(); ?>images/slider_video.mp4" type="video/mp4">
                                    <?php } ?>
                                    <source src="<?php echo theme_url(); ?>images/slider_video.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                    </video>
                                </div>
                            </div>
                            <?php } else { ?> 
                                        
                                <?php $this->load->view('header_images/top_header_images');?>
                            <?php } ?>
                            <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>

    

</div>

<?php $about_res = get_db_single_row('wl_cms_pages','*'," AND page_id = '1' AND status='1'");
$page_name=$about_res['page_name'];
$page_url=$about_res['friendly_url'];
$about_image=$about_res['image']!='' ? base_url().'uploaded_files/staticimage/'.$about_res['image']: 'assets/designer/themes/default/images/boutbg.jpg';
$text_color=$about_res['title_background'];
?>
<section class="commondiv aboutbg" style=" padding: 100px 0px;background-image: url(<?php echo $about_image;?>) ;background-repeat: no-repeat" id="<?php echo $page_url; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag" style ="color:<?php echo $text_color; ?>"><?php echo $page_name;?></h3>
                
            </div>
        </div>
    </div>
</section>
<?php 
$welcome_res = get_db_single_row('wl_cms_pages','*'," AND page_id = '2' AND status='1'");

$page_name=$welcome_res['page_name'];
$page_description=$welcome_res['page_description'];
$about_image=$welcome_res['image']!='' ? base_url().'uploaded_files/staticimage/'.$welcome_res['image']: 'uploaded_files/thumb_cache/thumb_1300_867_brands.jpg';
$text_color1=$welcome_res['title_background'];

?>
<section class="commondiv aboutbg" style="padding: 100px 0px;background-image: url(<?php echo $about_image;?>);background-repeat:  no-repeat;-o-background-size:auto 900px !important;-moz-background-size:auto 900px !important;-webkit-background-size:auto 900px !important; background-size:cover !important;width:100%; " id="aboutus">
    <div class="container he100">
        <div class="row he100">
            <div class="col-md-12 col-xs-12 col-sm-12 he100">
                <div class="services_ccco"> <h3 class="service_tag" style ="color:<?php echo $text_color1; ?>"><?php echo $page_name;?></h3>
               <!-- <div class="comments-space"> <p style="font-size:18px; font-family:verdana,geneva,sans-serif;"><?php //echo $page_description;?></p> </div> -->

               <p style="font-size:18px; font-family:verdana,geneva,sans-serif;"><?php echo $page_description;?></p>
            </div>
            </div>
        </div>
    </div>
</section>

<?php $welcome_res = get_db_single_row('wl_cms_pages','*'," AND page_id = '3' AND status='1'");
$page_name=$welcome_res['page_name'];
$page_description=$welcome_res['page_description'];
$about_image=$welcome_res['image']!='' ? base_url().'uploaded_files/staticimage/'.$welcome_res['image']: 'assets/designer/themes/default/images/slider-services-background.jpg';
$text_color2=$welcome_res['title_background'];
$servicepage_url=$welcome_res['friendly_url'];
?>
<a id="creative"></a>
<section class="commondiv sevice" style="padding: 100px 0px; background: url(<?php echo $about_image;?>);background-repeat:  no-repeat;-o-background-size:auto 400px !important;-moz-background-size:auto 400px !important;-webkit-background-size:auto 400px !important; background-size:cover !important;" id="<?php echo $servicepage_url; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag" style ="color:<?php echo $text_color2; ?>"><?php echo $page_name;?></h3>
            </div>
        </div>
    </div>
</section>

<main>
 
    <?php
    $category_name = get_db_field_value('wl_service_categories','category_name'," AND parent_id = '0' ");
    $condtion_array = array(
    'field' =>"*,(SELECT COUNT(category_id) FROM wl_service_categories AS b
    WHERE b.parent_id=a.category_id ) AS total_subcategories",
    'condition'=>"AND parent_id = '$parent_id' AND status='1'",
    'order'=>'sort_order',
    'debug'=>FALSE
    );
    //$condtion_array['offset'] = 0;
    //$condtion_array['limit'] = 5;
    $res = $this->service_category_model->getcategory($condtion_array);
    //echo "<pre>";print_r($res);die;
    if(sizeof($res)>0){
        for($r=0;$r<= sizeof($res)-1;$r++){
            //echo $res[$r]['category_id'];die;
            
            $subcategory_id=$res[$r]['category_id'];        
            $where                   = "wlp.status ='1' ";
            $condition['category_id'] = $res[$r]['category_id'];
            $condition['orderbyo'] = 'sort_order asc';
            $condition['where'] = $where;
            $res_array    =  $this->services_model->get_services($config['per_page'],$offset,$condition);
            $res[$r]['sub_services'] = $res_array;
           
        }
    }
    //echo "<pre>";print_r($res);die;
    ?>
    
    
    <?php
    $wrp = 0;
    for($r=0;$r<= sizeof($res)-1;$r++){
        
        if(sizeof($res[$r]['sub_services'])>0) {
            $t[] = $wrp;
        ?>
    <a id="<?php echo $res[$r]['friendly_url']; ?>"></a>
            <div class="wrapper" id="js-wrapper<?php echo $wrp!=0 ?$wrp: "";?>">
        <ul class="stories">
            <?php  
                
                for($rs=0;$rs<=sizeof($res[$r]['sub_services'])-1;$rs++){ 
                     $lr = $r+1;
                    $lrs = $rs+1;
                    ?>
            
                   <li><a class="trigger1" href="#story<?php echo $lr.$lrs; ?>">Scene 1</a></li> 
               <?php }
                ?>
        </ul>
    
      
           <div class="sections" id="js-slideContainer<?php echo $wrp!=0 ?$wrp: "";?>">
<div class="swiper-container">
<div class="swipe-wrapper">
        <?php  
                
                for($rs=0;$rs<=sizeof($res[$r]['sub_services'])-1;$rs++){
                    $productImage=$res[$r]['sub_services'][$rs]['media'];
                    $service_name=$res[$r]['sub_services'][$rs]['service_name'];
                    $prodImage=get_image('services',$productImage,'1300','867');
                  //  $service_description=strip_tags(html_entity_decode($res[$r]['sub_services'][$rs]['service_description']));
                    $service_description=$res[$r]['sub_services'][$rs]['service_description'];
                    $category_name=$res[$r]['category_name']; 
                    $text_color=$res[$r]['sub_services'][$rs]['text_colour'];
                    $sr = $r+1;
                    $srs = $rs+1;
                    ?>
               <div class="swiper-slide">
                <section class="section combg" style="background: url(<?php echo $prodImage;?>) no-repeat;" id="story<?php echo $sr.$srs; ?>">     
                    <div class="container he100">
                        <div class="row he100">
                            <div class="col-md-12 col-xs-12 col-sm-12 he100">
                                <div class="services_ccco">
                                    <h2 style="color:"><?php echo $category_name; ?></h2>
                                    
                                    <h4 style="color:<?php echo $text_color; ?>"><?php echo $service_name;?></h4>
                                    <p><div class='comments-space'>
                                        <?php echo $service_description ?></div></p>
                                </div>
                            </div>
                        </div>
                    </div>       
                </section>
</div>
                <?php }
        
        ?>
       </div></div>   
        </div>
        
  </div>
   <?php 
   $wrp++;
        }
                }
   // print_r($t);die;
    ?>
    
   

<style type="text/css">


.wrapper {
  width: 100%;
  height: 100vh !important;
  overflow: hidden;
}
.wrapper .scroll-title {
  font-size: 21px;
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
  font-weight: 100;
}
.wrapper .cta {
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
}
.wrapper ul.stories {

    position: absolute;
    top: 50%;
    right: 20px;
    transform: translate(-50%, 0);
    display: block;
    z-index: 20;
    list-style: none;
    width: auto;
    padding: 0;
}
.wrapper ul.stories li {
   display: none;
}
.wrapper ul.stories li a {
    color: white;
    font-size: 0px;
    background: #f36f21;
    width: 12px;
    height: 12px;
    display: block;
    margin: 10px 0;
    border-radius: 50%;
}
.wrapper .progress-bar-container {
  max-width: 300px;
  height: 10px;
  border: #f1f0f0;
  border-radius: 10px;
  position: absolute;
  bottom: 100px;
  z-index: 20;
  display: table;
  overflow: hidden;
  width: 100%;
  background: #222;
  margin: 0 auto;
  left: 50%;
  right: 0;
  transform: translate(-50%, 0);
}
.wrapper .progress-bar-container .progress-bar {
  width: 0%;
  height: 20px;
  position: absolute;
  left: 0;
  top: 0;
  background: red;
  transition: all 0.3s ease;
}
.wrapper .sections {
  width: 400%;
  height: 100%;
}
.wrapper .sections .section {
  height: 100%;
  width: calc(100% / 4);
  float: left;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
}

.wrapper .sections .section .sectionTitle {
  font-size: 30px;
  color: #fff;
  position: absolute;
  z-index: 5;
}
</style>




<?php
            if(is_array($brand_array) && !empty($brand_array) ){?>
<section class="commondiv noshado" id="Clients">
    
                <div class="heading text-center">
                    <h3><?php echo $this->admin_info->clients_menu_text; ?></h3>
                </div>

                <div class="slider_effect">
                    <div class="width_lll">
                    
                        <div class="outlogoidv">
                        <?php $j=1;$k='';
if(is_array($brand_array) && !empty($brand_array) ){
    $counter=1;
    foreach($brand_array as $val){
        $link_url=site_url($val['friendly_url']);   
        $brandImage=get_image('brand',$val["brand_image"],'','');
        ?>
                            <div class="slider_loo"><img src="<?php echo $brandImage;?>"></div>
                    <?php if($counter%15==0){
                        if($j==1)
                        {
                        echo ' </div></div><div class="width_lll_'.$j.'"><div class="outlogoidv">';
                        $j='';
                        }
                        else{echo ' </div></div><div class="width_lll'.$k.'"><div class="outlogoidv">';}
                        }
                    $counter++;} 

                        } ?>
                           
                        </div>
                       
                    </div>
                </div>

             
</section>
            <?php } ?>
<?php
            if(is_array($gallery_array) && !empty($gallery_array) ){?>
<section class="commondiv " style="background-color: #000; padding: 100px 0px;" id="Portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag"><?php echo $this->admin_info->portfolio_menu_text; ?></h3>
                <br>
            <br>
            <br>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="works">
                <?php foreach($gallery_array as $val){
          // $cls = ( $ix==0 )   ? "cat-w fl" : "cat-w fl ml10" ;               
        $id=$val['gallery_id'];
        $total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
        $link_url = base_url().$val['friendly_url'];           
        ?>
                    <div>
                        <!-- <a href="<?php //echo $link_url;?>" class="work_mg"> -->

                        <a href="javascript:void(0);" class="work_mg portfolio_slider_item" data-portfolioid="<?php echo $id; ?>">
                            <img src="<?php echo get_image('gallery',$val['gallery_image'],'','','R'); ?>">
                            <span><?php echo $val['gallery_title'];?></span>
                        </a>
                    </div>
                    
                <?php } ?>
                    
                
                </div>
            </div>
        </div>
    </div>
</section>
            <?php } ?>


<section class="commondiv noshado">
   
<div class="content-row row_padding_top light-section">
                                
 <!-- <a href="#footer" class="span_contact">
      <div class="inner_spancon"><div class="arrow_svg">
        <img src="<?php echo theme_url(); ?>images/arrowicon.png">
      </div>
       get in touch
    </div>
  </a>-->
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
    
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-backward secondary-font">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
     <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
</div> 
</section>

<?php 
$this->load->view("bottom_application");?>
<style>
     .services_ccco h4{margin: 0 0 25px 0;font-size: 30px;color: #FFF;font-weight: 600;font-family: Bauhausb Medium;letter-spacing: 3px;text-shadow: 4px 3px 3px #000000bd;}
     .services_ccco h2{margin: 0 0 25px 0;font-size: 40px;color: #FFF;font-weight: 600;font-family: Bauhausb Medium;letter-spacing: 3px;text-shadow: 4px 3px 3px #000000bd;}
</style>   
