<?php //trace($res); exit;
if(is_array($res) && !empty($res)){
	foreach($res as $val){
		
		//$link_url = site_url($val['friendly_url']);
		$discounted_price = $val['product_discounted_price']>0 && $val['product_discounted_price']!=null ? TRUE : FALSE;
		$prodImage=get_image('products',$val["media"],'295','275');
		$alt_tag=$val['product_alt'];			
		$product_description = char_limiter($val['products_description'],140);	
		$dateval=getDateFormat($val['event_from_date'],1);
		$dateres=explode(' ',$dateval);
		$par_id=get_parent_category($val['category_id']); 
		$category_name=get_db_field_value('wl_categories','category_name'," AND category_id='".$par_id."' AND status='1'");
		//trace($dateres);
		?> 
		<div class="item">
<div class="pro_box">
<p class="pro_pic"><span><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><img src="<?php echo $prodImage; ?>" alt="<?php echo $alt_tag;?>"></a></span></p>
<div class="pro_cont">
<p class="pro_title red"><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><?php echo $val['product_name'];?></a></p>
<p class="pro_desc"><?php echo char_limiter($product_description,200); ?></p>
<p class="text-uppercase fs13 text-uppercase red weight700"><a href="<?php echo $link_url;?>" title="View Details">View Details</a></p>
</div>
</div>
</div>
        
		<?php
	}
}?>