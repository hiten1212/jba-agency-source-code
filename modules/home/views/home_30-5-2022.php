<?php $this->load->view("top_application");?>
<?php //$this->load->view('header_images/top_header_images');?>


<div class="main-slider-top">
     <a class="navbar-brand" href="index.php"><h1><img src="<?php echo theme_url(); ?>images/logo.png" alt="" title=""></h1></a>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12 co-sm-12 pad-0">
                <div class="slider-area">
                    <div class="sloganmiddle">
                        <h1>Bringing Ideas To Life</h1>
						<?php 

 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 6;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
                        <ul>
                            <?php
	foreach($res as $val){
	 //$link_url = site_url($val['friendly_url']);
	  $link_url = site_url().'#Services';//.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
<li><a  href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></li>
<?php }
if($total_categories >6)
{
?>
<li><a  href="<?php echo base_url('category');?>" title="View All &raquo;"><b>View All &raquo;</b></a></li>
<?php } ?>
                        </ul>
   <?php } ?>
                    </div>
                    <div class="slider-fluid">
                        <div class="mainslider_js slider_dots owl-theme">
                            <div>
                                <video width="" muted="" autoplay="" loop="">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.mp4" type="video/mp4">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.ogg" type="video/ogg">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>

                            <div><img src="<?php echo theme_url(); ?>images/slider-img4.jpg"></div>
                            
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>

    

</div>


<section class="commondiv sevice" style=" padding: 100px 0px;" id="Services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Services</h3>
            </div>
        </div>
    </div>
</section>

<main>

 
    
<?php $category_name = get_db_field_value('wl_service_categories','category_name'," AND parent_id = '0' ");
$condtion_array = array(
'field' =>"*,(SELECT COUNT(category_id) FROM wl_service_categories AS b
WHERE b.parent_id=a.category_id ) AS total_subcategories",
'condition'=>"AND parent_id = '$parent_id' AND status='1'",
'order'=>'sort_order',
'debug'=>FALSE
);
$condtion_array['offset'] = 0;
$condtion_array['limit'] = 25;
$res = $this->service_category_model->getcategory($condtion_array);
$total_categories	=  $this->service_category_model->total_rec_found;
if($total_categories > 0){	
$counter='1';
      foreach($res as $val){ 
          $total_subcategories = $val['total_subcategories'];         
          $sel = $val['friendly_url'];  
		  $wraper=$counter==1?'':$counter-1;
		  ?> 
    
    <div class="wrapper" id="js-wrapper<?php echo $wraper;?>">
        <ul class="stories">
		<?php $where                   = "wlp.status ='1' ";
		     $condition['category_id'] = $val['category_id'];
			 $condition['where'] = $where;
		
		
		$res_array               =  $this->services_model->get_services($config['per_page'],$offset,$condition);
		
		$total_product=get_found_rows();
		if($total_product > 0){	
		 $seg2=$this->uri->segment(1);
		 $j=1;
		 foreach($res_array as $val2){
		 ?>
      <li><a class="trigger<?php echo $j;?>" href="#story<?php echo $counter;?><?php echo $j;?>">Scene <?php echo $j;?></a></li>
            
		 <?php $j++;} ?>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer<?php echo $counter;?>">
          
       <?php $i=1;
		 foreach($res_array as $val2){
			$link_url = site_url($val2['friendly_url']);		
		$prodImage=get_image('services',$val2["media"],'1300','867'); 			
		 	$sel = $this->uri->rsegment(3);   ?>     
        <section class="section combg" style="background: url(<?php echo $prodImage;?>) no-repeat;" id="story<?php echo $counter;?><?php echo $i;?>">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3><?php echo $val['category_name'];?></h3>
                            <p>
						<?php if(!empty($val2['service_description'])){?>
						<div class='comments-space'><?php echo strip_tags(html_entity_decode($val2['service_description']));?></div>
						<?php } ?>
							</p>
                            
    
    

                        </div>
                    </div>
                </div>
            </div>       
        </section>
		
		 <?php $i++; }?>


          
          
        </div>
		<?php } ?>
        
  </div>
  <?php $counter++;
	  } ?>
<?php
}?>

  








<?php
			if(is_array($brand_array) && !empty($brand_array) ){?>
<section class="commondiv" id="Clients">
    
                <div class="heading text-center">
                    <h3>Clients</h3>
                </div>

                <div class="slider_effect">
                    <div class="width_lll">
					
                        <div class="outlogoidv">
						<?php $j=1;$k='';
if(is_array($brand_array) && !empty($brand_array) ){
	$counter=1;
	foreach($brand_array as $val){
		$link_url=site_url($val['friendly_url']);	
		$brandImage=get_image('brand',$val["brand_image"],'110','47');
		?>
                            <div class="slider_loo"><img src="<?php echo $brandImage;?>"></div>
					<?php if($counter%7==0){
						if($j==1)
						{
						echo ' </div></div><div class="width_lll_'.$j.'"><div class="outlogoidv">';
						$j='';
						}
						else{echo ' </div></div><div class="width_lll'.$k.'"><div class="outlogoidv">';}
						}
					$counter++;} 

						} ?>
                           
                        </div>
                       
                    </div>
                </div>

             
</section>
			<?php } ?>
<?php
			if(is_array($gallery_array) && !empty($gallery_array) ){?>
<section class="commondiv" style="background-color: #000; padding: 100px 0px;" id="Portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Portfolio</h3>
                <br>
            <br>
            <br>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="works">
                <?php foreach($gallery_array as $val){
          // $cls = ( $ix==0 )	 ? "cat-w fl" : "cat-w fl ml10" ;				
        $id=$val['gallery_id'];
        $total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
        $link_url = base_url().$val['friendly_url'];		   
        ?>
                    <div>
                        <a href="<?php echo $link_url;?>" class="work_mg">
                            <img src="<?php echo get_image('gallery',$val['gallery_image'],'550','380','R'); ?>">
                            <span><?php echo $val['gallery_title'];?></span>
                        </a>
                    </div>
					
				<?php } ?>
                    
                
                </div>
            </div>
        </div>
    </div>
</section>
			<?php } ?>


<section class="commondiv" >
   
<div class="content-row row_padding_top light-section">
                                
 <!-- <a href="#footer" class="span_contact">
      <div class="inner_spancon"><div class="arrow_svg">
        <img src="<?php echo theme_url(); ?>images/arrowicon.png">
      </div>
       get in touch
    </div>
  </a>-->
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
    
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-backward secondary-font">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
     <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
</div> 
</section>
<?php $this->load->view('project_footer');
// echo $this->uri->rsegment(1);
?>

<?php /* <p id="back-top"><a href="#top"><span></span></a></p> */?>
<script src="https://webxcreation.in/designer/jba/js/jquery.min.js"></script>
<script src="https://webxcreation.in/designer/jba/js/bootstrap.min.js"></script>
<script src="https://webxcreation.in/designer/jba/js/owl.carousel.min.js"></script>

<script src="https://webxcreation.in/designer/jba/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>    
    <script src="https://webxcreation.in/designer/jba/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js" ></script>
    <script src="https://webxcreation.in/designer/jba/libs/gsap/3.7.1/Draggable.min.js" ></script>
    <script src="https://webxcreation.in/designer/jba/libs/gsap/3.7.1/ScrollTrigger.min.js" ></script>
    <script src='https://webxcreation.in/designer/jba/libs/three.js/r83/three.js'></script>
  <script src='https://webxcreation.in/designer/jba/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.js'></script>
  <script src="https://webxcreation.in/designer/jba/js1/clapatwebgl.js"></script>
  <script src="https://webxcreation.in/designer/jba/js1/plugins.js"></script>
    <script src="https://webxcreation.in/designer/jba/js1/common.js"></script>
    <script src="https://webxcreation.in/designer/jba/js1/scripts.js"></script>
<script src="https://webxcreation.in/designer/jba/js/main.js"></script>

<script src="//s3-us-west-2.amazonaws.com/s.cdpn.io/16327/gsap-latest-beta.min.js"></script>
<script src="//s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollToPlugin3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
<script type="text/javascript">
  gsap.defaultEase = Linear.easeNone;
    var titles = document.querySelectorAll(".sectionTitle");
    var controller = new ScrollMagic.Controller();
    var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


    
    
    // create timeline
    // this could also be created in a loop
    tl.to("#js-slideContainer1", {duration: 1, xPercent: 0 }, "label1");
    tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
    tl.to("#js-slideContainer1", {duration: 1, xPercent: -25 }, "label2");
    tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
    tl.to("#js-slideContainer1", {duration: 1, xPercent: -50 }, "label3");
    tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
    tl.to("#js-slideContainer1", {duration: 1, xPercent: -75 }, "label4");
    tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


    new ScrollMagic.Scene({
    triggerElement: "#js-wrapper",
    triggerHook: "onLeave",
    duration: "400%"

    })
    .setPin("#js-wrapper")
    .setTween(tl)
    .addIndicators({
      colorTrigger: "black",
      colorStart: "black",
      colorEnd: "black"
    })
    .addTo(controller);



</script>


<script type="text/javascript">
  gsap.defaultEase = Linear.easeNone;
    var titles = document.querySelectorAll(".sectionTitle");
    var controller = new ScrollMagic.Controller();
    var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


    
    
    // create timeline
    // this could also be created in a loop
    tl.to("#js-slideContainer2", {duration: 1, xPercent: 0 }, "label1");
    tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
    tl.to("#js-slideContainer2", {duration: 1, xPercent: -25 }, "label2");
    tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
    tl.to("#js-slideContainer2", {duration: 1, xPercent: -50 }, "label3");
    tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
    tl.to("#js-slideContainer2", {duration: 1, xPercent: -75 }, "label4");
    tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


    new ScrollMagic.Scene({
    triggerElement: "#js-wrapper1",
    triggerHook: "onLeave",
    duration: "400%"

    })
    .setPin("#js-wrapper1")
    .setTween(tl)
    .addIndicators({
      colorTrigger: "black",
      colorStart: "black",
      colorEnd: "black"
    })
    .addTo(controller);



</script>



<script type="text/javascript">
  gsap.defaultEase = Linear.easeNone;
    var titles = document.querySelectorAll(".sectionTitle");
    var controller = new ScrollMagic.Controller();
    var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


    
    
    // create timeline
    // this could also be created in a loop
    tl.to("#js-slideContainer3", {duration: 1, xPercent: 0 }, "label1");
    tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
    tl.to("#js-slideContainer3", {duration: 1, xPercent: -25 }, "label2");
    tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
    tl.to("#js-slideContainer3", {duration: 1, xPercent: -50 }, "label3");
    tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
    tl.to("#js-slideContainer3", {duration: 1, xPercent: -75 }, "label4");
    tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


    new ScrollMagic.Scene({
    triggerElement: "#js-wrapper2",
    triggerHook: "onLeave",
    duration: "400%"

    })
    .setPin("#js-wrapper2")
    .setTween(tl)
    .addIndicators({
      colorTrigger: "black",
      colorStart: "black",
      colorEnd: "black"
    })
    .addTo(controller);



</script>


<script type="text/javascript">
  gsap.defaultEase = Linear.easeNone;
    var titles = document.querySelectorAll(".sectionTitle");
    var controller = new ScrollMagic.Controller();
    var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


    
    
    // create timeline
    // this could also be created in a loop
    tl.to("#js-slideContainer4", {duration: 1, xPercent: 0 }, "label1");
    tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
    tl.to("#js-slideContainer4", {duration: 1, xPercent: -25 }, "label2");
    tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
    tl.to("#js-slideContainer4", {duration: 1, xPercent: -50 }, "label3");
    tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
    tl.to("#js-slideContainer4", {duration: 1, xPercent: -75 }, "label4");
    tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


    new ScrollMagic.Scene({
    triggerElement: "#js-wrapper3",
    triggerHook: "onLeave",
    duration: "400%"

    })
    .setPin("#js-wrapper3")
    .setTween(tl)
    .addIndicators({
      colorTrigger: "black",
      colorStart: "black",
      colorEnd: "black"
    })
    .addTo(controller);



</script>


<script type="text/javascript">
  gsap.defaultEase = Linear.easeNone;
    var titles = document.querySelectorAll(".sectionTitle");
    var controller = new ScrollMagic.Controller();
    var tl = gsap.timeline();


    // change behaviour of controller to animate scroll instead of jump

    //NOT WORKING FOR ME
    /*controller.scrollTo(function (newpos) {
      gsap.to(window, {duration: 0.5, scrollTo: {y: newpos}});
    });

    //  bind scroll to anchor links
    $(document).on("click", "a[href^='#']", function (e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });*/

    //GSAP scrollTo
    document.querySelectorAll(".stories a").forEach((trigger, index) => {
      trigger.addEventListener("click", () => {
        gsap.to(window, {duration: 1, scrollTo:{y:"#section" + (index + 1), offsetY:0}});
      });
    });


    
    
    // create timeline
    // this could also be created in a loop
    tl.to("#js-slideContainer5", {duration: 1, xPercent: 0 }, "label1");
    tl.from(titles[1], {duration: 0.5, opacity: 1 }, "label1+=0.5");
    tl.to("#js-slideContainer5", {duration: 1, xPercent: -25 }, "label2");
    tl.from(titles[2], {duration: 0.5, opacity: 1 }, "label2+=0.5");
    tl.to("#js-slideContainer5", {duration: 1, xPercent: -50 }, "label3");
    tl.from(titles[3], {duration: 0.5, opacity: 1 }, "label3+=0.5");
    tl.to("#js-slideContainer5", {duration: 1, xPercent: -75 }, "label4");
    tl.from(titles[4], {duration: 0.5, opacity: 1 }, "label4+=0.5");


    new ScrollMagic.Scene({
    triggerElement: "#js-wrapper4",
    triggerHook: "onLeave",
    duration: "400%"

    })
    .setPin("#js-wrapper4")
    .setTween(tl)
    .addIndicators({
      colorTrigger: "black",
      colorStart: "black",
      colorEnd: "black"
    })
    .addTo(controller);



</script>

<script type="text/javascript">
  var showChar = 256;
var ellipsestext = "...";
var moretext = "See More";
var lesstext = "See Less";
$('.comments-space').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

function validate_frm(obj){
  
  var flag = true;
  
  /*if($('#term').prop("checked") == false){
    
    $('#term_validate').html('Terms & condition field is required');
    flag = false;

  }else{
    $('#term_validate').html('');
  }
  
  if($('#contactmode1').prop("checked") == false && $('#contactmode2').prop("checked") == false && $('#contactmode3').prop("checked") == false){
    
    $('#contact_mode_validate').html('Contact mode field is required');
    flag = false;

  }else{
    $('#contact_mode_validate').html('');
  }*/
  
  return flag;
  
}
ajax_from_post({'form':'#enq_frm'});

function ajax_from_post(obj)
{
  
  $(obj.form).on('submit',(function(e) {    
        e.preventDefault();
    var frmObj = this;
    
    var jq_frmObj = $(frmObj);
    
    var ajax_submit_button=$("input[type='submit']");
      var button_val=$(ajax_submit_button,frmObj).val();
    
     if(!button_val)
     {
       var ajax_submit_button=$("input[type='image']");
     }
      
    
      
     $(ajax_submit_button).attr('disabled', 'disabled');    
    
          $.ajax({
            url: jq_frmObj.attr('action'),
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){ 
        
                    console.log(data);
                    data = JSON.parse(data);
        
                    if(data.st == 0)
                    {              
            $(ajax_submit_button).removeAttr("disabled");
            
             var ctp_url='';
                        
            /*if(typeof(captcha_url)=='string')
            { ctp_url=captcha_url;  
            }           
            else            
            {           
             ctp_url=$("img[src*='captcha']").attr('src');            
            }           
            if(ctp_url)
            {
            
              if(ctp_url.indexOf('?')>0)
              {             
                ctp_url=ctp_url.substr(0,ctp_url.indexOf('?'));             
              }
                            
              ctp_url=ctp_url+'?'+Math.random();            
              $("img[src*='captcha']").attr('src',ctp_url);           
              $('input[name=verification_code]').val('');
            }*/
            
                  
                        $( ".required",frmObj ).remove();
                        var data1    = JSON.parse(data.msg); 
                        $(':input',frmObj).each(function(){                            
                                        var elementName = $(this).attr('name');        
                                        var message = data1[elementName];
                                        if(message){
                                        var element = $('<div>' + message + '</div>')
                                                        .attr({
                                                            'class' : 'required'
                                                        })
                                                        .css ({
                                                            display: 'none'
                                                        });
                                        $(this).after(element);
                     if(frmObj.id=='enq_frm'){
                    /*  if($('#term').prop("checked") == false){   
                        $('#term_validate').html('Terms & condition field is required');
                      }
                      if($('.contactmode').prop("checked") == false){  
                      
                        $('.contact_mode_validate').html('Contact mode field is required');
                      }*/
                     }
                                        $(element).fadeIn();
}
                        });
                    }

                    if(data.st == 1)
                    {            
            $(".required").remove();
            $(ajax_submit_button).removeAttr("disabled");           
            if(data.ref!='')
            {             
             window.location.href=data.ref;
                                 
            }else
            {            
              $('#error_msg',frmObj).html(data.msg);
              frmObj.reset();
              
            } 
                      
                    }
            },
            error: function(){}             
        });
    }));
  
}  
</script>


<script src="<?php echo resource_url();?>Scripts/script.int.dg.js"></script>
</body>
</html>
