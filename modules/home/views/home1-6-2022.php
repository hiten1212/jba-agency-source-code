<?php $this->load->view("top_application");?>
<?php //$this->load->view('header_images/top_header_images');?>


<div class="main-slider-top">
     <a class="navbar-brand" href="index.php"><h1><img src="<?php echo theme_url(); ?>images/logo.png" alt="" title=""></h1></a>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12 co-sm-12 pad-0">
                <div class="slider-area">
                    <div class="sloganmiddle">
                        <h1>Bringing Ideas To Life</h1>
						<?php 

 $condtion_array = array(
   'field' =>"category_id,category_name,parent_id,friendly_url,(SELECT COUNT(category_id) FROM wl_service_categories AS b WHERE b.parent_id=a.category_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 6;
   $res = $this->service_category_model->getcategory($condtion_array);
   $total_categories	=  $this->service_category_model->total_rec_found;
   if($total_categories > 0){
?>
                        <ul>
                            <?php
	foreach($res as $val){
	 //$link_url = site_url($val['friendly_url']);
	  $link_url = site_url().'#Services';//.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
<li><a  href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></li>
<?php }
if($total_categories >6)
{
?>
<li><a  href="<?php echo base_url('category');?>" title="View All &raquo;"><b>View All &raquo;</b></a></li>
<?php } ?>
                        </ul>
   <?php } ?>
                    </div>
                    <div class="slider-fluid">
                        <div class="mainslider_js slider_dots owl-theme">
                            <div>
                                <video width="" muted="" autoplay="" loop="">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.mp4" type="video/mp4">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.ogg" type="video/ogg">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>

                            <div><img src="<?php echo theme_url(); ?>images/slider-img4.jpg"></div>
                            
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>

    

</div>


<section class="commondiv sevice" style=" padding: 100px 0px;" id="Services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Services</h3>
            </div>
        </div>
    </div>
</section>

<main>
 
    <?php $category_name = get_db_field_value('wl_service_categories','category_name'," AND parent_id = '0' ");
$condtion_array = array(
'field' =>"*,(SELECT COUNT(category_id) FROM wl_service_categories AS b
WHERE b.parent_id=a.category_id ) AS total_subcategories",
'condition'=>"AND parent_id = '$parent_id' AND status='1'",
'order'=>'sort_order',
'debug'=>FALSE
);
$condtion_array['offset'] = 0;
$condtion_array['limit'] = 5;
$res = $this->service_category_model->getcategory($condtion_array);
$total_categories	=  $this->service_category_model->total_rec_found;
$head_category_array = array();
if($total_categories > 0){
	
      foreach($res as $val){
		  array_push($head_category_array,$val);
		  $subcategory_id=$val['category_id'];		
			 $where                   = "wlp.status ='1' ";
		     $condition['category_id'] = $val['category_id'];
			 $condition['where'] = $where;
		
		
		$res_array               =  $this->services_model->get_services($config['per_page'],$offset,$condition);
		
		$total_product=get_found_rows();
		if($total_product > 0){	
	$sub_category_array[$subcategory_id] = array();
      foreach($res_array as $val2){ 
          $total_subcategories = $val['total_subcategories'];         
          $sel = $val['friendly_url']; 
		  array_push($sub_category_array[$subcategory_id],$val2);
		  
		  
	  }
}
		 
		
	  }
	  }
	  
	  //trace($head_category_array);
//echo "<pre>";
//trace($sub_category_array);
/*
<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="box_design_thee">
					<div class="row">
						<div class="col-md-5 pd-r0">
						<?php if(is_array($sub_category_array[$head_category_array[0]['category_id']][0]) && !empty($sub_category_array[$head_category_array[0]['category_id']][0])){?>
							<div class="box_de__">
								<img src="<?php echo get_image('category',$sub_category_array[$head_category_array[0]['category_id']][0]["category_image"],'450','253');?>" alt="<?php echo $sub_category_array[$head_category_array[0]['category_id']][0]['category_name'];?>"  >
								<a href="<?php echo site_url($sub_category_array[$head_category_array[0]['category_id']][0]['friendly_url']);?>"><span><?php echo $sub_category_array[$head_category_array[0]['category_id']][0]['category_name'];?></span></a>
							</div>
						<?php } ?>
							<?php if(is_array($sub_category_array[$head_category_array[0]['category_id']][1]) && !empty($sub_category_array[$head_category_array[0]['category_id']][1])){?>
							<div class="box_de__">
								<img src="<?php echo get_image('category',$sub_category_array[$head_category_array[0]['category_id']][1]["category_image"],'450','253');?>" alt="<?php echo $sub_category_array[$head_category_array[0]['category_id']][1]['category_name'];?>"  >
								<a href="<?php echo site_url($sub_category_array[$head_category_array[0]['category_id']][1]['friendly_url']);?>"><span><?php echo $sub_category_array[$head_category_array[0]['category_id']][1]['category_name'];?></span></a>
							</div>
							<?php } ?>
							<?php if(is_array($sub_category_array[$head_category_array[0]['category_id']][2]) && !empty($sub_category_array[$head_category_array[0]['category_id']][2])){?>
							<div class="box_de__">
								<img src="<?php echo get_image('category',$sub_category_array[$head_category_array[0]['category_id']][2]["category_image"],'450','253');?>" alt="<?php echo $sub_category_array[$head_category_array[0]['category_id']][2]['category_name'];?>"  >
								<a href="<?php echo site_url($sub_category_array[$head_category_array[0]['category_id']][2]['friendly_url']);?>"><span><?php echo $sub_category_array[$head_category_array[0]['category_id']][2]['category_name'];?></span></a>
							</div>
							<?php } ?>
						</div>

						<div class="col-md-7">
						<?php if(is_array($head_category_array[0]) && !empty($head_category_array[0])){?>
							<div class="box_de__big">
								<img src="<?php echo get_image('category',$head_category_array[0]["category_image"],'450','253');?>" alt="<?php echo $head_category_array[0]['category_name'];?>"  >
								<a href="<?php echo site_url($head_category_array[0]['friendly_url']);?>"><span><?php echo $head_category_array[0]['category_name'];?></span></a>
							</div>
						<?php } ?>
							<div class="row">
							<?php if(is_array($sub_category_array[$head_category_array[0]['category_id']][3]) && !empty($sub_category_array[$head_category_array[0]['category_id']][3])){?>
								<div class="col-md-6">
									<div class="box_de__">
										<img src="<?php echo get_image('category',$sub_category_array[$head_category_array[0]['category_id']][3]["category_image"],'450','253');?>" alt="<?php echo $sub_category_array[$head_category_array[0]['category_id']][3]['category_name'];?>"  >
								<a href="<?php echo site_url($sub_category_array[$head_category_array[0]['category_id']][3]['friendly_url']);?>"><span><?php echo $sub_category_array[$head_category_array[0]['category_id']][3]['category_name'];?></span></a>
									</div>
								</div>
							<?php } ?>
								<?php if(is_array($sub_category_array[$head_category_array[0]['category_id']][4]) && !empty($sub_category_array[$head_category_array[0]['category_id']][4])){?>
								<div class="col-md-6">
									<div class="box_de__">
										<img src="<?php echo get_image('category',$sub_category_array[$head_category_array[0]['category_id']][4]["category_image"],'450','253');?>" alt="<?php echo $sub_category_array[$head_category_array[0]['category_id']][4]['category_name'];?>"  >
								<a href="<?php echo site_url($sub_category_array[$head_category_array[0]['category_id']][4]['friendly_url']);?>"><span><?php echo $sub_category_array[$head_category_array[0]['category_id']][4]['category_name'];?></span></a>
									</div>
								</div>
<?php } ?>
							</div>
						</div>

					</div>
				</div>
			</div>
*/
		  ?>
    
    
    <div class="wrapper" id="js-wrapper">
        <ul class="stories">
      <li><a class="trigger11" href="#story1">Scene 1</a></li>
            <li><a class="trigger22" href="#story2">Scene 2</a></li>
            <li><a class="trigger33"href="#story3">Scene 3</a></li>
            <li><a class="trigger44"href="#story4">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer">
          
      
        <section class="section bg_1 combg" id="story1">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Creative</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            
    
    

                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_2 combg" id="story2">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_3 combg" id="story3">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_4 combg" id="story3">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Creative</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>


  <div class="wrapper" id="js-wrapper1">
        <ul class="stories">
      <li><a class="trigger1" href="#story11">Scene 1</a></li>
            <li><a class="trigger2" href="#story22">Scene 2</a></li>
            <li><a class="trigger3"href="#story33">Scene 3</a></li>
            <li><a class="trigger3"href="#story44">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer1">
          
      
        <section class="section bg_brnad_1 combg" id="story11">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Branding</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_brnad_2 combg" id="story22">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_brnad_3 combg" id="story33">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_brnad_4 combg" id="story33">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Branding</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    

<style type="text/css">


.wrapper {
  width: 100%;
  height: 100vh !important;
  overflow: hidden;
}
.wrapper .scroll-title {
  font-size: 21px;
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
  font-weight: 100;
}
.wrapper .cta {
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  display: block;
  z-index: 20;
  list-style: none;
  width: auto;
  padding: 0;
  color: white;
}
.wrapper ul.stories {

    position: absolute;
    top: 50%;
    right: 20px;
    transform: translate(-50%, 0);
    display: block;
    z-index: 20;
    list-style: none;
    width: auto;
    padding: 0;
}
.wrapper ul.stories li {
   display: none;
}
.wrapper ul.stories li a {
    color: white;
    font-size: 0px;
    background: #f36f21;
    width: 12px;
    height: 12px;
    display: block;
    margin: 10px 0;
    border-radius: 50%;
}
.wrapper .progress-bar-container {
  max-width: 300px;
  height: 10px;
  border: #f1f0f0;
  border-radius: 10px;
  position: absolute;
  bottom: 100px;
  z-index: 20;
  display: table;
  overflow: hidden;
  width: 100%;
  background: #222;
  margin: 0 auto;
  left: 50%;
  right: 0;
  transform: translate(-50%, 0);
}
.wrapper .progress-bar-container .progress-bar {
  width: 0%;
  height: 20px;
  position: absolute;
  left: 0;
  top: 0;
  background: red;
  transition: all 0.3s ease;
}
.wrapper .sections {
  width: 400%;
  height: 100%;
}
.wrapper .sections .section {
  height: 100%;
  width: calc(100% / 4);
  float: left;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
}

.wrapper .sections .section .sectionTitle {
  font-size: 30px;
  color: #fff;
  position: absolute;
  z-index: 5;
}
</style>




  <div class="wrapper" id="js-wrapper2">
        <ul class="stories">
      <li><a class="trigger1" href="#story12">Scene 1</a></li>
            <li><a class="trigger2" href="#story23">Scene 2</a></li>
            <li><a class="trigger3"href="#story34">Scene 3</a></li>
            <li><a class="trigger3"href="#story45">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer2">
          
      
        <section class="section bg_media_1 combg" id="story12">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Media</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_media_2 combg" id="story23">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_media_3 combg" id="story34">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_media_4 combg" id="story35">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Media</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    

  <div class="wrapper" id="js-wrapper3">
        <ul class="stories">
      <li><a class="trigger1" href="#story13">Scene 1</a></li>
            <li><a class="trigger2" href="#story24">Scene 2</a></li>
            <li><a class="trigger3"href="#story35">Scene 3</a></li>
            <li><a class="trigger3"href="#story46">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer3">
          
      
        <section class="section bg_events_1 combg" id="story13">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Events</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_events_2 combg" id="story23">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_events_3 combg" id="story34">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_events_4 combg" id="story35">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Events</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    



  <div class="wrapper" id="js-wrapper4">
        <ul class="stories">
      <li><a class="trigger1" href="#story13">Scene 1</a></li>
            <li><a class="trigger2" href="#story24">Scene 2</a></li>
            <li><a class="trigger3"href="#story35">Scene 3</a></li>
            <li><a class="trigger3"href="#story46">Scene 4</a></li>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer4">
          
      
        <section class="section bg_webs_1 combg" id="story14">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3>Webs & Application</h3>
                            <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                        </div>
                    </div>
                </div>
            </div>       
        </section>


          <section class="section bg_webs_2 combg" id="story25">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>


          <section class="section bg_webs_3 combg" id="story36">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>

          <section class="section bg_webs_4 combg" id="story37">
                <div class="container he100">
                    <div class="row he100">
                        <div class="col-md-12 col-xs-12 col-sm-12 he100">
                            <div class="services_ccco">
                                <h3>Webs & Application</h3>
                                <p><div class='comments-space'>Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.
                            Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social. Produced high-quality creative assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social media. assets at scale, promoting personalization, individuality, and joyful living for the leading OEM's social.</div></p>
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          
          
        </div>
        
  </div>
    



<?php /*?><section class="commondiv sevice" style=" padding: 100px 0px;" id="Services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Services</h3>
            </div>
        </div>
    </div>
</section>

<main>
 
    
<?php $category_name = get_db_field_value('wl_service_categories','category_name'," AND parent_id = '0' ");
$condtion_array = array(
'field' =>"*,(SELECT COUNT(category_id) FROM wl_service_categories AS b
WHERE b.parent_id=a.category_id ) AS total_subcategories",
'condition'=>"AND parent_id = '$parent_id' AND status='1'",
'order'=>'sort_order',
'debug'=>FALSE
);
$condtion_array['offset'] = 0;
$condtion_array['limit'] = 25;
$res = $this->service_category_model->getcategory($condtion_array);
$total_categories	=  $this->service_category_model->total_rec_found;
if($total_categories > 0){	
$counter='1';
      foreach($res as $val){ 
          $total_subcategories = $val['total_subcategories'];         
          $sel = $val['friendly_url'];  
		  $wraper=$counter==1?'':$counter-1;
		  ?> 
    
    <div class="wrapper" id="js-wrapper<?php echo $wraper;?>">
        <ul class="stories">
		<?php $where                   = "wlp.status ='1' ";
		     $condition['category_id'] = $val['category_id'];
			 $condition['where'] = $where;
		
		
		$res_array               =  $this->services_model->get_services($config['per_page'],$offset,$condition);
		
		$total_product=get_found_rows();
		if($total_product > 0){	
		 $seg2=$this->uri->segment(1);
		 $j=1;
		 foreach($res_array as $val2){
		 ?>
      <li><a class="trigger<?php echo $j;?>" href="#story<?php echo $counter;?><?php echo $j;?>">Scene <?php echo $j;?></a></li>
            
		 <?php $j++;} ?>
        </ul>
    
      
    
        <div class="sections" id="js-slideContainer<?php echo $counter;?>">
          
       <?php $i=1;
		 foreach($res_array as $val2){
			$link_url = site_url($val2['friendly_url']);		
		$prodImage=get_image('services',$val2["media"],'1300','867'); 			
		 	$sel = $this->uri->rsegment(3);   ?>     
        <section class="section combg" style="background: url(<?php echo $prodImage;?>) no-repeat;" id="story<?php echo $counter;?><?php echo $i;?>">     
            <div class="container he100">
                <div class="row he100">
                    <div class="col-md-12 col-xs-12 col-sm-12 he100">
                        <div class="services_ccco">
                            <h3><?php echo $val['category_name'];?></h3>
                            <p>
						<?php if(!empty($val2['service_description'])){?>
						<div class='comments-space'><?php echo strip_tags(html_entity_decode($val2['service_description']));?></div>
						<?php } ?>
							</p>
                            
    
    

                        </div>
                    </div>
                </div>
            </div>       
        </section>
		
		 <?php $i++; }?>


          
          
        </div>
		<?php } ?>
        
  </div>
  <?php $counter++;
	  } ?>
<?php
}  ?><?php */?>

  








<?php
			if(is_array($brand_array) && !empty($brand_array) ){?>
<section class="commondiv" id="Clients">
    
                <div class="heading text-center">
                    <h3>Clients</h3>
                </div>

                <div class="slider_effect">
                    <div class="width_lll">
					
                        <div class="outlogoidv">
						<?php $j=1;$k='';
if(is_array($brand_array) && !empty($brand_array) ){
	$counter=1;
	foreach($brand_array as $val){
		$link_url=site_url($val['friendly_url']);	
		$brandImage=get_image('brand',$val["brand_image"],'110','47');
		?>
                            <div class="slider_loo"><img src="<?php echo $brandImage;?>"></div>
					<?php if($counter%7==0){
						if($j==1)
						{
						echo ' </div></div><div class="width_lll_'.$j.'"><div class="outlogoidv">';
						$j='';
						}
						else{echo ' </div></div><div class="width_lll'.$k.'"><div class="outlogoidv">';}
						}
					$counter++;} 

						} ?>
                           
                        </div>
                       
                    </div>
                </div>

             
</section>
			<?php } ?>
<?php
			if(is_array($gallery_array) && !empty($gallery_array) ){?>
<section class="commondiv" style="background-color: #000; padding: 100px 0px;" id="Portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h3 class="service_tag">Portfolio</h3>
                <br>
            <br>
            <br>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="works">
                <?php foreach($gallery_array as $val){
          // $cls = ( $ix==0 )	 ? "cat-w fl" : "cat-w fl ml10" ;				
        $id=$val['gallery_id'];
        $total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
        $link_url = base_url().$val['friendly_url'];		   
        ?>
                    <div>
                        <a href="<?php echo $link_url;?>" class="work_mg">
                            <img src="<?php echo get_image('gallery',$val['gallery_image'],'550','380','R'); ?>">
                            <span><?php echo $val['gallery_title'];?></span>
                        </a>
                    </div>
					
				<?php } ?>
                    
                
                </div>
            </div>
        </div>
    </div>
</section>
			<?php } ?>


<section class="commondiv" >
   
<div class="content-row row_padding_top light-section">
                                
 <!-- <a href="#footer" class="span_contact">
      <div class="inner_spancon"><div class="arrow_svg">
        <img src="<?php echo theme_url(); ?>images/arrowicon.png">
      </div>
       get in touch
    </div>
  </a>-->
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
    
    
    <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-backward secondary-font">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
     <div class="title-moving-outer has-animation">
        <h1 class="big-title title-moving-forward">Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together Let's work together

</h1>
    </div>
    
</div> 
</section>

<?php $this->load->view("bottom_application");?>
    
