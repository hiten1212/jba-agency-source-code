<?php $this->load->view("top_application");?>
<?php $this->load->view('header_images/top_header_images');?>

<div class="clearfix"></div>
</div>
<div class="projects_section">
  <div class="container">
    <div class="hm_cate_list">
      <div class="row">
        <div class="col-lg-6">
          <div class="tab-content">
            <h2>Deliver superior customer<br>
              interactions anytime, anywhere.<span></span></h2>
            <p class="cat-ttext"> Engage your customers in meaningful conversations with Sirius. Deploy domain-trained, highly customizable assistants to kickstart your digital transformation. <br>
              <br>
              Sirius adapts to all your needs right from adopting any IT 
              ecosystem to building beautiful conversational flows for your people.</p>
            <p class="clearfix"></p>
          </div>
        </div>
        <div class="col-lg-6"> <img src="<?php echo theme_url(); ?>images/deliver.jpg" class="img-fluid d-block mx-auto" alt=""></div>
      </div>
    </div>
  </div>
</div>
<div class="projects_section projects_section-1">
  <div class="container">
    <div class="hm_cate_list">
      <div class="row">
        <div class="col-lg-6"> <img src="<?php echo theme_url(); ?>images/proj-2.jpg" class="img-fluid d-block mx-auto" alt=""></div>
        <div class="col-lg-6">
          <div class="value">
            <div class="value-box">
              <div class="value-box-in">Chatbot market is projected to reach <strong>USD 102.29 billion</strong> by <strong>2026.</strong></div>
            </div>
          </div>
          <div class="value">
            <div class="value-box">
              <div class="value-box-in">According to Salesforce, <strong>69%</strong> of consumers prefer to use chatbots for the speed at which they can communicate with a brand. </div>
            </div>
          </div>
          <div class="value">
            <div class="value-box">
              <div class="value-box-in">According to MIT review, <strong>90%</strong> of the businesses reported faster complaints resolution with the bots. </div>
            </div>
          </div>
          <div class="value">
            <div class="value-box">
              <div class="value-box-in">According to Accenture Digital, <strong>57%</strong> of businesses claimed that chatbot delivers large return on investment on minimal investment.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="auto_section">
  <div class="container">
    <h2>Automate and streamline customer journeys <span></span></h2>
    <div class="hm_cate_list mt-4">
      <ul id="cate_scroll" class="owl-carousel owl-theme">
        <li class="item animated2 infinite fadeInUpDown">
          <div class="cate_w">
            <div class="cate_des">
              <p class="icon-sol"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M438.6 105.4C451.1 117.9 451.1 138.1 438.6 150.6L182.6 406.6C170.1 419.1 149.9 419.1 137.4 406.6L9.372 278.6C-3.124 266.1-3.124 245.9 9.372 233.4C21.87 220.9 42.13 220.9 54.63 233.4L159.1 338.7L393.4 105.4C405.9 92.88 426.1 92.88 438.6 105.4H438.6z" fill="#ffffff"/>
                </svg></p>
              <p class="cate_ttl-text">Understand the true meaning behind each customer’s request—delivering the right response at the right time. </p>
            </div>
          </div>
        </li>
        <li class="item animated3 infinite fadeInUpDown">
          <div class="cate_w">
            <div class="cate_des">
              <p class="icon-sol"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M438.6 105.4C451.1 117.9 451.1 138.1 438.6 150.6L182.6 406.6C170.1 419.1 149.9 419.1 137.4 406.6L9.372 278.6C-3.124 266.1-3.124 245.9 9.372 233.4C21.87 220.9 42.13 220.9 54.63 233.4L159.1 338.7L393.4 105.4C405.9 92.88 426.1 92.88 438.6 105.4H438.6z" fill="#ffffff"/>
                </svg></p>
              <p class="cate_ttl-text">Deploy quickly and easily across numerous messaging channels and the web, giving customers a familiar experience. </p>
            </div>
          </div>
        </li>
        <li class="item animated2 infinite fadeInUpDown">
          <div class="cate_w">
            <div class="cate_des">
              <p class="icon-sol"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M438.6 105.4C451.1 117.9 451.1 138.1 438.6 150.6L182.6 406.6C170.1 419.1 149.9 419.1 137.4 406.6L9.372 278.6C-3.124 266.1-3.124 245.9 9.372 233.4C21.87 220.9 42.13 220.9 54.63 233.4L159.1 338.7L393.4 105.4C405.9 92.88 426.1 92.88 438.6 105.4H438.6z" fill="#ffffff"/>
                </svg></p>
              <p class="cate_ttl-text">Customers don’t have to repeat themselves, improving customer satisfaction (CSAT) and Net Promoter Score (NPS) results. </p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Product Section start --> 

<!-- Projects Section start -->
<div class="deploy_section">
  <div class="container">
    <h2>Deploy intelligent,
      <p>customer-centric chatbots</p>
      <span></span></h2>
    <div class="row">
      <div class="col-lg-4 col-sm-6">
        <div class="proj_we">
          <p class="sec1 animated2 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-1.jpg" alt=""></p>
          <p class="sec2">1. Build once, deploy anywhere</p>
          <p class="clearfix"></p>
          <p class="text">Build conversation flows once and deploy them across any channel in seconds. </p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="proj_we">
          <p class="sec1 animated3 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-2.jpg" alt=""></p>
          <p class="sec2">2. Update on the fly</p>
          <p class="clearfix"></p>
          <p class="text">Fine-tune conversations to keep them up to date and engaging.</p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="proj_we">
          <p class="sec1 animated2 infinite fadeInUpDown"><img src="<?php echo theme_url(); ?>images/pro-icon-3.jpg" alt=""></p>
          <p class="sec2">3. Empower your end-user</p>
          <p class="clearfix"></p>
          <p class="text">Build chatbots for all your customer needs in one place.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Projects Section end --> 

<!-- pro feature Section start -->
<div class="feature_section">
  <div class="container">
    <h2>Product features <span></span></h2>
    <div class="row">
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-1.jpg" alt=""></p>
          <p class="feature-title">Omnichannel capabilities</p>
          <p class="feature-text">Deploy Sirius across social media channels or integrate it with voice assistants. </p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-2.jpg" alt=""></p>
          <p class="feature-title">Intent prediction</p>
          <p class="feature-text">Understand more than what’s being said.</p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-3.jpg" alt=""></p>
          <p class="feature-title">Vertical ontologies</p>
          <p class="feature-text">Cut down deployment time with a library of prebuilt intents.</p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-4.jpg" alt=""></p>
          <p class="feature-title">Sentiment dectection </p>
          <p class="feature-text">Detect when a customer is impatient with the chat flow.</p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-5.jpg" alt=""></p>
          <p class="feature-title">API integrations</p>
          <p class="feature-text">Connect to data sources to pull relevant customer information into the conversation. </p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="feature-box">
          <p class=""><img src="<?php echo theme_url(); ?>images/feat-6.jpg" alt=""></p>
          <p class="feature-title">Voice deployments</p>
          <p class="feature-text">Create, test and fine-tune utterances to clarify the messages.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- pro feature Section end --> 

<!-- go conversational Section start -->
<div class="conver_section">
  <div class="container">
    <h2>Go conversational with Sirius <span></span></h2>
    <div class="row">
      <div class="col-lg-6">
        <ul class="conver">
          <li>Keeps getting better </li>
          <li>Comes with laser-sharp accuracy</li>
          <li>Speaks your customer’s language </li>
          <li>Generates leads</li>
          <li>Engages with employees</li>
          <li>Helps in BI reporting</li>
        </ul>
        <p class="mt-5"><a href="<?php echo base_url('contactus');?>" title="Request a demo" class="vw-button">Request a demo <i class='fas fa-arrow-right'></i></a></p>
      </div>
      <div class="col-lg-6"> <img src="<?php echo theme_url(); ?>images/conver-image.png" class="img-fluid d-block mx-auto mt-4" alt=""></div>
    </div>
  </div>
</div>
<!-- go conversational Section end --> 

<!--counter-->
<div class="counter">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4">
        <h2>Put Sirius to
          <p>good use</p>
          <span></span></h2>
      </div>
      <div class="col-lg-8 col-md-8">
        <p class="c-text">Delight your employees with self-service support on-demand and personalized conversations. <br>
          <br>
          Automate interactions to improve Net Promoter Score or free your HR and IT teams from routine chores so they can focus on  priority tasks.</p>
      </div>
    </div>
  </div>
</div>
<!--counter end--> 

<!-- industries Section start -->
<div class="client-section">
  <div class="container">
    <div class="indus">
      <div class="tab_linkd tb_links"> <a href="#tb1s" class="act" title="Industries"><span></span> Industries </a> <a href="#tb2s" title="Functions"><span></span> Functions</a> </div>
      <p class="clearfix"></p>
      <div class="tab_conts" id="tb1s">
        <div class="owl-carousel owl-theme mt-1 owl-industries">
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-university"></i></p>
              <p class="tb-title">Banking</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-cash-register"></i></p>
              <p class="tb-title">Retail</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-hand-holding-heart"></i></p>
              <p class="tb-title">Insurance</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-broadcast-tower"></i></p>
              <p class="tb-title">Telecom</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-heartbeat"></i></p>
              <p class="tb-title">Healthcare</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-store-alt"></i></p>
              <p class="tb-title">Mall</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-shopping-bag"></i></p>
              <p class="tb-title">Luxury Retail</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-plane-departure"></i></p>
              <p class="tb-title">Airport</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-school"></i></p>
              <p class="tb-title">Education</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-luggage-cart"></i></p>
              <p class="tb-title">Travel and Hospitality</p>
            </div>
          </div>
        </div>
      </div>
      <div class="tab_conts tab_hides" id="tb2s">
        <div class="owl-carousel owl-theme mt-1 owl-industries">
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fab fa-sellsy"></i></p>
              <p class="tb-title">Sales</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-bullhorn"></i></p>
              <p class="tb-title">Marketing</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-donate"></i></p>
              <p class="tb-title">Finance</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-laptop-code"></i></p>
              <p class="tb-title">IT</p>
            </div>
          </div>
          <div class="item">
            <div class="attrc_box">
              <p class="tb-icon"><i class="fas fa-headset"></i></p>
              <p class="tb-title">HR Support</p>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<!-- industries Section end --> 

<!-- proven Section start -->
<div class="proven">
  <div class="container">
    <div class="prov-bg">
      <div class="pro-circle">
        <h2>Proven results for the future of customer experience <span></span></h2>
        <p class="text-center"><img src="<?php echo theme_url(); ?>images/hand.jpg" alt=""></p>
      </div>
      <div class="pro-left">
        <div class="prs-1">
          <p class="pr-bl2">01</p>
          <div class="pr-bl1">
            <p class="pr01">Intent prediction </p>
            <p class="pr02">Understand consumer intent through behavioral analysis and NLU </p>
          </div>
        </div>
        <div class="prs-2">
          <p class="pr-bl2">02</p>
          <div class="pr-bl1">
            <p class="pr01">Data-driven analytics </p>
            <p class="pr02">Bring advanced analytical insights to maximize efficiency and improve response accuracy. </p>
          </div>
        </div>
      </div>
      <div class="pro-right">
        <div class="pls-1">
          <p class="pl-bl2">03</p>
          <div class="pl-bl1">
            <p class="pr01">Visual content </p>
            <p class="pr02">Provide rich, visual content to enhance customer engagement and improve CSAT. </p>
          </div>
        </div>
        <div class="pls-2">
          <p class="pl-bl2">04</p>
          <div class="pl-bl1">
            <p class="pr01">Emotion interpretation </p>
            <p class="pr02">Predict a customer’s need to speak with an expert by seamlessly escalating to a human agent. </p>
          </div>
        </div>
      </div>
      <p class="clearfix"></p>
    </div>
    <div class="countern">
      <div id="counter">
        <ul>
          <li>
            <div class="num counter-value" data-count="95">95
              <p>%</p>
            </div>
            <div class="sub-texts">success rate</div>
          </li>
          <li>
            <div class="num counter-value" data-count="5">5
              <p>X</p>
            </div>
            <div class="sub-texts">user engagement</div>
          </li>
          <li>
            <div class="num counter-value" data-count="78">78
              <p>%</p>
            </div>
            <div class="sub-texts">drop in call center traffic</div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- proven Section end --> 

<!--Get Section-->
<div class="get">
  <div class="container">
    <div class="get-bg">
      <div class="gets">
        <h2>Get started with conversational AI today<span></span></h2>
        <p class="g-text">Enterprises of all sizes across the world are seeing the powerful impact of conversational AI in customer and employee service. Click below to start your conversational AI journey with us.</p>
        <p class="clearfix"></p>
        <p><a href="<?php echo base_url('contactus');?>" class="vw-button1" title="Talk to an expert">Talk to an expert</a> <a href="<?php echo base_url('contactus');?>" class="vw-button2" title="">Start with Sirius</a></p>
      </div>
    </div>
  </div>
</div>
<!--Get Section End-->

<div class="clearfix"></div>

<<?php $this->load->view("bottom_application");?>