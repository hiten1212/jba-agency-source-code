
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="fav.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Panymoc</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo theme_url(); ?>css/ak.css">

<link rel="stylesheet" href="<?php echo theme_url(); ?>css/conditional_ak.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css" media="screen">
<!--
@import url("<?php echo resource_url(); ?>fancybox/jquery.fancybox.css");
-->
</style>
</head>
<body style="background:#232f3e;">
<div class="p10" style="height:275px; border:0px solid #000;">
<div class="ac">
	<p class="text-center mt20"><img src="<?php echo theme_url();?>images/celex-wine-collections.jpg"></p>
    <div class="cb"></div>
    <div class="dob">

  <p class="fs16 text-uppercase black weight600 pb8">18 years old buyer can purchase our product. </p>    
  <p class="fs16 text-uppercase black">ENTER YOUR DATE OF BIRTH</p>
  
  <p class="mt10"> <select name="" id="birthdate"></select>
  
  <select name="" id="birthmonth">
    <option value="">MM</option>
    <option value="1">January</option>
    <option value="2">February</option>
    <option value="3">March</option>
    <option value="4">April</option>
    <option value="5">May</option>
    <option value="6">June</option>
    <option value="7">July</option>
    <option value="8">August</option>
    <option value="9">September</option>
    <option value="10">October</option>
    <option value="11">November</option>
    <option value="12">December</option>
  </select> 
  
  <select name="" id="birthyear"></select> 
  
  <!-- <input type="button" value="Enter" class="enter_btn" onClick="window.open('home.htm','_parent')"> -->
  <input type="button" value="Enter" class="enter_btn" onClick="validateUser()">
  </p> </p>
  </div>
    
</div>
</div>
<script>
	var d = new Date();
    var n = d.getFullYear();
	var str	=	'<option value="">YYYY</option>';
	for(var year=n;year>1940;year--){
		str	+=	'<option value="'+year+'">'+year+'</option>';
	}
	document.getElementById("birthyear").innerHTML = str;
	
	var daystr	=	'<option value="">DD</option>';
	for(var day=1;day<32;day++){
		daystr	+=	'<option value="'+day+'">'+day+'</option>';
	}
	document.getElementById("birthdate").innerHTML = daystr;
	
	function validateUser(){
		var birthday	=	document.getElementById("birthdate").value;
		var birthmonth	=	document.getElementById("birthmonth").value;
		var birthyear	=	document.getElementById("birthyear").value;
		
		if((birthyear!='')&(birthmonth!='')&(birthdate!='')){
			var age = 18;
			var mydate = new Date();
			mydate.setFullYear(birthyear, birthmonth-1, birthday);
			
			var currdate = new Date();
			var setDate = new Date();         
			setDate.setFullYear(mydate.getFullYear() + age, birthmonth-1, birthday);
			
			if ((currdate - setDate) > 0){
			<?php 
				$this->session->set_userdata('dgopener','yes');
				?>
			window.open('<?php echo @$_SERVER['HTTP_REFERER'];?>','_parent')
				//window.location.href="<?php echo @$_SERVER['HTTP_REFERER'];?>";
				
			}else{
				alert("You Dont't Have Legal  Age.");
			}
		}else{
			alert("Please fill all the fields.");
		}
	}
	</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script> 
<script type="text/javascript">var Page='';</script> 
<script type="text/javascript" src="<?php echo resource_url();?>Scripts/script.int.dg.js"></script>
</body>
</html>