<?php $this->load->view("top_application");

?>
<?php $this->load->view("banner/inner_page_top_banner");	 ?>
<?php echo navigation_breadcrumb("blog");?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   }</script>

<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1>Blog</h1>
<?php
if(is_array($res) && !empty($res)){
		echo form_open("",'id="myform" method="post" ');
		?>
	<input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
	<input type="hidden" name="offset" value="0">				
	<input type="hidden" name="categories" id="category" value="<?php echo $this->input->post('categories');?>" />				
	<?php
	if($this->uri->rsegment(1)=="category" && $this->uri->rsegment(2)=="index" ){
		?>
		<input type="hidden" name="category_id" id="category_id" value="<?php echo $this->uri->rsegment(3);?>" />
		<?php
	}else{
		?>
		<input type="hidden" name="category_id" id="category_id" value="<?php echo $this->input->get_post('category_id');?>" />
		<?php
	}
	?>
	<?php echo form_close();?> 
<div class="w-100 mt-5 mb-4">
<ul class="float" id="">

 <?php $data = array('res'=>$res);
            $this->load->view('blogs/blog_data',$data);?>  

<?php /*?><p class="mt-4 text-center"><img src="images/loader.gif" alt=""></p><?php */?>
</ul>
<?php
		}else{
			?>
			<p class="mt-3 text-center"><strong><?php echo $this->config->item('no_record_found');?></strong></p>
			<?php
}?>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="clearfix"></div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>
<script type="text/javascript">

function get_likes(blog_id){  
	
 var ajax_url='<?php echo base_url().'blogs/get_likes'?>'; 
 $.ajax({
			type: "POST",
			url: ajax_url,
			dataType: "html",			
			data: {blog_id: blog_id},
			cache:false,
			success:
				function(data)
				{					
					//if(data=='sucess'){											
						$('#likeId'+blog_id).html(data);
					//}
					
				}				
	});  
}
</script>