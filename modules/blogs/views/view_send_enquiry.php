<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no;user-scalable=0;"/>
<?php
$meta_rec = $this->meta_info;
if( is_array($meta_rec) && !empty($meta_rec) ){
	?>
	<title><?php echo $meta_rec['meta_title'];?></title>
	<meta name="description" content="<?php echo $meta_rec['meta_description'];?>" />
	<meta  name="keywords" content="<?php echo $meta_rec['meta_keyword'];?>" />
	<?php
}?>
<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico"/>
<link href="<?php echo base_url(); ?>assets/developers/css/proj.css" rel="stylesheet" type="text/css" />
<script> var _siteRoot='index.html',_root='index.html';</script>
<script> var site_url = '<?php echo site_url();?>';</script>
<script> var theme_url = '<?php echo theme_url();?>';</script>
<script> var resource_url = '<?php echo resource_url(); ?>'; var gObj = {};</script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<link rel="stylesheet" href="<?php echo theme_url(); ?>css/conditional.css">
<?php 
if($this->uri->rsegment(1)=='home' || $this->uri->rsegment(1)==''){ ?>
<link rel="stylesheet" href="<?php echo theme_url(); ?>css/fluid_dg.css">
<?php
}
?>
<?php
if(($this->uri->rsegment(1)=='blog' || $this->uri->rsegment(1)=='blogs') && $this->uri->rsegment(2)=='detail'){
	?>
       
    <?php
}
?>
<script src="<?php echo base_url(); ?>assets/developers/js/common.js"></script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.2.min.js"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
</head>
<body style="padding:0">
<div class="p-3 pt-1 pb-1">
<h1 class="black fs20 mb-2">Enquiry Now</h1>
<?php echo validation_message();
  echo form_open_multipart('blogs/send_enquiry/'.$blog_id,'class="" role="form"');
  if($blog_id > 0){
  ?>
    <div><input name="blog_name" id="blog_name" type="text" value="<?php echo $blog_name;?>" class="form-control" disabled>
    </div>
    <?php
  }
  ?>
  
<div class="mt-1"><input name="first_name" id="first_name" type="text" placeholder="Name *" class="form-control" value="<?php echo set_value('first_name'); ?>" />
</div>

<div class="mt-1"><input name="email" id="email" type="text" placeholder="Email ID *" class="form-control" value="<?php echo set_value('email'); ?>" />
</div>

<div class="mt-1"><input name="mobile_number" id="mobile_number" type="text" placeholder="Mobile Number *" class="form-control" value="<?php echo set_value('mobile_number'); ?>" />
</div>

<div class="mt-1"><textarea name="description" id="description" cols="25" rows="3" class="form-control" placeholder="Enquiry *"><?php echo set_value('description'); ?></textarea>
</div>


<div  class="mt-1"><input name="verification_code" id="verification_code" autocomplete="off" type="text" class="form-control float-left" style="width:125px;" placeholder="Enter Code *">
<img src="<?php echo site_url('captcha/normal');?>" class="float-left mt-2" alt="" title="" id="captchaimage" > &nbsp; <a href="javascript:void(0);" title="Change Verification Code"><img src="<?php echo theme_url(); ?>images/ref2.png" class="float-left" alt="Refresh" title="Refresh" onClick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal'); ?>/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code').focus();"></a>
</div>

<div class="clearfix"></div>

<div class="mt-1"><input name="submit" type="submit" value="Send" class="btn btn-blue"></div>
<input type="hidden" name="action" value="enquiry_post">
<input type="hidden" name="prod_id" value="<?php echo $blog_id;?>">    
<?php echo  form_close(); ?>
</div>
</body>
</html>