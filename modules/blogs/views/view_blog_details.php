<?php $this->load->view("top_application");
$date_format=getDateFormat($res['added_date'],1);	
?>
<?php $this->load->view("banner/inner_page_top_banner");	 ?>
<nav aria-label="breadcrumb" class="breadcrumb_bg">
<div class="container">
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
  <li class="breadcrumb-item"><a href="<?php echo site_url('blogs'); ?>">Blog</a></li>
  <li class="breadcrumb-item active"><?php echo $page_title;?></li>
</ol>
</div>
</nav>
<!-- Breadcrumb End -->

<div class="container">
<div class="mid_area">
<div class="cms_area">
<div class="w-100 mt-3 mb-4">
<div class="row">
<?php  if(is_array($media_res) && !empty($media_res)){   ?>
<div class="col-lg-4">
<div class="blog_img">


<figure><img src="<?php echo get_image('blogs',$media_res['media'],702,369,'R');?>" alt="<?php echo $res['blog_alt'];?>" class="img-fluid"/></figure>
</div>
</div>
<?php } ?>
<!-- end -->
<?php $facebook_link = $this->admin_info->facebook_link != null ? 'href="'.$this->admin_info->facebook_link.'" target="_blank"' : 'href="javascript:void(0);"';
$twitter_link = $this->admin_info->twitter_link != null ? 'href="'.$this->admin_info->twitter_link.'" target="_blank"' : 'href="javascript:void(0);"';
$google_link = $this->admin_info->google_link != null ? 'href="'.$this->admin_info->google_link.'" target="_blank"' : 'href="javascript:void(0);"';
$linkedin_link = $this->admin_info->linkedin_link != null ? 'href="'.$this->admin_info->linkedin_link.'" target="_blank"' : 'href="javascript:void(0);"';
$youtube_link = $this->admin_info->youtube_link != null ? 'href="'.$this->admin_info->youtube_link.'" target="_blank"' : 'href="javascript:void(0);"';
$pinterest_link = $this->admin_info->pinterest_link != null ? 'href="'.$this->admin_info->pinterest_link.'" target="_blank"' : 'href="javascript:void(0);"';
$instagram_link = $this->admin_info->instagram_link != null ? 'href="'.$this->admin_info->instagram_link.'" target="_blank"' : 'href="javascript:void(0);"';
$rss_link = $this->admin_info->rss_link != null ? 'href="'.$this->admin_info->rss_link.'" target="_blank"' : 'href="javascript:void(0);"'; ?>
<?php /*?><a <?php echo $facebook_link;?> title="Facebook"><i class="fab fa-facebook-f"></i></a><a <?php echo $twitter_link;?> title="Twitter"><i class="fab fa-twitter"></i></a><a <?php echo $linkedin_link;?> title="Linkedin"><i class="fab fa-linkedin-in"></i></a><a <?php echo $youtube_link;?> title="Youtube"><i class="fab fa-youtube"></i></a><?php */?>

<div class="col-lg-8">
<div class="blog-detail">
<h1><?php echo $page_title;?></h1>
<p class="blog-dat"><i class="far fa-calendar-alt"></i> <?php echo getDateFormat($res["added_date"],2);?></p>

<?php if(!empty($res['blog_description'])){?>
<div class="blog-det-txt"><?php echo $res['blog_description'];?></div>
<?php } ?>


</div>
</div>
<!-- end -->



</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="clearfix"></div>

<?php $this->load->view("bottom_application");?>

