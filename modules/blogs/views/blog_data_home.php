<?php 
if(is_array($res) && !empty($res)){ 
		//trace($res);
	foreach($res as $val){
		
		$param = array('where'=>"rev.status='1' AND rev.blog_id='".$val['blogs_id']."'"); 		         $res_array   = $this->blog_model->get_review(500,0,$param); 
		$review=count($res_array);
			
		$link_url = site_url($val['friendly_url']);		
		$prodImage=get_image('blogs',$val["media"],'419','240');
		$alt_tag=$val['blog_alt'];		
		$date_format=getDateFormat($val['added_date'],1);
				
		?>
        <div class="item">
<div class="blog_outer">
<div class="blog_img">
<p class="red_butt"><a href="<?php echo $link_url;?>"><i class="fas fa-long-arrow-alt-right"></i></a></p>
<figure><a href="<?php echo $link_url;?>"><img src="<?php echo $prodImage;?>" alt="" class="img-fluid"/></a></figure>
</div>
<div class="blog_txt_area">
<p class="blog-dat"><i class="far fa-calendar-alt"></i> <?php echo getDateFormat($val["added_date"],2);?></p>
<p class="blog-hed"><a href="<?php echo $link_url;?>"><?php echo char_limiter($val['blog_name'],70,'..');?></a></p>
<p class="blog-txt"><?php echo char_limiter($val['blog_description'],200);?></p>
</div>
</div>
</div>       
<!-- end -->                  
                   		
		<?php
	}
}?>