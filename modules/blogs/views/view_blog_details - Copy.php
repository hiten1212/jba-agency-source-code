<?php $this->load->view("top_application");
$date_format=getDateFormat($res['added_date'],1);	
?>
<div class="breadcrumb_outer">
<div class="container">
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
  <li class="breadcrumb-item"><a href="<?php echo site_url('blogs'); ?>">Newsroom</a></li>
  <li class="breadcrumb-item active"><?php echo $page_title;?></li>
</ol>
</div>
</div>
<!-- Breadcrumb End -->

<div><img src="<?php echo theme_url(); ?>images/newsroom-banner.jpg" class="w-100" alt=""></div>

<div class="container" style="margin-top:-70px;">
<div class="cms">

<div>
<div class="row">
<div class="col-lg-8 no_pad">
<div class="newsroom_dtl_lft">
<h1><?php echo $page_title;?></h1>
<p class="float-left mt-3"><span class="fa fa-calendar"></span>  <?php echo $date_format; ?></p>
<!--<p class="float-right pink fs15 mt-2"><i class="far fa-comment-dots"></i> <b>0</b></p>-->
<p class="clearfix"></p>
<p class="follow_links">
<?php $facebook_link = $this->admin_info->facebook_link != null ? 'href="'.$this->admin_info->facebook_link.'" target="_blank"' : 'href="javascript:void(0);"';
$twitter_link = $this->admin_info->twitter_link != null ? 'href="'.$this->admin_info->twitter_link.'" target="_blank"' : 'href="javascript:void(0);"';
$google_link = $this->admin_info->google_link != null ? 'href="'.$this->admin_info->google_link.'" target="_blank"' : 'href="javascript:void(0);"';
$linkedin_link = $this->admin_info->linkedin_link != null ? 'href="'.$this->admin_info->linkedin_link.'" target="_blank"' : 'href="javascript:void(0);"';
$youtube_link = $this->admin_info->youtube_link != null ? 'href="'.$this->admin_info->youtube_link.'" target="_blank"' : 'href="javascript:void(0);"';
$pinterest_link = $this->admin_info->pinterest_link != null ? 'href="'.$this->admin_info->pinterest_link.'" target="_blank"' : 'href="javascript:void(0);"';
$instagram_link = $this->admin_info->instagram_link != null ? 'href="'.$this->admin_info->instagram_link.'" target="_blank"' : 'href="javascript:void(0);"';
$rss_link = $this->admin_info->rss_link != null ? 'href="'.$this->admin_info->rss_link.'" target="_blank"' : 'href="javascript:void(0);"'; ?>

<a <?php echo $facebook_link;?> title="Facebook"><i class="fab fa-facebook-f"></i></a><a <?php echo $twitter_link;?> title="Twitter"><i class="fab fa-twitter"></i></a><a <?php echo $linkedin_link;?> title="Linkedin"><i class="fab fa-linkedin-in"></i></a><a <?php echo $youtube_link;?> title="Youtube"><i class="fab fa-youtube"></i></a>

</p>
<?php  if(is_array($media_res) && !empty($media_res)){   ?>
<p class="text-center mt-4"><img src="<?php echo get_image('blogs',$media_res['media'],702,369,'R');?>" alt="<?php echo $res['blog_alt'];?>" class="w-100" ></p>
<?php } ?>
<?php if(!empty($res['blog_description'])){?>
<div class="mt-5"><?php echo $res['blog_description'];?></div>
<?php } ?>

<div class="mt-5 noprint">
    <?php 
if(is_array($comments_res) && !empty($comments_res)){ ?>
    <p class="fs18 pink text-uppercase">Reviews</p>
    <?php foreach($comments_res as $val){	 ?>
    <div class="border1 p-3 mt-2">
    <p class="weight600 black fs15"><?php echo ucwords($val["poster_name"])?></p>
    <p class="fs12"><span class="far fa-calendar-alt"></span> <?php echo getDateFormat($val["posted_date"],2);?></p>
    <div class="mt-1 fs13"><?php echo nl2br($val["comment"])?> </div>
    </div>
           
  <?php }
} ?>
    
    <div class="post_cmnts">
   <a name="review" id="review"></a>
    <p class="fs20">Post Your Comment</p>
    <div class="form-group">
    <?php
	echo form_open(current_url_query_string() . "#review");
	echo error_message();
	echo validation_message();
?>
    <div class="row">
    <div class="col-md-6 p-1 mt-1"><input id="name" name="name" placeholder="Name *" type="text" value="<?php echo set_value('name'); ?>"></div>
    <div class="col-md-6 p-1 mt-1"><input name="email" type="text" id="email" placeholder="Email Id *" autocomplete="off" value="<?php echo set_value('email'); ?>"></div>
    <div class="col-12 p-1 mt-1"><textarea name="comments" rows="4" id="comment" placeholder="Comments *" autocomplete="off"><?php echo set_value('comments'); ?></textarea></div>
    <div class="col-12 p-1 mt-1"><p>
      <input name="verification_code" id="verification_code" autocomplete="off" type="text" placeholder="Enter Code *" class="float-left" style="width:110px;"><img src="<?php echo site_url('captcha/normal/blog/blog');?>" class="ml-1" alt="" id="captchaimage1" /> <a href="javascript:void(0);" title="Change Verification Code"><img src="<?php echo theme_url();?>images/refresh.png"  alt="Refresh"  onclick="document.getElementById('captchaimage1').src='<?php echo site_url('captcha/normal/blog/blog');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="ml-1"></a>   
    
    </p><p class="clearfix"></p>
    <p class="mt-1 fs12">Type the characters shown above.</p></div>
    </div>
    <p class="p-2">
 <label><button type="submit" id="submit" class="btn btn-pink"> Post </button></label></p>
    
    <input type="hidden" name="post_cmt" value="Y" />
<input type="hidden" name="blogs_id" value="<?php echo $res['blogs_id'];?>" />
<input type="hidden" name="blog_title" value="<?php echo $res['blog_name'];?>" />


<?php echo form_close();?>
    </div>
    </div>
    
    </div>
</div>
</div>


<div class="col-lg-4 no_pad">
<?php if(is_array($res_news) && !empty($res_news)){ 
		//trace($res);	?>
<p class="fs20 pink weight700">Recent Posts</p>
<div class="newsroom_dtl_rgt">
 <?php foreach($res_news as $val){
		
		$param = array('where'=>"rev.status='1' AND rev.blog_id='".$val['blogs_id']."'"); 		         $res_array   = $this->blog_model->get_review(10,0,$param); 
		$review=count($res_array);
			
		$link_url = site_url($val['friendly_url']);		
		$prodImage=get_image('blogs',$val["media"],'390','205');
		$alt_tag=$val['blog_alt'];		
		$date_format=getDateFormat($val['added_date'],1); ?>
        <div>
        <p><span class="far fa-calendar-alt"></span> <?php echo $date_format; ?></p>
        <p class="newsroom_title"><a href="<?php echo $link_url;?>" title="<?php echo $val['blog_name'];?>"><?php echo char_limiter($val['blog_name'],70,'..');?></a></p>
        <div class="row mt-3">
        
        <p class="col-6 no_pad like_ico"><i class="far fa-thumbs-up"></i> 
        <span id="likeId<?php echo $val['blogs_id']; ?>" ><?php echo $val['likes'];?></span></p>
        <p class="col-6 no_pad comment_ico"><i class="far fa-comment-dots"></i> <?php echo $review; ?></p>
        
        </div>
        <p class="mt-2 pink text-right"><a href="<?php echo $link_url;?>">Read More</a></p>
        </div>
	  <p class="bb mt-4 mb-4"></p>
	<?php } ?>

</div>
<?php } ?>
</div>
</div>
</div>

</div>
</div>

<!--Subscribe to Our Newsletter-->
<div class="newsletter_area">
<div class="container">
<div class="newsletter_box">
<p class="newsletter_title">Subscribe to Our Newsletter</p>
<div  id="my_newsletter_msg" class="mt5 text-center"></div>
<?php echo form_open('pages/join_newsletter','name="newsletter" id="chk_newsletter" onsubmit="return join_newsletter();" '); ?>
<div class="row">
<div class="col-md-6 no_pad"><div class="newsletter_field">
<input type="text" name="subscriber_name" id="subscriber_name" placeholder="Name"></div></div>
<div class="col-md-6 no_pad">
<div class="newsletter_field">
<input name="subscriber_email" id="subscriber_email" type="text" placeholder="Email ID *">
</div></div>
<div class="col-md-6 no_pad">
<div class="newsletter_field"><input name="verification_code" id="verification_code1" autocomplete="off" value="" type="text" placeholder="Enter Code *" class="w-50" >
<img src="<?php echo site_url('captcha/normal/homenewslwtter');?>" class="" alt="" id="captchaimage1"/><a href="javascript:void(0);" title="Change Verification Code" > &nbsp;<img src="<?php echo theme_url(); ?>images/refsh.png" alt="Refresh" onclick="document.getElementById('captchaimage1').src='<?php echo site_url('captcha/normal/homenewslwtter'); ?>/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code1').focus();" id="refresh" class=""></a> 
</div></div>
<div class="col-md-6 no_pad"><p>
<input name="submit" type="submit" value="Subscribe">
<input name="subscribe_me" type="hidden" id="saction" value="Y" />
</p></div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>

<?php $this->load->view("bottom_application");?>

