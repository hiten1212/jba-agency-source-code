<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Blog_model extends MY_Model
 {


	 public function get_blogs($limit='10',$offset='0',$param=array())
	 {

		$category_id		=   @$param['category_id'];	
		$status			    =   @$param['status'];
		$blogid			=   @$param['blogid'];
		$orderby			=	@$param['orderby'];
		$orderbyn			=	@$param['orderbyn'];
		$where			    =	@$param['where'];	
		$jwhere			    =	@$param['jwhere'];		
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);
		
		//trace($param);

		if($where!='')
		{
			$this->db->where($where);

		}
		if($category_id!='')
		{
			$this->db->where("FIND_IN_SET( '".$category_id."',wlp.category_links )");
		}
		

		if($blogid!='')
		{
			$this->db->where("wlp.blogs_id  ","$blogid");
		}

		
		
		if($status!='')
		{
			$this->db->where("wlp.status","$status");
		}
		
		$search_prod_type=$this->input->get_post('search_prod_type',TRUE);
		
		if($search_prod_type=='is_featured')
		{
			$this->db->where("wlp.featured_blog",'1');
		}
		
		if($keyword!='')
		{
			$this->db->where("(wlp.blog_name LIKE '%".$keyword."%' )");
		}

		if($this->input->get_post("sortbyprice")!=""){
			$orderby = $this->input->get_post("sortbyprice");
		}
		if($this->input->get_post("sortbyname")!=""){
			$orderbyn = $this->input->get_post("sortbyname");
		}

		if($orderby!='')
		{
			$orderby=$this->db->escape_str($orderby);
			if($orderby=="lth"){
				$this->db->order_by('price ','asc');
			}
			if($orderby=="htl"){
				$this->db->order_by('price ','desc');
			}
		}

		if($orderbyn!='')
		{
			$orderbyn=$this->db->escape_str($orderbyn);
			if($orderbyn=="az"){
				$this->db->order_by('wlp.blog_name ','asc');
			}
			if($orderbyn=="za"){
				$this->db->order_by('wlp.blog_name ','desc');
			}
		}

		if(empty($orderby) && empty($orderbyn))
		{
			$this->db->order_by('wlp.blogs_id ','desc');
		}

	  $this->db->group_by("wlp.blogs_id");
	  if($limit!="")
		$this->db->limit($limit,$offset);
		
		if($this->uri->segment(1)=='sitepanel')
		{
			$rev_cond='';
		}
		else
		{
			$rev_cond='tbl_blog_review.status=1 AND ';
		}
		


		
		$this->db->select("SQL_CALC_FOUND_ROWS wlp.*,wlp.status as blog_status,wlpm.media,wlpm.media_type,wlpm.is_default,(SELECT count(review_id) FROM tbl_blog_review WHERE ".$rev_cond." tbl_blog_review.blog_id =wlp.blogs_id ) AS review_count",FALSE);			
		
		$this->db->from('wl_blogs as wlp');
		$this->db->where('wlp.status !=','2');
		
		$this->db->join('wl_blogs_media AS wlpm','wlp.blogs_id=wlpm.blogs_id'.$jwhere,'left');
				
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? @$result[0]: $result;
		return $result;

	}
	
	public function get_blogs_url($blog_id)
	{
		$query="SELECT * FROM wl_blogs_url WHERE blogs_id='".$blog_id."'  ";
		$db_query=$this->db->query($query);
		if($db_query->num_rows() > 0)
		{
			$res=$db_query->result_array();
			return $res;
		}
		else
		{
			return false;
		}
		
	}

	public function get_blog_media($limit='4',$offset='0',$param=array())
    {

		 $default			    =   @$param['default'];
		 $blogid			    =   @$param['blogid'];
		 $media_type			=   @$param['media_type'];
		 $where  =   @$param['where'];
		 //$color_id=$this->uri->segment('6');
		 if( is_array($param) && !empty($param) )
		 {
			$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE);
			if($limit)
			$this->db->limit($limit,$offset);
			$this->db->from('wl_blogs_media');
			$this->db->where('blogs_id',$blogid);

			if($default!='')
			{
				$this->db->where('is_default',$default);
			}
			if($media_type!='')
			{
				$this->db->where('media_type',$media_type);
			}
						
			if($where!=''){			
				$this->db->where($where);			
			}
			
			$this->db->order_by('id ASC');
			
			$q=$this->db->get();
			//echo_sql();
			$result = $q->result_array();
			$result = ($limit=='1') ? $result[0]: $result;
			return $result;

		 }

	}


	public function get_blog_base_price($param=array())
    {

		 $where			    =   @$param['where'];


		  $this->db->select('*',FALSE);

		  $this->db->from('wl_blog_attributes');

		  if($where!='')
		  {
			  $this->db->where($where);
		  }


		  $q=$this->db->get();
		  $result = $q->row_array();
		  return $result;
	}

	public function get_blog_brand($brand_id,$param=array())
    {
		 $brand_id  = (int) $brand_id;

		if($brand_id > 0)
		{

		  $where			    =   @$param['where'];

		  if($where!='')
		  {
			  $this->db->where($where);
		  }


		  $this->db->select('*',FALSE);

		  $this->db->from('wl_brands');

		  $this->db->where(array('brand_id ='=>$brand_id,'status !='=>'2'));




		  $q=$this->db->get();
		  $result = $q->row_array();
		  return $result;
		}
	}



	public function related_blogs_added($blogId,$limit='NULL',$start='NULL')
	{
		$res_data =  array();
		$condtion = ($blogId!='') ? "status ='1' AND blog_id = '$blogId'  ":"status ='1'";
		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"id DESC",
													'limit'=>$limit,
													'start'=>$start,
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_blogs_related',$fetch_config);
		if( is_array($result) && !empty($result) )
		{
			foreach ($result as $val )
			{
				$res_data[$val['id']] =$val['related_id'];
			}
		}
		return $res_data;
	}

	public function update_viewed($id,$counter=0)
	{
	  $id = (int) $id;
	  if($id>0)
	  {
		  $posted_data = array(
					'blogs_viewed'=>($counter+1)
				 );

		  $where = "blogs_id = '".$id."'";
		  $this->category_model->safe_update('wl_blogs',$posted_data,$where,FALSE);
	  }

	}



	public function get_related_blogs($blogId)
	{
		$condtion = (!empty($blogId)) ? "status !='2'  AND blogs_id NOT IN(".implode(",",$blogId). ")" :"status !='2'";

		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"blogs_id DESC",
													'limit'=>'NULL',
													'start'=>'NULL',
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_blogs',$fetch_config);
		return $result;
	}


	public function related_blogs($blogId,$start='NULL',$limit='NULL')
	{
		$res_data =  array();
		$condtion = ($blogId!='') ? "status ='1' AND blog_id = '$blogId'  ":"status ='1'";
		$fetch_config = array(
		'condition'=>$condtion,
		'order'=>"RAND()",
		'limit'=>$limit,
		'start'=>$start,
		'debug'=>FALSE,
		'return_type'=>"array"
		);
		
		$result = $this->findAll('wl_blogs_related',$fetch_config);
		if( is_array($result) && !empty($result) )
		{
			foreach ($result as $val )
			{
				$res_data[$val['id']] = $this->get_blogs(1,0, array('blogid'=>$val['related_id'],'where'=>"status ='1'"));
			}
		}
		$res_data = array_filter($res_data);
		return $res_data;
	}


	public function get_related_colors($colorId,$limit='NULL',$start='NULL')
	{
		$condtion = (!empty($colorId)) ? "status !='2'  AND color_id NOT IN(".implode(",",$colorId). ")" :"status !='2'";

		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"color_id DESC",
													'limit'=>'NULL',
													'start'=>'NULL',
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_colors',$fetch_config);
		return $result;
	}

	public function related_colors($param=array())
	{
	  $res_data =  array();

	  $where			=	@$param['where'];
	  $limit			=   @$param['limit'];
	  $offset			=	@$param['offset'];

	  if($limit>0)
	  {
		$this->db->limit($limit,$offset);
	  }
	  if($where!='')
	  {
		$this->db->where($where);
	  }
	  $this->db->select('SQL_CALC_FOUND_ROWS wlc.color_name,wlc.status as color_status',FALSE);
	  $this->db->from('wl_colors as wlc');
	  $this->db->where('wlc.status !=','2');
	  //$this->db->join('wl_blog_attributes AS wlpatt','wlc.color_id=wlpatt.color_id');
	  $q=$this->db->get();
	  $result = $q->result_array();

	  if( is_array($result) && !empty($result) )
	  {
		  foreach ($result as $val )
		  {
			$res_data[$val['attribute_id']] = $val;
		  }
	  }

	  $res_data = array_filter($res_data);
	  return $res_data;
	}

	public function get_related_sizes($sizeId,$limit='NULL',$start='NULL')
	{
		$condtion = (!empty($sizeId)) ? "status !='2'  AND size_id NOT IN(".implode(",",$sizeId). ")" :"status !='2'";

		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"size_id DESC",
													'limit'=>'NULL',
													'start'=>'NULL',
													'debug'=>FALSE,
													'return_type'=>"array"
												 );
		$result = $this->findAll('wl_sizes',$fetch_config);
		return $result;
	}

	public function related_sizes($param=array())
	{
	  $res_data =  array();

	  $where			=	@$param['where'];
	  $limit			=   @$param['limit'];
	  $offset			=	@$param['offset'];

	  if($limit>0)
	  {
		$this->db->limit($limit,$offset);
	  }
	  if($where!='')
	  {
		$this->db->where($where);
	  }
	  $this->db->select('SQL_CALC_FOUND_ROWS wls.size_name,wls.status as size_status',FALSE);
	  $this->db->from('wl_sizes as wls');
	  $this->db->where('wls.status !=','2');
	  //$this->db->join('wl_blog_attributes AS wlpatt','wls.size_id=wlpatt.size_id');
	  $q=$this->db->get();
	  $result = $q->result_array();

	  if( is_array($result) && !empty($result) )
	  {
		  foreach ($result as $val )
		  {
			$res_data[$val['attribute_id']] = $val;
		  }
	  }

	  $res_data = array_filter($res_data);
	  return $res_data;
	}


	public function get_shipping_methods()
	{
		$condtion = "status =1";
		$fetch_config = array(
													'condition'=>$condtion,
													'order'=>"shipping_id DESC",
													'debug'=>FALSE,
													'return_type'=>"array"
													);
		$result = $this->findAll('wl_shipping',$fetch_config);
		return $result;
	}

	public function get_blog_colors($id)
	{
		$id = (int) $id;
		if($id>0)
		{

			$this->db->select('wc.color_id, wc.color_name, wc.color_code');
			$this->db->from('wl_blog_colors as wpc');
			$this->db->join('wl_colors as wc','wc.color_id=wpc.color_id','left');
			$this->db->join('wl_blog_stock as wps','wps.blog_color=wpc.color_id','left');
			$this->db->where('wpc.blog_id',$id)->group_by('wc.color_id')->order_by('wps.blog_quantity desc');
			$q=$this->db->get();
			return $result = $q->result_array();

		}
	}

	public function get_blog_sizes($id)
	{
		$id = (int) $id;
		if($id>0)
		{

			$this->db->select('ws.size_id, ws.size_name');
			$this->db->from('wl_blog_sizes as wps');
			$this->db->join('wl_sizes as ws','ws.size_id=wps.size_id');
			$this->db->join('wl_blog_stock as wpst','wpst.blog_size=ws.size_id');
			$this->db->where('wps.blog_id',$id)->group_by('wpst.blog_size')->order_by('wpst.blog_quantity desc');
			$q=$this->db->get();
			return $result = $q->result_array();

		}
	}
	
	public function get_blog_categories()
	{
		
		$this->db->select('wc.category_id, wc.category_name');
		$this->db->from('wl_categories as wc');
		$this->db->join('wl_blogs as wp','wp.category_id=wc.category_id');
		$this->db->where('wp.status !=','2');
		$this->db->where('wc.status !=','2');
		$this->db->order_by('wc.category_name asc');
		$this->db->group_by('wp.category_id');
		$q=$this->db->get();
		return $result = $q->result_array();
		
	}
	
	/*---------Manage Blog Stock -------------*/
	
	public function blog_id_stock_list($blog_id)
	{
		$query="SELECT * FROM wl_blog_stock WHERE blog_id='".$blog_id."'  ORDER BY blog_size ASC";
		$db_query=$this->db->query($query);
		if($db_query->num_rows() > 0)
		{
			$res=$db_query->result_array();
			return $res;
		}
		else
		{
			return false;
		}
		
	}
	 
	/*---------End  Blog Stock -------------*/ 
	
	
	public function blog_out_stock_list($limit='10',$offset='0',$param=array())
	{
		$category_id		=   @$param['category_id'];
		$brand_id			=   @$param['brand_id'];
		$status			    =   @$param['status'];			
		$where			    =	@$param['where'];
		$orderby			=	@$param['orderby'];		
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));	
		$keyword			=   $this->db->escape_str($keyword);
		
		
		if($keyword!='')
		{
			$this->db->where("(wlp.blog_name LIKE '%".$keyword."%' OR wlp.blog_code LIKE '%".$keyword."%' )");
		}
		
		if($orderby!='')
		{		
			//$this->db->order_by($orderby);
			
		}
		else
		{
			//$this->db->order_by('wlp.blogs_id ','desc');
		}
	
	    //$this->db->group_by("wlp.blogs_id"); 	
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wlp.*,stock.blog_quantity,wlp.status as blog_status,wlpatt.*,wlpm.media,wlpm.media_type,wlpm.is_default',FALSE);
		$this->db->from('wl_blogs as wlp');
		$this->db->where('wlp.status !=','2');
		
		$cond="stock.blog_id IN(SELECT IF(sum(blog_quantity) = '0', blog_id,'' ) as pid FROM `wl_blog_stock` WHERE 1 group by blog_id )";
		$this->db->where($cond);
		
		//$this->db->where(array('wlpatt.color_id ='=>'0','wlpatt.size_id ='=>'0'));
		$this->db->join('wl_blog_attributes AS wlpatt','wlp.blogs_id=wlpatt.blog_id');
		#--------------------------------------------------------------------------------------#
		
		//exit;
		
		$this->db->join('wl_blog_stock AS stock',"wlp.blogs_id=stock.blog_id",'left');
		
		#-------------------------------------------------------------------------------------#
		$this->db->join('wl_blogs_media AS wlpm','wlp.blogs_id=wlpm.blogs_id','left');
		//$this->db->where('stock.blog_quantity',0);		
	
		$this->db->group_by('stock.blog_id'); 
		
		$q=$this->db->get();
       //echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
			
	}
	 
	/*---------End  Blog Stock -------------*/ 
  
  	
	
	public function blog_in_stock_list($limit='10',$offset='0',$param=array())
	{
		$category_id		=   @$param['category_id'];
		$brand_id			=   @$param['brand_id'];
		$status			    =   @$param['status'];			
		$where			    =	@$param['where'];
		$orderby			=	@$param['orderby'];		
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));	
		$keyword			=   $this->db->escape_str($keyword);
		
		
		if($keyword!='')
		{
			$this->db->where("(wlp.blog_name LIKE '%".$keyword."%' OR wlp.blog_code LIKE '%".$keyword."%' )");
		}
		
		if($status!='')
		{
			$this->db->where("wlp.status","$status");
		}
		$search_prod_type=$this->input->get_post('search_prod_type',TRUE);
		
		if($search_prod_type=='new_arrival')
		{
			$this->db->where("wlp.new_arrival",'1');
		}
		if($search_prod_type=='new_arrival')
		{
			$this->db->where("wlp.new_arrival",'1');
		}
		if($search_prod_type=='hot_blog')
		{
			$this->db->where("wlp.hot_blog",'1');
		}
		
		
		if($orderby!='')
		{		
			//$this->db->order_by($orderby);
			
		}
		else
		{
			//$this->db->order_by('wlp.blogs_id ','desc');
		}
	
	   $this->db->group_by("wlp.blogs_id"); 	
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wlp.*,stock.blog_quantity,wlp.status as blog_status,wlpatt.*,wlpm.media,wlpm.media_type,wlpm.is_default',FALSE);
		$this->db->from('wl_blogs as wlp');
		$this->db->where('wlp.status !=','2');
		
			$cond="stock.blog_id IN(SELECT 
	IF(sum(blog_quantity)> '0', blog_id,'' ) as pid
	 FROM `wl_blog_stock` WHERE 1 group by blog_id 
)";
$this->db->where($cond);
		
		//$this->db->where(array('wlpatt.color_id ='=>'0','wlpatt.size_id ='=>'0'));
		$this->db->join('wl_blog_attributes AS wlpatt','wlp.blogs_id=wlpatt.blog_id');
		#--------------------------------------------------------------------------------------#
		
		//exit;
		
		$this->db->join('wl_blog_stock AS stock',"wlp.blogs_id=stock.blog_id",'left');
		
		#-------------------------------------------------------------------------------------#
		$this->db->join('wl_blogs_media AS wlpm','wlp.blogs_id=wlpm.blogs_id','left');
		//$this->db->where('stock.blog_quantity',0);
		
	
		//$this->db->group_by('stock.blog_id'); 
		
		$q=$this->db->get();
       //echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
			
	}
/*---------Color media-------------*/
	
	public function get_color_image($opts=array())
	{
		$status_flag=FALSE;
		$keyword = $this->db->escape_str($this->input->post('keyword'));
		$status = $this->db->escape_str($this->input->post('status'));
		
		if($status!=='' && in_array($status,array('1','0')))
		{
			$opts['condition']= "color_status ='".$status."' AND blogs_id='".$this->uri->segment(4)."' ";
			$status_flag=TRUE;
		}
		if( ! $status_flag && (!array_key_exists('condition',$opts) || $opts['condition']==''))
		{
			$opts['condition']= "color_status !='2' ";
		}
		if($keyword!='')
		{
			$opts['condition'].= "AND (color_id ".$keyword.")";
		}
		
		$opts['condition'].= "AND blogs_id='".$this->uri->segment(4)."' AND color_id!=0";
		
		if(!array_key_exists('order',$opts) || $opts['order']=='')
		{
			$opts['order']= "id DESC ";
		}
		$fetch_config = array(
								'condition'=>$opts['condition'],
								'order'=>$opts['order'],
								'debug'=>FALSE,
								'return_type'=>"array"							  
							);	
		if(array_key_exists('limit',$opts) && applyFilter('NUMERIC_GT_ZERO',$opts['limit'])>0){
			$fetch_config['limit']=$opts['limit'];
		}	
		if(array_key_exists('offset',$opts) && applyFilter('NUMERIC_WT_ZERO',$opts['offset'])!=-1){
			$fetch_config['start']=$opts['offset'];
		}		
		$result = $this->findAll('wl_blogs_media',$fetch_config);
		return $result;	
	}
	
	// Add news information ...................
	public function add_color_image()
	{  		
			$color_id= $this->input->post('color_id');
			$this->load->library('upload');									
			$data_upload_sugg = $this->upload->my_upload('media',"blogs");
		$defalut_image = 'Y';	
		if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) )
		{ 
			$add_data = array(
			'blogs_id'=>$this->input->post('blog_id'),
			'media_type'=>'photo',
			'is_default'=>$defalut_image,									
			'media'=>$data_upload_sugg['upload_data']['file_name'],
			'media_date_added' => $this->config->item('config.date.time'),
			'color_id'=>$this->input->post('color_id')
			);															
			$this->blog_model->safe_insert('wl_blogs_media', $add_data ,FALSE );
		
		
		
		return TRUE;
		}
		else
		{
			$this->delete_in("wl_blogs_media","id = $color_id",FALSE);
			return FALSE;
		}	
	}
	
	public function update_color_img($rowdata)
	{  
		 $where =  "id = '".$rowdata->id."'";
		
		$data = array(
						'color_id'  	=> $this->input->post('color_id'),
						'blogs_id'  	=> $this->input->post('blog_id')
					 );
					 
				
		$banId =  $this->safe_update('wl_blogs_media',$data,$where,FALSE);
		
		$this->load->library('upload');
		$data =  $this->upload->my_upload('media','blogs');
		
		if( is_array($data)  && !empty($data) )
		{ 
			$this->safe_update('wl_blogs_media',
													array(
													'media'=>$data['upload_data']['file_name']
													),
													"id = $rowdata->id ",
													FALSE
												);
			unlink(UPLOAD_DIR.'/blog/'.$rowdata->media);
			return FALSE;
		}
	}
	
	public function get_blog_by_id($id)
	{
		$id = (int) $id;
		if($id>0)
		{
			
			$this->db->select('*');
			$this->db->from('wl_blog_colors');
			$this->db->where('blog_id',$id);
			$q=$this->db->get();
			return $result = $q->result_array();
			
		}
	}
	
	
	// Get help center by id.....
	public function get_color_by_id($id){
		
		$id = (int) $id;
	    
		if($id!='' && is_numeric($id)){
			
			$condtion = "color_status !='2' AND id=$id";
			
			$fetch_config = array(
							  'condition'=>$condtion,							 					 
							  'debug'=>FALSE,
							  'return_type'=>"object"							  
							  );
			
			$result = $this->find('wl_blogs_media ',$fetch_config);
			return $result;		
		
		 }
	}
	
	public function change_color_status()
	{
	   $arr_ids = $_REQUEST['arr_ids'];
		if(is_array($arr_ids)){
		
			$str_ids = implode(',', $arr_ids);
			
			if($this->input->post('Activate')=='Activate')
			{
				$query = $this->db->query("UPDATE wl_blogs_media   
				                           SET color_status='1' 
										   WHERE id in ($str_ids)");
										   
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('activate') ); 
			}
			
			if($this->input->post('Deactivate')=='Deactivate')
			{
				$query = $this->db->query("UPDATE wl_blogs_media  
				                           SET color_status='0' 
										   WHERE id in ($str_ids)");
										   
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('deactivate') ); 
				
			}
			
			if($this->input->post('Delete')=='Delete')
			{
				$query = $this->db->query("DELETE FROM wl_blogs_media 
				                           WHERE id in ($str_ids)");
										   
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('deleted')); 
			}
		}
	}
	
	/*---------End Color media-------------*/
	
	public function get_review($limit='10',$offset='0',$param=array())
	{
		$status			    =   @$param['status'];
		$rev_status			=   @$param['rev_status'];
		$blogid			=   @$param['blogid'];
		$rev_type			=   @$param['rev_type'];
		$orderby			=	@$param['orderby'];
		$where			    =	@$param['where'];
		
		$join			    =	@$param['join'];
		$keyword			=   trim($this->input->get_post('keyword',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		$return_type = !array_key_exists('return_type',$param) ? "result_array"     : $param['return_type'];

		if($blogid!='')
		{
			$this->db->where("prd.blogs_id  ","$blogid");
		}
		
		if($rev_type!='')
		{
			$this->db->where("rev.rev_type  ","$rev_type");
		}

		if($rev_status!='')
		{
			$this->db->where("rev.status","$rev_status");
		}
		
		if($status!='')
		{
			$this->db->where("prd.status","$status");
		}


		if($keyword!='')
		{
			$this->db->where("(rev.blog_title LIKE '%".$keyword."%' OR rev.poster_name LIKE '%".$keyword."%' OR rev.poster_email LIKE '%".$keyword."%' )");
		}

		if($where!='')
		{
			$this->db->where($where,"",FALSE);
		}

		if($orderby!='')
		{
			$this->db->order_by($orderby);

		}
		else
		{
			$this->db->order_by('rev.review_id ','desc');
		}

		if($limit)
		$this->db->limit($limit,$offset);

		$this->db->select('SQL_CALC_FOUND_ROWS rev.*,prd.blog_name',FALSE);
		$this->db->from('tbl_blog_review as rev');
		//$this->db->where('rev.status !=2');
		$this->db->join('wl_blogs AS prd','rev.blog_id=prd.blogs_id','left');
		if(is_array($join)){
			$this->db->join($join["tableName"],$join["cond"],$join["type"]);
		}
		$q=$this->db->get();
		//echo_sql();
		
		
		$result =  ($return_type!='')  ? $q->$return_type()  :  $q->result_array() ;
		return $result;

	}

}