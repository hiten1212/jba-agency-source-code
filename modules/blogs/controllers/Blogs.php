<?php
class Blogs extends Public_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model(array('category/category_model','blogs/blog_model'));
		$this->load->helper(array('blogs/blog','category/category','query_string'));
		$this->load->library(array('Dmailer'));
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}
	
	public function index(){
		
		$this->page_section_ct = 'blog';
		$offset                 = (int) $this->input->post('offset');
		$curr_symbol = display_symbol();
		$posted_data = is_array($this->input->post()) ? array_filter($this->input->post()) : array();
		if($this->input->post()){
			$posted_data= array_filter($this->input->post());
			unset($posted_data['submit']);
		}
		$condition               = array();
		$cat_res = '';
		//$record_per_page        = (int) $this->input->post('per_page');
		$record_per_page        = 200;//(int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');
		$category_id = $this->input->post('category_id') ? (int) $this->input->post('category_id') : $this->uri->rsegment(3);
		$page_segment           = find_paging_segment();
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$base_url      = ( $category_id!='' ) ?   "blogs/index/$category_id/pg/" : "blogs/index/pg/";
		$condition['status']     = '1';
		$condition['orderby']     = 'blogs_id asc';
		$page_title             = "Blogs";
		
		$where                   = "wlp.status ='1' ";		
		$featured = $this->input->post('featured',TRUE);
				
		if($this->uri->rsegment(3)=="latest-blogs" || $featured!=''){
			$where.=" AND is_latest = '1'";
			$page_title = 'Latest Blogs';
		}

		$category_id     = (int) $this->input->post('category_id',TRUE);
		$keyword			=   trim($this->input->post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		if($category_id!='' )
		{
			$condition['category_id'] = $category_id;
		  //$where.=" AND wlp.category_id = '".$category_id."'";
		}

		if($keyword){
			$condition['keyword'] = $keyword;
			$where.=" AND (wlp.blog_name LIKE '%".$keyword."%')";
		}

		if( is_array($posted_data) && !empty($posted_data ))
		{	
			$category_id            = (int) $this->input->post('category_id',TRUE);
			if($category_id!='' )
			{
				$condition['category_id'] = $category_id;
				$cat_res = get_db_single_row('wl_categories','*'," AND category_id = '$category_id'");
				$page_title.=" (".$cat_res['category_name'].")";
			}

		}
		
		$condition['where'] = $where;
		$res_array  =  $this->blog_model->get_blogs($config['per_page'],$offset,$condition);
		//echo_sql();
		$config['total_rows'] = $data['totalProduct']	=  get_found_rows();

	    $data['frm_url'] = $base_url;
		
		$data['record_per_page'] = $record_per_page;
		$data['heading_title'] = $page_title;
		$data['res']           = $res_array;
		$data['cat_res'] = $cat_res;

		if($this->input->is_ajax_request())
		{
			$this->load->view('blogs/blog_data',$data);
		}
		else
		{
			$this->load->view('blogs/view_blog_listing',$data);
		}

	}
	

	public function detail(){		

		$this->meta_info['entity_id'];
		$data['unq_section'] = "Product";
		$blogId = (int) $this->meta_info['entity_id'];
		$option = array('blogid'=>$blogId);
		$res =  $this->blog_model->get_blogs(1,0,$option);

		if(is_array($res)&& !empty($res) ){			

			$data['page_title'] = $res['blog_name'];
			$data['res']       = $res;
			
			if($this->input->post('post_cmt')=='Y')
			{
				$this->post_comment();
			}
			
			/* End  Post Your  comments */ 
			 
			$config['per_page']		=   $this->config->item('per_page');
			$offset                 =   $this->uri->segment(4,0);
			$limit				    =	$config['per_page'];
			$next				    = 	$offset+$limit;
				
			$param = array('where'=>"rev.status='1' AND rev.blog_id='".$res['blogs_id']."'"); 
			$res_array   = $this->blog_model->get_review(500,0,$param); 
			$config['total_rows']	        = get_found_rows(); 
			/*$more_paging['start_tag']       = '<div class="text-center blue fs15 p6">';
			$more_paging['text']            = 'View More';
			$more_paging['end_tag']         = '</div>';
			$more_paging['more_container']  = 'my_data';
			$data['more_link']              =  more_paging("forum/detail/$id/$next",$config['total_rows'],$limit,$next,$more_paging); 			*/
			$data['comments_res']  = $res_array;  
			
			$media_res         = $this->blog_model->get_blog_media(1,0,array('blogid'=>$res['blogs_id']));	
			$data['media_res'] = $media_res;
			
			$where              = "wlp.status ='1' ";					
			$condition['where'] = $where;
		   $res_array  =  $this->blog_model->get_blogs(100,0,$condition);
		//echo_sql();
			$data['res_news'] = $res_array;
			
			$this->load->view('blogs/view_blog_details',$data);
		}else{
			redirect('blogs', '');
		}

	}
	
	public function post_comment()
	{
		$this->form_validation->set_rules('name',"Name",'trim|alpha|required|max_length[50]');
		$this->form_validation->set_rules('email',"Email",'trim|required|valid_email|max_length[80]');
		//$this->form_validation->set_rules('rating',"Select Rating",'trim|required');
		$this->form_validation->set_rules('comments',"Comments",'trim|required|max_length[5000]');
		$this->form_validation->set_rules('verification_code','Verification Code','trim|required|valid_captcha_code[blog]');
			
		if($this->form_validation->run()===TRUE)
		{
			$posted_data=array(
					'poster_name'   => $this->input->post('name',TRUE),
					'poster_email'  => $this->input->post('email',TRUE),
					'rate'        	=> $this->input->post('rating',TRUE),
					'blog_id' 		=> $this->input->post('blogs_id',TRUE),
					'blog_title' 	=> $this->input->post('blog_title',TRUE),
					'comment'       => $this->input->post('comments',TRUE),
					'posted_date'  	=> $this->config->item('config.date.time'),
					'status'       	=> '0',
			);
			//trace($posted_data);
			//exit;
			$this->blog_model->safe_insert('tbl_blog_review',$posted_data,FALSE);
			
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your comment has been posted successfully.We will get back to you soon.');
			$blog_frindly_url=get_db_field_value('wl_blogs','friendly_url',array("blogs_id"=>$this->input->post('blogs_id')));
 			redirect($blog_frindly_url,'');
			//exit;
		}
	}	

	public function download_pdf(){		

		$pId=(int)$this->uri->segment(3,0);		

		if($pId > 0){			

			$pdf_name='brochure_doc';

			$file=get_db_field_value('wl_blogs', $pdf_name, array("blogs_id"=>$pId));

			if($file !="" && @file_exists(UPLOAD_DIR."/brochures/".$file)){

				$this->load->helper('download');

				$data = file_get_contents(UPLOAD_DIR."/brochures/".$file);

				$name = $file;

				force_download($name, $data);

			}else{

				redirect('blogs', '');

			}

		}else{

			redirect('blogs', '');

		}

	}
	
	
	public function get_likes(){	

		 $blogs_id=(int) $_POST['blog_id'];
				
		$likecount=get_db_field_value("wl_blogs","likes"," AND blogs_id='".$blogs_id."' "); 
				
		$likecount=$likecount+1;
		$posted_data=array('likes' => $likecount);	
		
		$where = "blogs_id = '".$blogs_id."'";				
		$this->blog_model->safe_update('wl_blogs',$posted_data,$where,false);
		
		$likesRes=get_db_field_value("wl_blogs","likes"," AND blogs_id='".$blogs_id."' ");
		
		echo $likesRes;
		
	}

	

}

?>