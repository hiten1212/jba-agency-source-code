<?php

class Brand extends Public_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model(array('brand/brand_model'));
	}



	public function index(){

		$record_per_page         = (int) $this->input->post('per_page');
		$page_segment            =  find_paging_segment();
		$config['per_page']	     =  '200';//( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');

		$offset                 = (int) $this->input->post('offset');
		$base_url                =   "brand/index/pg/";
		$param= array('status'=>'1');

		$res_array   = $this->brand_model->get_brands($param,$offset,$config['per_page']);

		//echo_sql();
		$config['total_rows'] = $data['totalProduct']	= get_found_rows();
		//$data['page_links'] = front_pagination($base_url,$config['total_rows'],$config['per_page'],$page_segment);

		$data['record_per_page'] = $config['per_page'];
		$data['res'] = $res_array;
		$data['frm_url'] = $base_url;


		if($this->input->is_ajax_request()){
			$this->load->view('brand/brand_data',$data);
		}
		else{
			$this->load->view('brand/view_brands',$data);
		}
	}
	
	
	public function detail()
	{
		$id     = (int) $this->meta_info['entity_id'];	
		
		//$id =  (int) $this->uri->segment(3);	
		$res  = $this->brand_model->get_brand_by_id($id);									
		
		$data['gallery_title']=$res['gallery_title'];
		$data['res'] = $res; 
		$this->load->view('brand/view_brand_details',$data);

	}


}