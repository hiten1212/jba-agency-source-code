<?php $this->load->view('top_application');?>
<script type="text/javascript">function serialize_form(){ return $('#myform').serialize(); }</script>
<!-- Breadcrum Starts-->
<div class="breadcrumb_outer">
<div class="container">
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
  <li class="breadcrumb-item"><a href="<?php echo site_url('brand'); ?>">Our Brands</a></li>
  <li class="breadcrumb-item active"><?php echo $res['brand_name']; ?></li>
</ol>
</div>
</div>
<!-- Breadcrumb End -->

<?php $this->load->view("banner/inner_page_top_banner");	
     //echo $res['brand_url']; 
 ?>

<div class="container">
<div class="cms">
<div>
<img src="<?php echo get_image('brand',$res["brand_image"],'180','98');?>" alt="<?php echo $res['brand_name'];?>" class="float-md-left border1 mr-3 mb-3">
<h1><?php echo $res['brand_name']; ?></h1>
<?php if($res['brand_url']!='') {?>
<p class="blue mt-2 fs15">URL: <a href="<?php echo $res['brand_url']; ?>" target="_blank"><?php echo $res['brand_url']; ?></a></p><?php } ?>
<p class="clearfix"></p>
</div>
<?php if($res['brand_description']!='') {?>
<div class="mt-2"><?php echo $res['brand_description']; ?> </div>
<?php } ?>
</div>
</div>

<script>
jQuery(document).ready(function(e) {
  jQuery('[id ^="per_page"]').live('change',function(){
		$("[id ^='per_page'] option[value=" + jQuery(this).val() + "]").attr('selected', 'selected'); jQuery('#myform').submit();
	});
});
</script>
<?php //echo front_record_per_page('per_page1', 'per_page', 'myform');;?>
<?php $this->load->view("bottom_application");?>