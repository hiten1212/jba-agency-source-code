<?php
$this->load->helper(array('string','text'));
//$friendly_url = $this->uri->rsegment(1)?$this->uri->rsegment(1):"";
$banner_left_array = array();
$sql = "SELECT * FROM wl_banners WHERE banner_image!='' AND status='1' AND (banner_page = 'products' OR banner_page = 'category')  ORDER BY  RAND() LIMIT 2";
$query = $this->db->query($sql);
if($query->num_rows() > 0){
	$result_banner = $query->result_array();
	foreach($result_banner as $val){
		if($val['banner_image']!='' && file_exists(UPLOAD_DIR."/banner/".$val['banner_image'])){
			array_push($banner_left_array,$val);
		}
	}
}

if(!empty($banner_left_array)){
	$p=1;
	foreach($banner_left_array as $key=>$val){
		$mrg_top_cls = $p > 1 ? 'mt20' : 'mt10';
		$ban_title="";
		$link_ban = ($val['banner_url']!='' ) ? $val['banner_url'] : "";
		if($link_ban!=''){
			$link_ban = !preg_match("~^http~",$link_ban) ? "http://".$link_ban : $link_ban;
		}
		if($link_ban!=''){
			?>
			<a href="<?php echo $link_ban;?>" target="_blank"><img src="<?php echo get_image('banner',$val['banner_image'],'218','179','R'); ?>" alt="<?php echo escape_chars($ban_title);?>" title="<?php echo escape_chars($ban_title);?>" class="img-responsive <?php echo $mrg_top_cls;?>"/></a>
			<?php
		}else{
			?>
			<img src="<?php echo get_image('banner',$val['banner_image'],'218','179','R'); ?>" alt="<?php echo escape_chars($ban_title);?>" title="<?php echo escape_chars($ban_title);?>" class="img-responsive <?php echo $mrg_top_cls;?>" />
			<?php
		}
		$p++;
	}
}