<?php
$this->load->helper(array('string','text'));
$banner_left_array = array();
$sql = "SELECT * FROM wl_banners WHERE banner_image!='' AND status='1' AND banner_page = 'inner_page' ORDER BY  RAND() LIMIT 1";
$query = $this->db->query($sql);
if($query->num_rows() > 0){
	$result_banner = $query->result_array();
	foreach($result_banner as $val){
		if($val['banner_image']!='' && file_exists(UPLOAD_DIR."/banner/".$val['banner_image'])){
			array_push($banner_left_array,$val);
		}
	}
}
//hidden-sm
if(!empty($banner_left_array)){
	$p=1;
	foreach($banner_left_array as $key=>$val){
		
		$ban_title="";
		$link_ban = ($val['banner_url']!='' ) ? $val['banner_url'] : "";
		if($link_ban!=''){
			$link_ban = !preg_match("~^http~",$link_ban) ? "http://".$link_ban : $link_ban;
		}		

		if($link_ban!=''){
			?>
            <div class="banner-area"><a href="<?php echo $link_ban;?>" target="_blank"><img src="<?php echo get_image('banner',$val['banner_image'],'1600','400','R'); ?>" class="img-fluid" alt=""></a></div>
			<?php
		}else{
			?>
			<div class="banner-area"><img src="<?php echo get_image('banner',$val['banner_image'],'1600','400','R'); ?>" class="img-fluid" alt=""></div>
			<?php
		}
		$p++;
	}
}