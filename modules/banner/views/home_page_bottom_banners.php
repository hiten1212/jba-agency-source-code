<?php

$this->load->helper(array('string','text'));

$banner_left_array = array();

$sql = "SELECT * FROM wl_banners WHERE banner_image!='' AND status='1' AND banner_position = 'Home Page Bottom'  ORDER BY  RAND() LIMIT 2";

$query = $this->db->query($sql);

if($query->num_rows() > 0){

	$result_banner = $query->result_array();

	foreach($result_banner as $val){

		if($val['banner_image']!='' && file_exists(UPLOAD_DIR."/banner/".$val['banner_image'])){

			array_push($banner_left_array,$val);

		}

	}

}



if(!empty($banner_left_array)){

	?>

	<!--ads-->

	<div class="mt30 mb30">

	<?php

	$p=1;

	foreach($banner_left_array as $key=>$val){

		$mrgn_cls = $p==1 ? 'class="pull-left"' : 'class="pull-left ml15"';

		$ban_title="";

		$link_ban = ($val['banner_url']!='' ) ? $val['banner_url'] : "";

		if($link_ban!=''){

			$link_ban = !preg_match("~^http~",$link_ban) ? "http://".$link_ban : $link_ban;

		}

		if($link_ban!=''){

			?>

			<div <?php echo $mrgn_cls;?>><a href="<?php echo $link_ban;?>" target="_blank"><img src="<?php echo get_image('banner',$val['banner_image'],'505','210','R'); ?>" alt="<?php echo escape_chars($ban_title);?>" title="<?php echo escape_chars($ban_title);?>"  /></a></div>

			<?php

		}else{

			?>

			<div <?php echo $mrgn_cls;?>><img src="<?php echo get_image('banner',$val['banner_image'],'505','210','R'); ?>" alt="<?php echo escape_chars($ban_title);?>" title="<?php echo escape_chars($ban_title);?>" /></div>

			<?php

		}

		$p++;

	}?>

	<div class="clearfix"></div>

	</div>

	<?php

}

