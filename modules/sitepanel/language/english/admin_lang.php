<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Global
|--------------------------------------------------------------------------
*/
$lang['activate']             = "Record has been activated successfully.";
$lang['deactivate']           = "Record has been de-activated successfully.";
$lang['deleted']              = "Record has been deleted successfully.";
$lang['successupdate']        = "Record has been updated successfully.";
$lang['order_updated']        = "The Selected Record(s) has been re-ordered.";
$lang['password_incorrect']   = "The Old Password is incorrect";
$lang['recordexits']          = "Record address already exists.";
$lang['success']              = "Record added successfully.";
$lang['paysuccess']           = "Payment added successfully.";
$lang['admin_logout_msg']     = "Logout successfully ..";
$lang['admin_mail_msg']       = "Mail sent Successfully...";
$lang['forgot_msg']           = "Email Id does not exist.";
$lang['admin_reply_msg']      = "Enquiry reply sent Successfully...";
$lang['pic_uploaded']         = 'Photos has been uploaded successfully.';
$lang['pic_uploaded_err']	  = 'Please upload at least one photo.';
$lang['pic_delete']			  = 'Photo has been deleted successfully.';

$lang['child_to_deactivate']     =  "The selected record(s) has some sub-category/product.Please de-activate them first";
$lang['child_to_activate']       =  "The selected record(s) has some sub-category/product.Please activate them first";
$lang['child_to_delete']         =  "The selected record(s) has some sub-category/product.Please delete them first";
$lang['marked_paid']        = "The selected record(s) has been marked as Paid";
$lang['marked_unpaid']        = "The selected record(s) has been marked as unpaid and the stock has been reversed successfully.";
$lang['payment_succeeded']  = "The payment has been made successfully.";
$lang['payment_failed']     = "The payment has been canceled.";
$lang['email_sent']	     = "The Email has been sent successfully to the selected Users/Members.";
$lang['verified'] = "The selected record(s) has been set as Verified";

$lang['top_menu_list'] = array( "Dashboard"=>"sitepanel/dashbord/",
/*"Manage Products"=>    array( "Manage Categories"=>"sitepanel/category/",
"Manage Products"=>"sitepanel/products",
//"Manage Service Review"=>"sitepanel/products/review",
"Manage Products Enquiries"=>"sitepanel/enquiry/products/",
),*/


"Manage Service"	=>  array( 
"Manage Services category"=>	"sitepanel/Service_category/",
//"Manage Service"=>	"sitepanel/services ",
),


"Manage Portfolio"	=>  array( 
//"Manage Gallery"=>	"sitepanel/image_gallery",
"Manage Portfolio"=>	"sitepanel/gallery",
//"Manage Video Gallery"=>	"sitepanel/video_gallery",
),

/*"Newsletter" =>array(
"Manage Newsletter"			    =>"sitepanel/newsletter/"
),

"Manage Account Forms"	=>  array( 
"Manage Order Form"=>	"sitepanel/account_forms/",
"Manage Application Form"=>	"sitepanel/account_forms/application_form_list ",
),
"Careers" =>array(
"Manage Careers"			    =>"sitepanel/careers/",
"Manage Applied Jobs"=>"sitepanel/enquiry/careers" ,
),*/

"Other Management"  =>array(
"Manage Home Content"=>"sitepanel/staticpages/",
//"Manage News"=>"sitepanel/news/" ,
//"Manage Mail Contents"=>"sitepanel/mailcontents/",
"Manage Contact Enquiries"=>"sitepanel/enquiry/" , 
//"Manage Career Enquiries"=>"sitepanel/enquiry/career/",
//"Manage project Enquiries"=>"sitepanel/enquiry/services/",
//"Manage Service Type Enquiry"=>"sitepanel/enquiry/services_type/",
//"Manage Dynamic Pages"	 =>"sitepanel/dynamic_pages/",
//"Manage Blog"=>"sitepanel/blogs/",
//"Manage News and Events"=>"sitepanel/events/" ,
//"Manage PDF Download"=>"sitepanel/broucher/",
//"Manage Banners"=>"sitepanel/banners/",
"Manage Clients"=>"sitepanel/brand/",
//"Manage City"=>"sitepanel/city/",
//"Manage Faq's"=>"sitepanel/faq/",
//"Manage Testimonials"=>"sitepanel/testimonial/",
"Manage Home Images"=>"sitepanel/header_images/",
"Manage Meta Tags"=>"sitepanel/meta/"   ,
"Manage Admin Settings"=>"sitepanel/setting/" ,
"Change Password"=>"sitepanel/setting/change" ,
"Logout"=>"sitepanel/logout"
),

);

$lang['top_menu_icon'] = array(
"Category Management"=>"lft-ico2.png",
"Manage Service"=>"lft-ico2.png",
"Manage Products"=>"lft-ico2.png",
"Members Management"=>"lft-ico3.png",
"Manage Account Forms"=>"lft-ico3.png",
"Manage Portfolio"=>"lft-ico4.png",
"Manage Clients"=>"lft-ico4.png",
"Newsletter"=>"lft-ico5.png",
"Portfolio"=>"news-lt-.png",
"Careers"=>"lft-ico6.png",
"Manage Entities"=>"lft-ico6.png",
"Other Management"=>"lft-ico7.png",
);




/* Location: ./application/modules/sitepanel/language/admin_lang.php */