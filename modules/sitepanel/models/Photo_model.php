<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Photo_model extends MY_Model{

	public function __construct()
	{
		parent::__construct();
	}

	public function getAlbumphoto($offset=FALSE,$per_page=FALSE)
	{
		$keyword = $this->db->escape_str(trim($this->input->get_post('keyword',TRUE)));
		$cat_id = $this->db->escape_str(trim($this->input->get_post('cat_id',TRUE)));
		$condtion = "status !='2'";
		if($keyword!=''){
			$condtion.=" AND title LIKE '%".$keyword."%' ";
		}
		if($cat_id!=''){
			$condtion.=" AND cat_id='$cat_id' ";
		}
		$fetch_config = array(
								'condition'=>$condtion,
								'order'=>"id DESC",
								'limit'=>$per_page,
								'start'=>$offset,							 
								'debug'=>FALSE,
								'return_type'=>"array"							  
		                     );		
		$result = $this->findAll('tbl_photo',$fetch_config);
		return $result;	
	}
	public function get_photo_by_id($id)
	{
		$id = applyFilter('NUMERIC_GT_ZERO',$id);
		if($id>0)
		{
			$condtion = "status !='2' AND id=$id";
			$fetch_config = array(
			'condition'=>$condtion,							 					 
			'debug'=>FALSE,
			'return_type'=>"object"							  
			);
			$result = $this->find('tbl_photo',$fetch_config);
			return $result;		
		}
	}	
	
	
}
// model end here