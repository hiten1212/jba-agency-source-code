<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Setting_model extends MY_Model



{



	public function __construct()



	{



		parent::__construct();



	}







	public function get_admin_info($id)



	{



		$id = (int) $id;



		if($id!='' && is_numeric($id))



		{



			$condtion = "admin_id = $id";



			$fetch_config = array(



			'condition'=>$condtion,



			'debug'=>FALSE,



			'return_type'=>"object"



			);



			



			$result = $this->find('tbl_admin',$fetch_config);



			return $result;



		}



	}



	



	public function update_info($id)



	{



		$cond = "admin_id =$id ";



		$num_row = $this->findCount('tbl_admin',$cond);
		$rowdata = $this->get_admin_info($id);

		$unlink_image1 = array('source_dir'=>"logo",'source_file'=>$rowdata->header_logo);
		$unlink_image2 = array('source_dir'=>"logo",'source_file'=>$rowdata->footer_logo);
		//$unlink_image3 = array('source_dir'=>"logo",'source_file'=>$rowdata->header_video);
                $unlink_image4 = array('source_dir'=>"companyprofile",'source_file'=>$rowdata->company_profile);
		
		//video upload
		/*$uploaded_file2 = $rowdata->header_video;
		if( !empty($_FILES) && $_FILES['header_video']['name']!='' ){
			$this->load->library('upload');
			$uploaded_data2 =  $this->upload->my_upload('header_video','logo');

			if( is_array($uploaded_data2)  && !empty($uploaded_data2) ){
				$uploaded_file2 = $uploaded_data2['upload_data']['file_name'];
				removeImage($unlink_image3);
			}
		}*/
		
			//header logo upload
		$uploaded_file = $rowdata->header_logo;
		if( !empty($_FILES) && $_FILES['header_logo']['name']!='' ){
			$this->load->library('upload');
			$uploaded_data =  $this->upload->my_upload('header_logo','logo');

			if( is_array($uploaded_data)  && !empty($uploaded_data) ){
				$uploaded_file = $uploaded_data['upload_data']['file_name'];
				removeImage($unlink_image1);
			}
		}
		// footer logo upload
		$uploaded_file1 = $rowdata->footer_logo;
		if( !empty($_FILES) && $_FILES['footer_logo']['name']!='' ){
			$this->load->library('upload');
			$uploaded_data1 =  $this->upload->my_upload('footer_logo','logo');

			if( is_array($uploaded_data1)  && !empty($uploaded_data1) ){
				$uploaded_file1 = $uploaded_data1['upload_data']['file_name'];
				removeImage($unlink_image2);
			}
		}

		



		if( $num_row > 0){


			$data     = array(
			'admin_email'=>$this->input->post('admin_email',TRUE), 
			'international_email'=>$this->input->post('international_email',TRUE), 
			'suppliers_email'=>$this->input->post('suppliers_email',TRUE), 
			'enquiry_email'=>$this->input->post('enquiry_email',TRUE), 
			//'header_video'=>$uploaded_file2,
			'header_logo'=>$uploaded_file,
			'footer_logo'=>$uploaded_file1,
			'address'=>$this->input->post('address',TRUE),
			'contact_details'=>$this->input->post('contact_details',TRUE),
			'whatsapp_number'=>$this->input->post('whatsapp_number',TRUE),
			'whatsapp_text'=>$this->input->post('whatsapp_text',TRUE),
			'international_address'=>$this->input->post('international_address',TRUE),
			'domestic_google_map'=>$this->input->post('domestic_google_map',TRUE),
			'suppliers_address'=>$this->input->post('suppliers_address',TRUE),
			'suppliers_google_map'=>$this->input->post('suppliers_google_map',TRUE),
			'international_google_map'=>$this->input->post('international_google_map',TRUE),
			'website'=>$this->input->post('website',TRUE),
			'phone'=>$this->input->post('phone',TRUE),
			'international_phone'=>$this->input->post('international_phone',TRUE),
			'suppliers_phone'=>$this->input->post('suppliers_phone',TRUE),
			'contact_phone'=>$this->input->post('contact_phone',TRUE),
			'fax'=>$this->input->post('fax',TRUE),
			'youtuble_url'=>$this->input->post('youtuble_url',TRUE),
			'city'=>$this->input->post('city',TRUE),
			'state'=>$this->input->post('state',TRUE),
			'zipcode'=>$this->input->post('zipcode',TRUE),	
			'header_background'=>$this->input->post('header_background',TRUE),
			'footer_background'=>$this->input->post('footer_background',TRUE),
			'portfolio_list_background'=>$this->input->post('portfolio_list_background',TRUE),
			'portfolio_detail_background'=>$this->input->post('portfolio_detail_background',TRUE),	
			'start_project_background'=>$this->input->post('start_project_background',TRUE),		
			'google_map'=>$this->input->post('google_map',TRUE),
			'facebook_link'=>$this->input->post('facebook_link',TRUE),
			'twitter_link'=>$this->input->post('twitter_link',TRUE),
			'linkedin_link'=>$this->input->post('linkedin_link',TRUE),
			'google_link'=>$this->input->post('google_link',TRUE),
			'youtube_link'=>$this->input->post('youtube_link',TRUE),
			'pinterest_link'=>$this->input->post('pinterest_link',TRUE),
			'instagram_link'=>$this->input->post('instagram_link',TRUE),
			'soundcloud_link'=>$this->input->post('soundcloud_link',TRUE),
			'rss_link'=>$this->input->post('rss_link',TRUE),
			'google_analytics_id'=>$this->input->post('google_analytics_id',TRUE),
			'google_web_code'=>$this->input->post('google_web_code',TRUE),
			'footer_time'=>$this->input->post('footer_time',TRUE), 
			'country'=>$this->input->post('country',TRUE),
                        'website_url'=>$this->input->post('website_url',TRUE),   
                        'footer1_header'=>$this->input->post('footer1_header',TRUE),   
                        'footer2_header'=>$this->input->post('footer2_header',TRUE),   
                        'footer3_header'=>$this->input->post('footer3_header',TRUE),       
                        'copyright_text'=>$this->input->post('copyright_text',TRUE), 
                        'logo_background'=>$this->input->post('logo_background',TRUE),       
                        'start_project_text'=>$this->input->post('start_project_text',TRUE),
                        'portfolio_menu_text'=>$this->input->post('portfolio_menu_text',TRUE),       
                        'clients_menu_text'=>$this->input->post('clients_menu_text',TRUE),   
			);

			if( !empty($_FILES) && $_FILES['brochure']['name']!='' ){
				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_upload('brochure','brochure');
				if( is_array($uploaded_data)  && !empty($uploaded_data) ){

					$uploaded_file = $uploaded_data['upload_data']['file_name'];

					

					$data2     = array('brochure'=>$uploaded_file);			

					$where = "admin_id=".$id." ";

					$this->safe_update('tbl_admin',$data2,$where,FALSE);

				}

			}

                         if( !empty($_FILES) && $_FILES['company_profile']['name']!='' ){
				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_pdf_upload('company_profile','companyprofile');
                                
				if( is_array($uploaded_data)  && !empty($uploaded_data) ){

					$uploaded_file = $uploaded_data['upload_data']['file_name'];
                                        removeImage($unlink_image4);

					$data3     = array('company_profile'=>$uploaded_file);			

					$where = "admin_id=".$id." ";

					$this->safe_update('tbl_admin',$data3,$where,FALSE);

				}

			}


			$where = "admin_id=".$id." ";



			$this->safe_update('tbl_admin',$data,$where,FALSE);



			$this->session->set_userdata('msg_type',"success" );



			$this->session->set_flashdata('success',lang('successupdate') );



		}



	}



	



	public function change_password($old_pass,$id)



	{



		



		$cond = "admin_id =$id AND admin_password ='$old_pass' ";



		$num_row = $this->findCount('tbl_admin',$cond);



		



		if( $num_row > 0){



			$data     = array('admin_password'=>$this->input->post('new_pass',TRUE));



			



			$where = "admin_id=".$id." ";



			$this->safe_update('tbl_admin',$data,$where,FALSE);



			$this->session->set_userdata('msg_type',"success" );



			$this->session->set_flashdata('success',lang('successupdate') );



		}else



		{



			$this->session->set_userdata(array('msg_type'=>'error'));



			$this->session->set_flashdata('error',lang('password_incorrect'));



		}



	}



        public function update_banner_info($id)
	{
		$cond = "admin_id =$id ";

		$num_row = $this->findCount('tbl_admin',$cond);
		$rowdata = $this->get_admin_info($id);

		$unlink_image3 = array('source_dir'=>"logo",'source_file'=>$rowdata->header_video);
		
		//video upload
		$uploaded_file2 = $rowdata->header_video;
		if( !empty($_FILES) && $_FILES['header_video']['name']!='' ){
			$this->load->library('upload');
			$uploaded_data2 =  $this->upload->my_upload('header_video','logo');

			if( is_array($uploaded_data2)  && !empty($uploaded_data2) ){
				$uploaded_file2 = $uploaded_data2['upload_data']['file_name'];
				removeImage($unlink_image3);
			}
		}

		if( $num_row > 0){

                        $video_on_off = $this->input->post('video_on_off',TRUE) == ""?0:$this->input->post('video_on_off',TRUE);
                        
			$data     = array(
			'banner_slogan'=>$this->input->post('banner_slogan',TRUE),
                        'banner_text_color'=>$this->input->post('banner_text_color',TRUE),    
                         'video_on_off'=>$video_on_off,   
			'header_video'=>$uploaded_file2  
			);

			$where = "admin_id=".$id." ";

			$this->safe_update('tbl_admin',$data,$where,FALSE);

			$this->session->set_userdata('msg_type',"success" );

			$this->session->set_flashdata('success',lang('successupdate') );



		}



	}


          public function updatecompanyprofilestatus($id, $status){
            $data2     = array('companyprofile_status'=>$status);			

            $where = "admin_id=".$id." ";

            $this->safe_update('tbl_admin',$data2,$where,FALSE);
            
            $this->session->set_userdata('msg_type',"success" );
            
            $this->session->set_flashdata('success',lang('successupdate') );
        }
        
        public function updatecompanyprofile($id){
            $data2     = array('companyprofile_status'=>0, 'company_profile'=>"");			
            

            $where = "admin_id=".$id." ";

            $this->safe_update('tbl_admin',$data2,$where,FALSE);
            
            $this->session->set_userdata('msg_type',"success" );
            
            $this->session->set_flashdata('success',lang('successupdate') );
        }






}



// model end here