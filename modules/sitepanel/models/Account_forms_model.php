<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_forms_model extends MY_Model
{
	public function get_order_form($limit='10',$offset='0',$param=array())
	{
		$status = @$param['status'];
		$id = @$param['id'];
		$keyword = trim($this->input->get_post('keyword',TRUE));
		$keyword = $this->db->escape_str($keyword);
		if($status!='')
		{
			$this->db->where("status","$status");
		}

		if($id!='')
		{
			$this->db->where("order_id","$id");
		}
		
		if($keyword!='')
		{
			$this->db->like('first_name',"$keyword");
		}
		
		$this->db->order_by('order_id',"DESC");
		$this->db->limit($limit,$offset);

		$this->db->select('SQL_CALC_FOUND_ROWS*',FALSE);
		$this->db->from('wl_order');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? $result[0]: $result;
		return $result;
	}
	
	
	public function get_application_form_list($limit='10',$offset='0',$param=array())
	{
		$status = @$param['status'];
		$id = @$param['id'];
		$keyword = trim($this->input->get_post('keyword',TRUE));
		$keyword = $this->db->escape_str($keyword);
		if($status!='')
		{
			$this->db->where("status","$status");
		}

		if($id!='')
		{
			$this->db->where("application_id","$id");
		}
		
		if($keyword!='')
		{
			$this->db->like('name',"$keyword");
		}
		
		$this->db->order_by('application_id',"DESC");
		$this->db->limit($limit,$offset);

		$this->db->select('SQL_CALC_FOUND_ROWS*',FALSE);
		$this->db->from('wl_application_form');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? $result[0]: $result;
		return $result;
	}
}?>