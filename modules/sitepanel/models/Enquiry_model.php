<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Enquiry_model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_enquiry($offset,$per_page,$condition='')
	{
		$status_flag=FALSE;
		
		$fetch_config = array(
							  'condition'=>$condition,
							  'order'=>"id DESC",
							  'limit'=>$per_page,
							  'start'=>$offset,							 
							  'debug'=>FALSE,
							  'return_type'=>"array"							  
							  );		
		$result = $this->findAll('wl_enquiry',$fetch_config);
		return $result;	
		
	}
	
	public function getemail_by_id($id_array)
	{	
		$id_array=explode(',',$id_array);	
			
		if(is_array($id_array))
		{	 
			$emailarray='';			
			foreach($id_array as $value)
			{			
				$query = $this->db->query("SELECT * FROM  wl_enquiry WHERE id='$value'");				
				foreach ($query->result() as $row)
				{
					$emailarray=$row->email.','.$emailarray;				
				}				
			}
			return $emailarray;		
		}		
	}
	
	public function get_admin_email()
	{	 
		$query = $this->db->query("SELECT * FROM tbl_admin ");	
			
		if ($query->num_rows() > 0)
		{	
			
			return $query->row_array();		
		}	
	}


	
}
// model end here