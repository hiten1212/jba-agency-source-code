<?php
class Gallery extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('gallery/gallery_model'));  
		$this->config->set_item('menu_highlight','gallery management');	
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		
	}
	
	public  function index($page = NULL)
	{
		
			$parent_id            =  (int) $this->uri->segment(4,0);			
			$condtion             =   array('type'=>'1');			
			$pagesize             =  (int) $this->input->get_post('pagesize');				
			$config['limit']	  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');								
			$offset               =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;				
			$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));			
			$status			      =   $this->input->get_post('status',TRUE);	
			$gallery_title='';			
			if($parent_id > 0 )
			{
			$condtion['parent_id'] = $parent_id;
			 $gallery_title = get_db_field_value('wl_gallery','gallery_title'," AND gallery_id='$parent_id'");
			}
			
			if($status!='')
			{
			$condtion['status'] = $status;
			}
			
			$condtion['type'] = array(1,2);
			
			$res_array               =  $this->gallery_model->get_all_gallery($config['limit'],$offset,$condtion);	
			//echo_sql();
			$config['total_rows']    =  get_found_rows();	
			
			$data['heading_title']  =  ( $parent_id > 0 ) ? $gallery_title :  'Portfolio Listing';
			
			$data['pagelist']             =  $res_array; 	
			
			$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);	
			
			
			if( $this->input->post('status_action')!='')
			{	
			
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v){
					
					//$total_photo  = count_photo("AND parent_id='$v' ");
					$total_photo  = count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$v'");
					if( $total_photo>0)
					{
						
					}else
					{
					$where = array('entity_type'=>'gallery/details','entity_id'=>$v);
					
					$this->gallery_model->safe_delete('wl_meta_tags',$where,TRUE);
					//$this->gallery_model->safe_delete('wl_gallery',array('gallery_id'=>$v),TRUE);
					}
					}
				
			}		
			$this->update_status('wl_gallery','gallery_id');			
			}
			
			if( $this->input->post('update_order',TRUE)!='')
		{
			$this->update_displayOrder('wl_gallery','sort_order','gallery_id');
		}
			
			$data['parent_id']             =  $parent_id ;
			$this->load->view('gallery/view_gallery_list',$data);	
			
			
	}
	
	
	
	public function add()
	{			
			
			$parent_id         =  (int) $this->uri->segment(4,0);


			$data['heading_title']  =  ( $parent_id > 0 ) ? 'Add Portfolio' :  'Add Photo Portfolio';
			
			$posted_friendly_url = $this->input->post('friendly_url');
		    $this->cbk_friendly_url = seo_url_title($posted_friendly_url);
			
			$seo_url_length = $this->config->item('seo_url_length');
			
		 	$img_allow_size =  $this->config->item('allow.file.size');
		 	$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		 	$data['ckeditor']    =  set_ck_config(array('textarea_id'=>'description'));		
	       // $this->form_validation->set_rules('gallery_title','Title',"required|xss_clean|max_length[250]|unique[wl_gallery.gallery_title='".$this->db->escape_str($this->input->post('gallery_title'))."' AND status!='2']");
			
		
		if($parent_id < 1){		
			$this->form_validation->set_rules('friendly_url','Page URL',"required|max_length[$seo_url_length]|callback_checkurl");
			}
			$this->form_validation->set_rules('gallery_title','Title',"required|max_length[250]|unique[wl_gallery.gallery_title='".$this->db->escape_str($this->input->post('gallery_title'))."' AND status!='2']");
			if($parent_id ==0){	
			$this->form_validation->set_rules('description','Description',"max_length[25000]");
			}
			else{
				
					$this->form_validation->set_rules('description','Description',"max_length[25000]");
				$this->form_validation->set_rules('portfolio_type','Portfolio Type',"required");
			}
		
		
		$this->form_validation->set_rules('gallery_alt','Alt',"max_length[100]");			
		// $this->form_validation->set_rules('gallery_image','Image',"required|file_required|file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");			
			if($this->form_validation->run()==TRUE)
			{				
			$redirect_url = "gallery/details";
			$gallery_alt = $this->input->post('gallery_alt');
			if($gallery_alt =='')
			{
			  $gallery_alt = $this->input->post('gallery_title');
			}
				
				 $uploaded_file = "";					
			    if( !empty($_FILES) && $_FILES['gallery_image']['name']!='' )
				{			  
					$this->load->library('upload');							
					$uploaded_data =  $this->upload->my_upload('gallery_image','gallery');				
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
						$uploaded_file = $uploaded_data['upload_data']['file_name'];					
					}	 
				}
				
				  $image_name = ($this->input->post('gal_image_name')!="")?$this->input->post('gal_image_name'):$uploaded_file;
			
			      $posted_data = array(
					'gallery_title'=>$this->input->post('gallery_title',TRUE),
					'description'=>$this->input->post('description'),
					'parent_id' =>$parent_id,
					'gallery_alt'=>$gallery_alt,
                    'friendly_url' => $this->cbk_friendly_url,
					'post_date'=>$this->config->item('config.date.time'),
					'status'=>'1',
					'type'=>$this->input->post('portfolio_type'),
				 );

			  if($this->input->post('portfolio_type') == 1){
                                    $posted_data['gallery_image'] =$image_name;
                                } else if($this->input->post('portfolio_type') == 2){
                                   $uploadedvideo_file = "";					
                                    if( !empty($_FILES) && $_FILES['video_thumbnail']['name']!='' )
                                        {			  
                                                $this->load->library('upload');							
                                                $uploaded_data =  $this->upload->my_upload('video_thumbnail','gallery');				
                                                if( is_array($uploaded_data)  && !empty($uploaded_data) )
                                                { 								
                                                        $uploadedvideo_file = $uploaded_data['upload_data']['file_name'];					
                                                }	 
                                        }
				
				  $video_thumbnail = ($this->input->post('video_thumbnail')!="")?$this->input->post('video_thumbnail'):$uploadedvideo_file;
                                  
                                  $posted_data['video_thumbnail'] =$video_thumbnail;
                                  $posted_data['video_url'] =$this->input->post('video_url');
                              }
			  
			 $galleryId = $this->gallery_model->safe_insert('wl_gallery',$posted_data,FALSE);
			 
			 if( $galleryId > 0 )
			{

			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$galleryId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('gallery_title'),80),
							  'meta_description'=>get_text($this->input->post('gallery_title')),
							  'meta_keyword'=>get_keywords($this->input->post('gallery_title'))
							  );

if($parent_id < 1){
			  create_meta($meta_array);
}
			}
			 					 
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('success'));			
				$redirect_path= ($parent_id>0) ? 'gallery/index/'.$parent_id : 'gallery';
				redirect('sitepanel/'.$redirect_path, '');
				
			
			}
			$data['parent_id'] = $parent_id; 
			
			if($parent_id > 0){
				
			$this->load->view('gallery/view_photo_add',$data);	
				
			}else{
			
			$this->load->view('gallery/view_gallery_add',$data);	
			
			}
	   
	   }
	
	 
	public function edit($productId)
	{
	     
		  	$img_allow_size =  $this->config->item('allow.file.size');
			$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
			$data['ckeditor']    =  set_ck_config(array('textarea_id'=>'description'));
			 $Id = (int) $this->uri->segment(4);		
			$res = $this->gallery_model->get_gallery_by_id($Id);			
					
		   if(  is_array($res) && !empty($res) )  { 
		   
		   $posted_friendly_url = $this->input->post('friendly_url');		
		   $this->cbk_friendly_url = seo_url_title($posted_friendly_url);		
		   $seo_url_length = $this->config->item('seo_url_length');		   
		// $this->form_validation->set_rules('gallery_title','Title',"required|xss_clean|max_length[250]|unique[wl_gallery.gallery_title='".$this->db->escape_str($this->input->post('gallery_title'))."' AND status!='2' AND gallery_id != '".$Id."']");
	
				
	//if($res['parent_id'] < 1){
		$this->form_validation->set_rules('gallery_title','Title',"required|max_length[250]|unique[wl_gallery.gallery_title='".$this->db->escape_str($this->input->post('gallery_title'))."' AND status!='2' AND gallery_id != '".$Id."']");
			//$this->form_validation->set_rules('friendly_url','Page URL',"required|max_length[$seo_url_length]|callback_checkurl");
			//$this->form_validation->set_rules('description','Description',"required|max_length[25000]");	
		//}
		
		if($res['parent_id'] == 0){	
			$this->form_validation->set_rules('description','Description',"max_length[25000]");
			}
			else{
				
				$this->form_validation->set_rules('description','Description',"max_length[25000]");
				$this->form_validation->set_rules('portfolio_type','Portfolio Type',"required");
			}
		
		  $this->form_validation->set_rules('gallery_alt','Alt',"max_length[100]");				
		//  $this->form_validation->set_rules('gallery_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");		 					
		if($this->form_validation->run()==TRUE)
		{		
				if($this->input->post('portfolio_type') == 1){
                        $uploaded_file = $res['gallery_image'];				 
                        $unlink_image = array('source_dir'=>"gallery",'source_file'=>$res['gallery_image']);


                        if($this->input->post('cat_img_delete')==='Y')
                         {					
                                removeImage($unlink_image);						
                                $uploaded_file = NULL;	

                         }				
                         if( !empty($_FILES) && $_FILES['gallery_image']['name']!='' )
                         {			  
                                        $this->load->library('upload');	

                                        $uploaded_data =  $this->upload->my_upload('gallery_image','gallery');

                                        if( is_array($uploaded_data)  && !empty($uploaded_data) )
                                        { 								
                                                $uploaded_file = $uploaded_data['upload_data']['file_name'];
                                            removeImage($unlink_image);	
                                        }

                        }
                    }
                    
                    if($this->input->post('portfolio_type') == 2){
                        $uploaded_video_file = $res['video_thumbnail'];				 
                        $unlink_videoimage = array('source_dir'=>"gallery",'source_file'=>$res['video_thumbnail']);
                         if( !empty($_FILES) && $_FILES['video_thumbnail']['name']!='' )
                         {			  
                                        $this->load->library('upload');	

                                        $uploaded_data =  $this->upload->my_upload('video_thumbnail','gallery');

                                        if( is_array($uploaded_data)  && !empty($uploaded_data) )
                                        { 								
                                                $uploaded_video_file = $uploaded_data['upload_data']['file_name'];
                                            removeImage($unlink_videoimage);	
                                        }

                        }
                    }
			        		
				
				  $gallery_alt = $this->input->post('gallery_alt');

				if($gallery_alt =='')
				{
				  $gallery_alt = $this->input->post('gallery_alt');
				}	
					 						
						$posted_data = array(
						'gallery_title'=>$this->input->post('gallery_title',TRUE),
						'description'=>$this->input->post('description'),
					    'gallery_alt'=>$gallery_alt,
			            'friendly_url'=>$this->cbk_friendly_url,
						'type'=>$this->input->post('portfolio_type'),
						);

					 if($this->input->post('portfolio_type') == 1){
                                               $image_name = ($this->input->post('gal_image_name')!="")?$this->input->post('gal_image_name'):$uploaded_file;

                                                $posted_data['gallery_image'] =$image_name;
                                            } else if($this->input->post('portfolio_type') == 2){
                                                $video_thumbnail = ($this->input->post('video_thumbnail')!="")?$this->input->post('video_thumbnail'):$uploaded_video_file;

                                              $posted_data['video_thumbnail'] =$video_thumbnail;
                                              $posted_data['video_url'] =$this->input->post('video_url');
                                          }

						//print_r($posted_data);die;
						$where = "gallery_id = '".$res['gallery_id']."'"; 						
						$this->gallery_model->safe_update('wl_gallery',$posted_data,$where,FALSE);	
						
						if($res['parent_id'] < 1){
						update_meta_page_url('gallery/details',$res['gallery_id'],$this->cbk_friendly_url);
						}
						$this->session->set_userdata(array('msg_type'=>'success'));
						$this->session->set_flashdata('success',lang('successupdate'));	
						$redirect_path= $res['parent_id']>0 ? 'gallery/index/'. $res['parent_id'] : 'gallery';
								
						redirect('sitepanel/'.$redirect_path.'/'.query_string(), ''); 	
						
					
					}
				$data['heading_title']  =  ( $res['parent_id'] > 0 ) ? 'Edit Portfolio' :  'Edit Portfolio';
			    $data['res']=$res;
				if($res['parent_id'] > 0){
					
				$this->load->view('gallery/view_photo_edit',$data);	
					
				}else{ 
				
			    $this->load->view('gallery/view_gallery_edit',$data);
				
				}
				
		   }else
		   {
			   
			  redirect('sitepanel/gallery', ''); 	 
			   
		   }
		   
	 }
	 
	 
	 public function checkurl(){
	  
	  		$product_id=(int)$this->input->post('gallery_id');
			
			if($product_id!=''){
				$cont='and entity_id !='.$product_id;
			}else{
				$cont='';
			}
			
	 		$posted_friendly_url = $this->input->post('friendly_url');
		
			$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
			
			$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();
			
			if($urlcount>0)
			{
				$this->form_validation->set_message('checkurl', 'URL already exists.');
			    return FALSE;
								
			}else
			{
				 return TRUE;
			}
  
  } 
	
	
}
// End of controller