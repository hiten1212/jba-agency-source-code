<?php
class Brand extends Admin_Controller
{

	public $current_controller;
	public function __construct()
	{

		parent::__construct();
		$this->load->model(array('brand/brand_model'));
		$this->load->helper('category/category');
		$this->config->set_item('menu_highlight','manage entities');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");

		$this->current_controller = $this->router->fetch_class();

	}

	public  function index()
	{

		 $pagesize               =  (int) $this->input->get_post('pagesize');



	     $config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');



		 $offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;



		 $base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));



		 $parent_id              =   (int) $this->uri->segment(4,0);







		 $condtion = " ";



		 $where="";



		 $keyword = trim($this->input->get_post('keyword',TRUE));



		 $keyword = $this->db->escape_str($keyword);



		 if(!empty($keyword)){



			 $where="brand_name like '%$keyword%'";



		 }



		 



		$condtion_array = array(



		                'field' =>"*",



						 'condition'=>$condtion,



						 'where'=>$where,



						 'order'=>"brand_name ASC",



						 'limit'=>$config['limit'],



						  'offset'=>$offset	,



						  'debug'=>FALSE



						 );



		$res_array              =  $this->brand_model->get_brands($condtion_array);







		$config['total_rows']	=  $this->brand_model->total_rec_found;







		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);


		$data['heading_title']  =  'Clients';

		$data['res']            =  $res_array;

		$data['parent_id']      =  $parent_id;

		if( $this->input->post('status_action')!='')
		{
			if( $this->input->post('status_action')=='Delete'){
				$prod_id=$this->input->post('arr_ids');

				foreach($prod_id as $v){
					$imageName=get_db_field_value("wl_brands","brand_image"," AND brand_id='".$v."'");
					$unlink_image = array('source_dir'=>"brand",'source_file'=>$imageName);
					removeImage($unlink_image);
					$where = array('entity_type'=>'brand/detail','entity_id'=>$v);
					$this->brand_model->safe_delete('wl_meta_tags',$where,TRUE);
				}
			}

			$this->update_status('wl_brands','brand_id');

		}

		if( $this->input->post('update_order')!='')
		{
			$this->update_displayOrder('wl_brands','sort_order','brand_id');

		}
		if( $this->input->post('set_as')!='' )
		{
		    $set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_brands','brand_id',array($set_as=>'1'));
		}


		if( $this->input->post('unset_as')!='' )
		{
		    $unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_brands','brand_id',array($unset_as=>'0'));
		}

		$this->load->view('brand/view_brand_list',$data);
	}

	public function add(){

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));

		$data['heading_title'] = 'Add client';
		//$this->form_validation->set_rules('brand_name','Partnership Name',"trim|required|max_length[32]|unique[wl_brands.brand_name='".$this->db->escape_str($this->input->post('brand_name'))."' AND status!='2']");

		//$this->form_validation->set_rules('brand_url','URL',"trim");

		//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		
		//$this->form_validation->set_rules('brand_description','Description',"max_length[25000]");
		//$this->form_validation->set_rules('brand_image','Image',"required|file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		
		$this->form_validation->set_rules('brand_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");


		if($this->form_validation->run()===TRUE)
		{

			$uploaded_file = "";
			if( !empty($_FILES) && $_FILES['brand_image']['name']!='' ){

				$this->load->library('upload');

				$uploaded_data =  $this->upload->my_upload('brand_image','brand');

				if( is_array($uploaded_data)  && !empty($uploaded_data) ){

					$uploaded_file = $uploaded_data['upload_data']['file_name'];
				}
			}
			$image_name = ($this->input->post('brand_image_name')!="")?$this->input->post('brand_image_name'):$uploaded_file;
			$posted_data = array(
			'brand_name'=>$this->input->post('brand_name'),
			'brand_url'=>$this->input->post('brand_url'),
			'brand_description'=>$this->input->post('brand_description'),
			'friendly_url'		=>	$this->cbk_friendly_url,
			'brand_date_added'=>$this->config->item('config.date.time'),
			'brand_image'=>@$image_name
			);
 
			$posted_data = $this->security->xss_clean($posted_data);

			$entityId =  $this->brand_model->safe_insert('wl_brands',$posted_data,FALSE);

			if($entityId>0) {	
			
				$redirect_url="brand/detail";

				$meta_array  = array(
				'entity_type'		=>	$redirect_url,
				'entity_id'		=>	$entityId,
				'page_url'		=>	$this->cbk_friendly_url,
				'meta_title'		=>	get_text($this->input->post('brand_name'),80),
				'meta_description'=>	get_text($this->input->post('brand_description')),
				'meta_keyword'	=>	get_keywords($this->input->post('brand_description'))
				);

				//create_meta($meta_array);
			}

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/brand', '');

		}

		$this->load->view('brand/view_brand_add',$data);
	}


	public function edit()
	{
		$brandId = (int) $this->uri->segment(4);
		$rowdata=$this->brand_model->get_brand_by_id($brandId);

		$brandId = $rowdata['brand_id'];	
		$data['heading_title'] = 'Edit client';
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));
		
		$img_allow_size =  $this->config->item('allow.file.size');

		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

		$seo_url_length = $this->config->item('seo_url_length');
		
		if( !is_array($rowdata) )
		{
			$this->session->set_flashdata('message', lang('idmissing'));
			redirect('sitepanel/brand', '');

		}

			//$this->form_validation->set_rules('brand_name','Partnership Name',"trim|required|max_length[32]|unique[wl_brands.brand_name='".$this->db->escape_str($this->input->post('brand_name'))."' AND status!='2' AND brand_id != ".$brandId."]");

			//$this->form_validation->set_rules('brand_url','URL',"trim");		

			//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
			//$this->form_validation->set_rules('brand_description','Description',"max_length[25000]");
			
			$this->form_validation->set_rules('brand_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");


			if($this->form_validation->run()==TRUE)
			{
				$uploaded_file = $rowdata['brand_image'];
				$unlink_image = array('source_dir'=>"brand",'source_file'=>$rowdata['brand_image']);

			 	if($this->input->post('brand_img_delete')==='Y')
				 {
					removeImage($unlink_image);
					$uploaded_file = NULL;
				 }

				 if( !empty($_FILES) && $_FILES['brand_image']['name']!='' )

				 {
						$this->load->library('upload');

						$uploaded_data =  $this->upload->my_upload('brand_image','brand');

						if( is_array($uploaded_data)  && !empty($uploaded_data) )
						{
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
						    removeImage($unlink_image);
						}

				}

				$image_name = ($this->input->post('brand_image_name')!="")?$this->input->post('brand_image_name'):$uploaded_file;
				$posted_data = $this->security->xss_clean($posted_data);
				$posted_data = array(

					'brand_name'=>$this->input->post('brand_name'),
					'brand_url'=>$this->input->post('brand_url'),
					'brand_description'=>$this->input->post('brand_description'),
					'friendly_url'		=>	$this->cbk_friendly_url,
					'brand_image'=>@$image_name
				 );

			 	$where = "brand_id = '".$brandId."'";
				$this->brand_model->safe_update('wl_brands',$posted_data,$where,FALSE);

				$id=$brandId;
				//update_meta_page_url('brand/detail',$id,$this->cbk_friendly_url);			

				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/brand'.'/'.query_string(), '');
			}

		$data['edit_result']=$rowdata;
		$this->load->view('brand/view_brand_edit',$data);

	}

	public function checkurl(){

		$brand_id=(int)$this->input->post('brand_id');


		if($brand_id!=''){

			$cont='and entity_id !='.$brand_id;
		}else{
			$cont='';
		}


		$posted_friendly_url = $this->input->post('friendly_url');
		$cbk_friendly_url = seo_url_title($posted_friendly_url);


		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$cbk_friendly_url."'".$cont."")->num_rows();

	if($urlcount>0)
		{
			$this->form_validation->set_message('checkurl', 'URL already exists.');
			return FALSE;
		}else
		{
			return TRUE;
		}
	}
}


// End of controller