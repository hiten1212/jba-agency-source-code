<?php

class Broucher extends Admin_Controller

{

	public $current_controller;

	

	public function __construct()

	{

		parent::__construct();

		$this->load->model(array('broucher/broucher_model'));

		$this->load->helper('category/category');

		$this->config->set_item('menu_highlight','manage entities');

		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");

		$this->current_controller = $this->router->fetch_class();

	}



	public  function index()

	{

		 $pagesize               =  (int) $this->input->get_post('pagesize');

	     $config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		 $offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		 $base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));

		 $parent_id              =   (int) $this->uri->segment(4,0);



		 $condtion = " ";

		 $where="";

		 $keyword = trim($this->input->get_post('keyword',TRUE));

		 $keyword = $this->db->escape_str($keyword);

		 if(!empty($keyword)){

			 $where="broucher_name like '%$keyword%'";

		 }

		 

		$condtion_array = array(

		                'field' =>"*",

						 'condition'=>$condtion,

						 'where'=>$where,

						 'order'=>"broucher_name ASC",

						 'limit'=>$config['limit'],

						  'offset'=>$offset	,

						  'debug'=>FALSE

						 );

		$res_array              =  $this->broucher_model->get_brouchers($condtion_array);



		$config['total_rows']	=  $this->broucher_model->total_rec_found;



		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);



		$data['heading_title']  =  'PDF Download';



		$data['res']            =  $res_array;



		$data['parent_id']      =  $parent_id;





		if( $this->input->post('status_action')!='')

		{

			$this->update_status('wl_brouchers','broucher_id');

		}

		if( $this->input->post('update_order')!='')

		{

			$this->update_displayOrder('wl_brouchers','sort_order','broucher_id');

		}



		if( $this->input->post('set_as')!='' )

		{

		    $set_as    = $this->input->post('set_as',TRUE);

			$this->set_as('wl_brouchers','broucher_id',array($set_as=>'1'));

		}



		if( $this->input->post('unset_as')!='' )

		{

		    $unset_as   = $this->input->post('unset_as',TRUE);

			$this->set_as('wl_brouchers','broucher_id',array($unset_as=>'0'));

		}



		$this->load->view('broucher/view_broucher_list',$data);





	}



	public function add(){

		$img_allow_size =  $this->config->item('allow.doc.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$data['heading_title'] = 'Add PDF Download';

		$this->form_validation->set_rules('broucher_name','PDF Download Name',"trim|required|max_length[32]|unique[wl_brouchers.broucher_name='".$this->db->escape_str($this->input->post('broucher_name'))."' AND status!='2']");

		//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		//$this->form_validation->set_rules('gallery_file','Broucher image',"required|file_allowed_type[image]");

		$this->form_validation->set_rules('broucher_doc','PDF Download',"required|file_size_max[$img_allow_size]|file_allowed_type[document]");

		if($this->form_validation->run()===TRUE){

			
			$posted_data = array(
			'broucher_name'=>$this->input->post('broucher_name'),
			//'friendly_url'		=>	$this->cbk_friendly_url,
			'broucher_date_added'=>$this->config->item('config.date.time')
			);
			
			if($_FILES["broucher_doc"]["name"]!=""){
					$this->load->library('upload');
					$uploaded_file="";
					$uploaded_data =  $this->upload->my_upload('broucher_doc','brouchers');
					if( is_array($uploaded_data)  && !empty($uploaded_data)){
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
						$posted_data["broucher_doc"]=$uploaded_file;
						@unlink(UPLOAD_DIR."/brouchers/".$res["broucher_doc"]);
					}
			}
			
			$uploaded_file = "";	
			    if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){						

					$this->load->library('upload');		
					$uploaded_data =  $this->upload->my_upload('gallery_file','brouchers');				

					if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 	
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
						$posted_data["gallery_file"]=$uploaded_file;
						
					}	
				}

			$posted_data = $this->security->xss_clean($posted_data);
			$entityId =  $this->broucher_model->safe_insert('wl_brouchers',$posted_data,FALSE);
			
			/*if($entityId>0) {	
				$redirect_url="products/broucher/".$entityId;
				$meta_array  = array(
				'entity_type'		=>	$redirect_url,
				'entity_id'		=>	$entityId,
				'page_url'		=>	$this->cbk_friendly_url,
				'meta_title'		=>	get_text($this->input->post('broucher_name'),80),
				'meta_description'=>	get_text($this->input->post('news_description')),
				'meta_keyword'	=>	get_keywords($this->input->post('news_description'))
				);
				create_meta($meta_array);
			}*/

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/broucher', '');
		}
		$this->load->view('broucher/view_broucher_add',$data);
	}

	public function edit(){

		$broucherId = (int) $this->uri->segment(4);
		$rowdata=$this->broucher_model->get_broucher_by_id($broucherId);
		$broucherId = $rowdata['broucher_id'];
		$data['heading_title'] = 'Edit PDF Download';
		$img_allow_size =  $this->config->item('allow.doc.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		//$posted_friendly_url = $this->input->post('friendly_url');
		//$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		//$seo_url_length = $this->config->item('seo_url_length');

		if( !is_array($rowdata) ){
			$this->session->set_flashdata('message', lang('idmissing'));
			redirect('sitepanel/broucher', '');
		}

			$this->form_validation->set_rules('broucher_name','PDF Download Name',"trim|required|max_length[32]|unique[wl_brouchers.broucher_name='".$this->db->escape_str($this->input->post('broucher_name'))."' AND status!='2' AND broucher_id != ".$broucherId."]");

			//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
			//$this->form_validation->set_rules('gallery_file','Broucher image',"required|file_allowed_type[image]");
			$this->form_validation->set_rules('broucher_doc','PDF Download',"required|file_size_max[$img_allow_size]|file_allowed_type[document]");

			if($this->form_validation->run()==TRUE){
				
				$uploaded_file = $rowdata['gallery_file'];				 

					$unlink_image = array('source_dir'=>"brouchers",'source_file'=>$rowdata['gallery_file']);

													

					if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){			  

						  

						$this->load->library('upload');					

						$uploaded_data =  $this->upload->my_upload('gallery_file','brouchers');

						

						if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 								

						   $uploaded_file = $uploaded_data['upload_data']['file_name'];

						   removeimage($unlink_image);	

						}

					

				    }else{

						 $uploaded_file = $rowdata['gallery_file'];

					}
				
				
				
				$posted_data = array(
					'broucher_name'=>$this->input->post('broucher_name'),
					'gallery_file'	=>	$uploaded_file
					//'friendly_url'		=>	$this->cbk_friendly_url
				 );
				 
				if($_FILES["broucher_doc"]["name"]!=""){
						$this->load->library('upload');
						$uploaded_file="";
						$uploaded_data =  $this->upload->my_upload('broucher_doc','brouchers');
						if( is_array($uploaded_data)  && !empty($uploaded_data)){
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
							$posted_data["broucher_doc"]=$uploaded_file;
							@unlink(UPLOAD_DIR."/brouchers/".$res["broucher_doc"]);
						}
				}
				$posted_data = $this->security->xss_clean($posted_data);
			
			 	$where = "broucher_id = '".$broucherId."'";
				$this->broucher_model->safe_update('wl_brouchers',$posted_data,$where,FALSE);
				
				/*$id=$broucherId;
				update_meta_page_url('products/broucher/'.$id,$id,$this->cbk_friendly_url);	*/	
				
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));

				redirect('sitepanel/broucher'.'/'.query_string(), '');
			}

		$data['edit_result']=$rowdata;
		$this->load->view('broucher/view_broucher_edit',$data);

	}

	
	public function delete_pdf(){
		
		$pId=(int)$this->uri->segment(4,0);		
		if($pId > 0){
			
			$pdf_name='broucher_doc';
			$file=get_db_field_value('wl_brouchers', $pdf_name, array("broucher_id"=>$pId));
			if($file !="" && @file_exists(UPLOAD_DIR."/brouchers/".$file)){
								
				@unlink(UPLOAD_DIR."/brouchers/".$file);
				
				$pproduct_data = array(
					'broucher_doc'=>""
				);

				$where = "broucher_id = '".$pId."' ";
				$this->broucher_model->safe_update('wl_brouchers',$pproduct_data,$where,FALSE);				
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success','File Deleted!');
				
				redirect('sitepanel/broucher/edit/'.$pId, '');	
					
			}else{
				redirect('broucher', '');
			}
		}else{
			redirect('broucher', '');
		}
	}
	

	public function checkurl(){

		$broucher_id=(int)$this->input->post('broucher_id');

		

		if($broucher_id!=''){

			$cont='and entity_id !='.$broucher_id;

		}else{

			$cont='';

		}

		

		$posted_friendly_url = $this->input->post('friendly_url');

		$cbk_friendly_url = seo_url_title($posted_friendly_url);

		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$cbk_friendly_url."'".$cont."")->num_rows();

		

		if($urlcount>0)

		{

			$this->form_validation->set_message('checkurl', 'URL already exists.');

			return FALSE;

		}else

		{

			return TRUE;

		}

		

	}





}

// End of controller