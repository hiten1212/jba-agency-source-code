<?php

class Location extends Admin_Controller {
	public $view_path;
	public $current_controller;
	public function __construct() {
		parent::__construct();
		$this->current_controller = $this->router->fetch_class();
		$this->load->model(array('location_model'));
		$this->load->helper(array('download'));
		$this->config->set_item('menu_highlight', 'location');
		$this->view_path = $this->current_controller . "/";
	}
	public function index() {

		if ($this->input->post('status_action') != '') {
			$this->update_status('wl_countries', 'id');
		}

		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter' => 'result'), array('per_page'));
		$keyword = trim($this->input->get_post('keyword', TRUE));
		$keyword = $this->db->escape_str($keyword);
		$condtion = " ";
		if ($keyword != '') {
			$condtion = "AND country_name like '%" . $keyword . "%'";
		}
		$condtion_array = array(
        					'condition' => $condtion,
       						'limit' => $config['limit'],
        					'offset' => $offset,
        					'debug' => FALSE
		);
		$res_array = $this->location_model->get_record($condtion_array);
		$config['total_rows'] = $this->location_model->total_rec_found;
		$data['page_links'] = admin_pagination($base_url, $config['total_rows'], $config['limit'], $offset);
		$data['heading_title'] = "Manage Country";
		$data['res'] = $res_array;
		$data['includes'] = $this->view_path . 'list_vew';
		$this->load->view('includes/sitepanel_container', $data);
	}
	public function add_edit() {
		$id = (int) $this->uri->segment(4, 0);
		$row_data = '';

		if ($id > 0) {
			$row_data = $this->location_model->get_record_by_id($id);
			$operation_allowed=operation_allowed("wl_countries",$id);

			if(!$operation_allowed){
				$this->session->set_userdata(array('msg_type'=>'error'));
				$this->session->set_flashdata('error',lang('operation_not_allowed'));
				redirect('sitepanel/location');
			}
		}
		$data['row'] = $row_data;
		$data['parentData'] = '';
		$data['heading_title'] = ($id > 0) ? 'Edit Country' : 'Add Country';
		$data['cancel_url'] 	 = "sitepanel/".$this->current_controller;
		if (is_object($row_data)) {
			$this->form_validation->set_rules('country_name', 'Country Name', "trim|required|max_length[100]|unique[wl_countries.country_name='" . $this->db->escape_str($this->input->post('country_name')) . "' AND wl_countries.status!='2' AND wl_countries.id !=$id]");		} else {
			$this->form_validation->set_rules('country_name', 'Country Name', "trim|required|max_length[100]|unique[wl_countries.country_name='" . $this->db->escape_str($this->input->post('country_name')) . "' AND wl_countries.status!='2']");		}
		if ($this->form_validation->run() === TRUE) {
			if ($id > 0) {
				$posted_data = array(
            					'country_name' => $this->input->post('country_name'),
            					'country_temp_name' => url_title($this->input->post('country_name'),'dash',true),				            	'cont_currency' => $this->input->post('cont_currency')
				);
				$this->location_model->safe_update('wl_countries', $posted_data, "id ='" . $id . "'", FALSE);				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('successupdate'));
			} else {
				$posted_data = array(
            					'country_name' => $this->input->post('country_name'),
            					'country_temp_name' => url_title($this->input->post('country_name'),'dash',true),
            					'cont_currency' => $this->input->post('cont_currency'),
								'created_by'		=> $this->admin_id
				);
				$this->location_model->safe_insert('wl_countries', $posted_data, FALSE);
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('success'));
			}
			redirect('sitepanel/location', '');
		}
		$data['includes'] = $this->view_path . 'addedit_view';
		$this->load->view('includes/sitepanel_container', $data);
	}
	public function state() {
		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter' => 'result'), array('per_page'));
		$keyword = $this->db->escape_str(trim($this->input->get_post('keyword', TRUE)));
		$country_id = $this->db->escape_str(trim($this->input->get_post('country_id', TRUE)));
		$condtion = " ";
		if ($keyword != '') {
			$condtion .= "AND title like '%" . $keyword . "%'";
		}
		if ($country_id > 0) {
			$condtion .= "AND country_id ='" . $country_id . "'";
		}
		$condtion_array = array(					        'condition' => $condtion,					
					        'limit' => $config['limit'],					
					        'offset' => $offset,					
					        'debug' => FALSE);
		$res_array = $this->location_model->get_states($condtion_array);
		$config['total_rows'] = $this->location_model->total_rec_found;
		$data['page_links'] = admin_pagination($base_url, $config['total_rows'], $config['limit'], $offset);
		$data['heading_title'] = "Manage States";		$data['res'] = $res_array;
		if ($this->input->post('status_action') != '') {
			if ($this->input->post('status_action') == "Set Popular") {
				$arr_ids = $this->input->post('arr_ids');

				if (is_array($arr_ids)) {
					$id_str = implode(',', $arr_ids);
					$data = array("is_state_popular" => "1");
					$where = "id IN($id_str)";
					$this->location_model->safe_update("wl_states", $data, $where, TRUE);
				}
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('successupdate'));
				redirect($this->input->server('HTTP_REFERER'));
			} elseif ($this->input->post('status_action') == "Unset Popular") {
				$arr_ids = $this->input->post('arr_ids');

				if (is_array($arr_ids)) {
					$id_str = implode(',', $arr_ids);
					$data = array("is_state_popular" => "0");
					$where = "id IN($id_str)";
					$this->location_model->safe_update("wl_states", $data, $where, TRUE);
				}
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('successupdate'));
				redirect($this->input->server('HTTP_REFERER'));
			} else {
				$this->update_status('wl_states', 'id');
			}
		}
		$data['includes'] = $this->view_path . 'state_list_vew';
		$this->load->view('includes/sitepanel_container', $data);
	}
	public function state_add_edit() {		$id = (int) $this->uri->segment(4, 0);
		$row_data = '';
		if ($id > 0) {
			$row_data = $this->location_model->get_single_row("wl_states", $id);
			$operation_allowed=operation_allowed("wl_states",$id);

			if(!$operation_allowed){
				$this->session->set_userdata(array('msg_type'=>'error'));
				$this->session->set_flashdata('error',lang('operation_not_allowed'));
				redirect('sitepanel/location/state');
			}
		}
		$data['row'] = $row_data;
		$data['parentData'] = '';
		$data['heading_title'] = ($id > 0) ? 'Edit State' : 'Add State';
		$data['cancel_url'] 	 = "sitepanel/".$this->current_controller.'/state';
		if (is_object($row_data)) {
			$this->form_validation->set_rules('country_id', 'Country', "trim|required|max_length[11]");			$this->form_validation->set_rules('title', 'State Name', "trim|required|max_length[10000]|unique[wl_states.title='" . $this->db->escape_str($this->input->post('title')) . "' AND wl_states.status!='2' AND wl_states.country_id='" . $this->db->escape_str($this->input->post('country_id')) . "' AND wl_states.id !=$id]");		} else {
			$this->form_validation->set_rules('country_id', 'Country', "trim|required|max_length[11]");			$this->form_validation->set_rules('title', 'State Name', "trim|required|max_length[100]|unique[wl_states.title='" . $this->db->escape_str($this->input->post('title')) . "' AND wl_states.status!='2' AND wl_states.country_id='" . $this->db->escape_str($this->input->post('country_id')) . "']");		}
		if ($this->form_validation->run() === TRUE) {

			$url_title=url_title($this->input->post('title'),'dash',true);

			$posted_data = array(
            					'title' => $this->input->post('title'),
								'temp_title' => $url_title,
            					'country_id' => url_title($this->input->post('country_id')));

			$posted_data = $this->security->xss_clean($posted_data);

			if ($id > 0) {

				$this->location_model->safe_update('wl_states', $posted_data, "id ='" . $id . "'", FALSE);
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('successupdate'));
			} else {

				$posted_data['created_by']		= $this->admin_id;

				$this->location_model->safe_insert('wl_states', $posted_data, FALSE);				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('success'));
			}
			$redirect_path = 'location/state_add_edit';

			if ($id > 0) {
				$redirect_path = 'location/state';
			}
			redirect('sitepanel/' . $redirect_path, '');
		}
		$data['includes'] = $this->view_path . 'state_addedit_view';
		$this->load->view('includes/sitepanel_container', $data);
	}
	//City
	public function city() {

		if ($this->input->post('status_action') != '') {$this->update_status('wl_cities', 'id');}

		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter' => 'result'), array('per_page'));
		$keyword = $this->db->escape_str(trim($this->input->get_post('keyword', TRUE)));
		$country_id = $this->db->escape_str(trim($this->input->get_post('country_id', TRUE)));
		$state_id = $this->db->escape_str(trim($this->input->get_post('state_id', TRUE)));
		$is_city_popular = $this->db->escape_str(trim($this->input->get_post('is_city_popular', TRUE)));
		$is_othercity_popular = $this->db->escape_str(trim($this->input->get_post('is_othercity_popular', TRUE)));
		$condtion = " ";
		if ($keyword != '') {
			$condtion .= "AND title like '%" . $keyword . "%'";
		}
		if ($state_id > 0) {
			$condtion .= "AND state_id ='" . $state_id . "'";
		}
		if ($country_id > 0) {
			$condtion .= "AND country_id ='" . $country_id . "'";
		}
		if ($is_city_popular > 0) {
			$condtion .= "AND is_city_popular ='" . $is_city_popular . "'";
		}
		if ($is_othercity_popular > 0) {
			$condtion .= "AND is_othercity_popular ='" . $is_othercity_popular . "'";
		}
			
		$condtion_array = array(
	        				'condition' => $condtion,	
	        				'limit' => $config['limit'],	
					        'offset' => $offset,	
					        'debug' => FALSE	
		);
		$res_array = $this->location_model->get_city($condtion_array);
		$config['total_rows'] = $this->location_model->total_rec_found;		$data['page_links'] = admin_pagination($base_url, $config['total_rows'], $config['limit'], $offset);		$data['heading_title'] = "Manage City";
		$data['res'] = $res_array;
		$data['includes'] = $this->view_path . 'city_list_vew';
		$this->load->view('includes/sitepanel_container', $data);
	}
	public function city_add_edit() {
		$id = (int) $this->uri->segment(4, 0);
		$row_data = '';
		if ($id > 0) {
			$row_data = $this->location_model->get_single_row("wl_cities", $id);
			$operation_allowed=operation_allowed("wl_cities",$id);

			if(!$operation_allowed){
				$this->session->set_userdata(array('msg_type'=>'error'));
				$this->session->set_flashdata('error',lang('operation_not_allowed'));
				redirect('sitepanel/location/city');
			}
		}
		$data['row'] = $row_data;
		$data['parentData'] = '';
		$data['heading_title'] = ($id > 0) ? 'Edit City' : 'Add City';
		$data['cancel_url'] 	 = "sitepanel/".$this->current_controller.'/city';
		if (is_object($row_data)) {
			$this->form_validation->set_rules('country_id', 'Country', "trim|required|max_length[11]");
			$this->form_validation->set_rules('state_id', 'State', "trim|required|max_length[11]");
			$this->form_validation->set_rules('title', 'City Name', "trim|required|max_length[100]|unique[wl_cities.title='" . $this->db->escape_str($this->input->post('title')) . "' AND wl_cities.status!='2' and wl_cities.state_id='".$this->db->escape_str($this->input->post('state_id'))."' and wl_cities.country_id='".$this->db->escape_str($this->input->post('country_id'))."' AND wl_cities.id !=$id]");		} else {
			$this->form_validation->set_rules('country_id', 'Country', "trim|required|max_length[11]");
			$this->form_validation->set_rules('state_id', 'State', "trim|required|max_length[11]");			$this->form_validation->set_rules('title', 'City Name', "trim|required|max_length[100]|unique[wl_cities.title='" . $this->db->escape_str($this->input->post('title')) . "' AND wl_cities.status!='2' and wl_cities.state_id='".$this->db->escape_str($this->input->post('state_id'))."' and wl_cities.country_id='".$this->db->escape_str($this->input->post('country_id'))."' ]");		}
		if ($this->form_validation->run() === TRUE) {
			if ($id > 0) {
				$url_title=str_replace("-","",url_title($this->input->post('title')));
				$posted_data = array(
								'title' => $this->input->post('title'),
								'temp_title' => $url_title,
            					'country_id' => $this->input->post('country_id'),
            					'state_id' => $this->input->post('state_id')
				);
				$posted_data = $this->security->xss_clean($posted_data);
				$this->location_model->safe_update('wl_cities', $posted_data, "id ='" . $id . "'", FALSE);
				
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('successupdate'));
			} else {
				$url_title=str_replace("-","",url_title($this->input->post('title')));
				$posted_data = array(
            						'title' => $this->input->post('title'),
									'temp_title' => $url_title,
						            'country_id' => $this->input->post('country_id'),            
						            'state_id' => $this->input->post('state_id'),
									'created_by'		=> $this->admin_id
				);
				$posted_data = $this->security->xss_clean($posted_data);
				
				$city_id = $this->location_model->safe_insert('wl_cities', $posted_data, FALSE);
				$data = array("city_group_id" => $city_id);
				
				$where = "id ='" . $city_id . "'";
				$this->location_model->safe_update('wl_cities', $data, $where, FALSE);
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('success'));
			}
			$redirect_path = 'location/city_add_edit';
			if ($id > 0) {
				$redirect_path = 'location/city';
			}
			redirect('sitepanel/' . $redirect_path, '');
		}
		$data['includes'] = $this->view_path . 'city_addedit_view';
		$this->load->view('includes/sitepanel_container', $data);
	}

	public function neighborhood() {
		
		if ($this->input->post('status_action') != '') {
			$this->update_status('tbl_localities', 'id');
		}
		
		
		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter' => 'result'), array('per_page'));

		$keyword = $this->db->escape_str(trim($this->input->get_post('keyword', TRUE)));
		$country_id = $this->db->escape_str(trim($this->input->get_post('country_id', TRUE)));
		$state_id = $this->db->escape_str(trim($this->input->get_post('state_id', TRUE)));
		$city_id = $this->db->escape_str(trim($this->input->get_post('city_id', TRUE)));
		$condtion = " ";
		if ($keyword != '') {
			$condtion .= "AND title like '%" . $keyword . "%'";
		}
		if ($city_id > 0) {
			$condtion .= "AND city_id ='" . $city_id . "'";
		}
		if ($state_id > 0) {
			$condtion .= "AND state_id ='" . $state_id . "'";
		}
				if ($country_id > 0) {
			$condtion .= "AND country_id ='" . $country_id . "'";
		}
		$condtion_array = array(
        					'condition' => $condtion,
       						'limit' => $config['limit'],					        'offset' => $offset,
					        'debug' => FALSE					
							);

		$res_array = $this->location_model->get_neighborhood($condtion_array);
		$config['total_rows'] = $this->location_model->total_rec_found;
		$data['page_links'] = admin_pagination($base_url, $config['total_rows'], $config['limit'], $offset);
		$data['heading_title'] = "Manage Locations";
		$data['res'] = $res_array;
		$data['includes'] = $this->view_path . 'neighborhood_list_vew';
		$this->load->view('includes/sitepanel_container', $data);	}

	public function neighborhood_add_edit() {		$id = (int) $this->uri->segment(4, 0);
		
		$row_data = '';
		if ($id > 0) {
			$row_data = $this->location_model->get_single_row("tbl_localities", $id);
			$operation_allowed=operation_allowed("tbl_localities",$id);
			if(!$operation_allowed){
				$this->session->set_userdata(array('msg_type'=>'error'));
				$this->session->set_flashdata('error',lang('operation_not_allowed'));
				redirect('sitepanel/location/neighborhood');
			}
		}
		$data['row'] = $row_data;
		$data['parentData'] = '';
		$data['heading_title'] = ($id > 0) ? 'Edit Location' : 'Add Location';
		$data['cancel_url'] 	 = "sitepanel/".$this->current_controller.'/neighborhood';
		
		$this->form_validation->set_rules('country_id', 'Country', "trim|required|max_length[11]");
		$this->form_validation->set_rules('state_id', 'State', "trim|required|max_length[11]");
		$this->form_validation->set_rules('city_id', 'City', "trim|required|max_length[11]");
			
		if (is_object($row_data)) {
			
			$this->form_validation->set_rules('title', 'Location', "trim|required|max_length[100]|unique[tbl_localities.title='" . $this->db->escape_str($this->input->post('title')) . "' AND tbl_localities.status!='2' AND tbl_localities.country_id='" . $this->db->escape_str($this->input->post('country_id')) . "' AND tbl_localities.state_id='" . $this->db->escape_str($this->input->post('state_id')) . "' AND tbl_localities.city_id='" . $this->db->escape_str($this->input->post('city_id')) . "' AND tbl_localities.id !=$id]");		} else {
			$this->form_validation->set_rules('title', 'Location', "trim|required|max_length[100]|unique[tbl_localities.title='" . $this->db->escape_str($this->input->post('title')) . "' AND tbl_localities.status!='2' AND tbl_localities.country_id='" . $this->db->escape_str($this->input->post('country_id')) . "' AND tbl_localities.state_id='" . $this->db->escape_str($this->input->post('state_id')) . "' AND tbl_localities.city_id='" . $this->db->escape_str($this->input->post('city_id')) . "']");		}
		if ($this->form_validation->run() === TRUE) {
			
			$posted_data = array(
            					'title' => $this->input->post('title'),
					           	'country_id' => $this->input->post('country_id'),
            					'state_id' => $this->input->post('state_id'),
            					'city_id' => $this->input->post('city_id'));
			
			$posted_data = $this->security->xss_clean($posted_data);
			
			if ($id > 0) {
								$this->location_model->safe_update('tbl_localities', $posted_data, "id ='" . $id . "'", FALSE);				$this->session->set_userdata(array('msg_type' => 'success'));				$this->session->set_flashdata('success', lang('successupdate'));
							} else {			
				$this->location_model->safe_insert('tbl_localities', $posted_data, FALSE);
				$this->session->set_userdata(array('msg_type' => 'success'));
				$this->session->set_flashdata('success', lang('success'));
			}
			$redirect_path = 'location/neighborhood';
			redirect('sitepanel/' . $redirect_path, '');
		}
		
		$data['includes'] = $this->view_path . 'neighborhood_addedit_view';
		$this->load->view('includes/sitepanel_container', $data);
	}
	//Online List Ajax Methods
	public function bind_state() {
		$parent_id = $this->input->post('parent_id');
		$from_section = $this->input->post('from_section');		$arr = array("tbl_name" => "wl_states", "select_fld" => "id,title", "where" => "and status='1' and country_id ='" . $parent_id . "'");
		if ($from_section == "neighborhood_country") {			echo common_dropdown('state_id', '', $arr, 'style="width:235px;" onchange="bind_data(this.value,\'sitepanel/location/bind_city\',\'city_list\',\'loader_city\',\'city_section\');"');		} else {			echo common_dropdown('state_id', '', $arr, 'style="width:235px;"');		}	}	public function bind_city() {		$parent_id = $this->input->post('parent_id');		$arr = array("tbl_name" => "wl_cities", "select_fld" => "id,title", "where" => "and status='1' and state_id ='" . $parent_id . "'");		echo common_dropdown('city_id', '', $arr, 'style="width:235px;"');
	}
}
// End of controller