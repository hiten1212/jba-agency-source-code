<?php



class Staticpages extends Admin_Controller {







	public function __construct()



	{		



		parent::__construct(); 			  



		$this->load->model(array('pages/pages_model'));  		

                $this->load->model(array('sitepanel/setting_model'));


		$this->config->set_item('menu_highlight','other management');		



	}



	



	



	public  function index(){



		



		$pagesize               =  (int) $this->input->get_post('pagesize');		



	    $config['limit']		=  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');			



		$offset                 = ($this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;		



		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));					



		$res_array              =  $this->pages_model->get_all_cms_page($offset,$config['limit']);		



		$total_record        	=  $this->pages_model->total_rec_found;



		



		$data['result_found'] = $total_record;	



		



		$data['page_links']     =  admin_pagination("$base_url",$total_record,$config['limit'],$offset);		



		$data['heading_title']  = 'Manage Home Content';



		$data['pagelist']       = $res_array; 			



		$data['offset'] = $offset;		

                $data['admin_info'] = $this->setting_model->get_admin_info($this->session->userdata('admin_id'));

		$this->load->view('staticpage/staticpage_list_index_view',$data); 	



		



		



	}







	public function pagedatadisplay()



	{



		$id = (int) $this->uri->segment(4);



		$res = $this->pages_model->getStaticpage_by_id($id); 



		



		$data['heading_title'] = 'Manage Home Content';



		$data['page_title']    = 'View Page Information';



		$data['pageresult']    =$res;		



		$this->load->view('staticpage/statispage_detail_view',$data);



	}







	public function edit(){		



		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'page_desc'));	

		$data['ckeditor2']  =  set_ck_config(array('textarea_id'=>'short_desc'));

		$page_id = (int) $this->uri->segment(4);

		$res = $this->pages_model->get_cms_page(array('page_id'=>$page_id));				



		if( is_array( $res ) ){ 				



				$this->form_validation->set_rules('page_short_description', 'Heading','trim');
				$this->form_validation->set_rules('page_name', 'page name','required');
				$this->form_validation->set_rules('page_description', 'Description','trim');



				if ($this->form_validation->run() == TRUE){	


					$uploaded_file = $res['image'];				 
				$unlink_image = array('source_dir'=>"staticimage",'source_file'=>$res['image']);
			 	if($this->input->post('static_img_delete')==='Y')
				 {					
					removeImage($unlink_image);						
					$uploaded_file = NULL;	
								
				 }				
				 if( !empty($_FILES) && $_FILES['image']['name']!='' )
				 {			  
						$this->load->library('upload');	
							
						$uploaded_data =  $this->upload->my_upload('image','staticimage');
					
						if( is_array($uploaded_data)  && !empty($uploaded_data) )
						{ 								
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
						    removeImage($unlink_image);	
						}
						
				}	



						$posted_data = array(
                                                'page_name'=>$this->input->post('page_name'),
                                                    
						'page_description'=>$this->input->post('page_description'),
						'image'=>$uploaded_file,

						'page_short_description'=>$this->input->post('page_short_description'),

						'page_updated_date'=>$this->config->item('config.date.time'),
                                                    
                                                'title_background'=>$this->input->post('title_background'),
						);						



						$where = "page_id = '".$res['page_id']."'"; 

						$this->pages_model->safe_update('wl_cms_pages',$posted_data,$where,FALSE);

						$this->session->set_userdata(array('msg_type'=>'success'));

						$this->session->set_flashdata('success',lang('successupdate'));	

						redirect('sitepanel/staticpages/'.query_string(), ''); 					



				}

			



		$data['heading_title']  = 'Edit Static Pages';



		$data['page_title']     = 'Edit Information';



		$data['pageresult']     = $res;		



		$this->load->view('staticpage/statispage_edit_view',$data);



			



		}else



		{



			redirect('sitepanel/staticpages','');



		}



	}



}



// End of controller