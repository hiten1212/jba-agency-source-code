<?php
class Managephoto extends Admin_Controller
{
	public function __construct()
	{		
		parent::__construct(); 				
		$this->load->model(array('photo_model'));
		$this->config->set_item('menu_highlight','photo');				
	}
	 
	public  function index()
	{
		  $data['rwcat']=get_db_multiple_row("tbl_photo_category","id,category_name","status ='1' ORDER BY category_name ASC");
		 $pagesize               =  (int) $this->input->get_post('pagesize');
	     $config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');		 		 				
		 $offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;		
		 $base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));				 
		 $parent_id              =   (int) $this->uri->segment(4,0);	
		 
		 $keyword = trim($this->input->get_post('keyword',TRUE));		
		 $keyword = $this->db->escape_str($keyword);
	     $condtion = " ";
		 
		$condtion_array = array(
		                'field' =>"*",
						 'condition'=>$condtion,
						 'order'=>"id DESC",
						 'limit'=>$config['limit'],
						  'offset'=>$offset	,
						  'debug'=>FALSE
						 );							 						 	
		$res_array              =  $this->photo_model->getAlbumphoto($condtion_array);
		$config['total_rows']	=  $this->photo_model->total_rec_found;	
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		$data['heading_title']  =  'Manage Album Photo';
		$data['res']            =  $res_array; 	
		$data['parent_id']      =  $parent_id; 	
		
		if( $this->input->post('status_action')!='')
		{	
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
					$rw=get_db_single_row("tbl_photo","photo,id"," AND id ='$v'");
					if(is_array($rw) && count($rw) > 0 ){
					  
						  $unlink_image = array('source_dir'=>"photo_img",'source_file'=>$rw['photo']);
						  @unlink($unlink_image); 
						  $this->db->query("DELETE FROM tbl_photo WHERE id ='".$v."'");
					}
                  						
				}
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been deleted successfully.");
				redirect("sitepanel/managephoto");
			}			
			if( $this->input->post('status_action')=='Activate')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
					
                  $this->db->query("UPDATE tbl_photo SET status='1' WHERE id ='$v' AND status !='2' "); 		
				}
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been activated successfully.");
				redirect("sitepanel/managephoto");
			}	
			if( $this->input->post('status_action')=='Deactivate')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
                  $this->db->query("UPDATE tbl_photo SET status='0' WHERE id ='$v' AND status !='2' "); 		
				}
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been de-activated successfully.");
				redirect("sitepanel/managephoto");
			}	
		}
		$this->load->view('album/view_albumphoto_list',$data);		
		
		
	}	
	
	public function add()
	{
		 $data['heading_title'] = 'Add Album Photo';
		 $img_allow_size =  $this->config->item('allow.file.size');
		 $img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		 $data['rwcat']=get_db_multiple_row("tbl_photo_category","id,category_name","status ='1' ORDER BY category_name ASC");
		 
		  $this->form_validation->set_rules('cat_id','Album',"trim|required|max_length[80]");
		 $this->form_validation->set_rules('title','Title',"trim|required|max_length[80]");
		 // $this->form_validation->set_rules('description','Description',"trim|required|max_length[8888850]|xss_clean");
		 $this->form_validation->set_rules('image1','Image',"file_required|file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		 
		if($this->form_validation->run()===TRUE)
		{
			  	
				$uploaded_file = "";
				if( !empty($_FILES) && $_FILES['image1']['name']!='' ){
					$this->load->library('upload');
					$uploaded_data =  $this->upload->my_upload('image1','photo_img');
					if( is_array($uploaded_data)  && !empty($uploaded_data) ){
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
					}
				}
				$posted_data = array(
				    'cat_id'=>$this->input->post('cat_id'),
					'title'=>$this->input->post('title'),
					'description'=>$this->input->post('description'),
					'photo'=>$uploaded_file,
					'status'=>1
				 );
		    $this->photo_model->safe_insert('tbl_photo',$posted_data,FALSE);	
								
			$this->session->set_userdata(array('msg_type'=>'success'));			
			$this->session->set_flashdata('success',lang('success'));				
			redirect('sitepanel/managephoto', '');		
		}	
		$this->load->view('album/view_albumphoto_add',$data);		  
	}
	public function edit()
	{
		$brandId = (int) $this->uri->segment(4);
		$rowdata=$this->photo_model->get_photo_by_id($brandId);
		$brandId = $rowdata->id;		
		$data['heading_title'] = 'Edit Album Photo';
		$data['rwcat']=get_db_multiple_row("tbl_photo_category","id,category_name","status ='1' ORDER BY category_name ASC");
		
		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		
		if( !is_object($rowdata) )
		{
			$this->session->set_flashdata('message', lang('idmissing'));	
			redirect('sitepanel/managephoto', ''); 	
			
		}
		 $this->form_validation->set_rules('cat_id','Album',"trim|required|max_length[80]");
		 $this->form_validation->set_rules('title','Title',"trim|required|max_length[80]");
		 //$this->form_validation->set_rules('description','Description',"trim|required|max_length[8888850]|xss_clean");
		$this->form_validation->set_rules('image1','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");		
			if($this->form_validation->run()==TRUE)
			{	
				$uploaded_file = $rowdata->photo;
				$unlink_image = array('source_dir'=>"photo_img",'source_file'=>$rowdata->photo);
				 if( !empty($_FILES) && $_FILES['image1']['name']!='' )
				 {
						$this->load->library('upload');
						$uploaded_data =  $this->upload->my_upload('image1','photo_img');
						if( is_array($uploaded_data)  && !empty($uploaded_data) )
						{
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
						    removeImage($unlink_image);
						}
				}
				$posted_data = array(
					'photo'=>$uploaded_file,
					'cat_id'=>$this->input->post('cat_id'),
					'title'=>$this->input->post('title'),
					'description'=>$this->input->post('description')
				 );
			 	$where = "id = '".$brandId."'"; 				
				$this->photo_model->safe_update('tbl_photo',$posted_data,$where,FALSE);	
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',lang('successupdate'));								
				redirect('sitepanel/managephoto'.'/'.query_string(), ''); 	
			}						
		$data['res']=$rowdata;		
		$this->load->view('album/view_albumphoto_edit',$data);				
		
	}
	
	
}
// End of controller