<?php
class Services extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('city/city_model','services/services_model'));
		$this->load->helper('service_category/service_category');
		$this->config->set_item('menu_highlight','product management');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}

	public  function index($page = NULL)
	{
		$condtion = array();

		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter'=>'result'),array('per_page'));

		$category_id = (int) $this->input->get_post('category_id');
		$brand_id = (int) $this->input->get_post('brand_id');
		$status = $this->input->get_post('status',TRUE);

		$cat_name = '';

		if($category_id > 0 )
		{
			$condtion['category_id'] = $category_id;
			$cat_name       = 'in ';
			$cat_name .= get_db_field_value('wl_categories','category_name'," AND category_id='$category_id'");
		}
		
		if($brand_id > 0 )
		{
			$condtion['brand_id'] = $brand_id;
			$cat_name       = 'in ';
			$cat_name .= get_db_field_value('wl_brands','brand_name'," AND brand_id='$brand_id'");
		}

		if($status!='')
		{
			$condtion['status'] = $status;
		}

		$res_array               =  $this->services_model->get_services($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Services';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');

				foreach($prod_id as $v)
				{
					$where = array('entity_type'=>'services/detail','entity_id'=>$v);
					$this->services_model->safe_delete('wl_meta_tags',$where,TRUE);
				}
			}

			$this->update_status('wl_services','service_id');
		}

if( $this->input->post('update_order',TRUE)!='')
		{
			$this->update_displayOrder('wl_services','sort_order','service_id');
		}
		/* service set as a */
		if( $this->input->post('set_as')!='' )
		{
			$set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_services','service_id',array($set_as=>'1'));
		}

		if( $this->input->post('unset_as')!='' )
		{
			$unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_services','service_id',array($unset_as=>'0'));
		}
		/* End service set as a */
		
		$categories_sql="SELECT wc.category_id, wc.category_name FROM wl_service_categories AS wc, wl_services AS wp WHERE wc.category_id =wp.category_id group by wc.category_id ";
		$data['categories']  = custom_result_set($categories_sql);

		$data['category_result_found'] = "Total ".$config['total_rows']." result(s) found ".strtolower($cat_name)." ";
		
		$this->load->view('services/view_services_list',$data);

	}

	public function add()
	{
		$data['heading_title'] = 'Add Service';
		$categoryposted=$this->input->post('catid');
		$data['categoryposted']=$categoryposted;
		$categoryposted=$this->input->post('catid');
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'description'));
		$data['hckeditor']  =  set_ck_config(array('textarea_id'=>'highlights'));
		$data['imckeditor']  =  set_ck_config(array('textarea_id'=>'important_details'));
		$data['ckeditor_offered']  =  set_ck_config(array('textarea_id'=>'offered'));

		//$posted_friendly_url = $this->input->post('friendly_url');
		$posted_friendly_url = $this->input->post('service_name');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$this->form_validation->set_rules('category_id','Project status type',"trim|required");
		//$this->form_validation->set_rules('service_name','Project Name',"trim|required|max_length[255]");
		
		//$this->form_validation->set_rules('service_name','Project  Name',"trim|required|max_length[255]|unique[wl_services.service_name='".$this->db->escape_str($this->input->post('service_name'))."' AND status!='3']");
		
		$this->form_validation->set_rules('service_code','Project Code',"trim|required|max_length[65]|unique[wl_services.service_code='".$this->db->escape_str($this->input->post('service_code'))."' AND status!='3']");
		
		//$this->form_validation->set_rules('video_link','Video Link',"trim|required|max_length[255]");
		//$this->form_validation->set_rules('service_description','Project Description',"required|max_length[85000]");
		$this->form_validation->set_rules('highlights','Project Description',"max_length[85000]");
		$this->form_validation->set_rules('important_details','Important Description',"max_length[85000]");
		$this->form_validation->set_rules('broucher_doc','Upload PDF',"file_allowed_type[document]");
		$this->form_validation->set_rules('visit_link','Visit Link',"prep_url|max_length[150]");
		//$this->form_validation->set_rules('service_price','Price',"trim|required|is_valid_amount|callback_check_price_zero");
		//$this->form_validation->set_rules('service_discounted_price','Discount Price',"trim|is_valid_amount|callback_check_price");
		//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		$this->form_validation->set_rules('service_alt','Alt',"trim|max_length[100]");
		$this->form_validation->set_rules('text_colour','text colour',"trim|max_length[100]");
		//$this->form_validation->set_rules('description_text_colour','description text colour',"trim|max_length[100]");
		$this->form_validation->set_rules('city_id[]','Project City',"trim");
		$this->form_validation->set_rules('service_images1','Image1',"required|file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

		if($this->form_validation->run()===TRUE)
		{
			$redirect_url = "services/detail";
			
			$service_alt = $this->input->post('service_alt');

			if($service_alt =='')
			{
				$service_alt = $this->input->post('service_name');
			}

		  $category_links = get_parent_categories($this->input->post('category_id'),"AND status!='2'","category_id,parent_id");
			$category_links = array_keys($category_links);
			$category_links = implode(",",$category_links);

			//$discounted_price = $this->input->post('service_discounted_price',TRUE);
			//$discounted_price = ($discounted_price=='') ? "0.0000" : $discounted_price;
			
			$posted_data = array(
			'category_id'=>$this->input->post('category_id'),
			'category_links'=>$category_links,
			'service_name'=>$this->input->post('service_name',TRUE),
			'service_code'=>$this->input->post('service_code',TRUE),
			'friendly_url'=>$this->cbk_friendly_url,
			'service_alt'=>$service_alt,
			'text_colour'=>$this->input->post('text_colour',TRUE),
			//'description_text_colour'=>$this->input->post('description_text_colour',TRUE),
			//'video_link'=>$this->input->post('video_link',TRUE),
			//'service_price'=>$this->input->post('service_price',TRUE),
			//'service_discounted_price'=>$discounted_price,
			'service_description'=>$this->input->post('service_description'),
			//'highlights'=>$this->input->post('highlights'),
			//'important_details'=>$this->input->post('important_details'),
			'service_added_date'=>$this->config->item('config.date.time')
			);
			
			if($_FILES["broucher_doc"]["name"]!="")
			{
				$this->load->library('upload');
				$uploaded_file="";
				$uploaded_data =  $this->upload->my_upload('broucher_doc','services');
				if( is_array($uploaded_data)  && !empty($uploaded_data))
				{
					$uploaded_file = $uploaded_data['upload_data']['file_name'];
					$posted_data["broucher_doc"]=$uploaded_file;
					@unlink(UPLOAD_DIR."/services/".$res["broucher_doc"]);
				}
			}
				
			//$posted_data = $this->security->xss_clean($posted_data);
			$serviceId = $this->services_model->safe_insert('wl_services',$posted_data,FALSE);
			
			#--------------Services City-------------------------------------------#
			$city_ids=$this->input->post('city_id',TRUE);

				if(is_array($city_ids)){

					for($i=0; $i<count($city_ids); $i++){

						$size_data_array = array(
						'city_id'=>$city_ids[$i],
						'services_id'=>$serviceId
						);

						$this->city_model->safe_insert('wl_services_city',$size_data_array,FALSE);
						//echo $this->db->last_query();exit;

					}
				}
			#------------------------Services City end --------------------#

			$this->add_service_media($serviceId);

			if( $serviceId > 0 )
			{
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$serviceId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('service_name'),80),
							  //'visit_link'=>$this->input->post('visit_link',TRUE),
							  'meta_description'=>get_text($this->input->post('service_description')),
							  'meta_keyword'=>get_keywords($this->input->post('service_description'))
							  );

			  create_meta($meta_array);
			}
			
			
			
			
			
			
			
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/services?category_id='.$this->input->post('category_id'), '');

		}
		//available city
			$size_cond_config = array(
			'condition' => " AND status='1' ",
			'order'=>'city_name '
			);
			$city = $this->city_model->get_citys($size_cond_config);
			$data['city'] = $city;

		$this->load->view('services/view_service_add',$data);

	}

	public function edit($serviceId)
	{
		$data['heading_title'] = 'Edit Service';
		$serviceId = (int) $this->uri->segment(4);
		$option = array('serviceid'=>$serviceId);
		$res =  $this->services_model->get_services(1,0, $option);
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'description'));
		$data['hckeditor']  =  set_ck_config(array('textarea_id'=>'highlights'));
		$data['imckeditor']  =  set_ck_config(array('textarea_id'=>'important_details'));
		$data['ckeditor_offered']  =  set_ck_config(array('textarea_id'=>'offered'));

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$this->form_validation->set_rules('category_id','Project  status type',"trim|required");
		//$this->form_validation->set_rules('service_name','Service Name',"trim|required|max_length[255]");
		
		//$this->form_validation->set_rules('service_name','Project Name',"trim|required|max_length[255]|unique[wl_services.service_name='".$this->db->escape_str($this->input->post('service_name'))."' AND status!='3' AND service_id != '".$serviceId."']");	
		//$this->form_validation->set_rules('service_code','Project Code',"trim|required|max_length[65]|unique[wl_services.service_code='".$this->db->escape_str($this->input->post('service_code'))."' AND status!='3' AND service_id != '".$serviceId."']");
		
		//$this->form_validation->set_rules('video_link','Video Link',"trim|required|max_length[255]");
		$this->form_validation->set_rules('city_id[]','Services City',"trim");
		//$this->form_validation->set_rules('service_description','Project Description',"required|max_length[85000]");
		$this->form_validation->set_rules('highlights','highlights Description',"max_length[85000]");
		$this->form_validation->set_rules('important_details','Important Description',"max_length[85000]");
		$this->form_validation->set_rules('visit_link','Visit Link',"prep_url|max_length[150]");
		//$this->form_validation->set_rules('service_price','Price',"trim|required|is_valid_amount|callback_check_price_zero");
		//$this->form_validation->set_rules('service_discounted_price','Discount Price',"trim|is_valid_amount|callback_check_price");
		$this->form_validation->set_rules('text_colour','text colour',"trim|max_length[100]");
		//$this->form_validation->set_rules('description_text_colour','description text colour',"trim|max_length[100]");
		//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		$this->form_validation->set_rules('service_alt','Alt',"trim|max_length[100]");
		$this->form_validation->set_rules('service_images1','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		
		$this->form_validation->set_rules('broucher_doc','Brochure',"file_allowed_type[document]");

		if( is_array( $res ) && !empty( $res ))
		{
			if($this->form_validation->run()==TRUE)
			{
				$service_alt = $this->input->post('service_alt');

				if($service_alt =='')
				{
					$service_alt = $this->input->post('service_name');
				}
			
				$category_links = get_parent_categories($this->input->post('category_id'),"AND status!='2'","category_id,parent_id");
				$category_links = array_keys($category_links);
				$category_links = implode(",",$category_links);

				//$discounted_price = $this->input->post('service_discounted_price',TRUE);
				//$discounted_price = ($discounted_price=='') ? "0.0000" : $discounted_price;
				
				$posted_data = array(
				'category_id'=>$this->input->post('category_id'),
				'category_links'=>$category_links,
				'service_name'=>$this->input->post('service_name',TRUE),
				'text_colour'=>$this->input->post('text_colour',TRUE),
				//'description_text_colour'=>$this->input->post('description_text_colour',TRUE),
				//'service_code'=>$this->input->post('service_code',TRUE),
				'visit_link'=>$this->input->post('visit_link',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,
				'service_alt'=>$service_alt,
				//'video_link'=>$this->input->post('video_link',TRUE),
				//'service_price'=>$this->input->post('service_price',TRUE),
				//'service_discounted_price'=>$discounted_price,
				'service_description'=>$this->input->post('service_description'),
				//'highlights'=>$this->input->post('highlights'),
				//'important_details'=>$this->input->post('important_details'),
				'service_added_date'=>$this->config->item('config.date.time')
				);
				
				if($_FILES["broucher_doc"]["name"]!="")
				{
					$this->load->library('upload');
					$uploaded_file="";
					$uploaded_data =  $this->upload->my_upload('broucher_doc','services');
					if( is_array($uploaded_data)  && !empty($uploaded_data))
					{
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
						$posted_data["broucher_doc"]=$uploaded_file;
						@unlink(UPLOAD_DIR."/services/".$res["broucher_doc"]);
					}
				}
				
				//$posted_data = $this->security->xss_clean($posted_data);
				$where = "service_id = '".$res['service_id']."'";
				$this->services_model->safe_update('wl_services',$posted_data,$where,FALSE);
				
				#--------------------Product city----------------------------------------#
				$this->db->where('services_id', $res['service_id']);
				$this->db->delete('wl_services_city ');

				$city_ids=$this->input->post('city_id',TRUE);

				if(is_array($city_ids)){

					for($i=0; $i<count($city_ids); $i++){

						$size_data_array = array(
						'city_id'=>$city_ids[$i],
						'services_id'=>$res['service_id']
						);

						$this->city_model->safe_insert('wl_services_city',$size_data_array,FALSE);

					}
				}
				#----------------------------product size end----------------------#
				
				
				
				$this->edit_service_media($res['service_id']);
				update_meta_page_url('services/detail',$res['service_id'],$this->cbk_friendly_url);
				
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/services/'. query_string(), '');
			}
			
			
			// Available Size Records
			$this->db->select('city_id', FALSE);
			$this->db->where('services_id',$res['service_id']);

			$query_city = $this->db->get('wl_services_city')->result_array();
			
			$data['city_array']	=$query_city;
			
			//available city
			$size_cond_config = array(
			'condition' => " AND status='1' ",
			'order'=>'city_name '
			);
			$city = $this->city_model->get_citys($size_cond_config);
			$data['city'] = $city;

			$data['res']=$res;
			$media_option = array('serviceid'=>$res['service_id']);
			$res_photo_media = $this->services_model->get_service_media(5,0, $media_option);
			$data['res_photo_media']=$res_photo_media;
			
			$this->load->view('services/view_service_edit',$data);
		}
		else
		{
			redirect('sitepanel/services', '');
		}
	}

	public function add_service_media($serviceId)
	{
		if( !empty($_FILES) && ( $serviceId > 0 ) )
		{
			unset($_FILES['broucher_doc']);
			$defalut_image = 'Y';

			foreach($_FILES as $key=>$val)
			{
				$imgfld=$key;

				if(array_key_exists($imgfld,$_FILES))
				{
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"services");

					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) )
					{
						$add_data = array(
						'service_id'=>$serviceId,
						'media_type'=>'photo',
						'is_default'=>'Y',
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'media_date_added' => $this->config->item('config.date.time')
						);
						$this->services_model->safe_insert('wl_services_media', $add_data ,FALSE );

					}

					$defalut_image = 'N';
				}
			}

		}

  }

	public function edit_service_media($serviceId)
	{
		unset($_FILES['broucher_doc']);
		//Current Media Files resultset
		$media_option = array('serviceid'=>$serviceId);
		$res_photo_media = $this->services_model->get_service_media(5,0, $media_option);
		$res_photo_media = !is_array($res_photo_media ) ? array() : $res_photo_media ;

		$delete_media_files = $this->input->post('service_img_delete'); //checkbox items given for image deletion

		$arr_delete_items = array(); //holding our deleted ids for later use

		/* Tracking delete media ids coming from checkboxes */

		if(is_array($delete_media_files) && !empty($delete_media_files))
		{

			foreach($res_photo_media as $key=>$val)
			{
				$media_id = $val['id'];
				if(array_key_exists($media_id,$delete_media_files))
				{
					 $media_file = $res_photo_media[$key]['media'];
					 $unlink_image = array('source_dir'=>"services",'source_file'=>$media_file);
					 removeImage($unlink_image);
					 array_push($arr_delete_items,$media_id);
				}
			}
		}

		/* Tracking Ends */

		/* Iterating Form Files */


		if( !empty($_FILES) && ( $serviceId > 0 ) )
		{
			$sx = 0;
			foreach($_FILES as $key=>$val)
			{
				$imgfld=$key;


				if(array_key_exists($imgfld,$_FILES))
				{
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"services");

					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) )
					{
						/*  uploading successful  */
						$add_data = array(
						'service_id'=>$serviceId,
						'media_type'=>'photo',
						'is_default'=>'Y',
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'media_date_added' => $this->config->item('config.date.time')
						);

						/* If there already exists record in the database update then else insert new entry
						   $res_photo_media  holding existing resultset from databse in the form given below:
						   $res_photo_media = array( 0 => array(row1) )

						*/

						if(array_key_exists($sx,$res_photo_media))
						{
						       $media_id  = $res_photo_media[$sx]['id'];
							   $media_file = $res_photo_media[$sx]['media'];
						       $where = "id = '".$media_id."'";
				               $this->services_model->safe_update('wl_services_media',$add_data,$where,FALSE);
							   $unlink_image = array('source_dir'=>"services",'source_file'=>$media_file);
							   removeImage($unlink_image);

							   /* New File has been browsed and delete checkbox also checked for this file */
							   /* This  media id cannot be removed as it been browsed and updated */
							   if(in_array($media_id,$arr_delete_items))
							   {
									$media_del_index = array_search($media_id,$arr_delete_items);
									unset($arr_delete_items[$media_del_index]);
							   }


						}else
						{
							$this->services_model->safe_insert('wl_services_media', $add_data ,FALSE );

						}

					}


				}
				$sx++;
			}

		}

		if(!empty($arr_delete_items))
		{
			$del_ids = implode(',',$arr_delete_items);
			$where = " id IN(".$del_ids.") ";
			$this->services_model->delete_in('wl_services_media',$where,FALSE);
		}

  }

	public function related()
	{
		$serviceId =  (int) $this->uri->segment(4);
		$option = array('serviceid'=>$serviceId);
		$res =  $this->services_model->get_services(1,0, $option);


		if( is_array($res) )
		{
			$data['heading_title'] = "Add Related services";

			$fetch_related_services  = $this->services_model->related_services_added($res['service_id']);

			if(empty($fetch_related_services))
			{
				$fetch_not_ids = array($res['service_id']);

			}else
			{
				$fetch_not_ids=array_values($fetch_related_services);
				array_push($fetch_not_ids,$res['service_id']);

			}

			$res_array  = $this->services_model->get_related_services($fetch_not_ids);
			$data['res'] = $res_array;

				/* Add related services */

				if($this->input->post('add_related')=='Add related service')
				{

					$this->add_related_services($res['service_id']);
					$this->session->set_flashdata('message',"Related service has been added successfully." );
					redirect('sitepanel/services/related/'.$serviceId, '');

				}

			/* End of  related services */

			$this->load->view('services/view_add_related_services',$data);
		}

	}

	public function add_related_services($service_id)
	{
		$arr_ids = $this->input->post('arr_ids');

		if( is_array($arr_ids))
		{
			foreach($arr_ids as $val )
			{
				$rec_exits = $this->services_model->is_record_exits('wl_services_related', array('condition'=>"related_id =".$val." AND service_id =".$service_id." "));
				if( !$rec_exits )
				{
					$posted_data = array(
					'service_id'=>$service_id,
					'related_id'=>$val,
					'related_date_added'=>$this->config->item('config.date.time')
					);
					$this->services_model->safe_insert('wl_services_related',$posted_data,FALSE);
				}
			}
		}
	}

	public function remove_related_services($serviceId)
	{
		$arr_ids = $this->input->post('arr_ids');
		if( is_array($arr_ids) )
		{
			if($this->input->post('remove_related')=='Remove service')
			{
				foreach($arr_ids as $val )
				{
					$data = array('id'=>$val );
					$this->services_model->safe_delete('wl_services_related',$data,FALSE);
				}

			}
		}
	}

	public function view_related()
	{
		$serviceId =  (int) $this->uri->segment(4);
		$option = array('serviceid'=>$serviceId);
		$res =  $this->services_model->get_services(1,0, $option);

		if( is_array($res) )
		{
			$data['heading_title'] = "View Related services";
			$res_array  = $this->services_model->related_services($res['service_id']);
			$data['res'] = $res_array;

			/* Remove related services */

				if($this->input->post('remove_related')=='Remove service')
				{
					$this->remove_related_services($res['service_id']);
					$this->session->set_flashdata('message',"Related service has been removed successfully." );
				    redirect('sitepanel/services/view_related/'.$serviceId, '');

				}

			/* End of  remove related services */

			$this->load->view('services/view_related_services',$data);


		}

	}

	public function check_price()
	{
		$disc_price = floatval($this->input->post('service_discounted_price'));
		$price      = floatval($this->input->post('service_price'));
		if($disc_price>=$price)
		{
			$this->form_validation->set_message('check_price', 'Discount price must be less than actual price.');
			return FALSE;
		}else
		{
			return TRUE;
		}
	}
	
	public function check_price_zero()
	{
		$price      = floatval($this->input->post('service_price'));
		if($price <= 0)
		{
			$this->form_validation->set_message('check_price_zero', 'Price must be a postive numeric number.');
			return FALSE;
		}else
		{
			return TRUE;
		}
	}

	public function checkurl(){

	  		$service_id=(int)$this->input->post('service_id');

			if($service_id!=''){
				$cont='and entity_id !='.$service_id;
			}else{
				$cont='';
			}

	 		$posted_friendly_url = $this->input->post('friendly_url');

			$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

			$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();

			if($urlcount>0)
			{
				$this->form_validation->set_message('checkurl', 'URL already exists.');
			    return FALSE;

			}else
			{
				 return TRUE;
			}

  }
	
	
}
// End of controller