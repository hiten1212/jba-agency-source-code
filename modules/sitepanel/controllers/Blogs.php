<?php
class Blogs extends Admin_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('blogs/blog_model'));
		$this->load->helper('category/category');
		$this->config->set_item('menu_highlight','blog management');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}

	public  function index($page = NULL){

		$condtion = array();
		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter'=>'result'),array('per_page'));
		$category_id = (int) $this->input->get_post('category_id');		

		$status = $this->input->get_post('status',TRUE);

		$cat_name = '';
		if($category_id > 0 ){
			$condtion['category_id'] = $category_id;
			$cat_name       = 'in ';
			$cat_name .= get_db_field_value('wl_categories','category_name'," AND category_id='$category_id'");
		}

		if($status!=''){
			$condtion['status'] = $status;
		}

		$res_array               =  $this->blog_model->get_blogs($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Blogs Lists';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);		

		if( $this->input->post('status_action')!=''){
			if( $this->input->post('status_action')=='Delete'){
				$prod_id=$this->input->post('arr_ids');

				foreach($prod_id as $v){
					$where = array('entity_type'=>'blogs/detail','entity_id'=>$v);
					$this->blog_model->safe_delete('wl_meta_tags',$where,TRUE);
				}
			}
			$this->update_status('wl_blogs','blogs_id');
		}
		/* Blog set as a */
		if( $this->input->post('set_as')!='' ){
		    $set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_blogs','blogs_id',array($set_as=>'1'));
		}
		if( $this->input->post('unset_as')!='' ){
		    $unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_blogs','blogs_id',array($unset_as=>'0'));
		}
		/* End blog set as a */
		$data['category_result_found'] = "Total ".$config['total_rows']." result(s) found ".strtolower($cat_name)." ";
		$this->load->view('catalog/view_blog_list',$data);
	}

	public function add(){

		$data['heading_title'] = 'Add Blog';
		$categoryposted=$this->input->post('catid');
		$data['categoryposted']=$categoryposted;
		$categoryposted=$this->input->post('catid');
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		//$this->form_validation->set_rules('category_id','Category Name',"trim|required");
		$this->form_validation->set_rules('blog_name','Blog Name',"trim|required|max_length[255]|unique[wl_blogs.blog_name='".$this->db->escape_str($this->input->post('blog_name'))."' AND status!='3']");
				
		//$this->form_validation->set_rules('posted_by','Posted By',"trim|required");
	
		//$this->form_validation->set_rules('youtube','Youtube Link',"trim|valid_url");

		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");	
				
		$this->form_validation->set_rules('blog_description','Description',"max_length[25000]");	
		
		$this->form_validation->set_rules('blog_images1','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images2','Image2',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images3','Image3',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images4','Image4',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images5','Image5',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");	
		$this->form_validation->set_rules('blog_alt','Alt',"trim|max_length[100]");

		if($this->form_validation->run()===TRUE){	
			
			$redirect_url = "blogs/detail";	
			$blog_alt = $this->input->post('blog_alt');
			if($blog_alt ==''){
				$blog_alt = $this->input->post('blog_name');
			}
		  				
			$uploaded_file = "";				
			if( !empty($_FILES) && $_FILES['brochure_doc']['name']!='' )
			{			  
				$this->load->library('upload');	
					
				$uploaded_data =  $this->upload->my_upload('brochure_doc','brochures');
			
				if( is_array($uploaded_data)  && !empty($uploaded_data) )
				{ 								
					$uploaded_file = $uploaded_data['upload_data']['file_name'];
				
				}		
				
			}	
			
			$posted_data = array(				
			'blog_name'=>$this->input->post('blog_name',TRUE),
			'blog_alt'=>$this->input->post('blog_alt',TRUE),
			'posted_by'=>$this->input->post('posted_by',TRUE),
			'friendly_url'=>$this->cbk_friendly_url, //url_title($this->input->post('blog_name')),			
			'blog_description'=>$this->input->post('blogs_description'),
			'added_date'=>$this->config->item('config.date.time')
			);			
			//trace($posted_data);
			//exit;
			$posted_data = $this->security->xss_clean($posted_data);
			$blogId = $this->blog_model->safe_insert('wl_blogs',$posted_data,FALSE);
			$this->add_blog_media($blogId);
			if( $blogId > 0 ){
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$blogId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('blog_name'),80),
							  'meta_description'=>get_text($this->input->post('blogs_description')),
							  'meta_keyword'=>get_keywords($this->input->post('blogs_description'))
							  );
			  create_meta($meta_array);
			}			

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/blogs?category_id='.$this->input->post('category_id'), '');
		}		

		$this->load->view('catalog/view_blog_add',$data);
	}





	public function edit($blogId){
		
		$data['heading_title'] = 'Edit Blog';
		$blogId = (int) $this->uri->segment(4);
		$option = array('blogid'=>$blogId);
		$res =  $this->blog_model->get_blogs(1,0, $option);
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');
				
		$this->form_validation->set_rules('blog_name','Blog Name',"trim|required|max_length[255]|unique[wl_blogs.blog_name='".$this->db->escape_str($this->input->post('blog_name'))."' AND status!='3' AND blogs_id != '".$blogId."']");	
				
		//$this->form_validation->set_rules('posted_by','Posted By',"trim|required");		
		$this->form_validation->set_rules('youtube','Youtube Link',"trim|valid_url");
		
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");	
				
		$this->form_validation->set_rules('blog_description','Description',"max_length[25000]");		
		
		$this->form_validation->set_rules('blog_images1','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images2','Image2',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images3','Image3',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images4','Image4',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('blog_images5','Image5',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");			
		$this->form_validation->set_rules('blog_alt','Alt',"trim|max_length[100]");

		if( is_array( $res ) && !empty( $res )){			

			if($this->form_validation->run()==TRUE){				

				$uploaded_file = $res['brochure_doc'];				
			    if( !empty($_FILES) && $_FILES['brochure_doc']['name']!='' )
				{			  
					$this->load->library('upload');	
						
					$uploaded_data =  $this->upload->my_upload('brochure_doc','brochures');
				
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
					
					}		
					
				}
				
				$category_id=$this->input->post('category_id');				
				$category_links = get_parent_categories($category_id,"AND status!='2'","category_id,parent_id");
				$category_links = array_keys($category_links);
				$category_links = implode(",",$category_links);	
								
				$posted_data = array(
				'category_id'=>$category_id,
				//'category_links'=>$category_links,	
				'blog_name'=>$this->input->post('blog_name',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,
				'blog_alt'=>$this->input->post('blog_alt',TRUE),
				'posted_by'=>$this->input->post('posted_by',TRUE),
				//'brochure_doc'=>$uploaded_file,
				'youtube'=>$this->input->post('youtube',TRUE),
								
				'blog_description'=>$this->input->post('blog_description'),
				'updated_date'=>$this->config->item('config.date.time')
				);
				//trace($posted_data);
				//exit;		
				$posted_data = $this->security->xss_clean($posted_data);
				$where = "blogs_id = '".$res['blogs_id']."'";
				$this->blog_model->safe_update('wl_blogs',$posted_data,$where,FALSE);
				$this->edit_blog_media($res['blogs_id']);
				update_meta_page_url('blogs/detail',$res['blogs_id'],$this->cbk_friendly_url);
				
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/blogs/'. query_string(), '');
			}
			
			
			$data['res']=$res;
			
			$media_option = array('blogid'=>$res['blogs_id']);
			$res_photo_media = $this->blog_model->get_blog_media(5,0, $media_option);
			$data['res_photo_media']=$res_photo_media;			
							
			$this->load->view('catalog/view_blog_edit',$data);
		}else{
			redirect('sitepanel/blogs', '');
		}
	}

	public function add_blog_media($blogId){
		
		//unset($_FILES['brochure_doc']);
				
		if( !empty($_FILES) && ( $blogId > 0 ) ){			
			$defalut_image = 'Y';
			foreach($_FILES as $key=>$val){
				$imgfld=$key;
				if(array_key_exists($imgfld,$_FILES)){
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"blogs");
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ){
						$add_data = array(
						'blogs_id'=>$blogId,
						'media_type'=>'photo',
						'is_default'=>$defalut_image,
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'date_added' => $this->config->item('config.date.time')
						);
						$this->blog_model->safe_insert('wl_blogs_media', $add_data ,FALSE );
					}
					$defalut_image = 'N';
				}
			}
		}

  }

	public function edit_blog_media($blogId){
		//Current Media Files resultset
		//unset($_FILES['brochure_doc']);
		$media_option = array('blogid'=>$blogId);
		$res_photo_media = $this->blog_model->get_blog_media(5,0, $media_option);
		$res_photo_media = !is_array($res_photo_media ) ? array() : $res_photo_media ;
		$delete_media_files = $this->input->post('blog_img_delete'); //checkbox items given for image deletion
		$arr_delete_items = array(); //holding our deleted ids for later use
		/* Tracking delete media ids coming from checkboxes */
		if(is_array($delete_media_files) && !empty($delete_media_files)){

			foreach($res_photo_media as $key=>$val){
				$media_id = $val['id'];
				if(array_key_exists($media_id,$delete_media_files))	{
					 $media_file = $res_photo_media[$key]['media'];
					 $unlink_image = array('source_dir'=>"blogs",'source_file'=>$media_file);
					 removeImage($unlink_image);
					 array_push($arr_delete_items,$media_id);
				}
			}
		}

		/* Tracking Ends */

		/* Iterating Form Files */

		if( !empty($_FILES) && ( $blogId > 0 ) ){
			$sx = 0;
			foreach($_FILES as $key=>$val){
				$imgfld=$key;
				if(array_key_exists($imgfld,$_FILES)){
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"blogs");
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ){
						/*  uploading successful  */
						$add_data = array(
						'blogs_id'=>$blogId,
						'media_type'=>'photo',
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'date_added' => $this->config->item('config.date.time')
						);

						/* If there already exists record in the database update then else insert new entry
						   $res_photo_media  holding existing resultset from databse in the form given below:
						   $res_photo_media = array( 0 => array(row1) )
						*/


						if(array_key_exists($sx,$res_photo_media)){
						       $media_id  = $res_photo_media[$sx]['id'];
							   $media_file = $res_photo_media[$sx]['media'];
						       $where = "id = '".$media_id."'";
				               $this->blog_model->safe_update('wl_blogs_media',$add_data,$where,FALSE);
							   $unlink_image = array('source_dir'=>"blogs",'source_file'=>$media_file);
							   removeImage($unlink_image);
							   /* New File has been browsed and delete checkbox also checked for this file */
							   /* This  media id cannot be removed as it been browsed and updated */
							   if(in_array($media_id,$arr_delete_items)){
									$media_del_index = array_search($media_id,$arr_delete_items);
									unset($arr_delete_items[$media_del_index]);
							   }

						}else{
							$this->blog_model->safe_insert('wl_blogs_media', $add_data ,FALSE );
						}
					}
				}
				$sx++;
			}
		}

		if(!empty($arr_delete_items)){
			$del_ids = implode(',',$arr_delete_items);
			$where = " id IN(".$del_ids.") ";
			$this->blog_model->delete_in('wl_blogs_media',$where,FALSE);
		}
  }


	public function delete_pdf(){
		
		$pId=(int)$this->uri->segment(4,0);	
		if($pId > 0){
			$pdf_name='brochure_doc';
			$file=get_db_field_value('wl_blogs', $pdf_name, array("blogs_id"=>$pId));
			if($file !="" && @file_exists(UPLOAD_DIR."/brochures/".$file)){	
				@unlink(UPLOAD_DIR."/brochures/".$file);
				$pblog_data = array(
					'brochure_doc'=>""
				);
				$where = "blogs_id = '".$pId."' ";
				$this->blog_model->safe_update('wl_blogs',$pblog_data,$where,FALSE);
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success','File Deleted!');
				redirect('sitepanel/blogs/edit/'.$pId, '');	
			}else{
				redirect('blogs', '');
			}
		}else{
			redirect('blogs', '');
		}
	}

	public function comments()
	{
		$condtion               = array();
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$category_id            =  (int) $this->uri->segment(4,0);
		$status			        =   $this->input->get_post('status',TRUE);
		
		if($this->uri->segment(4)>0)
		{
			$condtion['blogid'] =	$this->uri->segment(4);
		}
		
		$res_array =  $this->blog_model->get_review($config['limit'],$offset,$condtion);
		//echo_sql();exit;
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Blog Comment List';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		
		if( $this->input->post('status_action')!='')
		{
			 $this->update_status('tbl_blog_review','review_id');
		}

		$this->load->view('catalog/view_comment_list',$data);
	}
	
	
	public function comments_reply()
	{
		$condtion               = array();
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		=  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url               =   current_url_query_string(array('filter'=>'result'),array('per_page'));
		$blog_id            	=  (int) $this->uri->segment(4,0);
		$review_id              =  (int) $this->uri->segment(5,0);
		$status			        =   $this->input->get_post('status',TRUE); 
		
		if($this->uri->segment(4)>0)
		{
			$condtion['blog_id'] =	$this->uri->segment(4);
		}
		if($this->uri->segment(5)>0)
		{
			$condtion['review_id'] =	$this->uri->segment(5);
		}
		
		$res_array =  $this->blog_model->get_review_replay($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Article Comment Reply Lists';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		
		if( $this->input->post('status_action')!='')
		{
			 $this->update_status('tbl_blog_review_reply','id');
		}

		$this->load->view('catalog/view_comment_reply_list',$data);
	}


	public function checkurl(){

		$blog_id=(int)$this->input->post('blogs_id');

		

		if($blog_id!=''){

			$cont='and entity_id !='.$blog_id;

		}else{

			$cont='';

		}

		

		$posted_friendly_url = $this->input->post('friendly_url');

		

		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();

		

		if($urlcount>0){

			$this->form_validation->set_message('checkurl', 'URL already exists.');

			return FALSE;

		}else{

			return TRUE;

		}

		

	}

	

}

// End of controller