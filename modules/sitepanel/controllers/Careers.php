<?php
class Careers extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('careers/careers_model'));
		$this->config->set_item('menu_highlight','job management');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}

	public  function index($page = NULL){

		$condtion = array();
		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter'=>'result'),array('per_page'));
		
		$status = $this->input->get_post('status',TRUE);
		
		if($status!='')
		{
			$condtion['status'] = $status;
		}

		$res_array               =  $this->careers_model->get_careers($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Careers Lists';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');
				foreach($prod_id as $v){
					$where = array('entity_type'=>'careers/detail','entity_id'=>$v);
					$this->careers_model->safe_delete('wl_meta_tags',$where,TRUE);
					}
			}
			$this->update_status('wl_jobs','jobs_id');
		}

		/* job set as a */

		if( $this->input->post('set_as')!='' )
		{
		    $set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_jobs','jobs_id',array($set_as=>'1'));
		}

		if( $this->input->post('unset_as')!='' )
		{
		    $unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_jobs','jobs_id',array($unset_as=>'0'));
		}
		/* End job set as a */		

		$data['category_result_found'] = "Total ".$config['total_rows']." result(s) found ".strtolower($cat_name)." ";
		$this->load->view('careers/view_job_list',$data);

	}


	public function add()
	{
		$data['heading_title'] = 'Add';
		$categoryposted=$this->input->post('catid');
		$data['categoryposted']=$categoryposted;
		$categoryposted=$this->input->post('catid');
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'description'));
		
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$this->form_validation->set_rules('job_title','Job Title',"trim|required|max_length[255]");
		
		$this->form_validation->set_rules('salary_range','salary range',"trim|max_length[255]");
		
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");	
		$this->form_validation->set_rules('location','Location',"trim|max_length[255]");	
		$this->form_validation->set_rules('jobs_description','Description',"max_length[8500]|required");

		if($this->form_validation->run()===TRUE)
		{
			$redirect_url = "careers/detail";			
			$job_alt = $this->input->post('job_alt');

			if($job_alt ==''){
				$job_alt = $this->input->post('job_title');
			}			
			$posted_data = array(	
			'job_title'=>$this->input->post('job_title',TRUE),			
			'friendly_url'=>$this->cbk_friendly_url,
			'location'=>$this->input->post('location',TRUE),
			'salary_range'=>$this->input->post('salary_range',TRUE),									
			'jobs_description'=>$this->input->post('jobs_description',TRUE),
			'job_added_date'=>$this->config->item('config.date.time')
			);
			
			$posted_data = $this->security->xss_clean($posted_data);
			$jobId = $this->careers_model->safe_insert('wl_jobs',$posted_data,FALSE);

			if( $jobId > 0 )
			{
			  $meta_array  = array(
				  'entity_type'=>$redirect_url,
				  'entity_id'=>$jobId,
				  'page_url'=>$this->cbk_friendly_url,
				  'meta_title'=>get_text($this->input->post('job_title'),80),
				  'meta_description'=>get_text($this->input->post('jobs_description')),
				  'meta_keyword'=>get_keywords($this->input->post('jobs_description'))
				  );
			  	  create_meta($meta_array);
			}
			
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/careers', '');
		}
		$this->load->view('careers/view_job_add',$data);
	}


	public function edit($jobId)
	{

		$data['heading_title'] = 'Edit';
		$jobId = (int) $this->uri->segment(4);
		$option = array('jobid'=>$jobId);
		$res =  $this->careers_model->get_careers(1,0, $option);
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'description'));		

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$this->form_validation->set_rules('job_title','job Title',"trim|required|max_length[255]");
					
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		
		$this->form_validation->set_rules('location','Location',"trim|max_length[255]");
		$this->form_validation->set_rules('salary_range','salary range',"trim|max_length[255]");
		
		$this->form_validation->set_rules('jobs_description','Description',"max_length[8500]|required");		
				
		if( is_array( $res ) && !empty( $res ))
		{
			if($this->form_validation->run()==TRUE)
			{
				$posted_data = array(				
				'job_title'=>$this->input->post('job_title',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,
				'location'=>$this->input->post('location',TRUE),
				'salary_range'=>$this->input->post('salary_range',TRUE),										
				'jobs_description'=>$this->input->post('jobs_description',TRUE),			
				'job_added_date'=>$this->config->item('config.date.time')
				);
				
				$posted_data = $this->security->xss_clean($posted_data);
				$where = "jobs_id = '".$res['jobs_id']."'";
				$this->careers_model->safe_update('wl_jobs',$posted_data,$where,FALSE);				
				update_meta_page_url('careers/detail',$res['jobs_id'],$this->cbk_friendly_url);
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/careers/'. query_string(), '');

			}
			$data['res']=$res;			
			$this->load->view('careers/view_job_edit',$data);

		}else
		{
			redirect('sitepanel/jobs', '');
		}

	}

	public function add_job_media($jobId)
	{
		if( !empty($_FILES) && ( $jobId > 0 ) )
		{
			$defalut_image = 'Y';

			foreach($_FILES as $key=>$val)
			{
				$imgfld=$key;

				if(array_key_exists($imgfld,$_FILES))
				{
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"jobs");

					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) )
					{
						$add_data = array(
						'jobs_id'=>$jobId,
						'media_type'=>'photo',
						'is_default'=>$defalut_image,
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'media_date_added' => $this->config->item('config.date.time')
						);
						$this->job_model->safe_insert('wl_jobs_media', $add_data ,FALSE );
					}

					$defalut_image = 'N';
				}
			}

		}

  }

	public function edit_job_media($jobId)
	{
		//Current Media Files resultset
		$media_option = array('jobid'=>$jobId);
		$res_photo_media = $this->job_model->get_job_media(4,0, $media_option);
		$res_photo_media = !is_array($res_photo_media ) ? array() : $res_photo_media ;

		$delete_media_files = $this->input->post('job_img_delete'); //checkbox items given for image deletion

		$arr_delete_items = array(); //holding our deleted ids for later use

		/* Tracking delete media ids coming from checkboxes */

		if(is_array($delete_media_files) && !empty($delete_media_files))
		{

			foreach($res_photo_media as $key=>$val)
			{
				$media_id = $val['id'];
				if(array_key_exists($media_id,$delete_media_files))
				{
					 $media_file = $res_photo_media[$key]['media'];
					 $unlink_image = array('source_dir'=>"Jobs",'source_file'=>$media_file);
					 removeImage($unlink_image);
					 array_push($arr_delete_items,$media_id);
				}
			}
		}

		/* Tracking Ends */

		/* Iterating Form Files */

		if( !empty($_FILES) && ( $jobId > 0 ) )
		{
			$sx = 0;
			foreach($_FILES as $key=>$val)
			{
				$imgfld=$key;

				if(array_key_exists($imgfld,$_FILES))
				{
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"Jobs");

					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) )
					{
						/*  uploading successful  */
						$add_data = array(
						'jobs_id'=>$jobId,
						'media_type'=>'photo',
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'job_code'=>$this->input->post('job_code'),
						'media_date_added' => $this->config->item('config.date.time')
						);

						/* If there already exists record in the database update then else insert new entry
						   $res_photo_media  holding existing resultset from databse in the form given below:
						   $res_photo_media = array( 0 => array(row1) )

						*/

						if(array_key_exists($sx,$res_photo_media))
						{
						       $media_id  = $res_photo_media[$sx]['id'];
							   $media_file = $res_photo_media[$sx]['media'];
						       $where = "id = '".$media_id."'";
				               $this->job_model->safe_update('wl_jobs_media',$add_data,$where,FALSE);
							   $unlink_image = array('source_dir'=>"Jobs",'source_file'=>$media_file);
							   removeImage($unlink_image);

							   /* New File has been browsed and delete checkbox also checked for this file */
							   /* This  media id cannot be removed as it been browsed and updated */
							   if(in_array($media_id,$arr_delete_items))
							   {
									$media_del_index = array_search($media_id,$arr_delete_items);
									unset($arr_delete_items[$media_del_index]);
							   }


						}else
						{
							$this->job_model->safe_insert('wl_jobs_media', $add_data ,FALSE );

						}

					}


				}
				$sx++;
			}

		}

		if(!empty($arr_delete_items))
		{
			$del_ids = implode(',',$arr_delete_items);
			$where = " id IN(".$del_ids.") ";
			$this->job_model->delete_in('wl_jobs_media',$where,FALSE);
		}

  }

	 public function checkurl(){

	  		$job_id=(int)$this->input->post('jobs_id');

			if($job_id!=''){
				$cont='and entity_id !='.$job_id;
			}else{
				$cont='';
			}

	 		$posted_friendly_url = $this->input->post('friendly_url');

			$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

			$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();

			if($urlcount>0)
			{
				$this->form_validation->set_message('checkurl', 'URL already exists.');
			    return FALSE;

			}else
			{
				 return TRUE;
			}

  }
		
}
// End of controller