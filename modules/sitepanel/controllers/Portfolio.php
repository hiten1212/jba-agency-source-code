<?php
class Portfolio extends Admin_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('portfolio/portfolio_model'));
	    $this->config->set_item('menu_highlight','portfolio');
		$this->form_validation->set_error_delimiters("<p class='required'>","</p>");
	}

	public function index(){

			$pagesize                =  (int) $this->input->get_post('pagesize');
			$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
			$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
			$base_url                =  current_url_query_string(array('filter'=>'result'),array('per_page'));
			$param = array();
			$res_array                 = $this->portfolio_model->get_portfolio($config['limit'],$offset,$param);
			$config['total_rows']	  = get_found_rows();
		    $data['page_links']      =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
			$data['heading_title'] = 'Portfolio';
			$data['res'] = $res_array; 

		    if( $this->input->post('status_action')!=''){

				   $this->update_status('wl_portfolio','portfolio_id');

			}
			/* Portfolio set as a */	

		if( $this->input->post('set_as')!='' ){	
			
			//featured_product hot_products new_arrival_products
		    $set_as    = $this->input->post('set_as',TRUE);	
			if($set_as=='Set Latest'){$set_as='latest_portfolio';}
			$this->set_as('wl_portfolio','portfolio_id',array($set_as=>'1'));
		}
		if( $this->input->post('unset_as')!='' ){

		    $unset_as   = $this->input->post('unset_as',TRUE);
			if($unset_as=='Unset Latest'){$unset_as='latest_portfolio';}
			$this->set_as('wl_portfolio','portfolio_id',array($unset_as=>'0'));
		}
		/* End portfolio set as a */

		$this->load->view('sitepanel/portfolio/view_portfolio_list',$data);

	}

	public function add(){

		$data['heading_title'] = "Add Portfolio";
		$pdf_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_dim           =  $this->config->item('allow.imgage.dimension');
		$data['ckeditor']    =  set_ck_config(array('textarea_id'=>'description'));
		$seo_url_length = $this->config->item('seo_url_length');
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

		$this->form_validation->set_rules('portfolio_title','Portfolio Title',"required|max_length[90]|unique[wl_portfolio.portfolio_title='".$this->db->escape_str($this->input->post('portfolio_title'))."' AND status!='2' ]");

		$this->form_validation->set_rules('friendly_url','Page URL',"required|max_length[$seo_url_length]|callback_checkurl");

		$this->form_validation->set_rules('portfolio_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

		$this->form_validation->set_rules('portfolio_description','Description','required|max_length[15000]');

		$this->form_validation->set_rules('alt_tag','Alt Tag','trim|max_length[150]');

		if($this->form_validation->run()==TRUE){

			$uploaded_file = "";

			if( !empty($_FILES) && $_FILES['portfolio_image']['name']!='' ){	

				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_upload('portfolio_image','portfolio');
				if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 

				   $uploaded_file = $uploaded_data['upload_data']['file_name'];	
				}

			}

			$posted_data=array(
				'portfolio_title'=> $this->input->post('portfolio_title',TRUE),
				'portfolio_image'=>$uploaded_file,
				'portfolio_description' =>$this->input->post('portfolio_description',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,
				'alt_tag' =>$this->input->post('alt_tag',TRUE),
				'status'=>'1',
				'date_added'  =>$this->config->item('config.date.time')
			);
			$posted_data = $this->security->xss_clean($posted_data);
			$insertId=$this->portfolio_model->safe_insert('wl_portfolio',$posted_data,FALSE); 

			if( $insertId > 0 ){

			  $redirect_url='portfolio/details';
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$insertId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('portfolio_title'),80),
							  'meta_description'=>get_text($this->input->post('portfolio_description')),
							  'meta_keyword'=>get_keywords($this->input->post('portfolio_description'))
							  );
			  create_meta($meta_array);

			}
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/portfolio');

		}

	   $this->load->view('sitepanel/portfolio/view_add_portfolio',$data);

	}

	public function edit(){

	    $this->load->model(array('portfolio/portfolio_model'));
	    $data['ckeditor']    =  set_ck_config(array('textarea_id'=>'portfolio_description'));
	    $id                      = (int) $this->uri->segment(4);
	    $param                   = array('where'=>"wl_portfolio.portfolio_id ='$id' ");
		$res                     = $this->portfolio_model->get_portfolio(1,0,$param);
		$pdf_allow_size			 =  $this->config->item('allow.file.size');
		$img_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_dim           =  $this->config->item('allow.imgage.dimension');

       if( is_array($res) && !empty($res)){	

			$data['heading_title'] = "Edit Portfolio";
			$this->form_validation->set_rules('portfolio_title','Portfolio Title',"trim|required|max_length[90]|unique[wl_portfolio.portfolio_title='".$this->db->escape_str($this->input->post('portfolio_title'))."' AND status!='2' AND portfolio_id!='".$id."' ]");

			$seo_url_length = $this->config->item('seo_url_length');
			$this->cbk_friendly_url = seo_url_title($this->input->post('friendly_url',TRUE));
			$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|unique[wl_meta_tags.page_url='".$this->cbk_friendly_url."' AND entity_id!='".$id."'] ");

			$this->form_validation->set_rules('portfolio_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

			$this->form_validation->set_rules('portfolio_description','Description','trim|required|max_length[15000]');	

			$this->form_validation->set_rules('alt_tag','Alt Tag','trim|max_length[150]');


			if($this->form_validation->run()==TRUE){

				$uploaded_file = $res['portfolio_image'];
				$unlink_image = array('source_dir'=>"portfolio",'source_file'=>$res['portfolio_image']);

				if($this->input->post('portfolio_image_delete')==='Y'){

				  removeImage($unlink_image);
				  $uploaded_file = NULL;
			    }
				if( !empty($_FILES) && $_FILES['portfolio_image']['name']!='' ){

					$this->load->library('upload');	
					$uploaded_data =  $this->upload->my_upload('portfolio_image','portfolio');
					if( is_array($uploaded_data)  && !empty($uploaded_data) ){
					   $uploaded_file = $uploaded_data['upload_data']['file_name'];
					   removeImage($unlink_image);
					}
				}

				$posted_data=array(	
				'portfolio_title'=> $this->input->post('portfolio_title',TRUE),
				'portfolio_image'=>$uploaded_file,
				'friendly_url'=>$this->cbk_friendly_url,
				'portfolio_description' =>$this->input->post('portfolio_description',TRUE),
				'alt_tag' =>$this->input->post('alt_tag',TRUE),
				'date_updated'  =>$this->config->item('config.date.time')
				);

				$posted_data = $this->security->xss_clean($posted_data);
				$where = "portfolio_id = '".$res['portfolio_id']."'"; 
				$this->portfolio_model->safe_update('wl_portfolio',$posted_data,$where,FALSE);
				update_meta_page_url('portfolio/details',$res['portfolio_id'],$this->cbk_friendly_url);

				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/portfolio/'.query_string(),'');

			}

	   }else{
		  redirect('sitepanel/portfolio'); 
	   }
	   $data['res'] = $res;	
	   $this->load->view('sitepanel/portfolio/view_edit_portfolio',$data);
	}

	 public function download_pdf(){

		$id = (int) $this->uri->segment(4);
		$param                  =  array('where'=>"portfolio_id ='$id' ");
		$res_topic                    =  $this->portfolio_model->get_portfolio(1,0,$param);
		if(is_array($res_topic) && !empty($res_topic)){
		  if($res_topic['portfolio_pdf']!='' && file_exists(UPLOAD_DIR."/portfolio/pdf/".$res_topic['portfolio_pdf'])){

			  $this->load->helper('download');
			  $data = file_get_contents(UPLOAD_DIR."/portfolio/pdf/".$res_topic['portfolio_pdf']);
			  $name = $res_topic['portfolio_pdf'];
			  force_download($name, $data);

		  }

		}

	  }

  public function checkurl(){

		$portfolio_id=(int)$this->input->post('portfolio_id');
		if($portfolio_id!=''){
			$cont='and entity_id !='.$portfolio_id;
		}else{
			$cont='';
		}

		$posted_friendly_url = $this->input->post('friendly_url');		
		$cbk_friendly_url = seo_url_title($posted_friendly_url);
		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$cbk_friendly_url."'".$cont."")->num_rows();
		if($urlcount>0){
			$this->form_validation->set_message('checkurl', 'URL already exists.');
			return FALSE;
		}else
		{
			return TRUE;
		}
	}

}

/* End of file pages.php */

?>


