<?php
class Dynamic_pages extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('dynamic_pages/dynamic_pages_model'));
		$this->load->helper('dynamic_pages/dynamic_pages');
		$this->config->set_item('menu_highlight','dynamic pages management');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->default_view = 'dynamic_pages';
	}

	public  function index()
	{
		
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$parent_id              =   (int) $this->uri->segment(4,0);
		
		$keyword = trim($this->input->get_post('keyword',TRUE));
		$keyword = $this->db->escape_str($keyword);
		
		$condtion = "AND parent_id = '$parent_id'";

		$condtion_array = array(
		'field' =>"*,( SELECT COUNT(page_id) FROM wl_dynamic_pages AS b
		WHERE b.parent_id=a.page_id ) AS total_subcategories",
		'condition'=>$condtion,
		'limit'=>$config['limit'],
		'offset'=>$offset	,
		'debug'=>FALSE
		);
		$res_array              =  $this->dynamic_pages_model->getpages($condtion_array);

		$config['total_rows']	=  $this->dynamic_pages_model->total_rec_found;

		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		$data['heading_title']  =  ( $parent_id > 0 ) ? 'Sub Pages' :  'Main Pages';

		$data['res']            =  $res_array;
		$data['parent_id']      =  $parent_id;

		if( $this->input->post('status_action')!='')
		{
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v){
					$rowdata=$this->dynamic_pages_model->get_dynamic_page_by_id($v);
					$total_page  = count_dynamic_page("AND parent_id='$v' ");
					if($total_page>0)
					{
					}else
					{
						$where = array('entity_type'=>'dynamic_pages/detail','entity_id'=>$v);
						$this->dynamic_pages_model->safe_delete('wl_meta_tags',$where,TRUE);
					}
				}
			}
			$this->update_status('wl_dynamic_pages','page_id');
		}
		if( $this->input->post('update_order')!='')
		{
			$this->update_displayOrder('wl_dynamic_pages','sort_order','page_id');
		}
		
		// category set as a
		if( $this->input->post('set_as')!='' )
		{
			$set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_dynamic_pages','page_id',array($set_as=>'1'));
		}
		
		if( $this->input->post('unset_as')!='' )
		{
			$unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_dynamic_pages','page_id',array($unset_as=>'0'));
		}
		// End category set as a
		
		$this->load->view($this->default_view.'/view_page_list',$data);
		
	}
	
	public function add()
	{
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'page_desc'));
		$parent_id         =  (int) $this->uri->segment(4,0);

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$page_name = $this->db->escape_str($this->input->post('page_name'));
		$posted_friendly_url = $this->input->post('friendly_url');
		
		if( $parent_id!='' && $parent_id > 0 )
		{
			$parent_id = applyFilter('NUMERIC_GT_ZERO',$parent_id);
			$data['heading_title'] = 'Add Subpage';
			
			if($parent_id<=0)
			{
				redirect("sitepanel/dynamic_pages");
			}
			
			$parentdata=$this->dynamic_pages_model->get_dynamic_page_by_id($parent_id);

			if(!is_array($parentdata))
			{
				$this->session->set_flashdata('message', lang('invalidRecord'));
				redirect('sitepanel/dynamic_pages', '');
			}
			
			$this->cbk_friendly_url = 	seo_url_title($posted_friendly_url);
			$data['parentData'] = $parentdata;
			
		}else
		{
			$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
			$data['parentData'] = '';
			$data['heading_title'] = 'Add Main Page';
		}
		
		$seo_url_length = $this->config->item('seo_url_length');
		$this->form_validation->set_rules('page_name','Page Name',"trim|required|max_length[100]|unique[wl_dynamic_pages.page_name='".$page_name."' AND status!='2' AND parent_id='".$parent_id."']");
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		$this->form_validation->set_rules('page_description','Description',"required|max_length[6000]");
		$this->form_validation->set_rules('page_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('page_alt','Alt',"trim|max_length[100]");
		
		if($this->form_validation->run()===TRUE)
		{
			$uploaded_file = "";
			
			if( !empty($_FILES) && $_FILES['page_image']['name']!='' )
			{
				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_upload('page_image','dynamic_page_image');
				
				if( is_array($uploaded_data)  && !empty($uploaded_data) )
				{
					$uploaded_file = $uploaded_data['upload_data']['file_name'];
				}
			}
			
			$redirect_url = "dynamic_pages/detail";
			$page_alt = $this->input->post('page_alt');
			
			if($page_alt =='')
			{
				$page_alt = $this->input->post('page_name');
			}
			
			#-------------------MAX SORT ORDER------------#
			$this->db->select_max('sort_order');
			$query = $this->db->get('wl_dynamic_pages');
			$max_sort_order= $query->row_array();
			$max_sort_orders=$max_sort_order['sort_order']+1;
			#--------------------------------------------#
			

			$posted_data = array(
			'page_name'=>$this->input->post('page_name'),
			'page_alt'=>$page_alt,
			'page_description'=>$this->input->post('page_description'),
			'parent_id' =>$parent_id,
			'friendly_url'=>$this->cbk_friendly_url,
			'date_added'=>$this->config->item('config.date.time'),
			'page_image'=>$uploaded_file,
			'sort_order'=>$max_sort_orders
			);

			$posted_data = $this->security->xss_clean($posted_data);
			$insertId = $this->dynamic_pages_model->safe_insert('wl_dynamic_pages',$posted_data,FALSE);

			if( $insertId > 0 )
			{
				$meta_array  = array(
				'entity_type'=>$redirect_url,
				'entity_id'=>$insertId,
				'page_url'=>$this->cbk_friendly_url,
				'meta_title'=>get_text($this->input->post('page_name'),80),
				'meta_description'=>get_text($this->input->post('page_description')),
				'meta_keyword'=>get_keywords($this->input->post('page_description'))
				);
				
				create_meta($meta_array);
			}
			
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			$redirect_path= isset($parentdata) && is_array($parentdata) ? 'dynamic_pages/index/'.$parentdata['page_id'] : 'dynamic_pages';
			redirect('sitepanel/'.$redirect_path, '');

		}
		$data['parent_id'] = $parent_id;
		$this->load->view($this->default_view.'/view_page_add',$data);

	}
	
	public function edit()
	{
		$data['ckeditor'] = set_ck_config(array('textarea_id'=>'page_desc'));
		$pagId = (int) $this->uri->segment(4);
		$rowdata=$this->dynamic_pages_model->get_dynamic_page_by_id($pagId);

		$data['heading_title'] = ($rowdata['parent_id'] > 0 ) ? 'Subpage' : 'Main Page';
		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		if( !is_array($rowdata) )
		{
			$this->session->set_flashdata('message', lang('idmissing'));
			redirect('sitepanel/dynamic_pages', '');
		}
		
		$pageId = $rowdata['page_id'];
		$this->form_validation->set_rules('page_name','Page Name',"trim|required|max_length[100]|unique[wl_dynamic_pages.page_name='".$this->db->escape_str($this->input->post('page_name'))."' AND status!='2' AND parent_id='".$rowdata['parent_id']."' AND page_id!='".$pageId."']");
		$seo_url_length = $this->config->item('seo_url_length');
		$this->cbk_friendly_url = seo_url_title($this->input->post('friendly_url',TRUE));
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");
		$this->form_validation->set_rules('page_description','Description',"required|max_length[6000]");
		
		$this->form_validation->set_rules('page_image','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('page_alt','Alt',"trim|max_length[100]");
		
		if($this->form_validation->run()==TRUE)
		{
			$uploaded_file = $rowdata['page_image'];
			$unlink_image = array('source_dir'=>"category",'source_file'=>$rowdata['page_image']);
			if($this->input->post('page_img_delete')==='Y')
			{
				removeImage($unlink_image);
				$uploaded_file = NULL;
			}
			if( !empty($_FILES) && $_FILES['page_image']['name']!='' )
			{
				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_upload('page_image','dynamic_page_image');
				
				if( is_array($uploaded_data)  && !empty($uploaded_data) )
				{
					$uploaded_file = $uploaded_data['upload_data']['file_name'];
					removeImage($unlink_image);
				}
			}
			
			
			$page_alt = $this->input->post('page_alt');
			
			if($page_alt ==''){
				$page_alt = $this->input->post('page_name');
			}
			
			$posted_data = array(
			'page_name'=>$this->input->post('page_name'),
			'page_alt'=>$page_alt,
			'page_description'=>$this->input->post('page_description'),
			'friendly_url'=>$this->cbk_friendly_url,
			'page_image'=>$uploaded_file,
			);
			
			$posted_data = $this->security->xss_clean($posted_data);
			$where = "page_id = '".$pageId."'";
			$this->dynamic_pages_model->safe_update('wl_dynamic_pages',$posted_data,$where,FALSE);

			update_meta_page_url('dynamic_pages/detail',$pageId,$this->cbk_friendly_url);

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('successupdate'));
			$redirect_path= $rowdata['parent_id']>0 ? 'dynamic_pages/index/'. $rowdata['parent_id'] : 'dynamic_pages';

			redirect('sitepanel/'.$redirect_path.'/'.query_string(), '');

		}

		$data['pageresult']=$rowdata;
		$this->load->view($this->default_view.'/view_page_edit',$data);

	}
	
	public function delete()
	{
		$catId = (int) $this->uri->segment(4,0);
	  $rowdata=$this->dynamic_pages_model->get_dynamic_page_by_id($catId);

	  if( !is_array($rowdata) )
	  {
			$this->session->set_flashdata('message', lang('idmissing'));
			redirect('sitepanel/dynamic_pages', '');
		}
		else
		{
			$total_page  = count_dynamic_page("AND parent_id='$catId' ");
		  
		  if( $total_page>0 )
		  {
			  $this->session->set_userdata(array('msg_type'=>'error'));
			  $this->session->set_flashdata('error',lang('child_to_delete'));
		  }else
		  {
			  $where = array('page_id'=>$catId);
			  $this->dynamic_pages_model->safe_delete('wl_dynamic_pages',$where,TRUE);
			  $entity_type = "dynamic_pages/detail";
			  $where = array('entity_id'=>$catId,"entity_type"=>$entity_type);
			  $this->utils_model->safe_delete('wl_meta_tags',$where,FALSE); 
			  $this->session->set_userdata(array('msg_type'=>'success'));
			  $this->session->set_flashdata('success',lang('deleted') );
		  }
		  redirect($_SERVER['HTTP_REFERER'], '');
	  }
	}
	
	public function checkurl(){
		$page_id=(int)$this->input->post('page_id');
		
		if($page_id!=''){
			$cont='and entity_id !='.$page_id;
		}else{
			$cont='';
		}
		
		$posted_friendly_url = $this->input->post('friendly_url');
		$cbk_friendly_url = seo_url_title($posted_friendly_url);
		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$cbk_friendly_url."'".$cont."")->num_rows();
		
		if($urlcount>0)
		{
			$this->form_validation->set_message('checkurl', 'URL already exists.');
			return FALSE;
		}else
		{
			return TRUE;
		}
		
	}

}
// End of controller