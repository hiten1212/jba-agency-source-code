<?php
class Subadmin extends Admin_Controller 
{
  public function __construct() 
  {
    parent::__construct();

    $this->load->model(array('sitepanel/subadmin_model'));
    $this->load->library(array('safe_encrypt'));
	$this->config->set_item('menu_highlight','subadmin management');	
  }
  
  public  function index()
  {	  
     
	     $condtion               = array();	
				
    	 $pagesize               =  (int) $this->input->get_post('pagesize');
		 		
	     $config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		 		 				
		 $offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;	
		
		 $base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));		
				
		$status			         =   $this->input->get_post('status',TRUE);
			
		if($status!='')
		{
			$condtion['status'] = $status;
		}

		$admin_type			         =   $this->input->get_post('admin_type',TRUE);
			
		if($admin_type!='')
		{
			$condtion['admin_type'] = $admin_type;
		}	

		$res_array              = $this->subadmin_model->get_subadmins($config['limit'],$offset,$condtion);		//echo_sql();
		$total_record           = get_found_rows();			
		$data['page_links']     =  admin_pagination($base_url,$total_record,$config['limit'],$offset);
		$data['heading_title']  = 'SubAdmins';
		$data['pagelist']       = $res_array; 	
		$data['total_rec']      = $total_record  ;
		
		if( $this->input->post('status_action')!='')
		{			
			$this->update_status('tbl_admin','admin_id');			
		}
		//trace($this->input->post());
		$this->load->view('subadmin/subadmin_list_view',$data); 	
				
	}

	public function add()
	{
				
		 $data['heading_title'] = 'Add SubAdmin';
		
		 $this->form_validation->set_rules('admin_type', 'Group', 'trim|required');
		 $this->form_validation->set_rules('admin_email', 'Email ID','trim|required|valid_email|max_length[80]');
		 $this->form_validation->set_rules('admin_username','Username',"trim|required|max_length[30]|xss_clean|unique[tbl_admin.admin_username='".$this->db->escape_str($this->input->post('admin_username'))."' AND status!='2']");
		 $this->form_validation->set_rules('admin_password', 'Password', 'trim|required|max_length[20]|valid_password');
		 
		 
		if($this->form_validation->run()===TRUE)
		{
			   
				
			 $posted_data = array(
									'admin_key'           =>random_string(),
									'admin_type'=>$this->input->post('admin_type'),
									'admin_username'=>$this->input->post('admin_username'),
									'admin_password'=>$this->input->post('admin_password'),	
									'admin_email'=>$this->input->post('admin_email'),
									'post_date'=>$this->config->item('config.date.time')
								 );
								
		    $this->subadmin_model->safe_insert('tbl_admin',$posted_data,FALSE);	

			/* Intimation mail to subadmin */
			$this->load->library('email');
			$adm_groups = $this->config->item('admin_groups');
			$res_data = $this->db->get_where('tbl_admin',array('admin_id'=>1))->row();

			$mail_to      = $this->input->post('admin_email');
			$mail_subject = "Subadmin registration at ".$this->config->item('site_name'); 
			$from_email   = $res_data->admin_email;
			$from_name    =  $this->config->item('site_name');
			
			$username = $this->input->post('admin_username');
			$password = $this->input->post('admin_password');
			$admin_rights = $adm_groups[$this->input->post('admin_type')];
		  

			$verify_url= "<a href=".base_url()."sitepanel/>Click here </a>";
			
			$body = " Dear friend,<br />
			Your login details are as follows:<br />
			User name :  {username}<br />        
			Password:  {password}<br /> 
			Your credentials will work as {admin_rights}<br />
			Click here to login {link}<br />  <br />						   
			Thanks and Regards,<br />						   
			{site_name} Team  ";

			
			
			$body			=	str_replace('{username}',$username,$body);
			$body			=	str_replace('{password}',$password,$body);
			$body			=	str_replace('{admin_rights}',$admin_rights,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body           =	str_replace('{link}',$verify_url,$body);
					
			$this->email->from($from_email, $from_name);
			$this->email->to($mail_to);			
			$this->email->subject($mail_subject);				
			$this->email->message($body);
			$this->email->set_mailtype('html');
			$this->email->send();
			
			/* End Intimation mail to subadmin */
								
			$this->session->set_userdata(array('msg_type'=>'success'));			
			$this->session->set_flashdata('success',lang('success'));				
			$redirect_path= 'subadmin';			
			redirect('sitepanel/'.$redirect_path, '');		
					
		}	
		
		$this->load->view('subadmin/view_subadmin_add',$data);		  
		  
	}
	
	
	public function edit()
	{
		$admId = (int) $this->uri->segment(4);
		
		$cond = array(
						'admin_id'=>$admId
					  );
		$rowdata=$this->subadmin_model->get_subadmins(1,0,$cond);
				
		
		
		$data['heading_title'] = 'SubAdmin';
		
		if( !is_array($rowdata) )
		{
			$this->session->set_flashdata('message', lang('idmissing'));	
			redirect('sitepanel/subadmin', ''); 	
			
		}
		
		$admId = $rowdata['admin_id'];		 		
		
		$this->form_validation->set_rules('admin_type', 'Group', 'trim|required');	
	   $this->form_validation->set_rules('admin_email', 'Email ID','trim|required|valid_email|max_length[80]');
	   $this->form_validation->set_rules('admin_username','Username',"trim|required|max_length[30]|xss_clean|unique[tbl_admin.admin_username='".$this->db->escape_str($this->input->post('admin_username'))."' AND status!='2' AND admin_id != ".$admId."]");
	   $this->form_validation->set_rules('admin_password', 'Password', 'trim|required|max_length[20]|valid_password');	
		if($this->form_validation->run()==TRUE)
		{	
			$posted_data = array(
				'admin_type'=>$this->input->post('admin_type'),
				'admin_username'=>$this->input->post('admin_username'),
				'admin_password'=>$this->input->post('admin_password'),	
				'admin_email'=>$this->input->post('admin_email')
			 );
			 
			$where = "admin_id = '".$admId."'"; 				
			$this->subadmin_model->safe_update('tbl_admin',$posted_data,$where,FALSE);	

			
			$username = $this->input->post('admin_username');
			$password = $this->input->post('admin_password');
			$admin_rights = $adm_groups[$this->input->post('admin_type')];

			if($rowdata['admin_username']!=$username || $rowdata['admin_password']!=$password || $rowdata['admin_type']!=$this->input->post('admin_type'))
			{
			  /* Intimation mail to subadmin */
			  $this->load->library('email');
			  $adm_groups = $this->config->item('admin_groups');
			  $res_data = $this->db->get_where('tbl_admin',array('admin_id'=>1))->row();

			  $mail_to      = $this->input->post('admin_email');
			  $mail_subject = "Subadmin registration at ".$this->config->item('site_name'); 
			  $from_email   = $res_data->admin_email;
			  $from_name    =  $this->config->item('site_name');
			  
			  
			

			  $verify_url= "<a href=".base_url()."sitepanel/>Click here </a>";
			  
			  $body = " Dear friend,<br />
			  Your login details are changed as follows:<br />
			  User name :  {username}<br />        
			  Password:  {password}<br /> 
			  Your credentials will work as {admin_rights}<br />
			  Click here to login {link}<br />  <br />						   
			  Thanks and Regards,<br />						   
			  {site_name} Team  ";

			  
			  
			  $body			=	str_replace('{username}',$username,$body);
			  $body			=	str_replace('{password}',$password,$body);
			  $body			=	str_replace('{admin_rights}',$admin_rights,$body);
			  $body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			  $body			=	str_replace('{url}',base_url(),$body);
			  $body           =	str_replace('{link}',$verify_url,$body);
					  
			  $this->email->from($from_email, $from_name);
			  $this->email->to($mail_to);			
			  $this->email->subject($mail_subject);				
			  $this->email->message($body);
			  $this->email->set_mailtype('html');
			  $this->email->send();
			  
			  /* End Intimation mail to subadmin */
			}
						
			$this->session->set_userdata(array('msg_type'=>'success'));				
			$this->session->set_flashdata('success',lang('successupdate'));								
			$redirect_path= 'subadmin';
							
			redirect('sitepanel/'.$redirect_path.'/'.query_string(), ''); 	
						
		}						
			
		$data['result']=$rowdata;		
		$this->load->view('subadmin/view_subadmin_edit',$data);				
		
	}
	
}
// End of controller