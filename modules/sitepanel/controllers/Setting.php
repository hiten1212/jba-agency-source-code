<?php



class Setting extends Admin_Controller



{



	public function __construct()



	{



		parent::__construct();



		$this->load->helper('ckeditor');



		$this->load->model(array('sitepanel/setting_model'));



		$this->config->set_item('menu_highlight','other management');	



	}







	public  function index($page = null)



	{



		$data['heading_title'] = 'Admin Setting';



		$data['admin_info'] = $this->setting_model->get_admin_info($this->session->userdata('admin_id'));



		$this->load->view('dashboard/setting_edit_view',$data);



	}



	



	public function edit()
	{
		//$this->db->query('ALTER TABLE `tbl_admin`  ADD `contact_details` TEXT NULL DEFAULT NULL  AFTER `address`');

		$this->form_validation->set_rules('admin_email', 'Email ID',  'required|valid_emails'); 			
		$this->form_validation->set_rules('enquiry_email', 'Enquiry Email ID',  'valid_email');
		$this->form_validation->set_rules('phone', 'Phone',  'required');
		//$this->form_validation->set_rules('contact_phone', 'Mobile No.',  'required');
		//$this->form_validation->set_rules('youtuble_url', 'Video',  'required');
		$this->form_validation->set_rules('address', 'Address',  'required');
		$this->form_validation->set_rules('address', 'Address',  'required');
			
		$this->form_validation->set_rules('city', 'City',  'trim');
		$this->form_validation->set_rules('state', 'State',  'trim');
		$this->form_validation->set_rules('zipcode', 'Zipcode',  'trim');
		$this->form_validation->set_rules('country', 'Country',  'trim');
		$this->form_validation->set_rules('footer_time', 'Need Help',  'trim'); 
		$this->form_validation->set_rules('map1', 'Map',  'trim');
		$this->form_validation->set_rules('map2', 'Map',  'trim');
		$this->form_validation->set_rules('map3', 'Map',  'trim');

		$this->form_validation->set_rules('facebook_link', 'Facebook Link',  'trim');
		$this->form_validation->set_rules('twitter_link', 'Twitter Link',  'trim');
		$this->form_validation->set_rules('linkedin_link', 'LinkedIn Link',  'trim');
		$this->form_validation->set_rules('google_link', 'Google+ Link',  'trim');
		$this->form_validation->set_rules('youtube_link', 'Youtube Link',  'trim');		
		$this->form_validation->set_rules('pinterest_link', 'Pinterest Link',  'trim');
		$this->form_validation->set_rules('instagram_link', 'Instagram Link',  'trim');
		$this->form_validation->set_rules('soundcloud_link', 'Soundcloud Link',  'trim');
		$this->form_validation->set_rules('rss_link', 'RSS Link',  'trim');

		$this->form_validation->set_rules('brochure','Brochure',"file_allowed_type[document]");

		$this->form_validation->set_rules('google_map', 'Google Map',  'trim');
                
                $this->form_validation->set_rules('website_url', 'Website url',  'trim');
                $this->form_validation->set_rules('footer1_header', 'Footer1 Header',  'trim');
                $this->form_validation->set_rules('footer2_header', 'Footer2 Header',  'trim');
                $this->form_validation->set_rules('footer3_header', 'Footer3 Header',  'trim');
                $this->form_validation->set_rules('copyright_text', 'Copyright Text',  'trim');
                $this->form_validation->set_rules('company_profile', 'Company Profile',  "file_allowed_type[document]");

                 $this->form_validation->set_rules('logo_background', 'Logo Background',  'trim');
                $this->form_validation->set_rules('start_project_text', 'Start Project Text',  'trim');
                $this->form_validation->set_rules('portfolio_menu_text', 'Portfolio Menu Text',  'trim');
                $this->form_validation->set_rules('clients_menu_text', 'clients Menu Text',  'trim');

		if ($this->form_validation->run() == TRUE)
		{
			$this->setting_model->update_info( $this->session->userdata('admin_id'));
			redirect('sitepanel/setting/','');
		}


		$data['heading_title'] = 'Admin Setting';
		$data['admin_info'] = $this->setting_model->get_admin_info($this->session->userdata('admin_id'));

		$this->load->view('dashboard/setting_edit_view',$data);
	}



	



	public function change()



	{



		$this->form_validation->set_rules('old_pass', 'Old Password', 'required');



		$this->form_validation->set_rules('new_pass', 'New Password', 'required|valid_password');



		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_pass]');



		



		if ($this->form_validation->run() === TRUE)



		{



		



			$this->setting_model->change_password( $this->input->post('old_pass'),$this->session->userdata('admin_id'));



			redirect('sitepanel/setting/change','');



		}



		



		$data['heading_title'] = 'Change Admin Password';



		//$data['admin_info'] = $this->setting_model->get_admin_info($this->session->userdata('admin_id'));



		$this->load->view('dashboard/setting_change_view',$data);



	}


        public function editbannersettings()
	{
		$this->form_validation->set_rules('banner_slogan', 'Banner Slogan',  'required');
                
		if ($this->form_validation->run() == TRUE)
		{
			$this->setting_model->update_banner_info( $this->session->userdata('admin_id'));
			redirect('sitepanel/staticpages/','');
		}


		$data['heading_title'] = 'Admin Setting';
                
		$this->load->view('staticpage/staticpage_list_index_view',$data);
	}

	
	 public function activatecompanyprofile()
	{
            $this->setting_model->updatecompanyprofilestatus( $this->session->userdata('admin_id'), 0);
            redirect('sitepanel/setting/','');
            
        }
        
        public function deactivatecompanyprofile()
	{
           $this->setting_model->updatecompanyprofilestatus( $this->session->userdata('admin_id'), 1);
            redirect('sitepanel/setting/',''); 
        }
        
        public function deletecompanyprofile()
	{
            $id = $this->session->userdata('admin_id');
            $cond = "admin_id =$id ";
            
            $num_row =  $this->setting_model->findCount('tbl_admin',$cond);
	    $rowdata = $this->setting_model->get_admin_info($id);
            
            $unlink_image4 = array('source_dir'=>"companyprofile",'source_file'=>$rowdata->company_profile);
            
            removeImage($unlink_image4);
            
            $this->setting_model->updatecompanyprofile( $this->session->userdata('admin_id'), 0);
            redirect('sitepanel/setting/',''); 
        }



}



// End of controller