<?php
class Products extends Admin_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('products/product_model','brand/brand_model'));
		$this->load->helper('category/category');
		$this->config->set_item('menu_highlight','product management');
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}

	public  function index($page = NULL){

		$condtion = array();
		$pagesize = (int) $this->input->get_post('pagesize');
		$config['limit'] = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset = ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url = current_url_query_string(array('filter'=>'result'),array('per_page'));
		$category_id = (int) $this->input->get_post('category_id');		

		$status = $this->input->get_post('status',TRUE);

		$cat_name = '';
		if($category_id > 0 ){
			$condtion['category_id'] = $category_id;
			$cat_name       = 'in ';
			$cat_name .= get_db_field_value('wl_categories','category_name'," AND category_id='$category_id'");
		}

		if($status!=''){
			$condtion['status'] = $status;
		}

		$res_array               =  $this->product_model->get_products($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Product  Lists';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);		

		if( $this->input->post('status_action')!=''){
			if( $this->input->post('status_action')=='Delete'){
				$prod_id=$this->input->post('arr_ids');

				foreach($prod_id as $v){
					$where = array('entity_type'=>'products/detail','entity_id'=>$v);
					$this->product_model->safe_delete('wl_meta_tags',$where,TRUE);
				}
			}
			$this->update_status('wl_products','products_id');
		}
		/* Product set as a */
		if( $this->input->post('set_as')!='' ){
		    $set_as    = $this->input->post('set_as',TRUE);
			$this->set_as('wl_products','products_id',array($set_as=>'1'));
		}
		if( $this->input->post('unset_as')!='' ){
		    $unset_as   = $this->input->post('unset_as',TRUE);
			$this->set_as('wl_products','products_id',array($unset_as=>'0'));
		}
		/* End product set as a */
		$data['category_result_found'] = "Total ".$config['total_rows']." result(s) found ".strtolower($cat_name)." ";
		$this->load->view('catalog/view_product_list',$data);
	}
	
	
	public function review()
	{
		$condtion               = array();
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$category_id            =  (int) $this->uri->segment(4,0);
		$status			        =   $this->input->get_post('status',TRUE);		
		
		$condtion['rev_type']	=	'P';		
		if($this->uri->segment(4)>0)
		{
			$condtion['prod_id'] =	$this->uri->segment(4);
		}		
			
		$res_array               =  $this->product_model->get_review($config['limit'],$offset,$condtion);
		//echo_sql();
		$config['total_rows']    =  get_found_rows();
		$data['heading_title']   =  'Product Review Lists';
		$data['res']             =  $res_array;
		$data['page_links']      =   admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		
		if( $this->input->post('status_action')!='')
		{			
			$this->update_status('wl_review','review_id');
		}
		$data['category_result_found'] = "Total ".$config['total_rows']." result(s) found ";
		$this->load->view('catalog/view_review_list',$data);
	}

	public function add(){

		$data['heading_title'] = 'Add Product ';
		$categoryposted=$this->input->post('catid');
		$data['categoryposted']=$categoryposted;
		$categoryposted=$this->input->post('catid');
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));
		$data['ckeditor2']  =  set_ck_config(array('textarea_id'=>'description1'));
		$data['ckeditor3']  =  set_ck_config(array('textarea_id'=>'description2'));

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$this->form_validation->set_rules('category_id','Category Name',"trim|required");
		$this->form_validation->set_rules('product_name','Product  Name',"trim|required|max_length[255]|unique[wl_products.product_name='".$this->db->escape_str($this->input->post('product_name'))."' AND status!='3']");
		$this->form_validation->set_rules('product_code','Product Code',"trim|required|max_length[65]|unique[wl_products.product_code='".$this->db->escape_str($this->input->post('product_code'))."' AND status!='2']");
		
		//$this->form_validation->set_rules('product_price','Price',"trim|is_valid_amount|callback_check_price_zero");
		//$this->form_validation->set_rules('product_discounted_price','Discount Price',"trim|is_valid_amount|callback_check_price");
		$this->form_validation->set_rules('youtube_code','Youtube Code',"trim|max_length[400]");
		$this->form_validation->set_rules('youtube_code2','Youtube Code 2',"trim|max_length[400]");

		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");	
				
		$this->form_validation->set_rules('products_description','Description',"required|max_length[25000]");	
		$this->form_validation->set_rules('core_description','Core Module',"max_length[25000]");
		$this->form_validation->set_rules('related_description','Related Links',"max_length[25000]");	
		
		$this->form_validation->set_rules('product_images1','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images2','Image2',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images3','Image3',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images4','Image4',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images5','Image5',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");	
		$this->form_validation->set_rules('product_alt','Alt',"trim|max_length[100]");

		if($this->form_validation->run()===TRUE){			

			$category_id=$this->input->post('category_id');
			
			$redirect_url = "products/detail";	
			$product_alt = $this->input->post('product_alt');
			if($product_alt ==''){
				$product_alt = $this->input->post('product_name');
			}
		  	$category_links = get_parent_categories($category_id,"AND status!='2'","category_id,parent_id");
			$category_links = array_keys($category_links);
			$category_links = implode(",",$category_links);	
			
			$discounted_price = $this->input->post('product_discounted_price',TRUE);
			$discounted_price = ($discounted_price=='') ? "0.0000" : $discounted_price;		
				
			
			$posted_data = array(
			'category_id'=>$category_id,			
			'category_links'=>$category_links,
			'product_name'=>$this->input->post('product_name',TRUE),
			'product_alt'=>$this->input->post('product_alt',TRUE),
			'youtube_code'=>$this->input->post('youtube_code',TRUE),
			'youtube_code2'=>$this->input->post('youtube_code2',TRUE),
			'friendly_url'=>$this->cbk_friendly_url, //url_title($this->input->post('product_name')),
			'product_code'=>$this->input->post('product_code',TRUE),
			//'product_price'=>$this->input->post('product_price',TRUE),
			//'product_discounted_price'=>$discounted_price,			
			'products_description'=>$this->input->post('products_description'),
			'core_description'=>$this->input->post('core_description'),
			'related_description'=>$this->input->post('related_description'),
			'product_added_date'=>$this->config->item('config.date.time')
			);			
			
			$posted_data = $this->security->xss_clean($posted_data);
			$productId = $this->product_model->safe_insert('wl_products',$posted_data,FALSE);
			$this->add_product_media($productId);
			if( $productId > 0 ){
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$productId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('product_name'),80),
							  'meta_description'=>get_text($this->input->post('products_description')),
							  'meta_keyword'=>get_keywords($this->input->post('products_description'))
							  );
			  create_meta($meta_array);
			}			

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/products?category_id='.$this->input->post('category_id'), '');
		}		

		$this->load->view('catalog/view_product_add',$data);
	}





	public function edit($productId){
		
		$data['heading_title'] = 'Edit Product ';
		$productId = (int) $this->uri->segment(4);
		$option = array('productid'=>$productId);
		$res =  $this->product_model->get_products(1,0, $option);
		$data['ckeditor1']  =  set_ck_config(array('textarea_id'=>'description'));
		$data['ckeditor2']  =  set_ck_config(array('textarea_id'=>'description1'));
		$data['ckeditor3']  =  set_ck_config(array('textarea_id'=>'description2'));

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');

		$this->form_validation->set_rules('category_id','Category Name',"trim|required");
		$this->form_validation->set_rules('product_name','Product Name',"trim|required|max_length[255]|unique[wl_products.product_name='".$this->db->escape_str($this->input->post('product_name'))."' AND status!='3' AND products_id != '".$productId."']");	
		$this->form_validation->set_rules('product_code','Product Code',"trim|required|max_length[65]|unique[wl_products.product_code='".$this->db->escape_str($this->input->post('product_code'))."' AND status!='3' AND products_id != '".$productId."']");
		$this->form_validation->set_rules('youtube_code','Youtube Code',"trim|max_length[400]");
		$this->form_validation->set_rules('youtube_code2','Youtube Code 2',"trim|max_length[400]");
		//$this->form_validation->set_rules('product_price','Price',"trim|is_valid_amount|callback_check_price_zero");
		//$this->form_validation->set_rules('product_discounted_price','Discount Price',"trim|is_valid_amount|callback_check_price");

		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");					
		
		$this->form_validation->set_rules('products_description','Description',"required|max_length[25000]");	
		$this->form_validation->set_rules('core_description','Core Module',"max_length[25000]");
		$this->form_validation->set_rules('related_description','Related Links',"max_length[25000]");			
		
		$this->form_validation->set_rules('product_images1','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images2','Image2',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images3','Image3',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images4','Image4',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		$this->form_validation->set_rules('product_images5','Image5',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");			
		$this->form_validation->set_rules('product_alt','Alt',"trim|max_length[100]");

		if( is_array( $res ) && !empty( $res )){			

			if($this->form_validation->run()==TRUE){				

				$category_id=$this->input->post('category_id');
				
				$category_links = get_parent_categories($category_id,"AND status!='2'","category_id,parent_id");
				$category_links = array_keys($category_links);
				$category_links = implode(",",$category_links);								
				
				$discounted_price = $this->input->post('product_discounted_price',TRUE);
				$discounted_price = ($discounted_price=='') ? "0.0000" : $discounted_price;
				
				$posted_data = array(
				'category_id'=>$category_id,				
				'category_links'=>$category_links,	
				'product_name'=>$this->input->post('product_name',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,
				'product_alt'=>$this->input->post('product_alt',TRUE),
				'product_code'=>$this->input->post('product_code',TRUE),
				//'product_price'=>$this->input->post('product_price',TRUE),
				//'product_discounted_price'=>$discounted_price,
				'youtube_code'=>$this->input->post('youtube_code',TRUE),
				'youtube_code2'=>$this->input->post('youtube_code2',TRUE),				
				'products_description'=>$this->input->post('products_description'),
				'core_description'=>$this->input->post('core_description'),
			    'related_description'=>$this->input->post('related_description'),
				'product_updated_date'=>$this->config->item('config.date.time')
				);
						
				$posted_data = $this->security->xss_clean($posted_data);
				$where = "products_id = '".$res['products_id']."'";
				$this->product_model->safe_update('wl_products',$posted_data,$where,FALSE);
				$this->edit_product_media($res['products_id']);
				update_meta_page_url('products/detail',$res['products_id'],$this->cbk_friendly_url);
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/products/'. query_string(), '');
			}
			
			
			$data['res']=$res;
			
			$media_option = array('productid'=>$res['products_id']);
			$res_photo_media = $this->product_model->get_product_media(5,0, $media_option);
			$data['res_photo_media']=$res_photo_media;			
							
			$this->load->view('catalog/view_product_edit',$data);
		}else{
			redirect('sitepanel/products', '');
		}
	}

	public function add_product_media($productId){	
		if( !empty($_FILES) && ( $productId > 0 ) ){
			$defalut_image = 'Y';
			foreach($_FILES as $key=>$val){
				$imgfld=$key;
				if(array_key_exists($imgfld,$_FILES)){
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"products");
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ){
						$add_data = array(
						'products_id'=>$productId,
						'media_type'=>'photo',
						'is_default'=>$defalut_image,
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'media_date_added' => $this->config->item('config.date.time')
						);
						$this->product_model->safe_insert('wl_products_media', $add_data ,FALSE );
					}
					$defalut_image = 'N';
				}
			}
		}

  }

	public function edit_product_media($productId){
		//Current Media Files resultset
		$media_option = array('productid'=>$productId);
		$res_photo_media = $this->product_model->get_product_media(5,0, $media_option);
		
		trace($res_photo_media);
		//exit;
		$res_photo_media = !is_array($res_photo_media ) ? array() : $res_photo_media ;
		$delete_media_files = $this->input->post('product_img_delete'); //checkbox items given for image deletion
		$arr_delete_items = array(); //holding our deleted ids for later use
		/* Tracking delete media ids coming from checkboxes */
		if(is_array($delete_media_files) && !empty($delete_media_files)){

			foreach($res_photo_media as $key=>$val){
				$media_id = $val['id'];
				if(array_key_exists($media_id,$delete_media_files))	{
					 $media_file = $res_photo_media[$key]['media'];
					 $unlink_image = array('source_dir'=>"products",'source_file'=>$media_file);
					 removeImage($unlink_image);
					 array_push($arr_delete_items,$media_id);
				}
			}
		}

		/* Tracking Ends */

		/* Iterating Form Files */

		if( !empty($_FILES) && ( $productId > 0 ) ){
			$sx = 0;
			foreach($_FILES as $key=>$val){
				$imgfld=$key;
				if(array_key_exists($imgfld,$_FILES)){
					$this->load->library('upload');
					$data_upload_sugg = $this->upload->my_upload($imgfld,"products");
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ){
						/*  uploading successful  */
						$add_data = array(
						'products_id'=>$productId,
						'media_type'=>'photo',
						'media'=>$data_upload_sugg['upload_data']['file_name'],
						'media_date_added' => $this->config->item('config.date.time')
						);

						/* If there already exists record in the database update then else insert new entry
						   $res_photo_media  holding existing resultset from databse in the form given below:
						   $res_photo_media = array( 0 => array(row1) )
						*/


						if(array_key_exists($sx,$res_photo_media)){
						       $media_id  = $res_photo_media[$sx]['id'];
							   $media_file = $res_photo_media[$sx]['media'];
						       $where = "id = '".$media_id."'";
				               $this->product_model->safe_update('wl_products_media',$add_data,$where,FALSE);
							   $unlink_image = array('source_dir'=>"products",'source_file'=>$media_file);
							   removeImage($unlink_image);
							   /* New File has been browsed and delete checkbox also checked for this file */
							   /* This  media id cannot be removed as it been browsed and updated */
							   if(in_array($media_id,$arr_delete_items)){
									$media_del_index = array_search($media_id,$arr_delete_items);
									unset($arr_delete_items[$media_del_index]);
							   }

						}else{
							$this->product_model->safe_insert('wl_products_media', $add_data ,FALSE );
						}
					}
				}
				$sx++;
			}
		}

		if(!empty($arr_delete_items)){
			$del_ids = implode(',',$arr_delete_items);
			$where = " id IN(".$del_ids.") ";
			$this->product_model->delete_in('wl_products_media',$where,FALSE);
		}
  }


	public function delete_pdf(){
		$pId=(int)$this->uri->segment(4,0);		

		if($pId > 0){

			

			$pdf_name='experience';

			$file=get_db_field_value('wl_products', $pdf_name, array("products_id"=>$pId));

			if($file !="" && @file_exists(UPLOAD_DIR."/docs/".$file)){

								

				@unlink(UPLOAD_DIR."/docs/".$file);

				

				$pproduct_data = array(

					'experience'=>""

				);



				$where = "products_id = '".$pId."' ";

				$this->product_model->safe_update('wl_products',$pproduct_data,$where,FALSE);				

				$this->session->set_userdata(array('msg_type'=>'success'));

				$this->session->set_flashdata('success','File Deleted!');

				

				redirect('sitepanel/products/edit/'.$pId, '');	

					

			}else{

				redirect('products', '');

			}

		}else{

			redirect('products', '');

		}

	}

	



	public function check_price()

	{

		$disc_price = floatval($this->input->post('product_discounted_price'));

		$price      = floatval($this->input->post('product_price'));

		if($disc_price>=$price && $disc_price>0)

		{

			$this->form_validation->set_message('check_price', 'Discount price must be less than actual price.');

			return FALSE;

		}else

		{

			return TRUE;

		}

	}

	

	public function check_price_zero()

	{

		$price      = floatval($this->input->post('product_price'));

		if(!empty($price) && $price <= 0)

		{

			$this->form_validation->set_message('check_price_zero', 'Price must be a postive numeric number.');

			return FALSE;

		}else

		{

			return TRUE;

		}

	}

	

	public function checkurl(){

		$product_id=(int)$this->input->post('products_id');

		

		if($product_id!=''){

			$cont='and entity_id !='.$product_id;

		}else{

			$cont='';

		}

		

		$posted_friendly_url = $this->input->post('friendly_url');

		

		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();

		

		if($urlcount>0){

			$this->form_validation->set_message('checkurl', 'URL already exists.');

			return FALSE;

		}else{

			return TRUE;

		}

		

	}
	
	public function related()
	{
		$productId =  (int) $this->uri->segment(4);
		$option = array('productid'=>$productId);
		$res =  $this->product_model->get_products(1,0, $option);


		if( is_array($res) )
		{
			$data['heading_title'] = "Add Related Products";

			$fetch_related_products  = $this->product_model->related_products_added($res['products_id']);

			if(empty($fetch_related_products))
			{
				$fetch_not_ids = array($res['products_id']);

			}else
			{
				$fetch_not_ids=array_values($fetch_related_products);
				array_push($fetch_not_ids,$res['products_id']);

			}

			$res_array  = $this->product_model->get_related_products($fetch_not_ids);
			$data['res'] = $res_array;

				/* Add related products */

				if($this->input->post('add_related')=='Add related product')
				{

					$this->add_related_products($res['products_id']);
					$this->session->set_flashdata('message',"Related Product has been added successfully." );
					redirect('sitepanel/products/related/'.$productId, '');

				}

			/* End of  related products */

			$this->load->view('catalog/view_add_related_products',$data);
		}

	}


	public function add_related_products($product_id)
	{
		$arr_ids = $this->input->post('arr_ids');

		if( is_array($arr_ids))
		{
			foreach($arr_ids as $val )
			{
				$rec_exits = $this->product_model->is_record_exits('wl_products_related', array('condition'=>"related_id =".$val." AND product_id =".$product_id." "));
				if( !$rec_exits )
				{
					$posted_data = array(
					'product_id'=>$product_id,
					'related_id'=>$val,
					'related_date_added'=>$this->config->item('config.date.time')
					);
					$this->product_model->safe_insert('wl_products_related',$posted_data,FALSE);
				}
			}
		}
	}

	public function remove_related_products($productId)
	{
		$arr_ids = $this->input->post('arr_ids');
		if( is_array($arr_ids) )
		{
			if($this->input->post('remove_related')=='Remove product')
			{
				foreach($arr_ids as $val )
				{
					$data = array('id'=>$val );
					$this->product_model->safe_delete('wl_products_related',$data,FALSE);
				}

			}
		}
	}

	public function view_related()
	{
		$productId =  (int) $this->uri->segment(4);
		$option = array('productid'=>$productId);
		$res =  $this->product_model->get_products(1,0, $option);

		if( is_array($res) )
		{
			$data['heading_title'] = "View Related Products";
			$res_array  = $this->product_model->related_products($res['products_id']);
			$data['res'] = $res_array;

			/* Remove related products */

				if($this->input->post('remove_related')=='Remove product')
				{
					$this->remove_related_products($res['products_id']);
					$this->session->set_flashdata('message',"Related Product has been removed successfully." );
				    redirect('sitepanel/products/view_related/'.$productId, '');

				}

			/* End of  remove related products */

			$this->load->view('catalog/view_related_products',$data);


		}

	}


}

// End of controller