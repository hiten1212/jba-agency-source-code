<?php
class Events extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('events/events_model'));
	   $this->config->set_item('menu_highlight','events');
		 $this->form_validation->set_error_delimiters("<p class='required'>","</p>");
		
	}

	public function index()
	{
		 
			$pagesize                =  (int) $this->input->get_post('pagesize');			
			$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');			
			$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
			$base_url                =  current_url_query_string(array('filter'=>'result'),array('per_page'));
			
			$param = array();	
			$res_array                 = $this->events_model->get_events($config['limit'],$offset,$param);		
			$config['total_rows']	  = get_found_rows();	
		    $data['page_links']      =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);		
			$data['heading_title'] = 'News & Events';
			$data['res'] = $res_array; 	
			
		    if( $this->input->post('status_action')!='')
				{			
				   $this->update_status('wl_events','events_id');			
				}
				
		/* News set as a */				
		if( $this->input->post('set_as')!='' )
		{	
		    $set_as    = $this->input->post('set_as',TRUE);				
			if($set_as=='Set Breaking News'){$set_as='latest_events';}					
			$this->set_as('wl_events','events_id',array($set_as=>'1'));			
		}
		
		if( $this->input->post('unset_as')!='' )
		{	
		    $unset_as   = $this->input->post('unset_as',TRUE);				
			if($unset_as=='Unset Breaking News'){$unset_as='latest_events';}					
			$this->set_as('wl_events','events_id',array($unset_as=>'0'));			
		}
		/* End events set as a */
				
						
			$this->load->view('sitepanel/events/view_events_list',$data);
			
	}
	
	
	public function post_events()
	{	
		
		$data['heading_title'] = "Add";
		
		$pdf_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_dim           =  $this->config->item('allow.imgage.dimension');	
		
		$data['ckeditor']    =  set_ck_config(array('textarea_id'=>'events_description'));		
		$seo_url_length = $this->config->item('seo_url_length');
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		
		$this->form_validation->set_rules('events_title','Title',"required|max_length[90]|unique[wl_events.events_title='".$this->db->escape_str($this->input->post('events_title'))."' AND status!='2' ]");	
		$this->form_validation->set_rules('friendly_url','Page URL',"required|max_length[$seo_url_length]|callback_checkurl");	
		
		$this->form_validation->set_rules('start_date','start date','trim|required');
		$this->form_validation->set_rules('posted_by','posted by','trim');
				
		$this->form_validation->set_rules('events_image','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
				
		$this->form_validation->set_rules('events_description','Description','required|max_length[15000]');
		
				  		
		if($this->form_validation->run()==TRUE)
		{
			
			$uploaded_file = "";							   				
			if( !empty($_FILES) && $_FILES['events_image']['name']!='' )
			{			  
				$this->load->library('upload');					
				$uploaded_data =  $this->upload->my_upload('events_image','news');				
				if( is_array($uploaded_data)  && !empty($uploaded_data) )
				{ 								
				   $uploaded_file = $uploaded_data['upload_data']['file_name'];
				
				}
			}
						   				
						
			$posted_data=array(				
				'events_title'=> $this->input->post('events_title',TRUE),
				'events_image'=>$uploaded_file,				
				'start_date' =>$this->input->post('start_date',TRUE),				
				'events_description' =>$this->input->post('events_description',TRUE),
				'posted_by' =>$this->input->post('posted_by',TRUE),
				'friendly_url'=>$this->cbk_friendly_url,				
				'status'=>'1',						
				'recv_date'  =>$this->config->item('config.date.time')
			);	
			$posted_data = $this->security->xss_clean($posted_data);		
			$insertId=$this->events_model->safe_insert('wl_events',$posted_data,FALSE); 
			
			
			if( $insertId > 0 )
			{
			  $redirect_url='events/details';
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$insertId,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('events_title'),80),
							  'meta_description'=>get_text($this->input->post('events_description')),
							  'meta_keyword'=>get_keywords($this->input->post('events_description'))
							  );

			  create_meta($meta_array);
			  
			
			}
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success',lang('success'));
			redirect('sitepanel/events');
			
		}
						
	   $this->load->view('sitepanel/events/view_post_events',$data);
	   
	}
	
	
	public function edit_events()
	{	
	     
	    $this->load->model(array('events/events_model'));
	    $data['ckeditor']    =  set_ck_config(array('textarea_id'=>'events_description'));		
	    $id                      = (int) $this->uri->segment(4);
	    $param                   = array('where'=>"wl_events.events_id ='$id' ");			
		$res                     = $this->events_model->get_events(1,0,$param);
		
		$pdf_allow_size			 =  $this->config->item('allow.file.size');
		$img_allow_size          =  $this->config->item('allow.file.size');
		$img_allow_dim           =  $this->config->item('allow.imgage.dimension');		
				
       if( is_array($res) && !empty($res))
	   {	
		
			$data['heading_title'] = "Edit";		
			
			$this->form_validation->set_rules('events_title','Title',"trim|required|max_length[90]|unique[wl_events.events_title='".$this->db->escape_str($this->input->post('events_title'))."' AND status!='2' AND events_id!='".$id."' ]");	
			
			$seo_url_length = $this->config->item('seo_url_length');
			$this->cbk_friendly_url = seo_url_title($this->input->post('friendly_url',TRUE));

			$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|unique[wl_meta_tags.page_url='".$this->cbk_friendly_url."' AND entity_id!='".$id."'] ");	
			
			$this->form_validation->set_rules('start_date','start date','trim|required');
			$this->form_validation->set_rules('posted_by','posted by','trim');
						
			$this->form_validation->set_rules('events_image','Image1',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
					
			$this->form_validation->set_rules('events_description','Description','trim|required|max_length[15000]');		
			if($this->form_validation->run()==TRUE)
			{
				
				$uploaded_file = $res['events_image'];
				$unlink_image = array('source_dir'=>"events",'source_file'=>$res['events_image']);
				if($this->input->post('events_image_delete')==='Y')			    
				{					
				  removeImage($unlink_image);						
				  $uploaded_file = NULL;							  
			    }												
				if( !empty($_FILES) && $_FILES['events_image']['name']!='' )
				{			  
					$this->load->library('upload');					
					$uploaded_data =  $this->upload->my_upload('events_image','news');
					
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
					   $uploaded_file = $uploaded_data['upload_data']['file_name'];

					   removeImage($unlink_image);	
					
					}
				}
				
											
				$posted_data=array(				
				'events_title'=> $this->input->post('events_title',TRUE),				
				'events_image'=>$uploaded_file,					
				'start_date' =>$this->input->post('start_date',TRUE),
				'posted_by' =>$this->input->post('posted_by',TRUE),				
				'friendly_url'=>$this->cbk_friendly_url,
				'events_description' =>$this->input->post('events_description',TRUE),				
				'status'=>'1'
				);	
				$posted_data = $this->security->xss_clean($posted_data);
				$where = "events_id = '".$res['events_id']."'"; 						
				$this->events_model->safe_update('wl_events',$posted_data,$where,FALSE);				
				
				update_meta_page_url('events/details',$res['events_id'],$this->cbk_friendly_url);
				
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));
				redirect('sitepanel/events/'.query_string(),'');
				
				
			}
			
	   }else
	   {
		  redirect('sitepanel/events'); 
		   
	   }
		
	   $data['res'] = $res;				
	   $this->load->view('sitepanel/events/view_edit_events',$data);
	   
	}
			
	 public function download_pdf()
	  {
		$id = (int) $this->uri->segment(4);

		$param                  =  array('where'=>"events_id ='$id' ");
			
		$res_topic                    =  $this->events_model->get_events(1,0,$param);	
			

		if(is_array($res_topic) && !empty($res_topic))
		{
		  if($res_topic['events_pdf']!='' && file_exists(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']))
		  {
			  $this->load->helper('download');
			  $data = file_get_contents(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']);
			  $name = $res_topic['events_pdf'];
			  force_download($name, $data); 
		  }
		}
		
	  }

  public function check_event_time()
  {
	$start_date = $this->input->post('start_date');
	$end_date = $this->input->post('end_date');

	if("$start_date" != "" && "$end_date" != "")
	{
		$start_time = strtotime("$start_date");
		$end_time = strtotime("$end_date");
		if($start_time > $end_time)
		{
		  $this->form_validation->set_message('check_event_time',"Start Date must be less than or equal to End date");
		 return FALSE;
		}
		
	}

	 return TRUE;
  }	
  
  
  
  public function checkurl(){

		$events_id=(int)$this->input->post('events_id');

		

		if($events_id!=''){

			$cont='and entity_id !='.$events_id;

		}else{

			$cont='';

		}

		

		$posted_friendly_url = $this->input->post('friendly_url');

		$cbk_friendly_url = seo_url_title($posted_friendly_url);

		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$cbk_friendly_url."'".$cont."")->num_rows();

		

		if($urlcount>0)

		{

			$this->form_validation->set_message('checkurl', 'URL already exists.');

			return FALSE;

		}else

		{

			return TRUE;

		}

		

	}
	
	public function add_events_media($productId) 
	{ 
		if( !empty($_FILES) && ( $productId > 0 ) ) 
		{ 
			$defalut_image = 'Y'; 
			foreach($_FILES as $key=>$val) 
			{ 
				$imgfld=$key; 
				if(array_key_exists($imgfld,$_FILES)) 
				{ 
					@$this->load->library('upload'); 
					$data_upload_sugg = $this->upload->my_upload($imgfld,"events"); 
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ) 
					{ 
						$add_data = array( 
						'events_id'=>$productId, 
						'media_type'=>'photo', 
						'is_default'=>$defalut_image, 
						'media'=>$data_upload_sugg['upload_data']['file_name'], 
						'media_date_added' => $this->config->item('config.date.time') 
						); 
						$this->events_model->safe_insert('wl_events_media', $add_data ,FALSE ); 
					} 
					$defalut_image = 'N'; 
				} 
			} 
		} 
     }
 
	public function edit_events_media($productId) 
	{ 
		//Current Media Files resultset 
		$media_option = array('projectid'=>$productId); 
		$res_photo_media = $this->events_model->get_events_media(4,0, $media_option); 
		$res_photo_media = !is_array($res_photo_media ) ? array() : $res_photo_media ;   
		$delete_media_files = $this->input->post('product_img_delete'); //checkbox items given for image deletion   
		$arr_delete_items = array(); //holding our deleted ids for later use 
		/* Tracking delete media ids coming from checkboxes */ 
		if(is_array($delete_media_files) && !empty($delete_media_files)) 
		{ 
			foreach($res_photo_media as $key=>$val) 
			{ 
				$media_id = $val['id']; 
				if(array_key_exists($media_id,$delete_media_files)) 
				{ 
					 $media_file = $res_photo_media[$key]['media']; 
					 $unlink_image = array('source_dir'=>"events",'source_file'=>$media_file); 
					 removeImage($unlink_image); 
					 array_push($arr_delete_items,$media_id); 
				} 
			} 
		} 
		/* Tracking Ends */  
		/* Iterating Form Files */ 
		if( !empty($_FILES) && ( $productId > 0 ) ) 
		{ 
			$sx = 0; 
			foreach($_FILES as $key=>$val) 
			{ 
				$imgfld=$key;   
				if(array_key_exists($imgfld,$_FILES)) 
				{  
					@$this->load->library('upload'); 
					$data_upload_sugg = $this->upload->my_upload($imgfld,"events"); 
					if( is_array($data_upload_sugg)  && !empty($data_upload_sugg) ) 
					{ 
						/*  uploading successful  */ 
						$add_data = array( 
						'events_id'=>$productId, 
						'media_type'=>'photo', 
						'media'=>$data_upload_sugg['upload_data']['file_name'],  
						'media_date_added' => $this->config->item('config.date.time') 
						); 
						/* If there already exists record in the database update then else insert new entry 
						   $res_photo_media  holding existing resultset from databse in the form given below: 
						   $res_photo_media = array( 0 => array(row1) ) 
						*/ 
						if(array_key_exists($sx,$res_photo_media)) 
						{ 
						       $media_id  = $res_photo_media[$sx]['id']; 
							   $media_file = $res_photo_media[$sx]['media']; 
						       $where = "id = '".$media_id."'"; 
				               $this->events_model->safe_update('wl_events_media',$add_data,$where,FALSE); 
							   $unlink_image = array('source_dir'=>"events",'source_file'=>$media_file); 
							   removeImage($unlink_image);   
							   /* New File has been browsed and delete checkbox also checked for this file */ 
							   /* This  media id cannot be removed as it been browsed and updated */ 
							   if(in_array($media_id,$arr_delete_items)) 
							   { 
									$media_del_index = array_search($media_id,$arr_delete_items); 
									unset($arr_delete_items[$media_del_index]); 
							   } 
						}else{ 
							$this->events_model->safe_insert('wl_events_media', $add_data ,FALSE ); 
						} 
					} 
				} 
				$sx++; 
			} 
		} 
		if(!empty($arr_delete_items)) 
		{ 
			$del_ids = implode(',',$arr_delete_items); 
			$where = " id IN(".$del_ids.") "; 
			$this->events_model->delete_in('wl_events_media',$where,FALSE); 
		} 
    }
	
}


/* End of file pages.php */

?>
