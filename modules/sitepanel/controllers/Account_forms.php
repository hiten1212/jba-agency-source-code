<?php
class Account_forms extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('account_forms_model'));
		$this->config->set_item('menu_highlight','other management');
	}
	public  function index()
	{
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		 = ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$res_array              =  $this->account_forms_model->get_order_form($config['limit'],$offset);
		$total_record           =  get_found_rows();
		$config['total_rows']   =  $total_record;
		$data['page_links']      = admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		$data['res']            =  $res_array;
		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_order','order_id');
		}

		$data['heading_title'] = 'Manage Order Form';
		$data['pagelist']      = $res_array;
		$this->load->view('account_forms/view_order_form_list',$data);
	}

	public function display()
	{
		$res = $this->account_forms_model->getHelpcenter_by_id($this->uri->segment(4));
		$data['heading_title'] = 'View FAQ Information';
		$data['page_title']    = 'View FAQ Information';
		$data['pageresult']=$res;
		$this->load->view('common/view_helpcenter_detail',$data);
	}
	
	public function application_form_list()
	{
		$pagesize               =  (int) $this->input->get_post('pagesize');
		$config['limit']		=  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$res_array              =  $this->account_forms_model->get_application_form_list($config['limit'],$offset);
		$total_record           =  get_found_rows();
		$config['total_rows']   =  $total_record;
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		$data['res']            =  $res_array;
		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_application_form','application_id');
		}

		$data['heading_title'] = 'Manage Application Form';
		$data['pagelist']      = $res_array;
		$this->load->view('account_forms/view_application_forms_list',$data);
	}
}


//controllet end