<?php
class Enquiry extends  Admin_Controller{

	public function __construct()
	{

		parent::__construct();
		$this->load->model(array('sitepanel/enquiry_model'));
		$this->config->set_item('menu_highlight','other management');
		$this->form_validation->set_error_delimiters("<div class='red'>","</div>");
	}

	public  function index()
	{

		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');

		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

	$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));

	$condition               =  "type ='1' ";
		$keyword                 = $this->db->escape_str( $keyword );
		
		if($keyword!=''){
			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR service_type like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";

	}		
		$res_array     = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);
		$config['total_rows']	= $this->enquiry_model->total_rec_found;
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);


		if( $this->input->post('status_action')!=''){
			$this->update_status('wl_enquiry','id');
		}


		if($this->input->post('Send')=='Send Email'){
			$this->change_status();
		}


		$data['heading_title']  = 'Manage Contact Us Enquiries';
		$data['inq_type']       = 'General';

		$data['res']            = $res_array;
		$this->load->view('enquiry/view_enquiry_list',$data);

	}


	public function change_status()
	{

		$data['heading_title']  = 	'Manage Enquiry Send Mail';
		$data['ckeditor'] 		=  	set_ck_config(array('textarea_id'=>'message'));

		$this->preview();
		$arr_ids 				= 	$this->input->post('arr_ids');	

		$str_ids 				=  	is_array($arr_ids) ? implode(',', $arr_ids) : $arr_ids;			



		$rowdata				=	$this->enquiry_model->getemail_by_id($str_ids);



		$data['enq_result']		=	$rowdata;



		$data['ids']			=	$str_ids;



		$this->load->view('enquiry/view_enquiry_send',$data);



	}


	public function preview()
	{
		if($this->input->post('action')=='preview')
		{

			$type=$this->input->post('type');

			$this->form_validation->set_rules('sendto','TO','trim|required|valid_emails');

			$this->form_validation->set_rules('mail_cc','Mail CC','trim|valid_email');
			$this->form_validation->set_rules('subject','Subject','trim|required');
			$this->form_validation->set_rules('message','Message','trim|required|exclude_text[<br />]');
			
			if($this->form_validation->run()=== TRUE)
			{	
				echo form_open("sitepanel/enquiry/send_mail?type=$type",array("name"=>"sendmailfrm","id"=>"sendmailfrm"));
				echo form_hidden("subject",$this->input->post("subject"));
				echo form_hidden("message",$this->input->post("message"));
				echo form_hidden("sendto",$this->input->post("sendto"));
				echo form_hidden("mail_cc",$this->input->post("mail_cc"));
				echo form_hidden("arr_ids",$this->input->post("arr_ids"));

				echo form_close();
			  	echo '<script type="text/javascript">document.sendmailfrm.submit();</script>';

			}
	   	}	
	}


public function send_mail()
{

	 $arr_ids=$this->input->post('arr_ids');
	 if($this->input->post() && $this->input->post('sendto')!="")
	 {

		if($this->input->post("submit")!="" )
		{

			$subject = $this->input->post('subject');
			$message = $this->input->post('message');

			$mail_to = explode(",",$this->input->post('sendto'));
			$mail_cc = $this->input->post('mail_cc');

			$this->load->library('email');

			$config['mailtype']="html";
			$this->email->initialize($config);

			$rwadmin=$this->enquiry_model->get_admin_email();
			$this->email->from($rwadmin['admin_email'], $this->config->item('site_name'));


			if( is_array($mail_to) && !empty($mail_to))
			{
				foreach($mail_to as $to)
				{
					if($to!='')
					{
						$this->email->to($to);
						if($mail_cc!='')
						{
							$this->email->cc($mail_cc); 	

						}
						$this->email->subject($subject);
						$this->email->message($message);	

						$this->email->send();
						//$this->email->print_debugger();

					}	

				}

				$this->enquiry_model->safe_update('wl_enquiry',array('reply_status' =>"Y"),"id IN ('".$arr_ids."') ");

			}



			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success','Email has been sent successfully.');
			if($this->input->get_post('type')=='f')

			{
				redirect('sitepanel/enquiry/feedback', '');
			}
			else
			{
				redirect('sitepanel/enquiry', '');
			}
		}

		$data["heading_title"]="Enquiry Preview";
		$this->load->view('enquiry/view_enquiry_preview',$data);
	}
	else
	{
		redirect("sitepanel/enquiry");
	}	
}

   public  function career()
	{
		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');

		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$condition               =  "type ='2' ";
		$keyword                 = $this->db->escape_str( $keyword );


		if($keyword!=''){
			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";

		}

		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);

		$config['total_rows']	= $this->enquiry_model->total_rec_found;
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_enquiry','id');
		}


		$data['heading_title']  = 'Manage carrer Enquiry';
		$data['inq_type']       = 'service';

		$data['res']            = $res_array;

		$this->load->view('enquiry/view_enquiry_list',$data);
	}


	public  function products()



	{



		$keyword                 =  trim($this->input->post('keyword',TRUE));



		$pagesize                =  (int) $this->input->get_post('pagesize');



		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');



		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;



		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));



		$condition               =  "type ='3' ";



		$keyword                 = $this->db->escape_str( $keyword );



		



		if($keyword!='')



		{



			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";



		}



		



		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);



		$config['total_rows']	= $this->enquiry_model->total_rec_found;



		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);







		if( $this->input->post('status_action')!='')



		{



			$this->update_status('wl_enquiry','id');



		}



		



		$data['heading_title']  = 'Manage Product Enquiry';



		$data['inq_type']       = 'service';



		$data['res']            = $res_array;



		$this->load->view('enquiry/view_enquiry_product_list',$data);


	}
	
	public  function services()
	{
		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');

		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$condition               =  "type ='2' ";
		$keyword                 = $this->db->escape_str( $keyword );


		if($keyword!=''){
			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";

		}

		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);

		$config['total_rows']	= $this->enquiry_model->total_rec_found;
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_enquiry','id');
		}


		$data['heading_title']  = 'Manage Project Enquiry';
		$data['inq_type']       = 'service';

		$data['res']            = $res_array;

		$this->load->view('enquiry/view_enquiry_list',$data);
	}
	
	public  function services_type()
	{
		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');

		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		$condition               =  "type ='4' ";
		$keyword                 = $this->db->escape_str( $keyword );


		if($keyword!=''){
			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";

		}

		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);

		$config['total_rows']	= $this->enquiry_model->total_rec_found;
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_enquiry','id');
		}


		$data['heading_title']  = 'Manage Service Type Enquiry';
		$data['inq_type']       = 'service';

		$data['res']            = $res_array;

		$this->load->view('enquiry/view_enquiry_list',$data);
	}
	
   public  function donate()
        {

		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');
		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));

		$condition               =  "type ='4' ";
		$keyword                 = $this->db->escape_str( $keyword );	


		if($keyword!='')
		{
			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";
		}
		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);

		$config['total_rows']	= $this->enquiry_model->total_rec_found;

		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		if( $this->input->post('status_action')!='')
		{
			$this->update_status('wl_enquiry','id');
		}

		$data['heading_title']  = 'Manage Donate Enquiry';
		$data['inq_type']       = 'service';

		$data['res']            = $res_array;
		$this->load->view('enquiry/view_enquiry_list',$data);

	}



	public function download_document1(){

		$id=(int)$this->uri->segment(4);
		if($id > 0){
				
			$pdf_name='document1';
			$file=get_db_field_value('wl_enquiry', $pdf_name, array("id"=>$id));

			if($file !="" && @file_exists(UPLOAD_DIR."/documentes/".$file)){
				$this->load->helper('download');
				$data = file_get_contents(UPLOAD_DIR."/documentes/".$file);

				$name = $file;
				force_download($name, $data);
			}else{
				redirect('enquiry', '');
			}

		}else{
			redirect('enquiry', '');
		}

	}
	
	public function download_document2(){

		$id=(int)$this->uri->segment(4);
		if($id > 0){
				
			$pdf_name='document2';
			$file=get_db_field_value('wl_enquiry', $pdf_name, array("id"=>$id));

			if($file !="" && @file_exists(UPLOAD_DIR."/documentes/".$file)){
				$this->load->helper('download');
				$data = file_get_contents(UPLOAD_DIR."/documentes/".$file);

				$name = $file;
				force_download($name, $data);
			}else{
				redirect('enquiry', '');
			}

		}else{
			redirect('enquiry', '');
		}

	}

	public function send_reply()
	{
		$rid =  (int) $this->uri->segment(4);



		$this->db->select("*,CONCAT_WS(' ',first_name,last_name) AS name",FALSE);



		$res_data =  $this->db->get_where('wl_enquiry', array('id' =>$rid))->row();



	    $this->load->library('email');







		if( is_object( $res_data ) )



		{



			$this->form_validation->set_rules('subject', 'Subject', 'required');



			$this->form_validation->set_rules('message', 'Message', 'required');







			if ($this->form_validation->run() == TRUE)



			{



				/* Reply  mail to user */







				$this->enquiry_model->safe_update('wl_enquiry',array('reply_status' =>"Y"),"id ='".$rid."'");







				$mail_to      = $res_data->email;



				$mail_subject = $this->input->post('subject');



				$from_email   = $this->admin_info->admin_email;



				$from_name    =  $this->config->item('site_name');



				$body = "Dear ".$res_data->name.",<br /><br />	";



				$body .= $this->input->post('message');



				$body .= "<br /> <br />Thanks and Regards,<br />".$this->config->item('site_name')." Team ";



				



				$this->email->from($from_email,$from_name);



				$this->email->to($mail_to);



				$this->email->subject($mail_subject);



				$this->email->message($body);



				$this->email->set_mailtype('html');



				$this->email->send();



				$this->session->set_userdata(array('msg_type'=>'success'));



				$this->session->set_flashdata('success',lang('admin_mail_msg'));







				redirect('sitepanel/enquiry/send_reply/'.$res_data->id, '');







				/* End reply mail to user */



			}



			



			$data['heading_title'] = "Send Reply";



			$this->load->view('enquiry/view_send_enq_reply',$data);



			



		}else



		{



			redirect('sitepanel/enquiry/','');



		}



		



	}



public  function careers(){

		$keyword                 =  trim($this->input->post('keyword',TRUE));
		$pagesize                =  (int) $this->input->get_post('pagesize');
		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');
		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;
		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));
		
		$type                 =  $this->input->get_post('type',TRUE);
		
		$condition               =  "type ='4' ";
		
		$data['heading_title']  = 'Manage Applied Jobs';
		
		
		$keyword                 = $this->db->escape_str( $keyword );

		if($keyword!='')
		{



			$condition.=" AND  ( email like '%$keyword%' OR  city like '%$keyword%' OR country like '%$keyword%'  OR CONCAT_WS(' ',first_name,last_name) LIKE '%".$keyword."%' ) ";



		}



		



		$res_array              = $this->enquiry_model->get_enquiry($offset,$config['limit'],$condition);



		$config['total_rows']	= $this->enquiry_model->total_rec_found;



		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);







		if( $this->input->post('status_action')!='')



		{



			$this->update_status('wl_enquiry','id');



		}



		



		



		$data['inq_type']       = 'careers';



		$data['res']            = $res_array;



		$this->load->view('enquiry/view_careers_enquiry_list',$data);







	}





	



	public function download_resume(){



		



		$id=(int)$this->uri->segment(4);		



		if($id > 0){			



			$pdf_name='resume';



			$file=get_db_field_value('wl_enquiry', $pdf_name, array("id"=>$id));



			if($file !="" && @file_exists(UPLOAD_DIR."/resumes/".$file)){



				$this->load->helper('download');



				$data = file_get_contents(UPLOAD_DIR."/resumes/".$file);



				$name = $file;



				force_download($name, $data);



			}else{



				redirect('enquiry', '');



			}



		}else{



			redirect('enquiry', '');



		}



	}



}



// End of controller