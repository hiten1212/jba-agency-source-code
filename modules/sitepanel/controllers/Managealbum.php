<?php
class Managealbum extends Admin_Controller
{
	public function __construct()
	{		
		parent::__construct(); 				
		$this->load->model(array('album_model'));
		$this->config->set_item('menu_highlight','photo');				
	}
	 
	public  function index()
	{
		 $pagesize               =  (int) $this->input->get_post('pagesize');
	     $config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');		 		 				
		 $offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;		
		 $base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));				 
		 $parent_id              =   (int) $this->uri->segment(4,0);	
		 
		 $keyword = trim($this->input->get_post('keyword',TRUE));		
		 $keyword = $this->db->escape_str($keyword);
	     $condtion = " ";
		 
		$condtion_array = array(
		                'field' =>"*",
						 'condition'=>$condtion,
						 'order'=>"id DESC",
						 'limit'=>$config['limit'],
						  'offset'=>$offset	,
						  'debug'=>FALSE
						 );							 						 	
		$res_array              =  $this->album_model->getAlbum($condtion_array);
		$config['total_rows']	=  $this->album_model->total_rec_found;	
		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		$data['heading_title']  =  'Manage Photo Album';
		$data['res']            =  $res_array; 	
		$data['parent_id']      =  $parent_id; 	
		
		if( $this->input->post('status_action')!='')
		{	
			if( $this->input->post('status_action')=='Delete')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
					$rw=get_db_multiple_row("tbl_photo","photo,id","cat_id ='$v'");
					if(is_array($rw) && count($rw) > 0 ){
					   foreach($rw as $val1){
						  $unlink_image = array('source_dir'=>"photo_img",'source_file'=>$val1['photo']);
						  @unlink($unlink_image);
						  $this->db->query("DELETE FROM tbl_photo WHERE id ='".$val1['id']."'");
					   }
					}
					  $this->db->query("DELETE FROM tbl_photo_category WHERE id ='$v'"); 
				  
					$wherea = array('entity_type'=>'gallery/index','entity_id'=>$v);
					$this->album_model->safe_delete('wl_meta_tags',$wherea,TRUE);
				  
				  						
				}
				
				
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been deleted successfully.");
				redirect("sitepanel/managealbum");
			}
			
						
			if( $this->input->post('status_action')=='Activate')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
					
                  $this->db->query("UPDATE tbl_photo SET status='1' WHERE cat_id ='$v' AND status !='2' "); 		
				  $this->db->query("UPDATE tbl_photo_category SET status='1' WHERE id ='$v'"); 						
				}
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been activated successfully.");
				redirect("sitepanel/managealbum");
			}	
			if( $this->input->post('status_action')=='Deactivate')
			{
				$prod_id=$this->input->post('arr_ids');
				
				foreach($prod_id as $v)
				{
					
                  $this->db->query("UPDATE tbl_photo SET status='0' WHERE cat_id ='$v' AND status !='2' "); 		
				  $this->db->query("UPDATE tbl_photo_category SET status='0' WHERE id ='$v'"); 						
				}
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',"Information has been de-activated successfully.");
				redirect("sitepanel/managealbum");
			}	
		}
		$this->load->view('album/view_album_list',$data);		
		
		
	}	
	
	public function add()
	{
		 $data['heading_title'] = 'Add Photo Album';
		 $img_allow_size =  $this->config->item('allow.file.size');
		 $img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		 
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');
		 
		 $data['ckeditor']  =  set_ck_config(array('textarea_id'=>'cat_desc'));
		 $this->form_validation->set_rules('category_name','Album title',"trim|required|max_length[80]|unique[tbl_photo_category.category_name = '".$this->db->escape_str($this->input->post("category_name"))."'  AND status !='2']");
		 
		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl|callback_check_valid_url");
		 
		 $this->form_validation->set_rules('image1','Image',"file_required|file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");
		 //$this->form_validation->set_rules('description','Description',"max_length[300]");
		 
		if($this->form_validation->run()===TRUE)
		{
			  	
				$uploaded_file = "";
				if( !empty($_FILES) && $_FILES['image1']['name']!='' ){
					$this->load->library('upload');
					$uploaded_data =  $this->upload->my_upload('image1','photo_img');
					if( is_array($uploaded_data)  && !empty($uploaded_data) ){
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
					}
				}
				$posted_data = array(
					'category_name'=>$this->input->post('category_name'),
					'friendly_url'=>$this->cbk_friendly_url,
					'parent_id'=>0,
					'description'=>$this->input->post('description'),
					'image'=>$uploaded_file,
					'status'=>1
				 );
		    $insert_id=$this->album_model->safe_insert('tbl_photo_category',$posted_data,FALSE);
			
			if( $insert_id > 0 )
			{
			  $redirect_url = "gallery/index";
			  
			  $meta_array  = array(
							  'entity_type'=>$redirect_url,
							  'entity_id'=>$insert_id,
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('category_name'),80),
							  'meta_description'=>get_text($this->input->post('category_name')),
							  'meta_keyword'=>get_keywords($this->input->post('category_name'))
							  );

			  create_meta($meta_array);
			}
			
				
								
			$this->session->set_userdata(array('msg_type'=>'success'));			
			$this->session->set_flashdata('success',lang('success'));				
			redirect('sitepanel/managealbum', '');		
		}	
		$this->load->view('album/view_album_add',$data);		  
	}
	
	
	public function edit()
	{
		$brandId = (int) $this->uri->segment(4);
		
		$rowdata=$this->album_model->get_album_by_id($brandId);
		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'cat_desc'));
				
		$brandId = $rowdata->id;		
		$data['heading_title'] = 'Edit Photo Album';
		
		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');
		
		$posted_friendly_url = $this->input->post('friendly_url');
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);
		$seo_url_length = $this->config->item('seo_url_length');
		
		
		if( !is_object($rowdata) )
		{
			$this->session->set_flashdata('message', lang('idmissing'));	
			redirect('sitepanel/managealbum', ''); 	
			
		}
		 $this->form_validation->set_rules('category_name','Album title',"trim|required|max_length[80]|unique[tbl_photo_category.category_name = '".$this->db->escape_str($this->input->post("category_name"))."' AND id !='$brandId']");
		 
		//$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");//|callback_check_valid_url
		 
		$this->form_validation->set_rules('image1','Image',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");		
			if($this->form_validation->run()==TRUE)
			{	
				$uploaded_file = $rowdata->image;
				$unlink_image = array('source_dir'=>"photo_img",'source_file'=>$rowdata->image);
				 if( !empty($_FILES) && $_FILES['image1']['name']!='' )
				 {
						$this->load->library('upload');
						$uploaded_data =  $this->upload->my_upload('image1','photo_img');
						if( is_array($uploaded_data)  && !empty($uploaded_data) )
						{
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
						    removeImage($unlink_image);
						}
				}
				$posted_data = array(
					'image'=>$uploaded_file,
					'category_name'=>$this->input->post('category_name',TRUE),
					'description'=>$this->input->post('description'),
					'friendly_url'=>$this->cbk_friendly_url,
				 );
			 	$where = "id = '".$brandId."'"; 				
				$this->album_model->safe_update('tbl_photo_category',$posted_data,$where,FALSE);
				
				/*$chkmeta=count_record ("wl_meta_tags","entity_id ='".$rowdata['id']."' AND entity_type='gallery/index' AND page_url='".$rowdata['friendly_url']."'"); 
				if($chkmeta>0)
				{*/ 
				 update_meta_page_url('gallery/index',$rowdata->id,$this->cbk_friendly_url);
			/*	}else{
				       $meta_array  = array(
							  'entity_type'=>'gallery/index',
							  'entity_id'=>$rowdata['id'],
							  'page_url'=>$this->cbk_friendly_url,
							  'meta_title'=>get_text($this->input->post('category_name'),80),
							  'meta_description'=>get_text($this->input->post('category_name')),
							  'meta_keyword'=>get_keywords($this->input->post('category_name'))
							  ); 
			           create_meta($meta_array);	
				}*/ 				
				
				
					
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success',lang('successupdate'));								
				
				redirect('sitepanel/managealbum'.'/'.query_string(), ''); 	
							
			}						
		$data['res']=$rowdata;		
		$this->load->view('album/view_album_edit',$data);				
		
	}
	
	public function check_valid_url()
	{
		$url      = $this->input->post('friendly_url');
		if(strtolower($url)=='gallery')
		{
			$this->form_validation->set_message('check_valid_url', 'Invalid url.');
			return FALSE;
		}else
		{
			return TRUE;
		}
	}
	
	
	
	 public function checkurl(){

	  		$product_id=(int)$this->input->post('id');

			if($product_id!=''){
				$cont='and entity_id !='.$product_id;
			}else{
				$cont='';
			}

	 		$posted_friendly_url = $this->input->post('friendly_url');

			$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

			$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();

			if($urlcount>0)
			{
				$this->form_validation->set_message('checkurl', 'URL already exists.');
			    return FALSE;

			}else
			{
				 return TRUE;
			}
  } 
	
	
}
// End of controller