<?php


class Faq extends Admin_Controller


{





	public function __construct()


	{


		parent::__construct();


		$this->load->model(array('faq/faq_model','service_category/service_category_model','city/city_model'));


		$this->config->set_item('menu_highlight','other management');


	}





	public  function index()


	{





		$pagesize               =  (int) $this->input->get_post('pagesize');





		$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');





		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;





		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));





		$res_array              =    $this->faq_model->get_faq($config['limit'],$offset);





		$total_record           =   get_found_rows();





		$config['total_rows']   =   $total_record;





		$data['page_links']      =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);





		$data['res']            =  $res_array;





		if( $this->input->post('status_action')!='')


		{


			$this->update_status('wl_faq','faq_id');


		}


		


		if( $this->input->post('update_order')!='')


		{


			$this->update_displayOrder('wl_faq','sort_order','faq_id');


		}


$categories_sql="SELECT category_id, category_name FROM wl_service_categories where parent_id=0 AND  status!='2' order by sort_order ";
		$data['categories']  = custom_result_set($categories_sql);
		
		$city_sql="SELECT city_id, city_name FROM wl_city  where status!='2' order by sort_order ";
		$data['citys']  = custom_result_set($city_sql);


		$data['heading_title'] = 'Manage FAQ\'s';


		$data['pagelist']      = $res_array;


		$this->load->view('faq/view_faq_list',$data);


		


	}


	


	public function display()


	{


		


		$res = $this->faq_model->getHelpcenter_by_id($this->uri->segment(4));


		$data['heading_title'] = 'View FAQ Information';


		$data['page_title']    = 'View FAQ Information';


		$data['pageresult']=$res;


		


		$this->load->view('common/view_helpcenter_detail',$data);


		


	}


	


	public function add()


	{


		$data['ckeditor']      =  set_ck_config(array('textarea_id'=>'faq_answer'));


		$data['heading_title'] = 'Add FAQ';

$this->form_validation->set_rules('category_id','Services Category',"trim");
			//$this->form_validation->set_rules('city_id','City',"trim|required");

		$this->form_validation->set_rules('faq_question','Question',"trim|required|max_length[220]|unique[wl_faq.faq_question='".$this->db->escape_str($this->input->post('faq_question'))."'   AND status!='2']");


		$this->form_validation->set_rules('faq_answer','Answer','trim|required|required_stripped|max_length[8500]');





		if($this->form_validation->run()==TRUE)


		{


			


			#-------------------MAX SORT ORDER------------#


			$this->db->select_max('sort_order');


			$query = $this->db->get('wl_faq');


			$max_sort_order= $query->row_array();


			$max_sort_orders=$max_sort_order['sort_order']+1;


			#--------------------------------------------#


			


			$posted_data = array(

			//'category_id'=>$this->input->post('category_id',TRUE),
				//'city_id'=>$this->input->post('city_id',TRUE),
			'faq_question'=>$this->input->post('faq_question',TRUE),


			'faq_answer'=>$this->input->post('faq_answer',TRUE),


			'sort_order'=>$max_sort_orders,


			'faq_date_added'=>$this->config->item('config.date.time')


			);


			


			$posted_data = $this->security->xss_clean($posted_data);


			$this->faq_model->safe_insert('wl_faq',$posted_data,FALSE);


			$this->session->set_userdata(array('msg_type'=>'success'));


			$this->session->set_flashdata('success',lang('success'));


			redirect('sitepanel/faq', '');


			


		}


		


		$this->load->view('faq/view_faq_add',$data);


		


	}


	


	public function edit()


	{





		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'faq_answer'));


		


		$data['heading_title'] = 'Edit FAQ';


		


		 $Id = (int) $this->uri->segment(4);


		


		$res = $this->faq_model->get_faq(1,0,array('id'=>$Id));


		if(  is_array($res) && !empty($res) )

		{
			
			$this->form_validation->set_rules('category_id','Services Category',"trim");
			//$this->form_validation->set_rules('city_id','City',"trim|required");
			$this->form_validation->set_rules('faq_question','Question',"trim|required|max_length[220]|unique[wl_faq.faq_question='".$this->db->escape_str($this->input->post('faq_question'))."'   AND status!='2' AND faq_id != ".$Id."]");


			$this->form_validation->set_rules('faq_answer','Answer','trim|required|required_stripped|max_length[8500]');


			


			if($this->form_validation->run()==TRUE)


			{

//trace($posted_data);exit;
				


				$posted_data = array(

				//'category_id'=>$this->input->post('category_id',TRUE),
				//'city_id'=>$this->input->post('city_id',TRUE),
				'faq_question'=>$this->input->post('faq_question',TRUE),
				'faq_answer'=>$this->input->post('faq_answer',TRUE)


				);


				


				$posted_data = $this->security->xss_clean($posted_data);


				$where = "faq_id = '".$res['faq_id']."'";


				$this->faq_model->safe_update('wl_faq',$posted_data,$where,FALSE);


				$this->session->set_userdata(array('msg_type'=>'success'));


				$this->session->set_flashdata('success',lang('successupdate'));


				


				redirect('sitepanel/faq/'.query_string(), '');


				


			}


			


			$data['res']=$res;


			$this->load->view('faq/view_faq_edit',$data);


			


		}else


		{


			redirect('sitepanel/faq', '');


		}


		


	}


	


}


//controllet end