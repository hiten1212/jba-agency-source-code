<?php

class Enquiry_details extends  Admin_Controller{



	public function __construct()

	{

		parent::__construct();

		$this->load->model(array('sitepanel/enquiry_details_model'));

		$this->config->set_item('menu_highlight','other management');

		$this->form_validation->set_error_delimiters("<div class='red'>","</div>");

	}



	public  function index()

	{

		$enq_id = (int) $this->uri->segment(4);

		

		$keyword                 =  trim($this->input->post('keyword',TRUE));

		$pagesize                =  (int) $this->input->get_post('pagesize');

		$config['limit']		  =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');

		$offset                  =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;

		$base_url             =  current_url_query_string(array('filter'=>'result'),array('per_page'));

		

		$condition               =  "enquiry_id ='".$enq_id."' ";

		$keyword                 = $this->db->escape_str( $keyword );

		

		$res_array     = $this->enquiry_details_model->get_enquiry($offset,$config['limit'],$condition);

		

		$config['total_rows']	= $this->enquiry_details_model->total_rec_found;

		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);

		

		if( $this->input->post('status_action')!='')

		{

			$this->update_status('wl_enquiry_details','id');

		}

		if($this->input->post('Send')=='Send Email')

		{

			$this->change_status();

		}

		

		$data['heading_title']  = 'Manage Service Enquiries';

		$data['inq_type']       = 'General';

		$data['res']            = $res_array;

		$this->load->view('enquiry_details/view_enquiry_list',$data);

		

	}

	



}

// End of controller