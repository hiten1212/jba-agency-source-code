<?php



class Testimonial extends Admin_Controller



{



	public function __construct()



	{



		parent::__construct();



		$this->load->model(array('testimonials/testimonial_model'));



		$this->config->set_item('menu_highlight','other management');



		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");



	}







	public  function index()



	{



		$pagesize               =  (int) $this->input->get_post('pagesize');



		$config['limit']		 =  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');



		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;







		$base_url            =  current_url_query_string(array('filter'=>'result'),array('per_page'));







		$res_array               =  $this->testimonial_model->get_testimonial($config['limit'],$offset);



		$config['total_rows']    =  get_found_rows();







		$data['page_links']      =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);
		$data['heading_title']   =   'Testimonial';
		$data['res']             =  $res_array;

		if($this->input->post('status_action')!=''){
			
			if( $this->input->post('status_action')=='Delete')
			{
				$test_id=$this->input->post('arr_ids');		

				foreach($test_id as $v){					
				  $where = array('entity_type'=>'testimonials/details','entity_id'=>$v);
				  $this->testimonial_model->safe_delete('wl_meta_tags',$where,TRUE);					
				}
			}

			$this->update_status('wl_testimonial','testimonial_id');
		}

		$this->load->view('testimonial/view_testimonial_list',$data);

	}

	public function post(){



		$img_allow_size =  $this->config->item('allow.file.size');

		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');

		

		$posted_friendly_url = $this->input->post('friendly_url');

		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);

		$seo_url_length = $this->config->item('seo_url_length');



		$this->form_validation->set_rules('friendly_url','Page URL',"trim|required|max_length[$seo_url_length]|callback_checkurl");

		//$this->form_validation->set_rules('testimonial_title','Title','trim|required|max_length[150]');

		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');

		$this->form_validation->set_rules('photo','Photo',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

		$this->form_validation->set_rules('poster_name','Name','trim|required|alpha|max_length[30]');

		$this->form_validation->set_rules('testimonial_description','Comment','trim|required');



		$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'testimonial_description'));



		if($this->form_validation->run()==TRUE){



			$redirect_url="testimonials/details";

			

			$uploaded_file = "";

			if( !empty($_FILES) && $_FILES['photo']['name']!='' ){

				$this->load->library('upload');

				$uploaded_data =  $this->upload->my_upload('photo','testimonial');

				if( is_array($uploaded_data)  && !empty($uploaded_data) ){

					$uploaded_file = $uploaded_data['upload_data']['file_name'];

				}



			}

			

			$posted_data=array(

			//'testimonial_title'      => $this->input->post('testimonial_title'),

			'poster_name'             => $this->input->post('poster_name'),

			'friendly_url'=>$this->cbk_friendly_url,

			'email'                   => $this->input->post('email'),

			'testimonial_description' => $this->input->post('testimonial_description'),

			'photo'=>$uploaded_file,

			'status'=>'1',

			'posted_date'            =>$this->config->item('config.date.time')

			);

			

			$posted_data = $this->security->xss_clean($posted_data);

			$testimonialid=$this->testimonial_model->safe_insert('wl_testimonial',$posted_data,FALSE);



			if($testimonialid>0) {

			  $meta_array  = array(

			  'entity_type'=>$redirect_url,

			  'entity_id'=>$testimonialid,

			  'page_url'=>$this->cbk_friendly_url,

			  'meta_title'=>get_text($this->input->post('poster_name'),80),

			  'meta_description'=>get_text($this->input->post('testimonial_description')),

			  'meta_keyword'=>get_keywords($this->input->post('testimonial_description'))

			  );

				create_meta($meta_array);

			}

			$message = $this->config->item('testimonial_post_success');

			//$message = str_replace('<site_name>',$this->config->item('site_name'),$message);

			$this->session->set_userdata(array('msg_type'=>'success'));

			$this->session->set_flashdata('success','Your testimonial has been added successfully.');

			redirect('sitepanel/testimonial', '');



		}



		$data['heading_title'] = "Post Testimonial";

		$this->load->view('testimonial/view_post_testimonials',$data);	



	}



	



	public function edit(){

		$img_allow_size =  $this->config->item('allow.file.size');
		$img_allow_dim  =  $this->config->item('allow.imgage.dimension');		

		$id = (int) $this->uri->segment(4);		
		$param     = array('where'=>"testimonial_id ='$id' ");		
		$res       = $this->testimonial_model->get_testimonial(1,0,$param);		

		$posted_friendly_url = $this->input->post('friendly_url');		
		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);	
		$seo_url_length = $this->config->item('seo_url_length');

		if( is_array($res) && !empty($res) ){
			//$this->form_validation->set_rules('testimonial_title','Title','trim|required|max_length[150]');
			$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');

			//$this->form_validation->set_rules('photo','Photo',"file_allowed_type[image]|file_size_max[$img_allow_size]|check_dimension[$img_allow_dim]");

			$this->form_validation->set_rules('poster_name','Name','trim|required|alpha|max_length[30]');
			$this->form_validation->set_rules('testimonial_description','Comment','trim|required');	
			$data['ckeditor']  =  set_ck_config(array('textarea_id'=>'testimonial_description'));
			if($this->form_validation->run()==TRUE){			
				
				$uploaded_file = $res[0]['photo'];				
				$unlink_image = array('source_dir'=>"testimonial",'source_file'=>$res[0]['photo']);

			 	if($this->input->post('photo_img_delete')==='Y'){
					removeImage($unlink_image);
					$uploaded_file = NULL;
				}
			

				if( !empty($_FILES) && $_FILES['photo']['name']!='' ){
						$this->load->library('upload');
						$uploaded_data =  $this->upload->my_upload('photo','testimonial');
						if( is_array($uploaded_data)  && !empty($uploaded_data) ){
							$uploaded_file = $uploaded_data['upload_data']['file_name'];
						    removeImage($unlink_image);
						}
				}				

				$posted_data=array(
				//'testimonial_title'       => $this->input->post('testimonial_title'),
				'poster_name'             => $this->input->post('poster_name'),
				'friendly_url'            =>$this->cbk_friendly_url,
				'email'                   => $this->input->post('email'),
				'photo'                   =>$uploaded_file,
				'testimonial_description' => $this->input->post('testimonial_description')
				);		

				$posted_data = $this->security->xss_clean($posted_data);			

				$where = "testimonial_id = '".$id."'";
				$this->testimonial_model->safe_update('wl_testimonial',$posted_data,$where,FALSE);

				update_meta_page_url('testimonials/details',$res[0]['testimonial_id'],$this->cbk_friendly_url);

			$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',lang('successupdate'));	
				redirect('sitepanel/testimonial', '');
			}

			$data['heading_title'] = "Edit Testimonial";
			$data['res'] = $res;
			$this->load->view('testimonial/view_edit_testimonials',$data);

		}else{

			redirect('sitepanel/testimonial', '');
		}	
	}

	public function checkurl(){

		$product_id=(int)$this->input->post('products_id');

		if($product_id!=''){



			$cont='and entity_id !='.$product_id;



		}else{



			$cont='';



		}



		



		$posted_friendly_url = $this->input->post('friendly_url');



		$this->cbk_friendly_url = seo_url_title($posted_friendly_url);



		$urlcount=$this->db->query("select * from wl_meta_tags where page_url='".$this->cbk_friendly_url."'".$cont."")->num_rows();



		



		if($urlcount>0)



		{



			$this->form_validation->set_message('checkurl', 'URL already exists.');



			return FALSE;



		}else



		{



			return TRUE;



		}



		



	}







}



// End of controller