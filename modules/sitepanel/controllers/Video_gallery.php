<?php

class Video_gallery extends Admin_Controller

{



	public function __construct()

	{

		     parent::__construct(); 				

			$this->load->model(array('video_gallery/video_gallery_model'));

			$this->config->set_item('menu_highlight','gallery management');	

	}



	public  function index($page = NULL)

	{		

		$pagesize               =  (int) $this->input->get_post('pagesize');		

		$config['limit']		=  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');			

		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;	

				

		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));

		$param					= array('type'=>'2');				

						

		$res_array              =  $this->video_gallery_model->get_video_gallery($config['limit'],$offset,$param);			

		$config['base_url']     =  base_url().'sitepanel/video_gallery/pages/'; 		

		$config['total_rows']	=  get_found_rows();	

		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);				

		$data['heading_title']  = 'Video Gallery';

		$data['res'] = $res_array; 		

		

		if( $this->input->post('status_action')!='')

		{			

			$this->update_status('wl_gallery','id');			

		}

		

		if( $this->input->post('set_as')!='' ){

		    $set_as    = $this->input->post('set_as',TRUE);

			$this->set_as('wl_gallery','id',array($set_as=>'1'));

		}

		if( $this->input->post('unset_as')!='' ){

		    $unset_as   = $this->input->post('unset_as',TRUE);

			$this->set_as('wl_gallery','id',array($unset_as=>'0'));

		}

			

		$this->load->view('video_gallery/view_video_list',$data);	

			

	} 

	

	

	public function one_added(){

		    $extension = substr(strrchr($_FILES['gallery_file']['name'], '.'), 1);

			if($_FILES['gallery_file']['name']!='' && $this->input->post('embed_code')!=''){

				$this->form_validation->set_message('one_added', 'Please upload video file or embeded code.');

				return FALSE;

			}

			if($_FILES['gallery_file']['name']=='' && $this->input->post('embed_code')==''){

				$this->form_validation->set_message('one_added', 'Please upload atleast video file or embeded code.');

				return FALSE;

			}

			else{

				return TRUE;

			}

		}

	



	public function add()

	{		  

		$data['heading_title'] = 'Add Video';	

		$allow_video_size          =  $this->config->item('allow_video_size');

		$img_allow_size =  $this->config->item('allow.file.size');	

		
		 $this->form_validation->set_rules('title','Video Title',"required|max_length[99]|unique[wl_gallery.title='".$this->db->escape_str($this->input->post('title'))."' AND wl_gallery.type='2' AND wl_gallery.status!='2']");
		 $this->form_validation->set_rules('gallery_file','Gallery image',"trim|file_allowed_type[image]");		
		 $this->form_validation->set_rules('embed_code','Embed Code','trim|required|max_length[500]');

		if($this->form_validation->run()==TRUE){
			
			$uploaded_file = "";	
			    if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){						

					$this->load->library('upload');		
					$uploaded_data =  $this->upload->my_upload('gallery_file','image_gallery');				

					if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 	
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
					}	
				}

															

				$posted_data = array(

				'title'	=>	$this->input->post('title'),
				'embed_code'=>$this->input->post('embed_code',TRUE),
				'gallery_file'	=>	$uploaded_file,	
				'type'    =>  '2',
				'post_date'		=>	$this->config->item('config.date.time')							

				);

								

		    $this->video_gallery_model->safe_insert('wl_gallery',$posted_data,FALSE);									

			$this->session->set_userdata(array('msg_type'=>'success'));			

			$this->session->set_flashdata('success',lang('success'));		

			redirect('sitepanel/video_gallery', '');

		}

		$this->load->view('video_gallery/view_video_add',$data);		  

	}

	

	

	public function edit()

	{

		$Id = (int) $this->uri->segment(4);		   

		$data['heading_title'] = 'Update Video';				

		$param=array('id'=> $Id);					

		$rowdata=$this->video_gallery_model->get_video_gallery(1,0,$param);

				 

		if( is_array($rowdata) ){

			

				$this->form_validation->set_rules('title','Video Title',"required|max_length[99]|unique[wl_gallery.title='".$this->db->escape_str($this->input->post('title'))."' AND wl_gallery.type='2' AND wl_gallery.id!='".$rowdata['id']."' AND wl_gallery.status!='2']");
				$this->form_validation->set_rules('gallery_file','Gallery Image',"file_allowed_type[image]");		
				$this->form_validation->set_rules('embed_code','Embed Code','trim|required|max_length[1000]');

				

				if($this->form_validation->run()==TRUE){					

						$uploaded_file = $rowdata['gallery_file'];				 

					$unlink_image = array('source_dir'=>"image_gallery",'source_file'=>$rowdata['gallery_file']);

													

					if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){			  

						  

						$this->load->library('upload');					

						$uploaded_data =  $this->upload->my_upload('gallery_file','image_gallery');

						

						if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 								

						   $uploaded_file = $uploaded_data['upload_data']['file_name'];

						   removeimage($unlink_image);	

						}

					

				    }else{

						 $uploaded_file = $rowdata['gallery_file'];

					}			

					$posted_data = array(

					'title'	=>	$this->input->post('title',TRUE),
					'embed_code'	=>	$this->input->post('embed_code',TRUE),
					'gallery_file'	=>	$uploaded_file

					);

					

					$where = "id = '".$rowdata['id']."'";

					$this->video_gallery_model->safe_update('wl_gallery',$posted_data,$where,TRUE);

					$this->session->set_userdata(array('msg_type'=>'success'));				

				    $this->session->set_flashdata('success',lang('successupdate'));	

					redirect('sitepanel/video_gallery/'.query_string(), ''); 

					 

				}

				$data['res']=$rowdata;

				$this->load->view('video_gallery/view_video_edit',$data);

				

			

		}else

		{

			redirect('sitepanel/video_gallery', ''); 	 

		}

		

	}

	

	

	public  function details(){

		$id                      = (int) $this->uri->segment(4);

		$param                   = array('where'=>"id ='$id' ","result_type"=>"row_array");

		$res                     = $this->video_gallery_model->get_video_gallery(1,0,$param);

		$data['res'] = $res;

		$this->load->view('video_gallery/detail',$data);



	}

	

	public function playvideo(){

				

		$Id  = (int) $this->uri->segment(4);

		$query=$this->db->query("SELECT * FROM wl_gallery WHERE id='".$Id."'");

		$row_founds = $query->num_rows();

		if($row_founds>0) 

		{

			$data['res']= $query->row_array();

		}

		

		$this->load->view('video_gallery/play_video',$data);

			

    } 

	

}

// End of controller