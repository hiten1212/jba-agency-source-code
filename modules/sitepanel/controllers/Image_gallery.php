<?php

class Image_gallery extends Admin_Controller

{



	public function __construct()

	{

		     parent::__construct(); 				

			$this->load->model(array('image_gallery/image_gallery_model'));  			

			$this->config->set_item('menu_highlight','gallery management');	

	}



	public  function index($page = NULL)
	{
		//echo phpinfo();
		$album_id =  (int) $this->uri->segment(4);		
		
		$pagesize               =  (int) $this->input->get_post('pagesize');		

		$config['limit']		=  ( $pagesize > 0 ) ? $pagesize : $this->config->item('pagesize');			

		$offset                 =  ( $this->input->get_post('per_page') > 0 ) ? $this->input->get_post('per_page') : 0;	

				

		$base_url               =  current_url_query_string(array('filter'=>'result'),array('per_page'));		

		$param					= array('type'=>'1','album_id'=>$album_id); 

		

		$res_array              =  $this->image_gallery_model->get_image_gallery($config['limit'],$offset,$param);

		

		$config['base_url']     =  base_url().'sitepanel/image_gallery/pages/'; 		

		$config['total_rows']	=  get_found_rows();	

		$data['page_links']     =  admin_pagination($base_url,$config['total_rows'],$config['limit'],$offset);				

		$data['heading_title']  = 'Image Gallery Lists';

		$data['res'] = $res_array; 		

		

		if( $this->input->post('status_action')!='')

		{	
		  if($this->input->post('status_action')=='Delete'){
			
			$arr_ids = $this->input->post('arr_ids');	
			
			if(is_array($arr_ids) && !empty($arr_ids)){
				
		     foreach($arr_ids as $v){
		      $param=array('id'=> $v);

		          $rowdata=$this->image_gallery_model->get_image_gallery(1,0,$param);
					$unlink_image = array('source_dir'=>"image_gallery",'source_file'=>$rowdata['gallery_file']);
					removeimage($unlink_image);
					
			  }
					
			}
					
		}
			$this->update_status('wl_gallery','id');			

		}

		if( $this->input->post('set_as')!='' ){

		    $set_as    = $this->input->post('set_as',TRUE);

			$this->set_as('wl_gallery','id',array($set_as=>'1'));

		}

		if( $this->input->post('unset_as')!='' ){

		    $unset_as   = $this->input->post('unset_as',TRUE);

			$this->set_as('wl_gallery','id',array($unset_as=>'0'));

		}

					

		$this->load->view('image_gallery/view_image_gallery_list',$data);	

	} 

	public function add(){	
			  
		$album_id =  (int) $this->uri->segment(4);
//		if(!$album_id){
//			redirect('sitepanel/image_gallery', '');
//		}	
		
		$data['heading_title'] = 'Add Image';	
		//$this->form_validation->set_rules('title','Image Title',"required|max_length[99]|unique[wl_gallery.title='".$this->db->escape_str($this->input->post('title'))."' AND wl_gallery.type='1' AND wl_gallery.status!='2']");
		$this->form_validation->set_rules('title','Image Title',"required|max_length[99]");		
		$this->form_validation->set_rules('gallery_file','Gallery image',"required|file_allowed_type[image]");
		//$this->form_validation->set_rules('alt_tag','Alt Tag',"trim|max_length[50]");
		//$this->form_validation->set_rules('description','Description',"trim|required|max_length[10000]");		 		

		if($this->form_validation->run()==TRUE){		

			    $uploaded_file = "";	
			    if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){						

					$this->load->library('upload');		
					$uploaded_data =  $this->upload->my_upload('gallery_file','image_gallery');				

					if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 	
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
					}	
				}

				$posted_data = array(
				'album_id'	=>	$album_id,
				'title'	=>	$this->input->post('title'),
				'post_date'		=>	$this->config->item('config.date.time'),
				'gallery_file'	=>	$uploaded_file,					
				'type'    =>  '1'		
				);								

		    $this->image_gallery_model->safe_insert('wl_gallery',$posted_data,FALSE);									
			$this->session->set_userdata(array('msg_type'=>'success'));			
			$this->session->set_flashdata('success',lang('success'));		
			redirect('sitepanel/image_gallery/index/', '');

		}

		$this->load->view('image_gallery/view_image_add',$data);		  

	}



	public function edit(){		

		$Id = (int) $this->uri->segment(4);		   
		$album_id =  (int) $this->uri->segment(5);

		$data['heading_title'] = 'Update Image Gallery';

		

		$param=array('id'=> $Id);

					

		$rowdata=$this->image_gallery_model->get_image_gallery(1,0,$param);

			 

		if( is_array($rowdata) ){

			

				//$this->form_validation->set_rules('title','Image Gallery Title',"required|max_length[99]|unique[wl_gallery.title='".$this->db->escape_str($this->input->post('title'))."' AND wl_gallery.id!='".$rowdata['id']."' AND wl_gallery.type='1' AND wl_gallery.status!='2']");
				$this->form_validation->set_rules('title','Image Title',"required|max_length[99]");				
				$this->form_validation->set_rules('gallery_file','Gallery Image',"file_allowed_type[image]");

				//$this->form_validation->set_rules('alt_tag','Alt Tag',"trim|max_length[50]");
				//$this->form_validation->set_rules('description','Description',"trim|required|max_length[10000]");

				

				if($this->form_validation->run()==TRUE){

					

					$uploaded_file = $rowdata['gallery_file'];				 

					$unlink_image = array('source_dir'=>"image_gallery",'source_file'=>$rowdata['gallery_file']);

													

					if( !empty($_FILES) && $_FILES['gallery_file']['name']!='' ){			  

						  

						$this->load->library('upload');					

						$uploaded_data =  $this->upload->my_upload('gallery_file','image_gallery');

						

						if( is_array($uploaded_data)  && !empty($uploaded_data) ){ 								

						   $uploaded_file = $uploaded_data['upload_data']['file_name'];

						   removeimage($unlink_image);	

						}

					

				    }else{

						 $uploaded_file = $rowdata['gallery_file'];

					}

					

					$posted_data = array(

					'title'	=>	$this->input->post('title'),					
					'gallery_file'	=>	$uploaded_file

					);

					

					$where = "id = '".$rowdata['id']."'"; 				

					$this->image_gallery_model->safe_update('wl_gallery',$posted_data,$where,FALSE);						

					$this->session->set_userdata(array('msg_type'=>'success'));				

				    $this->session->set_flashdata('success',lang('successupdate'));	

					redirect('sitepanel/image_gallery/index/'.query_string(), ''); 

					 

				}

				$data['res']=$rowdata;

				$this->load->view('image_gallery/view_image_edit',$data);

				

			

		}else

		{

			redirect('sitepanel/image_gallery/index/', ''); 	 

		}

		

	}

	

}

// End of controller