<?php $this->load->view('includes/header');
$pcatID =($this->uri->segment(4) > 0)? $this->uri->segment(4):"0";
$pcatID = (int) $pcatID;

?>
<?php  $back_link = 'sitepanel/careers';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1">Add</a> <?php echo anchor("sitepanel/careers",'Cancel','class="btn1" ' );?></span>
        </div>
        
    <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart('sitepanel/careers/add/',array('id'=>'form'));?>

     <div id="tab-general">
     
   
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>      
    
    <?php
			$default_params = array(
				'heading_element' => array(
				'field_heading'=>"Name",
				'field_name'=>"job_title",
				'field_placeholder'=>"Title",
				'exparams' => 'size="40"'
				),
				'url_element'  => array(
				'field_heading'=>"Page URL",
				'field_name'=>"friendly_url",
				'field_placeholder'=>"Page URL",
				'exparams' => 'size="40"',
				'pre_seo_url' =>'',
				'pre_url_tag'=>FALSE
				)
			);
			seo_add_form_element($default_params);
			?> 
     <p class="clearfix"></p>
    <?php /*?><p class="form_title1">Location <span class="required">*</span> :</p>
    <div class="form_field form_field2"><input name="location" type="text" style="width:450px" class="form-control" value="<?php echo set_value('location');?>"><?php echo form_error('location');?></div>
    <p class="clearfix"></p>
    
    <p class="clearfix"></p>
    <p class="form_title1">Salary <span class="required">*</span> :</p>
    <div class="form_field form_field2"><input name="salary_range" type="text" style="width:450px" class="form-control" value="<?php echo set_value('salary_range');?>"><?php echo form_error('salary_range');?></div>
    <p class="clearfix"></p><?php */?>
    
    <div class="form_title1"><span class="required">*</span>Description :</div>
    <div class="form_field form_field2">
      <textarea name="jobs_description" rows="5" cols="50" class="form-control" id="description"><?php echo set_value('jobs_description');?></textarea><?php echo display_ckeditor($ckeditor);?><?php echo form_error('jobs_description');?>
    </div>
    <p class="clearfix"></p>
    
   
     </div>
    
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
   
      <input type="hidden" name="action" value="add" />
   </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
  
</div>
</div>
<!--Body End-->
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//--></script>
<?php $selected_date = $stock_expected_date=='' ? 'y' : $stock_expected_date;
$default_date = date('Y-m-d',strtotime(date('Y-m-d',time())));?>
<script type="text/javascript">
  $(document).ready(function(){
	$('.start_date,.end_date').on('click',function(e){
  e.preventDefault();
  cls = $(this).hasClass('start_date') ? 'start_date1' : 'end_date1';
  $('.'+cls+':eq(0)').focus();
});

$( ".start_date1").on('focus',function(){
		$(this).datepicker({
		showOn: "focus",
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		defaultDate: 'y',
		buttonText:'',
		minDate:'<?php echo $default_date;?>' ,
		maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+180 days"));?>',
		yearRange: "c-100:c+100",
		buttonImageOnly: true,
		onSelect: function(dateText, inst) {
					  $('.start_date1').val(dateText);
					  $( ".end_date1").datepicker("option",{
						minDate:dateText ,
						maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+180 days"));?>',
					});
				  }
	});

});

$( ".end_date1").on('focus',function(){
		$(this).datepicker({
				  showOn: "focus",
				  dateFormat: 'yy-mm-dd',
				  changeMonth: true,
				  changeYear: true,
				  defaultDate: 'y',
				  buttonText:'',
				  minDate:'<?php echo $posted_start_date!='' ? $posted_start_date :  $default_date;?>' ,
				  maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+180 days"));?>',
				  yearRange: "c-100:c+100",
				  buttonImageOnly: true,
				  onSelect: function(dateText, inst) {
					$('.end_date1').val(dateText);
				  }
			  });
  });

});

function sales_product_showhide(val){
	
	$("#sh").hide();	
	if(val==2){		
		$("#sh").show();	
	}
}

</script>
<?php $this->load->view('includes/footer');?>