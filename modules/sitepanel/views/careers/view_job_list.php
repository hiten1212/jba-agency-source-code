<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<div class="breadcrumb">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li><?php echo anchor('sitepanel/dashbord','Dashboard');?></li>
   <?php    $segment=4;
   $catid = (int)$this->input->get_post('category_id',TRUE);
   if($catid ){
	   echo admin_category_breadcrumbs($catid,$segment);
   }else{
	   echo '<li class="active">Careers</li>';
   }?>
    </ol>
  </div>
</div>
<!--Breadcrumb End--> 

<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
    <p class="add-product"><a href="<?php echo base_url();?>sitepanel/careers/add/<?php echo $this->input->get('category_id');?>">Add</a></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
     <?php
    if(error_message() !=''){
	    echo error_message();
    }?>
    <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize();   } </script>
    <?php echo form_open("sitepanel/careers/",'id="search_form" method="get" ');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword2" value="<?php echo $this->input->post('keyword2',TRUE);?>" type="text" class="form-control" placeholder="Job Title">
      </p>
      <p class="serch-sec2">
        <select name="status" class="form-control">
          <option value="">Status</option>
        <option value="1" <?php echo $this->input->get_post('status',TRUE)==='1' ? 'selected="selected"' : '';?>>Active</option>
        <option value="0" <?php echo $this->input->get_post('status',TRUE)==='0' ? 'selected="selected"' : '';?>>In-active</option>
        </select>
      </p>
      
      <p class="serch-sec2">
      <input type="hidden" name="search" value="yes"  />
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
       
      <p class="serch-sec2"><?php if($this->input->get_post('keyword2')!='' || $this->input->get_post('status')!=''){ echo anchor("sitepanel/careers/",'<span>Clear Search</span>','class="form-control"'); }?></p>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div>      
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div><p class="clearfix"></p>
    </div>
    <div class="clearfix"></div>
    <?php echo form_close();?>
    <?php
		if( is_array($res) && !empty($res) ){
			echo form_open(current_url_query_string(),'id="data_form"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="25%" class="fs14">Title</td>
              <td width="10%" class="text-center fs13">Details</td>
              <td width="10%" class="text-center fs13">Status</td>
              <td width="12%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php
			  $atts = array(
				'width'      => '740',
				'height'     => '600',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
				);
			  $atts_edit = array(
			  'width'      => '525',
			  'height'     => '375',
			  'scrollbars' => 'no',
			  'status'     => 'no',
			  'resizable'  => 'no',
			  'screenx'    => '0',
			  'screeny'    => '0'
			  );
			  foreach($res as $catKey=>$pageVal){
				  ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['jobs_id'];?>" /></td>
            <td width="25%" class="fs14">
             <?php echo $pageVal['job_title'];?>                    
				    <div id="dialog_<?php echo $pageVal['jobs_id'];?>" title="Description" style="display:none;"><?php echo $pageVal['jobs_description'];?></div>
                    </td>
            
            <td width="10%" class="u fs13" align="center" >
		    <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $pageVal['jobs_id'];?>').dialog( {width: 650} );">View Details</a>   
		    </td>
            <td width="10%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="12%" class="text-center grey fs13"><?php echo anchor("sitepanel/careers/edit/$pageVal[jobs_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');?> 
                    </td>
          </tr>
            <?php
			  }?>
               <?php
	   if($page_links!=''){
		   ?>
            <tr><td colspan="9" align="center" height="30"><?php echo $page_links; ?></td></tr>
            <?php }?>
         
        </table>
      </div>
      <div class="mt15 ">
      <p class=" mt10 serch-sec8 fl">
      	   <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onclick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
			   <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onclick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
			   <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onclick="return validcheckstatus('arr_ids[]','delete','Record');"/>
          </p>
	  <p class=" ml20 serch-sec8 fl">
	      <?php
			   /*echo form_dropdown("set_as",$this->config->item('jobs_set_as_config'),$this->input->post('set_as'),'class="w15 mt10 form-control " style="width:175px;" onchange="return onclickgroup()"'); ?>
                </p>
                <p class=" ml20 serch-sec8 fl">
                <?php 
			   echo form_dropdown("unset_as",$this->config->item('jobs_unset_as_config'),$this->input->post('unset_as'),'class="w15 mt10  form-control " style="width:175px;" onchange="return onclickgroup()"');*/
			   ?>
      </p>
       <p class="clearfix"></p>  
          </div> 



      
    </div>
    <?php
			echo form_close();
		}else{
			echo "<center><strong> No record(s) found !</strong></center>" ;
		}?>
  </div>
</div>
<!--Body End--> 
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer');?>