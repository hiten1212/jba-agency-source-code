<?php $this->load->view('includes/header');
$block_path="careers/job_applied/";
?>
<!--Breadcrumb-->
<?php 
echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End--> 

<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
   
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
    <?php echo error_message();
   echo form_open("",'id="search_form"');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Name, Email">
      </p>
      
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php
      if($this->input->get_post('keyword')!=''){
	      echo anchor("sitepanel/careers/job_applied/",'<span>Clear Search</span>','class="form-control"');
      }?>
      </p>
      <div class="records fs14"> Records Per Page :
       <?php echo display_record_per_page();?>
      </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
   
    <div class="clearfix"></div>
     <?php echo form_close();
	 echo anchor("sitepanel/downloaddata/",'<span>Doenload Zip</span>','class=""');
   if( is_array($res) && !empty($res) ){
	   echo form_open("",'id="data_form"');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);"></td>
              <td width="25%" class="fs14">User Info</td>
              <td width="20%" class="text-center fs13">Email</td>
              <td width="40%" class="text-center fs13">Other Details</td>
             <td width="10%" class="text-center fs13">Date</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php
	     $atts = array(
				'width'      => '600',
				'height'     => '300',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
			 );
		$atts2 = array(
				'width'      => '700',
				'height'     => '600',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
			 );
			 foreach($res as $catKey=>$res){
				 $address_details=array();
				 ?>
          <tr>
            <td width="5%"><input type="checkbox" name="arr_ids[]" value="<?php echo $res['id'];?>" /></td>
            <td width="25%" class="fs14"><?php echo ucwords($res['first_name'].' '.$res['last_name']);?> <br>
				   <?php
				   if($res['address']!="" && $res['address']!='0'){
					   $address_details[]="<b>Address : </b>".nl2br($res['address']);
				   }
				   if($res['relevant_experience']!="" && $res['relevant_experience']!='0'){
					   $address_details[]="<b>Experience : </b>".$res['relevant_experience'];
				   }
				   if($res['expected_salary']!="" && $res['expected_salary']!='0'){
					   $address_details[]="<b>Expected Salary : </b>".$res['expected_salary'];
				   }
				   if($res['applied_for']!="" && $res['applied_for']!='0'){
					   $address_details[]="<b>Applied for : </b>".$res['applied_for'];
				   }
				   if($res['state']!="" && $res['state']!='0'){
					   $address_details[]="<b>State : </b>".ucwords($res['state']);
				   }
				   if($res['zipcode']!="" && $res['zipcode']!='0'){
					   $address_details[]="<b>Pincode : </b>".ucwords($res['zipcode']);
				   }
				   if($res['company_name']!="" && $res['company_name']!='0'){
					   $address_details[]="<b>Company : </b>".ucwords($res['company_name']);
				   }
				   if($res['phone_number']!="" && $res['phone_number']!='0'){
					   $address_details[]="<b>Phone : </b>".$res['phone_number'];
				   }
				   if($res['ext_number']!="" && $res['ext_number']!='0'){
					   $address_details[]="<b>Ext : </b>".$res['ext_number'];
				   }
				   if($res['mobile_number']!="" && $res['mobile_number']!='0'){
					   $address_details[]="<b>Mobile : </b>".$res['mobile_number'];
				   }
				   if($res['fax_number']!="" && $res['fax_number']!='0'){
					   $address_details[]="<b>Fax : </b>".$res['fax_number'];
				   }
				   if($res['resume']!="" && $res['resume']!='0'){
					   $address_details[]="<b>Resume : </b><a href='".base_url()."uploaded_files/resumes/".$res['resume']."' target='_blank'>Download/View Resume</a>";
				   }
				   if(!empty($address_details)){
					   echo implode("<br>",$address_details)."<br />";
				   }
				   if(!empty($res['product_service'])){
					   echo "<b>Product Name : </b>".$res['product_service']."<br />";
				   }
				   if(!empty($res['quantity'])){
					   echo "<b>Quantity : </b>".$res['quantity']."<br />";
				   }
				   if(!empty($res['unit_type'])){
					   echo "<b>Unit Type : </b>".$res['unit_type']."<br />";
				   }
				   if(!empty($res['order_amount'])){
					   echo "<b>Order Value : </b>".$res['order_amount']." ".$res['currency']."<br />";
				   }				  
				   if(!empty($res['requirement'])){
					   echo "<b>Requirement : </b>".$res['requirement']."<br />";
				   }
				   if($res['reply_status']=='Y'){
					   echo '<span class="b red">Replied</span> &nbsp; | &nbsp;';
				   }
				   echo anchor_popup('sitepanel/enquiry/send_reply/'.$res['id'], '<span class="black b">Send Reply</span>', $atts);?> |
                   <?php echo anchor_popup('sitepanel/careers/sent_to_clients/'.$res['id'], '<span class="black b">Send to Clients</span>', $atts2);?>
                   </td>
            <td width="20%" class="text-center fs13"><?php echo $res['email'];?></td>
            <td width="40%" class="text-center fs13"><?php echo strip_tags($res['message']);?></td>
          <td width="10%" class="text-center fs13"><?php echo getDateFormat($res['receive_date'],2);?></td> 
          </tr>
           <?php
			 }?>
           <?php
		 if($page_links!=''){
			 ?>
           <tr><td colspan="4" align="center" height="30"><?php echo $page_links; ?></td></tr>
           <?php
		 }?> 
        </table>
      </div>
      <p class="mt15"><input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
     </p>
    </div>
     <?php echo form_close();
	 }else{
		 echo "<center><strong> No record(s) found !</strong></center>" ;
	 }?>
  </div>
</div>
<!--Body End--> 
<?php $this->load->view('includes/footer'); ?>