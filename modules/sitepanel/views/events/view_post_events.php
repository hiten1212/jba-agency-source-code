<?php $this->load->view('includes/header'); ?>  
   
<?php  
$back_link = 'sitepanel/events';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
  
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <?php //echo validation_message();
   echo error_message();
   echo form_open_multipart('sitepanel/events/post_events/');?>
   	
     <?php
			$default_params = array(
								'heading_element' => array(
								  'field_heading'=>"Title",
								  'field_name'=>"events_title",
								  'field_placeholder'=>"",
								  'exparams' => 'size="40"'
								),
								'url_element'  => array(
								  'field_heading'=>"Page URL",
								  'field_name'=>"friendly_url",
								  'field_placeholder'=>"Page URL",
								  'exparams' => 'size="40"',
								  'pre_seo_url' =>'',
								  'pre_url_tag'=>FALSE
								)
								
								);
			seo_add_form_element($default_params);	
			?> 
            <p class="form_title1"><span class="required">*</span> Date :</p>
    <div class="form_field form_field2"><input name="start_date" class="start_date1 " type="text"  style="padding:2px; width:133px;" value="<?php echo set_value('start_date');?>"><a href="#" class="start_date"><img src="<?php echo base_url();?>assets/developers/images/cal0.png" width="16" height="16" alt=""></a><?php echo form_error('start_date');?></div>
    <p class="clearfix"></p> 
    
    <?php /*?><p class="form_title1"><span class="required">*</span> Posted By :</p>
    <div class="form_field form_field2"><input name="posted_by" style="width:450px" class="form-control" value="<?php echo set_value('posted_by');?>"><?php echo form_error('posted_by');?></div>
    <p class="clearfix"></p> <?php */?>
    
    
    <p class="form_title1"><span class="required">*</span> Image :</p>
    <div class="form_field form_field2"><input name="events_image" type="file" class="txtbox" /> [ <?php echo $this->config->item('events.best.image.view');?> ]
       <?php echo form_error('events_image');?></div>
    <p class="clearfix"></p> 
        
  	    
    <p class="form_title1"><span class="required">*</span> Description :</p>
    <div class="form_field form_field2"> <textarea name="events_description" rows="5" cols="50" id="events_description" > <?php echo set_value('events_description');?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('events_description');?>
    </div>
    <p class="clearfix"></p>
    
    
     
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Post" class="btn1" />
      <input type="hidden" name="action" value="addevents" />
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
  
</div>
</div>
<?php
$default_date = date("Y-m-d");
$posted_start_date = $this->input->post('start_date');
?>
<script type="text/javascript">
  $(document).ready(function(){
	$('.btn_sbt2').on('click',function(e){
		e.preventDefault();
		$start_date = $('.start_date1:eq(0)').val();
		$end_date = $('.end_date1:eq(0)').val();
		$start_date = $start_date=='From' ? '' : $start_date;
		$end_date = $end_date=='To' ? '' : $end_date;
		$(':hidden[name="keyword2"]','#myform').val($('input[type="text"][name="keyword2"]').val());
		$(':hidden[name="start_date"]','#myform').val($start_date);
		$(':hidden[name="end_date"]','#myform').val($end_date);
		$("#myform").submit();
	});
	$('.start_date,.end_date').on('click',function(e){
	  e.preventDefault();
	  cls = $(this).hasClass('start_date') ? 'start_date1' : 'end_date1';
	  $('.'+cls+':eq(0)').focus();
	});
	$( ".start_date1").on('focus',function(){
			$(this).datepicker({
			showOn: "focus",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			defaultDate: 'y',
			buttonText:'',
			minDate:'<?php echo $default_date;?>' ,
			maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+5 years"));?>',
			yearRange: "c-100:c+100",
			buttonImageOnly: true,
			onSelect: function(dateText, inst) {
						  $('.start_date1').val(dateText);
						  $( ".end_date1").datepicker("option",{
							minDate:dateText ,
							maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+180 days"));?>',
						});

					  }
		});
	});
	$( ".end_date1").on('focus',function(){
			$(this).datepicker({
					  showOn: "focus",
					  dateFormat: 'yy-mm-dd',
					  changeMonth: true,
					  changeYear: true,
					  defaultDate: 'y',
					  buttonText:'',
					  minDate:'<?php echo $posted_start_date!='' ? $posted_start_date :  $default_date;?>' ,
					  maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+180 days"));?>',
					  yearRange: "c-100:c+100",
					  buttonImageOnly: true,
					  onSelect: function(dateText, inst) {
						$('.end_date1').val(dateText);
					  }
				  });
	  });

  });
</script>
<?php $this->load->view('includes/footer'); ?>