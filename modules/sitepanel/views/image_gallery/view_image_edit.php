<?php $this->load->view('includes/header'); ?>  

<?php  $back_link = 'sitepanel/image_gallery';

echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>



<!--Breadcrumb End-->



<!--Body-->

<div class="container-fluid">

<div class="mid_area">

    

  <div class="mt10 p8">

  

  <div class="box_style">

	<div class="recent-table p10 mb15">

          <?php echo $heading_title; ?><span class="white" style="float:right"><?php echo anchor("sitepanel/image_gallery/",'Cancel','class="btn1" ' );?></span>

        </div>

       <?php echo validation_message('');?>

<?php echo error_message(); ?> 

<?php echo form_open_multipart(current_url_query_string());?>  

   

     <p class="form_title1"><span class="required">*</span> Gallery Title :</p>

    <div class="form_field form_field2"><input name="title" type="text" style="width:450px" class="form-control" value="<?php echo set_value('title',$res['title']);?>"></div>

    <p class="clearfix"></p>
	
     <p class="form_title1"><span class="required">*</span> Gallery Image :</p>

    <div class="form_field form_field2"><input name="gallery_file" id="gallery_file" type="file" style="width:450px" class="form-control">

	<?php

		 $j=1;

		 $product_path = "image_gallery/".$res['gallery_file'];

		?>

         <a href="#"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a>

         <div id="dialog_<?php echo $j;?>" title="Gallery Image" style="display:none;">

         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  />

         </div>

         

         <br />

   <?php echo $this->config->item('gallery.best.image.view');?>

         </div>

    <p class="clearfix"></p>   

    

    <p class="form_title1"></p>

    <div class="form_field form_field">

    <input type="submit" name="sub" value="Update" class="btn1" />

		<input type="hidden" name="action" value="edit" />

   

       </div>

    <p class="clearfix"></p>

  <?php echo form_close(); ?>  

  </div>

  </div>

  

</div>

</div>

<?php $this->load->view('includes/footer'); ?>