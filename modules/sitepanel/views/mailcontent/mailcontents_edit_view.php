<?php $this->load->view('includes/header'); ?> 
 <?php  $back_link = 'sitepanel/mailcontents';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>

<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?>
        </div>
       <?php echo validation_message();
	    echo error_message(); ?>
     
   <?php echo form_open(current_url_query_string());?> 
   
    <div class="red " style="margin-left:100px;"><strong>* Please do not change the variables enclosed with { } .</strong></div>
    <p class="clearfix"></p>
     
  	<p class="form_title1">Section :</p>
    <div class="form_field form_field2"><strong><?php echo $pageresult->email_section;?></strong></div>
    <p class="clearfix"></p>
   
   <p class="form_title1">Mail Subject <span class="required">*</span> :</p>
    <div class="form_field form_field2"><input name="email_subject" size="40" type="text" style="width:450px" class="form-control" value="<?php echo set_value('email_subject',$pageresult->email_subject);?>"><?php
			echo display_ckeditor($ckeditor); ?></div>
    <p class="clearfix"></p>
    
    <div class="form_title1"><span class="required">*</span> Mail Contents :</div>
    <div class="form_field form_field2">
    <textarea name="email_content" rows="5" cols="50" id="mail_content" class="form-control"><?php echo set_value('email_content',$pageresult->email_content);?></textarea>
			<?php
			echo display_ckeditor($ckeditor); ?>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Update" class="btn1" />
			<input type="hidden" name="id" value="<?php echo $pageresult->id;?>" />
          </div>
    <p class="clearfix"></p>
    <?php echo form_close(); ?>
  </div>
  </div>
  
</div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer'); ?>