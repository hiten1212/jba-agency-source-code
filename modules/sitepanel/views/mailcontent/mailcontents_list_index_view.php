<?php $this->load->view('includes/header'); ?>   
<?php 
echo admin_breadcrumb('Mail Contents'); ?>
<!--Breadcrumb End--> 

<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
   
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:15px;">
    <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize(); } </script> 
      
       <?php  echo error_message(); ?>
		<?php echo form_open("sitepanel/mailcontents/",'id="pagingform" method="get" '); ?>
    
    <?php /*?><hr style="margin-bottom:5px; margin-top:5px;"><?php */?>
    <div>
      
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div><p class="clearfix"></p>
    </div>
    <div class="clearfix"></div>
    	 <?php echo form_close();
    
      validation_message();
     
	 if( count($pagelist) > 0 ){ 
	 ?>
		<?php echo form_open("sitepanel/mailcontents/",'id="data_form"');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll box_h_235">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
             <!-- <td width="3%">Sl.</td>-->
              <td width="25%" class="fs14">Section</td>
              <td width="12%" class="text-center fs13">Details</td>
              <?php /*?><td width="14%" class="text-center fs13">Current Status</td><?php */?>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <?php		  
		    $srlno=0; 		
			foreach($pagelist as $catKey=>$pageVal){ 			  
		    $srlno++; 
		   ?> 
          <tr>
            <?php /*?><td width="3%"><?php echo $i;?></td><?php */?>
            <td width="25%" class="fs14"><?php echo $pageVal['email_section'];?></td>
            <td width="12%" class="text-center fs13"> <a href="#"  onclick="$('#dialog_<?php echo $pageVal['id'];?>').dialog({ width: 650 });">view</a>              
			  <div id="dialog_<?php echo $pageVal['id'];?>" title="Mail Content" style="display:none;">
			    <?php echo $pageVal['email_content'];?>
               </div>  
                         </td>
           <?php /*?> <td width="14%" class="text-center fs13"><span title="<?php echo ($val['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($val['status']==1)? "green2":"red";?> fs20 mt8"></span></td><?php */?>
            <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/mailcontents/edit/$pageVal[id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>
          </tr>
           <?php
			}
		
		  ?>
          <?php
		if($page_links!='')
		{
		?>
        <tr><td colspan="5" align="right" height="30"><?php echo $page_links; ?></td></tr>
            
         <?php
		}
		?>
        </table>
      </div>
      
    </div>
   	<?php echo form_close(); ?>
 <?php 
  }else{
	  
    echo "<center><strong> No record(s) found !</strong></center>" ;
	
 }
?>     
  </div>
</div>
<!--Body End--> 

<?php $this->load->view('includes/footer'); ?>