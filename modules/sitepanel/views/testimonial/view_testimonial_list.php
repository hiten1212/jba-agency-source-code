<?php $this->load->view('includes/header');
echo admin_breadcrumb($heading_title); ?>

<!--Breadcrumb End--> 
<!--Body-->

<div class="container-fluid">
 <div class="mid_area">

  <h1 class="fl">  <?php echo $heading_title;?></h1>

  <p class="add-product"><?php //echo anchor("sitepanel/testimonial/post","Post Testimonial",'' );?></p>

  <p class="clearfix"></p>
  <hr style="margin-bottom:5px; margin-top:10px;">
  <?php
  if(error_message() !=''){
	  echo error_message();
  }

  echo form_open("sitepanel/testimonial/",'id="search_form" method="get" '); ?>
  <div class="search-section">

      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Name">
      </p>
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
       <p class="serch-sec2">
   <?php 
      	if($this->input->get_post('keyword')!='')
				{ 
				 echo anchor("sitepanel/testimonial/",'<span>Clear Search</span>','class="form-control"');
				} 
				?>
      </p>

      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div>

      <p class="clearfix"></p>
    </div>
  <hr style="margin-bottom:5px; margin-top:5px;">

  <div class="clearfix"></div>
  <?php echo form_close();

  if(is_array($res) && ! empty($res)){
	  echo form_open("sitepanel/testimonial/",'id="data_form"');?>

	  <div class="p8" id="my_data">
	   <div class="box_style mob_scroll ">
	    <div class="recent-table">
	     <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>
	       <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
	       <td width="30%" class="fs14">Poster</td>
	       <td width="44%" class="text-left fs13">Description</td>
	       <td width="12%" class="text-center fs13">Current Status</td>
	       <td width="10%" class="text-center fs13">Action</td>
	      </tr>
	     </table>
	    </div>
	    <p class="clearfix"></p>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <?php    
		   foreach($res as $catKey=>$pageVal){
			   $photo = $pageVal['photo']?$pageVal['photo']:"no_image.png";
			      ?>
		     <tr>
		      <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['testimonial_id'];?>" /></td>
		      <td width="30%" class="fs14" align="left">
		      <?php /* <div class="fl p5">
               <img src="<?php echo base_url().'uploaded_files/testimonial/'.$photo;?>" width="60" class="" start="" />
               </div>*/?>
               <div class="fl p5">
               Name : <?php echo ucwords($pageVal['poster_name']);?>
		       <?php if($pageVal['email']!=''){
			       echo "<br />Email : ".$pageVal['email'];
		       }?>

               </div> 
		      </td>

		      <td width="44%" class="text-left fs13"><?php echo $pageVal['testimonial_description'];?></span></td>
		      <td width="12%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
		      <td width="10%" class="text-center grey"><?php echo anchor("sitepanel/testimonial/edit/$pageVal[testimonial_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>

	     </tr>
		     <?php
	     }

	     if($page_links!=''){
		     ?>
		     <tr><td colspan="5" align="center" height="30"><?php echo $page_links; ?></td></tr>
		     <?php
	     }?>
	    </table>
	   </div>
	   <p class="mt15">
	    <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>

    <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>

    <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onclick="return validcheckstatus('arr_ids[]','delete','Record');"/>

   </p>
	  </div>
	  <?php
	  echo form_close();

  }else{
	  echo "<center><strong> No record(s) found !</strong></center>" ;

  }?>
 </div>
</div>
<!--Body End--> 
<?php $this->load->view('includes/footer'); ?>