<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/testimonial';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>

<div class="container-fluid">
 <div class="mid_area">
  <div class="mt10 p8">
   <div class="box_style">
    <div class="recent-table p10 mb15"><?php echo $heading_title;?> <span class="white" style="float:right"> <?php echo anchor("sitepanel/testimonial",'<span>Cancel</span>','class="btn1"');?></span></div>
    <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart("sitepanel/testimonial/post");?>
    <?php /*?> <p class="form_title1"><span class="required">*</span> Title :</p>
    <div class="form_field form_field2"><input name="testimonial_title" size="40" style="width:450px" type="text" class="form-control" value="<?php echo set_value('testimonial_title');?>"></div>
    <p class="clearfix"></p><?php */?>
    <?php
    $default_params = array(
    'heading_element' => array(
    'field_heading'=>"Name",
    'field_name'=>"poster_name",
    'field_placeholder'=>"",
    'exparams' => 'size="40"'
    ),
    'url_element'  => array(
    'field_heading'=>"Page URL",
    'field_name'=>"friendly_url",
    'field_placeholder'=>"Your Page URL",
    'exparams' => 'size="40"',
    'pre_seo_url' =>'',
    'pre_url_tag'=>FALSE
    )
    );
    seo_add_form_element($default_params);
    ?>
    <p class="form_title1"><span class="required">*</span> Email :</p>
    <div class="form_field form_field2"><input name="email" size="40" style="width:450px" type="text" class="form-control" value="<?php echo set_value('email');?>"><?php echo form_error('email');?></div>
    <p class="clearfix"></p>
    
    <?php /*?><p class="form_title1"><span class="required"></span> Photo :</p>
    <div class="form_field form_field2"><input name="photo" size="40" style="width:450px" type="file" class="form-control"><?php echo form_error('photo');?></div>
    <p class="clearfix"></p>     <?php */?>

    <p class="form_title1"><span class="required">*</span> Comment :</p>
    <div class="form_field form_field2"><textarea name="testimonial_description" class="form-control" rows="5" style="width:450px;" id="testimonial_description" ><?php echo set_value('testimonial_description');?></textarea> <?php echo form_error('testimonial_description');?><?php /*echo display_ckeditor($ckeditor);*/?> </div>
    <p class="clearfix"></p>    

    <p class="form_title1"></p>
    <div class="form_field form_field"><input type="submit" name="sub" value="Post" class="btn1" /></div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
   </div>
  </div>
 </div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer');?>