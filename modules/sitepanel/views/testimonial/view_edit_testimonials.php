<?php $this->load->view('includes/header');?>

<!--Breadcrumb-->

<?php  $back_link = 'sitepanel/testimonial';

echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>

<div class="container-fluid">

 <div class="mid_area">

  <div class="mt10 p8">

   <div class="box_style">

    <div class="recent-table p10 mb15"><?php echo $heading_title;?> <span class="white" style="float:right"> <?php echo anchor("sitepanel/testimonial",'<span>Cancel</span>','class="btn1"');?></span></div>

    <?php $res=$res[0]; //echo validation_message();

    error_message();

    echo form_open_multipart("sitepanel/testimonial/edit/".$res['testimonial_id']);?>

    <p class="form_title1"><span class="required">*</span> Name * :</p>

    <div class="form_field form_field2"><input name="poster_name" size="40" style="width:450px" type="text" class="form-control" value="<?php echo set_value('poster_name',$res['poster_name']);?>"></div>

    <p class="clearfix"></p>

    <?php /*

    $default_params = array(

    'heading_element' => array(

    'field_heading'=>"Name",

    'field_name'=>"poster_name",

    'field_value'=>$res['poster_name'],

    'field_placeholder'=>"",

    'exparams' => 'size="40"'

    ),

    'url_element'  => array(

    'field_heading'=>"Page URL",

    'field_name'=>"friendly_url",

    'field_value'=>$res['friendly_url'],

    'field_placeholder'=>"Your Page URL",

    'exparams' => 'size="40"',

    )

    );

    seo_edit_form_element($default_params);
*/
    ?>

    <p class="form_title1"><span class="required">*</span> Email :</p>

    <div class="form_field form_field2"><input name="email" size="40" style="width:450px" type="text" class="form-control" value="<?php echo set_value('email',$res['email']);?>"><?php echo form_error('email');?></div>

    <p class="clearfix"></p>         



    <?php /* <p class="form_title1">Photo <span class="required"></span> :</p>

    <div class="form_field form_field2"><input name="photo" type="file" class="form-control" style="width:450px;"><?php if($res['photo']!='' && file_exists(UPLOAD_DIR."/testimonial/".$res['photo'])){?><a href="javascript:void(0);"  onclick="$('#dialog').dialog({width:'auto'});">View</a> | <input type="checkbox" name="photo_img_delete" value="Y" /> Delete <br /><?php }?><div id="dialog" title="Photo" style="display:none;"> <img src="<?php echo base_url().'uploaded_files/testimonial/'.$res['photo'];?>"  /> </div></div> 

     <p class="clearfix"></p>*/?>



    <p class="form_title1"><span class="required">*</span> Comment :</p>

    <div class="form_field form_field2"><textarea name="testimonial_description" class="form-control" rows="5" style="width:450px;" id="testimonial_description" ><?php echo set_value('testimonial_description',$res['testimonial_description']);?></textarea> <?php echo form_error('testimonial_description');?><?php /*echo display_ckeditor($ckeditor);*/?> </div>

    <p class="clearfix"></p>



    <p class="form_title1"></p>

    <div class="form_field form_field"><input type="submit" name="sub" value="Update" class="btn1" /></div>

    <p class="clearfix"></p>

    <?php echo form_close();?>

   </div>

  </div>

 </div>

</div>

<!--Body End-->

<?php $this->load->view('includes/footer');?>