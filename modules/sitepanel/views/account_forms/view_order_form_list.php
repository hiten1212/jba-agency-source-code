<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php 


echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End-->
<!--Body-->

<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title;?></h1>
    <p class="add-product">&nbsp;</p>
    <p class="clearfix"></p>
    <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize(); } </script>
    <hr style="margin-bottom:5px; margin-top:10px;">
    <?php if(error_message() !='') echo error_message();?>
    <?php echo form_open("sitepanel/account_forms",'id="search_form" method="get"');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Name">
      </p>
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php if( $this->input->get_post('keyword')!='' ) echo anchor("sitepanel/account_forms",'<span>Clear Search</span>','class="form-control"');?>
      </p>
      <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?> </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div class="clearfix"></div>
    <?php echo form_close();
   if( is_array($pagelist) && !empty($pagelist)){
	   echo form_open("sitepanel/account_forms",'id="data_form" ');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="25%" class="fs14">Name</td>
             
              <td width="14%" class="text-center fs13">Post Date</td>
              <td width="15%" class="text-center fs13">Details</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php
	     		foreach($res as $catKey=>$pageVal){
		     ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['order_id'];?>" /></td>
            <td width="25%" class="fs14"><?php echo $pageVal['first_name'];?> <br />
            </td>
           
            <td width="14%" class="text-center fs13"><?php echo getDateFormat($pageVal['receive_date'],'1')?></td>
            <td width="15%" class="text-center grey"><a href="<?php echo base_url().'uploaded_files/order_pdf/'.$pageVal['order_pdf_file']?>" target="_blank">View Details</a>
            
            <?php if($pageVal['drawing_sheet']!='' && file_exists(UPLOAD_DIR."/drawing_sheet/".$pageVal['drawing_sheet'])){?>&nbsp;|&nbsp;
            <a href="<?php echo base_url().'uploaded_files/drawing_sheet/'.$pageVal['drawing_sheet']?>" target="_blank">View Drawing Sheet</a>
            
            <?php }?>
            
            </td>
          </tr>
          <?php
	     }?>
          <?php
		 if($page_links!=''){
			 ?>
          <tr>
            <td colspan="5" align="center" height="30"><?php echo $page_links; ?></td>
          </tr>
          <?php }?>
        </table>
      </div>
      <p class="mt15">
         
        <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
      </p>
    </div>
    <?php echo form_close();


   }else{


	   echo "<center><strong> No record(s) found !</strong></center>";


   }?> </div>
</div>
<?php $this->load->view('includes/footer');?>
