<?php $this->load->view('includes/header'); ?> 

 <?php  $back_link = 'sitepanel/staticpages';

echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>



<!--Breadcrumb End-->



<!--Body-->

<div class="container-fluid">

<div class="mid_area"><!--

  <h1>Edit Product</h1>-->

    

  <div class="mt10 p8">

  

  <div class="box_style">

	<div class="recent-table p10 mb15">

          <?php echo $heading_title; ?>

        </div>

       <?php echo validation_message();

	    echo error_message(); ?>

     

   <?php echo form_open_multipart(current_url_query_string());?>  


 <p class="form_title1"><span class="required">*</span> Title :</p>
 <div class="form_field form_field2"><input type="text" name="page_name" size="50" value="<?php echo set_value('page_name',$pageresult['page_name']);?>"><?php echo form_error('page_name');?>  </div>
    <p class="clearfix"></p> 
    
    <p class="form_title1">Background :</p>
    <div class="form_field form_field2"><input name="title_background" type="color"  value="<?php echo set_value('title_background',$pageresult['title_background']);?>"><?php echo form_error('page_name');?>  </div>
    <p class="clearfix"></p>
  	
	<p class="form_title1"><span class="required"></span> Image :</p>
    <div class="form_field form_field2"><input type="file" name="image" />
	  <?php echo form_error('image');?>
     
      <br />
<?php



   if($pageresult['image']!='' && file_exists(UPLOAD_DIR."/staticimage/".$pageresult['image'])){



	   ?>



	   <a href="#"  onclick="$('#dialog').dialog({width:'auto',height:'auto'});">View</a> <!--| <input type="checkbox" name="static_img_delete" value="Y" /> Delete -->



	   <?php	



   }?>


	  [ ( File should be .jpg, .png, .gif format and file size should not be more then 2 MB (2048 KB)) ( Best image size <?php echo $pageresult['img_size']; ?> ) ]
     <div id="dialog" title="gallery Image" style="display:none;">
  <img src="<?php echo base_url().'uploaded_files/staticimage/'.$pageresult['image'];?>"  /> </div>
    </div>
    <p class="clearfix"></p>

    <?php /* if($pageresult['page_short_description']!=''){	 ?>

    <div class="form_title1">* Heading :</div>

    <div class="form_field form_field2">

      <textarea name="page_short_description" rows="1" cols="80" class="form-control"><?php echo $pageresult['page_short_description'];?></textarea></textarea>

    </div>

    <p class="clearfix"></p>

    <?php

	 }*/
	 
	 /*if($pageresult['page_id'] == 1){
	 ?>	
    <div class="form_title1"><span class="required">*</span> Short Description :</div>
    <div class="form_field form_field2">
    <textarea name="page_short_description" rows="5" cols="50" id="short_desc" class="form-control"><?php echo $pageresult['page_short_description'];?></textarea>
			<?php
			echo display_ckeditor($ckeditor2); ?>
    </div>
    <p class="clearfix"></p>
    <?php
	 }*/
	 ?>
	 <div class="form_title1"><span class="required"></span> Description :</div>
    <div class="form_field form_field2">
  <textarea name="page_description" rows="5" cols="50" id="page_desc" class="form-control"><?php echo $pageresult['page_description']; ?></textarea>
			<?php echo display_ckeditor($ckeditor); ?>
    </div>
    <p class="clearfix"></p>

    

    <p class="form_title1"></p>

    <div class="form_field form_field">

    <input type="submit" name="sub" value="Update" class="btn1" />

			<input type="hidden" name="id" value="<?php echo $pageresult['page_id'];?>" />

          </div>

    <p class="clearfix"></p>

    <?php echo form_close(); ?>

  </div>

  </div>

  

</div>

</div>

<!--Body End-->

<?php $this->load->view('includes/footer'); ?>