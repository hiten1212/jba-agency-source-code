<?php $this->load->view('includes/header'); ?>
<!--Breadcrumb-->
<?php echo admin_breadcrumb('Manage Home Content'); ?>
<!--Breadcrumb End-->
<!--Body-->

<div class="container-fluid">
    <div class="mid_area">
        <h1 class="fl"> <?php echo $heading_title; ?></h1>
        <p class="clearfix"></p>
        <hr style="margin-bottom:5px; margin-top:15px;">
        <script type="text/javascript">function serialize_form() {
                return $('#pagingform').serialize();
            }</script>
        <?php echo error_message(); ?>
        <?php echo form_open("sitepanel/staticpages/", 'id="search_form" method="get" '); ?>
        <div class="search-section">
            <p class="serch-sec">Search</p>
            <p class="serch-sec1">
                <input name="keyword" value="<?php echo $this->input->get_post('keyword'); ?>" type="text" class="form-control" placeholder="Page Name">
            </p>
            <p class="serch-sec6">
                <input name="search" type="submit" value="Submit" class="btn1">
            </p>
            <p class="serch-sec2">
                <?php
                if ($this->input->get_post('keyword') != '') {


                    echo anchor("sitepanel/staticpages/", '<span>Clear Search</span>', 'class="form-control"');
                }
                ?>
            </p>
            <p class="clearfix"></p>
        </div>
        <hr style="margin-bottom:5px; margin-top:5px;">
        <div>
            <div class="records fs14"> Records Per Page : <?php echo display_record_per_page(); ?> </div>
            <p class="clearfix"></p>
        </div>
        <div class="clearfix"></div>
                <?php echo form_close(); ?>
<?php
if (is_array($pagelist) && !empty($pagelist)) {


    echo form_open("sitepanel/staticpages/", 'id="data_form" ');
    ?>
            <div class="p8" id="my_data">
                <div class="box_style mob_scroll ">
                    <div class="recent-table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="3%">Sl.</td>
                                <td width="25%" class="fs14">Page Name</td>
                                <td width="12%" class="text-center fs13">Details</td>
            <?php /* ?><td width="14%" class="text-center fs13">Current Status</td><?php */ ?>
                                <td width="15%" class="text-center fs13">Action</td>
                            </tr>
                        </table>
                    </div>
                    <p class="clearfix"></p>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <?php
            $i = $offset;


            foreach ($pagelist as $val) {


                $i++;
                ?>
                            <tr>
                                <td width="3%"><?php echo $i; ?></td>
                                <td width="25%" class="fs14"><?php echo $val['page_name']; ?></td>
                                <td width="12%" class="text-center fs13"><a href="javascript:void();" onclick="$('#dialog_<?php echo $val['page_id']; ?>').dialog({width: 650});">View</a>
                                    <div id="dialog_<?php echo $val['page_id']; ?>" title="Description" style="display:none;">
                                        <img src="<?php echo base_url() . 'uploaded_files/staticimage/' . $val['image']; ?>"  /><br>
        <?php echo $val['page_description']; ?> </div></td>
                            <?php /* ?> <td width="14%" class="text-center fs13"><span title="<?php echo ($val['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($val['status']==1)? "green2":"red";?> fs20 mt8"></span></td><?php */ ?>
                                <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/staticpages/edit/$val[page_id]/" . query_string(), '<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>', 'title="Edit"'); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        if ($page_links != '') {
                            ?>
                            <tr>
                                <td colspan="6" align="right" height="30"><?php echo $page_links; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
    <?php
    echo form_close();
} else {


    echo "<center><strong> No record(s) found !</strong></center>";
}
?>

    </div>
    
<div class="mid_area">
    <div class="mt10 p8">
    <div class="box_style">
        <div class="recent-table p10 mb15">
           Banner Video & Slogan Setting        </div>
        <?php echo validation_message();
   echo error_message();
   echo form_open_multipart('sitepanel/setting/editbannersettings/');?>
            <p class="form_title1"><span class="required">*</span>Banner Slogan :</p>
            <div class="form_field form_field2"><input name="banner_slogan" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('banner_slogan',$admin_info->banner_slogan);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
            <p class="clearfix"></p> 
            
            <p class="form_title1">Banner Text Color :</p>
            <div class="form_field form_field2"><input name="banner_text_color" type="color"  value="<?php echo set_value('banner_text_color',$admin_info->banner_text_color);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
            <p class="clearfix"></p> 

          <p class="form_title1"><span class="required">*</span> Header Video :</p>
	    <div class="form_field form_field2">
	     <input type="file" name="header_video" class="form-control" style="width:450px;"/>
	     <?php
	     if($admin_info->header_video!='' && file_exists(UPLOAD_DIR."/logo/".$admin_info->header_video)){
		     ?>
		     <a href="javascript:void(0);"  onclick="$('#dialog3').dialog({width:'auto'});">View</a>
		     <?php
	     }?>
	     <br />[ <?php echo $this->config->item('slide.best.video.view');?> ] 
	     <div id="dialog3" title="Header Logo" style="display:none;"> 
		 <video width="" muted="" autoplay="" loop="">
                                  <source src="<?php echo base_url().'uploaded_files/logo/'.$admin_info->header_video;?>" type="video/mp4">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.ogg" type="video/ogg">
                                  Your browser does not support HTML5 video.
                                </video>
		 
		  </div>
	    </div> 
            <p class="clearfix"></p>
            
            <p class="form_title1">Video ON/OFF :</p>
            <div class="form_field form_field2"><input type="checkbox" name="video_on_off" value="1" <?php echo $admin_info->video_on_off== 1 ? "checked" : ""; ?>/></div>
            <p class="clearfix"></p> 


            <p class="form_title1"></p>
            <div class="form_field form_field">
                <input type="submit" class="btn1" value="Update Info">
            </div>
            <p class="clearfix"></p>
        <?php echo form_close();?>
    </div>
</div>
</div>

</div>
<!--Body End-->
        <?php $this->load->view('includes/footer'); ?>
