<div class="p10 pt5 pb5">
 <h1><?php echo $res['title'];?></h1>
 <?php
 $youtubeid=get_Youtube_Id($res['embed_code']);
 $embed_code = $youtubeid; 
 $embed_link = "https://www.youtube.com/embed/";
 if (strstr($embed_code, "https://youtu.be/")) {
	   $embed = str_replace("https://youtu.be/","",$embed_code);
 }else{
	   $embed = str_replace("https://www.youtube.com/watch?v=","",$embed_code);
 }
 $embed_link.=$embed;  
 ?>
 <div><iframe width="100%" height="365" src="<?php echo $embed_link;?>" frameborder="0" allowfullscreen></iframe></div>
</div>