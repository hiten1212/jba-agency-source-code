<?php $this->load->view('includes/header'); ?>  
<?php  $back_link = 'sitepanel/team';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area"> 
  <div class="mt10 p8">
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <?php //echo validation_message();
   echo error_message();
   echo form_open_multipart(current_url_query_string());?>
    <?php
			$default_params = array(
								'heading_element' => array(
									  'field_heading'=>$heading_title,
									  'field_name'=>"team_title",
									  'field_value'=>$res['team_title'],
									  'field_placeholder'=>"",
									  'exparams' => 'size="40"'
									),
									  'url_element'  => array(
									  'field_heading'=>"Page URL",
									  'field_name'=>"friendly_url",
									  'field_value'=>$res['friendly_url'],
									  'field_placeholder'=>"Your Page URL",
									  'exparams' => 'size="40"',
								   )

						  );
			seo_edit_form_element($default_params);	
			?>
  	<p class="form_title1"><span class="required">*</span> Image :</p>
    <div class="form_field form_field2"><input name="team_image" type="file" /> 
      <img  src="<?php echo get_image('team',$res['team_image'],40,40,'AR');?>" style="vertical-align:middle;" />[ <?php echo $this->config->item('team.best.image.view');?> ]
         <?php echo form_error('team_image');?></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Post :</p>
    <div class="form_field form_field2"><input name="designation" type="text" size="40" value="<?php echo set_value('designation',$res['designation']); ?>" />  <?php echo form_error('designation');?></div>
    <p class="clearfix"></p> 

   <?php /*?> <p class="form_title1"><span class="required">*</span> Description :</p>
    <div class="form_field form_field2"> <textarea name="team_description" rows="5" cols="50" id="team_description" ><?php echo set_value('team_description',$res['team_description']);?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('team_description');?>
    </div><?php */?>

    <p class="clearfix"></p> 
    <p class="form_title1"></p>
    <div class="form_field form_field">
   <input type="submit" name="sub" value="Edit" class="btn1" />
    <input type="hidden" name="team_id" id="pg_recid" value="<?php echo $res['team_id'];?>">
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
</div>
</div>
<?php $this->load->view('includes/footer'); ?>