<?php $this->load->view('includes/header');

$block_path=$inq_type=='Wholesale' ? "enquiry/wholesale/" : "enquiry/";
$type=$this->uri->segment(3);
?>
<!--Breadcrumb-->
<?php 

echo admin_breadcrumb($heading_title); ?>

<!--Breadcrumb End--> 
<!--Body-->
<div class="container-fluid">
  <div class="mid_area">

    <h1 class="fl"> <?php echo $heading_title; ?></h1>

    <p class="clearfix"></p>

    <hr style="margin-bottom:5px; margin-top:15px;">
    <?php echo error_message();
   echo form_open("",'id="search_form"');?>
    <div class="search-section">

      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Name, Email">
      </p>

      <p class="serch-sec6">
        <input name="search" type="submit" value="Submit" class="btn1">

      </p>
      <p class="serch-sec2">
      <?php

      if($this->input->get_post('keyword')!=''){
	      echo anchor("sitepanel/enquiry/".$this->uri->rsegment(2),'<span class="form-control">Clear Search </span>' );
      }?>
      </p>
      <p class="clearfix"></p>
    </div>

    <hr style="margin-bottom:5px; margin-top:5px;">
    <div>

      <div class="records fs14"> Records Per Page :
       <?php echo display_record_per_page();?>

      </div><p class="clearfix"></p>
    </div>

    <div class="clearfix"></div>
     <?php echo form_close();
   if( is_array($res) && !empty($res) ){
	   echo form_open("",'id="data_form"');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', checked);"></td>
              <td width="35%" class="fs14">User Info</td>

              <td width="30%" class="text-center fs13">Email</td>

              
              <td width="30%" class="text-center fs13"><?php echo $hd = $inq_type == 'callback' ? 'Best Time to Call' : 'Comment Details';?></td>  

            </tr>

          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php
	     $atts = array(
				'width'      => '600',
				'height'     => '400',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
			 );

			 foreach($res as $catKey=>$res){

				 $address_details=array(); ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $res['id'];?>" /></td>

            <td width="35%" class="fs14"><?php echo ucwords($res['first_name'].' '.$res['last_name']);?> <br>
			  <?php
				   if($res['address']!="" && $res['address']!='0'){
					   $address_details[]="<b>Address : </b>".nl2br($res['address']);

				   }
				   if($res['service_type']!="" && $res['service_type']!='0'){
					   $address_details[]="<b>Service : </b>".$res['service_type'];
				   }
				   
				    if($res['apply_for']!="" && $res['apply_for']!='0'){
					   $address_details[]="<b>Apply for : </b>".ucwords($res['apply_for']);
				   }
				   if($res['location']!="" && $res['location']!='0'){
					   $address_details[]="<b>Location : </b>".ucwords($res['location']);
				   }
				   if($res['state']!="" && $res['state']!='0'){
					   $address_details[]="<b>State : </b>".ucwords($res['state']);
				   }
				   if($res['city']!="" && $res['city']!='0'){
					   $address_details[]="<b>city : </b>".ucwords($res['city']);
				   }
				   if($res['country']!="" && $res['country']!='0'){
					   $address_details[]="<b>Country : </b>".ucwords($res['country']);
				   }
				   if($res['zipcode']!="" && $res['zipcode']!='0'){
					   $address_details[]="<b>Pincode : </b>".ucwords($res['zipcode']);
				   }

				   if($res['company_name']!="" && $res['company_name']!='0'){
				  $address_details[]="<b>Company : </b>".ucwords($res['company_name']);
				   }
				   if($res['phone_number']!="" && $res['phone_number']!='0'){
					   $address_details[]="<b>Phone : </b>".$res['phone_number'];
				   }

				   if($res['ext_number']!="" && $res['ext_number']!='0'){
					   $address_details[]="<b>Ext : </b>".$res['ext_number'];
				   }

				   if($res['mobile_number']!="" && $res['mobile_number']!='0'){
					   $address_details[]="<b>Mobile : </b>".$res['mobile_number'];
				   }

				   if($res['amount']!="" && $res['amount']!='0'){
					   $address_details[]="<b>Amount : </b>".$res['amount'];
				   }

				   if(!empty($address_details)){
					   echo implode("<br>",$address_details)."<br />";
				   }

				   if(!empty($res['product_service'])){
					   echo "<b>Product Title : </b>".$res['product_service']."<br />";
				   }
				   
				   if(!empty($res['receive_date'])){
					   echo "<b>Post Date : </b>".getDateformat($res['receive_date'],1)."<br />";
				   }
				   
				   if(!empty($res['relevant_experience'])){
					   echo "<b>Relevant Experience : </b>".$res['relevant_experience']."<br />";
				   }

				   

				   if($res['reply_status']=='Y'){
					   echo '<span class="b red">Replied</span> &nbsp; | &nbsp;';

			   }
				   echo anchor_popup('sitepanel/enquiry/send_reply/'.$res['id'], '<span class="red">Send Reply</span>', $atts);?></td>
            <td width="30%" class="text-center fs13"><?php echo $res['email'];?></td>
           
             <td width="30%" class="text-center fs13"><?php echo strip_tags($res['message']);?></td>
            
          </tr>
           <?php
			 }?>
           <?php
		 if($page_links!=''){
			 ?>
           <tr><td colspan="4" align="right" height="30"><?php echo $page_links; ?></td></tr>



           <?php



		 }?> 



        </table>



      </div>



      <p class="mt15">
	  
	  <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>



     </p>



    </div>



     <?php echo form_close();



	 }else{



		 echo "<center><strong> No record(s) found !</strong></center>" ;



	 }?>



  </div>



</div>



<!--Body End--> 



<?php $this->load->view('includes/footer'); ?>