<?php $this->load->view('includes/header');
$pcatID =($this->uri->segment(4) > 0)? $this->uri->segment(4):"0";
$pcatID = (int) $pcatID;
$url_prm=''; if($this->uri->segment('4')!=''){ $url_prm='?category_id='.$this->uri->segment('4'); }
?>
<?php  $back_link = 'sitepanel/products'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->   

  <div class="mt10 p8"> 
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1">Add Product</a> <?php echo anchor("sitepanel/products".$url_prm,'Cancel','class="btn1" ' );?>
        </div> 
         <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
    <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart('sitepanel/products/add/',array('id'=>'form'));?>
     <div id="tab-general"> 
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
      <?php
       $selcatID = ($this->input->post('category_id')!='') ? $this->input->post('category_id'): $pcatID;
     $selcatID = (int) $selcatID;       	
      ?>
     <p class="form_title1"><span class="required">*</span> Select :</p>
    <div class="form_field form_field2">
     <?php $selected_category = array($selcatID);?>
    <select name="category_id" size="8" class="form-control" style="width:450px;" >
     <?php echo get_nested_dropdown_menu(0,$selected_category);?>
  </select><?php echo form_error('category_id');?></div>
    <p class="clearfix"></p>
    <?php
			$default_params = array(
				'heading_element' => array(
				'field_heading'=>"Name",
				'field_name'=>"product_name",
				'field_placeholder'=>"Your Project Name",
				'exparams' => 'size="40"'
				),
				'url_element'  => array(
				'field_heading'=>"Page URL",
				'field_name'=>"friendly_url",
				'field_placeholder'=>"Your Page URL",
				'exparams' => 'size="40"',
				'pre_seo_url' =>'',
				'pre_url_tag'=>FALSE
				)
			);
			seo_add_form_element($default_params);
			?> 
  	<p class="form_title1"><span class="required">*</span> Product Code :</p>
    <div class="form_field form_field2"><input name="product_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('product_code');?>" /><?php echo form_error('product_code');?></div>
    <p class="clearfix"></p>     
      
     <?php /*?><p class="form_title1"><span class="required">**</span> Price :</p>
     <div class="form_field form_field2"><input name="product_price" type="text" maxlength=8 style="width:450px;" class="form-control" value="<?php echo set_value('product_price');?>" /> Maximum of 5 digits<?php echo form_error('product_price');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1"><span class="required">**</span> Discounted Price :</p>
     <div class="form_field form_field2"><input name="product_discounted_price" type="text" maxlength=8 style="width:450px;" class="form-control" value="<?php echo set_value('product_discounted_price');?>" /> Maximum of 5 digits<?php echo form_error('product_discounted_price');?></div>
     <p class="clearfix"></p><?php */?>
    
    <p class="form_title1"><span class="required"></span> Youtube Video 1:</p>
    <div class="form_field form_field2"><input name="youtube_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube_code');?>" /><?php echo form_error('youtube_code');?></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required"></span> Youtube Video 2 :</p>
    <div class="form_field form_field2"><input name="youtube_code2" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube_code2');?>" /><?php echo form_error('youtube_code2');?></div>
    <p class="clearfix"></p>                 

    <div class="form_title1"><span class="required">*</span>Description :</div>
    <div class="form_field form_field2">
      <textarea name="products_description" rows="5" cols="50" class="form-control" id="description"><?php echo set_value('products_description');?></textarea><?php echo display_ckeditor($ckeditor1);?><?php echo form_error('products_description');?>
    </div>
    <p class="clearfix"></p>   
    <?php /*?>  
     <div class="form_title1">Core Module :</div>
    <div class="form_field form_field2">
      <textarea name="core_description" rows="5" cols="50" class="form-control" id="description1"><?php echo set_value('core_description');?></textarea><?php echo display_ckeditor($ckeditor2);?><?php echo form_error('core_description');?>
    </div>
    <p class="clearfix"></p> 
    
     <div class="form_title1">Related Links :</div>
    <div class="form_field form_field2">
      <textarea name="related_description" rows="5" cols="50" class="form-control" id="description2"><?php echo set_value('related_description');?></textarea><?php echo display_ckeditor($ckeditor3);?><?php echo form_error('related_description');?>
    </div>
    <p class="clearfix"></p>  <?php */?>  

     </div>

    <div id="tab-image"> 
    <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p> 
       <?php for($i=1;$i<=$this->config->item('total_product_images');$i++){?>
      <p class="form_title1"><span class="required">**</span> Image :</p>
    <div class="form_field form_field2"><input name="product_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">[ <?php echo $this->config->item('product.best.image.view');?> ] <?php echo form_error('product_images'.$i);?></div>
    <p class="clearfix"></p>
     <?php }?>    

    <p class="form_title1"> Alt Tag Text :</p>
    <div class="form_field form_field2"><input name="product_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('product_alt');?>" /><?php echo form_error('product_alt');?></div>
    <p class="clearfix"></p> 
    </div>   

    <p class="form_title1"></p>
    <div class="form_field form_field">
      <input type="hidden" name="action" value="add" />
   </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
</div>
</div>
<!--Body End-->
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//--></script>
<?php $this->load->view('includes/footer');?>