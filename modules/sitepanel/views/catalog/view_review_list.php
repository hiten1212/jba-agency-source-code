<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<div class="breadcrumb">
 <div class="container-fluid">
  <ol class="breadcrumb">
   <li><?php echo anchor('sitepanel/dashbord','Dashboard');?></li>
   <li><?php echo anchor('sitepanel/products','Services'); ?></li>
   <li class="active"><?php echo $heading_title; ?></li>
  </ol>
 </div>
</div>
<!--Breadcrumb End--> 

<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <h1 class="fl"> <?php echo $heading_title; ?></h1>
  
  <p class="clearfix"></p>
  <hr style="margin-bottom:5px; margin-top:10px;">
  <?php
  if(error_message() !=''){
	  echo error_message();
  }?>
  <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize();   } </script>
  <?php echo form_open("",'id="myform" ');?>
  <div>
   <p class="total-results red fs14 b"><?php echo $category_result_found; ?></p>
   <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?></div>
   <p class="clearfix"></p>
  </div>
  <div class="clearfix"></div>
  <?php echo form_close();
  if( is_array($res) && !empty($res)) {
	  echo form_open("sitepanel/products/review/".$this->uri->segment(4),'id="" ');
	  ?>
      
	    <p class="clearfix"></p>  
	  <div class="p8" id="my_data">
	   <div class="box_style mob_scroll ">
	    <div class="recent-table">
	     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
				<td width="68" style="text-align: center;">
                <input type="checkbox" onclick="$('input[name*=\'arr_ids\']').attr('checked', this.checked);" /></td>
				<td width="278"  class="left">Name/Email</td>
				<td width="401" class="left">Product Name</td>
				<td width="64" class="center">Rate</td>
				<!--<td width="319" class="left">Title</td>-->
				<td width="285" class="left">Comment</td>
				<td width="131" class="center">Post Date.</td>
				<td width="86" class="center">Status</td>
			  </tr>
	     </table>
	    </div>
	    <p class="clearfix"></p>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <?php
	      $i=0;
	      foreach($res as $catKey=>$pageVal){
		      $i++;
		      ?>
		    <tr>
					<td width="5%" style="text-align: center;">
                    <input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['review_id'];?>" /></td>
					<td width="21%" valign="top" class="left"> 
					<?php echo $pageVal['poster_name'];?> <br/>                   
                    <?php echo $pageVal['email'];?>  
					</td>
					<td width="31%" valign="top" class="left"> <?php echo $pageVal['product_name'];?> </td>
					<td width="5%" valign="top" class="center"><?php 
					 echo $pageVal['rate'];
					//echo rating_html($pageVal['rate'],5);?></td>
					<!--<td class="left" valign="top"><?php echo $pageVal['review_title'];?></td>-->
                    
					<td width="21%" valign="top" class="left"><?php echo $pageVal['comment'];?></td>
					<td width="11%" valign="top" class="center"><?php echo getDateFormat($pageVal['posted_date'],1);?></td>
					<td width="6%" valign="top" class="center"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
				</tr>
		     <?php
	     }
	     if($page_links!=''){
		     ?>
		     <tr><td colspan="9" align="center" height="30"><?php echo $page_links; ?></td></tr>
		     <?php
	     }?>
	    </table>
	   </div>
	   <div class="mt15 ">
	    <p class=" mt10 serch-sec8 fl">
        <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onclick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
          <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onclick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
	     <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
         </p>
	   </div> 
	  </div>
	  <?php
	  echo form_close();
  }else{
	  echo "<center><strong> No record(s) found !</strong></center>" ;
  }?>
 </div>
</div>
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer');?>