<?php $this->load->view('includes/header');
$pcatID =($this->uri->segment(4) > 0)? $this->uri->segment(4):"0";
$pcatID = (int) $pcatID;
?>
<?php  $back_link = 'sitepanel/blogs'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Newsroom</h1>-->   

  <div class="mt10 p8"> 
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1">Add Blog</a> <?php echo anchor("sitepanel/blogs".$url_prm,'Cancel','class="btn1" ' );?>
        </div> 
         <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
    <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart('sitepanel/blogs/add/',array('id'=>'form'));?>
     <div id="tab-general"> 
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
      
    <?php
			$default_params = array(
				'heading_element' => array(
				'field_heading'=>"Name",
				'field_name'=>"blog_name",
				'field_placeholder'=>"Blogs Name",
				'exparams' => 'size="40"'
				),
				'url_element'  => array(
				'field_heading'=>"Page URL",
				'field_name'=>"friendly_url",
				'field_placeholder'=>"Page URL",
				'exparams' => 'size="40"',
				'pre_seo_url' =>'',
				'pre_url_tag'=>FALSE
				)
			);
			seo_add_form_element($default_params);
			?> 
    <?php /*?> <p class="form_title1"><span class="required"></span> Brochure Document :</p>
    <div class="form_field form_field2"><input type="file" name="brochure_doc" />
	[ <?php echo $this->config->item('pdf.best.size');?> ]</div>
    <p class="clearfix"></p> <?php */?>
    
   <?php /*?> <p class="form_title1"><span class="required">*</span> Posted By :</p>
    <div class="form_field form_field2"><input name="posted_by" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('posted_by');?>" /><?php echo form_error('posted_by');?></div>
    <p class="clearfix"></p>  <?php */?>
    
   <?php /*?> <p class="form_title1"><span class="required"></span> Youtube Link :</p>
    <div class="form_field form_field2"><input name="youtube" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube');?>" /><?php echo form_error('youtube');?></div>
    <p class="clearfix"></p>    <?php */?>              

    <div class="form_title1">Description :</div>
    <div class="form_field form_field2">
      <textarea name="blogs_description" rows="5" cols="50" class="form-control" id="description"><?php echo set_value('blogs_description');?></textarea><?php echo display_ckeditor($ckeditor1);?><?php echo form_error('blogs_description');?>
    </div>
    <p class="clearfix"></p>    

     </div>

    <div id="tab-image"> 
    <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p> 
       <?php for($i=1;$i<=$this->config->item('total_blog_images');$i++){?>
      <p class="form_title1"><span class="required">**</span> Image :</p>
    <div class="form_field form_field2"><input name="blog_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">[ <?php echo $this->config->item('blog.best.image.view');?> ] <?php echo form_error('blog_images'.$i);?></div>
    <p class="clearfix"></p>
     <?php }?>    

    <p class="form_title1"> Alt Tag Text :</p>
    <div class="form_field form_field2"><input name="blog_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('blog_alt');?>" /><?php echo form_error('blog_alt');?></div>
    <p class="clearfix"></p> 
    </div>   

    <p class="form_title1"></p>
    <div class="form_field form_field">
      <input type="hidden" name="action" value="add" />
   </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
</div>
</div>
<!--Body End-->
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//--></script>
<?php $this->load->view('includes/footer');?>