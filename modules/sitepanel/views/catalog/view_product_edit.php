<?php $this->load->view('includes/header');
$url_prm='';
if($this->input->get('category_id')){ $url_prm='?category_id='.$this->input->get('category_id'); }?>
<?php  $back_link = 'sitepanel/products'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->   

  <div class="mt10 p8"> 
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1"><?php echo $heading_title;?></a> <?php echo anchor("sitepanel/products".$url_prm,'Cancel','class="btn1" ' );?>
        </div>
         <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>

   <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart(current_url_query_string(),array('id'=>'form'));?>

     <div id="tab-general">
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
     <p class="form_title1"><span class="required">*</span> Select :</p>
    <div class="form_field form_field2">
   <?php $selected_category = array($res['category_id']);?>
    <select name="category_id" size="8" class="form-control" style="width:450px;" >
     <?php echo get_nested_dropdown_menu(0,$selected_category);?>
  </select><?php echo form_error('category_id');?></div>
    <p class="clearfix"></p>
    <?php
			$default_params = array(
				'heading_element' => array(
				'field_heading'=>" Product Name",
				'field_name'=>"product_name",
				'field_value'=>$res['product_name'],
				'field_placeholder'=>"Your Product Name",
				'exparams' => 'size="40"'
				),

				'url_element'  => array(
				'field_heading'=>"Page URL",
				'field_name'=>"friendly_url",
				'field_value'=>$res['friendly_url'],
				'field_placeholder'=>"Your Page URL",
				'exparams' => 'size="40"',

				)

			);
			seo_edit_form_element($default_params);
			?>  	
    <p class="form_title1"><span class="required">*</span> Product Code :</p>
    <div class="form_field form_field2"><input name="product_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('product_code',$res['product_code']);?>" /><?php echo form_error('product_code');?></div>
    <p class="clearfix"></p>

    <?php /*?> <p class="form_title1"><span class="required">**</span> Price :</p>
     <div class="form_field form_field2"><input name="product_price" type="text" maxlength=8 style="width:450px;" class="form-control" value="<?php echo set_value('product_price',$res['product_price']);?>" /> Maximum of 5 digits<?php echo form_error('product_price');?></div>
     <p class="clearfix"></p>

     <p class="form_title1"><span class="required">**</span> Discounted Price :</p>
     <div class="form_field form_field2"><input name="product_discounted_price" type="text" maxlength=8 style="width:450px;" class="form-control" value="<?php echo set_value('product_discounted_price',$res['product_discounted_price']);?>" /> Maximum of 5 digits<?php echo form_error('product_discounted_price');?></div>
     <p class="clearfix"></p>  <?php */?>  
     
         <p class="form_title1"><span class="required"></span> Youtube Video 1 :</p>
    <div class="form_field form_field2"><input name="youtube_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube_code',$res['youtube_code']);?>" /><?php echo form_error('youtube_code');?></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required"></span> Youtube Video 2 :</p>
    <div class="form_field form_field2"><input name="youtube_code2" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube_code2',$res['youtube_code2']);?>" /><?php echo form_error('youtube_code2');?></div>
    <p class="clearfix"></p>    

    <div class="form_title1"><span class="required">*</span>Description :</div>
    <div class="form_field form_field2">
      <textarea name="products_description" rows="5" cols="50" class="form-control" id="description"><?php echo $res['products_description'];?></textarea><?php echo display_ckeditor($ckeditor1);?><?php echo form_error('products_description');?>
    </div>
   <?php /*?>   <p class="clearfix"></p>
    <div class="form_title1">Core Module :</div>
    <div class="form_field form_field2">
      <textarea name="core_description" rows="5" cols="50" class="form-control" id="description1"><?php echo $res['core_description'];?></textarea><?php echo display_ckeditor($ckeditor2);?><?php echo form_error('core_description');?>
    </div>
    <p class="clearfix"></p>
    <div class="form_title1">Related Links :</div>
    <div class="form_field form_field2">
      <textarea name="related_description" rows="5" cols="50" class="form-control" id="description2"><?php echo $res['related_description'];?></textarea><?php echo display_ckeditor($ckeditor3);?><?php echo form_error('related_description');?>
    </div>
    <p class="clearfix"></p><?php */?>  
    
   
    <input type="hidden" name="products_id" value="<?php echo $res['products_id'];?>">

    

     </div>

     

     

    <div id="tab-image">

     <div style="margin-bottom:10px;line-height:20px">

      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>

      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>

     </div>

     <p class="clearfix"></p>

     <input type="hidden" name="product_exclude_images_ids" value="" id="product_exclude_images_ids" />

     <?php

     //trace($res_photo_media);

     $j=0;

     for($i=1;$i<=$this->config->item('total_product_images');$i++){

	     $product_img  = @$res_photo_media[$j]['media'];

	     $product_path = "products/".$product_img;

	     $product_img_auto_id  = @$res_photo_media[$j]['id'];

	     ?>

	     <p class="form_title1"><span class="required">**</span> Image :</p>

	     <div class="form_field form_field2"><input name="product_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">

	      <?php

	      if( $product_img!='' && file_exists(UPLOAD_DIR."/".$product_path) ){

		      ?>

		      <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a> | <input type="checkbox" name="product_img_delete[<?php echo $product_img_auto_id;?>]" value="Y" /> Delete

		      <?php

	      }?>

	      <br />[ <?php echo $this->config->item('product.best.image.view');?> ]

	      <div id="dialog_<?php echo $j;?>" title="Proejct Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  /> </div>

	      <input type="hidden" name="media_ids[]" value="<?php echo $product_img_auto_id;?>" />

	      <?php echo form_error('product_images'.$i);?>

	     </div>

	     <p class="clearfix"></p>

	     <?php

	     $j++;

     }?>

     

     <p class="form_title1"> Alt Tag Text :</p>

     <div class="form_field form_field2"><input name="product_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('product_alt',$res['product_alt']);?>" /><?php echo form_error('product_alt');?></div>

     <p class="clearfix"></p>

    </div>

    

    <p class="form_title1"></p>

    <div class="form_field form_field">

   

      <input type="hidden" name="action" value="add" />

   </div>

    <p class="clearfix"></p>

    <?php echo form_close();?> 

  </div>

  </div>

  

</div>

</div>

<script type="text/javascript"><!--

$('#tabs a').tabs();

$('#languages a').tabs();

$('#vtab-option a').tabs();

//-->

$('#product_exclude_images_ids').val('');

function delete_product_images(img_id)

{

	//alert($('#product_exclude_images_ids').val());

	img_id = img_id.toString();

	exclude_ids1 = $('#product_exclude_images_ids').val();

	exclude_ids1_arr = exclude_ids1=='' ? Array() : exclude_ids1.split(',');



	if($.inArray(img_id,exclude_ids1_arr)==-1){

		exclude_ids1_arr.push(img_id);

	}



	exclude_ids1 =  exclude_ids1_arr.join(',');



	$('#product_exclude_images_ids').val(exclude_ids1);

	$('#product_image'+img_id).remove();

	$('#del_img_link_'+img_id).remove();



	alert($('#product_exclude_images_ids').val());

}



</script>

<?php $default_date = date('Y-m-d',strtotime(date('Y-m-d',time())));?>

<script type="text/javascript">

$(document).ready(function(){

	$('.start_date,.end_date').live('click',function(e){

	  e.preventDefault();

	  cls = $(this).hasClass('start_date') ? 'start_date1' : 'end_date1';

	  $('.'+cls+':eq(0)').focus();

	});

	$( ".end_date1").live('focus',function(){

		$(this).datepicker({

			showOn: "focus",

			dateFormat: 'yy-mm-dd',

			changeMonth: true,

			changeYear: true,

			defaultDate: '<?php echo $selected_date;?>',

			buttonText:'',

			minDate:'<?php echo $default_date;?>' ,

			maxDate:'<?php echo date('Y-m-d',strtotime(date('Y-m-d',time())."+1 year"));?>',

			yearRange: "c-100:c+100",

			buttonImageOnly: true,

			onSelect: function(dateText, inst) {}

		});

	});

});

</script>

<?php $this->load->view('includes/footer');?>