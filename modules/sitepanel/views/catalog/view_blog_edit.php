<?php $this->load->view('includes/header');
$url_prm='';
if($this->input->get('category_id')){ $url_prm='?category_id='.$this->input->get('category_id'); }?>
<?php  $back_link = 'sitepanel/blogs'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Newsroom</h1>-->   

  <div class="mt10 p8"> 
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1"><?php echo $heading_title;?></a> <?php echo anchor("sitepanel/blogs".$url_prm,'Cancel','class="btn1" ' );?>
        </div>
         <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>

   <?php //echo validation_message();
    echo error_message();
    echo form_open_multipart(current_url_query_string(),array('id'=>'form'));?>

     <div id="tab-general">
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
 
    <?php
			$default_params = array(
				'heading_element' => array(
				'field_heading'=>$heading_title." Name",
				'field_name'=>"blog_name",
				'field_value'=>$res['blog_name'],
				'field_placeholder'=>"Blogs Name",
				'exparams' => 'size="40"'
				),

				'url_element'  => array(
				'field_heading'=>"Page URL",
				'field_name'=>"friendly_url",
				'field_value'=>$res['friendly_url'],
				'field_placeholder'=>"Page URL",
				'exparams' => 'size="40"',
				)

			);
			seo_edit_form_element($default_params);
			?>  	
   
   <?php /*?> <p class="form_title1"><span class="required"></span> Brochure Document :</p>
    <div class="form_field form_field2">
    <input type="file" name="brochure_doc" />
		<?php
        if($res['brochure_doc']!='' && file_exists(UPLOAD_DIR."/brochures/".$res['brochure_doc']))
        { 
        ?>
            <a href="<?php echo site_url('blogs/download_pdf/'.$res['blogs_id']);?>" class="u">View </a>
            | <a href="<?php echo site_url('sitepanel/blogs/delete_pdf/'.$res['blogs_id']);?>" class="u">Delete</a>
            [ <?php echo $this->config->item('pdf.best.size');?> ]
        <?php	
        }
        ?>
        <br />
    </div>
    <p class="clearfix"></p><?php */?>
    
   <?php /*?> <p class="form_title1"><span class="required">*</span> Posted By :</p>
    <div class="form_field form_field2"><input name="posted_by" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('posted_by',$res['posted_by']);?>" /><?php echo form_error('posted_by');?></div>
    <p class="clearfix"></p> <?php */?> 
    
    <?php /*?><p class="form_title1"><span class="required"></span> Youtube Link :</p>
    <div class="form_field form_field2"><input name="youtube" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('youtube',$res['youtube']);?>" /><?php echo form_error('youtube');?></div>
    <p class="clearfix"></p>   <?php */?>
     
         

    <div class="form_title1">Description :</div>
    <div class="form_field form_field2">
      <textarea name="blog_description" rows="5" cols="50" class="form-control" id="description"><?php echo $res['blog_description'];?></textarea><?php echo display_ckeditor($ckeditor1);?><?php echo form_error('blog_description');?>
    </div>
    <p class="clearfix"></p>   
    <input type="hidden" name="blogs_id" value="<?php echo $res['blogs_id'];?>">
     </div>
    <div id="tab-image">
     <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
     <p class="clearfix"></p>
     <input type="hidden" name="blog_exclude_images_ids" value="" id="blog_exclude_images_ids" />
     <?php
     //trace($res_photo_media);
     $j=0;
     for($i=1;$i<=$this->config->item('total_blog_images');$i++){

	     $blog_img  = @$res_photo_media[$j]['media'];
	     $blog_path = "blogs/".$blog_img;
	     $blog_img_auto_id  = @$res_photo_media[$j]['id'];
	     ?>
	     <p class="form_title1"><span class="required">**</span> Image :</p>
	     <div class="form_field form_field2"><input name="blog_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">
	      <?php
	      if( $blog_img!='' && file_exists(UPLOAD_DIR."/".$blog_path) ){
		      ?>
		      <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a> | <input type="checkbox" name="blog_img_delete[<?php echo $blog_img_auto_id;?>]" value="Y" /> Delete
		      <?php
	      }?>
	      <br />[ <?php echo $this->config->item('blog.best.image.view');?> ]
	      <div id="dialog_<?php echo $j;?>" title="Blogs Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/'.$blog_path;?>"  /> </div>
	      <input type="hidden" name="media_ids[]" value="<?php echo $blog_img_auto_id;?>" />
	      <?php echo form_error('blog_images'.$i);?>
	     </div>
	     <p class="clearfix"></p>
	     <?php
	     $j++;
     }?>
     <p class="form_title1"> Alt Tag Text :</p>
     <div class="form_field form_field2"><input name="blog_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('blog_alt',$res['blog_alt']);?>" /><?php echo form_error('blog_alt');?></div>
     <p class="clearfix"></p>
    </div>    

    <p class="form_title1"></p>
    <div class="form_field form_field">
      <input type="hidden" name="action" value="add" />
   </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div> 

</div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//-->
$('#blog_exclude_images_ids').val('');
function delete_blog_images(img_id)
{
	//alert($('#blog_exclude_images_ids').val());
	img_id = img_id.toString();
	exclude_ids1 = $('#blog_exclude_images_ids').val();
	exclude_ids1_arr = exclude_ids1=='' ? Array() : exclude_ids1.split(',');

	if($.inArray(img_id,exclude_ids1_arr)==-1){
		exclude_ids1_arr.push(img_id);
	}
	exclude_ids1 =  exclude_ids1_arr.join(',');
	$('#blog_exclude_images_ids').val(exclude_ids1);
	$('#blog_image'+img_id).remove();
	$('#del_img_link_'+img_id).remove();
	alert($('#blog_exclude_images_ids').val());
}
</script>
<?php $this->load->view('includes/footer');?>