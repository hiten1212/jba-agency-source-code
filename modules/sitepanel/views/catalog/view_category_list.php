<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->

<div class="breadcrumb">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li><?php echo anchor('sitepanel/dashbord','Home');?></li>
      <?php $segment=4;

  $catid    = (int) $this->uri->segment(4,0);

  if($catid ){

	  echo admin_category_breadcrumbs($catid,$segment);

  }else{

	  echo '<li class="active">Category</li>';

  }?>
    </ol>
  </div>
</div>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
    <p class="add-product"><?php echo anchor("sitepanel/category/add/$parent_id","Add $heading_title",'' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:15px;">
    <?php

   echo error_message();

   echo form_open("sitepanel/category/index/$parent_id",'id="search_form" method="get" ');

   ?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="<?php echo $heading_title;?> Name">
      </p>
      <p class="serch-sec2">
        <select name="status" class="form-control">
          <option value="">Status</option>
          <option value="1" <?php echo $this->input->get_post('status')==='1' ? 'selected="selected"' : '';?>>Active</option>
          <option value="0" <?php echo $this->input->get_post('status')==='0' ? 'selected="selected"' : '';?>>In-active</option>
        </select>
      </p>
      <p class="serch-sec3">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php

      if( $this->input->get_post('keyword')!='' || $this->input->get_post('status')!='' ){

	      $parentid = (int) $this->input->get_post('parent_id');

	      if($parentid > 0 ){

		      echo anchor("sitepanel/category/index/$parentid",'<span>Clear Search</span>','class="form-control"');

	      }else{

		      echo anchor("sitepanel/category/",'<span>Clear Search</span>','class="form-control"');

	      }

      }?>
      </p>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div>
      <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?> </div>
      <p class="clearfix"></p>
    </div>
    <div class="clearfix"></div>
    <input type="hidden" name="parent_id" value="<?php echo $parent_id;?>"  />
    <?php echo form_close();

   

   if(is_array($res) && ! empty($res)){

	   echo form_open("sitepanel/category/",'id="data_form"');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', checked);" /></td>
              <td width="25%" class="fs14">Name</td>
              <td width="9%" class="text-center fs13">Image</td>
              <td width="12%" class="text-center fs13">Display Order</td>
              <td width="14%" class="text-center fs13">Current Status</td>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php

	     foreach($res as $catKey=>$pageVal){

		     $imgdisplay=FALSE;

		     $displayorder       = ($pageVal['sort_order']!='') ? $pageVal['sort_order']: "0";

		     $total_subcategory  =  $pageVal['total_subcategories'];

		     $condtion_product   =  "AND category_id='".$pageVal['category_id']."'";
			
			 //$condtion_product   =  "AND FIND_IN_SET( '".$pageVal['category_id']."',category_ids )";

		     $total_products     =  count_products($condtion_product);

		     ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo  $pageVal['category_id'];?>" <?php if($pageVal['is_fixed']=='1'){ echo 'disabled=true'; }?> />
              <input type="hidden" name="category_count" value="Y" />
              <input type="hidden" name="product_count" value="Y" /></td>
            <td width="25%" class="fs14"><?php echo $pageVal['category_name'];

		       $category_set_in = array();

		       if($pageVal['is_featured']!="" && $pageVal['is_featured']!='0'){

			       $category_set_in[]='<b class="red">Featured : </b> <span class="fa fa-check-circle green fs24 mt5"></span>';

		       }

		       if(!empty($category_set_in)){

			       echo "<br /><br />".implode("<br>",$category_set_in);

		       }

		       if($total_subcategory>0){

			       echo "<br><br>".anchor("sitepanel/category/index/".$pageVal['category_id'],'Subcategory ['. $total_subcategory.']','class="refSection" ' );

		       }elseif($total_products>0){

			       echo "<br><br>".anchor("sitepanel/products?category_id=".$pageVal['category_id'],'Products ['. $total_products.']','class="refSection" ' );

		       }else{

			       echo "<br><br>".anchor("sitepanel/category/index/".$pageVal['category_id'],'Subcategory ['. $total_subcategory.']','class="refSection" ')." | ".anchor("sitepanel/products?category_id=".$pageVal['category_id'],'Products ['. $total_products.']','class="refSection" ');

		       }?> </td>
            <td width="9%" align="center" class=" fs13"><p class="pro_pic"><span><img src="<?php echo get_image('category',$pageVal['category_image'],58,58,'R');?>" alt=""></span></p></td>
            <td width="12%" class="text-center fs13"><?php if($pageVal['is_fixed']=='1'){ echo $displayorder; }else{?>
              <input type="text" name="ord[<?php echo $pageVal['category_id'];?>]" value="<?php echo $displayorder;?>" size="5" class="w50 mt10 select-style"  />
              <?php }?></td>
            <td width="14%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="15%" class="text-center grey"><?php

		       if($pageVal['is_fixed']=='0') {

			       echo anchor("sitepanel/category/edit/$pageVal[category_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');

			       echo "&nbsp;";

			       echo '  '.anchor("sitepanel/category/delete/$pageVal[category_id]/".query_string(),'<span class="fa fa-trash-o fa-2x"></span>','title="Delete" class="ml15" onclick = \'return confirm("Are you sure to delete this category");\'');

		       }?>
            </td>
          </tr>
          <?php

	     }?>
          <?php

	   if($page_links!=''){

		   ?>
          <tr>
            <td colspan="6" align="right" height="30"><?php echo $page_links; ?></td>
          </tr>
          <?php }?>
        </table>
      </div>
      <div class="mt15 ">
        <p class=" mt10 serch-sec8 fl">
          <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
          <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
          <input name="update_order" type="submit"  value="Update Order" class="btn1" />
        </p>
        <?php /*<p class=" ml20 serch-sec8 fl">
          <?php

	      if($pageVal['parent_id']==0){

		      echo form_dropdown("set_as",$this->config->item('category_set_as_config'),$this->input->post('set_as'),'class=" mt10 form-control fl" style="width:250px;" onchange="return onclickgroup()"');

		      echo form_dropdown("unset_as",$this->config->item('category_unset_as_config'),$this->input->post('unset_as'),'class="ml5 mt10 form-control fl" style="width:250px;" onchange="return onclickgroup()"');

	      }?>
        </p>*/?>
        <p class="clearfix"></p>
      </div>
    </div>
    <?php

	   echo form_close();

   }else{

	   echo "<center><strong> No record(s) found !</strong></center>" ;

   }?>
  </div>
</div>
<script type="text/javascript">

function onclickgroup(){

	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){

		$('#data_form').submit();

	}

}

</script>
<?php $this->load->view('includes/footer'); ?>
