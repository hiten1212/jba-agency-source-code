<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<div class="breadcrumb">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li><?php echo anchor('sitepanel/dashbord','Dashboard');?></li>
   <?php    $segment=4;
   $catid = (int)$this->input->get_post('category_id');
   if($catid ){
	   echo admin_category_breadcrumbs($catid,$segment);
   }else{
	   echo '<li class="active">Blogs</li>';
   }?>
    </ol>
  </div>
</div>
<!--Breadcrumb End--> 
<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
    <p class="add-product"><a href="<?php echo base_url();?>sitepanel/blogs/add/<?php echo $this->input->get('category_id');?>">Add Blog</a></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:15px;">
     <?php
    if(error_message() !=''){
	    echo error_message();
    }?>
    <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize();   } </script>
    <?php echo form_open("sitepanel/blogs/",'id="search_form" method="get" ');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword2" value="<?php echo $this->input->get_post('keyword2');?>" type="text" class="form-control" placeholder="Blogs Name">
      </p>
      <p class="serch-sec2">
        <select name="status" class="form-control">
          <option value="">Status</option>
        <option value="1" <?php echo $this->input->get_post('status')==='1' ? 'selected="selected"' : '';?>>Active</option>
        <option value="0" <?php echo $this->input->get_post('status')==='0' ? 'selected="selected"' : '';?>>In-active</option>
        </select>
      </p>
      
      <?php
       if(is_array($categories) && !empty($categories)){
	       ?>
      <p class="serch-sec4">
        <select name="category_id" class="form-control">
          <option value=""> Select Category</option>
           <?php
	        foreach($categories as $cv){
		        ?>
		        <option value="<?php echo $cv['category_id'];?>" <?php echo $this->input->get_post('category_id')==$cv['category_id'] ? 'selected="selected"' : '';?>><?php echo $cv['category_name'];?></option>
		        <?php
	        }?>
        </select>
      </p>
       <?php
       }else{
	       ?>
	       <input type="hidden" name="category_id" value="<?php echo $this->input->get_post('category_id');?>"  />
	       <?php
       } 
	   ?>
      <p class="serch-sec6">
      <input type="hidden" name="search" value="yes"  />
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php
       if( $this->input->get_post('keyword2')!='' || $this->input->get_post('status')!=''  ||  $this->input->get_post('search_prod_type')!=''){
	       if($this->input->get_post('category_id')!=''){
		       $search_category=$this->input->get_post('category_id');
		       echo anchor("sitepanel/blogs?category_id=".$search_category,'<span>Clear Search</span>','class="form-control"');
	       }else{
		       echo anchor("sitepanel/blogs/",'<span>Clear Search</span>','class="form-control"');
	       }
       }?>
      </p>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div>
      <p class="total-results red fs14"><?php echo $category_result_found; ?></p>
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div><p class="clearfix"></p>
    </div>
    <div class="clearfix"></div>
    <?php echo form_close();?>
    <?php
		if( is_array($res) && !empty($res) ){
			echo form_open(current_url_query_string(),'id="data_form"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', checked);" /></td>
              <td width="25%" class="fs14">Blogs Name</td>              
              <td width="10%" class="text-center fs13">Blogs Picture</td>             
              <td width="10%" class="text-center fs13">Details</td>
              <td width="10%" class="text-center fs13">Current Status</td>
              <td width="12%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php
			  $atts = array(
				'width'      => '740',
				'height'     => '600',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
				);
			  $atts_edit = array(
			  'width'      => '525',
			  'height'     => '375',
			  'scrollbars' => 'no',
			  'status'     => 'no',
			  'resizable'  => 'no',
			  'screenx'    => '0',
			  'screeny'    => '0'
			  );
			  foreach($res as $catKey=>$pageVal){
				  ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['blogs_id'];?>" /></td>
            <td width="25%" class="fs14">
             <?php echo $pageVal['blog_name'];
				    ?>
                    <br />
            <?php /*<a href="<?php echo base_url().'sitepanel/blogs/comments/'.$pageVal['blogs_id'];?>" class="fs13 grey">View Comments(<?php echo $pageVal['review_count'];?>)</a><br />*/?>
           <?php //echo "<strong>Category:</strong> &nbsp;". @$cat_name=get_db_field_value("wl_categories",'category_name'," AND category_id = '".$pageVal['category_id']."'")?><br />
           <?php
           $product_set_in = array();
			  
		       if($pageVal['is_latest']!="" && $pageVal['is_latest']!='0')
		       $product_set_in[]='<b>Latest : </b> <span class="fa fa-check-circle green fs18"></span>';
		       
		       if(!empty($product_set_in)){
			       echo "<br />";
			       echo implode("<br>",$product_set_in)."<br />";
		       }
			   ?>
           
				    <div id="dialog_<?php echo $pageVal['blogs_id'];?>" title="Description" style="display:none;">
					<?php
					echo $pageVal['blog_description'];
					?>
                    </div>
                    </td>            
            <td width="10%" class=" fs13" align="center" ><p class="pro_pic"><span><img src="<?php echo get_image('blogs',$pageVal['media'],60,60,'R');?>" alt=""></span></p></td>
           
            <td width="10%" class="text-center fs13" ><a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $pageVal['blogs_id'];?>').dialog( {width: 650,position: 'center' } );">View Details</a></td>
            <td width="10%" class="text-center fs13"><span title="<?php echo ($pageVal['blog_status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['blog_status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="12%" class="text-center grey"><?php echo anchor("sitepanel/blogs/edit/$pageVal[blogs_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');?>
            <?php /*
				    <p><?php echo anchor_popup('sitepanel/blogs/related/'.$pageVal['blogs_id'], 'Add Related Blogs', $atts);?></p>
				    <p><?php echo anchor_popup('sitepanel/blogs/view_related/'.$pageVal['blogs_id'], 'View Related Blogs', $atts);?></p>
				    */?>
                    </td>
          </tr>
            <?php
			  }?>
               <?php
	   if($page_links!=''){
		   ?>
            <tr><td colspan="9" align="right" height="30"><?php echo $page_links; ?></td></tr>
            <?php }?> 
        </table>
      </div>
      <div class="mt15 ">
      <p class=" mt10 serch-sec8 fl">
      	  <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
	      <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
	     <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onclick="return validcheckstatus('arr_ids[]','delete','Record');"/>
          </p>
	  <p class=" ml20 serch-sec8 fl">
	     <?php
	     /* if($pageVal['parent_id']==0){
		      echo form_dropdown("set_as",$this->config->item('blog_set_as_config'),$this->input->post('set_as'),'class=" mt10 form-control fl" style="width:250px;" onchange="return onclickgroup()"');
		      echo form_dropdown("unset_as",$this->config->item('blog_unset_as_config'),$this->input->post('unset_as'),'class="ml5 mt10 form-control fl" style="width:250px;" onchange="return onclickgroup()"');
	      }*/
		  ?>
      </p>
       <p class="clearfix"></p> 
          </div> 
    </div>
    <?php
			echo form_close();
		}else{
			echo "<center><strong> No record(s) found !</strong></center>" ;
		}?>
  </div>
</div>
<!--Body End--> 
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer');?>