<?php $this->load->view('includes/header');
$blog_id = $this->uri->segment(4);
$rev_id = $this->uri->segment(5);
 ?>
<style>
.orange,.orange a,orange a:hover{color:#f58220}
.orange2,.orange2 a,orange2 a:hover{color:#f84e20}
</style>
<?php echo admin_breadcrumb($heading_title); ?>
<div class="container-fluid"> 
     <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
 <p class="add-product"><?php //echo anchor("sitepanel/blog/add_blog/",'Add Blog','' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
     <?php 
                if(error_message() !=''){
               	   echo error_message();
                }
        ?> 	                
		<?php echo form_open("sitepanel/blogs/comments_reply/".$blog_id.'/'.$rev_id,'id="search_form" method="get" '); ?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1"> 
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Article Title,Poster Name,Comment">
      </p>           
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
       <p class="serch-sec2">
        <?php 
				if($this->input->get_post('keyword')!=''){ 
					echo anchor("sitepanel/blogs/comments_reply/".$blog_id.'/'.$rev_id,'<span>Clear Search</span>','class="form-control"');
				} 
				?>
      </p>
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    
    <div class="clearfix"></div>
    <?php echo form_close();?>
		<?php		
		if( is_array($res) && !empty($res) )
		{
			echo form_open(current_url_query_string(),'id="myform"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll box_h_235">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="4%" nowrap="nowrap"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').attr('checked', this.checked);" /></td>
              <td width="14%" nowrap="nowrap"class="text-center fs13">Article Article</td>
              <td width="21%" nowrap="nowrap" class="text-center fs13">User Info</td>              
               <td width="25%" nowrap="nowrap" class="text-center fs13">Comment Replied</td>              
              <td width="14%" nowrap="nowrap" class="text-center fs13">Post Date</td>
              <td width="11%" nowrap="nowrap" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php 	
			$atts = array(
									'width'      => '740',
									'height'     => '600',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'yes',
									'screenx'    => '0',
									'screeny'    => '0'
									 );
		$j=1;
		foreach($res as $catKey=>$pageVal)
		{ 
		?> 
          <tr>
            <td width="4%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['id'];?>" /></td>
            <td width="14%" class="fs14" >
            <?php echo forum_title($pageVal['blog_id']);?></td>
            <td width="21%" align="'left" class=" fs13"><?php echo $pageVal['poster_name'];?>
            </td>            
            
         
           <td width="25%" class="text-left fs13"> <?php echo nl2br($pageVal['comment']);?>  </td>
           
            <td width="15%" class="text-center fs13"><?php echo getDateFormat($pageVal['posted_date'],1);?></td>
            <td width="10%" class="text-center grey"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
          </tr>
          <?php
		$j++;
		}		   
		
		 if($page_links!=''){ ?>
         <tr><td colspan="6" align="center" height="30"><?php echo $page_links; ?></td></tr>
          <?php }?>
        
          
        </table>
      </div>
      <p class="mt15"><input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
				<input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
				<input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
                </p>
    </div>
    <?php
		echo form_close();
	}else
	{
		echo "<center><strong> No record(s) found !</strong></center>" ;
	}
	?> 
  </div>    
    
</div>
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer'); ?>