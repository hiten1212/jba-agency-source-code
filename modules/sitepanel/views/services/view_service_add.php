<?php $this->load->view('includes/header');
$pcatID =($this->uri->segment(4) > 0)? $this->uri->segment(4):"0";
$pcatID = (int) $pcatID;

$values_posted_back=(is_array($this->input->post())) ? TRUE : FALSE;
$stock_expected_date = $this->input->post('stock_expected_date');
$url_prm=''; if($this->uri->segment('4')!=''){ $url_prm='?category_id='.$this->uri->segment('4'); }
$back_link = 'sitepanel/services'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <div class="mt10 p8">
   <div class="box_style">
    <div class="recent-table p10 mb15"><?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1">Add Service</a> <?php echo anchor("sitepanel/services".$url_prm,'Cancel','class="btn1" ' );?></span></div>
    <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
    <?php echo validation_message();
    echo error_message();
    echo form_open_multipart('sitepanel/services/add/',array('id'=>'form'));?>
    <div id="tab-general">
     <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
     <p class="clearfix"></p>
     <?php
     $selcatID = ($this->input->post('category_id')!='') ? $this->input->post('category_id'): $pcatID;
     $selcatID = (int) $selcatID;
     ?>
     <p class="form_title1">Service status type :</p>
     <div class="form_field form_field2">
      <?php $selected_category = array($selcatID);?>
      <select name="category_id" size="8" class="form-control" style="width:680px;">
       <?php echo get_nested_dropdown_menu(0,$selected_category);?>
      </select>
      <?php echo form_error('category_id');?>
     </div>
     <p class="clearfix"></p>

     <?php
    /* $default_params = array(
     'heading_element' => array(
     'field_heading'=>"Project Name",
     'field_name'=>"service_name",
     'field_placeholder'=>"Project Name",
     'exparams' => 'style="width:680px;"'
     ),
     'url_element'  => array(
     'field_heading'=>"",
     'field_name'=>"friendly_url",
     'field_placeholder'=>"Your Page URL",
     'exparams' => 'style="width:680px;display:none;"',
     'pre_seo_url' =>'',
     'pre_url_tag'=>FALSE
     )
     );
     seo_add_form_element($default_params);*/
     ?>
	 
	 	 <p class="form_title1">Service Name :</p>
	 <div class="form_field form_field2"><input name="service_name" type="text" class="form-control" value="<?php echo set_value('service_name');?>" placeholder="Service Name" style="width:680px;">
<?php echo form_error('service_name');?>
<input type="hidden" name="friendly_url" id="friendly_url" value="" class="seo_friendly_url edit_url form-control" placeholder="Your Page URL" style="width:680px;display:none;">
    </div>
	 <p class="clearfix"></p>
	<p class="form_title1"><span class="required"></span> Service Text colour :</p>
        <div class="form_field form_field2">
          <input name="text_colour" type="color" style="width:450px;height:45px" class="form-control" value="<?php echo set_value('text_colour');?>">
        </div>
        <p class="clearfix"></p>
     <input name="service_code" type="hidden" style="width:450px;" class="form-control" value="<?php echo mt_rand(100000,999999);?>" /><?php echo form_error('service_code');?>
     <?php /*<p class="form_title1"><span class="required">*</span> Service Code :</p>
    <div class="form_field form_field2"><input name="service_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('service_code');?>" /><?php echo form_error('service_code');?></div>
    <p class="clearfix"></p> 
     
     <p class="form_title1">Services City :</p>
     <div class="form_field form_field2">
      <div style='width:450px; height:100px; overflow-x:hidden; overflow-y:scroll; border:1px solid gray'>
       <table width="100%" cellpadding="0" cellspacing="0">
        <?php
        $selected_city=($this->input->post('city_id',TRUE))?$this->input->post('city_id',TRUE):array();
        if(is_array($city) && !empty($city)){
	        foreach($city as $val){
		        ?>
		        <tr style="border:0px;"><td width="95%" style="padding:1px;"> <input type="checkbox" name="city_id[]" id="city_id" style="margin-left:3px;" value="<?php echo $val['city_id'];?>" <?php echo in_array($val['city_id'],$selected_city) ? ' checked="checked"' : '';?>  /> <?php echo $val['city_name'];?></td></tr>
		        <?php
	        }
        }?>
       </table>
      </div>
      <?php echo form_error('city_id[]');?>
     </div>
     <p class="clearfix"></p>*/?>  
		
      <?php /* <p class="form_title1">Upload PDF :</p>
     <div class="form_field form_field2"><input name="broucher_doc" type="file" style="width:680px;" class="form-control" /><?php echo form_error('broucher_doc');?></div>
     <p class="clearfix"></p>
        
     
     <p class="form_title1"><span class="required">*</span> Video Link (Youtube) :</p>
     <div class="form_field form_field2"><input name="video_link" type="text" style="width:680px;" class="form-control" value="<?php echo set_value('video_link');?>" /><?php echo form_error('video_link');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1"><span class="required">*</span> Price :</p>
     <div class="form_field form_field2"><input name="service_price" type="text" maxlength=8 style="width:680px;" class="form-control" value="<?php echo set_value('service_price');?>" /> Maximum of 5 digits<?php echo form_error('service_price');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1"><span class="required">**</span> Discounted Price :</p>
     <div class="form_field form_field2"><input name="service_discounted_price" type="text" maxlength=8 style="width:680px;" class="form-control" value="<?php echo set_value('service_discounted_price');?>" /> Maximum of 5 digits<?php echo form_error('service_discounted_price');?></div>
     <p class="clearfix"></p>
	 
	  <p class="form_title1"> Visit Link :</p>
     <div class="form_field form_field2"><input name="visit_link" type="text" style="width:680px;" class="form-control" value="<?php echo set_value('visit_link');?>" /><?php echo form_error('visit_link');?></div>
     <p class="clearfix"></p>*/?>
     
   <!--<p class="form_title1"><span class="required"></span> Service Description Text colour :</p>
        <div class="form_field form_field2">
          <input name="description_text_colour" type="color" style="width:450px;height:45px" class="form-control" value="<?php //echo set_value('description_text_colour');?>">
        </div>
        <p class="clearfix"></p>-->
	 
	   <div class="form_title1"> Description :</div>
     <div class="form_field form_field2"><textarea name="service_description" rows="5" cols="50" class="form-control" id="description"><?php echo set_value('service_description');?></textarea><?php echo display_ckeditor($ckeditor);?><?php echo form_error('service_description');?></div>
     <p class="clearfix"></p>
	 
	    <?php /*<div class="form_title1"><span class="required">*</span> Highlights Description :</div>
     <div class="form_field form_field2"><textarea name="highlights" rows="5" cols="50" class="form-control" id="highlights"><?php echo set_value('highlights');?></textarea><?php echo display_ckeditor($hckeditor);?><?php echo form_error('highlights');?></div>
     <p class="clearfix"></p>
   
    
     <div class="form_title1"><span class="required">*</span> Important Details :</div>
     <div class="form_field form_field2"><textarea name="important_details" rows="5" cols="50" class="form-control" id="important_details"><?php echo set_value('important_details');?></textarea><?php echo display_ckeditor($imckeditor);?><?php echo form_error('important_details');?></div>
     <p class="clearfix"></p>*/?>
    </div>
     
    <div id="tab-image">
     <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
     <p class="clearfix"></p>
     <?php for($i=1;$i<=$this->config->item('total_service_images');$i++){?>
     <p class="form_title1"><span class="required">**</span> Image :</p>
     <div class="form_field form_field2"><input name="service_images<?php echo $i;?>" type="file" style="width:680px;" class="form-control">[ <?php echo $this->config->item('service.best.image.view');?> ] <?php echo form_error('service_images'.$i);?></div>
     <p class="clearfix"></p>
     <?php }?>
     
	 
	 
	 
     <!--<p class="form_title1"> Alt Tag Text :</p>
     <div class="form_field form_field2"><input name="service_alt" type="text" style="width:680px;" class="form-control" value="<?php echo set_value('service_alt');?>" /><?php echo form_error('service_alt');?></div>
     <p class="clearfix"></p>-->
    </div>
    <p class="form_title1"></p>
    <div class="form_field form_field">
     <input type="hidden" name="action" value="add" />
    </div>
    <p class="clearfix"></p>
    <?php echo form_close();?>
   </div>
  </div>
 </div>
</div>
<!--Body End-->
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//--></script>
<?php $this->load->view('includes/footer');?>