<?php $this->load->view('includes/header');
$url_prm='';
if($this->input->get('category_id')){ $url_prm='?category_id='.$this->input->get('category_id'); }
$back_link = 'sitepanel/services'.$url_prm;
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <div class="mt10 p8">
   <div class="box_style">
    <div class="recent-table p10 mb15"><?php echo $heading_title;?> <span class="white" style="float:right"> <a href="javascript:void(0);" onclick="$('#form').submit();" class="btn1">Update service</a> <?php echo anchor("sitepanel/services".$url_prm,'Cancel','class="btn1" ' );?></div>
    <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
    <?php echo validation_message();
    echo error_message();
    echo form_open_multipart(current_url_query_string(),array('id'=>'form'));?>
    <div id="tab-general">
     <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
     <p class="clearfix"></p>
     <p class="form_title1">Service status type :</p>
     <div class="form_field form_field2">
      <?php $selected_category = array($res['category_id']);?>
      <select name="category_id" size="8" class="form-control" style="width:450px;">
       <?php echo get_nested_dropdown_menu(0,$selected_category);?>
      </select><?php echo form_error('category_id');?>
     </div>
     <p class="clearfix"></p>

     <?php
    /* $default_params = array(
     'heading_element' => array(
     'field_heading'=>"Project Name",
     'field_name'=>"service_name",
     'field_value'=>$res['service_name'],
     'field_placeholder'=>"Project Name",
     'exparams' => 'style="width:680px;"'
     ),
     'url_element'  => array(
     'field_heading'=>"Page URL",
     'field_name'=>"friendly_url",
     'field_value'=>$res['friendly_url'],
     'field_placeholder'=>"Your Page URL",
     'exparams' => 'style="width:680px;display:none;"',
     )
     );
     seo_edit_form_element($default_params);*/
	 
	
     ?>
	 <p class="form_title1"> Service Name :</p>
	 <div class="form_field form_field2"><input name="service_name" type="text" class="form-control" value="<?php echo set_value('service_name',$res['service_name']);?>" placeholder="Service Name" style="width:680px;">
<?php echo form_error('service_name');?>
<input type="hidden" name="friendly_url" value="<?php echo set_value('friendly_url',$res['friendly_url']);?>" class="seo_friendly_url edit_url form-control" placeholder="Your Page URL" style="width:680px;display:none;">
    </div>
	  <p class="clearfix"></p>
	<p class="form_title1"><span class="required"></span> Service Text colour :</p>
        <div class="form_field form_field2">
          <input name="text_colour" type="color" style="width:450px;height:45px" class="form-control" value="<?php echo $res['text_colour'];?>">
        </div>
        <p class="clearfix"></p>
     <?php /* <p class="form_title1"><span class="required">*</span> Service Code :</p>
    <div class="form_field form_field2"><input name="service_code" type="text" style="width:450px;" class="form-control" value="<?php echo set_value('service_code',$res['service_code']);?>" /><?php echo form_error('service_code');?></div>
    <p class="clearfix"></p>	 
       <p class="form_title1">Upload PDF :</p>
      <div class="form_field form_field2"><input name="broucher_doc" type="file" style="width:680px;" class="form-control" /><?php echo form_error('broucher_doc');?>
      
      <p class="pt5">
	<?php
	  if($res["broucher_doc"]!="" && file_exists(UPLOAD_DIR."/services/".$res["broucher_doc"])){
		  echo anchor(site_url("services/download_pdf/".$res["service_id"]),"Download File",array("class"=>"red b"));
		 // echo "&nbsp;&nbsp;&nbsp;&nbsp;"; 
		 // echo anchor(site_url("sitepanel/broucher/delete_pdf/".$edit_result["broucher_id"]),"Delete File",array("class"=>"black b"));
	  }?>
      </p>
            
      </div>
      <p class="clearfix"></p>
     
    
     <p class="form_title1"><span class="required">*</span> Video Link (Youtube) :</p>
     <div class="form_field form_field2"><input name="video_link" type="text" style="width:680px;" class="form-control" value="<?php echo set_value('video_link',$res['video_link']);?>" /><?php echo form_error('video_link');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1"><span class="required">*</span> Price :</p>
     <div class="form_field form_field2"><input name="service_price" type="text" maxlength=8 style="width:680px;" class="form-control" value="<?php echo set_value('service_price',$res['service_price']);?>" /> Maximum of 5 digits<?php echo form_error('service_price');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1"><span class="required">**</span> Discounted Price :</p>
     <div class="form_field form_field2"><input name="service_discounted_price" type="text" maxlength=8 style="width:680px;" class="form-control" value="<?php echo set_value('service_discounted_price');?>" /> Maximum of 5 digits<?php echo form_error('service_discounted_price');?></div>
     <p class="clearfix"></p>
	 
	 <p class="form_title1"> Visit Link :</p>
     <div class="form_field form_field2"><input name="visit_link" type="text" style="width:680px;" class="form-control" value="<?php echo set_value('visit_link',$res['visit_link']);?>" /><?php echo form_error('visit_link');?></div>
     <p class="clearfix"></p>
     
     <p class="form_title1">Services City :</p>
     <div class="form_field form_field2">
      <div style='width:450px; height:100px; overflow-x:hidden; overflow-y:scroll; border:1px solid gray'>
       <table width="100%" cellpadding="0" cellspacing="0">
        <?php
        $sarray=array();
        foreach($city_array as $key=>$val){
	        $sarray[]=$val["city_id"];
        }
        $sarray=($sarray)?$sarray:array();
        if(is_array($city) && !empty($city)){
	        foreach($city as $val){
		        ?>
		        <tr style="border:0px;"><td width="95%" style="padding:1px;"> <input type="checkbox" name="city_id[]" id="city_id" style="margin-left:3px;" value="<?php echo $val['city_id'];?>" <?php echo in_array($val['city_id'],$sarray) ? ' checked="checked"' : '';?>  /> <?php echo $val['city_name'];?></td></tr>
		        <?php
	        }
        }?>
       </table>
      </div>
      <?php echo form_error('city_id[]');?>
     </div>
     <p class="clearfix"></p>*/?>
     <!--<p class="form_title1"><span class="required"></span> Service Description Text colour :</p>
        <div class="form_field form_field2">
          <input name="description_text_colour" type="color" style="width:450px;height:45px" class="form-control" value="<?php //echo $res->description_text_colour;?>">
        </div>
        <p class="clearfix"></p>-->
     <div class="form_title1"> Service Description :</div>
     <div class="form_field form_field2"><textarea name="service_description" rows="5" cols="50" class="form-control" id="description"><?php echo set_value('service_description',$res['service_description']);?></textarea><?php echo display_ckeditor($ckeditor);?><?php echo form_error('service_description');?></div>
     <p class="clearfix"></p>
     
       <?php /* <div class="form_title1"><span class="required">*</span> Highlights Description :</div>
     <div class="form_field form_field2"><textarea name="highlights" rows="5" cols="50" class="form-control" id="highlights"><?php echo set_value('highlights',$res['highlights']);?></textarea><?php echo display_ckeditor($hckeditor);?><?php echo form_error('highlights');?></div>
     <p class="clearfix"></p>
    
    
     <div class="form_title1"><span class="required">*</span> Important Details :</div>
     <div class="form_field form_field2"><textarea name="important_details" rows="5" cols="50" class="form-control" id="important_details"><?php echo set_value('important_details',$res['important_details']);?></textarea><?php echo display_ckeditor($imckeditor);?><?php echo form_error('important_details');?></div>
     <p class="clearfix"></p>*/?>
    </div>
     <input type="hidden" name="service_id" value="<?php echo $res['service_id'];?>">
    </div>
    
    <div id="tab-image">
     <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
     <p class="clearfix"></p>
     <input type="hidden" name="service_exclude_images_ids" value="" id="service_exclude_images_ids" />
     <?php
     //trace($res_photo_media);
     $j=0;
     for($i=1;$i<=$this->config->item('total_service_images');$i++){
	     $service_img  = @$res_photo_media[$j]['media'];
	     $service_path = "services/".$service_img;
	     $service_img_auto_id  = @$res_photo_media[$j]['id'];
	     ?>
	     <p class="form_title1"><span class="required">**</span> Image :</p>
	     <div class="form_field form_field2"><input name="service_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">
	      <?php
	      if( $service_img!='' && file_exists(UPLOAD_DIR."/".$service_path) ){
		      ?>
		      <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a> | <input type="checkbox" name="service_img_delete[<?php echo $service_img_auto_id;?>]" value="Y" /> Delete
		      <?php
	      }?>
	      <br />[ <?php echo $this->config->item('service.best.image.view');?> ]
	      <div id="dialog_<?php echo $j;?>" title="Service Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/'.$service_path;?>"  /> </div>
	      <input type="hidden" name="media_ids[]" value="<?php echo $service_img_auto_id;?>" />
	      <?php echo form_error('service_images'.$i);?>
	     </div>
	     <p class="clearfix"></p>
	     <?php
	     $j++;
     }?>
     
     <!--<p class="form_title1"> Alt Tag Text :</p>
     <div class="form_field form_field2"><input name="service_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('service_alt',$res['service_alt']);?>" /><?php echo form_error('service_alt');?></div>
     <p class="clearfix"></p>-->
    </div>
    <p class="form_title1"></p>
    <div class="form_field form_field">
     <input type="hidden" name="action" value="add" />
    </div>
    <p class="clearfix"></p>
    <?php echo form_close();?>
   </div>
  </div>
 </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//-->
$('#service_exclude_images_ids').val('');
function delete_service_images(img_id)
{
	//alert($('#service_exclude_images_ids').val());
	img_id = img_id.toString();
	exclude_ids1 = $('#service_exclude_images_ids').val();
	exclude_ids1_arr = exclude_ids1=='' ? Array() : exclude_ids1.split(',');

	if($.inArray(img_id,exclude_ids1_arr)==-1){
		exclude_ids1_arr.push(img_id);
	}

	exclude_ids1 =  exclude_ids1_arr.join(',');

	$('#service_exclude_images_ids').val(exclude_ids1);
	$('#service_image'+img_id).remove();
	$('#del_img_link_'+img_id).remove();

	alert($('#service_exclude_images_ids').val());
}
</script>
<?php $this->load->view('includes/footer');?>