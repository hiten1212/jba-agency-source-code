<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<div class="breadcrumb">
 <div class="container-fluid">
  <ol class="breadcrumb">
   <li><?php echo anchor('sitepanel/dashbord','Dashboard');?></li>
   <?php    $segment=4;
   $catid = (int)$this->input->get_post('category_id');
   if($catid ){
	   echo admin_service_category_breadcrumbs($catid,$segment);
   }else{
	   echo '<li class="active">Projects</li>';
   }?>
  </ol>
 </div>
</div>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <h1 class="fl"> <?php echo $heading_title; ?></h1>
  <p class="add-product"><a href="<?php echo base_url();?>sitepanel/services/add/<?php echo $this->input->get('category_id');?>">Add <?php echo $heading_title; ?></a></p>
  <p class="clearfix"></p>
  <hr style="margin-bottom:5px; margin-top:10px;">
  <?php
  if(error_message() !=''){
	  echo error_message();
  }?>
  <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize();   } </script>
  <?php echo form_open("sitepanel/services/",'id="search_form" method="get" ');?>
  <div class="search-section">
   <p class="serch-sec">Search</p>
   <p class="serch-sec1"><input name="keyword2" value="<?php echo $this->input->get_post('keyword2');?>" type="text" class="form-control" placeholder="<?php echo $heading_title; ?> Name"></p>
   <p class="serch-sec2">
    <select name="status" class="form-control">
     <option value="">Status</option>
     <option value="1" <?php echo $this->input->get_post('status')==='1' ? 'selected="selected"' : '';?>>Active</option>
     <option value="0" <?php echo $this->input->get_post('status')==='0' ? 'selected="selected"' : '';?>>In-active</option>
    </select>
   </p>
   <?php
   if(is_array($categories) && !empty($categories)){
	   ?>
	   <p class="serch-sec1">
	    <select name="category_id" class="form-control">
	     <option value=""> Select <?php echo $heading_title; ?> Status</option>
	     <?php
	     foreach($categories as $cv){
		     ?>
		     <option value="<?php echo $cv['category_id'];?>" <?php echo $this->input->get_post('category_id')==$cv['category_id'] ? 'selected="selected"' : '';?>><?php echo $cv['category_name'];?></option>
		     <?php
	     }?>
	    </select>
	   </p>
	   <?php
   }else{
	   ?>
	   <input type="hidden" name="category_id" value="<?php echo $this->input->get_post('category_id');?>"  />
	   <?php
   }?>
   <p class="serch-sec2">
    <input type="hidden" name="search" value="yes"  />
    <input name="search" type="submit" value="Submit" class="btn1">
   </p>
   <p class="serch-sec2">
    <?php
    if( $this->input->get_post('search')!=''){
	    if($this->input->get_post('category_id')!=''){
		    $search_category=$this->input->get_post('category_id');
		    echo anchor("sitepanel/services?category_id=".$search_category,'<span>Clear Search</span>','class="form-control"');
	    }else{
		    echo anchor("sitepanel/services/",'<span>Clear Search</span>','class="form-control"');
	    }
    }?>
   </p>
   <p class="clearfix"></p>
  </div>
  <hr style="margin-bottom:5px; margin-top:5px;">
  <div>
   <p class="total-results red fs14 b"><?php echo $category_result_found; ?></p>
   <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?></div>
   <p class="clearfix"></p>
  </div>
  <div class="clearfix"></div>
  <?php echo form_close();
  if( is_array($res) && !empty($res)) {
	  echo form_open(current_url_query_string(),'id="data_form"');
	  ?>
	  <div class="p8" id="my_data">
	   <div class="box_style mob_scroll box_h_235">
	    <div class="recent-table">
	     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	       <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
	       <td width="47%" class="fs14"><?php echo $heading_title; ?> Name</td>
	       <td width="10%" class="text-center fs13">Image</td>
	       <td width="14%" class="text-center fs13">View <?php echo $heading_title; ?> Details</td>
		   <td width="12%" class="text-center fs13">Display Order</td>
	       <td width="7%" class="text-center fs13">Status</td>
	       <td width="7%" class="text-center fs13">Action</td>
	      </tr>
	     </table>
	    </div>
	    <p class="clearfix"></p>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <?php
	     $atts = array(
	     'width'      => '740',
	     'height'     => '600',
	     'scrollbars' => 'yes',
	     'status'     => 'yes',
	     'resizable'  => 'yes',
	     'screenx'    => '0',
	     'screeny'    => '0'
	     );
	     $atts_edit = array(
	     'width'      => '525',
	     'height'     => '375',
	     'scrollbars' => 'no',
	     'status'     => 'no',
	     'resizable'  => 'no',
	     'screenx'    => '0',
	     'screeny'    => '0'
	     );
	     foreach($res as $catKey=>$pageVal){
			 $displayorder       = ($pageVal['sort_order']!='') ? $pageVal['sort_order']: "0";
			 $media_option = array('serviceid'=>$pageVal['service_id']);
		$res_photo_media = $this->services_model->get_service_media(5,0, $media_option);
		//trace($res_photo_media);exit;
			 
		     ?>
		     <tr>
		      <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['service_id'];?>" /></td>
		      <td width="47%" class="fs14">
		       <?php echo $pageVal['service_name'];
		       $service_set_in = array();
		       if($pageVal['is_home']!="" && $pageVal['is_home']!='0')
		       $service_set_in[]='<b>Project as Home  : </b> <span class="fa fa-check-circle green fs18"></span>';
		       if(!empty($service_set_in)){
			       echo "<br />";
			       echo implode("<br>",$service_set_in)."<br />";
		       }?>
		       <div id="dialog_<?php echo $pageVal['service_id'];?>" title="Service Description" style="display:none;"><?php echo $pageVal['service_description'];?></div>
		      </td>
			   
		      <td width="10%" class=" fs13" align="center" ><p class="pro_pic"><span><img src="<?php echo get_image('services',$res_photo_media[0]['media'],62,62);?>" alt=""></span></p>
			  <?php	 $j=1;
			 // $service_img_auto_id  = @$res_photo_media[$j]['id'];
		  $product_path = "services/".$res_photo_media[0]['media'];
		  //trace($pageVal);
		?>
         <a href="#"  onclick="$('#dialogimg_<?php echo $pageVal['service_id'];?>').dialog({width:'450px'});">View Image </a>
         <div id="dialogimg_<?php echo $pageVal['service_id'];?>" title="View Image" style="display:none;">
         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  /> </div>	
			  
			  </td>
		      <td width="14%" class="text-center fs13" ><a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $pageVal['service_id'];?>').dialog( {width: 650} );">View Description</a></td>
			  <td width="12%" class="text-center fs13"><input type="text" name="ord[<?php echo $pageVal['service_id'];?>]" value="<?php echo $displayorder;?>" size="5" class="w50 mt10 select-style"  /></td>
		      <td width="7%" class="text-center fs13"><span title="<?php echo ($pageVal['service_status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['service_status']==1)? "green2":"red";?> fs20 mt8"></span></td>
		      <td width="7%" class="text-center grey"><?php echo anchor("sitepanel/services/edit/$pageVal[service_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');?></td>
		     </tr>
		     <?php
	     }
	     if($page_links!=''){
		     ?>
		     <tr><td colspan="9" align="center" height="30"><?php echo $page_links; ?></td></tr>
		     <?php
	     }?>
	    </table>
	   </div>
	   <div class="mt15 ">
	    <p class=" mt10 serch-sec8 fl">
	     <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onclick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
	     <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onclick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
	     <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onclick="return validcheckstatus('arr_ids[]','delete','Record');"/>
		 
		 <input name="update_order" type="submit"  value="Update Order" class="btn1" />
	    </p>
	 <?php /*  <p class=" ml20 serch-sec8 fl"><?php echo form_dropdown("set_as",$this->config->item('service_set_as_config'),$this->input->post('set_as'),'class="w15 mt10 form-control " style="width:175px;" onchange="return onclickgroup()"'); ?></p>
	    <p class=" ml20 serch-sec8 fl"><?php echo form_dropdown("unset_as",$this->config->item('service_unset_as_config'),$this->input->post('unset_as'),'class="w15 mt10  form-control " style="width:175px;" onchange="return onclickgroup()"');?></p>
	    <p class="clearfix"></p>*/?>
	   </div>
	  </div>
	  <?php
	  echo form_close();
  }else{
	  echo "<center><strong> No record(s) found !</strong></center>" ;
  }?>
 </div>
</div>
<!--Body End-->
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer');?>