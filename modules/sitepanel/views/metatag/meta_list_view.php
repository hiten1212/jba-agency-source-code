<?php $this->load->view('includes/header');?>
<?php 
echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End--> 

<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title;?></h1>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:15px;">
    <?php
   validation_message();
   error_message();
   echo form_open("sitepanel/meta/",'id="form" method="get" ');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="URL">
      </p>
      <p class="serch-sec6">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
      <?php if($this->input->get_post('keyword')!=''){ echo anchor("sitepanel/meta/",'<span>Clear Search</span>'); }?>
      </p>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div>
     
      <div class="records fs14"> Records Per Page :
       <?php echo display_record_per_page();?>
      </div><p class="clearfix"></p>
    </div>
    <div class="clearfix"></div>
    <?php echo form_close();
   if( is_array($pagelist) && !empty($pagelist) ) {
	   echo form_open("sitepanel/meta/",'id="myform"');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll box_h_235">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="35%" class="fs14">URL</td>
              <td width="40%" class="text-center fs13">Meta Details</td>
              <td width="12%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <?php
	     foreach($pagelist as $catKey=>$pageVal){
		     ?>
          <tr>
            
            <td width="35%" class="fs14"> <?php echo base_url().$pageVal['page_url'];?></td>
            <td width="40%" class=" fs13">
           <p> <strong> Title  : </strong> <?php echo $pageVal['meta_title'];?> </p>
		       <p> <strong> Keyword  : </strong> <?php echo$pageVal['meta_keyword'];?> </p>
		       <p> <strong> Description   : </strong> <?php echo $pageVal['meta_description'];?></p>
            </td>
            <td width="12%" class="text-center grey"><?php echo anchor("sitepanel/meta/edit/$pageVal[meta_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');?></td>
          </tr>
             <?php
	     }?>
         
         <?php
		 if($page_links!=''){
			 ?>
         <tr><td colspan="3" align="right" height="30"><?php echo $page_links; ?></td></tr>
          <?php }?>
          
        </table>
      </div>
      
    </div>
      <?php echo form_close();
  }else{
	  echo "<center><strong> No record(s) found !</strong></center>" ;
  }?>
  </div>
</div>
<?php $this->load->view('includes/footer');?>