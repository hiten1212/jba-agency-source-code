<?php $this->load->view('includes/header'); ?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/meta';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
  
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <?php
   validation_message();
   error_message();
   echo form_open(current_url_query_string());?>
   
  	<p class="form_title1"><span class="required">*</span> URL :</p>
    <div class="form_field form_field2"><input type="text" value="<?php echo base_url();?>" style="width:350px" class="form-control"  readonly="readonly" size="36"/><br />
      <input type="text" name="page_url" size="40" style="width:550px" class="form-control" value="<?php echo set_value('page_url',$res['page_url']);?>" readonly="readonly"> </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Title :</p>
    <div class="form_field form_field2"><textarea name="meta_title" rows="10" cols="50" id="title" class="form-control" style="width:525px;" ><?php echo set_value('meta_title',$res['meta_title']);?></textarea>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Keywords :</p>
    <div class="form_field form_field2"><textarea name="meta_keyword" rows="10" cols="50" id="keyword" class="form-control" style="width:525px;" ><?php echo set_value('meta_keyword',$res['meta_keyword']);?></textarea>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Description :</p>
    <div class="form_field form_field2"><textarea name="meta_description" rows="10" cols="50" id="description" class="form-control" style="width:525px;" ><?php echo set_value('meta_description',$res['meta_description']);?></textarea>
    </div>
    <p class="clearfix"></p>
    
    
     
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Save" class="btn1" />
      <input type="hidden" name="meta_id" value="<?php echo $res['meta_id'];?>"  />
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
  
</div>
</div>
<!--Body End--> 
<?php $this->load->view('includes/footer'); ?>