<?php $this->load->view('includes/header'); ?>  

 <?php  $back_link = 'sitepanel/newsletter';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
    
  <div class="mt10 p8">
  
  <div class="box_style" >
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?>
        </div>
     <?php error_message();validation_message(); ?>
    
  
  
    <?php echo form_open('sitepanel/newsletter/send_mail/');?>  
    <p class="form_title1"> To :</p>
    <div class="form_field form_field2"><input name="sendto" disabled="disabled" type="text" class="form-control" style="width:100%" value="<?php echo $this->input->post("sendto");?>">
	</div>
    <p class="clearfix"></p>
    
    <p class="form_title1"> Subject :</p>
    <div class="form_field form_field2"><?php echo $this->input->post("subject") ?></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"> Message :</p>
    <div class="form_field form_field2"><?php echo $this->input->post("message") ?></div>
    <p class="clearfix"></p>
    
	<p class="form_title1"></p>
    <div class="form_field form_field">
    		<input type="submit" name="submit" value="Send Mail" class="btn1" />
			<input type="hidden" name="arr_ids" value="<?php echo $this->input->post("arr_ids");?>">
			<input type="hidden" name="subject" value="<?php echo $this->input->post("subject");?>">
			<input type="hidden" name="message" value="<?php echo set_value('message',$this->input->post("message"));?>">
			
			<input type="hidden" name="sendto" value="<?php echo $this->input->post("sendto");?>">
			<input type="hidden" name="action" value="preview" />
		</div>
    <p class="clearfix"></p>	
     <?php echo form_close(); ?>
  </div>
  </div>
  
</div>
</div>
<?php $this->load->view('includes/footer'); ?>