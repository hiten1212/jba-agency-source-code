<?php $this->load->view('includes/header'); ?>  
<?php  $back_link = 'sitepanel/newsletter';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?>
        </div>
     <?php error_message();validation_message(); ?>
    
  
  
   <?php echo form_open('sitepanel/newsletter/change_status/');?>  
   
  	<p class="form_title1"><span class="required">*</span> To :</p>
    <div class="form_field form_field2"><input name="sendto" readonly="readonly" type="text" class="form-control" style="width:100%" value="<?php echo reduce_multiples($newsresult,",");?>"></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Subject :</p>
    <div class="form_field form_field2"><input name="subject" style="width:650px" type="text" class="form-control" value="<?php echo set_value('subject');?>">
    </div>
    <p class="clearfix"></p>
    
    <div class="form_title1"><span class="required">*</span> Message :</div>
    <div class="form_field form_field2">
      <textarea name="message" id="message" rows="4" class="form-control"><?php echo set_value('message');?></textarea><?php  echo display_ckeditor($ckeditor); ?>
    </div>
    <p class="clearfix"></p>
    
       
    <p class="form_title1"></p>
    <div class="form_field form_field">
   <input type="submit" name="sub" value="Preview" class="btn1" />
			<input type="hidden" name="Send" value="Send Email">
			<input type="hidden" name="arr_ids" value="<?php echo $ids;?>">
			<input type="hidden" name="action" value="preview" />
    </div>
    <p class="clearfix"></p>
     <?php echo form_close(); ?>
  </div>
  </div>
  
</div>
</div>
<!--Body End--> 
<?php $this->load->view('includes/footer'); ?>