<?php $this->load->view('includes/header');  ?>
<?php  $back_link = 'sitepanel/newsletter';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?>
        </div>
       <?php error_message();
   echo form_open(current_url_query_string());?>
   
  	<p class="form_title1"><span class="required">*</span> Name :</p>
    <div class="form_field form_field2"><input name="subscriber_name" type="text" class="form-control" style="width:450px" value="<?php echo set_value('subscriber_name',$catresult->subscriber_name);?>"><?php echo form_error('subscriber_name');?></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Email :</p>
    <div class="form_field form_field2"><input name="subscriber_email" style="width:450px" type="text" class="form-control" value="<?php echo set_value('subscriber_email',$catresult->subscriber_email);?>"><?php echo form_error('subscriber_email');?>
    </div>
    <p class="clearfix"></p>
       
    <p class="form_title1"></p>
    <div class="form_field form_field">
   <input type="submit" name="sub" value="Edit Member" class="btn1" />
      <input type="hidden" name="subscriber_id" value="<?php echo $catresult->subscriber_id;?>">
    </div>
    <p class="clearfix"></p>
     <?php echo form_close(); ?>
  </div>
  </div>
  
</div>
</div>
<!--Body End--> 
<?php $this->load->view('includes/footer'); ?>