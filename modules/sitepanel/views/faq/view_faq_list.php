<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php 


echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End-->
<!--Body-->

<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title;?></h1>
    <p class="add-product"><?php echo anchor("sitepanel/faq/add/",'Add FAQ','' );?></p>
    <p class="clearfix"></p>
    <script type="text/javascript">function serialize_form() { return $('#pagingform').serialize(); } </script>
    <hr style="margin-bottom:5px; margin-top:10px;">
    <?php if(error_message() !='') echo error_message();?>
    <?php echo form_open("sitepanel/faq/",'id="search_form" method="get"');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Question">
      </p>
      <?php
  /* if(is_array($categories) && !empty($categories)){
	   ?>
	   <p class="serch-sec1">
	    <select name="categorys_id" class="form-control">
	     <option value=""> Select Category</option>
	     <?php
	     foreach($categories as $cv){
		     ?>
		     <option value="<?php echo $cv['category_id'];?>" <?php echo $this->input->get_post('categorys_id')==$cv['category_id'] ? 'selected="selected"' : '';?>><?php echo $cv['category_name'];?></option>
		     <?php
	     }?>
	    </select>
	   </p>
	   <?php
   }else{
	   ?>
	   <input type="hidden" name="categorys_id" value="<?php echo $this->input->get_post('categorys_id');?>"  />
	   <?php
   }?>
   
   <?php 
   if(is_array($citys) && !empty($citys)){
	   ?>
	   <p class="serch-sec1">
	    <select name="citys_id" class="form-control">
	     <option value=""> Select City</option>
	     <?php
	     foreach($citys as $cv){
		     ?>
		     <option value="<?php echo $cv['city_id'];?>" <?php echo $this->input->get_post('citys_id')==$cv['city_id'] ? 'selected="selected"' : '';?>><?php echo $cv['city_name'];?></option>
		     <?php
	     }?>
	    </select>
	   </p>
	   <?php
   }else{
	   ?>
	   <input type="hidden" name="citys_id" value="<?php echo $this->input->get_post('citys_id');?>"  />
	   <?php
   }*/ ?>
      
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php if( $this->input->get_post('keyword')!='' || $this->input->get_post('citys_id')!='' || $this->input->get_post('categorys_id')!='' ) echo anchor("sitepanel/faq/",'<span>Clear Search</span>','class="form-control"');?>
      </p>
      <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?> </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div class="clearfix"></div>
    <?php echo form_close();


   if( is_array($pagelist) && !empty($pagelist)){


	   echo form_open("sitepanel/faq/",'id="data_form" ');?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="25%" class="fs14">Question/Answer</td>
              <!--<td width="25%" class="fs14">Category</td>-->
              <td width="12%" class="text-center fs13">Display Order</td>
              <td width="14%" class="text-center fs13">Current Status</td>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php


	     foreach($res as $catKey=>$pageVal){


		     ?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?php echo $pageVal['faq_id'];?>" /></td>
            <td width="25%" class="fs14"><strong> Question -</strong> <?php echo $pageVal['faq_question'];?> <br />
              <strong> Answer -</strong> <a href="javascript:void();"  onclick="$('#dialog_<?php echo $pageVal['faq_id'];?>').dialog( {width: 650} );">View</a>
              <div id="dialog_<?php echo $pageVal['faq_id'];?>" title="Answer" style="display:none;"> <?php echo $pageVal['faq_answer'];?></div></td>
              
              <?php /*<td width="25%" class="fs14"><strong> Category -</strong> <?php 
			  
			 // get_db_field_value('wl_brands','brand_name'," AND brand_id='$brand_id'");
			  
			    echo get_db_field_value("wl_service_categories", "category_name", " AND category_id = '".$pageVal['category_id']."'");?> <br />
              <?php /*<strong>City </strong> <?php   echo get_db_field_value("wl_city", "city_name", " AND city_id = '".$pageVal['city_id']."'");
              </td>*/?>
            <td width="12%" class="text-center fs13"><input type="text" class="w50 mt10 select-style" name="ord[<?php echo $pageVal['faq_id'];?>]" value="<?php echo $pageVal['sort_order'];?>" size="5" />
            </td>
            <td width="14%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)?"Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)?"green2":"red";?> fs20 mt8"></span></td>
            <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/faq/edit/$pageVal[faq_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"');?></td>
          </tr>
          <?php


	     }?>
          <?php


		 if($page_links!=''){


			 ?>
          <tr>
            <td colspan="5" align="center" height="30"><?php echo $page_links; ?></td>
          </tr>
          <?php }?>
        </table>
      </div>
      <p class="mt15">
        <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
        <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
        <input name="update_order" type="submit"  value="Update Order" class="btn1" />
        <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
      </p>
    </div>
    <?php echo form_close();


   }else{


	   echo "<center><strong> No record(s) found !</strong></center>";


   }?> </div>
</div>
<?php $this->load->view('includes/footer');?>
