<?php $this->load->view('includes/header');
$block_path=$inq_type=='Wholesale' ? "enquiry/wholesale/" : "enquiry/";
?>
<div id="content">
 <div class="breadcrumb_sitepanel"><?php echo anchor('sitepanel/dashbord','Home'); ?> &raquo; <?php echo anchor('sitepanel/enquiry/products/','Enquiry Listing'); ?> &raquo; <?php echo $heading_title;?></div>
 <div class="box">
  <div class="heading">
   <h1><img src="<?php echo base_url(); ?>assets/sitepanel/image/category.png" alt="" /> <?php echo $heading_title; ?></h1>
   <div class="buttons">&nbsp;</div>
  </div>
  <div class="content">
   <?php echo error_message();
   echo form_open("",'id="search_form"');?>
   <div align="right" class="breadcrumb_sitepanel"> Records Per Page : <?php echo display_record_per_page();?> </div>
  <?php /*?> <table width="100%"  border="0" cellspacing="3" cellpadding="3" >
    <tr>
     <td align="center" ><strong>Search</strong> [  name,email ]
      <input type="text" name="keyword" value="<?php echo $this->input->get_post('keyword');?>"  />&nbsp;
      <a  onclick="$('#search_form').submit();" class="button"><span> GO </span></a>
      <?php
      if($this->input->get_post('keyword')!=''){
	      echo anchor("sitepanel/enquiry/".$this->uri->rsegment(2),'<span>Clear Search</span>');
      }?>
     </td>
    </tr>
   </table><?php */?>
   <?php echo form_close(); 
   if( is_array($res) && !empty($res) ){
	   echo form_open("",'id="data_form"');?>
	   <table class="list" width="100%" id="my_data">
	    <thead>
	     <tr>
	      <td width="62" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').attr('checked', this.checked);" /></td>
	      <td width="1256" class="left">Detail</td>
	      </tr>
	    </thead>
	    <tbody>
	     <?php
			 foreach($res as $catKey=>$res){
				 $address_details=array();
				 ?>
				 <tr>
				  <td style="text-align: center;" valign="top"><input type="checkbox" name="arr_ids[]" value="<?php echo $res['id'];?>" /></td>
				  <td class="left" valign="top">				  
				   <?php
				   if($res['product_name']!="" && $res['product_name']!='0'){
					   $address_details[]="<b>Product Name : </b>".$res['product_name'];
				   }
				   if($res['product_code']!="" && $res['product_code']!='0'){
					   $address_details[]="<b>Product Code : </b>".$res['product_code'];
				   }
				   if($res['qty']!="" && $res['qty']!='0'){
					   $address_details[]="<b>Quantity : </b>".$res['qty'];
				   }
				   if($res['brand']!="" && $res['brand']!='0'){
					   $address_details[]="<b>Brand : </b>".nl2br($res['brand']);
				   }
				   
				   if($res['color']!="" && $res['color']!='0'){
					   $address_details[]="<b>Color : </b>".ucwords($res['color']);
				   }
				   if($res['size']!="" && $res['size']!='0'){
					   $address_details[]="<b>Size : </b>".ucwords($res['size']);
				   }
				   
				   if(!empty($address_details)){
					   echo implode("<br>",$address_details)."<br />";
				   }
				  ?>
				  </td>
			     </tr>
				 <?php
			 }?>
			</tbody>
		 </table>
		 <?php
		 if($page_links!=''){
			 ?>
			 <table class="list" width="100%">
			  <tr><td align="right" height="30"><?php echo $page_links; ?></td></tr>
			 </table>
			 <?php
		 }?>
		 <table class="list" width="100%">
		  <tr>
		   <td align="left" style="padding:2px" height="35">
                      
		    <input name="status_action" type="submit" class="button2" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
		   </td>
		  </tr>
		 </table>
		 <?php echo form_close();
	 }else{
		 echo "<center><strong> No record(s) found !</strong></center>" ;
	 }?>
	</div>
 </div>	
</div>
<?php $this->load->view('includes/footer'); ?>