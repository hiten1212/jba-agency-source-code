<?php
$curr_sec_val=$this->input->post('section') ? $this->input->post('section') :  $res->banner_page;
$curr_position_val=$this->input->post('banner_position') ? $this->input->post('banner_position') :  $res->banner_position;
?>
<?php $this->load->view('includes/header'); ?>  
 <?php  $back_link = 'sitepanel/banners';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>

<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?>
        </div>
       <?php echo validation_message('');?>
<?php echo error_message(); ?> 
<?php echo form_open_multipart(current_url_query_string());?>  
   
   <p class="form_title1"><span class="required">*</span> Banner Position :</p>
   <div class="form_field form_field2">
    <div id="ban_postion">
    <select name="section" id="section" class="form-control" style="width:450px"><?php /*?>onchange="change_ban_postions(this.value);"<?php */?>
			<option value="">Select Section</option>
			<?php 
			foreach ($this->config->item('bannersections')  as $key=>$val)
			{
				$sel = ($curr_sec_val==$key ) ? "selected" : "";
				?> 
				<option value="<?php echo $key;?>" <?php echo $sel;?> ><?php echo $val ;?></option> 
			<?php 
			} 
			?>  
		</select>
     	</div>    
     </div>
    <p class="clearfix"></p>   
    
  	<?php /*?><p class="form_title1"><span class="required">*</span> Banner Position :</p>
    <div class="form_field form_field2">
    <div id="ban_postion">
                
                <?php echo banner_postion_drop_down('banner_position',set_value('banner_postion',$res->banner_position),set_value('section',$res->banner_page),'class="form-control" style="width:450px"');?>
					                    </div>
    
       </div>
    <p class="clearfix"></p><?php */?>
    
     <p class="form_title1"><span class="required">*</span> Banner Image :</p>
    <div class="form_field form_field2"><input name="image1" id="image1" type="file" style="width:450px" class="form-control">
	<?php
		 $j=1;
		 $product_path = "banner/".$res->banner_image;
		?>
         <a href="#"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a>
         <div id="dialog_<?php echo $j;?>" title="Banner Image" style="display:none;">
         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  />
         </div>
         </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required"></span> Banner Url :</p>
    <div class="form_field form_field2"><input name="banner_url" type="text" style="width:450px" class="form-control" value="<?php echo set_value('banner_url',$res->banner_url);?>"></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Update Banner" class="btn1" />
		<input type="hidden" name="action" value="addbanner" />
   
       </div>
    <p class="clearfix"></p>
  <?php echo form_close(); ?>  
  </div>
  </div>
  
</div>
</div>
<script type="text/javascript">
function change_ban_postions(){
	var section = $('[name="section"]').val();
	if(section != '' && section != 'undefined')
	{
		$.ajax({
				  type: "POST",
				  url: "<?php echo base_url();?>sitepanel/banners/ajx_ban_postions",
				  data: { banner_section : section }
				}).done(function( data ) {
				  $('#ban_postion').html(data);
				});
		
		
	}
	return false;
	
}
</script>
<?php $this->load->view('includes/footer'); ?>