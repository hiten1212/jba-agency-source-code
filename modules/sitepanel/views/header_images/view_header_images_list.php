<?php $this->load->view('includes/header');



echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End-->
<!--Body-->

<div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title;?></h1>
    <p class="add-product"><?php echo anchor("sitepanel/header_images/add/",'Add Header Home Image','class="button" ' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
    <?php



  if(error_message() !=''){



	  echo error_message();



  }



  echo form_open("sitepanel/header_images/",'id="form" method="get" ');?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1">
        <select name="status" class="form-control">
          <option value="">Status</option>
          <option value="1" <?php echo $this->input->get_post('status')==='1' ? 'selected="selected"' : '';?>>Active</option>
          <option value="0" <?php echo $this->input->get_post('status')==='0' ? 'selected="selected"' : '';?>>In-active</option>
        </select>
      </p>
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      <p class="serch-sec2">
        <?php



		if($this->input->get_post('status')!=''){



			echo anchor("sitepanel/header_images/",'<span>Clear Search</span>','class="form-control"');



		}?>
      </p>
      <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?> </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div class="clearfix"></div>
    <?php echo form_close();



  $j=0;



  if( is_array($res) && !empty($res)){



	  echo form_open("sitepanel/header_images/",'id="myform"');



	  ?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="2%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="67%" class="fs14">Header Home Image</td>
              <?php /*?><td width="25%" class="text-center fs13">Line One</td>
              <td width="20%" class="text-center fs13">Line Two</td>
              <td width="12%" class="text-center fs13">Line Three</td>
                <td width="12%" class="text-center fs13">Line Four</td>
           <td width="12%" class="text-center fs13">Line Five</td><?php */?>
              <td width="10%" class="text-center fs13">Status</td>
              <td width="10%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php
	     foreach($res as $catKey=>$pageVal){
		     ?>
          <tr>
            <td width="2%"><input type="checkbox" name="arr_ids[]" value="<?=$pageVal['id'];?>" /></td>
            <td width="65%" class="fs14" align="center"><img src="<?php echo get_image('header_images',$pageVal['header_image'],140,66,'AR');?>" alt="">
              <?php
		       $j=1;
		       $product_path = "header_images/".$pageVal['header_image'];
		       ?>
              <br/>
              <br/>
              <a href="javascript:void(0);" onclick="$('#dialog_<?php echo $pageVal['id'];?>').dialog({width:'auto'});">View Image </a>
              <div id="dialog_<?php echo $pageVal['id'];?>" title="Header Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/'.$product_path;?>" width="350" /> </div></td>
            <?php /*?><td width="25%" class="text-center fs13"><?php echo $pageVal['line_one'];?></span></td>
            <td width="20%" class="text-center fs13"><?php echo $pageVal['line_two'];?></td>
           <td width="12%" class="text-center fs13"><?php echo $pageVal['line_three'];?></td>
            
               <td width="12%" class="text-center fs13"><?php echo $pageVal['line_four'];?></td>
              <td width="12%" class="text-center fs13"><?php echo $pageVal['line_five'];?></td><?php */?>
            <td width="10%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="10%" class="text-center grey"><?php echo anchor("sitepanel/header_images/edit/$pageVal[id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>
          </tr>
          <?php
		     $j++;
	     }
     
	     if($page_links!=''){
		     ?>
          <tr>
            <td colspan="7" align="center" height="30"><?php echo $page_links; ?></td>
          </tr>
          <?php



	     }?>
        </table>
      </div>
      <p class="mt15">
        <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
        <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
        <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
      </p>
    </div>
    <?php



	  echo form_close();



  }else{



	  echo "<center><strong> No record(s) found !</strong></center>";



  }?>
  </div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer');?>
