<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/header_images';



echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->

<div class="container-fluid">
  <div class="mid_area">
    <div class="mt10 p8">
      <div class="box_style">
        <div class="recent-table p10 mb15"><?php echo $heading_title; ?></div>
        <?php //echo validation_message('');



    echo form_open_multipart('sitepanel/header_images/add/');?>
        <p class="form_title1"><span class="required">*</span> Header Home Image :</p>
        <div class="form_field form_field2">
          <input name="image1" id="image1" type="file" style="width:450px" class="form-control">
          [ <?php echo $this->config->item('slide.best.image.view');?> ]<?php echo form_error('image1');?></div>
        <p class="clearfix"></p>
        <p class="form_title1"><span class="required"></span> Url :</p>
        <div class="form_field form_field2">
          <input name="header_url" type="text" style="width:450px" class="form-control" value="<?php echo set_value('header_url');?>">
        </div>
        <p class="clearfix"></p>
        <!--<p class="form_title1"><span class="required"></span> First Line Content :</p>
        <div class="form_field form_field2">
          <input name="line_one" type="text" style="width:650px" class="form-control" value="<?php //echo set_value('line_one');?>" maxlength='30'>
          A maximum of 30 characters<?php //echo form_error('line_one');?></div>
        <p class="clearfix"></p>
		<p class="form_title1"><span class="required"></span> Text colour :</p>
        <div class="form_field form_field2">
          <input name="text_colour" type="color" style="width:450px;height:45px" class="form-control" value="<?php //echo set_value('text_colour');?>">
        </div>
        <p class="clearfix"></p>-->
        <?php /*?><p class="form_title1"><span class="required"></span> Second Line Content :</p>
        <div class="form_field form_field2">
          <input name="line_two" type="text" style="width:650px" class="form-control" value="<?php echo set_value('line_two');?>" maxlength='20'>
          A maximum of 20 characters<?php echo form_error('line_two');?></div>
        <p class="clearfix"></p>
         <p class="form_title1"><span class="required"></span> Third Line Content :</p>
    <div class="form_field form_field2"><input name="line_three" type="text" style="width:650px" class="form-control" value="<?php echo set_value('line_three');?>" maxlength='30'> A maximum of 30 characters<?php echo form_error('line_three');?></div>

    <p class="clearfix"></p> 
       

      <p class="form_title1"><span class="required"></span> Fourth Line Content :</p>



    <div class="form_field form_field2"><input name="line_four" type="text" style="width:650px" class="form-control" value="<?php echo set_value('line_four');?>" maxlength='500'> A maximum of 500 characters<?php echo form_error('line_four');?></div>



    <p class="clearfix"></p>



    



     <p class="form_title1"><span class="required"></span> Fifth Line Content :</p>



    <div class="form_field form_field2"><input name="line_five" type="text" style="width:650px" class="form-control" value="<?php echo set_value('line_five');?>" maxlength='500'> A maximum of 500 characters<?php echo form_error('line_five');?></div>



    <p class="clearfix"></p><?php */?>
        <p class="form_title1"></p>
        <div class="form_field form_field">
          <input type="submit" name="sub" value="Add Header Image" class="btn1" />
          <input type="hidden" name="action" value="add" />
        </div>
        <p class="clearfix"></p>
        <?php echo form_close(); ?> </div>
    </div>
  </div>
</div>
<?php $this->load->view('includes/footer');?>
