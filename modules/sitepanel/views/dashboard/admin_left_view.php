<?php
 $menu_array=lang('top_menu_list');
 $top_menu_icon=lang('top_menu_icon');
 if(array_key_exists('Dashboard',$menu_array)) unset($menu_array['Dashboard']);
 $total_menu=count($menu_array);
 if($total_menu > 0){
	 foreach($menu_array as $key=>$value){
		 
		 $icon = array_key_exists( $key, $top_menu_icon) ? $top_menu_icon[$key] : "";
		$key1=str_replace(' ','',$key);
		 if(is_array($value)){  ?>
		 <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo<?php echo $key1;?>">
                <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?php echo $key1;?>" aria-expanded="false" aria-controls="collapse<?php echo $key1;?>">
				<?php  if($icon!=''){ ?>
					<img src="<?php echo base_url(); ?>assets/sitepanel/image/<?php echo $icon;?>" alt="">
					<?php }?> <span><i class="more-less fa fa-angle-down"></i> <?php echo $key;?></span></a>
                </h4>
            </div>
            <div id="collapseTwo<?php echo $key1;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo<?php echo $key1;?>">
                <div class="panel-body">
                <ul class="lft_links">
            <?php      foreach($value as $k=>$val){ ?>
                <li><?php echo anchor($val,$k);?></li>
                <?php }?>
                </ul>
              </div>
            </div>
        </div>
		<?php 
		
		}else{
			 $attr="class='top'";
			 echo '<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne'.$key1.'">
                <h4 class="panel-title">'.anchor($value,$key,$attr).'</h4>
            </div>
        </div> ';
		 }
		
	 }
 }?>
