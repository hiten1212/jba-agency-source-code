<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php 



//echo admin_breadcrumb($heading_title);

 ?>

<div class="breadcrumb">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="active">Dashboard</li>
    </ol>
  </div>
</div>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
  <div class="mid_area">
    <div style="float:left; width:50%">
   <h1><img src="<?php echo base_url(); ?>assets/sitepanel/image/dashboard.png" alt=""> Dashboard<br><span>Welcome to <?php  echo $this->config->item("site_admin"); ?> Administrator Area</span></h1>
  </div>
  <?php /* <div style="float:right">
   <div style="font-size:14px; color: #000;text-align: center;text-transform: uppercase;"><b style="Color:red">Escalate Your Complaint</b></div>
   <div style="font-size:14px;color: #000;text-align: center;text-transform: ; Color:red">If not satisfied with your Support Manager <br>Email: support@webxcreation.net Call: +91 80099 88788</div>
  </div>*/ ?>
  
  <div style="clear:both;"></div>
    <?php /*?><div style="float:right">
      <div style="font-size:14px; color: #000;text-align: center;text-transform: uppercase;"><b style="Color:red">Escalate Your Complaint</b></div>
      <div style="font-size:14px;color: #000;text-align: center;text-transform: ; Color:red">If not satisfied with your Support Manager <br>
        Email: rm@weblinkindia.net Call: +91 8929175310</div>
    </div><?php */?>
    <div style="clear:both;"></div>
    <div class="mt15">
      <?php /*?><div class="col-lg-3 col-sm-6 p8">
        <div class="count_box"><img src="<?php echo base_url(); ?>assets/sitepanel/image/total-ico2.png" alt="">
          <div><b><?php echo $total_categories;?></b><br>
            Total Categories</div>
          <p class="clearfix"></p>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 p8">
        <div class="count_box"><img src="<?php echo base_url(); ?>assets/sitepanel/image/total-ico2.png" alt="">
          <div><b><?php echo $total_products;?></b><br>
            Total Products</div>
          <p class="clearfix"></p>
        </div>
      </div><?php */?>
      <div class="col-lg-3 col-sm-6 p8">
        <div class="count_box"><img src="<?php echo base_url(); ?>assets/sitepanel/image/total-ico4.png" alt="">
          <div><b><?php echo  $total_enquiries; ?></b><br>
            Total Enquiry</div>
          <p class="clearfix"></p>
        </div>
      </div>
     
      <p class="clearfix"></p>
    </div>
    
    <p class="clearfix"></p>
    <div class="mt10 col-lg-3 col-md-12 p8">
      <div class="box_style mob_scroll box_h_372">
        <p class="heading">recently recieved enquiries</p>
        <p class="fs14 mt12 fr mr15 grey">Total Enquiries : <span class="green"><?php echo  $total_enquiries; ?></span></p>
        <p class="clearfix"></p>
        <p class="mt10 bb"></p>
        <p class="text-center mt40 mb40"><img src="<?php echo base_url(); ?>assets/sitepanel/image/enquiry.png" alt=""></p>
      </div>
    </div>
    <?php   /*if( is_array($recently_added_newsletter_members) && !empty($recently_added_newsletter_members) ){ ?>
    <div class="mt10 col-lg-4 col-md-12 p8">
      <div class="box_style mob_scroll box_h_372">
        <p class="heading">recently added Newsletter subscriber</p>
        <p class="fs14 mt5 fr mr15 grey">Total Subscriber : <span class="green"><?php echo $total_newsletter_members;?></span></p>
        <p class="clearfix"></p>
        <p class="mt10 bb"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php 		
					foreach($recently_added_newsletter_members as $catKey=>$pageVal)
					{
						
				  	 	?>
          <tr valign="top">
            <td width="4%"><i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i></td>
            <td width="72%"><p class="fs14"><?php echo $pageVal['subscriber_name'];?></p>
              <p class="fs13 mt5 grey"><?php echo $pageVal['subscriber_email'];?></p></td>
            <td width="24%" class="text-center"><?php //echo anchor("sitepanel/newsletter/edit/$pageVal[subscriber_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i> ','title="Edit"');?>
              <span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)?"green2":"red";?> fs20 ml15"></span> </td>
          </tr>
          <?php }?>
        </table>
      </div>
    </div>
    <?php }*/?>
    <p class="clearfix"></p>
    <?php   /*if( is_array($recently_added_testimonials) && !empty($recently_added_testimonials) ){ ?>
    <div class="mt10 col-lg-6 col-md-12 p8">
      <div class="box_style mob_scroll" style="min-height:205px;">
        <p class="total_count">Total Testimonials : <span class="green"><?php echo  $total_testimonials;?></span></p>
        <p class="heading">recently recieved Testimonials</p>
        <p class="clearfix"></p>
        <p class="mt10 bb"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php foreach($recently_added_testimonials as $catKeyt=>$pageValt){?>
          <tr valign="top">
            <td width="4%"><i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i></td>
            <td width="72%"><p class="fs14"><?php echo ucwords($pageValt['poster_name']);?></p>
              <p class="fs13 mt5 grey"><?php echo $pageValt['email'];?></p></td>
            <td width="24%" class="text-center"><?php //echo anchor("sitepanel/testimonial/edit/$pageValt[testimonial_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit" class="ml15"'); ?>
              <span title="<?php echo ($pageValt['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageValt['status']==1)? "green2":"red";?> fs20 ml15"></span> </td>
          </tr>
          <?php }?>
        </table>
      </div>
    </div>
    <?php } */?>
    <?php



if(is_array($recently_added_banners) && !empty($recently_added_banners) ){

/*

	?>
    <div class="mt10 col-lg-6 col-md-12 p8">
      <div class="box_style mob_scroll" style="min-height:205px;">
        <p class="total_count">Total Banner : <span class="green"><?php echo  $total_banners; ?></span></p>
        <p class="heading">recently added Banner</p>
        <p class="clearfix"></p>
        <p class="mt10 bb"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <?php



   foreach($recently_added_banners as $val){



	   ?>
          <tr valign="top">
            <td width="72%" class="fs14"><?php echo ucwords(str_replace("_"," ",$val['banner_page']));?></td>
            <td width="24%" class="text-center"><span title="<?php echo ($val['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($val['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
          </tr>
          <?php



   }



   ?>
        </table>
      </div>
    </div>
    <?php */



}



?>
    <p class="clearfix"></p>
  </div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer');?>
<script>$(window).load(function(){



	var data = [



    ['Pending', 12],['Ready For Dispach', 9], ['Delivered', 14], 



    ['Canceled', 16],['Returned', 7]



  ];



   



  var plot4 = $.jqplot('Graph1', [data], {



	gridPadding: {top:0, bottom:0, left:0, right:0},



    seriesDefaults: {



      renderer:$.jqplot.DonutRenderer,



      rendererOptions:{



        sliceMargin:0,startAngle:-90,showDataLabels:1,dataLabels:'value',totalLabel:1,shadow:0,padding:0,innerDiameter:160



      }



    },legend:{show:true,location:'e'},grid:{shadow:0,background:'#fff',borderWidth:0}



  });



})</script>
