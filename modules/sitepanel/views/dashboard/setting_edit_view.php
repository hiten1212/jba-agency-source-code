<?php $this->load->view('includes/header');?>
<?php
echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
<div class="mid_area">
  <div class="mt10 p8">
  <div class="box_style">
	<div class="recent-table p10 mb15">
         <?php echo $heading_title; ?>
        </div>
        <?php echo validation_message();
   echo error_message();
   echo form_open_multipart('sitepanel/setting/edit/');?>
  	<p class="form_title1"><span class="required">*</span>Email :</p>
    <div class="form_field form_field2"><input name="admin_email" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('admin_email',$admin_info->admin_email);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
		<?php /*<p class="form_title1"><span class="required"></span>Email2 :</p>
    <div class="form_field form_field2"><input name="enquiry_email" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('enquiry_email',$admin_info->enquiry_email);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>*/?> 

    <p class="form_title1"><span class="required">*</span>Phone :</p>
    <div class="form_field form_field2"><input name="phone" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('phone',$admin_info->phone);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p> 



<!--<p class="form_title1"><span class="required">*</span> Header Video :</p>-->
	    <div class="form_field form_field2">
	     <!--<input type="file" name="header_video" class="form-control" style="width:450px;"/>-->
	     <?php
	     //if($admin_info->header_video!='' && file_exists(UPLOAD_DIR."/logo/".$admin_info->header_video)){
		     ?>
		     <!--<a href="javascript:void(0);"  onclick="$('#dialog3').dialog({width:'auto'});">View</a>-->
		     <?php
	     //}?>
	     <!--<br />[ <?php //echo $this->config->item('slide.best.video.view');?> ]--> 
	     <div id="dialog3" title="Header Logo" style="display:none;"> 
		 <video width="" muted="" autoplay="" loop="">
                                  <source src="<?php echo base_url().'uploaded_files/logo/'.$admin_info->header_video;?>" type="video/mp4">
                                  <source src="<?php echo theme_url(); ?>images/slider_video.ogg" type="video/ogg">
                                  Your browser does not support HTML5 video.
                                </video>
		 
		  </div>
	    </div> 
	    <p class="clearfix"></p>	

<p class="form_title1"><span class="required">*</span> Header Logo :</p>
	    <div class="form_field form_field2">
	     <input type="file" name="header_logo" class="form-control" style="width:450px;"/>
	     <?php
	     if($admin_info->header_logo!='' && file_exists(UPLOAD_DIR."/logo/".$admin_info->header_logo)){
		     ?>
		     <a href="javascript:void(0);"  onclick="$('#dialog1').dialog({width:'auto'});">View</a>
		     <?php
	     }?>
	     <br />[ <?php echo $this->config->item('header_logo.best.image.view');?> ] 
	     <div id="dialog1" title="Header Logo" style="display:none;"> <img src="<?php echo base_url().'uploaded_files/logo/'.$admin_info->header_logo;?>"  /> </div>
	    </div> 
	    <p class="clearfix"></p>

            <p class="form_title1">Logo background :</p>
            <div class="form_field form_field2"><input name="logo_background" type="color"  value="<?php echo set_value('logo_background',$admin_info->logo_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
            <p class="clearfix"></p>
	    
	    <p class="form_title1"><span class="required">*</span> Portfolio Logo :</p>
	    <div class="form_field form_field2">
	     <input type="file" name="footer_logo" class="form-control" style="width:450px;"  />
	     <?php
	     if($admin_info->footer_logo!='' && file_exists(UPLOAD_DIR."/logo/".$admin_info->footer_logo)){
		     ?>
		     <a href="javascript:void(0);"  onclick="$('#dialog2').dialog({width:'auto'});">View</a>
		     <?php
	     }?>
	     <br />[ <?php echo $this->config->item('footer_logo.best.image.view');?> ] 
	     <div id="dialog2" title="Header Logo" style="display:none;"> <img src="<?php echo base_url().'uploaded_files/logo/'.$admin_info->footer_logo;?>"  /> </div>
	    </div> 
	    <p class="clearfix"></p>	
	
	<?php /*<p class="form_title1"><span class="required"></span>Phone 2:</p>
    <div class="form_field form_field2"><input name="contact_phone" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('contact_phone',$admin_info->contact_phone);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>*/?> 
   
  
    <p class="form_title1"><span class="required">*</span>Address :</p>
    <div class="form_field form_field2"><textarea name="address" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('address',$admin_info->address);?></textarea></div>    
    <p class="clearfix"></p>
    
    
    
    <p class="form_title1">Google Map Code :</p>
    <div class="form_field form_field2"><textarea name="domestic_google_map" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('domestic_google_map',$admin_info->domestic_google_map);?></textarea></div>
    <p class="clearfix"></p>
    
     <?php /*<p class="form_title1"><span class="required"></span>Contact details :</p>
    <div class="form_field form_field2"><textarea name="contact_details" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('contact_details',$admin_info->contact_details);?></textarea></div>    
    <p class="clearfix"></p>
	<p class="form_title1">Fax :</p>
    <div class="form_field form_field2"><input name="fax" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('fax',$admin_info->fax);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>*/?>
   
    
     <p class="form_title1">Whatsapp Number :</p>
    <div class="form_field form_field2"><input name="whatsapp_number" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('whatsapp_number',$admin_info->whatsapp_number);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    <p class="form_title1">Whatsapp Text :</p>
    <div class="form_field form_field2"><input name="whatsapp_text" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('whatsapp_text',$admin_info->whatsapp_text);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<p class="form_title1">Header background :</p>
    <div class="form_field form_field2"><input name="header_background" type="color"  value="<?php echo set_value('header_background',$admin_info->header_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<p class="form_title1">Footer background :</p>
    <div class="form_field form_field2"><input name="footer_background" type="color"  value="<?php echo set_value('footer_background',$admin_info->footer_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<p class="form_title1">Portfolio list background :</p>
    <div class="form_field form_field2"><input name="portfolio_list_background" type="color"  value="<?php echo set_value('portfolio_list_background',$admin_info->portfolio_list_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<p class="form_title1">Portfolio detail background :</p>
    <div class="form_field form_field2"><input name="portfolio_detail_background" type="color"  value="<?php echo set_value('portfolio_detail_background',$admin_info->portfolio_detail_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<p class="form_title1">Start project background :</p>
    <div class="form_field form_field2"><input name="start_project_background" type="color"  value="<?php echo set_value('start_project_background',$admin_info->start_project_background);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <p class="form_title1">Start project text :</p>
    <div class="form_field form_field2"><input name="start_project_text" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('start_project_text',$admin_info->start_project_text);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
    
     <p class="form_title1">Portfolio Menu Text :</p>
    <div class="form_field form_field2"><input name="portfolio_menu_text" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('portfolio_menu_text',$admin_info->portfolio_menu_text);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
     <p class="form_title1">Clients Menu Text :</p>
    <div class="form_field form_field2"><input name="clients_menu_text" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('clients_menu_text',$admin_info->clients_menu_text);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
    <?php if($this->admin_type==1){?>
    <?php /*?><p class="form_title1"><span class="required">*</span> Website :</p>
    <div class="form_field form_field2"><input name="website" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('website',$admin_info->website);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p> <?php */?>
    
     <?php /*?> <p class="form_title1"><span class="required">*</span> Mobile :</p>
    <div class="form_field form_field2"><input name="contact_phone" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('contact_phone',$admin_info->contact_phone);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p><?php */?> 
    
   <?php /*?> <p class="form_title1"><span class="required"></span> City :</p>
    <div class="form_field form_field2"><input name="city" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('city',$admin_info->city);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <p class="form_title1">State :</p>
    <div class="form_field form_field2"><input name="state" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('state',$admin_info->state);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p> 
    
     <p class="form_title1"><span class="required"></span> Zipcode :</p>
    <div class="form_field form_field2"><input name="zipcode" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('zipcode',$admin_info->zipcode);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p> 

    <p class="form_title1"><span class="required"></span> Country :</p>
    <div class="form_field form_field2"><input name="country" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('country',$admin_info->country);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p><?php */?>
    
    
<?php /*?> 
	 <p class="form_title1"><span class="required"></span> Brochure :</p>
    <div class="form_field form_field2"><input name="brochure" type="file" class="form-control" style="width:450px;" value="" <?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>>
    <?php
	if($admin_info->brochure!='' && file_exists(UPLOAD_DIR."/brochure/".$admin_info->brochure)){
			?>
			<a href="<?php echo base_url();?>uploaded_files/brochure/<?php echo $admin_info->brochure;?>" target="_blank">View</a>
  	<?php
	}?>
	<br />[ ( File should be xls,xlsm format and file size should not be more then 2 MB (2048 KB)) ] 
        <div id="dialog" title="" style="display:none;">
            <?php echo base_url().'uploaded_files/brochure/'.$admin_info->brochure;?>
        </div>
    </div>
<?php */?>

    <p class="clearfix"></p>
     <p class="form_title1">Facebook Link :</p>
    <div class="form_field form_field2"><input name="facebook_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('facebook_link',$admin_info->facebook_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <p class="form_title1">Twitter Link :</p>
    <div class="form_field form_field2"><input name="twitter_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('twitter_link',$admin_info->twitter_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <p class="form_title1">LinkedIn Link :</p>
    <div class="form_field form_field2"><input name="linkedin_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('linkedin_link',$admin_info->linkedin_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <?php /*?><p class="form_title1">Google Link :</p>
    <div class="form_field form_field2"><input name="google_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('google_link',$admin_info->google_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p><?php */?>

    <p class="form_title1">Youtube Link :</p>
    <div class="form_field form_field2"><input name="youtube_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('youtube_link',$admin_info->youtube_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>

    <p class="form_title1">Instagram Link :</p>
    <div class="form_field form_field2"><input name="instagram_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('instagram_link',$admin_info->instagram_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
	
	<?php /*?><p class="form_title1">Soundcloud Link :</p>
    <div class="form_field form_field2"><input name="soundcloud_link" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('soundcloud_link',$admin_info->soundcloud_link);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p><?php */?>
  

     <p class="form_title1">Google Analytics ID :</p>
    <div class="form_field form_field2"><input name="google_analytics_id" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('google_analytics_id',$admin_info->google_analytics_id);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p> 

    <p class="form_title1">Google Web Code :</p>
    <div class="form_field form_field2"><textarea name="google_web_code" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('google_web_code',$admin_info->google_web_code);?></textarea></div>
    <p class="clearfix"></p>  

<?php /*?><p class="form_title1">Home page welcome video :</p>
    <div class="form_field form_field2"><textarea name="youtuble_url" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('youtuble_url',$admin_info->youtuble_url);?></textarea></div>
    <p class="clearfix"></p> 	

    <p class="form_title1">Footer Time :</p>
    <div class="form_field form_field2"><textarea name="footer_time" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('footer_time',$admin_info->footer_time);?></textarea></div>
    <p class="clearfix"></p> <?php */?>

    <?php /*?> <p class="form_title1">Google Map Code :</p>
    <div class="form_field form_field2"><textarea name="google_map" cols="55" rows="6" style="width:450px;" class="form-control"><?php echo set_value('google_map',html_entity_decode($admin_info->google_map));?></textarea></div>
    <p class="clearfix"></p> <?php */?>
    
     <p class="form_title1">Website url :</p>
    <div class="form_field form_field2"><input name="website_url" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('website_url',$admin_info->website_url);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
     <p class="form_title1">Footer1 Header :</p>
    <div class="form_field form_field2"><input name="footer1_header" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('footer1_header',$admin_info->footer1_header);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
     <p class="form_title1">Footer2 Header :</p>
    <div class="form_field form_field2"><input name="footer2_header" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('footer2_header',$admin_info->footer2_header);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
     <p class="form_title1">Footer3 Header :</p>
    <div class="form_field form_field2"><input name="footer3_header" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('footer3_header',$admin_info->footer3_header);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>
    
     <p class="form_title1">Copyright Text :</p>
    <div class="form_field form_field2"><input name="copyright_text" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('copyright_text',$admin_info->copyright_text);?>"<?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>></div>
    <p class="clearfix"></p>  

    <p class="form_title1"> Company Profile :</p>
    <div class="form_field form_field2"><input name="company_profile" type="file" class="form-control" style="width:450px;" value="" <?php echo  $this->admin_type!=1 ? ' readonly="readonly"' : '';?>>
    <?php
	if($admin_info->company_profile!='' && file_exists(UPLOAD_DIR."/companyprofile/".$admin_info->company_profile)){
			?>
			<a href="<?php echo base_url();?>uploaded_files/companyprofile/<?php echo $admin_info->company_profile;?>" target="_blank">View</a>
                        &nbsp;
                        <?php if($admin_info->companyprofile_status == 1) { ?>
                        <a href="<?php echo base_url()."sitepanel/setting/activatecompanyprofile" ?>">Activate</a>
                        <?php }else if($admin_info->companyprofile_status == 0) { ?>
                        &nbsp;
                        <a href="<?php echo base_url()."sitepanel/setting/deactivatecompanyprofile" ?>">Deactivate</a>
                        &nbsp;
                        <?php } ?>
                        <a href="<?php echo base_url()."sitepanel/setting/deletecompanyprofile" ?>">Delete</a>
			     
  	<?php
	}?>
	<br />[ ( File should be pdf format and file size should not be more then 2 MB (2048 KB)) ]
    </div>    
  <?php } ?> 
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" class="btn1" value="Update Info"  />
    </div>
    <p class="clearfix"></p>
    <?php echo form_close();?>
  </div>
  </div>
</div>
</div>
<?php $this->load->view('includes/footer');?>