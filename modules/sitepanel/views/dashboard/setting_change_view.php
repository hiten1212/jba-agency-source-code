<?php $this->load->view('includes/header');?>
<?php 
echo admin_breadcrumb($heading_title); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
 
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
         <?php echo $heading_title; ?>
        </div>
       <?php echo validation_message();
	    echo error_message();
   echo form_open('sitepanel/setting/change/');?>
  	<p class="form_title1"><span class="required">*</span> Old Password :</p>
    <div class="form_field form_field2"><input name="old_pass" id="old_pass" type="password" class="form-control" style="width:450px;" value=""></div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> New Password :</p>
    <div class="form_field form_field2"><input name="new_pass" id="new_pass" type="password" class="form-control" style="width:450px;" value="">
    <span><small>(Note: Password must contain at least one alphabet, one number, one uppercase characters, one special character like @,$ etc and a minimum of 6 characters in length)</small></span>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"><span class="required">*</span> Confirm Password :</p>
    <div class="form_field form_field2"><input name="confirm_password" type="password" class="form-control" style="width:450px;" value="">
    <span><small>(Note: Password must contain at least one alphabet, one number, one uppercase characters, one special character like @,$ etc and a minimum of 6 characters in length)</small></span>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
   <input type="submit" class="btn1" value="Update Info"  />
    </div>
    <p class="clearfix"></p>
    <?php echo form_close();?>
  </div>
  </div>
  
</div>
</div>
<?php $this->load->view('includes/footer');?>