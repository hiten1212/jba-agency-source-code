<?php $this->load->view('includes/header'); 
$pid    = (int) $this->uri->segment(4,0);

if($pid!='') { $back_link = 'sitepanel/gallery';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); 
	$title=$heading_title;
}
else {	
	echo admin_breadcrumb($heading_title); 
	$title=$heading_title;
	}?>	
 
 <div class="container-fluid">
  <div class="mid_area">
    <h1 class="fl"> <?php echo $title;?> </h1>
    <p class="add-product"><?php echo anchor("sitepanel/gallery/add/$pid",'Add','' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
     <?php    if(error_message() !=''){
               	   echo error_message();
                }
	   ?> 	                
	<?php echo form_open("sitepanel/gallery/index/$pid",'id="form" method="get" '); ?>
    <div class="search-section">
        <p class="serch-sec">Search</p>
      <p class="serch-sec1"> 
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Title">
      </p>           
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      
       <p class="serch-sec2">
        <?php 
				if($this->input->get_post('keyword')!=''){ 
					echo anchor("sitepanel/gallery/",'<span>Clear Search</span>','class="form-control"');
				} 
				?>
      </p>
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    
    <div class="clearfix"></div>
    <?php echo form_close();?>
		<?php		
		if( is_array($pagelist) && !empty($pagelist) ){ 		
			echo form_open("sitepanel/gallery/",'id="myform"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll box_h_235">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <?php if($parent_id >0){}else{?><td width="25%" class="fs14">Title</td><?php } ?>
              <!--<td width="12%" class="text-center fs13">Section</td>-->
              <td width="12%" class="text-center fs13">Image</td>
			  <td width="12%" class="text-center fs13">Display Order</td>
              <td width="14%" class="text-center fs13">Status</td>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php 				
		foreach($pagelist as $catKey=>$pageVal)
			{ 		
			$id=$pageVal['gallery_id'];
			$total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
			 $displayorder       = ($pageVal['sort_order']!='') ? $pageVal['sort_order']: "0";
		?> <input type="hidden" name="photo_count" value="Y" />
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?=$pageVal['gallery_id'];?>" /></td>
             <?php if($parent_id >0){}else{?><td width="25%" class="fs13">
            <?php echo $pageVal['gallery_title'];?>
             <?php if($pid=='0'){?><br><br> <?php echo anchor("sitepanel/gallery/index/$pageVal[gallery_id]/".query_string(),'[ Photos & Videos'); ?>
			<?php echo $total_photo;?>]
			 <?php }?>
             </td><?php } ?>
            
                   <?php if($pageVal['type'] == 1) { ?>
            <td width="12%" class="fs13" align="center"> 
			<p class="pro_pic"><img src="<?php echo get_image('gallery',$pageVal['gallery_image'],100,70,'AR');?>" /><br /></p>	 

<?php	 $j=1;
		 $product_path = "gallery/".$pageVal['gallery_image'];
		?>
         <a href="#"  onclick="$('#dialogimg_<?php echo $pageVal['gallery_id'];?>').dialog({width:'450px'});">View Image </a>
         <div id="dialogimg_<?php echo $pageVal['gallery_id'];?>" title="View Image" style="display:none;">
         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  /> </div>				
            </td>
             <?php } else if($pageVal['type'] == 2) {?>
            
              <td width="12%" class="fs13" align="center"> 
			<p class="pro_pic"><img src="<?php echo get_image('gallery',$pageVal['video_thumbnail'],100,70,'AR');?>" /><br /></p>	 

<?php	 $j=1;
		 $product1_path = "gallery/".$pageVal['video_thumbnail'];
		?>
         <a href="#"  onclick="$('#dialogimg_<?php echo $pageVal['gallery_id'];?>').dialog({width:'450px'});">View Video Thumbnail </a>
         <div id="dialogimg_<?php echo $pageVal['gallery_id'];?>" title="View Video Thumbnail" style="display:none;">
         <img src="<?php echo base_url().'uploaded_files/'.$product1_path;?>"  /> </div>				
            </td>
            <?php } ?>			
			<td width="12%" class="text-center fs13"><input type="text" name="ord[<?php echo $pageVal['gallery_id'];?>]" value="<?php echo $displayorder;?>" size="5" class="w50 mt10 select-style"  /></td>
           
            <td width="14%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/gallery/edit/$pageVal[gallery_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>
          </tr>
          <?php	
		}		   
		?>         
        <?php
		 if($page_links!=''){
			 ?>
         <tr><td colspan="5" align="center" height="30"><?php echo $page_links; ?></td></tr>
          <?php }?>  
        </table>
        </div>
      <p class="mt15"><input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
    <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
    <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
	
	<input name="update_order" type="submit"  value="Update Order" class="btn1" />
    </p>
    </div>
    <?php
		echo form_close();
	}else
	{
		echo "<center><strong> No record(s) found !</strong></center>" ;
	}
	?> 
  </div>
</div>
<?php $this->load->view('includes/footer'); ?>