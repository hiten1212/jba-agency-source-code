<?php $this->load->view('includes/header'); ?>
<?php  $back_link = 'sitepanel/gallery';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
  
 <div class="container-fluid">
<div class="mid_area"> 
    
 <script src="<?php echo base_url(); ?>assets/sitepanel/css/dist_files/jquery.imgareaselect.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/sitepanel/css/dist_files/jquery.form.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/sitepanel/css/dist_files/imgareaselect.css">
<script src="<?php echo base_url(); ?>assets/sitepanel/js/functions.js"></script>
	  
	
	
  <div class="mt10 p8">  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <?php //echo validation_message();
   echo error_message();
   echo form_open_multipart('sitepanel/gallery/add/');?>
   	
     <?php
			$default_params = array(
								'heading_element' => array(
								  'field_heading'=>"Title",
								  'field_name'=>"gallery_title",
								  'field_placeholder'=>"",
								  'exparams' => 'size="40"'
								),
								'url_element'  => array(
								  'field_heading'=>"Page URL",
								  'field_name'=>"friendly_url",
								  'field_placeholder'=>"Your Page URL",
								  'exparams' => 'size="40"',
								  'pre_seo_url' =>'',
								  'pre_url_tag'=>FALSE
								)
								
								);
			seo_add_form_element($default_params);	
			?>  
            
    <p class="form_title1"><span class="required"></span> Alt :</p>
    <div class="form_field form_field2"><input type="text" name="gallery_alt" size="50" value="<?php echo set_value('gallery_alt');?>"><?php echo form_error('gallery_alt');?> 
      </div>
    <p class="clearfix"></p>
            
  <p class="form_title1"><span class="required">*</span> Image :</p>
    <div class="form_field form_field2"><input type="file" name="gallery_image"  id="profile-picX" /> [ <?php echo $this->config->item('gallery.best.image.view');?> ]
	<?php echo form_error('gallery_image');?><br />
	<img class="img-circle" id="profile_picture2" height="128" src="https://www.jbaagency.net/men-icon.jpg"  data-holder-rendered="true" style="width: 140px; height: 140px;"/> 
	<a type="button" class="btn btn-primary" id="change-profile-pic">Add Image</a>
	 
	
	</div>
    <p class="clearfix"></p>  
	
	<p class="form_title1"><span class="required"></span> Description :</p>
     <div class="form_field form_field2"> <textarea name="description" rows="5" cols="50" id="description" ><?php echo set_value('description');?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('description');?>
    </div>
    <p class="clearfix"></p>
     
    <p class="form_title1"></p>
    <div class="form_field form_field">
     <input type="submit" name="sub" value="Add" class="btn1" />
      <input type="hidden" name="action" value="addgallery" />
	  <input type="text" name="gal_image_name" value="" id="gal_image_name" style="visibility:hidden;" />
	  <input type="hidden" name="portfolio_type" value="1" />
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
	
	<?php /*?><div>
		<img class="img-circle" id="profile_picture" height="128" src="https://www.jbaagency.net/uploaded_files/thumb_cache/thumb_100_70_stock-photo-blurred-of-production-team-shooting-some-video-movie-for-tv-commercial-with-studio-equipment-set-644822698.jpg"  data-holder-rendered="true" style="width: 140px; height: 140px;"/>
		<br><br>
		<a type="button" class="btn btn-primary" id="change-profile-picX">Add Image</a>
	</div><?php */?>
	<div id="profile_pic_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				   <h3>Add Image</h3>
				</div>
				<div class="modal-body">
					<form id="cropimage" method="post" enctype="multipart/form-data" action="<?= base_url();?>change_pic.php">
						<strong>Upload Image:</strong> <br><br>
						<input type="file" name="profile-pic" id="profile-pic" />
						<input type="hidden" name="hdn-profile-id" id="hdn-profile-id" value="1" />
						<input type="hidden" name="hdn-x1-axis" id="hdn-x1-axis" value="" />
						<input type="hidden" name="hdn-y1-axis" id="hdn-y1-axis" value="" />
						<input type="hidden" name="hdn-x2-axis" value="" id="hdn-x2-axis" />
						<input type="hidden" name="hdn-y2-axis" value="" id="hdn-y2-axis" />
						<input type="hidden" name="hdn-thumb-width" id="hdn-thumb-width" value="" />
						<input type="hidden" name="hdn-thumb-height" id="hdn-thumb-height" value="" />
						<input type="hidden" name="action" value="" id="action" />
						<input type="hidden" name="image_name" value="" id="image_name" />						
						<div id='preview-profile-pic'></div>
						<div id="thumbs" style="padding:5px; width:600p"></div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="save_crop" class="btn btn-primary">Crop & Save</button>
				</div>
			</div>
		</div>
	</div>	
	
	 
	
  </div>
  </div>
  
</div>
</div>
<script>
$(document).ready(function() {

    $( "#save_crop" ).click(function() {
	 var gal_image_name = $('#image_name').val(); 
	 $('#gal_image_name').val(gal_image_name);
	 var galpath = site_url+"uploaded_files/gallery/"+gal_image_name; 
	// alert(galpath);
	 alert("Image Croped Successfully."); 
	 //$("#profile_picture").attr('src', ""+galpath);
	 $("#profile_picture2").attr('src', ""+galpath);
     // alert(min_date);
    });

});
</script>
<?php $this->load->view('includes/footer'); ?>