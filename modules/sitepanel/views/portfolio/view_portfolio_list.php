<?php $this->load->view('includes/header'); ?>  
<?php
echo admin_breadcrumb($heading_title); ?>
<div class="container-fluid">
     <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
    <p class="add-product"><?php echo anchor("sitepanel/portfolio/add/",'Add Portfolio','' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">     
	<?php
		if(error_message() !=''){
		   echo error_message();
		}
		?> 
		<?php echo form_open("sitepanel/portfolio/",'id="search_form" method="get" '); ?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1"> 
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="Portfolio Title">
      </p>
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
       <p class="serch-sec2">
        <?php 
			if($this->input->get_post('keyword')!=''){
				echo anchor("sitepanel/portfolio/",'<span>Clear Search</span>','class="form-control"');
			} 
			?>
      </p>
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    <div class="clearfix"></div>
    <?php echo form_close();?>
		<?php
		if( is_array($res) && !empty($res) ){
			echo form_open("sitepanel/portfolio/",'id="myform"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll ">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="25%" class="fs14">Title</td>
              <td width="12%" class="text-center fs13">Portfolio Picture</td>
              <td width="12%" class="text-center fs13">Description</td>
              <td width="14%" class="text-center fs13">Current Status</td>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php 
			$atts = array(
						'width'      => '740',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
				 );
		foreach($res as $catKey=>$pageVal){ 
		?>
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?=$pageVal['portfolio_id'];?>" /></td>
            <td width="25%" class="fs14">
            <?php echo $pageVal['portfolio_title'];?></td>
            <td width="12%" class=" fs13" align="center">
			<p class="pro_pic"><span><img src="<?php echo get_image('portfolio',$pageVal['portfolio_image'],60,60,'AR');?>" alt=""></span></p>
         </td>
           <td width="12%" class="text-center fs13"> <a href="#"  onclick="$('#dialog_<?php echo $pageVal['portfolio_id'];?>').dialog( {width: 650} );">View Details</a>
             <div id="dialog_<?php echo $pageVal['portfolio_id'];?>" title="Description" style="display:none;">
              <?php echo $pageVal['portfolio_description'];?></div></td>

            <td width="14%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>

            <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/portfolio/edit/$pageVal[portfolio_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>

          </tr>
          <?php
		}
		?>
        <?php
		 if($page_links!=''){
			 ?>
         <tr><td colspan="5" align="center" height="30"><?php echo $page_links; ?></td></tr>
          <?php }?> 
        </table>
      </div>
      <p class="mt15">
            <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
            <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
            <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
      </p>
    </div>
    <?php
		echo form_close();
	}else
	{
		echo "<center><strong> No record(s) found !</strong></center>" ;
	}
	?>
  </div> 
</div>
<script type="text/javascript">
function onclickgroup(){

	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();

	}
}
</script>
<?php $this->load->view('includes/footer'); ?>