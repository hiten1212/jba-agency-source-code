<?php $this->load->view('includes/header');

$back_link = 'sitepanel/broucher';

echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>

<!--Breadcrumb End-->



<!--Body-->

<div class="container-fluid">

 <div class="mid_area">

  <div class="mt10 p8">

   <div class="box_style">

    <div class="recent-table p10 mb15"><?php echo $heading_title;?><span class="white" style="float:right"> <?php echo anchor($back_link,'Cancel','class="btn1" ' );?></span></div>

    <?php //echo validation_message();

    echo error_message();

    echo form_open_multipart(current_url_query_string());

    

    /*$default_params = array(

		'heading_element' => array(

		'field_heading'=>"Broucher Name",

		'field_name'=>"broucher_name",

		'field_value'=>$edit_result['broucher_name'],

		'field_placeholder'=>"",

		'exparams' => 'size="60"'

		),

		'url_element'  => array(

		'field_heading'=>"Page URL",

		'field_name'=>"friendly_url",

		'field_value'=>$edit_result['friendly_url'],

		'field_placeholder'=>"Your Page URL",

		'exparams' => 'size="60"',

		)

		);

		seo_edit_form_element($default_params);*/

		?>  	

    <p class="form_title1">PDF Download Name <span class="required">*</span> :</p>

        <div class="form_field form_field2"><input name="broucher_name" type="text" class="form-control" value="<?php echo set_value('broucher_name',$edit_result['broucher_name']);?>" style="width:450px;"><?php echo form_error('broucher_name');?></div>
<p class="clearfix"></p>
     
          <?php /*<p class="form_title1"><span class="required">*</span> Broucher Image :</p>

    <div class="form_field form_field2"><input name="gallery_file" id="gallery_file" type="file" style="width:450px" class="form-control"><?php echo form_error('gallery_file');?>

	<?php

		 $j=1;

		 $product_path = "brouchers/".$edit_result['gallery_file'];

		?>

         <a href="#"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a>

         <div id="dialog_<?php echo $j;?>" title="Gallery Image" style="display:none;">

         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  />

         </div>

         

         <br />

   <?php echo $this->config->item('broucher.best.image.view');?>

         </div>*/?>

     <p class="clearfix"></p>

     

    <p class="form_title1">PDF Download <span class="required">*</span> :</p>

    <div class="form_field form_field2"><input name="broucher_doc" id="broucher_doc" type="file" class="form-control" style="width:450px;"><?php echo form_error('broucher_doc');?>
	<p class="pt5">
	<?php
	  if($edit_result["broucher_doc"]!="" && file_exists(UPLOAD_DIR."/brouchers/".$edit_result["broucher_doc"])){
		  echo anchor(site_url("broucher/download_pdf/".$edit_result["broucher_id"]),"Download File",array("class"=>"red b"));
		  echo "&nbsp;&nbsp;&nbsp;&nbsp;"; 
		  echo anchor(site_url("sitepanel/broucher/delete_pdf/".$edit_result["broucher_id"]),"Delete File",array("class"=>"black b"));
	  }?>
      </p>
    [ <?php echo $this->config->item('broucher.best.image.view');?> ] </div>

    <p class="clearfix"></p>

    

    <p class="form_title1"></p>

    <div class="form_field form_field">

     <input type="submit" name="sub" value="Update" class="btn1" />

     <input type="hidden" name="broucher_id" value="<?php echo $edit_result['broucher_id'];?>" />

     <input type="hidden" name="action" value="add" />

    </div>

    <p class="clearfix"></p>

    <?php echo form_close();?> 

   </div>

  </div>

 </div>

</div>

<!--Body End-->

<?php $this->load->view('includes/footer');?>