<?php $this->load->view('includes/header');?>

<!--Breadcrumb-->

<?php  echo admin_breadcrumb($heading_title); ?>

<!--Breadcrumb End--> 

<!--Body-->

<div class="container-fluid">

 <div class="mid_area">

  <h1 class="fl"> <?php echo $heading_title;?></h1>

  <p class="add-product"><?php echo anchor("sitepanel/broucher/add/",'Add PDF Download','');?></p>

  <p class="clearfix"></p>

  <hr style="margin-bottom:5px; margin-top:10px;">

  <?php echo validation_message();

  echo error_message();

  echo form_open("sitepanel/broucher/",'id="search_form" method="get" ');?>

  <div class="search-section">

   <p class="serch-sec">Search</p>

   <p class="serch-sec1"><input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="PDF Download Name"></p>

   <p class="serch-sec2"><input name="search" type="submit" value="Submit" class="btn1"> </p>

   <p class="serch-sec2"><?php if($this->input->get_post('keyword')!=''){ echo anchor("sitepanel/broucher/",'<span>Clear Search</span>','class="form-control"'); }?></p>

   <div class="records fs14"> Records Per Page : <?php echo display_record_per_page();?></div>

   <p class="clearfix"></p>

  </div>

  <hr style="margin-bottom:5px; margin-top:5px;">

  <div class="clearfix"></div>

  <?php echo form_close(); 

  if( is_array($res) && !empty($res) ){

	  echo form_open("sitepanel/broucher/",'id="data_form"');?>

	  <div class="p8" id="my_data">

	   <div class="box_style mob_scroll ">

	    <div class="recent-table">

	     <table width="100%" border="0" cellspacing="0" cellpadding="0">

	      <tr>

	       <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);"></td>

	       <td width="68%" class="fs14">PDF Download Name</td>
			<!--<td width="12%" class="text-center fs13">Image</td>-->
	       <td width="24%" class="text-center fs14">PDF Download</td>

	       <td width="10%" class="text-center fs13">Status</td>

	       <td width="10%" class="text-center fs13">Action</td>

	      </tr>

	     </table>

	    </div>

	    <p class="clearfix"></p>

	    <table width="100%" border="0" cellspacing="0" cellpadding="0">

	     <?php

	     $j=1;

	     foreach($res as $catKey=>$pageVal){

		     ?>

		     <tr>

		      <td width="3%"><input name="arr_ids[]" type="checkbox" value="<?php echo $pageVal['broucher_id'];?>"></td>

		      <td width="65%" class="fs14"><?php echo $pageVal['broucher_name'];?></td>
              
              <?php /*<td width="12%" class=" fs13" align="center"> 

			<p class="pro_pic"><span><img src="<?php echo get_image('brouchers',$pageVal['gallery_file'],60,60,'AR');?>" alt=""></span></p>

			<?php

		 $j=1;

		 $product_path = "brouchers/".$pageVal['gallery_file'];

		?>

         <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $pageVal['id'];?>').dialog({width:'auto'});">View Image </a>

         <div id="dialog_<?php echo $pageVal['id'];?>" title="Gallery Image" style="display:none;">

         <img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  /> </div>	

         </td>*/?>
              

		      <td width="24%" align="center" class="fs14">
              
              <?php
			  if($pageVal["broucher_doc"]!="" && file_exists(UPLOAD_DIR."/brouchers/".$pageVal["broucher_doc"])){
				  echo anchor(site_url("broucher/download_pdf/".$pageVal["broucher_id"]),"Download File",array("class"=>"red b"));
				  echo "<br />"; 
				  echo anchor(site_url("sitepanel/broucher/delete_pdf/".$pageVal["broucher_id"]),"Delete File",array("class"=>"black b"));
			  }?>
              
              </td>

		      <td width="10%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)?"green2":"red";?> fs20 mt8"></span></td>

		      <td width="10%" class="text-center grey"><?php echo anchor("sitepanel/broucher/edit/$pageVal[broucher_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>'); ?> </td>

		     </tr>

		     <?php

		     $j++;

	     }

	     

	     if($page_links!=''){

		     ?>

		     <tr><td colspan="4" align="center" height="30"><?php echo $page_links; ?></td></tr>

		     <?php

	     }?>

	    </table>

	   </div>

	   <div class="mt15">

       <p class=" mt10 serch-sec8 fl">

	    <input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>

	    <input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>

	    <input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>

       </p>        

	   

          <?php /*?><p class=" ml20 serch-sec8 fl">

          	<?php

          		echo form_dropdown("set_as",$this->config->item('broucher_set_as_config'),$this->input->post('set_as'),'class="w15 mt10 form-control" onchange="return onclickgroup()"'); ?>

           </p>

            <p class=" ml20 serch-sec8 fl">

        	<?php     

          		echo form_dropdown("unset_as",$this->config->item('broucher_unset_as_config'),$this->input->post('unset_as'),'class="w15 mt10 form-control" onchange="return onclickgroup()"');

          ?>

          </p><?php */?>

              

      

       <p class="clearfix"></p> 

       

	   </div>

	  </div>

	  <?php

	  echo form_close();

  }else{

	  echo "<center><strong> No record(s) found !</strong></center>" ;

  }?>

 </div>

</div> 

<!--Body End-->

<script type="text/javascript">

function onclickgroup(){

	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){

		$('#data_form').submit();

	}

}

</script>

<?php $this->load->view('includes/footer');?>