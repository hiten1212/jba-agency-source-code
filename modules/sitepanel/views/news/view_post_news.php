<?php $this->load->view('includes/header'); ?>  
   
<?php  $back_link = 'sitepanel/news';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
  
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
        <?php //echo validation_message();
   echo error_message();
   echo form_open_multipart('sitepanel/news/post_news/');?>
   	<div id="tab-general"> 
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
     <?php
			$default_params = array(
								'heading_element' => array(
								  'field_heading'=>"Title",
								  'field_name'=>"news_title",
								  'field_placeholder'=>"",
								  'exparams' => 'size="40"'
								),
								'url_element'  => array(
								  'field_heading'=>"Page URL",
								  'field_name'=>"friendly_url",
								  'field_placeholder'=>"Your Page URL",
								  'exparams' => 'size="40"',
								  'pre_seo_url' =>'',
								  'pre_url_tag'=>FALSE
								)
								
								);
			seo_add_form_element($default_params);	
			?>  
          <?php /*?>  <p class="form_title1"><span class="required">*</span> Upload Image :</p>
    <div class="form_field form_field2"><input name="news_image" type="file" class="txtbox" /> [ <?php echo $this->config->item('news.best.image.view');?> ]
       <?php echo form_error('news_image');?></div>
    <p class="clearfix"></p>   <?php */?>     
  	<?php /*?><p class="form_title1"><span class="required">*</span> Question :</p>
    <div class="form_field form_field2"><input name="news_question" size="40" style="width:450px" type="text" class="form-control" value="<?php echo set_value('news_question');?>"></div>
    <p class="clearfix"></p><?php */?>
    
    <p class="form_title1"><span class="required">*</span> Description :</p>
    <div class="form_field form_field2"> <textarea name="news_description" rows="5" cols="50" id="news_description" > <?php echo set_value('news_description');?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('news_description');?>
    </div>
    <p class="clearfix"></p>
   </div>
     
     
    <div id="tab-image"> 
    <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p> 
       <?php for($i=1;$i<=$this->config->item('total_news_images');$i++){?>
      <p class="form_title1"><span class="required">**</span> Image :</p>
    <div class="form_field form_field2"><input name="product_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">[ <?php echo $this->config->item('news.best.image.view');?> ] <?php echo form_error('product_images'.$i);?></div>
    <p class="clearfix"></p>
     <?php }?>    

    <p class="form_title1"> Alt Tag Text :</p>
    <div class="form_field form_field2"><input name="product_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('product_alt');?>" /><?php echo form_error('product_alt');?></div>
    <p class="clearfix"></p> 
    </div>
     
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Post" class="btn1" />
      <input type="hidden" name="action" value="addnews" />
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
  
</div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
$('#vtab-option a').tabs();
//--></script>
<?php $this->load->view('includes/footer'); ?>