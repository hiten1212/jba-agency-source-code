<?php $this->load->view('includes/header'); ?>  
<?php 
echo admin_breadcrumb($heading_title); ?>
     
<div class="container-fluid"> 
     <div class="mid_area">
    <h1 class="fl"> <?php echo $heading_title; ?></h1>
    <p class="add-product"><?php echo anchor("sitepanel/news/post_news/",'Post News','' );?></p>
    <p class="clearfix"></p>
    <hr style="margin-bottom:5px; margin-top:10px;">
     <?php 
                if(error_message() !=''){
               	   echo error_message();
                }
                ?> 	                
		<?php echo form_open("sitepanel/news/",'id="search_form" method="get" '); ?>
    <div class="search-section">
      <p class="serch-sec">Search</p>
      <p class="serch-sec1"> 
        <input name="keyword" value="<?php echo $this->input->get_post('keyword');?>" type="text" class="form-control" placeholder="News Title">
      </p>           
      <p class="serch-sec2">
        <input name="search" type="submit" value="Submit" class="btn1">
      </p>
      
       <p class="serch-sec2">
        <?php 
				if($this->input->get_post('keyword')!=''){ 
					echo anchor("sitepanel/news/",'<span>Clear Search</span>','class="form-control"');
				} 
				?>
      </p>
      <div class="records fs14"> Records Per Page :
        <?php echo display_record_per_page();?>
      </div>
      <p class="clearfix"></p>
    </div>
    <hr style="margin-bottom:5px; margin-top:5px;">
    
    <div class="clearfix"></div>
    <?php echo form_close();?>
		<?php		
		if( is_array($res) && !empty($res) )
		{
			echo form_open("sitepanel/news/",'id="data_form"');
			?>
    <div class="p8" id="my_data">
      <div class="box_style mob_scroll box_h_235">
        <div class="recent-table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="3%"><input type="checkbox" onclick="$('input[name*=\'arr_ids\']').prop('checked', this.checked);" /></td>
              <td width="25%" class="fs14">Title</td>              
              <td width="12%" class="text-center fs13">News Picture</td>
              <td width="12%" class="text-center fs13">Description</td>              
              <td width="14%" class="text-center fs13">Current Status</td>
              <td width="15%" class="text-center fs13">Action</td>
            </tr>
          </table>
        </div>
        <p class="clearfix"></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php 	
			$atts = array(
											'width'      => '740',
											'height'     => '600',
											'scrollbars' => 'yes',
											'status'     => 'yes',
											'resizable'  => 'yes',
											'screenx'    => '0',
											'screeny'    => '0'
									 );
		foreach($res as $catKey=>$pageVal)
		{ 
		?> 
          <tr>
            <td width="3%"><input type="checkbox" name="arr_ids[]" value="<?=$pageVal['news_id'];?>" /></td>
            <td width="25%" class="fs14">
            <?php echo $pageVal['news_title'];?><br />
           <?php  if($pageVal['latest_news']!="" && $pageVal['latest_news']!='0'){
			   ?><b class="red">Latest News & Events</b><?php
		       } ?>
            
            </td>            
            <td width="12%" class=" fs13" align="center"> 
			<p class="pro_pic"><span><img src="<?php echo get_image('news',$pageVal['media'],60,60,'AR');?>" alt=""></span></p>			
         </td>
         
           <td width="12%" class="text-center fs13"> <a href="#"  onclick="$('#dialog_<?php echo $pageVal['news_id'];?>').dialog( {width: 650} );">View Details</a>
                     <div id="dialog_<?php echo $pageVal['news_id'];?>" title="Description" style="display:none;">
					  <?php echo $pageVal['news_description'];?></div></td>
           
            <td width="14%" class="text-center fs13"><span title="<?php echo ($pageVal['status']==1)? "Active":"In-active";?>" class="fa fa-circle <?php echo ($pageVal['status']==1)? "green2":"red";?> fs20 mt8"></span></td>
            <td width="15%" class="text-center grey"><?php echo anchor("sitepanel/news/edit_news/$pageVal[news_id]/".query_string(),'<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>','title="Edit"'); ?></td>
          </tr>
          <?php
		
		}		   
		?> 
        
        <?php
		 if($page_links!=''){
			 ?>
         <tr><td colspan="5" align="center" height="30"><?php echo $page_links; ?></td></tr>
          <?php }?>
        
          
        </table>
      </div>
      <div class="mt15 ">
      <p class="mt12 serch-sec8 fl"><input name="status_action" type="submit"  value="Activate" class="btn1" id="Activate" onClick="return validcheckstatus('arr_ids[]','Activate','Record','u_status_arr[]');"/>
				<input name="status_action" type="submit" class="btn1" value="Deactivate" id="Deactivate"  onClick="return validcheckstatus('arr_ids[]','Deactivate','Record','u_status_arr[]');"/>
				<input name="status_action" type="submit" class="btn1" id="Delete" value="Delete"  onClick="return validcheckstatus('arr_ids[]','delete','Record');"/>
      </p>
          <p class="ml20 serch-sec8 fl">
          <?php         
            echo form_dropdown("set_as",$this->config->item('news_set_as_config'),$this->input->post('set_as'),'class="w15 mt10 form-control" onchange="return onclickgroup()"'); ?>
              </p>
          <p class="ml20 serch-sec8 fl">
            <?php     
            echo form_dropdown("unset_as",$this->config->item('news_unset_as_config'),$this->input->post('unset_as'),'class="w15 mt10 form-control" onchange="return onclickgroup()"');
           ?>
        </p>
         <p class="clearfix"></p>  
        </div>
        
    </div>
    <?php
		echo form_close();
	}else
	{
		echo "<center><strong> No record(s) found !</strong></center>" ;
	}
	?> 
  </div>    
    
</div>
<script type="text/javascript">
function onclickgroup(){
	if(validcheckstatus('arr_ids[]','set','record','u_status_arr[]')){
		$('#data_form').submit();
	}
}
</script>
<?php $this->load->view('includes/footer'); ?>