<?php $this->load->view('includes/header'); ?>  
   
<?php  $back_link = 'sitepanel/news';
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area">
  
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title;?>
        </div>
        <div id="tabs" class="htabs">
     <a href="#tab-general">General</a>
     <a href="#tab-image">Image</a>
    </div>
        <?php //echo validation_message();
   echo error_message();
   echo form_open_multipart(current_url_query_string());?>
   <div id="tab-general">
      <div style="margin-bottom:10px;line-height:20px">
      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>
      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>
     </div>
      <p class="clearfix"></p>
    <?php
			$default_params = array(
								'heading_element' => array(
									  'field_heading'=>$heading_title." Title",
									  'field_name'=>"news_title",
									  'field_value'=>$res['news_title'],
									  'field_placeholder'=>"",
									  'exparams' => 'size="40"'
									),
								'url_element'  => array(
									  'field_heading'=>"Page URL",
									  'field_name'=>"friendly_url",
									  'field_value'=>$res['friendly_url'],			  
									  'field_placeholder'=>"Your Page URL",
									  'exparams' => 'size="40"',
								   )
						  );
			seo_edit_form_element($default_params);			
			?>
  <?php /*?>	<p class="form_title1"><span class="required">*</span> Image :</p>
    <div class="form_field form_field2"><input name="news_image" type="file" /> 
      <img  src="<?php echo get_image('news',$res['news_image'],40,40,'AR');?>" style="vertical-align:middle;" />[ <?php echo $this->config->item('news.best.image.view');?> ]
         <?php echo form_error('news_image');?></div>
    <p class="clearfix"></p><?php */?>
    
    <p class="form_title1"><span class="required">*</span> Description :</p>
    <div class="form_field form_field2"> <textarea name="news_description" rows="5" cols="50" id="news_description" ><?php echo set_value('news_description',$res['news_description']);?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('news_description');?>
    </div>
    <p class="clearfix"></p>   
    
     </div>
     <div id="tab-image">

     <div style="margin-bottom:10px;line-height:20px">

      <span style="float:right; margin-right:10px;" class="required">*Required Fields</span>

      <br><span style="float:right; margin-right:10px;" class="required">**Conditional Fields</span>

     </div>

     <p class="clearfix"></p>

     <input type="hidden" name="product_exclude_images_ids" value="" id="product_exclude_images_ids" />

     <?php

    //trace($res_photo_media);

     $j=0;

     for($i=1;$i<=$this->config->item('total_news_images');$i++){

	     $product_img  = @$res_photo_media[$j]['media'];

	     $product_path = "news/".$product_img;

	     $product_img_auto_id  = @$res_photo_media[$j]['id'];

	     ?>

	     <p class="form_title1"><span class="required">**</span> Image :</p>

	     <div class="form_field form_field2"><input name="product_images<?php echo $i;?>" type="file" style="width:450px;" class="form-control">

	      <?php

	      if( $product_img!='' && file_exists(UPLOAD_DIR."/".$product_path) ){

		      ?>

		      <a href="javascript:void(0);"  onclick="$('#dialog_<?php echo $j;?>').dialog({width:'auto'});">View</a> | <input type="checkbox" name="product_img_delete[<?php echo $product_img_auto_id;?>]" value="Y" /> Delete

		      <?php

	      }?>

	      <br />[ <?php echo $this->config->item('news.best.image.view');?> ]

	      <div id="dialog_<?php echo $j;?>" title="Service Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/'.$product_path;?>"  /> </div>

	      <input type="hidden" name="media_ids[]" value="<?php echo $product_img_auto_id;?>" />

	      <?php echo form_error('product_images'.$i);?>

	     </div>

	     <p class="clearfix"></p>

	     <?php

	     $j++;

     }?>

     

     <p class="form_title1"> Alt Tag Text :</p>

     <div class="form_field form_field2"><input name="product_alt" type="text" style="width:450px;" size="40" class="form-control" value="<?php echo set_value('product_alt',$res['product_alt']);?>" /><?php echo form_error('product_alt');?></div>

     <p class="clearfix"></p>

    </div>
     
    <p class="form_title1"></p>
    <div class="form_field form_field">
   <input type="submit" name="sub" value="Edit" class="btn1" />
    <input type="hidden" name="news_id" id="pg_recid" value="<?php echo $res['news_id'];?>">
      </div>
    <p class="clearfix"></p>
    <?php echo form_close();?> 
  </div>
  </div>
  
</div>
</div>

<script type="text/javascript"><!--

$('#tabs a').tabs();

$('#languages a').tabs();

$('#vtab-option a').tabs();

//-->

$('#product_exclude_images_ids').val('');

function delete_product_images(img_id)

{

	//alert($('#product_exclude_images_ids').val());

	img_id = img_id.toString();

	exclude_ids1 = $('#product_exclude_images_ids').val();

	exclude_ids1_arr = exclude_ids1=='' ? Array() : exclude_ids1.split(',');



	if($.inArray(img_id,exclude_ids1_arr)==-1){

		exclude_ids1_arr.push(img_id);

	}



	exclude_ids1 =  exclude_ids1_arr.join(',');



	$('#product_exclude_images_ids').val(exclude_ids1);

	$('#product_image'+img_id).remove();

	$('#del_img_link_'+img_id).remove();



	alert($('#product_exclude_images_ids').val());

}



</script>

<?php $this->load->view('includes/footer'); ?>