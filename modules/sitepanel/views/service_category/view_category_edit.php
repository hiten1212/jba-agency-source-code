<?php $this->load->view('includes/header'); ?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/service_category'.($catresult['parent_id']==0 ? '' : '/index/'.$catresult['parent_id']);
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <div class="mt10 p8">
   <div class="box_style">
    <div class="recent-table p10 mb15"><?php echo $heading_title; ?> <span class="white" style="float:right"><?php echo anchor("sitepanel/service_category/",'Cancel','class="btn1" ' );?></span></div>
    <?php echo error_message();
    echo form_open_multipart(current_url_query_string(),array('id'=>'catfrm','name'=>'catfrm'));
    
		$default_params = array(
		'heading_element' => array(
		'field_heading'=>"Service Category Name",
		'field_name'=>"category_name",
		'field_value'=>$catresult['category_name'],
		'field_placeholder'=>"Service Category Name",
		'exparams' => 'size="40"'
		),
		'url_element'  => array(
		'field_heading'=>"Page URL",
		'field_name'=>"friendly_url",
		'field_value'=>$catresult['friendly_url'],			  
		'field_placeholder'=>"Page URL",
		'exparams' => 'size="40"',
		)
		);
		//seo_edit_form_element($default_params);?>
		
		<p class="form_title1"><span class="required">*</span> Service Category Name :</p>
		<div class="form_field form_field2"><input name="category_name" type="text" class="form-control" value="<?php echo set_value('category_name',$catresult['category_name']);?>" placeholder="Service Category Name" size="40" style="width:450px">


		


		<?php echo form_error('category_name');?>


    </div>
                
                 <p class="form_title1"><span class="required">*</span> Page URL :</p>
		<div class="form_field form_field2"><input name="friendly_url" type="text" class="form-control" value="<?php echo set_value('friendly_url',$catresult['friendly_url']);?>" placeholder="Page URL" size="40" style="width:450px">
                    <p class="clearfix"></p>
                    
                    <?php echo form_error('friendly_url');?>
		
		<?php /*<p class="form_title1">Upload Image :</p>
		<div class="form_field form_field2"><input name="category_image" type="file" class="form-control" style="width:450px;">
		 <?php
		 if($catresult['category_image']!='' && file_exists(UPLOAD_DIR."/service_category/".$catresult['category_image']))
		 {
			 ?>
			 <a href="#"  onclick="$('#dialog').dialog({width:'auto',height:'auto'});">View</a> | <input type="checkbox" name="cat_img_delete" value="Y" /> Delete 
			 <?php	
		 }?>
		 [ <?php echo $this->config->item('category.best.image.view');?> ]
		 <div id="dialog" title="Category Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/service_category/'.$catresult['category_image'];?>"  /></div>
		 <?php echo form_error('category_image');?>
		</div>
		<p class="clearfix"></p>
		
		<p class="form_title1">Alt :</p>
		<div class="form_field form_field2"><input name="category_alt" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('category_alt',$catresult['category_alt']);?>"><?php echo form_error('category_alt');?></div>
		<p class="clearfix"></p>
		
		<div class="form_title1">Description :</div>
		<div class="form_field form_field2"><textarea name="category_description" rows="5" cols="50" id="cat_desc" class="form-control"><?php echo set_value('category_description',$catresult['category_description']);?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('category_description');?></div>
		<p class="clearfix"></p>*/?>
		
		<p class="form_title1"></p>
		<div class="form_field form_field">
		 <input type="submit" name="sub" value="Update" class="btn1" />
		 <input type="hidden" name="action" value="editcategory" />
		 <input type="hidden" name="category_id" id="pg_recid" value="<?php echo $catresult['category_id'];?>">
		</div>
		<p class="clearfix"></p>
		<?php echo form_close(); ?> 
	 </div>
	</div>
 </div>
</div>
<?php $this->load->view('includes/footer'); ?>