<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/service_category'.($parent_id==0 ? '' : '/index/'.$parent_id);
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->
<!--Body-->
<div class="container-fluid">
 <div class="mid_area">
  <div class="mt10 p8">
   <div class="box_style">
    <div class="recent-table p10 mb15"><?php echo $heading_title; ?> <span class="white" style="float:right"><?php echo anchor($back_link,'Cancel','class="btn1" ' );?></span></div>
    <?php echo error_message();
    echo form_open_multipart("sitepanel/service_category/add/".$parent_id);
    
		$default_params = array(
		'heading_element' => array(
		'field_heading'=>"Service Category Name",
		'field_name'=>"category_name",
		'field_placeholder'=>"Service Category Name",
		'exparams' => 'size="40"'
		),
		'url_element'  => array(
		'field_heading'=>"Page URL",
		'field_name'=>"friendly_url",
		'field_placeholder'=>"Page URL",
		'exparams' => 'size="40"',
		'pre_seo_url' =>'',
		'pre_url_tag'=>FALSE
		)
		);
		
		if(is_array($parentData))
		{
			$pre_seo_url  = base_url().$parentData['friendly_url']."/";
			$default_params['url_element']['pre_seo_url'] = $pre_seo_url;
			$default_params['url_element']['pre_url_tag'] = TRUE;
			$default_params['url_element']['exparams'] = 'size="30"';
		}
		seo_add_form_element($default_params);?>
		
		<?php /*<p class="form_title1">Image :</p>
		<div class="form_field form_field2"><input type="file" name="category_image" class="form-control" style="width:450px;" />[ <?php echo $this->config->item('category.best.image.view');?> ]<?php echo form_error('category_image');?></div>
		<p class="clearfix"></p>
    
    <p class="form_title1">Alt :</p>
    <div class="form_field form_field2"><input name="category_alt" value="<?php echo set_value('category_alt');?>" type="text" class="form-control" style="width:450px;"><?php echo form_error('category_alt');?></div>
    <p class="clearfix"></p>*/?>
    
    <div class="form_title1">Description :</div>
    <div class="form_field form_field2">
      <textarea name="category_description" rows="5" cols="50" id="cat_desc" class="form-control" ><?php echo $this->input->post('category_description',TRUE);?></textarea> <?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('category_description');?>
    </div>
    <p class="clearfix"></p>
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
     <input type="submit" name="sub" value="Add" class="btn1" />
     <input type="hidden" name="action" value="addcategory" />
     <?php
     if(is_array($parentData))
     {
	     ?>
	     <input type="hidden" name="parent_id" value="<?php echo $parentData['category_id'];?>" />
	     <?php
     }?>
    </div>
    <p class="clearfix"></p>
    <?php echo form_close(); ?>
   </div>
  </div>
 </div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer');?>