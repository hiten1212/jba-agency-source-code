<?php



$sitepanel_menu_array = lang('top_menu_list'); 



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no;user-scalable=0;"/>
<title>Welcome to ::
<?php  echo $this->config->item("site_admin"); ?>
</title>
<script type="text/javascript" > var site_url = '<?php echo site_url();?>';</script>
<script type="text/javascript" > var base_url = '<?php echo base_url();?>';</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/developers/css/proj.css" />
<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<?php echo base_url(); ?>assets/sitepanel/css/main.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/sitepanel/css/conditional-preet.css" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/sitepanel/css/jquery.jqplot.min.css">
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/sitepanel/js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/sitepanel/js/jquery/ui/jquery-ui.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/sitepanel/js/jquery/ui/themes/ui-lightness/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/sitepanel/js/jquery/tabs.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/sitepanel/js/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/developers/js/common.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/sitepanel/js/common.js"></script>
</head>
<body>
<!--left Section-->
<?php if ( $this->session->userdata('admin_logged_in')==TRUE ){ ?>
<div class="left_nav closed">
  <div class="left_nav_scroll">
    <div class="p_open"></div>
    <p class="fs32 pt5 pr5 white text-right lft_panel_close"><span class="fa fa-times"></span></p>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="">
          <h4 class="panel-title"><a href="<?php echo base_url();?>sitepanel/dashbord" ><img src="<?php echo base_url(); ?>assets/sitepanel/image/lft-ico1.png" alt=""> <span> Dashboard</span></a></h4>
        </div>
      </div>
      <?php $this->load->view('dashboard/admin_left_view');?>
    </div>
  </div>
</div>
<?php }?>
<!--top-->
<div class="top_bg">
  <div class="container-fluid">
    <?php if ( $this->session->userdata('admin_logged_in')==TRUE ){ ?>
    <p class="nav_ico"><span class="lft_panel_open"><img src="<?php echo base_url(); ?>assets/sitepanel/image/menu.png" alt=""></span></p>
    <?php }?>
    <p class="logo p10"><img src="<?php echo $this->admin_info->header_logo!='' ? base_url().'uploaded_files/logo/'.$this->admin_info->header_logo:theme_url()."images/logo.png" ?>" width="200"  height="60" alt=""></p>
    <?php if ( $this->session->userdata('admin_logged_in')==TRUE ){ ?>
    <p class="logout"><?php echo '<span class="white">You are logged in as administrator |</span>'; ?> <a href="<?php echo base_url();?>sitepanel/logout"><img src="<?php echo base_url(); ?>assets/sitepanel/image/logout.png" alt=""></a></p>
    <?php }?>
    <p class="clearfix"></p>
  </div>
</div>
<p class="topfix"></p>
<!--top end-->
