<?php $this->load->view('includes/header');?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/dynamic_pages'.($parent_id==0 ? '' : '/index/'.$parent_id);
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
          <?php echo $heading_title; ?> <span class="white" style="float:right"><?php echo anchor($back_link,'Cancel','class="btn1" ' );?></span>
        </div>
         <?php //echo validation_message('alert');
    echo error_message();
    echo form_open_multipart("sitepanel/dynamic_pages/add/".$parent_id);?>
    
    <?php
     $default_params = array(
     'heading_element' => array(
     'field_heading'=>"Name",
     'field_name'=>"page_name",
     'field_placeholder'=>"Page Name",
     'exparams' => 'size="40"'
     ),
     'url_element'  => array(
     'field_heading'=>"Page URL",
     'field_name'=>"friendly_url",
     'field_placeholder'=>"Your Page URL",
     'exparams' => 'size="40"',
     'pre_seo_url' =>'',
     'pre_url_tag'=>FALSE
     )
     );
     seo_add_form_element($default_params);
     ?>
  <?php if($this->uri->segment(4)==''){?>
    <p class="form_title1">Image :</p>
    <div class="form_field form_field2"><input type="file" name="page_image" class="form-control" style="width:450px;" />[ <?php echo $this->config->item('page.best.image.view');?> ]<?php echo form_error('page_image');?></div>
    <?php }?>
    <p class="clearfix"></p>
    
   <?php /*?> <p class="form_title1">Alt :</p>
    <div class="form_field form_field2"><input name="page_alt" value="<?php echo set_value('page_alt');?>" type="text" class="form-control" style="width:450px;"><?php echo form_error('page_alt');?></div>
    <p class="clearfix"></p>
    <?php */?>
    <div class="form_title1">Description :</div>
    <div class="form_field form_field2">
      <textarea name="page_description" rows="5" cols="50" id="page_desc" class="form-control" ><?php echo $this->input->post('page_description');?></textarea> <?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('page_description');?>
    </div>
    <p class="clearfix"></p>
    
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Add" class="btn1" />
			  <input type="hidden" name="action" value="addcategory" />
			  <?php
			  if(is_array($parentData)){
				  ?>
				  <input type="hidden" name="parent_id" value="<?php echo $parentData['page_id'];?>" />
				  <?php
			  }?>
              </div>
    <p class="clearfix"></p>
    <?php echo form_close(); ?>
  </div>
  </div>
  
</div>
</div>
<!--Body End-->
<?php $this->load->view('includes/footer');?>