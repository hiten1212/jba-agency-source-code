<?php $this->load->view('includes/header'); ?>
<!--Breadcrumb-->
<?php  $back_link = 'sitepanel/dynamic_pages'.($pageresult['parent_id']==0 ? '' : '/index/'.$pageresult['parent_id']);
echo admin_breadcrumb($heading_title, array('Back To Listing'=>$back_link)); ?>
<!--Breadcrumb End-->

<!--Body-->
<div class="container-fluid">
<div class="mid_area"><!--
  <h1>Edit Product</h1>-->
    
  <div class="mt10 p8">
  
  <div class="box_style">
	<div class="recent-table p10 mb15">
         <?php echo $heading_title; ?> <span class="white" style="float:right"><?php echo anchor($back_link,'Cancel','class="btn1" ' );?></span>
        </div>
  	<?php echo error_message();
   echo form_open_multipart(current_url_query_string(),array('id'=>'catfrm','name'=>'catfrm'));?>
    
     <?php
     $default_params = array(
     'heading_element' => array(
     'field_heading'=>$heading_title." Name",
     'field_name'=>"page_name",
     'field_value'=>$pageresult['page_name'],
     'field_placeholder'=>"Page Name",
     'exparams' => 'size="40"'
     ),
     'url_element'  => array(
     'field_heading'=>"Page URL",
     'field_name'=>"friendly_url",
     'field_value'=>$pageresult['friendly_url'],
     'field_placeholder'=>"Your Page URL",
     'exparams' => 'size="40" ',
     )
     );
     seo_edit_form_element($default_params);
     ?>
   
   <?php if($pageresult['parent_id']=='0'){?>
  <p class="form_title1">Upload Image :</p>
    <div class="form_field form_field2"><input name="page_image" type="file" class="form-control" style="width:450px;">
	<?php
		   if($pageresult['page_image']!='' && file_exists(UPLOAD_DIR."/dynamic_pages_image/".$pageresult['page_image'])){
			   ?>
			   <a href="#"  onclick="$('#dialog').dialog({width:'auto',height:'auto'});">View</a> | <input type="checkbox" name="page_img_delete" value="Y" /> Delete 
			   <?php	
		   }?>
		 
		   [ <?php echo $this->config->item('page.best.image.view');?> ]
		   <div id="dialog" title="Page Image" style="display:none;"><img src="<?php echo base_url().'uploaded_files/dynamic_pages_image/'.$pageresult['page_image'];?>"  /></div>
		   <?php echo form_error('page_image');?></div>
           <?php }?>
           
    <p class="clearfix"></p>
    
     <?php /*?> <p class="form_title1">Alt :</p>
    <div class="form_field form_field2"><input name="page_alt" type="text" class="form-control" style="width:450px;" value="<?php echo set_value('page_alt',$pageresult['page_alt']);?>"><?php echo form_error('page_alt');?></div>
    <p class="clearfix"></p><?php */?>
    
    <div class="form_title1">Description :</div>
    <div class="form_field form_field2">
      <textarea name="page_description" rows="5" cols="50" id="page_desc" class="form-control"><?php echo set_value('page_description',$pageresult['page_description']);?></textarea><?php  echo display_ckeditor($ckeditor); ?><?php echo form_error('page_description');?>
    </div>
    <p class="clearfix"></p>
    
   
    
    <p class="form_title1"></p>
    <div class="form_field form_field">
    <input type="submit" name="sub" value="Update" class="btn1" />
		   <input type="hidden" name="action" value="editcategory" />
		   <input type="hidden" name="page_id" id="pg_recid" value="<?php echo $pageresult['page_id'];?>">
           </div>
    <p class="clearfix"></p>
    <?php echo form_close(); ?> 
  </div>
  </div>
  
</div>
</div>
<?php $this->load->view('includes/footer'); ?>