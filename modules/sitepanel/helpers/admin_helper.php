<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('CI'))
{
	function CI()
	{
		if (!function_exists('get_instance')) return FALSE;
		$CI =& get_instance();
		return $CI;
	}
}



if ( ! function_exists('admin_pagination'))
{
	function admin_pagination($base_uri, $total_rows, $record_per, $uri_segment,$refresh = FALSE)
	{
			$ci = CI();	
			
			$config['full_tag_open']        = '<ul class="pagination pagination-sm"> ';  
	        $config['full_tag_close']        = '</ul>';
					
			$config['per_page']			= $record_per;
		    $config['num_links']        = 8;	
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';	 	  
			$config['total_rows']		= $total_rows;
		    $config['uri_segment']		= $uri_segment;		
			$ci->load->library('pagination');
			
			$config['next_tag_open']		= '<li>';
			$config['next_tag_close']		= '</li> ';	
			$config['prev_tag_open']		= '<li>';
			$config['prev_tag_close']		= '</li> ';	
			$config['num_tag_open']		    = '<li>';
			$config['num_tag_close']		= '</li> ';
			$config['cur_tag_open']	= '<li class="active"><a href="javascript:void(0);" style="margin-left:3px;">';
		    $config['cur_tag_close']	    = '</a></li>';
					
			//$config['cur_tag_open']	= '&nbsp;<strong>';
		    //$config['cur_tag_close']	    = '</strong>';
			$config['page_query_string']	= TRUE;
			$config['base_url']             = $base_uri;
			$ci->pagination->initialize($config);
			$data = $ci->pagination->create_links();		
		 
		   return $data;	
		  
	}
}


function display_record_per_page()
{	
$ci = CI();
$post_per_page =  $ci->input->get_post('pagesize');

?>

<select name="pagesize" id="pagesize" class="form-control"  onchange="this.form.submit();">
    <?php
    foreach($ci->config->item('adminPageOpt') as $val)
    {
		
    ?>
    <option value="<?php echo $val;?>" <?php echo $post_per_page==$val ? "selected" : "";?>>
	  <?php echo $val;?></option>
    <?php
    }
    ?>
</select>

<?php
}
if ( ! function_exists('admin_category_breadcrumbs'))
{
	function admin_category_breadcrumbs($catid,$segment='')
	{
		$link_cat=array();	
		$ci = CI();		  
		$sql="SELECT category_name,category_id,parent_id
		FROM wl_categories WHERE category_id='$catid' AND status='1' ";		 
		$query=$ci->db->query($sql);		
		$num_rows     =  $query->num_rows();
		$segment      = $ci->uri->segment($segment,3);
			 
		  if ($num_rows > 0)
		  {
			 			  
				  foreach( $query->result_array() as $row )
				  {
							 
						if ( has_child( $row['parent_id'] ) )
						{
								
								$condtion_product   =  "AND category_id='".$row['category_id']."'";				
								$product_count      = count_products($condtion_product);
								
								if($product_count>0)
								{
									$link_url = base_url()."sitepanel/products?category_id=".$row['category_id'];
									
								}else
								{							
									$link_url = base_url()."sitepanel/category/index/".$row['category_id'];								
								}
								
								if( $segment!='' && ( $row['category_id']==$segment ) )
								{
									
									$link_cat[]=' <li class="active">'.$row['category_name'].'</li>';
									
								}else
								{
									
								  $link_cat[]=' <li><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';
								  
								}
								
								$link_cat[] = admin_category_breadcrumbs($row['parent_id'],$segment);
							 
						  }else
						  {	
							$link_url = base_url()."sitepanel/category/index/".$row['category_id'];				  
							$link_cat[] ='<li><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';	
									   
						  }     
					}    
		 }else
		 {
			  $link_url = base_url()."sitepanel/category";
			  $link_cat[]='<li><a href="'.$link_url.'">Category</a></li>';
			
		 }
		 
		 $link_cat = array_reverse($link_cat);
		 $var=implode($link_cat);
		 return $var;
		
	}
	
}

if ( ! function_exists('admin_service_category_breadcrumbs'))
{
	function admin_service_category_breadcrumbs($catid,$segment='')
	{
		$link_cat=array();	
		$ci = CI();		  
		$sql="SELECT category_name,category_id,parent_id
		FROM wl_service_categories WHERE category_id='$catid' AND status='1' ";		 
		$query=$ci->db->query($sql);		
		$num_rows     =  $query->num_rows();
		$segment      = $ci->uri->segment($segment,3);
			 
		  if ($num_rows > 0)
		  {
			 			  
				  foreach( $query->result_array() as $row )
				  {
							 
						if ( has_child( $row['parent_id'] ) )
						{
								
								$condtion_services   =  "AND category_id='".$row['category_id']."'";				
								$services_count      = count_services($condtion_services);
								
								if($services_count>0)
								{
									$link_url = base_url()."sitepanel/services?category_id=".$row['category_id'];
									
								}else
								{							
									$link_url = base_url()."sitepanel/service_category/index/".$row['category_id'];								
								}
								
								if( $segment!='' && ( $row['category_id']==$segment ) )
								{
									
									$link_cat[]=' <li class="active">'.$row['category_name'].'</li>';
									
								}else
								{
									
								  $link_cat[]=' <li><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';
								  
								}
								
								$link_cat[] = admin_service_category_breadcrumbs($row['parent_id'],$segment);
							 
						  }else
						  {	
							$link_url = base_url()."sitepanel/service_category/index/".$row['category_id'];				  
							$link_cat[] ='<li><a href="'.$link_url.'">'.$row['category_name'].'</a></li>';	
									   
						  }     
					}    
		 }else
		 {
			  $link_url = base_url()."sitepanel/service_category";
			  $link_cat[]='<li><a href="'.$link_url.'">Category</a></li>';
			
		 }
		 
		 $link_cat = array_reverse($link_cat);
		 $var=implode($link_cat);
		 return $var;
		
	}
	
}

function createMenu($arr_items,$level='top')
{
  $menu_items_count =  count($arr_items);
  if($menu_items_count > 0)
  {
	 foreach($arr_items as $key1=>$val1)
	 {
		 $menu_id = trim(strtolower($key1));
		 ?>
		 <li<?php echo $level=='top' ? ' id="'.$menu_id.'"' : '';?>>
		 <?php
		 if(is_array($val1) && !empty($val1))
		 {
			 
			?>
		   <a class="<?php echo $level;?>"><?php echo $key1; ?></a> <ul><?php createMenu($val1,'parent');?></ul>
		  <?php 
		 }
		 else
		 {
		 ?>
			<a href="<?php echo base_url().$val1; ?>"<?php echo $level=='top' ? ' class="top"' : '';?>><?php echo $key1; ?></a>
			
		 <?php 
		 }
		 ?>
		 </li>
		<?php
	 }
  }
}

if( ! function_exists('operation_allowed') )
{
	function operation_allowed($tbl,$recId,$autoFld='id',$whereFld='created_by')
	{
		$ci=CI();
		$allowed=FALSE;
		if($ci->admin_type==2)
		{
			$qry=$ci->db->query("select $autoFld from $tbl where $autoFld='".$recId."' and $whereFld='".$ci->admin_id."'");
			if($qry->num_rows() > 0 )
			{
				$allowed=TRUE;
			}
		}
		else
		{
			$allowed=TRUE;
		}
		return $allowed;
	}
}

if ( ! function_exists('admin_page_breadcrumbs'))
{
	function admin_page_breadcrumbs($catid,$segment='')
	{
		$link_cat=array();	
		$ci = CI();		  
		$sql="SELECT page_name,page_id,parent_id
		FROM wl_dynamic_pages WHERE page_id='$catid' AND status='1' ";		 
		$query=$ci->db->query($sql);		
		$num_rows     =  $query->num_rows();
		$segment      = $ci->uri->segment($segment,3);
			 
		  if ($num_rows > 0)
		  {
			 			  
				  foreach( $query->result_array() as $row )
				  {
							 
						if ( has_child_pages( $row['parent_id'] ) )
						{
								
								
									$link_url = base_url()."sitepanel/dynamic_pages/index/".$row['page_id'];								
								
								
								if( $segment!='' && ( $row['page_id']==$segment ) )
								{
									
									$link_cat[]=' <li class="active">'.$row['page_name'].'</li>';
									
								}else
								{
									
								  $link_cat[]=' <li><a href="'.$link_url.'">'.$row['page_name'].'</a></li>';
								  
								}
								
								$link_cat[] = admin_page_breadcrumbs($row['parent_id'],$segment);
							 
						  }else
						  {	
							$link_url = base_url()."sitepanel/dynamic_pages/index/".$row['page_id'];				  
							$link_cat[] ='<li><a href="'.$link_url.'">'.$row['page_name'].'</a></li>';	
									   
						  }     
					}    
		 }else
		 {
			  $link_url = base_url()."sitepanel/dynamic_pages";
			  $link_cat[]='<li><a href="'.$link_url.'">Main Pages</a></li>';
			
		 }
		 
		 $link_cat = array_reverse($link_cat);
		 $var=implode($link_cat);
		 return $var;
		
	}
	
}

if (!function_exists('common_dropdown')) {



	function common_dropdown($name, $selval, $tbl_arr, $extra = '', $stval = '', $multiselect = FALSE) {

		$CI = CI();

		$select_fld = $tbl_arr['select_fld'];

		$tbl_name = $tbl_arr['tbl_name'];

		$where = $tbl_arr['where'];



		$fld_arr = explode(",", $select_fld);

		$id = $fld_arr[0];

		$title = $fld_arr[1];



		$query = $CI->db->query("select $select_fld from $tbl_name where 1 $where order by $title");



		$arr = array();

		if ($stval != 'no') {

			if ($stval != '') {

				$arr = array('' => $stval);

			} else {

				$arr = array('' => "Select One");

			}

		}





		if ($query->num_rows() > 0) {

			$res = $query->result();





			foreach ($res as $val) {

				$cid = $val->$id;

				$arr[$cid] = $val->$title;

			}

		}



		return ($multiselect) ? form_multiselect($name, $arr, $selval, $extra) : form_dropdown($name, $arr, $selval, $extra);

		//return form_dropdown($name,$arr,$selval,$extra);

	}



}


if ( ! function_exists('admin_breadcrumb')){
	function admin_breadcrumb($page_title, $crumbs=""){
		?>
        <div class="breadcrumb">
<div class="container-fluid">
  <ol class="breadcrumb">
    <li><?php echo anchor('sitepanel/dashbord','Dashboard');?></li>
    <?php
		   if(@is_array($crumbs)){
			   foreach($crumbs as $key=>$val){
				   echo '<li>'.anchor($val,$key,array("title"=>$key,"itemprop"=>"url"))."</li>";
			   }
		   }?>
    <li class="active"><?php echo $page_title;?></li>
  </ol>
</div>
</div>
		<?php
	}
}