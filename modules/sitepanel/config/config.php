<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Global
|--------------------------------------------------------------------------

*/
$config['site_admin'] = "JBA Agency.";



$config['site_admin_name'] = "JBA Agency";

$config['category.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 285*195 )";

$config['product.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 600*548 )";

$config['brand.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 180X122 )";

$config['city.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 180X122 )";

$config['broucher.best.image.view'] = "( File should be .rtf .doc .docx .pdf format and file size should not be more then 4 MB (4096 KB)) ";

$config['service.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 1400X600 )";

$config['gallery.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 1025X707)";
$config['galleryimg.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 840X594)";

$config['broucher.best.image.view'] = "( File should be .rtf .doc .docx .pdf format and file size should not be more then 4 MB (4096 KB)) ( Best image size 100X142)";

$config['slide.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 1400X550 or 1400X600 )";

$config['slide.best.video.view'] = "( File should be .mp4 format and file size should not be more then 4 MB (4096 KB)) ( Best video size 2mb )";  

$config['news.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 450X450 )"; 

$config['page.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 390X240 )"; 

$config['blog.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 750X394 )";
$config['events.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 500X328 )"; 
$config['header_logo.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 270X80 )";
$config['footer_logo.best.image.view'] = "( File should be .jpg, .png format and file size should not be more then 4 MB (4096 KB)) ( Best image size 241X70 )";
$config['total_product_images'] = "5";

$config['total_news_images'] = "3";

$config['total_service_images'] = "1";

$config['total_blog_images'] = "1";


$config['pagesize'] = "20";

$config['adminPageOpt'] = array(
	$config['pagesize'],
	2*$config['pagesize'],
	3*$config['pagesize'],
	4*$config['pagesize'],
	5*$config['pagesize'],
	6*$config['pagesize'],
	7*$config['pagesize'],
	8*$config['pagesize'],
	9*$config['pagesize'],
	10*$config['pagesize'],
	20*$config['pagesize']
);

$config['bannersz'] =  array(

'Home Page Top Right'=>"290x130",



'Home Page Middle'=>"1027x101",



'Home Page Bottom'=>"505x210"



);







$config['bannersections'] = array(



'inner_page'=>"Inner Page ( Best image size 1600X411 )"

//'home_page'=>"Home Page (Middle) ( Best image size 600X188 )"





/*,



'category'=>"Category",



'subcategory'=>"Subcategory",



'login'=>"Login",



'register'=>"Registration",



'myaccount'=>"My Account",



'search'=>'Search',



'cart'=>"Cart",



'checkout'=>"Checkout",



'static'=>'Static Pages',



'testimonials'=>'Testimonials',



'faq'=>'FAQ',



'sitemap'=>'Sitemap',



'video'=>'Video',



'wholesale inquiry'=>'Wholesale Inquiry'*/



);







$config['admin_groups'] =  array(



'2'=>"Half Admin Rights",



'3'=>"Low Admin Rights",



);















/* End of file account.php */



/* Location: ./application/modules/sitepanel/config/sitepanel.php */