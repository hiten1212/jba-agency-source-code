<?php
class Products extends Public_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model(array('category/category_model','products/product_model','brand/brand_model'));
		$this->load->helper(array('products/product','category/category','query_string'));
		$this->load->library(array('Dmailer'));
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
	}	
	
	public function index(){
		
		$this->page_section_ct = 'product';
		$offset                 = (int) $this->input->post('offset');
		$curr_symbol = display_symbol();
		$posted_data = is_array($this->input->post()) ? array_filter($this->input->post()) : array();
		if($this->input->post()){
			$posted_data= array_filter($this->input->post());
			unset($posted_data['submit']);
		}
		
		$condition               = array();
		$cat_res = '';
		//$record_per_page        = (int) $this->input->post('per_page');
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');
		$category_id = $this->input->post('category_id') ? (int) $this->input->post('category_id') : $this->uri->rsegment(3);
		$page_segment           = find_paging_segment();
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$base_url      = ( $category_id!='' ) ?   "products/index/$category_id/pg/" : "products/index/pg/";
		$condition['status']     = '1';
		$condition['orderby']     = 'products_id asc';
		$page_title             = "Products";
		/*if($category_id=='brand')
		{
			$page_title               = "Our Products";
		}*/
		$where                   = "wlp.status ='1' ";		
		$featured = $this->input->post('featured',TRUE);
		
		if($this->uri->rsegment(3)=="featured" || $featured!=''){
			$where.=" AND featured_product = '1'";
			$page_title = 'Products';
		}

		$category_id     = (int) $this->input->post('category_id',TRUE);
		$keyword			=   trim($this->input->post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		if($category_id!='' )
		{
			$condition['category_id'] = $category_id;
		  //$where.=" AND wlp.category_id = '".$category_id."'";
		}

		if($keyword){
			$condition['keyword'] = $keyword;
			$where.=" AND (wlp.product_name LIKE '%".$keyword."%' OR  wlp.product_code LIKE '%".$keyword."%' )";
		}

		$brand            = $this->input->post('brand',TRUE);

		if($brand )
		{
			$condition['brand_id'] = $brand;
			//$where.=" AND wlp.product_brand_id = ".$brand;
		}

		if( is_array($posted_data) && !empty($posted_data ))
		{	
			$category_id            = (int) $this->input->post('category_id',TRUE);			
			$brand_ids            = $this->input->post('brands',TRUE);
			$brand_id            = $this->input->post('brand_id',TRUE);
			$prange                 =  $this->input->post('amount',TRUE);
			$prange = str_replace("$curr_symbol","",$prange);
			
			$minprice                 =  $this->db->escape_str($this->input->post('minprice',TRUE));
			$maxprice                 =  $this->db->escape_str($this->input->post('maxprice',TRUE));
			
			//trace($prange);
			if($category_id!='' )
			{
				$condition['category_id'] = $category_id;
				$cat_res = get_db_single_row('wl_categories','*'," AND category_id = '$category_id'");
				$page_title.=" (".$cat_res['category_name'].")";
			}
			
			if($brand_id!='' )
			{
				$condition['brand_id'] = $brand_id;
			}			

			if($brand_ids )
			{
				$brand_ids_array = explode(',',$brand_ids);
				$where.=" AND ( ";
				foreach($brand_ids_array as $brand_id){
					$where.="wlp.brand_id ='".$brand_id."' OR ";
				}
				$where = substr($where,0,-3);
				$where.=" )";
			}
						
			if(is_numeric($minprice) && is_numeric($maxprice)){
				$where.="  AND (IF(wlp.product_discounted_price > 0 , wlp.product_discounted_price,wlp.product_price) >= '".$minprice."' AND ( IF(wlp.product_discounted_price > 0 , wlp.product_discounted_price,wlp.product_price) <='".$maxprice."' ) ) ";
			}

		}
		
		$condition['where'] = $where;
		$res_array  =  $this->product_model->get_products($config['per_page'],$offset,$condition);
		//echo_sql();
		$config['total_rows'] = $data['totalProduct']	=  get_found_rows();

	    $data['frm_url'] = $base_url;
		
		$data['record_per_page'] = $record_per_page;
		$data['heading_title'] = $page_title;
		$data['res']           = $res_array;
		$data['cat_res'] = $cat_res;

		if($this->input->is_ajax_request())
		{
			$this->load->view('products/product_data',$data);
		}
		else
		{
			$this->load->view('products/view_product_listing',$data);
		}

	}
	
	public function search()
	{

		$this->page_section_ct = 'search';
		$curr_symbol = display_symbol();

		$condition               = array();
		$where                   = "wlp.status ='1'";
		$cat_res = '';

		$page_segment           = 3;

		//$record_per_page=$this->input->post("per_page");
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');

		$config['limit']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		//$offset                 = (int) $this->uri->segment($page_segment,0);
		$offset                 = (int) $this->input->post('offset');
		$base_url      =  "products/search/";

		$category_id     = (int) $this->input->get_post('category_id',TRUE);
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);
		
		if($category_id!='' )
		{
			$condition['category_id'] = $category_id;
			$where.=" AND FIND_IN_SET('".$category_id."',wlp.category_links )";
			
		  //$where.=" AND wlp.category_id = '".$category_id."'";
		}
		if($brand_id)
		{
			$where.=" AND wlp.brand_id = '".$brand_id."'";
		}
				
		
		if($keyword){
			$condition['keyword'] = $keyword;
			$where.=" AND (wlp.product_name LIKE '%".$keyword."%' OR  wlp.product_code LIKE '%".$keyword."%')";
		}
				
		$condition['where'] = $where;
		$res_array   =  $this->product_model->get_products($config['limit'],$offset,$condition);

		//echo_sql();

		$config['total_rows'] = $data['totalProduct']	=  get_found_rows();
		$data['res']              =  $res_array;
		//$data['page_links']       =  front_pagination($base_url,$config['total_rows'],$config['limit'],$page_segment);
		$data['record_per_page'] = $record_per_page;
		$data['heading_title']    = "Search Result";
		$data['cat_res'] = '';
		$data['frm_url'] = $base_url;
		if($this->input->is_ajax_request())
		{
			$this->load->view('products/product_data',$data);
		}
		else
		{
			$this->load->view('products/view_product_listing',$data);
		}
		//$this->load->view('products/view_product_listing',$data);
	}
	
	function product_post_review()
	{
		$this->form_validation->set_rules('poster_name',"Name",'trim|alpha|required|max_length[100]');
		$this->form_validation->set_rules('email',"Email",'trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('rating',"Select Rating",'trim|required');
		$this->form_validation->set_rules('comment',"Review",'trim|required|max_length[1000]');
		$this->form_validation->set_rules('verification_code',"Verification Code",'trim|required|valid_captcha_code');

		if($this->form_validation->run()===TRUE){
			$posted_data=array(
					'rev_type'   	=> 'P',
					'poster_name'   => $this->input->post('poster_name'),
					'email'    		=> $this->input->post('email'),
					'rate'        	=> $this->input->post('rating'),
					'prod_id' 		=> $this->input->post('prod_id'),
					'mem_id' 		=> $this->session->userdata('user_id'),
					'comment'       => $this->input->post('comment'),
					'posted_date'  	=> $this->config->item('config.date.time'),
					'status'       	=> '0',
			);
			$this->product_model->safe_insert('wl_review',$posted_data,FALSE);

			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your review has been posted successfully.We will get back to you soon.');

			$product_frindly_url=get_db_field_value('wl_products','friendly_url',array("products_id"=>$this->input->post('prod_id')));

			redirect($product_frindly_url,TRUE);
		}
	}
	

	public function detail(){	
	
		
		if($this->input->post('action')=='Submit') {
			$this->send_enquiry();
		}	

		$this->meta_info['entity_id'];
		$data['unq_section'] = "Product";
		$productId = (int) $this->meta_info['entity_id'];
		$option = array('productid'=>$productId);
		$res =  $this->product_model->get_products(1,0,$option);

		if(is_array($res)&& !empty($res) ){	
		
		if($this->input->post('action')=='review_post')
		{
			$this->product_post_review();
		}		

			$data['title'] = "Products";
			$data['res']       = $res;
			$media_res         = $this->product_model->get_product_media(5,0,array('productid'=>$res['products_id']));
			
			$related_products         = $this->product_model->related_products($res['products_id'],0,6);
			$data['related_products'] = $related_products;
			

			$data['media_res'] = $media_res;
			$this->load->view('products/view_product_details',$data);
		}else{
			redirect('category', '');
		}

	}	
	
	public function send_enquiry() {
		
		$this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		
		 $product_id = $this->input->post('prod_id');				
		
		$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required'); 
		$this->form_validation->set_rules('first_name', 'Full Name', 'trim|alpha|required|max_length[80]'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'trim|required|min_length[5]|max_length[25]|callback_valid_mobile[mobile_number]');		
			 
		$this->form_validation->set_rules('message', 'Comment', 'trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code', 'Verification code', 'trim|required|valid_captcha_code[enuiry]');

	$data['page_error'] = "";
	
	 
    if($this->form_validation->run()==TRUE){
			
			$product_name="";
			if($product_id > 0){
	    		$product_res =  get_db_single_row('wl_products','product_name, product_code, friendly_url',array('products_id'=>$product_id));
				$product_name=$product_res['product_name'];
			}
		
			$posted_data=array(
			'first_name'    => $this->input->post('first_name'),
			'type'           => '3',
			'email'         => $this->input->post('email'),
			'mobile_number'  => $this->input->post('mobile_number'),
			'message'       => $this->input->post('message'),
			'product_service'    => $product_name,
			'receive_date'     =>$this->config->item('config.date.time')
			);

			$posted_data = $this->security->xss_clean($posted_data);
			$this->product_model->safe_insert('wl_enquiry',$posted_data,FALSE);	
			// Send  mail to user
			$content    =  get_content('wl_auto_respond_mails','8');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$name = ucwords($this->input->post('first_name'));
			$body			=	str_replace('{mem_name}',$name,$body);
			$body 			=   str_replace('{body_text}', 'You have placed an enquiry and details are given below.', $body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			//$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{product_name}',$product_res['product_name'],$body);
			$body			=	str_replace('{message}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);			
		
			$this->dmailer->mail_notify($mail_conf);
			// End send  mail to user
			// Send  mail to admin
			$body       =  $content->email_content;
			$name = 'Admin';
			$body			=	str_replace('{mem_name}',ucwords($this->input->post('first_name')),$body);
			$body 			=   str_replace('{body_text}', 'You have received an enquiry and details are given below.', $body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			//$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{product_name}',$product_res['product_name'],$body);
			$body			=	str_replace('{message}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			//trace($mail_conf);
			//exit;
			$this->dmailer->mail_notify($mail_conf);
			// End send  mail to admin
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully.');
			
			//$link_url = base_url().$product_res['friendly_url'];			
			//redirect($link_url."#enquiry", '');
			redirect_top('pages/thanks', '');
		}
		
		    
		$data['product_id'] = $this->uri->rsegment(3);
	$product_name =  get_db_field_value('wl_products','product_name'," AND products_id='".$data['product_id']."' ");
	$data['product_name'] = $product_name;
	$data['is_header'] = false;
    $this->load->view('products/view_send_enquiry',$data);
	
  }


	public function download_pdf(){		

		$pId=(int)$this->uri->segment(3,0);		

		if($pId > 0){			

			$pdf_name='experience';

			$file=get_db_field_value('wl_products', $pdf_name, array("products_id"=>$pId));

			if($file !="" && @file_exists(UPLOAD_DIR."/docs/".$file)){

				$this->load->helper('download');

				$data = file_get_contents(UPLOAD_DIR."/docs/".$file);

				$name = $file;

				force_download($name, $data);

			}else{

				redirect('products', '');

			}

		}else{

			redirect('products', '');

		}

	}

	

	public function string_search()

	{

		$mysearchString            = $this->input->post('mysearchString',TRUE);

		if($mysearchString){

			$query = 	$this->db->query("SELECT product_name

											FROM wl_products

											WHERE product_name

											LIKE '$mysearchString%' AND status = '1'

											LIMIT 20"); // limits our results list to 20.



			if($query) {

				foreach($query->result() as $value) {

					echo '<li onClick="fill(\''.$value->product_name.'\')";>'.substr($value->product_name,0,20).'</li>';

				}

			}else{

				echo 'Oops! some technical error occured';

			}

		}

	}



	
	

	function product_application()

	{

		$pid = (int) $this->uri->rsegment('3');

		$product_application = "";

		if(!empty($pid) && $pid > 0){

			$product_application = get_db_field_value('wl_products','product_application'," AND products_id='".$pid."' ");

		}

		$data['product_application']       = $product_application;

		

		$data['page_heading'] = "Product Application";

		$this->load->view('products/product_application',$data);

	}

	

	function product_specification()

	{

		$pid = (int) $this->uri->rsegment('3');

		$product_specification = "";

		if(!empty($pid) && $pid > 0){

			$product_specification = get_db_field_value('wl_products','product_specification'," AND products_id='".$pid."' ");

		}

		$data['product_specification']       = $product_specification;

		

		$data['page_heading'] = "Product Specification";

		$this->load->view('products/product_specification',$data);

	}

	

	public function valid_mobile($field)

	{

		

		//$mobile = $this->input->post('mobile_number');

		if($field!=''){

		$mobile = $field;

		

		$this->form_validation->set_message('valid_mobile', 'The %s field may only contain number,+,-.');

		

		return ( ! preg_match("/^([0-9\-\+])+$/i", $mobile)) ? FALSE : TRUE;

		

		}



	}
	
	public function keyword_suggestions()
	{
		$inputString = trim($this->input->post('mysearchString'));

		if($inputString!="")
		{
			$keywords = explode(" ",$inputString);
			$search_cond='(';
			foreach($keywords as $kwd){
				$search_cond.="wlp.product_name LIKE '%".$kwd."%' AND ";
			}
			$search_cond=substr($search_cond,0,-5);
			$search_cond.=") AND wlp.status ='1'";

			$param = array();
			$param['where'] = $search_cond;
			$result = $this->product_model->get_products(10,0,$param);
			$data = array();
			$data['result'] = $result;
		}

		$data['keyword'] = $inputString;
		$this->load->view('products/keyword_suggestions',$data);

	}
	
}

?>