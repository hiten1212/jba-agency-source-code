<p class="fs11 float-right clear_all"><a href="javascript:void(0)" class="uu ">Clear All</a></p>
<h2 class="fs14 d-none d-lg-block">Filter Results</h2>
<h2 class="d-md-block d-lg-none showhide hand">Filter Results <i class="fas fa-bars"></i><p class="clearfix"></p></h2>

<div class="filter_dis">
<div class="flter_bx">
<div class="filt_hed">Categories</div>


<div id="categories_list" class="list_area">
<?php
if($parent_id > 0)
{
	$category_name = get_db_field_value('wl_categories','category_name'," AND category_id = '$parent_id' ");
	$condtion_array = array(
	'field' =>"*,(SELECT COUNT(category_id) FROM wl_categories AS b
	WHERE b.parent_id=a.category_id ) AS total_subcategories",
	'condition'=>"AND parent_id = '$parent_id' AND status='1'",
	'order'=>'sort_order',
	'debug'=>FALSE
	);
	$condtion_array['offset'] = 0;
	$condtion_array['limit'] = 25;
	$res = $this->category_model->getcategory($condtion_array);
	$total_categories	=  $this->category_model->total_rec_found;
	if($total_categories > 0){
		?>

<p class="left_attribute"> <a href="javascript:void(0)"><b><?php echo $category_name;?></b></a></p>

<?php
   foreach($res as $val)
   {
	   $link_url = site_url($val['friendly_url']);
	   ?>
	   <li><a href="<?php echo $link_url;?>"><?php echo $val['category_name'];?></a></li>
	   <?php
   }?> 
</ul>
</li> 
		<?php }
		}else{?>
        <?php 
$condtion_array = array(
'field' =>"category_id, category_name, friendly_url",
'condition'=>"AND parent_id = '0' AND status='1' AND category_id != '".$parent_id."' ",
'order'=>'sort_order',
'debug'=>FALSE
);

$condtion_array['offset'] = 0;
$condtion_array['limit'] = 20;
$level_0_res = $this->category_model->getcategory($condtion_array);

if(is_array($level_0_res) && !empty($level_0_res))
{
?>
<?php
$m=1;
	  foreach($level_0_res as $val)
	  {
		  $link_url = site_url($val['friendly_url']);
		  $category_name = strlen($val['category_name']) > 40 ? char_limiter($val['category_name'],40,'') : $val['category_name'];
		  // check sub categories
		  $condtion_array_1 = array(
		  'field' =>"category_id, category_name, friendly_url",
		  'condition'=>"AND status='1' AND parent_id = '".$val['category_id']."' ",
		  'order'=>'sort_order',
		  'debug'=>FALSE
		  );
		  
		  $condtion_array_1['offset'] = 0;
		  $condtion_array_1['limit'] = 20;
		  $level_1_res = $this->category_model->getcategory($condtion_array_1);
		  if(is_array($level_1_res) && !empty($level_1_res))
		  {
		  	$expanded = ($m=='1') ? 'true' : 'false';
			$show = ($m=='1') ? 'show' : '';
			  ?>
<p class="left_attribute1" data-toggle="collapse" data-target="#cat<?php echo $val['category_id'];?>">
<a href="javascript:void(0)"><?php echo $category_name;?></a></p>
<div id="cat<?php echo $val['category_id'];?>" class="ml20 mb10 subcate_list collapse <?php echo $show;?>" style="">
<div class="scroll_bar2" id="style2">
<?php
	foreach($level_1_res as $val1)
	{
		$link_url_1 = site_url($val1['friendly_url']);
		?>

<p class="left_attribute"><a href="<?php echo $link_url_1;?>"><?php echo $val1['category_name'];?></a></p>
<?php
	}?>
</div>
</div>
<?php $m++;}else{?>
<p class="left_attribute"><a href="<?php echo $link_url;?>"><?php echo $category_name;?></a></p>

<?php }
}?>
  <?php }
  }?>






</div>

</div>
</div>