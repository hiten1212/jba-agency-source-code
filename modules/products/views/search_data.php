<?php
if(is_array($res) && !empty($res)){
	foreach($res as $val){
		$link_url = site_url($val['friendly_url']);		
		$prodImage=get_image('products',$val["media"],'272','272');
		$alt_tag=$val['product_alt'];		
		?>
        <li class="listpager">
        	<div class="feat-sec">
            <p class="feat-title"><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><?php echo char_limiter($val['product_name'],50,'..');?></a></p>
            <div class="feat-box">
            <figure><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><img src="<?php echo $prodImage;?>" alt="<?php echo $alt_tag;?>" class="imf-fluid" title="<?php echo $val['product_name'];?>" alt=""></a></figure>
            </div>
            <p class="text-center"><a href="<?php echo $link_url;?>" class="vew-but">view details</a></p>
            </div>        	                        
        </li>           		
		<?php
	}
}?>