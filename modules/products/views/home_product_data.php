<?php //trace($res);
if(is_array($res) && !empty($res)){
	foreach($res as $val){
		$link_url = site_url($val['friendly_url']);		
		$prodImage=get_image('products',$val["media"],'286','261');
		$alt_tag=$val['product_alt'];?>	
        
        <div class="item">
<div class="prod_hom_out">
<div class="prod_area">
<figure><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><img src="<?php echo $prodImage; ?>" alt="<?php echo $alt_tag;?>" class="img-fluid"/></a></figure>
</div>
<div class="prod_txt_area">
<p class="prod_txt_hed"><a href="<?php echo $link_url;?>" title="<?php echo $val['product_name'];?>"><?php echo $val['product_name'];?></a></p>
<p class="prod_cont"><?php echo char_limiter($val['products_description'],200); ?></p>
<p class="prod_vew"><a href="<?php echo $link_url;?>" title="View Details">View Details</a></p>
</div>
</div>
</div>
 
		<?php
	}
}?>