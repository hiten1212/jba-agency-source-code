<?php $this->load->view("top_application");
$segment=3;
@$catid = (int) $cat_res['category_id'];
if($catid){
	echo category_breadcrumbs($catid,$segment);
}else{	
	echo navigation_breadcrumb($heading_title);	
}?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   }</script>
<div class="container">
<div class="mid_area">
<div>
<h1 class="mb-2"><?php echo $heading_title;?></h1>

<div class="row">


<?php /*?><div class="col-lg-3 mb-4">
<?php 
//if(is_array($cat_res) && !empty($cat_res)){
   //@$data['parent_id'] = $cat_res['parent_id'];
   //@$data['cat_id'] = $cat_res['category_id'];
   $this->load->view('products/left_categories',$data);
//}
?>
</div><?php */?>
<!-- filter section end -->



<div class="col-lg-12">
<?php
if(is_array($res) && !empty($res)){?>
<div id="my_data" class="pro_list">
     <ul id="prodListingContainer">
      <?php $data = array('res'=>$res);
      $this->load->view('products/product_data',$data);?>
     </ul>
     <div class="clearfix"></div>
     <?php if($totalProduct > $record_per_page){?>
     <p class="mt20 mb20 text-center" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p>
     <?php }?>
    </div>
<?php
}else{
	?>
	<p class="mt20 mb20 text-center"><strong><?php echo $this->config->item('no_record_found');?></strong></p>
	<?php
}?> 

</div>
<!-- category section end -->

</div>



</div>
</div>
</div>
<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>