<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php $category_id=$res['category_id'];
echo category_breadcrumbs($res['category_id'],'',$res['product_name']);
?>
  <?php echo error_message();?>
<!-- MIDDLE STARTS -->
<div class="container">
<div class="mid_area">
<div class="w-100 mt-3 mb-4">
<div class="row">

<div class="col-lg-5 col-xl-5">
<div class="pc_box">
<div class="dtl_pic">
<figure><a href="<?php echo get_image('products',$res['media'],900,900,'R');?>" class="MagicZoom" id="Zoomer" title="" data-options="zoomMode:magnifier;"><img src="<?php echo get_image('products',$res['media'],470,429,'R');?>" alt=""  class="zoom_img" ></a></figure>
</div>
<?php

	   ?>
<div class="ac o-hid rel thum_w">
<div id="dtl_scroll" class="owl-carousel owl-theme ">

<?php
	     $thumbc['width']=600;
   $thumbc['height']=548;
   $thumbc['source_path']=UPLOAD_DIR.'/products/';
   if(is_array($media_res) && !empty($media_res)){
	  foreach($media_res as $v){
		  $thumbc['org_image']=$v['media'];
		  Image_thumb($thumbc,'R');
		  $cache_file="thumb_".$thumbc['width']."_".$thumbc['height']."_".$thumbc['org_image'];
		  $catch_img_url=thumb_cache_url().$cache_file;
		  ?>
<div class="item">
<div class="ds_thm">
<figure><a href="<?php echo img_url();?>products/<?php echo $v['media'];?>" data-zoom-id="Zoomer" data-image="<?php echo $catch_img_url;?>"><img src="<?php echo get_image('products',$v['media'],100,91,'R');?>" alt="<?php echo $res['product_alt'];?>"></a></figure>
</div>
</div>
<?php } ?>

<div class="clearfix"></div>

</div>

<div class="clearfix"></div>
</div>
<?php } ?>
</div>
</div>
<!-- left section end -->


<div class="col-lg-7 col-xl-7">
<div class="detail_sec">
<h1><?php echo $res['product_name'];?></h1>
<p class="fs16 mb-2"><strong>Product Code :</strong> <?php echo $res['product_code'];?></p>
<p class="mt-2"> <a href="<?php echo base_url();?>products/send_enquiry/<?php echo $res["products_id"];?>" class="btn btn-danger pop1" data-type="iframe">Send Enquiry</a> </p>
<div class="clearfix"></div>

<div class="w-100 border pt-2 pl-2 pr-2 pb-4 mt-4">
<p class="fs20 black weight600 bb pb-2">Description</p>
<div class="scroll_bar mt-2 pr-2" id="style1">
<?php if($res['products_description']!=''){?>
<div class="fs14 black"><?php echo $res['products_description'];?>
</div>
<?php } ?>
</div>
</div>
<!-- description section end -->

</div>
</div>
<!-- right section end -->


</div>
<!-- image or content section end -->

<div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<!-- video section start -->
<div class="w-100 mt-5 mb-4">
<div class="row">
<?php if(!empty($res['youtube_code'])){?>
<div class="col-md-6 mb-2">
<div class="w-100 p-2 border">
<?php echo  html_entity_decode($res['youtube_code']);?>

</div>
</div>
<?php } ?>
<!-- end -->

<?php if(!empty($res['youtube_code2'])){?>
<div class="col-md-6 mb-2">
<div class="w-100 p-2 border"><?php echo  html_entity_decode($res['youtube_code2']);?>

</div>
</div>
<?php } ?>
<!-- end -->

</div>
</div>
<!-- video section end -->

</div>
</div>
<div class="clearfix"></div>
<!-- MIDDLE ENDS -->

<?php $this->load->view("bottom_application");?>