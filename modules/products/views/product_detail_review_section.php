<?php
$param	=	array('productid'=>$res['products_id'],'rev_status'=>'1');
$list_review=$this->product_model->get_review($limit='100',$offset='0',$param);
// trace($list_review); 
?>

<div class="container nds" id="reviews">
		  <div class="dtl_cont">
<div class="acc_box mt30 pb10 noprint">
<p class="fs16 blue text-uppercase oswald"><strong>Customer Reviews</strong></p>
    <hr>
	
<div class="mt20 dtl_testimonials">
<ul class="myulx">
<?php
if(is_array($list_review)&& !empty($list_review)){
 foreach($list_review as $key=>$val)
 {
	 ?>
<li>
<div class="dtl_test_head">
<p class="blue"><?php echo $val['poster_name'];?> <b><?php echo getDateFormat($val['posted_date'],1);?></b> </p>
<p class="mt5 fs12"><?php echo review_rating_html($val['rate'], 5);?><?php /*?><i class="fa fa-star orange" aria-hidden="true"></i><i class="fa fa-star orange" aria-hidden="true"></i><i class="fa fa-star orange" aria-hidden="true"></i><i class="fa fa-star orange" aria-hidden="true"></i><i class="fa fa-star gray" aria-hidden="true"></i><?php */?></p>

</div>
<div class="dtl_test_text">

<?php /*?><div class="img_left">
<p class="testimonials_thum"><span><img src="<?php echo theme_url();?>images/testi-thum1.jpg" alt=""></span></p>
</div><?php */?>

<div class="cnt"><b>&nbsp;</b> <?php echo $val['comment'];?> </div>
<p class="clearfix"></p>

</div>
</li>
<?php }
}?>


</ul>
<a name="post_reviews" id="post_reviews"></a>
</div> 
<?php echo form_open(current_url_query_string().'#post_reviews','name=prd_review');?>
    <div class="post_cmnts"> 
      <p class=" fs22">Post Your Review</p>
      <div class="form-group">	  
        <div class="row">
          <div class="col-12 col-md-6 p-2">
            <input autocomplete="off" name="poster_name" placeholder="Name *" type="text" value="<?php echo set_value('poster_name',$this->session->userdata('name'));?>"><?php echo form_error('poster_name');?>
          </div>
          <div class="col-12 col-md-6 p-2">
            <input name="email" type="text" placeholder="Email Id *" autocomplete="off" value="<?php echo set_value('email',$this->session->userdata('username'));?>"><?php echo form_error('email');?>
          </div>
          
         <div class="col-12 col-md-6 p-2">
           <input type="text" id="rating" name="rating"  class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="<?php echo set_value('rating',4);?>" data-size="xs" title=""><?php echo form_error('rating');?>
          </div>
          <?php /*?><div class="col-12 col-md-6 p-2">
           <input type="file" name="image">
          </div>   <?php */?>       
         
          <div class="col-12 p-2">
            <textarea name="comment" rows="4" id="comment" placeholder="Comments *" autocomplete="off"><?php echo set_value('comment');?></textarea><?php echo form_error('comment');?>
          </div>
          <div class="col-12 p-2 pt-0">
            <p>
              <input type="text" class="fl" id="verification_code" name="verification_code" placeholder="Enter Code *" style="width:130px;"> <img src="<?php echo base_url(); ?>captcha/normal" id="captchaimage" alt="" class="vam ml20"> <a href="javascript:viod(0);" ><img src="<?php echo theme_url();?>images/refresh.png" alt="" onclick="document.getElementById('captchaimage').src='<?php echo site_url(); ?>captcha/normal/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code').focus();"></a>
			  
             </p>
            <p class="clearfix"></p>
            <p class="mt5 fs12">Type the characters shown above.</p>
			<?php echo form_error('verification_code');?>
          </div>
		  
        </div>
		
      </div>
      <label>
        <button type="submit" id="submit" class="btn btn-success"> Post </button>
		<input type="hidden" name="action" value="review_post">
<input type="hidden" name="prod_id" value="<?php echo $res['products_id'];?>">
      </label>
    </div>
	<?php echo form_close();?>
	
  </div>
</div></div>