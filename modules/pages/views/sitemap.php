<?php $this->load->view('top_application');
$this->load->model(array('category/category_model'));
?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('Sitemap');?>

<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1>Sitemap</h1>
<div class="sitemap mt-2">
<div class=" mb-2 bg-4 p-2 black">
  <h4 class="mb-0">Quick Links</h4>
</div>
<a href="<?php echo base_url();?>" title="Home">Home</a> <a href="<?php echo base_url('about-company');?>" title="About Us">About Us</a> <a href="<?php echo base_url('industries');?>" title="Industries">Industries</a> <a href="<?php echo base_url('functions');?>" title="Functions">Functions</a> <a href="<?php echo base_url('resourses');?>" title="Resources">Resources</a> <a href="<?php echo base_url('contactus');?>" title="Support &amp; Contact Us">Support &amp; Contact Us</a>
<p class="clearfix"></p>
</div>
<?php  $condtion_array = array(
   'field' =>"page_id,page_name,parent_id,friendly_url,(SELECT COUNT(page_id) FROM wl_dynamic_pages AS b WHERE b.parent_id=a.page_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '1' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 5;
   $res_products = $this->dynamic_pages_model->getpages($condtion_array);
   $total_page_products	=  $this->dynamic_pages_model->total_rec_found;
   if($total_page_products > 0){
?>
<div class="sitemap mt-2">
<div class=" mb-2 bg-4 p-2 black">
  <h4 class="mb-0">Functions</h4>
</div>
<?php
	foreach($res_products as $val){
	 $link_url = site_url($val['friendly_url']);
	 $total_subcategories = $val['total_subcategories'];
	 ?>
                      <a  href="<?php echo $link_url;?>" title="<?php echo $val['page_name'];?>"><?php echo $val['page_name'];?></a>
<?php }
if($total_page_products >5)
{
?>
<a  href="<?php echo base_url('products');?>" title="View All &raquo;"><strong>View More</strong></a>
<?php } ?>
<p class="clearfix"></p>
</div>
<?php } ?>
<?php  $condtion_array = array(
   'field' =>"page_id,page_name,parent_id,friendly_url,(SELECT COUNT(page_id) FROM wl_dynamic_pages AS b WHERE b.parent_id=a.page_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '3' AND status='1'",
   'order'=>'sort_order',
   'debug'=>FALSE
   );
   $condtion_array['offset'] = 0;
   $condtion_array['limit'] = 5;
   $res_use = $this->dynamic_pages_model->getpages($condtion_array);
   $total_page_use	=  $this->dynamic_pages_model->total_rec_found;
   if($total_page_use > 0){
?>
<div class="sitemap mt-2">
<div class=" mb-2 bg-4 p-2 black">
  <h4 class="mb-0">information</h4>
</div>
<?php
	foreach($res_use as $val){
	 $link_url = site_url($val['friendly_url']);
	 $total_subcategories = $val['total_subcategories'];
	 ?>
                      <a  href="<?php echo $link_url;?>" title="<?php echo $val['page_name'];?>"><?php echo $val['page_name'];?></a>
<?php }
if($total_page_use >5)
{
?>
<a  href="<?php echo base_url('use');?>" title="View All &raquo;"><strong>View More</strong></a>
<?php } ?>
<p class="clearfix"></p>
</div>
   <?php } ?>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="clearfix"></div>


<?php $this->load->view("bottom_application");?>