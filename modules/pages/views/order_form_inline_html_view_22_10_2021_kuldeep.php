<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body style="font-size:14px; color:#333; margin:15px auto; padding:5px; font-family:Arial, Helvetica, sans-serif; min-width:1230px;width:1230px;line-height:22px;">
<div style="background:#fff;padding:20px;border-radius:10px;margin-bottom:20px;box-shadow:0 0 10px #ccc;">
  <div style="float:left;margin:20px 0 0 0;">
    <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">PILBARA POWDER COATINGS</h1>
    <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
  </div>
  <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
  <p style="margin:0;padding:0;clear:both;"></p>
  <div style="border:1px solid #ddd;padding:1rem!important;padding:0;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>
  <p style="margin:0;padding:0;clear:both;"></p>
<div style="float:left;width:48%;">
	<p style="font-size:1.8em;text-decoration: none;margin:2rem 0 0 0;padding:0;">Purchase Order</p>
	<p style="margin:1.5rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Customer</p>
	<div style="margin:15px 0 0 0;">
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Name</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['first_name'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Address</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['address'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Suburb, State Postcode</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['state'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Phone</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['phone'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Email</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['email'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
    </div>
  </div>	
  
<div style="float:right;width:48%;padding:22px 0 0 0;">
	<div style="padding:0;margin:0;width:44%;float:left;padding:.5rem!important;">
                  <input type="email" style="display:block;width:75%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="Date" value="<?php echo $res['purchase_date']?>"><img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""> </div>
	<div style="padding:0;margin:0;width:50%;float:right;padding:.5rem!important;">
                  <input type="email" style="display:block;width:94%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="PO" value="<?php echo $res['po_text'];?>"></div><p style="margin:0;padding:0;clear:both;"></p>
	<p style="margin:.4rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Ship To</p>
	<div style="margin:15px 0 0 0;">
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
	    <textarea style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;height: 342px;"><?php echo $res['ship_to'];?></textarea>
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
    </div>
  </div>	
<p style="margin:0 0 45px 0;padding:0;clear:both;"></p>	
	<table width="100%" border="1" rules="all" style="border: 1px solid #ddd;">
  <tbody>
    <tr>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Contact</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Ship Via</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Delivery</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Shipping Terms</td>
    </tr>
    <tr>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['contact_no'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['ship_via'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['delivery'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['shipping_term'];?>"></td>
    </tr>
    </tbody>
</table>
<p style="margin:0 0 65px 0;padding:0;clear:both;"></p>	
	<table width="100%" border="1" rules="all" style="border: 1px solid #ddd;">
  <tbody>
    <tr>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:18%">Item</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Description</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:20%">M2</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:20%">Total (ex. GST)</td>
    </tr>
	<?php 
	$get_order_detial_data = $this->db->query("SELECT * FROM wl_orders_products WHERE orders_id='".$res['order_id']."'")->result_array();
	if(is_array($get_order_detial_data) && !empty($get_order_detial_data))
	{
		foreach($get_order_detial_data as $key=>$val){
	?>
    <tr>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:87%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['item_name']?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:94.6%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['description']?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:88%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['m2']?>"></td>
      <td style="padding:.75rem;background:#f8f9fa"><input type="email" style="display:block;width:88%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['item_price']?>"></td>
    </tr>
	<?php }
	}?>
	
    </tbody>
</table>
<p style="margin:0 0 45px 0;padding:0;clear:both;"></p>	
	<table width="100%" border="1" rules="all" style="border: 1px solid #ddd;">
  <tbody>
    <tr>
      <td width="66%">&nbsp;</td>
      <td style="padding:.5rem 0"><table width="100%" border="0">
  <tbody>
    <tr>
      <td style="padding:.15rem .75rem;width:60%;font-size:16px">Subtotal</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer15" value="<?php echo $res['sub_total'];?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">GST</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer14" value="<?php echo $res['gst']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">Shipping</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer13" value="<?php echo $res['shipping']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">Other</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer12" value="<?php echo $res['other']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px"><strong>Total</strong></td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['total_price'];?>"></td>
    </tr>
    </tbody>
</table>
</td>
    </tr>
    </tbody>
</table>
  <p style="margin:0;padding:0;clear:both;"></p>
	<p style="margin:10px 0 15px 0;padding:0;">(Please use the drawing sheet below for detail works)</p>
	<p style="font-weight:600;font-size:1.1em;margin:0;padding:0;">Comments or Special Instructions</p>
	<div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
	    <textarea rows="6" style="display:block;width:98%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;"><?php echo $res['comment'];?></textarea>
      </div>
  <p style="margin:0 0 45px 0;padding:0;clear:both;"></p>
  <div>
    <div style="float:left;margin:20px 0 0 0;">
    <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">PILBARA POWDER COATINGS</h1>
    <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341

</p>
  </div>
    <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
    <p style="margin:0;padding:0;clear:both;"></p>
	  <div style="border:1px solid #ddd;padding:1rem!important;padding:0;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>
  </div>
	<p style="font-weight:600;font-size: 1.6em;text-transform: uppercase!important;margin-top: 1.5rem!important; margin: 0;padding:0;">DRAWING SHEET</p>
	<div style="border:#333 5px solid; min-height:600px;padding: 1rem;margin-top: .5rem;"></div>
	<p style="margin:.5rem!important 0 0 0;">
    <input name="" type="submit" value="Submit" style="font-size:1em;text-transform:uppercase;padding:10px 30px;border-radius:5px;color:#fff;background-color:#007bff;border-color:#007bff;display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">
  </p>
</div>
</body>
</html>