<?php $this->load->view("top_application"); ?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb($page_heading);?>
<?php $Res = get_db_single_row('tbl_admin','*'," AND admin_id='1' AND status='1'");?>
<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1>Contact Us</h1>

<div class="row contact_box">
<div class="col-12 col-lg-6">
<h2 class="fs20 mb-1">How to Reach?</h2>
<p class="gray fs16 mb-2 pb-1">Feel free to call us, we will be very happy to assist you.</p>

<div class="addrs_w">
<div class=" contact_add ">
<span><i class="fas fa-map-marker-alt"></i></span>
<p class="mt-2"><strong class="black">Registered Office:</strong><br>
<?php echo $this->admin_info->address;?></p></div>
<hr class="mb-3">

<div class="c_address">
<span><b><i class="fas fa-phone-volume"></i></b>&nbsp; <?php echo $this->admin_info->phone; ?></span>
<!--<hr class="mb-3">
<span><b><i class="fas fa-mobile-alt"></i></b>&nbsp; <?php echo $this->admin_info->contact_phone; ?></span>-->
<hr class="mb-3">
<span><b><i class="far fa-clock"></i></b>&nbsp;  Mon-Sat: 8:00AM - 6:00PM</span>
<hr class="mb-2">
<span><b><i class="fas fa-envelope"></i> </b>&nbsp; 
<a href="mailto:<?php echo $this->admin_info->admin_email; ?>"><?php echo $this->admin_info->admin_email; ?></a></span>
</div>

<div class="clearfix"></div>
</div>
</div>
<div class="col-12 col-lg-6"  id="enq">
<h2 class="fs20">Have a Query?</h2>
<div class="contact_form_cont">
<div class="gray fs16 mt5">Just Fill the Below Information:</div>
<?php echo form_open('pages/contactus#cid');?>
<fieldset class="contact_form mb-0 pb-0" style="border:none;">
<div class="clearfix"></div>
<div class="mt10">
<div class="mt-1">
<input type="text" class="large" name="first_name" id="first_name" placeholder="First Name *" value="<?php echo set_value('first_name');?>"><?php echo form_error('first_name');?>
</div>

<div class="mt-1">
<input type="text" class="large" name="last_name" id="last_name" placeholder="Last Name " value="<?php echo set_value('last_name');?>"><?php echo form_error('last_name');?>
</div>

<div class="mt-1">
<input type="text" class="large" name="email" id="email" placeholder="Email ID *" value="<?php echo set_value('email');?>"><?php echo form_error('email');?>
</div>

<div class="mt-1">
<input type="text" class="large" name="mobile_number" id="mobile_number" placeholder="Mobile Number *" value="<?php echo set_value('mobile_number');?>"><?php echo form_error('mobile_number');?>
</div>

<div class="mt-1">
<textarea class="large" name="message" id="message" class="large" cols="45" rows="4" placeholder="Enquiry/Comment *"><?php echo set_value('message');?></textarea><?php echo form_error('message');?>
</div>
<div>
<input name="verification_code" id="verification_code" type="text" placeholder="Enter Code *" class="vam" style="width:120px">
<img src="<?php echo site_url('captcha/normal');?>" class="vam" alt="" id="captchaimage" /> <a href="javascript:void(0);" title="Change Verification Code"><img src="<?php echo theme_url();?>images/ref.png"  alt="Refresh"  onclick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="ml-1 vam"></a><?php echo form_error('verification_code');?> </div>
<div class="mt-2">
<input name="submit" type="submit" value="Submit" class="btn btn-danger btn-md radius-3" onclick="window.location.href=('thankyou.htm')">
<input name="reset" type="reset" value="Reset" class="btn btn-default btn-md radius-3 ml3">
</div>
</div>
</fieldset>
<?php echo form_close();?>
</div>
</div>
</div>



</div>
</div>
</div><div class="">
<?php echo html_entity_decode($Res['domestic_google_map']); ?>
</div>
<div class="clearfix"></div>
<?php $this->load->view("bottom_application");?>