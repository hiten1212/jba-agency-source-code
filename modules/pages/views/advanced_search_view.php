<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Ebrahim Al Neama &amp; Sons Holding Group W.L.L</title>
<link rel="shortcut icon" href="<?php echo base_url() ?>favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;0,800;1,600;1,900&display=swap" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/developers/css/proj.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo theme_url();?>css/conditional_css.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body style="padding:0"> <?php $url = base_url()."category/show_category"; ?>
<div class="p-3 pt-1 pb-1">
<h1 class="black fs20 mb-2">Search</h1>
<?php echo form_open('products/search',array('name'=>"frm11",'method'=>'post','target'=>'_parent'));?>

<div class="mt10"><input name="keyword2" id="keyword" type="text" placeholder="Enter Keyword" class="form-control"></div>
<p class="mt10"> <select name="category_id" id="category_id" class="form-control" onchange="onclickcategory(this.value,'<?php echo $url;?>','subcategory_list')"> 
   <option value="">Select Category</option>
   <?php echo get_root_level_categories_dropdown(0);?>
  </select>
</p>
<?php /*?><p class="mt10">
 <select name="scategory_id" id="subcategory_list" class="form-control">
   <option value="">Sub-category</option>
  </select>
</p><?php */?>
<p class="mt10"><input name="submit" type="submit" value="Search" class="btn btn-primary" ></p>
<?php echo form_open();?>
</div>
<script>
function onclickcategory(category_value,ajax_url,response_id){	 
	$.ajax({type: "POST",
		url: ajax_url,
		dataType: "html",
		data: {"category_id":category_value},
		cache:false,
		success:function(data){
			$("#"+response_id+" option").remove();
			$("#"+response_id).append(data);}    
	}); 	
}
</script>  
</body>
</html>