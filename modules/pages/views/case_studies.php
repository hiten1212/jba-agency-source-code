<?php $this->load->view('top_application');
 $this->load->view('banner/inner_page_top_banner');
 echo navigation_breadcrumb('Case Studies');?>

<!-- MIDDLE STARTS -->
<div class="container">
<div class="inner_box">
<div class="cms">
<h1>Case Studies</h1>

<div class="appl_links">
<div class="row">
<div class="col-lg-4 no_pad"><a href="<?php echo site_url('axalta');?>" title="Axalta">Axalta</a></div>
<div class="col-lg-4 no_pad"><a href="<?php echo site_url('interpon');?>" title="Interpon">Interpon</a></div>
<div class="col-lg-4 no_pad"><a href="<?php echo site_url('dulux');?>" title="Dulux">Dulux</a></div>
<div class="col-lg-4 no_pad"><a href="<?php echo site_url('oxytech');?>" title="Oxytech">Oxytech</a></div>
</div>
</div>

</div>
</div>
</div>
<!-- MIDDLE ENDS -->

<?php $this->load->view("bottom_application");?>