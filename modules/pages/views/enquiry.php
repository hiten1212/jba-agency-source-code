<?php $this->load->view("top_application",array('is_header'=>FALSE));?>
<body style="padding:0">
<div class="p10 pt5 pb5">
 <h2 class="blue3 mb15">Enquiry Now</h2>
 <?php echo error_message();
 echo form_open('pages/enquiry');?>
 <div class="mt7"><input name="first_name" id="first_name" autocomplete="off" type="text" placeholder="Name *" class="p8 w100 radius-3" value="<?php echo set_value('first_name');?>" /><?php echo form_error('first_name');?></div>
 <div class="mt7"><input name="address" id="address" autocomplete="off" type="text" placeholder="Address *" class="p8 w100 radius-3" value="<?php echo set_value('address');?>" /><?php echo form_error('address');?></div>
 <div class="mt7"><input name="email" id="email" autocomplete="off" type="text" placeholder="Email ID *" class="p8 w100 radius-3" value="<?php echo set_value('email');?>" /><?php echo form_error('email');?></div>
 <div class="mt7"><input name="product_service" id="product_service" autocomplete="off" type="text" placeholder="Product Name *" class="p8 w100 radius-3" value="<?php echo set_value('product_service');?>" /><?php echo form_error('product_service');?></div>
 <div class="mt7">
  <select name="service_type" id="service_type" class="p8 w100 radius-3">
   <option value="Book a Service">Book a Service</option>
   <option value="Request a Demo">Request a Demo</option>
   <option value="Request a Quote">Request a Quote</option>
   <option value="Book a Training Session">Book a Training Session</option>
   <option value="Renew a Service Cover">Renew a Service Cover</option>
  </select>
 </div>
 <div class="mt7"><textarea name="message" id="message" cols="45" rows="5" class="p8 w100 radius-3" placeholder="Comment *"><?php echo set_value('message');?></textarea><?php echo form_error('message');?></div>
 <p class="mt7 fl"><input name="verification_code" id="verification _code" type="text" class="p8 radius-3"  placeholder="Word Verification *" value=""></p>
 <div class="mt12 fl ml10"><img src="<?php echo site_url('captcha/normal');?>" class="vam" alt="" id="captchaimage" /> <a href="javascript:void(0);" title="Change Verification Code"><img src="<?php echo theme_url();?>images/ref2.png"  alt="Refresh"  onclick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="vam"></a><?php echo form_error('verification_code');?></div>
 <div class="cb"></div>
 <p class="mt10"><input name="submit" type="submit" value="Submit" class="btn btn-danger trans_eff radius-3"></p>
 <?php echo  form_close();?>
</div>
</body>
</html>