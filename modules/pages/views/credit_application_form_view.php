<?php 
	$this->load->view('top_application');
	$this->load->view('banner/inner_page_top_banner');
	echo navigation_breadcrumb('Credit Application Form');
?>
<style>
 /* css handles the parentheses/brackets */
    .ordered_cls ul { counter-reset: item }
    .ordered_cls li { display: block ; counter-increment: item; }
    /* replace the counter before list item with
              open parenthesis + counter + close parenthesis */
    .ordered_cls li:before { content: " (" counter(item,lower-alpha) ") "; font-weight:bold; }
</style>
<!-- MIDDLE STARTS -->
<div class="container">
<div class="inner_box">
<div class="cms form_style">
<div class="float-md-left">
<h1>Credit Application Form</h1>
<p class="fs18 mt-1"><?php echo $this->config->item('site_name');?></p>
<p class="mt-2">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>

<div class="mt-3 text-danger border1 radius-10 p-3"><b>WARNING:</b> If you do not understand this document, you should seek independent legal advice. Please do not use correction fluid or tape as this is a legal document. Any corrections should be initialled.</div>
<?php 

echo form_open_multipart();?>

<div class="mt-4">
<p class="fs18 weight700 text-uppercase bg-dark white p-2 radius-5">APPLICANT DETAILS</p>
<?php echo validation_errors();?>
<div class="row mt-1">
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Customer Name</b></label>
<input type="text" class="form-control" name="name" value="<?php echo set_value('name');?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="abn"><b>ABN</b></label>
<input type="text" class="form-control" name="abn" value="<?php echo set_value('abn');?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="trading"><b>Trading Name</b></label>
<input type="text" class="form-control" name="trading_name" value="<?php echo set_value('trading_name');?>">
</div>
</div>
<?php 
$legal_entity_arr = array(
'1'=>'Sole Trader',
'2'=>'Partnership',
'3'=>'Pty Ltd',
'4'=>'Trust Co.',
'5'=>'Public Co.',
)?>
<div class="col-12 p-1">
<div class="form-group">
<p class="p-1"><b>Legal Entity</b></p>
<div class="row">
<?php foreach($legal_entity_arr as $key_leg=>$val_leg){?>
<div class="col-md-4 col-lg-3 col-xl-2 p-1">
<div class="form-check"><input class="form-check-input" name="legal_entity[]" <?php echo (@in_array($val_leg,$this->input->post('legal_entity'))) ? 'checked="checked"' : '';?>   type="checkbox" value="<?php echo $val_leg;?>" id="defaultCheck1"><label class="form-check-label" for="defaultCheck1"><?php echo $val_leg;?></label></div>
</div>
<?php }?>

</div>
</div>
</div>
<div class="col-md-6 p-1">
<div class="row">
<div class="col-md-8 p-1">
<div class="form-group">
<label for="street"><b>Street Address</b></label>
<input type="text" name="address" class="form-control" id="street" value="<?php echo set_value('address');?>">
</div>
</div>
<div class="col-md-4 p-1">
<div class="form-group">
<label></label>
<input type="text" name="address_post_code" class="form-control" placeholder="Post Code" value="<?php echo set_value('address_post_code');?>">
</div>
</div>
</div>
</div>
<div class="col-md-6 p-1">
<div class="row">
<div class="col-md-8 p-1">
<div class="form-group">
<label for="postal"><b>Postal Address</b></label>
<input type="text" class="form-control" id="postal" name="postal_address" value="<?php echo set_value('postal_address');?>" />
</div>
</div>
<div class="col-md-4 p-1">
<div class="form-group">
<label></label>
<input type="text" name="postal_address_code" class="form-control" placeholder="Post Code" value="<?php echo set_value('postal_address_code');?>">
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="telephone"><b>Office Telephone</b></label>
<input type="text" name="office_phone" class="form-control" id="telephone" value="<?php echo set_value('office_phone');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="fax"><b>Fax Number</b></label>
<input type="text" name="fax_number" class="form-control" id="fax" value="<?php echo set_value('fax_number');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="email"><b>Email Address</b></label>
<input type="text" name="email" class="form-control" id="email" value="<?php echo set_value('email');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="ah"><b>A.H. Telephone</b></label>
<input type="text" name="ah_phone" class="form-control" id="ah" value="<?php echo set_value('ah_phone');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="nature"><b>Nature of Business</b></label>
<input type="text" name="nature_business" class="form-control" id="nature" value="<?php echo set_value('nature_business');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="years"><b>Years in Business</b></label>
<input type="text" name="year_business" class="form-control" id="years" value="<?php echo set_value('year_business');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="accounts"><b>Accounts Contact</b></label>
<input type="text" name="account_contact" class="form-control" id="accounts" value="<?php echo set_value('account_contact');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="tel"><b>Acc. Telephone</b></label>
<input type="text" name="account_phone" class="form-control" id="tel" value="<?php echo set_value('account_phone');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="accmail"><b>Accounts Email</b></label>
<input type="text" name="account_email" class="form-control" id="accmail" value="<?php echo set_value('account_email');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="accmobile"><b>Acc. Mobile</b></label>
<input type="text" name="account_mobile" class="form-control" id="accmobile"  value="<?php echo set_value('account_mobile');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="purchasing"><b>Purchasing Contact</b></label>
<input type="text" name="purchase_contact" class="form-control" id="purchasing" value="<?php echo set_value('purchase_contact');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="mob"><b>Mobile No.</b></label>
<input type="text" name="mobile_number" class="form-control" id="mob" value="<?php echo set_value('mobile_number');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="credit"><b>Monthly Credit Req.</b></label>
<input type="text" name="monthly_credit_req" class="form-control" id="credit" value="<?php echo set_value('monthly_credit_req');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="date"><b>Today's Date</b></label>
<input type="date" name="todays_date" class="form-control" id="date" value="<?php echo set_value('todays_date');?>">
</div>
</div>
</div>


<p class="fs18 mt-5 weight700 text-uppercase bg-dark white p-2 radius-5">Trade References</p>

<div class="row mt-1">
<div class="col-md-6 col-lg-3 p-2 pb-0">
<label for="reference"><b>Reference Company</b></label>
</div>
<div class="col-md-6 col-lg-3 p-2 pb-0">
<label for="tradeContact"><b>Contact</b></label>
</div>
<div class="col-md-6 col-lg-3 p-2 pb-0">
<label for="tradephone"><b>Phone</b></label>
</div>
<div class="col-md-6 col-lg-3 p-2 pb-0">
<label for="tradeemail"><b>Email</b></label>
</div>
</div>

<?php 
$trade_reference_company 	= $this->input->post('trade_reference_company');
$trade_contact				= $this->input->post('trade_contact');
$trade_phone 				= $this->input->post('trade_phone');
$trade_email 				= $this->input->post('trade_email');

for($t=1;$t<=3;$t++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $t;?></p></div>
<div class="row mt-1">
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">

<input type="text" name="trade_reference_company[<?php echo $t;?>]" class="form-control" id="reference" placeholder="Enter Reference Company" value="<?php echo set_value('trade_reference_company[$t]',$trade_reference_company[$t]);?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">

<input type="text" name="trade_contact[<?php echo $t;?>]" class="form-control" id="tradeContact" placeholder="Enter Contact" value="<?php echo set_value('trade_contact[$t]',$trade_contact[$t]);?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<input type="text" name="trade_phone[<?php echo $t;?>]" class="form-control" id="tradephone" placeholder="Enter Phone" value="<?php echo set_value('trade_phone[$t]',$trade_phone[$t]);?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<input type="text" name="trade_email[<?php echo $t;?>]" class="form-control" id="tradeemail" placeholder="Enter Email" value="<?php echo set_value('trade_email[$t]',$trade_email[$t]);?>">
</div>
</div>
</div>
<?php }?>


<?php 
$soft_trade_name 		= $this->input->post('soft_trade_name');
$soft_trade_position	= $this->input->post('soft_trade_position');
$soft_trade_address 	= $this->input->post('soft_trade_address');
$soft_trade_mobile 		= $this->input->post('soft_trade_mobile');
?>
<p class="fs18 mt-5 weight700 text-uppercase bg-dark white p-2 radius-5">Sole Trader, Partner, Director, Trustee, Public Co. Officer Details</p>

<div class="row mt-1">
		  <div class="col-md-6 col-lg-3 p-2">
              <label for="dtl-name"><b>Name</b></label></div>
			 <div class="col-md-6 col-lg-2 p-2">
              <label for="dtl-pos"><b>Position</b></label></div>
				<div class="col-md-6 col-lg-5 p-2">
              <label for="dtl-addr"><b>Home Address</b></label></div>
					<div class="col-md-6 col-lg-2 p-2">
              <label for="dtl-mob"><b>Mobile No.</b></label></div>
		  </div>
		  
<?php for($sf=1;$sf<=4;$sf++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $sf;?></p></div>
<div class="row mt-1">
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<input type="text" name="soft_trade_name[<?php echo $sf;?>]" class="form-control" id="dtl-name" placeholder="Enter Name"  value="<?php echo set_value('soft_trade_name[$sf]',$soft_trade_name[$sf]);?>">
</div>
</div>
<div class="col-md-6 col-lg-2 p-2">
<div class="form-group">
<input type="text" name="soft_trade_position[<?php echo $sf;?>]" class="form-control" id="dtl-pos" placeholder="Enter Position"  value="<?php echo set_value('soft_trade_position[$sf]',$soft_trade_position[$sf]);?>">
</div>
</div>
<div class="col-md-6 col-lg-5 p-2">
<div class="form-group">
<input type="text" name="soft_trade_address[<?php echo $sf;?>]" class="form-control" id="dtl-addr" placeholder="Enter Address"  value="<?php echo set_value('soft_trade_address[$sf]',$soft_trade_address[$sf]);?>">
</div>
</div>
<div class="col-md-6 col-lg-2 p-2">
<div class="form-group">
<input type="text" name="soft_trade_mobile[<?php echo $sf;?>]" class="form-control" id="dtl-mob" placeholder="Enter Mobile No."  value="<?php echo set_value('soft_trade_mobile[$sf]',$soft_trade_mobile[$sf]);?>">
</div>
</div>
</div>
<?php }?>

<p class="fs18 mt-5 weight700 text-uppercase bg-dark white p-2 radius-5">FINANCIAL INFORMATION</p>
<div class="row">
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="turnover"><b>Turnover last financial year ($)</b></label>
<input type="text" name="finc_turn_over_last_year" class="form-control" id="turnover" placeholder="" value="<?php echo set_value('finc_turn_over_last_year');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="Capital"><b>Paid up Capital ($)</b></label>
<input type="text" name="finc_paid_up_capital" class="form-control" id="Capital" placeholder="" value="<?php echo set_value('finc_paid_up_capital');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="Accountants"><b>Accountants</b></label>
<input type="text" name="finc_accountants" class="form-control" id="Accountants" placeholder="" value="<?php echo set_value('finc_accountants');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="finContact"><b>Contact No.</b></label>
<input type="text" name="finc_contact_number" class="form-control" id="finContact" placeholder="" value="<?php echo set_value('finc_contact_number');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="fintel"><b>Telephone/Mobile</b></label>
<input type="text" name="finc_mobile_number" class="form-control" id="fintel" placeholder="" value="<?php echo set_value('finc_mobile_number');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="bankname"><b>Bank Name</b></label>
<input type="text" name="finc_bank_name" class="form-control" id="bankname" placeholder="" value="<?php echo set_value('finc_bank_name');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="bankaccount"><b>Account Name</b></label>
<input type="text" name="finc_account_name" class="form-control" id="bankaccount" placeholder="" value="<?php echo set_value('finc_account_name');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="finbsb"><b>BSB</b></label>
<input type="text" name="finc_bsb" class="form-control" id="finbsb" placeholder="" value="<?php echo set_value('finc_bsb');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="finaccno"><b>Account No.</b></label>
<input type="text" name="finc_account_number" class="form-control" id="finaccno" placeholder="" value="<?php echo set_value('finc_account_number');?>">
</div>
</div>
<div class="col-md-6 col-lg-3 p-2">
<div class="form-group">
<label for="bankmanager"><b>Bank Manager's Name</b></label>
<input type="text" name="finc_bank_manager_name" class="form-control" id="bankmanager" placeholder="" value="<?php echo set_value('finc_bank_manager_name');?>">
</div>
</div>

<?php 
$credit_application_arr = array(
'1'=>'Credit Application',
'2'=>'Guarantee',
'3'=>'Limited Warranty',
'4'=>'Direct Debit Authority',
);
?>
<div class="col-12 p-2">
<p class="weight700">Credit Application For</p>
<div class="row">
<?php foreach($credit_application_arr as $key=>$val){?>
<div class="col-md-4 col-lg-3 p-1">
<div class="form-check"><input class="form-check-input" type="checkbox" name="credit_application_for[]" value="<?php echo $val;?>" <?php echo (@in_array($val,$this->input->post('credit_application_for'))) ? 'checked="checked"' : '';?> id="defaultCheck1"><label class="form-check-label" for="defaultCheck1"><?php echo $val;?></label></div>
</div>
<?php }?>
</div>
</div>
</div>

<p class="fs18 mt-5 weight700 text-uppercase bg-dark white p-2 radius-5">CREDIT ACCOUNT TERMS AND CONDITIONS</p>
<div class="mt-2 border1 p-3 radius-10">
<p class="weight700 text-uppercase">1. Definitions and interpretation</p>
<p>Company means Reefwind Pty Ltd ABN 21 069 120 341, trading as Pilbara Powder Coatings and any Related Bodies Corporate.<br>
Conditions mean these Credit Account Terms and Conditions.<br>
Consequential loss means loss of expected savings, loss of use, loss of opportunity, loss of profit, loss of revenue, increased financing costs, loss arising from delay, or any consequential, special or indirect loss or damage, whether or not the possibility or potential extent of the loss or damage was known or foreseeable, and whether arising from a claim under an indemnity, contract, tort (including negligence), statute or otherwise.<br>
Customer means the party making this credit application.<br>
Goods mean any item, goods, product, or thing supplied by the Supplier in their ordinary course of business.<br>
Insolvency Event means, for the Customer, as applicable, being unable to pay a bill on time, being in liquidation or provisional liquidation, bankruptcy or under administration, having a controller (as defined in the Corporations Act 2001) or analogous person appointed to the Customer or any of the Customer's property, being taken under section 459F(1) of the Corporations Act to have failed to comply with a statutory demand, being unable to pay the Customer's debts, dying, ceasing to be of full legal capacity or otherwise becoming incapable of managing the Customer's own affairs for any reason, taking any step that could result in the Company becoming an insolvent under administration (as defined in section 9 of the Corporations Act 2001), entering into a compromise or arrangement with, or assignment for the benefit of, any of the Customer's members or creditors, or any similar event.<br>
<b>Loss</b> means any expense, cost, or damage of any kind and includes consequential loss and a fine or penalty imposed by a statutory or other authority.<br>
<b>Real property</b> means all real property interests held by the Customer now or in the future.<br>
<b>Other property</b> means any other securities, investments, bonds, cash, chattels, goods, monies due and payable, and any other form of property besides Real Property, whether tangible or intangible.<br>
<b>Related Bodies Corporate</b> has the same meaning as in the Corporations Act 2001.<br>
<b>Services</b> mean any service or services provided by the Company and/or Supplier in their ordinary course of business.<br>
<b>Supplier</b> means that in connection with the supply of any Goods or Services, the Company and or Related Bodies Corporate makes the supply to Customer.<br>
<b>Supplier T&amp;C</b> means any terms and conditions of quotation and supply, in any way described, notified by the Supplier to the Customer (whether on an invoice, quote, through the Supplier's website, docket, or otherwise), or supply agreement (however described) in the Supplier's standard form entered into in writing between the Supplier and Customer.<br>
<b>Singular words</b> include the plural and vice versa. A mention of anything after include, includes or including, does not limit what else might be included.</p>

<p class="weight700 mt-5 text-uppercase">2. Structure of this document</p>
<ol>
<li>The terms and conditions set out in this Section 5 and any other Supplier Ts&Cs that may be entered into are intended to regulate key trading terms, including the extension of credit in connection with the supply of Goods or Services by the Supplier to the Customer.</li>
<li>The terms and conditions set out in this Section 5, and any other Supplier Ts&Cs are incorporated by reference as part of the Supplier Ts&Cs and are legally binding between the Supplier and the Customer. To the extent of any inconsistency between the terms and conditions in this Section 5 and any other Supplier Ts&Cs that may appear on the Supplier invoices, quotes, dockets, website, or anywhere else the terms and conditions in Section 5 of this Credit Account Terms & Conditions shall prevail.</li>
</ol>

<p class="weight700 mt-5 text-uppercase">3. Customer obligations</p>
<ol>
<li>These Conditions apply if the Supplier accepts any order for Goods or Services or both from the Customer, whether for cash or credit. The Customer has no entitlement to credit unless the Supplier's sole discretion extends credit to the Customer. If the Supplier gives the Customer credit, the Supplier still reserves the right at any time and for any reason in the Supplier's sole discretion to refuse to supply any other Goods or Services to the Customer on credit terms. If the Supplier does decline to give the Customer further credit, then that decision does not affect the credit terms which apply to any amounts the Customer then owes to the Supplier.</li>
<li>Where the Customer fails to comply with any of the terms of these Conditions or in respect of any obligation to pay money to Supplier when due, suffers an insolvency event or makes any misrepresentation to the Supplier, they can pay their account on time. In that case, the balance of the Customer's account to the Supplier will become due and payable immediately.</li>
<li>The Customer agrees:
<ul class="ordered_cls">
<li>Payment is to be made in full without any deduction or setoff of the total amount due and payable by the Customer, on delivery of the Goods and Services provided, or, if credit terms are offered, <span class="text-danger">by the 15th or 30th day (28th day for February) of the month of invoice by automatic direct debit.</span></li>
<li>Pay any stamp duty assessed on this document or fee to register or maintain any security interest held by the Supplier regarding Goods or Services supplied to the Customer.</li>
<li>Advise the Supplier in writing of any insolvency event, any change in its name, ownership or control, or any steps taken to sell an asset or assets (separately or together having a value being greater than 20% in value of its gross assets) as soon as practicable and not later than within two business days of such event, change or step occurring.<br>
The Customer acknowledges that the Customer remains liable to pay for all Goods and/or Services supplied by the Supplier despite any such event.</li>
</ul>
</li>
</ol>

<p class="weight700 mt-5 text-uppercase">4. Overdue Accounts</p>
<ol>
<li>Any amount not paid by the due date will incur interest at a rate of 5% above the Reserve Bank of Australia Cash Rate calculated daily and compounded monthly, but in no circumstances will the interest charged exceed 20% per annum.</li>
<li>The Customer agrees to pay all costs and incurred by the Supplier in connection with the recovery of overdue amounts and enforcing the charge in clause 4(c) expenses including legal costs, commissions paid by the Supplier or its Related Bodies Corporate to any commercial or mercantile agent and dishonor fees</li>
<li><b class="text-danger">SECURITY AND CHARGE:</b> As security for any amounts due to the Supplier from time to time, the Customer charges all of its legal and equitable interest (both present and future) of whatsoever nature held in any and all Real Property and Other Property to the Supplier.</li>
<li>Without limiting the generality of the charge in clause 4(c), the Customer agrees, on request by the Supplier, to execute any documents and do all things reasonably required by the Supplier (including if a beneficial owner, as beneficial owner under the Property Law Act 1969 (WA) and Electronic Conveyancing Act 2014 (WA) or of any similar implied term under the applicable governing law) to perfect the charge given in clause 4 (c) including registering mortgage security over any Real Property. The Customer appoints the Supplier to be the Customer's lawful attorney to execute and register such documents and take all such steps. The Customer indemnifies the Supplier on an indemnity basis against all costs and expenses incurred by the Supplier in preparing and registering the mortgage documents.</li>
<li>The Customer consents unconditionally to the Supplier lodging a caveat or caveats noting its interest in any Real Property.</li>
<li>The Customer consents unconditionally to the Supplier lodging a mortgage, lien, or other registerable interest against any Other Property.</li>
<li>The Customer consents to a fixed and floating charge being placed over the Customer's corporate entity.</li>
<li>The Customer consents unconditionally to the Supplier to provide payment security in favour of the Supplier over all monies owed by third parties to the Customer. The Supplier may unconditionally claim direct payment from any third party owing monies to the Customer, only to the maximum extent of monies owed by the Customer to the Supplier, including all expenses due under these T&amp;C</li>
<li>A statement in writing signed by an authorised officer of the Supplier setting out the money due or owing to the Supplier at the date of the statement shall be sufficient evidence of the amount so due or owing until the contrary is proven.</li>
</ol>

<p class="weight700 mt-5 text-uppercase">5. Retention of title</p>
<ol>
<li>The Supplier retains legal and equitable title in any Goods supplied to the Customer until payment in full for or in connection with the supply of the relevant Goods has been received by the Supplier. Until payment in full has been received, the following terms apply.</li>
<li>Notwithstanding that title in the Goods remains with the Supplier until payment has been received in full, the Customer may sell such goods or use the Goods in a manufacturing or construction process in the ordinary course of the Customer's business. As between the Customer and the purchaser of any Goods, the Customer sells as principal and not as an Agent of the Supplier. The proceeds of the sale of each item of Goods must be held by the Customer in a separate fund on trust for the Supplier, and the Customer is under a duty to account to the Supplier for such proceeds. The creation of, or failure of, any trust shall not in any way limit the obligation of the Customer to pay all amounts to the Supplier for Goods supplied.</li>
<li>Until Goods are sold or used in any process, the Customer must keep the Goods safe and free from deterioration, destruction, loss, or harm, clearly designate the Goods as the property of the Supplier, store them in such a way they are clearly identified as the property of the Supplier and keep complete records, firstly, of the physical location of the Goods and, secondly, the ownership of the Goods by the Supplier.</li>
<li>The Supplier is irrevocably entitled at any time, and from time to time before the sale of any item of Goods by the Customer, to inspect or to recover and retake possession of such Goods and otherwise exercise all rights in relation to the Goods whether those rights are as owner and/or unpaid Seller or otherwise and whether those rights are conferred by common law, contract, statute or in any other way. To exercise such entitlement, the Supplier and its agents are irrevocably authorised by the Customer to enter any of the Customer's premises or vehicles or those of any third party. The Customer agrees to obtain the consent of any such third party to such entry by the Supplier and to indemnify the Supplier and its agents for any liability arising from any entry upon such third parties' premises or vehicles. The Supplier and its agents agree to take all reasonable care in removing the Goods from such premises or vehicles but, to the extent this liability may be disclaimed by law, are not liable for any damage or injury to such premises caused by the removal of the Goods.</li>
<li>This reservation of title and ownership is effective whether or not the Goods have been altered from their supplied form or commingled with other goods.</li>
</ol>


<p class="weight700 mt-5 text-uppercase">6. Security interest PPSA</p>
<div>
<ol>
<li>The retention of title arrangement described in clause 5 constitutes the grant of a purchase money security interest by the Customer in favour of the Supplier in respect of all present, and after-acquired Goods supplied to the Customer by the Supplier.</li>
<li>The Customer must immediately, if requested by the Supplier, sign any documents, provide all necessary information and do anything else required by the Supplier to ensure that the Supplier's purchase money security interest is perfected.</li>
<li>The Customer will not enter into any security agreement that permits any other person to have or register any security interest in respect of the Goods or any proceeds from the sale of the Goods until the Supplier has perfected its purchase money security interest.</li>
<li>For any Goods supplied that are not goods that are used predominately for personal, domestic, or household purposes, the parties agree to contract out of the application of ss 95, 118, 121(4), 130, 132(4),135, 142 or 143 of the PPSA about the Goods.</li>
<li>The Customer hereby waives any rights the Customer may otherwise have to:
<ul class="ordered_cls">
<li>Receive any notices the Customer would otherwise be entitled to receive under ss 95, 118, 121, 130, 132 or 135</li>
<li>Apply to a Court for an order concerning the removal of an accession under section 97</li>
<li>Object to a proposal of the Customer to purchase or retain any collateral under ss 130 and 135</li>
<li>Receive a copy of a verification statement confirming registration of a financing statement, or a financing change statement, relating to any security interest the Supplier may have in Goods supplied to the Customer from time to time.</li>
</ul>
</li>
<li>For the purpose of this clause, "PPSA" means the Personal Property Securities Act 2009.</li>
</ol>
<p>The expressions "accession", "collateral", "financing statement", "financing change
statement", "security agreement", "security interest," "perfected security interest" and
"verification statement" have the meanings given to them under, or in the context of the
PPSA. References to sections are to sections of the PPSA.</p>
</div>

<p class="weight700 mt-5 text-uppercase">7. Risk</p>
<div>Risk concerning any Goods passes to the Customer on delivery of the Goods. Delivery of Goods will be at the Supplier's premises when collecting the Goods by the Customer, its employees, agent or contractors. If the Supplier has expressly agreed to ship the Goods, risk in the Goods passes immediately on delivery of the Goods to the Customer's designated place of delivery by the Supplier or its agent.
<br><br>
Any goods provided to the Supplier by a Customer for any purpose whatsoever is done so at the Customer's own risk. The Customer agrees to indemnify and hold harmless the Supplier for any damage or loss that may occur while the Customer's goods are in the custody of the Supplier.</div>

<p class="weight700 mt-5 text-uppercase">8. Unclaimed Goods</p>
<div>The Customer has fourteen (14) days from the completion of any Goods Services supplied by the Supplier to pick up the Goods and Services from the Suppliers premises. Notification of the completed works and/or Services shall be deemed to commence from either or agreed to completion date specified on any valid purchase order, any change to this date agreed to in writing or, notification by email from the Supplier to the Customer's email address on their website. No other form of notice is valid.
<br><br>
Any Goods owned by the Customer and/or Goods and Services provided by the Supplier that are not picked up within fourteen (14) days after completion of any works and Services supplied by the Supplier become the lawful property of the Supplier in lieu of payment.
<br><br>
The Supplier reserves the right to either charge the Customer a daily holding fee of fifty dollars ($50.00) plus GST or dispose of the unclaimed Goods & Services in any manner the Supplier sees fit. This includes selling the Goods & Services for profit.</div>

<p class="weight700 mt-5 text-uppercase">9. Exclusion of implied terms</p>
<div>The Customer may have the benefit of consumer guarantees under the Australian Consumer Law. Otherwise, to the maximum extent permitted by law, all terms, conditions, or warranties would be implied into Supplier Ts&Cs or in connection with the supply of any Goods or Services by the Supplier under law or statute or custom or international convention are excluded.</div>

<p class="weight700 mt-5 text-uppercase">10. Limitation of liability</p>
<div>To the maximum extent permitted by law and subject to clauses 9 and 11, the Supplier's total liability arising out of or in connection with its performance of its obligations pursuant to these Conditions, the Supplier Ts&Cs, or arising out of or in connection with the supply of specific Goods or Services (including pursuant to or for breach of these Conditions, Supplier Ts&Cs or repudiation thereof, under statute law, in equity or for tort, including negligent acts or omissions) is limited as follows:
<ol>
<li>the Supplier shall have no liability to the Customer for any Consequential Loss;</li>
<li>the Supplier's total aggregate liability for loss, however arising, shall not exceed the GST exclusive aggregate price paid by the Customer to the Supplier for the specific Goods or Services that gave rise to the loss in question. The limitations and exclusions in this sub-clause 9 (b) do not apply to the extent that any Loss is directly attributable to:
<ul class="ordered_cls">
<li>the personal injury or death caused by the Supplier's default, breach of these Conditions or the Supplier Ts&Cs or negligence; or</li>
<li>fraud by the Supplier.</li>
</ul>
</li>
</ol>
<p class="mt-3">Each party must take reasonable steps to mitigate any loss it suffers or incurs.</p>
</div>

<p class="weight700 mt-5 text-uppercase">11. Limitation of liability under Competition and Consumer Act 2010 (CCA)</p>
<div>
<ol>
<li>The Customer must inspect the Goods on delivery and must within seven (7) days of delivery notify the Supplier in writing of any evident defect/damage, shortage in quantity, or failure to comply with the description or quote. The Customer must notify any other alleged defect in the Goods as soon as reasonably possible after any such defect becomes evident. Upon such notification, the Customer must allow the Supplier to inspect the Goods.</li>
<li>Under applicable State, Territory, and Commonwealth Law (including, without limiting the CCA), certain statutory implied guarantees and warranties (including, without limiting the statutory guarantees under the CCA) may be implied into these terms and conditions (Non-Excluded Guarantees).</li>
<li>The Supplier acknowledges that nothing in these terms and conditions purports to modify or exclude the Non-Excluded Guarantees.</li>
<li>Except as expressly set out in these terms and conditions or in respect of the Non-Excluded Guarantees, the Supplier makes no warranties or other representations under these terms and conditions, including but not limited to the quality or suitability of the Goods. The Supplier's liability in respect of these warranties is limited to the fullest extent permitted by law.</li>
<li>If the Customer is a consumer within the meaning of the CCA, the Supplier's liability is limited to the extent permitted by section 64A of Schedule 2.</li>
<li>If the Supplier is required to replace the Goods under this clause or the CCA but cannot do so, the Supplier may refund any money the Customer has paid for the Goods.</li>
<li>If the Customer is not a consumer within the meaning of the CCA, the Supplier's liability for any defect or damage in the Goods is
	<ul class="ordered_cls">
    <li>limited to the powder coating manufacturer's warranty;</li>
    <li>recoating at the Supplier's premises only;</li>
    <li>otherwise negated absolutely.</li>
    </ul>
</li>
<li>The Supplier shall not be liable for any defect or damage and shall not provide any warranty for:
	<ul class="ordered_cls">
    <li>the Customer failing to maintain or store any Goods properly</li>
    <li>transport, installation, uninstalling, fixing, or removal of any Goods;</li>
    <li>the Customer using the Goods for any purpose other than that for which they were designed;</li>
    <li>the Customer continuing the use of any Goods after any defect became apparent or should have become apparent to a reasonably prudent operator or user;</li>
    <li>the Customer failing to follow any instructions, guidelines, care and maintenance provided by the Supplier</li>
    <li>fair wear and tear, any accident, or act of God.</li>
    </ul>
</li>

</ol>
</div>

<p class="weight700 mt-5 text-uppercase">12. GST</p>
<div>If the Supplier has any liability to pay Goods and Services Tax (GST) on the supply of any Goods or Services to the Customer, the Customer must pay to the Supplier an amount equivalent to the GST liability of the Supplier at the same time as the consideration is paid for the Goods or Services (unless the consideration for that supply is explicitly expressed to be GST inclusive).</div>

<p class="weight700 mt-5 text-uppercase">13. Privacy disclosure and consent</p>
<div>
<p>The Customer authorises Supplier to:</p>
<ul class="ordered_cls">
<li>obtain credit information about its personal, consumer, and commercial creditworthiness from any their Accountant, Bank, or trade referee disclosed in this document and from any other credit provider or credit reporting agency to assess this application for credit, or in connection with any guarantee given by the Customer</li>
<li>use, disclose, or exchange with other credit providers information about the Customer's credit arrangements to assess this application for credit, monitor creditworthiness and collect overdue accounts, and</li>
<li>disclose the contents of any credit report on the Customer to the Supplier and its Related Bodies Corporate and any of their solicitors and mercantile agents</li>
</ul>
<p class="mt-3">Supplier complies with the privacy principles imposed by law about the collection and disclosure of information regarding individuals. For more details on the way the Supplier manages personal information, refer to www.pilbarapowdercoatings.com.au.</p>
</div>

<p class="weight700 mt-5 text-uppercase">14. Direct Debit Authority</p>
<div>The Customer agrees to sign the Irrevocable Direct Debit Authority to ensure payment of their account under clause 3(c)(i). This Irrevocable Direct Debit Authority is to remain in full force and effect while the term of this Credit Agreement exists and beyond until all outstanding monies are paid.</div>

<p class="weight700 mt-5 text-uppercase">15. Governing law</p>
<div>These Conditions are governed by and are to be interpreted according to the laws in force in the state of Western Australia and the parties submit to the non-exclusive jurisdiction of the courts operating in Western Australia. The operation of the United Nations Convention on Contracts for the Sale of International Goods is hereby excluded.</div>
</div>

<p class="fs18 weight700 text-uppercase bg-dark white p-2 radius-5 mt-5">ACKNOWLEDGMENT</p>
<p class="mt-3 weight700 fs16">Acknowledgment</p>
<p>The Customer acknowledges that the Goods it will acquire from the Supplier will be obtained for either the purpose of re-supply (whether or not in an altered form or as part of some other manufacture) or for the purpose of using them up or transforming them in trade or commerce in the course of a process of production or manufacture or in the course of repairing or treating other goods or fixtures on land.</p>

<p class="mt-3 weight700 fs16">Who must sign this Agreement on behalf of the Customer</p>
<p>For Companies: Where there is only one director for the Company, then that person must sign where there are two or more directors for the Company then 2 Directors or a Director + Company Secretary must sign; for a public or extensive reporting proprietary company, a duly authorised officer.<br>
Sole Traders operating under their name or a business name: The individual.<br>
Partnerships: All Partners of the Partnership.</p>

<p class="mt-3">Where this is not possible, please contact the Office &#8211; Pilbara Powder Coatings, 2015 Anderson Road, Karratha Industrial Estate WA (08) 9143 1837 or contactus@pilbarapowdercoatings.com.au</p>

<p class="mt-4 weight700 fs16">The Customer agrees to be bound by the Credit Account Terms and Conditions stated in Section 5.</p>
<div class="row">
<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Signatory</p>

<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Position</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>
				
<?php 
$ack_sig_print_name = $this->input->post('ack_sig_print_name');
$ack_sig_position	= $this->input->post('ack_sig_position');
$ack_sig_date 		= $this->input->post('ack_sig_date');

for($ack_sig=1;$ack_sig<=4;$ack_sig++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $ack_sig;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="ack_sig_print_name[<?php echo $ack_sig;?>]" class="form-control" id="sign-print" placeholder="Enter Print Name" value="<?php echo set_value('ack_sig_print_name[$ack_sig]',$ack_sig_print_name[$ack_sig]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="ack_sig_position[<?php echo $ack_sig;?>]" class="form-control" id="sign-pos" placeholder="Enter Position" value="<?php echo set_value('ack_sig_position[$ack_sig]',$ack_sig_position[$ack_sig]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<div class="row">
<div class="col-10 no_pad"><input type="date" name="ack_sig_date[<?php echo $ack_sig;?>]" class="form-control w-100" id="sign-date" placeholder="Select Date" value="<?php echo set_value('ack_sig_date[$ack_sig]',$ack_sig_date[$ack_sig]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>
</div>
</div>



<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Witness</p>

<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Position</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>
<?php 
$ack_wit_print_name = $this->input->post('ack_wit_print_name');
$ack_wit_position	= $this->input->post('ack_wit_position');
$ack_wit_date 		= $this->input->post('ack_wit_date');

for($ack_wit=1;$ack_wit<=4;$ack_wit++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $ack_wit;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="ack_wit_print_name[<?php echo $ack_wit;?>]" class="form-control" id="witn-print" placeholder="Enter Print Name" value="<?php echo set_value('ack_wit_print_name[$ack_wit]',$ack_wit_print_name[$ack_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="ack_wit_position[<?php echo $ack_wit;?>]" class="form-control" id="witn-pos" placeholder="Enter Position" value="<?php echo set_value('ack_wit_position[$ack_wit]',$ack_wit_position[$ack_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<div class="row">
<div class="col-10 no_pad"><input type="date" name="ack_wit_date[<?php echo $ack_wit;?>]" class="form-control w-100" id="witn-date" placeholder="Select Date" value="<?php echo set_value('ack_wit_date[$ack_wit]',$ack_wit_date[$ack_wit]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>

</div>
</div>

</div>

<p class="mt-5 mb-5 bb"></p>

<div>
<div class="float-md-left">
<h1>PERSONAL GUARANTEE & INDEMNITY FORM</h1>
<p class="fs18 mt-1">PILBARA POWDER COATINGS</p>
<p class="mt-2">and its Related Body Corporate ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>
</div>
<div class="mt-3 text-danger border1 radius-10 p-3"><b>WARNING:</b> THIS IS AN IMPORTANT DOCUMENT. IF YOU DO NOT UNDERSTAND THIS DOCUMENT YOU SHOULD SEEK INDEPENDENT LEGAL ADVICE.
<br><br>
<b>IMPORTANT:</b> As part of your application for credit, this Personal Guarantee and Indemnity Agreement must be completed and signed by all Directors, Sole Traders and Business Partners in the presence of Independent Witnesses. Spouses/Partners of all Directors, Sole Traders and Business Partners must sign as Guarantors in the presence of an independent witnesses where there is joint ownership of personal assets.</div>
<div class="row mt-3">
<div class="col-md-4 p-2">
<div class="form-group">
<label for="sign-print"><b>I/We, the Guarantor(s)</b></label>
<input type="text" name="guarantor_name" class="form-control" placeholder="Insert Guarantor(s) name as applicable" value="<?php echo set_value('guarantor_name');?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="sign-print"><b>hereby request the supplier to supply</b></label>
<input type="text" name="supplier_to_supply" class="form-control" placeholder="Insert Company Name/Partnership/Sole Trader" value="<?php echo set_value('supplier_to_supply');?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="sign-print"><b>Trading as (if applicable)</b></label>
<input type="text" name="trade_applicable" class="form-control" placeholder="Insert Registered Business Name" value="<?php echo set_value('trade_applicable');?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="sign-print"><b>(the "Customer") of</b></label>
<input type="text" name="guarantor_address" class="form-control" id="sign-print" placeholder="Insert Street Address (Not PO Box)" value="<?php echo set_value('guarantor_address');?>">
</div>
</div>
<div class="col-md-2 p-2">
<div class="form-group">
<label for="sign-print"></label>
<input type="text" name="guarantor_state" class="form-control" placeholder="State" value="<?php echo set_value('guarantor_state');?>">
</div>
</div>
<div class="col-md-2 p-2">
<div class="form-group">
<label for="sign-print"></label>
<input type="text" name="guarantor_zip_code" class="form-control" id="sign-print" placeholder="Postcode" value="<?php echo set_value('guarantor_zip_code');?>">
</div>
</div>
</div>
<p class="p-2">with Goods and Services on Credit.</p>

<div class="mt-2 border1 p-3 radius-10">
<p class="weight700 text-uppercase">Should the Supplier elect to supply Goods and/or Services:</p>
<ol>
<li>I/We guarantee payment to the Supplier of the total price charged by the Supplier for Goods or Services supplied to the Customer from time to time, without any deduction or setoff whatsoever. I/We also guarantee payment of any other monies now or in the future owing by the Customer to the Supplier.</li>
<li>I/We indemnify the Supplier against all costs, losses, and expenses that the Supplier incurs due to any default by the Customer. I/We agree to pay any stamp duty assessed on this Guarantee.</li>
<li>My/Our guarantee and indemnity under this Guarantee is a continuing guarantee and will not be affected:
  <ul class="ordered_cls">
  <li>if the Supplier grants any extension of time or other indulgences to the Customer or varies the terms of the Customer's account (even if this increases my/our liability under this Guarantee)</li>
  <li>by the release of any of the Guarantors or if this Guarantee is or becomes unenforceable against one or more of the Guarantors.</li>
  <li>any payment by the Customer being later avoided by law, whether or not I/we have been given notice of these matters.</li>
  </ul>
</li>
<li>I/We agree that an application for credit made by the Customer is deemed to have been accepted from the date of the first invoice by the Supplier to the Customer and, without further notice to me/us, this Guarantee will extend to all liabilities from the Customer to that Supplier.</li>
<li>This Guarantee extends to credit given to the Customer in the future by a company which is not now, but at the time such credit is extended, a Related Bodies Corporate.</li>
<li>This Guarantee may be withdrawn by the Guarantor(s) on expiry of 14 days following written notice of withdrawal being delivered to the Supplier's Company Secretary at the Supplier's registered office. This Guarantee will continue in force in respect of all debt incurred up to the date of withdrawal.</li>
<li>I/We authorise the Supplier to do each of the things listed in clause 12 of the Credit Account Terms and Conditions about my/our personal credit matters.
	<ul class="ordered_cls">
    <li>As security for the obligations and liabilities of the
Guarantor(s), I/we charge for the due and punctual payment
and performance of those obligations and liabilities, all of my/
our legal and equitable interest (including as beneficial owner,
both present and future) of whatsoever nature held in any and
all Real Property or Other Property in favour of the Supplier.</li>
    <li>Without limiting the generality of the charges in Clause 4, I/we agree on a request by the Supplier to execute any documents and do all things reasonably required by the Supplier to register mortgage security over any Real Property and Other Property. If the Guarantor(s) fails to deliver the requested documents, the Guarantor(s) hereby appoints Supplier to be the Guarantor's (s') lawful attorney to execute and register such documents. I/We indemnify the Supplier on an indemnity basis against all costs and expenses incurred by the Supplier, as the case may be in connection with the preparation and registration of such mortgage documents.</li>
    <li>I/We consent unconditionally to the Supplier lodging a caveat or caveats noting its interest in any Real Property.</li>
    <li>I/We consents unconditionally to the Supplier lodging a mortgage, lien, or other registerable interest against any Other Property.</li>
    <li>I/We agree to advise the Supplier in writing of the occurrence of any Insolvency Event, any change in my/our name, ownership or control, or any step being taken to sell an asset or assets (separately or together having a value being greater than 20% in value of my/our gross assets) as soon as practicable and not later than within two business days of such event, change or step occurring.</li>
    <li>If any payment made by or on behalf of the Customer is alleged to be void or voidable by any liquidator or like an officer of the Customer under any law related to insolvency, I/we indemnify the Supplier against any costs or losses it may incur in connection with such claim. This indemnity shall continue to apply notwithstanding any withdrawal under clause 6.</li>
    <li>If the charge created by clause 8(a) is or becomes void or unenforceable, it may be severed from this Guarantee without affecting the Supplier's rights against the Guarantor(s).</li>
    <li>If the Guarantor(s) is a trustee of a trust, the Guarantor(s) enters into this agreement in both the Guarantor's capacity and as trustee of that trust.</li>
    <li>Any condition or agreement under this Guarantee by or in favour of two or more persons is deemed to bind them jointly and severally or be in favour of each of them severally. If the Guarantor comprises more than one person, the Supplier may at any time, and from time to time, proceed against any or all of them in respect of the Guarantor's obligations as the Supplier may choose in its absolute discretion, and the Supplier is not to be obliged to make any claim against all the persons comprising the Guarantor.</li>
    <li>Until the whole of the Customer's obligations have been paid or satisfied in full, the Guarantor must not (except with the prior written consent of the Supplier) either directly or indirectly, and either before or after the winding up or bankruptcy of the Customer, or any person, take any steps to recover or enforce a right or claim against the Customer relating to any sum paid by the Guarantor to the Supplier under this Guarantee including without limitation proving or claiming in competition with the Supplier to diminish any distribution, dividend or payment which, but for the proof or claim, the Supplier would be entitled to receive according to the winding up or bankruptcy of the Customer.</li>
    <li>The definitions in the Credit Account Terms and Conditions shall apply in this Guarantee, except that "Real Property" shall mean all real property owned by the Guarantor(s) now or in the future, solely or jointly. Also, singular words include the plural and vice versa, and references to any party to this Guarantee include that party's executors, administrators, substitutes, successors or permitted assigns.</li>
    </ul>
</li>
</ol>
</div>


<p class="fs18 mt-5 weight700 text-uppercase bg-dark white p-2 radius-5">PERSONAL GUARANTEE & INDEMNITY FORM</p>
<p class="mt-2 fs15 weight700">CERTIFICATE OF GUARANTEE - Executed as a Deed</p>
<div class="mt-3 border1 radius-10 p-3"><b class="fs15">Acknowledgment:</b><br>
By signing below as Guarantor(s), I/we certify that I/we understand the terms of this Guarantee. In particular, I/we understand that if the Customer fails to make any required payments to the Supplier, the Supplier may recover the amount of these payments from me/us. In such case, the Supplier may, amongst other recovery rights, take a charge over any Real Property.
<br><br>
That I/We have had the opportunity of taking independent legal advice about the meaning and effect of this Guarantee. All Directors, Sole Traders, Business Partners and any other Guarantors complete, print and sign below as Guarantors in the presence of Independent Witnesses (not Spouses or Family Members).</div>
<div class="row">
<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Guarantor</p>

<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Address</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>

<?php 
$per_gua_print_name = $this->input->post('per_gua_print_name');
$per_gua_address	= $this->input->post('per_gua_address');
$per_gua_date 		= $this->input->post('per_gua_date');

for($per_gua=1;$per_gua<=4;$per_gua++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $per_gua;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="per_gua_print_name[<?php echo $per_gua;?>]" class="form-control" placeholder="Enter Print Name" value="<?php echo set_value('per_gua_print_name[$per_gua]',$per_gua_print_name[$per_gua]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="per_gua_address[<?php echo $per_gua;?>]" class="form-control" id="sign-pos" placeholder="Enter Address" value="<?php echo set_value('per_gua_address[$per_gua]',$per_gua_address[$per_gua]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<div class="row">
<div class="col-10 no_pad"><input type="date" name="per_gua_date[<?php echo $per_gua;?>]" class="form-control w-100" placeholder="Select Date" value="<?php echo set_value('per_gua_date[$per_gua]',$per_gua_date[$per_gua]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>
</div>
</div>

<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Witness <span class="fs12">(Executed by Independent Witnesses)</span></p>

<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Address</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>
				
<?php 
$per_wit_print_name = $this->input->post('per_wit_print_name');
$per_wit_address	= $this->input->post('per_wit_address');
$per_wit_date 		= $this->input->post('per_wit_date');

for($per_wit='1';$per_wit<=4;$per_wit++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $per_wit;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="per_wit_print_name[<?php echo $per_wit;?>]" class="form-control" placeholder="Enter Print Name" value="<?php echo set_value('per_wit_print_name[$per_wit]',$per_wit_print_name[$per_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="per_wit_address[<?php echo $per_wit;?>]" class="form-control" placeholder="Enter Address" value="<?php echo set_value('per_wit_address[$per_wit]',$per_wit_address[$per_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<div class="row">
<div class="col-10 no_pad"><input type="date" name="per_wit_date[<?php echo $per_wit;?>]" class="form-control w-100"  placeholder="Select Date" value="<?php echo set_value('per_wit_date[$per_wit]',$per_wit_date[$per_wit]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>

</div>
</div>

</div>

<p class="mt-5 mb-5 bb"></p>

<div>
<div class="float-md-left">
<h1>LIMITED WARRANTY</h1>
<p class="fs18 mt-1">PILBARA POWDER COATINGS</p>
<p class="mt-2">and its Related Body Corporate ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>
</div>

<div class="row mt-3">
<div class="col-md-6 p-2">
<div class="form-group">
<label for="sign-print"><b>I/We, the Customer(s)</b></label>
<input type="text" name="limited_customer_name" class="form-control" placeholder="Same as Applicant in Section 1 of Credit Application Form" value="<?php echo set_value('limited_customer_name');?>">
</div>
</div>
<div class="col-md-6 p-2">
<div class="form-group">
<label for="sign-print"><b>Trading as (if applicable)</b></label>
<input type="text" name="limited_business_name" class="form-control" placeholder="Insert Registered Business Name" value="<?php echo set_value('limited_business_name');?>">
</div>
</div>
<div class="col-md-6 p-2">
<div class="form-group">
<label for="sign-print"><b>Of</b></label>
<input type="text" name="limited_address" class="form-control" placeholder="Insert Street Address (Not PO Box)" value="<?php echo set_value('limited_address');?>">
</div>
</div>
<div class="col-md-3 p-2">
<div class="form-group">
<label for="sign-print"></label>
<input type="text" name="limited_state" class="form-control" placeholder="State" value="<?php echo set_value('limited_state');?>">
</div>
</div>
<div class="col-md-3 p-2">
<div class="form-group">
<label for="sign-print"></label>
<input type="text" name="limited_zip_code" class="form-control" placeholder="Postcode" value="<?php echo set_value('limited_zip_code');?>">
</div>
</div>
</div>
<div class="mt-3 border1 radius-10 p-3"><b class="fs15">Terms and Conditions of this Limited Warranty:</b><br>
<ol>
<li>This Limited Warranty is implied and expressed to the extent of the manufacturer of the powder coating product used. These manufacturers are Axalta, Interpon, Dulux and OxyTech or any other manufacturer whose powder coating products Pilbara Powder Coatings may use.</li>
<li>Manufacturer Warranties specified in Clause 1 above can be found at the following websites:
	<div class="row mt-2">
    <div class="col-md-3 p-1">
    <p>Axlata</p><p><a href="https://www.axalta.com/" target="_blank" class="text-primary">www.axalta.com</a></p>
    </div>
    <div class="col-md-3 p-1">
    <p>Interpon</p><p><a href="http://www.specifyinterpon.com.au/" target="_blank" class="text-primary">www.specifyinterpon.com.au</a></p>
    </div>
    <div class="col-md-3 p-1">
    <p>Dulux</p><p><a href="https://duluxpowders.com.au/" target="_blank" class="text-primary">www.duluxpowders.com.au</a></p>
    </div>
    <div class="col-md-3 p-1">
    <p>OxyTech</p><p><a href="https://www.oxytech.com.au/" target="_blank" class="text-primary">www.oxytech.com.au</a></p>
    </div>
    </div>
</li>
<li>No warranty is implied or expressed by the Supplier other than that of the Warranties of the manufacturers and their powder coating products, specified in Clause 1 above.</li>
<li>Pilbara Powder Coatings denies any expressed or implied liabilities that may arise from a successful Warranty claim. Any successful Warranty claim is strictly limited to the liability of the manufacturer's Warranty.</li>
<li>Where a manufacturer accepts a Warranty claim, at its sole discretion and in limited circumstances, the Supplier may agree to recoat the affected Goods at the Supplier's cost.</li>
<li>When the Supplier agrees to recoat any affected Goods under a manufacturer's Warranty, the recoating is to be done only at the Supplier's premises.</li>
<li>When the Supplier agrees to recoat any affected Goods under a manufacturer's Warranty, in no way whatsoever does this make Supplier liable under the manufacturer's Warranty for any claims that may arise from the Warranty claim.</li>
<li>Any Goods the Supplier agrees to recoat are to be recoated at the Supplier's premises only. The Customer must arrange for the Goods to be delivered to, and pick-up from, the Supplier's premises at the Customer's own expense.</li>
<li>The recoating of any Goods at the Supplier's discretion is limited to the recoating only at the Suppliers premises and excludes:
	<ul class="ordered_cls">
    <li>transport of any Goods to and from the Supplier's premises;</li>
    <li>uninstallation and/or installation of any Goods;</li>
    <li>unfixing and/or fixing of any Goods;</li>
    <li>anything other than the recoating of the Goods at the Supplier's premises.</li>
    </ul>
</li>
<li>The Supplier may refuse to recoat any Goods where:
	<ul class="ordered_cls">
    <li>previously repainted or repaired OEM finishes</li>
    <li>failure due to pre-existing rust or rust originating from the Customer's Goods;</li>
    <li>scratches, abrasions, or stone chips</li>
    <li>damage caused by accidents.</li>
    <li>damage to the paint film caused by improper care, abrasive detergents or waxes, acid rain, industrial emissions or fallout, or heavy-duty pressure washing.</li>
    <li>failure resulting from misuse or abuse, including improper storage.</li>
    <li>failures of systems or components containing products from other manufacturers not specified in Clause 1</li>
    <li>failure of finishes/systems which have been applied outside manufacturers recommendations</li>
    <li>failure of Goods that have not been applied fo their specific use</li>
    <li>there is fair wear and tear, any accident, or act of God.</li>
    </ul>
</li>
<li>This Warranty sets forth the entire limited Warranty by the Supplier. Except as otherwise expressly set forth herein, the Supplier makes no representations or warranties, either expressed or implied, with respect to any Goods and Services provided by the Customer. Without limitation that the Goods and Services will increase productivity, financial performance, efficiency or profitability, the Customer agrees that any implied warranty of merchantability or fitness for a particular use is expressly denied.</li>
<li>In no event shall any Goods representation or Warranties received by the Customer from the Supplier create any more Warranty than that of the manufacturer's Warranty. Where a term in this Limited Warranty conflicts with the manufacturer's Warranty, the manufacturer's Warranty shall prevail with all liabilities and obligations resting with the manufacturer.</li>
<li>Notwithstanding any provision of this Warranty or a manufacturer's Warranty or any incentive document to the contrary, in no event shall the Supplier be liable to the Customer for:
	<ul class="ordered_cls">
    <li>any punitive, exemplary, or other special damages arising under or relating to any Warranty or the subject matter thereof;</li>
    <li>any indirect, incidental, or consequential damages (including, without limitation, loss of use, income, profits or anticipated profits, business or business opportunity, savings or business reputation) arising under or relating to any Warranty agreement or any incentive document or the subject matter hereof, regardless of whether such damages are based in contract, breach of warranty, tort, negligence or any other theory, irrespective of whether such party of, knew of, or should have known of the possibility of such damages</li>
    <li>the remedies set forth herein shall be the sole and exclusive remedy for any breach of a manufacturer's warranty;</li>
    <li>no event shall aggregate the liability to the Customer for each claim arising from a manufacturer's Warranty or subject thereof, regardless of whether such claims are based in contract, breach of warranty, tort, negligence, or any other theory exceeding that specified in the manufacturer's Warranty;</li>
    <li>This section shall survive any termination or expiration of a manufacturer's Warranty.</li>
    </ul>
</li>
<li>This Warranty Agreement shall be governed by, and construed, and enforced according to the laws of the state of Western Australia and the Commonwealth of Australia. Each party consents and submits to the exclusive jurisdiction of and service of process by the Courts of Western Australia.</li>
</ol>
</div>


<p class="mt-4 fs15 weight700">CERTIFICATE OF GUARANTEE - Executed as a Deed</p>
<div class="mt-3 border1 radius-10 p-3"><b class="fs15">Acknowledgment:</b><br>
The Customer acknowledges that this is a Limited Warranty to the extent of the terms set out above. This Warranty Agreement may be executed in counterparts with the same force and effect as if executed in a complete document. Facsimile or electronically transmitted signatures shall have the same force and effect as original signatures.
<br><br>
The Customer acknowledges, understands and agrees to this Limited Warranty. The signatory/ies below hereby confirm they have the authority to sign this agreement and all agreements on behalf of the Customer.</div>
<div class="row">
<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Signatory</p>

<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Address</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>
<?php 
$cirt_gua_print_name 	= $this->input->post('cirt_gua_print_name');
$cirt_gua_address		= $this->input->post('cirt_gua_address');
$cirt_gua_date 			= $this->input->post('cirt_gua_date');

for($cirt_gua=1;$cirt_gua<=4;$cirt_gua++){ ?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $cirt_gua;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="cirt_gua_print_name[<?php echo $cirt_gua;?>]" class="form-control" placeholder="Enter Print Name" value="<?php echo set_value('cirt_gua_print_name[$cirt_gua]',$cirt_gua_print_name[$cirt_gua]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<input type="text" name="cirt_gua_address[<?php echo $cirt_gua;?>]" class="form-control" placeholder="Enter Address" value="<?php echo set_value('cirt_gua_address[$cirt_gua]',$cirt_gua_address[$cirt_gua]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">

<div class="row">
<div class="col-10 no_pad"><input type="date" name="cirt_gua_date[<?php echo $cirt_gua;?>]" class="form-control w-100" placeholder="Select Date" value="<?php echo set_value('cirt_gua_date[$cirt_gua]',$cirt_gua_date[$cirt_gua]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>
</div>
</div>

<div class="col-12 no_pad">
<div class="p-1 border1 radius-10 mt-2">
<p class="weight700 text-uppercase fs15 maroon p-2 text-center">Witness</p>
<div class="row">
				<div class="col-md-4 p-2">
                    <label for="sign-print"><b>Print Name</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-pos"><b>Address</b></label></div>
				<div class="col-md-4 p-2">
                    <label for="sign-date"><b>Date</b></label></div>
				</div>
<?php 
$cirt_wit_print_name 	= $this->input->post('cirt_wit_print_name');
$cirt_wit_address		= $this->input->post('cirt_wit_address');
$cirt_wit_date 			= $this->input->post('cirt_wit_date');

for($cirt_wit=1;$cirt_wit<=4;$cirt_wit++){?>
<div class="col-12 pl-2 pt-2 d-block d-lg-none"><p class="count_no"><?php echo $cirt_wit;?></p></div>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="cirt_wit_print_name[<?php echo $cirt_wit;?>]" class="form-control" placeholder="Enter Print Name" value="<?php echo set_value('cirt_wit_print_name[$cirt_wit]',$cirt_wit_print_name[$cirt_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<input type="text" name="cirt_wit_address[<?php echo $cirt_wit;?>]" class="form-control" placeholder="Enter Address" value="<?php echo set_value('cirt_wit_address[$cirt_wit]',$cirt_wit_address[$cirt_wit]);?>">
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<div class="row">
<div class="col-10 no_pad"><input type="date" name="cirt_wit_date[<?php echo $cirt_wit;?>]" class="form-control w-100" placeholder="Select Date" value="<?php echo set_value('cirt_wit_date[$cirt_wit]',$cirt_wit_date[$cirt_wit]);?>"></div>
<div class="col-2 no_pad text-center"><?php /*?><a href="#"><i class="far fa-calendar-alt fs20 mt-1"></i></a><?php */?></div>
</div>
</div>
</div>
</div>
<?php }?>

</div>
</div>

</div>

<p class="mt-5 mb-5 bb"></p>

<div>
<div class="float-md-left">
<h1>DIRECT DEBIT AUTHORITY</h1>
<p class="fs18 mt-1">PILBARA POWDER COATINGS</p>
<p class="mt-2">and its Related Body Corporate ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>
</div>

<div class="p-3 mt-3 radius-10 border1 bg-light">
<p class="text-center radius-5">Request and Authority to debit the account named below to pay<br><b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b></p>
<hr>
<p class="mt-3 p-2 fs16 weight700 text-uppercase maroon">Request and Authority to debit</p>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Account Number</b></label>
<input type="text" name="dir_account_number" class="form-control" value="<?php echo set_value('dir_account_number');?>" >
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Surname or Company name</b></label>
<input type="text" name="dir_company_name" class="form-control" value="<?php echo set_value('dir_company_name');?>" >
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Given names or ABN/ARBN</b></label>
<input type="text" name="dir_abn" class="form-control" value="<?php echo set_value('dir_abn');?>" >
</div>
</div>
</div>
<div class="p-2">request and authorise <b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b> to arrange, through its own financial institution, a debit to your nominated account for any amount due and payable by you.<br><br>
This debit or charge will be made through the Bulk Electronic Clearing System Framework (BECS) from your account held at the financial institution you have nominated below and will be subject to the terms and conditions of the Direct Debit Request Service Agreement.</div>


<p class="mt-3 p-2 fs16 weight700 text-uppercase maroon">Name and address of financial institution at which account is held</p>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Financial institution name</b></label>
<input type="text" name="dir_institution_name" class="form-control" value="<?php echo set_value('dir_institution_name');?>" >
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Address</b></label>
<input type="text" name="dir_address" class="form-control" value="<?php echo set_value('dir_address');?>" >
</div>
</div>
</div>

<p class="mt-3 p-2 fs16 weight700 text-uppercase maroon">Account to be debited</p>
<div class="row">
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Name/s on account</b></label>
<input type="text" name="dir_account_name" class="form-control" value="<?php echo set_value('dir_account_name');?>" >
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>BSB number (Must be 6 Digits)</b></label>
<input type="text" name="dir_bsb_number" class="form-control" value="<?php echo set_value('dir_bsb_number');?>" >
</div>
</div>
<div class="col-md-4 p-2">
<div class="form-group">
<label for="customer"><b>Account number</b></label>
<input type="text" name="dir_account_dbt_number" class="form-control" value="<?php echo set_value('dir_account_dbt_number');?>" >
</div>
</div>
</div>

<div class="p-2 mt-2">
<p class="weight700 fs15">Acknowledgment</p>
<p class="mt-1">By signing and/or providing us with a valid instruction in respect to your Direct Debit Request, you have understood and agreed to the terms and conditions governing the debit arrangements between you and <b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b>, as set out in this Request and the Credit Application signed and agreed to by you (refer Section 5, Clause 3(c)(i) and 14).</p>

</div>

</div>

<p class="p-1 mt-2">
<input name="" id="Comanda" type="submit" value="Submit" class="btn btn-dark">
<span id="show_hide" style="display:none;"><img src="<?php echo theme_url();?>images/Btn2.png" /></span>
<input type="hidden" name="action" value="post" />
</p>

<p class="mt-3"><b>Email -</b><br> the completed form to <a href="mailto:accounts@pilbarapowdercoating.com.au" class="text-primary">accounts@pilbarapowdercoating.com.au</a></p>
<p class="mt-3"><b>Post -</b><br> the completed form to Pilbara Powder Coating, 2015 Anderson Road, Karratha Industrial Estate WA 6714</p>
<p class="mt-3"><b>In person -</b><br> 2015 Anderson Road, Karratha Industrial Estate WA</p>

</div>

<?php echo form_close();?>
</div>
</div>
</div>
<!-- MIDDLE ENDS -->

<script>
$('#Comanda').click(function() {
	$(this).hide();
	$("#show_hide").show();
});
</script>

<?php $this->load->view("bottom_application");?>