<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
$meta_rec = $this->meta_info;
if( is_array($meta_rec) && !empty($meta_rec) ){
	?>
	<title><?php echo $meta_rec['meta_title'];?></title>
	<meta name="description" content="<?php echo $meta_rec['meta_description'];?>" />
	<meta  name="keywords" content="<?php echo $meta_rec['meta_keyword'];?>" />
	<?php
}?>
<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico"/>
<link href="<?php echo base_url(); ?>assets/developers/css/proj.css" rel="stylesheet" type="text/css" />
<script> var _siteRoot='index.html',_root='index.html';</script>
<script> var site_url = '<?php echo site_url();?>';</script>
<script> var theme_url = '<?php echo theme_url();?>';</script>
<script> var resource_url = '<?php echo resource_url(); ?>'; var gObj = {};</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7cRoboto+Condensed:300%7cPoppins:300,400,500" rel="stylesheet">
<link rel="stylesheet" href="<?php echo theme_url(); ?>css/conditional_sh.css">
<?php 
if($this->uri->rsegment(1)=='home' || $this->uri->rsegment(1)==''){ ?>
<link rel="stylesheet" href="<?php echo theme_url(); ?>css/fluid_dg.css">
<?php
}
?>
<?php
if(($this->uri->rsegment(1)=='product' || $this->uri->rsegment(1)=='products') && $this->uri->rsegment(2)=='detail'){
	?>
    <link rel="stylesheet" href="<?php echo resource_url(); ?>zoom/magiczoomplus.css">
    <?php
}
?>
<script src="<?php echo base_url(); ?>assets/developers/js/common.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<div class="popup">
<h1>Map</h1>
 <div>
<?php echo html_entity_decode($content);?>
<div class="cb"></div></div>
</div>
</body>
</html>