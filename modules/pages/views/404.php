<?php $this->load->view("top_application"); ?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('Page Not Found');?>

<!-- MIDDLE STARTS -->
<div class="container">
<div class="inner_box">
<div class="cms">

<div class="mt-3 text-center"> <img src="<?php echo theme_url();?>images/404.jpg" class="mt-2 mw_96" alt="">    
<p class="mt-3"><a href="<?php echo base_url();?>" class="btn btn-danger">Back to Home Page.</a></p>
</div>

</div>
</div>
</div>
<!-- MIDDLE ENDS -->

<?php $this->load->view("bottom_application");?>