<?php $this->load->view("top_application",$is_header);?>
<body style="padding:0">
<div class="p-3">
<h1>Newsletter</h1>
<?php echo error_message();
 echo form_open('pages/newsletter','role="form"');?>
<p class="mt-2"><input name="subscriber_name" id="subscriber_name" type="text" placeholder="Name *" class="p-2 w-100"value="<?php echo set_value('subscriber_name');?>"><?php echo form_error('subscriber_name');?></p>
<p class="mt-2"><input name="subscriber_email" id="subscriber_email" type="text" placeholder="Email ID *" class="p-2 w-100" value="<?php echo set_value('subscriber_email');?>"><?php echo form_error('subscriber_email');?></p>
<p class="mt-1"><input name="verification_code" id="verification_code" autocomplete="off" type="text" class="p-2" style="width:100px;" placeholder="Enter Code *"> <img src="<?php echo site_url('captcha/normal/newsletters');?>" class="vam" alt="" title="" id="captchaimage"> &nbsp; <a href="javascript:void(0);" title="Change Verification Code"  > &nbsp; <img src="<?php echo theme_url(); ?>images/refresh.png" class="vam" alt="Refresh" title="Refresh" onClick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal/newsletters'); ?>/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code').focus();"></a></p>
<?php echo form_error('verification_code');?>
<p class="mt-2"><input type="submit" id="submit"  class="btn btn-success" value="Subscribe"> 
<input name="subscribe_me" type="hidden"  value="Y" class="">
</p>
<?php echo form_close();?>
</div>

</body>
</html>