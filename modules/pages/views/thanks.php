<?php $this->load->view("top_application"); ?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('Thank You');?>

<div class="container">
<div class="mid_area">
<div class="cms_area">
<div class="w-100 mt-3 mb-5">

<br><br>
<div class="fs14 text-center mt10"><img src="<?php echo theme_url(); ?>images/th.png" class="img-fluid" alt="">
<p class="mt-2 fs16 weight600"><?php echo error_message();?></p>
<p class="mt-2"><a href="<?php echo base_url();?>" class="btn btn-danger btn-md radius-3 ">Go Back</a></p>
</div>
<br>
<br>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>




<?php $this->load->view("bottom_application");?>