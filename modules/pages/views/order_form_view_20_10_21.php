<?php $this->load->view('top_application');?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('ORDER FORM');?>

<!-- MIDDLE STARTS -->
<div class="container">
<div class="inner_box">
<?php echo form_open_multipart('');?>
<div class="cms form_style">
<div class="float-md-left mb-3 mt-3">
<p class="fs20"><?php echo $this->config->item('site_name');?></p>
<p>and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>

<div class="mt-3 mb-4 text-danger border1 radius-10 p-3"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>
<?php 
echo validation_message();
echo error_message();?>
<div class="row">
<div class="col-lg-6 no_pad mt-2 mb-2"><h1>Purchase Order</h1></div>

<div class="col-lg-6 no_pad pl-lg-3">
<div class="row">
<div class="col-md-6 no_pad"><input name="purchase_date" type="date" class="p-2 w-75 radius-5" placeholder="Date" value="<?php echo set_value('purchase_date');?>"> <?php /*?><a href="#"><i class="far fa-calendar-alt fs20 ml-1"></i></a><?php */?></div>
<div class="col-md-6 no_pad"><input name="po_text" type="text" class="p-2 w-100" placeholder="PO" value="<?php echo set_value('po_text');?>"></div>
</div>
</div>
</div>

<div class="row mt-4">
<div class="col-lg-6 no_pad pr-lg-3">
<p class="fs18 weight700 text-uppercase bg-dark white p-2 radius-5">Customer</p>
<div class="row mt-1">
<div class="col-12 p-2">
<div class="form-group">
<label for="customer"><b>Name</b></label>
<input type="text" class="form-control" name="name" value="<?php echo set_value('name');?>">
</div>
</div>
<div class="col-12 p-2">
<div class="form-group">
<label for="customer"><b>Address</b></label>
<input type="text" class="form-control" name="address" value="<?php echo set_value('address');?>">
</div>
</div>
<div class="col-12 p-2">
<div class="form-group">
<label for="customer"><b>Suburb, State Postcode</b></label>
<input type="text" class="form-control" name="state" value="<?php echo set_value('state');?>">
</div>
</div>
<div class="col-12 p-2">
<div class="form-group">
<label for="customer"><b>Phone</b></label>
<input type="text" class="form-control" name="phone" value="<?php echo set_value('phone');?>">
</div>
</div>
<div class="col-12 p-2">
<div class="form-group">
<label for="customer"><b>Email</b></label>
<input type="text" class="form-control" name="email" value="<?php echo set_value('email');?>">
</div>
</div>
</div>
</div>

<div class="col-lg-6 no_pad pl-lg-3">
<p class="fs18 weight700 text-uppercase bg-dark white p-2 radius-5">Ship To</p>
<div class="p-2">
  <textarea name="ship_to" rows="17" class="form-control"><?php echo set_value('ship_to');?></textarea>
</div>
</div>
</div>



<div class="table-responsive mt-5">
<table class="table table-bordered">
<thead>
  <tr class="bg-dark white">
    <th>Contact</th>
    <th>Ship Via</th>
    <th>Delivery</th>
    <th>Shipping Terms</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><input type="text" class="form-control" name="contact_no" value="<?php echo set_value('contact_no');?>"></td>
    <td><input type="text" class="form-control" name="ship_via" value="<?php echo set_value('ship_via');?>"></td>
    <td><input type="text" class="form-control" name="delivery" value="<?php echo set_value('delivery');?>"></td>
    <td><input type="text" class="form-control" name="shipping_term" value="<?php echo set_value('shipping_term');?>"></td>
  </tr>
</tbody>
</table>
</div>
  
  
  
<div class="table-responsive mt-5">
<table class="table table-bordered">
<thead>
  <tr class="bg-dark white">
    <th width="15%">Item</th>
    <th width="40%">Description</th>
    <th width="15%">M2</th>
    <th width="15%">Total (ex. GST)</th>
  </tr>
</thead>
<tbody>
<?php 
$item_name 	= $this->input->post('item_name');
$description	= $this->input->post('description');
$m2 		= $this->input->post('m2');
$item_price = $this->input->post('item_price');

for($i=1;$i<=11;$i++){?>
<tr>
  <td><input type="text" class="form-control" name="item_name[<?php echo $i;?>]" value="<?php echo set_value('item_name[$i]',$item_name[$i]);?>"></td>
  <td><input type="text" class="form-control" name="description[<?php echo $i;?>]" value="<?php echo set_value('description[$i]',$description[$i]);?>"></td>
  <td><input type="text" class="form-control" name="m2[<?php echo $i;?>]" value="<?php echo set_value('m2[$i]',$m2[$i]);?>"></td>
  <td class="bg-light"><input type="text" class="form-control" name="item_price[<?php echo $i;?>]" value="<?php echo set_value('item_price[$i]',$item_price[$i]);?>"></td>
</tr>
<?php }?>

</tbody>
</table>
<table class="table table-bordered">
<tbody>
<tr>
  <td width="70%">&nbsp;</td>
  <td width="30%">
  <table class="table m-0">
  <tbody>
  <tr>
    <td width="50%" class="p-1 border-0"><p class="mt-1 fs15">Subtotal</p></td>
    <td width="50%" class="p-1 border-0"><input type="text" class="form-control" name="sub_total" value="<?php echo set_value('sub_total');?>"></td>
  </tr>
  <tr>
    <td width="50%" class="p-1 border-0"><p class="mt-1 fs15">GST</p></td>
    <td width="50%" class="p-1 border-0"><input type="text" class="form-control" name="gst" value="<?php echo set_value('gst');?>"></td>
  </tr>
  <tr>
    <td width="50%" class="p-1 border-0"><p class="mt-1 fs15">Shipping</p></td>
    <td width="50%" class="p-1 border-0"><input type="text" class="form-control" name="shipping" value="<?php echo set_value('shipping');?>"></td>
  </tr>
  <tr>
    <td width="50%" class="p-1 border-0"><p class="mt-1 fs15">Other</p></td>
    <td width="50%" class="p-1 border-0"><input type="text" class="form-control" name="other" value="<?php echo set_value('other');?>"></td>
  </tr>
  <tr>
    <td width="50%" class="p-1 border-0"><p class="mt-1 fs15 weight700">Total</p></td>
    <td width="50%" class="p-1 border-0"><input type="text" class="form-control" name="total_price" value="<?php echo set_value('total_price');?>"></td>
  </tr>
</tbody>
</table>
  </td>
  <!--<td width="15%" class="text-right"><p class="mt-1 fs16">Subtotal</p></td>
  <td width="15%"><input type="text" class="form-control"></td>-->
</tr>
</tbody>
</table>

 <p>(Please use the drawing sheet below for detail works)</p>
  <p class="mt-2 weight700">Comments or Special Instructions</p>
  <div class="mt-1">
    <textarea name="comment" rows="6" class="form-control"><?php echo set_value('comment');?></textarea>
  </div>  
  
</div>


<div class="mt-5">
<div class="float-md-left mb-3 mt-3">
<p class="fs20">PILBARA POWDER COATINGS</p>
<p>and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
</div>
<div class="float-md-right"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></div>
<p class="clearfix"></p>

<div class="mt-3 mb-4 text-danger border1 radius-10 p-3"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>

<p class="mt-4 weight700 fs20 text-uppercase">Drawing Sheet</p>
<div class="mt-2 p-3" style="border:#333 5px solid; min-height:600px;"></div>
</div>
<div class="mt-2"><input type="submit" value="Submit" class="btn btn-primary">
<input type="hidden" name="action" value="post" />
</div>

</div>
<?php echo form_close();?>
</div>
</div>
<!-- MIDDLE ENDS -->


<?php $this->load->view("bottom_application");?>