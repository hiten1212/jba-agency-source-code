<?php $this->load->view('top_application');
echo navigation_breadcrumb("Contact Us");
$Res = get_db_single_row('tbl_admin','*'," AND admin_id='1' AND status='1'");
//trace($Res);
?>
<div class="contact_section">
<div class="container">
<div class="mid_area">

<div class="contact_outer">
<h1>Domestic Customers</h1>
<div class="contact_details">
<h2>Contact Us</h2>
<?php /*?><p class="address_txt "><b>Pick up the phone and call us at</b></p><?php */?>

<div class="address_dtls">

<p class="add_dtl">
<b><i class="fas fa-map-marker-alt"></i></b>
<span><strong>Address :</strong><?php echo $Res['address']; ?></span></p>

<p class="add_dtl">

<b><i class="fas fa-phone-alt"></i></b> 
<span><strong>TEL..</strong>
<a href="tel:<?php echo $Res['phone']; ?>"><?php echo $Res['phone']; ?></a> 
<?php /*?><a href="tel:+2652200 ">+966-11-265 2200</a><?php */?>
</span></p>

<p class="add_dtl">
<b><i class="fas fa-envelope"></i></b> 
<span>
<strong>Email</strong>
<a href="mailto:<?php echo $Res['admin_email']; ?>"><?php echo $Res['admin_email']; ?></a>
<?php /*?><a href="mailto:sales@carpets.com">sales@carpets.com</a><?php */?>
</span></p>
</div>
<div class="clearfix"></div>

</div>

<a id="cid"></a>
<div class="contact_rgt_cont">
<h2 class="fs20">Get In Touch</h2>
<?php 
	echo validation_message(); 
	echo form_open_multipart('pages/domestic#cid');?>    
<fieldset class="contact_form mt15" style="border:none;">
<div class="mt-2">
<input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name');?>" placeholder="Your Name *">
</div>


<div class="mt-2">
<input type="text" name="email" id="email" placeholder="Email ID *" value="<?php echo set_value('email');?>">
</div>

<div class="mt-2">
<input type="text" name="mobile_number" id="mobile_number" placeholder="Mobile Number *" value="<?php echo set_value('mobile_number');?>">
</div>


<div class="mt-2">
<textarea name="message" id="message" class="large" cols="45" rows="3" placeholder="Requirement *"><?php echo set_value('message');?></textarea>
</div>
<div>
<input name="verification_code" id="verification_code" autocomplete="off" type="text" placeholder="Enter Code *" class="vam" style="width:120px;">
<img src="<?php echo site_url('captcha/normal/contact/contact');?>" class="vam" alt="" id="captchaimage1"  /> <a href="javascript:void(0);" title="Change Verification Code">
<img src="<?php echo theme_url();?>images/refresh.png"  alt="Refresh"  onclick="document.getElementById('captchaimage1').src='<?php echo site_url('captcha/normal/contact/contact');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="vam"></a>
</div>
<div class="mt-2">
<input name="input" type="submit" value="Submit" class="btn btn-danger" >
<input name="reset" type="reset" value="Reset" class="btn btn-default">
</div>
</fieldset>
 <?php echo form_close();?>  
</div>

</div>

<div class="p15 map_w">

<?php echo html_entity_decode($Res['domestic_google_map']); ?>

</div>

</div>
</div>

</div>

<?php $this->load->view("bottom_application");?>