<?php $this->load->view('top_application');
echo navigation_breadcrumb("Career");?>
<?php $Rescarrer = get_db_single_row('wl_cms_pages','*'," AND friendly_url='career' AND status='1'"); ?>  
<div class="inner_pages">

<div class="container">
<div class="mid_area">
<a id="cid"></a>
<div class="cms_area">
<a href="#" class="btn_back" onClick="history.back(-1)"><i class="fas fa-long-arrow-alt-left"></i> Back</a>
<h1>Career</h1>
<a id="cid"></a>
<div class="row" id="a1">

<div class="col-lg-12 no_pad">
<?php 
	//echo validation_message(); 
	echo form_open_multipart('pages/career#cid'); ?>  
<div class="contact_form_cont">
    <p class="fs16 mb-2"><b>Just Fill the Below Information:</b></p>    
    <p class="mt-2 large"><input type="text" name="first_name" placeholder="First Name *" value="<?php echo set_value('first_name');?>"><?php echo form_error('first_name');?> 
	<input type="text" name="last_name" placeholder="Last Name" value="<?php echo set_value('last_name');?>"><?php echo form_error('last_name');?>   
	<input type="text" name="email" placeholder="E-mail ID *" value="<?php echo set_value('email');?>"><?php echo form_error('email');?> 
	<input type="text" name="phone_number" placeholder="Mobile No. *" value="<?php echo set_value('phone_number');?>"><?php echo form_error('phone_number');?></p>
    <p class="mt-2"><textarea name="message" rows="3" placeholder="Message  *"><?php echo set_value('message');?></textarea><?php echo form_error('message');?></p>
    <p class="text-left">Attach Resume<input type="file" name="cv_file"><?php echo form_error('cv_file');?>   </p>
    <p class="mt-2"><input type="text" name="verification_code" id="verification_code" autocomplete="off" placeholder="Enter Code  *" style="width:130px;"><img src="<?php echo site_url('captcha/normal/contact');?>" class="" alt="" id="captchaimage1"  /> <a href="javascript:void(0);" title="Change Verification Code">
<img src="<?php echo theme_url();?>images/ref.png"  alt="Refresh"  onclick="document.getElementById('captchaimage1').src='<?php echo site_url('captcha/normal/contact');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class=""></a>  
<?php echo form_error('verification_code');?></p>
    <p class="fs12 mt-1">Type the characters shown above.</p>    
    <p class="mt-1 pt-2"><input name="submit" type="submit" value="Upload Resume" class="btn btn-blue mr-1"></p>
</div>
<?php echo form_close();?>
</div>
</div>
    </div>
</div>
</div>
</div>
<div class="clearfix"></div>

<?php $this->load->view("bottom_application");?>