<?php $this->load->view("top_application",$is_header);?>
<?php echo error_message();
 echo form_open('','role="form"');$current_url=base64_decode($this->input->get('current_ul'));
 if($this->input->post("current_url"))
 $current_url=$this->input->post("current_url");?>
 <input type="hidden" name="current_url" value="<?php echo set_value('current_url',$current_url);?>"> 
<div class="p-3 pt-1 pb-1">
<h1 class="black fs20 mb-2">Refer a Friend</h1>
<div><input name="your_name" type="text" placeholder="Your Name *" class="form-control" value="<?php echo set_value('your_name');?>" maxlength="100"><?php echo form_error('your_name');?></div>
<div class="mt-1"><input name="your_email"  type="text" placeholder="Your Email ID *" class="form-control" value="<?php echo set_value('your_email');?>" maxlength="100"><?php echo form_error('your_email');?></div>
<div class="mt-1"><input name="friend_name" type="text" placeholder="Your Friend's Name *" class="form-control" value="<?php echo set_value('friend_name');?>" maxlength="100"><?php echo form_error('friend_name');?></div>
<div class="mt-1"><input name="friend_email" type="text" placeholder="Your Friend's Email ID *" class="form-control" value="<?php echo set_value('friend_email');?>" maxlength="100"><?php echo form_error('friend_email');?></div>
<div class="mt-1"><input name="verification_code" id="verification_code" autocomplete="off" type="text" placeholder="Enter Code *" class="form-control float-left" style="width:125px;">
<img src="<?php echo site_url('captcha/normal');?>" class="float-left mt-2" alt="" title="" id="captchaimage" > &nbsp; <a href="javascript:void(0);" title="Change Verification Code"  ><img src="<?php echo theme_url(); ?>images/ref.png" class="float-left mt-2" alt="Refresh" title="Refresh" onClick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal'); ?>/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code').focus();"></a></div>
<p class="clearfix"></p>
<?php echo form_error('verification_code');?>
<div class="mt-1"><input name="submit" type="submit" value="Send" class="btn_yel"></div>
</div>
<?php echo form_close();?>
</body>
</html>