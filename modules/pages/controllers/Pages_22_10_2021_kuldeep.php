<?php
class Pages extends Public_Controller{

	public function __construct() {

		parent::__construct();
		$this->load->helper(array('file','category/category'));
		$this->load->library(array('Dmailer'));
		$this->load->model(array('pages/pages_model'));
		$this->form_validation->set_error_delimiters("<div style='color:red; font-size:12px !important;'>","</div>");
	}

	public function index(){		

		$friendly_url = $this->uri->rsegments[3];
		$condition       = array('friendly_url'=>$friendly_url,'status'=>'1');
		$content         =  $this->pages_model->get_cms_page( $condition );
		$data['content'] = $content;
		$this->load->view('pages/cms_page_view',$data);
	}	

	
	public function career()
	{
		$this->form_validation->set_rules('first_name','First Name','trim|alpha|required|max_length[50]');
		$this->form_validation->set_rules('last_name','Last Name','trim|alpha|max_length[50]');	
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('phone_number','Mobile Number','trim|numeric|required|max_length[20]');
		//$this->form_validation->set_rules('apply_for','Apply For','trim|required|max_length[20]');
		//$this->form_validation->set_rules('state','State','trim|required|max_length[20]');
		//$this->form_validation->set_rules('country','Country','trim|required|max_length[20]');
		$this->form_validation->set_rules('message','Message','trim|required|max_length[8500]');
		$this->form_validation->set_rules('cv_file','Attach Your CV',"file_required|file_allowed_type[document]");
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[contact]');
		$data['page_error'] = "";

		if($this->form_validation->run()==TRUE)
		{
			    $uploaded_file = "";
				$filepath='';	
				
			    if( !empty($_FILES) && $_FILES['cv_file']['name']!='' )
				{			  
					$this->load->library('upload');	
						
					$uploaded_data =  $this->upload->my_upload('cv_file','cv_files');
				
					if( is_array($uploaded_data)  && !empty($uploaded_data) )
					{ 								
						$uploaded_file = $uploaded_data['upload_data']['file_name'];
						$filepath = UPLOAD_DIR.'/cv_files/'.$uploaded_file;
					
					}		
					
				}
				
			$posted_data=array(
			'type'          =>'2',  
			'service_type'   =>'career',
			'first_name'    => $this->input->post('first_name',TRUE),
			'last_name'     => $this->input->post('last_name',TRUE),
			'email'         => $this->input->post('email',TRUE),
			'phone_number'  => $this->input->post('phone_number',TRUE),
			'mobile_number'  => $this->input->post('mobile_number',TRUE),
			'cv_file'        => $uploaded_file,			
			'apply_for'		=> $this->input->post('apply_for',TRUE),
			'message'       => $this->input->post('message',TRUE),
			'receive_date'     =>$this->config->item('config.date.time')
			);
			
			
			$posted_data = $this->security->xss_clean($posted_data);
			$this->pages_model->safe_insert('wl_enquiry',$posted_data,FALSE);
				
				
				
			  /* Send  mail to user */						
			  $content    =  get_content('wl_auto_respond_mails','9');		
				$subject    =  str_replace('{site_name}',$this->config->item('-'),$content->email_subject.' '.$this->config->item('site_name'));
				$body       =  $content->email_content;	
				
				$name = $this->input->post('first_name');
									
				$body			=	str_replace('{mem_name}',$name,$body);
				$body			=	str_replace('{email}',$this->input->post('email'),$body);
				$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
				$body			=	str_replace('{country}',$this->input->post('country'),$body);
				$body			=	str_replace('{state}',$this->input->post('state'),$body);
				$body			=	str_replace('{city}',$this->input->post('city'),$body);				
				
				$body			=	str_replace('{applying_for}',$this->input->post('apply_for'),$body);
				$body			=	str_replace('{comments}',$this->input->post('message'),$body);
				$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
				$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
				//$body			=	str_replace('{link}',$verify_url,$body);
				
				$mail_conf =  array(
				'subject'=>$subject,
				'to_email'=>$this->input->post('email'),
				'from_email'=>$this->admin_info->admin_email,
				'from_name'=> $this->config->item('site_name'),
				'body_part'=>$body
				);	
				
				if($filepath){
					$mail_conf['attachment']=$filepath;
				}
		       // echo '<pre>';	print_r($mail_conf); exit();
						  
			    $this->dmailer->mail_notify($mail_conf);
			  
			  /* End send  mail to user */

			  /* Send  mail to admin */
					  
			  $body       =  $content->email_content;	
									
			
				$name = 'Admin';
									
				$body			=	str_replace('{mem_name}',$name,$body);
				$body			=	str_replace('{email}',$this->input->post('email'),$body);
				$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
				$body			=	str_replace('{country}',$this->input->post('country'),$body);
				$body			=	str_replace('{state}',$this->input->post('state'),$body);
				$body			=	str_replace('{city}',$this->input->post('city'),$body);
				
				$body			=	str_replace('{applying_for}',$this->input->post('apply_for'),$body);
				$body			=	str_replace('{comments}',$this->input->post('message'),$body);
				$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
				$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
				//$body			=	str_replace('{link}',$verify_url,$body);
				
				$mail_conf =  array(
				'subject'=>$subject,
				'to_email'=>$this->admin_info->admin_email,
				'from_email'=>$this->admin_info->admin_email,
				'from_name'=> $this->config->item('site_name'),
				'body_part'=>$body
				);	
									
				if($filepath){
					$mail_conf['attachment']=$filepath;
				}
									
				$this->dmailer->mail_notify($mail_conf);
				
				/* End send  mail to admin */

			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_flashdata('success', 'Your CV has been sent successfully.We will get you back very shortly.');
			redirect('pages/thanks', '');

		}else{

			$posted_data_str=$this->input->post('action');
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}			
		}	
		
		$this->load->view('pages/career');	
		
	}

	public function downloads(){		

		$data['heading_title'] = "Download";
		$this->load->view('pages/downloads_page_view',$data);
	}
	
	public function donate(){

		$this->form_validation->set_rules('first_name','Name','trim|alpha|required|max_length[30]');
		$this->form_validation->set_rules('last_name','Last Name','trim|alpha|max_length[30]');
		$this->form_validation->set_rules('amount','Amount','trim|numeric|required|max_length[10]');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		$this->form_validation->set_rules('mobile_number','Mobile Number','trim|required|numeric|min_length[5]|max_length[25]');
		
		$this->form_validation->set_rules('message','Message','trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[donate]');
		$data['page_error'] = "";

		if($this->form_validation->run()==TRUE){

			$posted_data=array(
			'type'          => '4',
			'first_name'    => $this->input->post('first_name'),
			'last_name'     => $this->input->post('last_name'),
			'email'         => $this->input->post('email'),
			'amount'         => $this->input->post('amount'),
			'phone_number'  => $this->input->post('phone_number'),
			'mobile_number'  => $this->input->post('mobile_number'),
			'company_name'  => '',
			'country'		=> '',
			'message'       => $this->input->post('message'),
			'receive_date'     =>$this->config->item('config.date.time')
			);
			$posted_data = $this->security->xss_clean($posted_data);
			$this->pages_model->safe_insert('wl_enquiry',$posted_data,FALSE);
			
			/* Send  mail to user */
			$content    =  get_content('wl_auto_respond_mails','4');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$name = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			$body			=	str_replace('{amount}',$this->input->post('amount'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);
			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to user */
			/* Send  mail to admin */
			$content    =  get_content('wl_auto_respond_mails','4');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$mname = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
			$body			=	str_replace('{mem_name}',$mname,$body);
			$body			=	str_replace('{email}',$this->input->post('email'),$body);
			$body			=	str_replace('{amount}',$this->input->post('amount'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number'),$body);
			$body			=	str_replace('{comments}',$this->input->post('message'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);
			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to admin */
			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_userdata(array('msg_type_convs_request'=>'success_contect'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully. We will get back to you soon.');
			redirect('pages/thanks', '');
		}else{
			$posted_data_str=$this->input->post('action');
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}
		}
		//$friendly_url = $this->uri->segment(2)==''?$this->uri->segment(1):$this->uri->segment(2);
		$condition1       = array('friendly_url'=>'address1','status'=>'1');
		$content1         =  $this->pages_model->get_cms_page( $condition1 );
		$data['content1'] = $content1['page_description'];
		
		$condition2       = array('friendly_url'=>'address2','status'=>'1');
		$content2         =  $this->pages_model->get_cms_page( $condition2 );
		$data['content2'] = $content2['page_description'];
		
		$condition3       = array('friendly_url'=>'address3','status'=>'1');
		$content3         =  $this->pages_model->get_cms_page( $condition3 );
		$data['content3'] = $content3['page_description'];
		
		$data['page_heading'] = "Contact Us";
		$this->load->view('pages/donate_view',$data);
	}
	
	public function case_studies()
	{
		$data['page_heading'] = "Case Studies";
		$this->load->view('pages/case_studies',$data);
	}
	
	public function order_form()
	{
		$data['page_heading'] = "Order Form";
		
		if($this->input->post('action')=='post')
		{
			$item_name 		= array_filter($this->input->post('item_name'));			
			$description	= $this->input->post('description');
			$m2 		= $this->input->post('m2');
			$item_price = $this->input->post('item_price');

			//echo count($item_name);
			$this->form_validation->set_rules('name','Name','trim|alpha|required|max_length[30]');			
			$this->form_validation->set_rules('address','Address','trim|required|max_length[255]');			
			$this->form_validation->set_rules('phone','Phone Number','trim|required|min_length[5]|max_length[25]');
			$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');			
			$this->form_validation->set_rules('comment','Comment','trim|max_length[8500]');
			for($k=1;$k<=count($item_name);$k++)
			{
				$this->form_validation->set_rules('item_name[]','Enter Item Name','max_length[100]');
				$this->form_validation->set_rules('description[]','Enter Item Name Description','max_length[100]');
				$this->form_validation->set_rules('m2[]','Enter Item Name m2','max_length[100]');
				$this->form_validation->set_rules('item_price[]','Enter Item Name Price','max_length[100]');
			}
			//$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[donate]');
			if($this->form_validation->run()==TRUE)
			{			
				$purchase_date = $this->input->post('purchase_date');
				$po_text = $this->input->post('po_text');
				$name = $this->input->post('name');
				$address = $this->input->post('address');
				$state = $this->input->post('state');
				$phone = $this->input->post('phone');
				$email = $this->input->post('email');
				$ship_to = $this->input->post('ship_to');
				$contact_no = $this->input->post('contact_no');
				$ship_via = $this->input->post('ship_via');
				$delivery = $this->input->post('delivery');
				$shipping_term = $this->input->post('shipping_term');
				$gst = $this->input->post('gst');
				$shipping = $this->input->post('shipping');
				$other = $this->input->post('other');
				$total_price = $this->input->post('total_price');
				$sub_total = $this->input->post('sub_total');
				$comment = $this->input->post('comment');
				
				$invoice_number    = get_auto_increment('wl_order');
				
				$item_name 		= array_filter($this->input->post('item_name'));
				$description 	= array_filter($this->input->post('description'));
				$m2 			= array_filter($this->input->post('m2'));
				$item_price 	= array_filter($this->input->post('item_price'));
				
				$posted_data=array(
				'invoice_number'=>$invoice_number,
				'purchase_date' => $purchase_date,
				'po_text'    	=> $po_text,
				'first_name'    => $name,
				'address'    	=> $address,
				'state'    		=> $state,
				'phone'    		=> $phone,
				'email'    		=> $email,
				'ship_to'    	=> $ship_to,
				'contact_no'    => $contact_no,
				'ship_via'    	=> $ship_via,
				'delivery'    	=> $delivery,
				'shipping_term' => $shipping_term,
				'gst'    		=> $gst,
				'shipping'    	=> $shipping,
				'other'    		=> $other,
				'sub_total'   	=> $sub_total,
				'total_price'   => $total_price,
				'comment'    	=> $comment,
				'receive_date'  => $this->config->item('config.date.time')
				);
	
				$posted_data = $this->security->xss_clean($posted_data);			
				
				
				//trace($item_name);			
				//trace($posted_data);exit;
				
				//trace($this->input->post());
				$insert_id = $this->pages_model->safe_insert('wl_order',$posted_data,FALSE);
				
				//$orderId='2';
				if($insert_id>0)
				{
					//trace($item_name);
					for($m=1;$m<=count($item_name);$m++){
						$product_data=array(
						'orders_id'		=>$insert_id,
						'item_name' 	=> $item_name[$m],
						'description'   => $description[$m],
						'm2'    		=> $m2[$m],
						'item_price'    => $item_price[$m],
						);
						
						$product_data = $this->security->xss_clean($product_data);						
						$this->pages_model->safe_insert('wl_orders_products',$product_data,FALSE);
					}
					
					$this->load->helper('pages/form_html');
					$this->load->library('M_pdf');				
										
					$form_res = $this->db->query("SELECT * FROM wl_order 
												  WHERE status='1' AND order_id='".$insert_id."'")->row_array();					
					ob_start();		
					$var=order_form_inline_html_view($form_res);
					
					$file_name = "Order".$insert_id.'.pdf';
					
					$this->db->query("UPDATE wl_order SET order_pdf_file='".$file_name."' 
									  WHERE order_id='".$insert_id."'");
			
					$html= ob_get_contents();
					ob_clean();
					$pdfFilePath = "uploaded_files/order_pdf/".$file_name;
					$this->m_pdf->pdf->WriteHTML($html); 
					$this->m_pdf->pdf->Output($pdfFilePath, "F"); //D For Download , F= Save file in folder
					
					
					// Send mail 
					$attach_path = FCPATH.$pdfFilePath;
					//$attach_path = array(FCPATH.'test_mail.txt',FCPATH.'test_mail.pdf');
					//$attach_path = array($_FILES['attach1']['name'],$_FILES['attach2']['name'],$_FILES['attach3']['name']);
					
					$subject = "Order Form";
					$body="Hello Admin";
					$mail_conf =  array(
					'subject'=>$subject,
					'to_email'=>'orders@pilbarapowdercoating.com.au',//$this->admin_info->admin_email,
					'from_email'=>$this->admin_info->admin_email,
					'from_name'=> $this->config->item('site_name'),
					'attachment'=>$attach_path,
					'body_part'=>$body,
					'libtype'=>'sendgrid',
					'debug'=>false
					);
			
					$this->dmailer->mail_notify($mail_conf);
					//end send mail
															
				}
				
				$this->session->set_flashdata('msg', "Your Order form submitted successfully.");
				redirect('pages/thanks', '');
				exit;
			}
		}
		
		$this->load->view('pages/order_form_view',$data);
	}
	
	
	public function test_mail()
	{
		$file_name = "Order10.pdf";
		$pdfFilePath = "uploaded_files/order_pdf/".$file_name;		
		
		// Send mail 
		$attach_path = FCPATH.$pdfFilePath;
		//$attach_path = array(FCPATH.'test_mail.txt',FCPATH.'test_mail.pdf');
		//$attach_path = array($_FILES['attach1']['name'],$_FILES['attach2']['name'],$_FILES['attach3']['name']);
		
		$subject = "Order Form";
		$body="Hello Admin";
		$mail_conf =  array(
		'subject'=>$subject,
		'to_email'=>'weblink.manish84@gmail.com',//$this->admin_info->admin_email,
		'from_email'=>$this->admin_info->admin_email,
		'from_name'=> $this->config->item('site_name'),
		'attachment'=>$attach_path,
		'body_part'=>$body,
		'libtype'=>'sendgrid',
		'debug'=>false
		);

		$this->dmailer->mail_notify($mail_conf);
		//end send mail
	}
	
	public function order_form_html()
	{
		$insert_id = 8;
		$data['title'] = "inline_html_view";
		$form_res = $this->db->query("SELECT * FROM wl_order 
									  WHERE status='1' AND order_id='".$insert_id."'")->row_array();
		$data['res'] = $form_res;
		$this->load->view('pages/order_form_inline_html_view',$data);
	}
	
	public function genrate_order_form_pdf()
	{
		$this->load->helper('pages/form_html');
		$this->load->library('M_pdf');
		
		$insert_id = 8;					
		$form_res = $this->db->query("SELECT * FROM wl_order 
									  WHERE status='1' AND order_id='".$insert_id."'")->row_array();
		
		ob_start();		
		$var=order_form_inline_html_view($form_res);
		
		$file_name = "Order".$insert_id.'.pdf';
		
		$this->db->query("UPDATE wl_order SET order_pdf_file='".$file_name."' 
						  WHERE order_id='".$insert_id."'");

		$html= ob_get_contents();
		ob_clean();
		$pdfFilePath = "uploaded_files/order_pdf/".$file_name;
		$this->m_pdf->pdf->WriteHTML($html); 
		$this->m_pdf->pdf->Output($pdfFilePath, "F"); //D For Download , F= Save file in folder
		
		$this->session->set_userdata(array('msg_type'=>'success'));				
		$this->session->set_flashdata('success', 'Thanks for create Order Form.');
		redirect('pages/thanks', '');
	}
	
	
	public function credit_application_form()
	{
		$data['page_heading'] = "Order Form";
		if($this->input->post('action')=='post')
		{
			$this->form_validation->set_rules('name','Name','trim|alpha|required|max_length[100]');			
			$this->form_validation->set_rules('abn','ABN','trim|required|max_length[100]');
			if($this->form_validation->run()==TRUE)
			{
				$legal_entity 			= @implode(',',$this->input->post('legal_entity'));
				$credit_application_for = @implode(',',$this->input->post('credit_application_for'));
				
				$posted_data=array(
				'name'					=>$this->input->post('name'),
				'abn' 					=> $this->input->post('abn'),
				'trading_name'    		=> $this->input->post('trading_name'),
				'legal_entity'			=> $legal_entity,
				'address'    			=> $this->input->post('address'),
				'address_post_code' 	=> $this->input->post('address_post_code'),
				'postal_address'    	=> $this->input->post('postal_address'),
				'postal_address_code'	=>$this->input->post('postal_address_code'),
				'office_phone'			=>$this->input->post('office_phone'),
				'fax_number' 			=> $this->input->post('fax_number'),
				'email'    				=> $this->input->post('email'),
				'ah_phone'    			=> $this->input->post('ah_phone'),
				'nature_business'    	=> $this->input->post('nature_business'),
				'year_business'    		=> $this->input->post('year_business'),
				'account_contact'    	=>$this->input->post('account_contact'),
				'account_phone'			=>$this->input->post('account_phone'),
				'account_email' 		=> $this->input->post('account_email'),
				'account_mobile'    	=> $this->input->post('account_mobile'),
				'purchase_contact'    	=> $this->input->post('purchase_contact'),
				'mobile_number'    		=> $this->input->post('mobile_number'),
				'monthly_credit_req'    => $this->input->post('monthly_credit_req'),
				'todays_date'    		=>$this->input->post('todays_date'),
				'finc_turn_over_last_year'=>$this->input->post('finc_turn_over_last_year'),
				'finc_paid_up_capital' 	=> $this->input->post('finc_paid_up_capital'),
				'finc_accountants'    	=> $this->input->post('finc_accountants'),
				'finc_contact_number'   => $this->input->post('finc_contact_number'),
				'finc_mobile_number'    => $this->input->post('finc_mobile_number'),
				'finc_bank_name'    	=> $this->input->post('finc_bank_name'),
				'finc_account_name'    	=>$this->input->post('finc_account_name'),
				'finc_bsb'				=>$this->input->post('finc_bsb'),
				'finc_account_number' 	=> $this->input->post('finc_account_number'),
				'finc_bank_manager_name'=> $this->input->post('finc_bank_manager_name'),
				'credit_application_for'=> $credit_application_for,
				'guarantor_name'    	=> $this->input->post('guarantor_name'),
				'supplier_to_supply'   	=> $this->input->post('supplier_to_supply'),
				'trade_applicable'    	=> $this->input->post('trade_applicable'),
				'guarantor_address'    	=>$this->input->post('guarantor_address'),
				'guarantor_state'		=>$this->input->post('guarantor_state'),
				'guarantor_zip_code' 	=> $this->input->post('guarantor_zip_code'),
				'limited_customer_name' => $this->input->post('limited_customer_name'),
				'limited_business_name' => $this->input->post('limited_business_name'),
				'limited_address'    	=> $this->input->post('limited_address'),
				'limited_state'    		=> $this->input->post('limited_state'),
				'limited_zip_code'    	=>$this->input->post('limited_zip_code'),
				'dir_account_number'	=>$this->input->post('dir_account_number'),
				'dir_company_name' 		=> $this->input->post('dir_company_name'),
				'dir_abn'    			=> $this->input->post('dir_abn'),
				'dir_institution_name'  => $this->input->post('dir_institution_name'),
				'dir_address'    		=> $this->input->post('dir_address'),
				'dir_account_name'    	=> $this->input->post('dir_account_name'),
				'dir_bsb_number'    	=>$this->input->post('dir_bsb_number'),
				'dir_account_dbt_number'=>$this->input->post('dir_account_dbt_number'),
				'status'				=>'1',
				'receive_date'  		=> $this->config->item('config.date.time')
				);
	
				$posted_data = $this->security->xss_clean($posted_data);
				//trace($this->input->post());
				//trace($posted_data);exit;				
				$insert_id = $this->pages_model->safe_insert('wl_application_form',$posted_data,FALSE);
				
				if($insert_id>0)
				{
					$trade_reference_company= array_filter($this->input->post('trade_reference_company'));
					$soft_trade_name 		= array_filter($this->input->post('soft_trade_name'));
					$ack_sig_print_name 	= array_filter($this->input->post('ack_sig_print_name'));
					$ack_wit_print_name 	= array_filter($this->input->post('ack_wit_print_name'));
					$per_gua_print_name 	= array_filter($this->input->post('per_gua_print_name'));
					$per_wit_print_name 	= array_filter($this->input->post('per_wit_print_name'));
					$cirt_gua_print_name 	= array_filter($this->input->post('cirt_gua_print_name'));
					$cirt_wit_print_name 	= array_filter($this->input->post('cirt_wit_print_name'));
					
					
					//trade_reference_data
					if(is_array($trade_reference_company) && !empty($trade_reference_company))
					{						
						$trade_contact	= $this->input->post('trade_contact');
						$trade_phone 	= $this->input->post('trade_phone');
						$trade_email 	= $this->input->post('trade_email');

						for($m=1;$m<=@count($trade_reference_company);$m++)
						{
							$trade_reference_data=array(
							'app_id'					=> $insert_id,
							'trade_reference_company' 	=> $trade_reference_company[$m],
							'trade_contact'   			=> $trade_contact[$m],
							'trade_phone'    			=> $trade_phone[$m],
							'trade_email'    			=> $trade_email[$m],
							);
							
							$trade_reference_data = $this->security->xss_clean($trade_reference_data);							
							$this->pages_model->safe_insert('wl_app_trade_reference',$trade_reference_data,FALSE);
						}
					}
					//end trade_reference_data
					
					//soft_trade_data
					if(is_array($soft_trade_name) && !empty($soft_trade_name))
					{						
						//$soft_trade_name		= $this->input->post('soft_trade_name');
						$soft_trade_position 	= $this->input->post('soft_trade_position');
						$soft_trade_address 	= $this->input->post('soft_trade_address');
						$soft_trade_mobile 		= $this->input->post('soft_trade_mobile');

						for($sf=1;$sf<=@count($soft_trade_name);$sf++)
						{
							$soft_trade_data=array(
							'app_id'					=> $insert_id,
							'soft_trade_name' 			=> $soft_trade_name[$sf],
							'soft_trade_position'   	=> $soft_trade_position[$sf],
							'soft_trade_address'    	=> $soft_trade_address[$sf],
							'soft_trade_mobile'    		=> $soft_trade_mobile[$sf],
							);
							
							$soft_trade_data = $this->security->xss_clean($soft_trade_data);							
							$this->pages_model->safe_insert('wl_app_soft_trade',$soft_trade_data,FALSE);
						}
					}
					//end soft_trade_data
					
					//ack_sig_data
					if(is_array($ack_sig_print_name) && !empty($ack_sig_print_name))
					{						
						//$ack_sig_print_name	= $this->input->post('ack_sig_print_name');
						$ack_sig_position 	= $this->input->post('ack_sig_position');
						$ack_sig_date 		= $this->input->post('ack_sig_date');						

						for($ack=1;$ack<=@count($ack_sig_print_name);$ack++)
						{
							$ack_sig_data=array(
							'app_id'			=> $insert_id,
							'ack_sig_print_name'=> $ack_sig_print_name[$ack],
							'ack_sig_position'  => $ack_sig_position[$ack],
							'ack_sig_date'    	=> $ack_sig_date[$ack],							
							);
							
							$ack_sig_data = $this->security->xss_clean($ack_sig_data);							
							$this->pages_model->safe_insert('wl_app_ack_signatory',$ack_sig_data,FALSE);
						}
					}
					//end ack_sig_data
					
					
					//ack_wit_data
					if(is_array($ack_wit_print_name) && !empty($ack_wit_print_name))
					{						
						//$ack_wit_print_name	= $this->input->post('ack_wit_print_name');
						$ack_wit_position 	= $this->input->post('ack_wit_position');
						$ack_wit_date 		= $this->input->post('ack_wit_date');						

						for($wit=1;$wit<=@count($ack_wit_print_name);$wit++)
						{
							$ack_wit_data=array(
							'app_id'			=> $insert_id,
							'ack_wit_print_name'=> $ack_wit_print_name[$wit],
							'ack_wit_position'  => $ack_wit_position[$wit],
							'ack_wit_date'    	=> $ack_wit_date[$wit],							
							);
							
							$ack_wit_data = $this->security->xss_clean($ack_wit_data);							
							$this->pages_model->safe_insert('wl_app_ack_witness',$ack_wit_data,FALSE);
						}
					}
					//end ack_wit_data
					
					
					//start per_gua_data
					if(is_array($per_gua_print_name) && !empty($per_gua_print_name))
					{						
						//$per_gua_print_name	= $this->input->post('per_gua_print_name');
						$per_gua_address 	= $this->input->post('per_gua_address');
						$per_gua_date 		= $this->input->post('per_gua_date');						

						for($per=1;$per<=@count($per_gua_print_name);$per++)
						{
							$per_gua_data=array(
							'app_id'			=> $insert_id,
							'per_gua_print_name'=> $per_gua_print_name[$per],
							'per_gua_address'  => $per_gua_address[$per],
							'per_gua_date'    	=> $per_gua_date[$per],
							);
							
							$per_gua_data = $this->security->xss_clean($per_gua_data);							
							$this->pages_model->safe_insert('wl_app_cert_guarantor',$per_gua_data,FALSE);
						}
					}
					//end per_gua_data
					
					
					//start per_wit_data
					if(is_array($per_wit_print_name) && !empty($per_wit_print_name))
					{						
						//$per_wit_print_name	= $this->input->post('per_wit_print_name');
						$per_wit_address 	= $this->input->post('per_wit_address');
						$per_wit_date 		= $this->input->post('per_wit_date');						

						for($per_wit=1;$per_wit<=@count($per_wit_print_name);$per_wit++)
						{
							$per_wit_data=array(
							'app_id'			=> $insert_id,
							'per_wit_print_name'=> $per_wit_print_name[$per_wit],
							'per_wit_address'   => $per_wit_address[$per_wit],
							'per_wit_date'    	=> $per_wit_date[$per_wit],
							);
							
							$per_wit_data = $this->security->xss_clean($per_wit_data);							
							$this->pages_model->safe_insert('wl_app_cert_witness',$per_wit_data,FALSE);
						}
					}
					//end per_wit_data
					
					
					//start cirt_gua_data
					if(is_array($cirt_gua_print_name) && !empty($cirt_gua_print_name))
					{						
						//$cirt_gua_print_name	= $this->input->post('cirt_gua_print_name');
						$cirt_gua_address 	= $this->input->post('cirt_gua_address');
						$cirt_gua_date 		= $this->input->post('cirt_gua_date');						

						for($cirt_gua=1;$cirt_gua<=@count($cirt_gua_print_name);$cirt_gua++)
						{
							$cirt_gua_data=array(
							'app_id'				=> $insert_id,
							'cirt_gua_print_name'	=> $cirt_gua_print_name[$cirt_gua],
							'cirt_gua_address'   	=> $cirt_gua_address[$cirt_gua],
							'cirt_gua_date'    		=> $cirt_gua_date[$cirt_gua],
							);
							
							$cirt_gua_data = $this->security->xss_clean($cirt_gua_data);							
							$this->pages_model->safe_insert('wl_app_cirt_gua_signatory',$cirt_gua_data,FALSE);
						}
					}
					//end cirt_gua_data
					
					//start cirt_gua_data
					if(is_array($cirt_wit_print_name) && !empty($cirt_wit_print_name))
					{						
						//$cirt_wit_print_name	= $this->input->post('cirt_wit_print_name');
						$cirt_wit_address 	= $this->input->post('cirt_wit_address');
						$cirt_wit_date 		= $this->input->post('cirt_wit_date');						

						for($cirt_wit=1;$cirt_wit<=@count($cirt_wit_print_name);$cirt_wit++)
						{
							$cirt_wit_data=array(
							'app_id'				=> $insert_id,
							'cirt_wit_print_name'	=> $cirt_wit_print_name[$cirt_wit],
							'cirt_wit_address'   	=> $cirt_wit_address[$cirt_wit],
							'cirt_wit_date'    		=> $cirt_wit_date[$cirt_wit],
							);
							
							$cirt_wit_data = $this->security->xss_clean($cirt_wit_data);							
							$this->pages_model->safe_insert('wl_app_cirt_gua_witness ',$cirt_wit_data,FALSE);
						}
					}
					//end cirt_gua_data
					
					
					//generate pdf file
					$this->load->helper('pages/form_html');
					$this->load->library('M_pdf');
					
					$form_res = $this->db->query("SELECT * FROM wl_application_form 
									  WHERE status='1' AND application_id='".$insert_id."'")->row_array();
					
					ob_start();		
					$var=credit_application_inline_html_view($form_res);
					
					$file_name = "Application".$insert_id.'.pdf';
					
					$this->db->query("UPDATE wl_application_form SET application_pdf_file='".$file_name."' 
									  WHERE application_id='".$insert_id."'");
		
					$html= ob_get_contents();
					ob_clean();
					$pdfFilePath = "uploaded_files/application_pdf/".$file_name;
					$this->m_pdf->pdf->WriteHTML($html); 
					$this->m_pdf->pdf->Output($pdfFilePath, "F"); //D For Download , F= Save file in folder
					//end generate pdf file
					
					
					// Send mail 
					$attach_path = FCPATH.$pdfFilePath;
					//$attach_path = array(FCPATH.'test_mail.txt',FCPATH.'test_mail.pdf');
					//$attach_path = array($_FILES['attach1']['name'],$_FILES['attach2']['name'],$_FILES['attach3']['name']);
					
					$subject = "Create Application Form";
					$body="Hello Admin";
					$mail_conf =  array(
					'subject'=>$subject,
					'to_email'=>'accounts@pilbarapowdercoating.com.au',//$this->admin_info->admin_email,
					'from_email'=>$this->admin_info->admin_email,
					'from_name'=> $this->config->item('site_name'),
					'attachment'=>$attach_path,
					'body_part'=>$body,
					'libtype'=>'sendgrid',
					'debug'=>false
					);
			
					$this->dmailer->mail_notify($mail_conf);
					//end send mail
					
				}
				
				//trace($this->input->post());exit;
				$this->session->set_userdata(array('msg_type'=>'success'));				
				$this->session->set_flashdata('success', 'Thanks for create application Form.');
				redirect('pages/thanks', '');				
			}				
		}
		$this->load->view('pages/credit_application_form_view',$data);
	}
	
	
	public function create_application_html()
	{
		$insert_id = 4;
		$data['title'] = "inline_html_view";
		$form_res = $this->db->query("SELECT * FROM wl_application_form 
									  WHERE status='1' AND application_id='".$insert_id."'")->row_array();
		$data['res'] = $form_res;
		$this->load->view('pages/credit_application_inline_html_view',$data);
	}	
	
	public function genrate_application_pdf()
	{
		$this->load->helper('pages/form_html');
		$this->load->library('M_pdf');
		
		$insert_id = 11;					
		$form_res = $this->db->query("SELECT * FROM wl_application_form 
						  WHERE status='1' AND application_id='".$insert_id."'")->row_array();
		
		ob_start();		
		$var=credit_application_inline_html_view($form_res);
		
		$file_name = "Application".$insert_id.'.pdf';
		
		$this->db->query("UPDATE wl_application_form SET application_pdf_file='".$file_name."' 
						  WHERE application_id='".$insert_id."'");

		$html= ob_get_contents();
		ob_clean();
		$pdfFilePath = "uploaded_files/application_pdf/".$file_name;
		$this->m_pdf->pdf->WriteHTML($html); 
		$this->m_pdf->pdf->Output($pdfFilePath, "F"); //D For Download , F= Save file in folder
		
		$this->session->set_userdata(array('msg_type'=>'success'));				
		$this->session->set_flashdata('success', 'Thanks for create application Form.');
		redirect('pages/thanks', '');
	}

	public function contactus()
	{
		$this->form_validation->set_rules('first_name','First Name','trim|alpha|required|max_length[30]');
		$this->form_validation->set_rules('last_name','Last Name','trim|alpha|max_length[30]');
		$this->form_validation->set_rules('phone_number','Phone','trim|max_length[15]');
		$this->form_validation->set_rules('mobile_number','Mobile Number','trim|required|is_numeric|max_length[15]');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[80]');
		//$this->form_validation->set_rules('country','Country','trim');
		$this->form_validation->set_rules('message','Message','trim|required|max_length[8500]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code');
		$data['page_error'] = "";

		if($this->form_validation->run()==TRUE)
		{

			$posted_data=array(
			'first_name'    => $this->input->post('first_name',TRUE),
			'last_name'     => $this->input->post('last_name',TRUE),
			'email'         => $this->input->post('email',TRUE),
			'phone_number'  => $this->input->post('phone_number',TRUE),
			'mobile_number'  => $this->input->post('mobile_number',TRUE),
			'company_name'  => '',
			'country'		=> '',
			'message'       => $this->input->post('message',TRUE),
			'receive_date'     =>$this->config->item('config.date.time')
			);

			$posted_data = $this->security->xss_clean($posted_data);
			$this->pages_model->safe_insert('wl_enquiry',$posted_data,FALSE);

			/* Send  mail to user */

			$content    =  get_content('wl_auto_respond_mails','5');
			$subject    =  str_replace('{site_name}',$this->config->item('site_name'),$content->email_subject);
			$body       =  $content->email_content;

			$verify_url = "<a href=".base_url().">Click here </a>";

			$name = $sender_name = ucwords($this->input->post('first_name',TRUE).' '.$this->input->post('last_name',TRUE));

			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{sender_name}',$name,$body);
			$body			=	str_replace('{email}',$this->input->post('email',TRUE),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number',TRUE),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number',TRUE),$body);
			$body			=	str_replace('{comments}',$this->input->post('message',TRUE),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email',TRUE),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);

			$this->dmailer->mail_notify($mail_conf);

			/* End send  mail to user */

			/* Send  mail to admin */

			$body       =  $content->email_content;

			$verify_url = "<a href=".base_url().">Click here </a>";

			$name = 'Admin';

			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{sender_name}',$sender_name,$body);
			$body			=	str_replace('{email}',$this->input->post('email',TRUE),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number',TRUE),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number',TRUE),$body);
			$body			=	str_replace('{comments}',$this->input->post('message',TRUE),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);

			$this->dmailer->mail_notify($mail_conf);

			/* End send  mail to admin */

			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_userdata(array('msg_type_convs_request'=>'success_contect'));
			$this->session->set_flashdata('success', 'Your enquiry has been submitted successfully. We will get back to you soon.');
			redirect('pages/contactus', '');

		}else{
			$posted_data_str=$this->input->post('action',TRUE);
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}
		}
		$friendly_url = $this->uri->segment(2)==''?$this->uri->segment(1):$this->uri->segment(2);
		$condition       = array('friendly_url'=>$friendly_url,'status'=>'1');
		$content         =  $this->pages_model->get_cms_page( $condition );
		$data['content'] = $content['page_description'];
		$data['page_heading'] = "Contact Us";

		$this->load->view('pages/contactus',$data);

	}
		
	public function is_subscribed($data){

		$this->db->select('subscriber_id');
		$this->db->from('wl_newsletters');
		$this->db->where($data);
		$this->db->where('status !=', '2');
		$query = $this->db->get();
		if ($query->num_rows() == 1){
			return TRUE;
		}else{
			return FALSE;
		}
	}	
	

	public function sitemap(){

		$data['title'] = "Sitemap";
		$this->load->view('sitemap',$data);
	}
	
	public function newsletter(){

		$data['default_email_text']= "Email Id";
		$this->form_validation->set_rules('subscriber_name','Name','trim|required|alpha|max_length[225]');
		$this->form_validation->set_rules('subscriber_email','Email','trim|required|valid_email|max_length[255]');
		$this->form_validation->set_rules('subscribe_me','Status','trim|required');
		$this->form_validation->set_rules('verification_code','Verification Code','trim|required|valid_captcha_code[newsletters]');
		if($this->form_validation->run()==TRUE){
			
			$res = $this->pages_model->add_newsletter_member();
			$this->session->set_userdata('msg_type',$res['error_type']);
			$this->session->set_flashdata($res['error_type'],$res['error_msg']);
			redirect('pages/newsletter', '');
		}
		$data['is_header'] = FALSE;
		$this->load->view('view_subscribe_newsletter',$data);
	}

	private function subscribe_newsletter($posted_data){

		$query = $this->db->query("SELECT subscriber_email,status FROM  wl_newsletters WHERE subscriber_email='$posted_data[subscriber_email]'");
		$subscribe_me  = $posted_data['subscribe_me'];
		if( $query->num_rows() > 0 ){
			
			$row = $query->row_array();
			if( $row['status']=='0' && ($subscribe_me=='Y') ){
				
				$where = "subscriber_email = '".$row['subscriber_email']."'";
				$this->pages_model->safe_update('wl_newsletters',array('status'=>'1'),$where,FALSE);
				$msg =  $this->config->item('newsletter_subscribed');
				return $msg;
			}else if($row['status']=='0' && ($subscribe_me=='N')){
				
				$msg =  $this->config->item('newsletter_not_subscribe');
				return $msg;
			}else if($row['status']=='1' && ($subscribe_me=='Y')){
				$msg =  $this->config->item('newsletter_already_subscribed');
				return $msg;
			}else if($row['status']=='1' && ($subscribe_me=='N')){
				$where = "subscriber_email = '".$row['subscriber_email']."'";
				$this->pages_model->safe_update('wl_newsletters',array('status'=>'0'),$where,FALSE);
				$msg =  $this->config->item('newsletter_unsubscribed');
			  return $msg;
		  }
	  }else{
		  if($subscribe_me=='N' ){
			  $msg =  $this->config->item('newsletter_not_subscribe');
			  return $msg;
		  }else{
			  $data =  array('status'=>'1', 'subscriber_name'=>$posted_data['subscriber_name'], 'subscriber_email'=>$posted_data['subscriber_email']);
			  $data = $this->security->xss_clean($data);
			  $this->pages_model->safe_insert('wl_newsletters',$data);
			  $msg =  $this->config->item('newsletter_subscribed');
			  return $msg;
			}

		}

	}

	public function join_newsletter(){

		$subscriber_name        =  $this->session->userdata("user_id")>0?"Member":"Guest";
		$subscriber_name        = $this->input->post('subscriber_name',TRUE);
		$subscriber_email       = $this->input->post('subscriber_email',TRUE);
		$subscribe_me           = $this->input->post('subscribe_me',TRUE);
		$this->form_validation->set_error_delimiters("<div style='color:#ff0000; font-weight:bold;'>","</div>");

		//$this->form_validation->set_rules('subscriber_name', 'Name', "trim|required|alpha");
		$this->form_validation->set_rules('subscriber_email', 'Email ID', "trim|required|valid_email");
		//$this->form_validation->set_rules('terms_conditions', 'Terms & Conditions', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code[homenewslwtter]');

		if ($this->form_validation->run() == TRUE){

			$posted_data = array( 'subscriber_name'=>$subscriber_name,'subscriber_email'=>$subscriber_email,'subscribe_me'=>$subscribe_me);
			$posted_data = $this->security->xss_clean($posted_data);
			$result      =  $this->subscribe_newsletter($posted_data);
			if( $result ){
				echo '<div class="" style="color:#ff0000; font-weight:bold;">'.$result.'</div>';
			}
		}else{
			header('Content-type: text/json');
			echo json_encode(array('error'=>validation_errors()));
			//echo '<div style="color:#FF0000"><font size="-1">'.validation_errors().'</font></div>';
		}
	}
	public function refer_to_friends(){

		$productId        = (int) $this->uri->segment(3);
		$data['heading_title'] = "Refer to a Friend";
		$this->form_validation->set_rules('your_name','Name','trim|required|alpha|max_length[100]');
		$this->form_validation->set_rules('your_email','Email','trim|required|valid_email|max_length[100]');
		$this->form_validation->set_rules('friend_name','Friend\'s Name','trim|required|alpha|max_length[100]');
		$this->form_validation->set_rules('friend_email','Friend\'s Email','trim|required|valid_email|max_length[100]');
		$this->form_validation->set_rules('verification_code','Verification code','trim|required|valid_captcha_code');
		if($this->form_validation->run()==TRUE){

			$your_name     = $this->input->post('your_name',TRUE);
			$your_email    =  $this->input->post('your_email',TRUE);
			$friend_name   = $this->input->post('friend_name',TRUE);
			$friend_email  = $this->input->post('friend_email',TRUE);
			$conditions   = "your_email ='$your_email' AND friend_email ='$friend_email' ";
			//$count_result = $this->pages_model->findCount('wl_invite_friends',$conditions);

			if( !$count_result ){

				$posted_data =  array('your_name'=>$your_name,
				'your_email'=>$your_email,
				'friend_name'=>$friend_name,
				'friend_email'=>$friend_email,
				'receive_date'=>$this->config->item('config.date.time')
				);
				$posted_data = $this->security->xss_clean($posted_data);
				//$this->pages_model->safe_insert('wl_invite_friends',$posted_data);
			}
			$content    =  get_content('wl_auto_respond_mails','3');
			$body       =  $content->email_content;
			if($productId > 0 ){

				$product_link_url = get_db_field_value('wl_products','friendly_url'," AND products_id='$productId'");
				//$product_link_url =  base_url()."products/detail/$productId";
				$link_url = base_url().$product_link_url;
				$link_url= "<a href=".$link_url.">Click here </a>";
				$text ="Product";
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',$this->config->item('product_referred_success'));
			}else{
				$link_url = base_url();
				$link_url= "<a href=".$link_url.">Click here </a>";
				$text ="Site";
				$this->session->set_userdata(array('msg_type'=>'success'));
				$this->session->set_flashdata('success',$this->config->item('site_referred_success'));

			}
			$body			=	str_replace('{friend_name}',$friend_name,$body);
			$body			=	str_replace('{your_name}',$your_name,$body);
			$body			=	str_replace('{site_name}',$this->config->item('site_name'),$body);
			$body			=	str_replace('{text}',$text,$body);
			$body			=	str_replace('{site_link}',$link_url,$body);
			$mail_conf =  array(
			'subject'=>"Invitation from ".$your_name." to see",
			'to_email'=>$friend_email,
			'from_email'=>$your_email,
			'from_name'=>$your_name,
			'body_part'=>$body
			);
			
			$this->dmailer->mail_notify($mail_conf);
			redirect('pages/refer_to_friends', '');
			$this->load->view('pages/view_refer_to_friend',$data);
		}
		$data['is_header'] = FALSE;
		$this->load->view('pages/view_refer_to_friend',$data);
	}

	public function unsubscribe(){

		$subscribe_id=$this->uri->segment(3);
		$this->pages_model->safe_update('wl_newsletters',array('status'=>'0'),array("md5(subscriber_id)"=>$subscribe_id),FALSE);
		$msg =  $this->config->item('newsletter_unsubscribed');
		$this->session->set_userdata(array('msg_type'=>'success'));
		$this->session->set_flashdata('success',$msg);
		redirect("pages/thanks");

	}

	public function thanks(){

		$this->load->view('thanks')	;
	}

	public function advanced_search(){

		//$priceOpts = array("0-100"=>"0-100","101-500"=>"101-500","501-1000"=>"501-1000",">1000"=>">1000");

		$data['heading_title'] = "Advanced Search";
		//$data['priceRange']   = $priceOpts;
		$this->load->view('pages/advanced_search_view',$data);
	}

	public function _404(){
		$this->load->view("404");
	}

	public function error_404(){

		$this->load->view("404");
	}

	public function download($filename=""){

		if($filename){
			$data=file_get_contents(UPLOAD_DIR."/$filename");
			$this->load->helper("download");
			force_download($filename,$data);
		}else{
			redirect("");
		}
	}

	public function valid_mobile($field){

		//$mobile = $this->input->post('mobile_number');
		if($field!=''){
		$mobile = $field;
		$this->form_validation->set_message('valid_mobile', 'The %s field may only contain number,+,-.');
		return ( ! preg_match("/^([0-9\-\+])+$/i", $mobile)) ? FALSE : TRUE;
		}
	} 

}







/* End of file pages.php */