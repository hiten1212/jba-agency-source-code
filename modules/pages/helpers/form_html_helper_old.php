<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('CI')){
	function CI()
	{
		if (!function_exists('get_instance')) return FALSE;
		$CI =& get_instance();
		return $CI;
	}
}

function credit_application_inline_html_view($res) 
{	
	$ci=CI();
	$site_details=get_db_single_row('tbl_admin','*',' and  admin_id =1');	
	?>
	<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body style="font-size:14px; color:#333; margin:15px auto; padding:5px; font-family:Arial, Helvetica, sans-serif; min-width:1230px;width:1230px;line-height:22px;">
<div style="background:#fff;padding:20px;border-radius:10px;margin-bottom:20px;box-shadow:0 0 10px #ccc;">
  <div style="float:left;">
    <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">Credit Application Form</h1>
    <p style="font-size:18px;margin:10px 0 0 0;padding:0;">PILBARA POWDER COATINGS</p>
    <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
  </div>
  <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
  <p style="margin:0;padding:0;clear:both;"></p>
  <div style="border:1px solid #ddd;padding:1rem!important;padding:0;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> If you do not understand this document, you should seek independent legal advice. Please do not use correction fluid or tape as this is a legal document. Any corrections should be initialled.</div>
  <p style="margin:0;padding:0;clear:both;"></p>
  <div>
    <p style="margin:1.5rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">APPLICANT DETAILS</p>
    <div style="margin:15px 0 0 0;">
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Customer Name</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['name'];?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="abn"><b>ABN</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="abn" value="<?php echo $res['abn'];?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="trading"><b>Trading Name</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="trading" value="<?php echo $res['trading_name'];?>">
        </div>
      </div>
      <div>
	  <?php 
$legal_entity_arr = array(
'1'=>'Sole Trader',
'2'=>'Partnership',
'3'=>'Pty Ltd',
'4'=>'Trust Co.',
'5'=>'Public Co.',
);

$legal_entity_exp = @explode(',',$res['legal_entity']);
?>
<p style="margin:0;padding:0;clear:both;"></p>

        <p style="padding:.25rem!important;"><b>Legal Entity</b></p>
        <div>
		<?php foreach($legal_entity_arr as $key_leg=>$val_leg){?>
          <div style="padding:.25rem!important;width:16%;float:left">
            <div>
              <input style="float:left;margin:5px 5px 0 0" type="checkbox" id="defaultCheck1" <?php echo (@in_array($val_leg,$legal_entity_exp)) ? 'checked="checked"' : '';?>>
              <label style="display:block" for="defaultCheck1"><?php echo $val_leg;?></label>
            </div>
          </div>
		  <?php }?> 
        </div>
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="display:flex;-ms-flex-wrap:wrap;">
        <div style="padding:.25rem!important;-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%;display:inline-flex;">
          <div style="-webkit-box-flex:0;-ms-flex:0 0 60.666667%;flex:0 0 60.666667%;max-width:60.666667%;">
            <label style="display:block" for="street"><b>Street Address</b></label>
            <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="street" value="<?php echo $res['address'];?>">
          </div>
          <div style="-webkit-box-flex:0;-ms-flex:0 0 29.333333%;flex:0 0 29.333333%;max-width:29.333333%;padding:.25rem !important;margin:0 0 0 15px">
            <div>
              <label style="display:block;margin:0 0 18px 0"></label>
              <input type="email" style="display:block;width:95%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="Post Code" value="<?php echo $res['address_post_code'];?>">
            </div>
          </div>
        </div>
        <div style="padding:.25rem!important;-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%;display:inline-flex;">
          <div style="-webkit-box-flex:0;-ms-flex:0 0 60.666667%;flex:0 0 60.666667%;max-width:60.666667%;">
            <label style="display:block" for="street"><b>Postal Address</b></label>
            <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="street" value="<?php echo $res['postal_address'];?>">
          </div>
          <div style="-webkit-box-flex:0;-ms-flex:0 0 29.333333%;flex:0 0 29.333333%;max-width:29.333333%;padding:.25rem !important;margin:0 0 0 15px">
            <div>
              <label style="display:block;margin:0 0 18px 0"></label>
              <input type="email" style="display:block;width:95%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="Post Code" value="<?php echo $res['postal_address_code'];?>">
            </div>
          </div>
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="telephone"><b>Office Telephone</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="telephone" value="<?php echo $res['office_phone'];?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="fax"><b>Fax Number</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="fax" value="<?php echo $res['fax_number']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="email"><b>Email Address</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="email" value="<?php echo $res['email']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="ah"><b>A.H. Telephone</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="ah" value="<?php echo $res['ah_phone']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="nature"><b>Nature of Business</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="nature" value="<?php echo $res['nature_business']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="years"><b>Years in Business</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="years" value="<?php echo $res['year_business']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="accounts"><b>Accounts Contact</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="accounts" value="<?php echo $res['account_contact']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="tel"><b>Acc. Telephone</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="tel" value="<?php echo $res['account_phone']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="accmail"><b>Accounts Email</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="accmail" value="<?php echo $res['account_email']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="accmobile"><b>Acc. Mobile</b></label>

          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="accmobile" value="<?php echo $res['account_mobile']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="purchasing"><b>Purchasing Contact</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="purchasing" value="<?php echo $res['purchase_contact']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="mob"><b>Mobile No.</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="mob" value="<?php echo $res['mobile_number']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="credit"><b>Monthly Credit Req.</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="credit" value="<?php echo $res['monthly_credit_req']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="date"><b>Today's Date</b></label>
          <input type="email" style="display:block;width:64%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="date" value="<?php echo $res['todays_date']?>">
        </div>
      </div>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Trade References</p>
	

    <div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="reference"><b>Reference Company</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradeContact"><b>Contact</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradephone"><b>Phone</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradeemail"><b>Email</b></label></div>
		
    <p style="margin:0;padding:0;clear:both;"></p>
		
    </div>
<?php 
$get_trade_reference_data = $ci->db->query("SELECT * FROM wl_app_trade_reference WHERE app_id='".$res['application_id']."'")->result_array();
for($t=0;$t<3;$t++){?>
    <div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="reference3" value="<?php echo @$get_trade_reference_data[$t]['trade_reference_company'];?>" >
        </div>

      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="tradeContact3" value="<?php echo @$get_trade_reference_data[$t]['trade_contact'];?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="tradephone3" value="<?php echo @$get_trade_reference_data[$t]['trade_phone'];?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="tradeemail3"  value="<?php echo @$get_trade_reference_data[$t]['trade_email'];?>">
        </div>
      </div>
    </div>
	<?php }?>
	
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Sole Trader, Partner, Director, Trustee, Public Co. Officer Details</p>
	
	<div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="reference"><b>Name</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradeContact"><b>Position</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradephone"><b>Home Address</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="tradeemail"><b>Mobile No.</b></label></div>
		
    <p style="margin:0;padding:0;clear:both;"></p>
		
    </div>
	<?php 
	$get_soft_trade_data = $ci->db->query("SELECT * FROM wl_app_soft_trade WHERE app_id='".$res['application_id']."'")->result_array();
	for($sf=0;$sf<4;$sf++){?>
    
    <div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="dtl-name4" value="<?php echo @$get_soft_trade_data[$sf]['soft_trade_name'];?>">
        </div>
      </div>
      <div style="width:24.3%;float:left;padding:.25rem !important">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="dtl-pos4" value="<?php echo @$get_soft_trade_data[$sf]['soft_trade_position'];?>">
        </div>
      </div>
      <div style="width:24.3%;float:left;padding:.25rem !important">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="dtl-addr4" value="<?php echo @$get_soft_trade_data[$sf]['soft_trade_address'];?>">
        </div>
      </div>
      <div style="width:24.3%;float:left;padding:.25rem !important">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="dtl-mob4" value="<?php echo @$get_soft_trade_data[$sf]['soft_trade_mobile'];?>">
        </div>
      </div>
    </div>
	<?php }?>
	
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">FINANCIAL INFORMATION</p>
    <div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="turnover"><b>Turnover last financial year ($)</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="Capital"><b>Paid up Capital ($)</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="Accountants"><b>Accountants</b></label></div>
		<div style="padding:.25rem!important;float:left;width:24.3%;">
          <label style="display:block" for="finContact"><b>Contact No.</b></label></div><p style="margin:0;padding:0;clear:both;"></p>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="turnover" placeholder="" value="<?php echo $res['finc_turn_over_last_year']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="Capital" placeholder="" value="<?php echo $res['finc_paid_up_capital']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="Accountants" placeholder="" value="<?php echo $res['finc_accountants']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="finContact" placeholder="" value="<?php echo $res['finc_contact_number']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="fintel"><b>Telephone/Mobile</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="fintel" placeholder="" value="<?php echo $res['finc_mobile_number']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="bankname"><b>Bank Name</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="bankname" placeholder="" value="<?php echo $res['finc_bank_name']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="bankaccount"><b>Account Name</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="bankaccount" placeholder="" value="<?php echo $res['finc_account_name']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="finbsb"><b>BSB</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="finbsb" placeholder="" value="<?php echo $res['finc_bsb']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="finaccno"><b>Account No.</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="finaccno" placeholder="" value="<?php echo $res['finc_account_number']?>">
        </div>
      </div>
      <div style="padding:.25rem!important;float:left;width:24.3%;">
        <div>
          <label style="display:block" for="bankmanager"><b>Bank Manager's Name</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="bankmanager" placeholder="" value="<?php echo $res['finc_bank_manager_name']?>">
        </div>
      </div>
      
	  <?php 
$credit_application_arr = array(
'1'=>'Credit Application',
'2'=>'Guarantee',
'3'=>'Limited Warranty',
'4'=>'Direct Debit Authority',
);
	  $credit_application_for_exp = @explode(',',$res['credit_application_for']);
	  ?>
      <p style="margin:0;padding:0;clear:both;"></p>
      <div>
        <p style="margin:0;padding:0;"><strong>Credit Application For</strong></p>
        <div>
		<?php foreach($credit_application_arr as $key_cr=>$val_cr){?>
          <div style="padding:.25rem!important;width:24.3%;float:left">
            <div>
              <input style="float:left;margin:5px 5px 0 0" type="checkbox" value="" id="defaultCheck1" <?php echo (@in_array($val_cr,$credit_application_for_exp)) ? 'checked="checked"' : '';?>>
              <label style="display:block" for="defaultCheck1"><?php echo $val_cr;?></label>
            </div>
          </div>
		  <?php }?>          
        </div>
      </div>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">CREDIT ACCOUNT TERMS AND CONDITIONS</p>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
      <p style="text-transform:uppercase;font-weight:600;margin:0;padding:0">1. Definitions and interpretation</p>
      <p style="margin:0;padding:0;">Company means Reefwind Pty Ltd ABN 21 069 120 341, trading as Pilbara Powder Coatings and any Related Bodies Corporate.<br>
        Conditions mean these Credit Account Terms and Conditions.<br>
        Consequential loss means loss of expected savings, loss of use, loss of opportunity, loss of profit, loss of revenue, increased financing costs, loss arising from delay, or any consequential, special or indirect loss or damage, whether or not the possibility or potential extent of the loss or damage was known or foreseeable, and whether arising from a claim under an indemnity, contract, tort (including negligence), statute or otherwise.<br>
        Customer means the party making this credit application.<br>
        Goods mean any item, goods, product, or thing supplied by the Supplier in their ordinary course of business.<br>
        Insolvency Event means, for the Customer, as applicable, being unable to pay a bill on time, being in liquidation or provisional liquidation, bankruptcy or under administration, having a controller (as defined in the Corporations Act 2001) or analogous person appointed to the Customer or any of the Customer's property, being taken under section 459F(1) of the Corporations Act to have failed to comply with a statutory demand, being unable to pay the Customer's debts, dying, ceasing to be of full legal capacity or otherwise becoming incapable of managing the Customer's own affairs for any reason, taking any step that could result in the Company becoming an insolvent under administration (as defined in section 9 of the Corporations Act 2001), entering into a compromise or arrangement with, or assignment for the benefit of, any of the Customer's members or creditors, or any similar event.<br>
        <b>Loss</b> means any expense, cost, or damage of any kind and includes consequential loss and a fine or penalty imposed by a statutory or other authority.<br>
        <b>Real property</b> means all real property interests held by the Customer now or in the future.<br>
        <b>Other property</b> means any other securities, investments, bonds, cash, chattels, goods, monies due and payable, and any other form of property besides Real Property, whether tangible or intangible.<br>
        <b>Related Bodies Corporate</b> has the same meaning as in the Corporations Act 2001.<br>
        <b>Services</b> mean any service or services provided by the Company and/or Supplier in their ordinary course of business.<br>
        <b>Supplier</b> means that in connection with the supply of any Goods or Services, the Company and or Related Bodies Corporate makes the supply to Customer.<br>
        <b>Supplier T&amp;C</b> means any terms and conditions of quotation and supply, in any way described, notified by the Supplier to the Customer (whether on an invoice, quote, through the Supplier's website, docket, or otherwise), or supply agreement (however described) in the Supplier's standard form entered into in writing between the Supplier and Customer.<br>
        <b>Singular words</b> include the plural and vice versa. A mention of anything after include, includes or including, does not limit what else might be included.</p>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">2. Structure of this document</p>
      <ol>
        <li style="margin:0 0 10px 0">The terms and conditions set out in this Section 5 and any other Supplier Ts&amp;Cs that may be entered into are intended to regulate key trading terms, including the extension of credit in connection with the supply of Goods or Services by the Supplier to the Customer.</li>
        <li style="margin:0 0 10px 0">The terms and conditions set out in this Section 5, and any other Supplier Ts&amp;Cs are incorporated by reference as part of the Supplier Ts&amp;Cs and are legally binding between the Supplier and the Customer. To the extent of any inconsistency between the terms and conditions in this Section 5 and any other Supplier Ts&amp;Cs that may appear on the Supplier invoices, quotes, dockets, website, or anywhere else the terms and conditions in Section 5 of this Credit Account Terms &amp; Conditions shall prevail.</li>
      </ol>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">3. Customer obligations</p>
      <ol>
        <li style="margin:0 0 10px 0">These Conditions apply if the Supplier accepts any order for Goods or Services or both from the Customer, whether for cash or credit. The Customer has no entitlement to credit unless the Supplier's sole discretion extends credit to the Customer. If the Supplier gives the Customer credit, the Supplier still reserves the right at any time and for any reason in the Supplier's sole discretion to refuse to supply any other Goods or Services to the Customer on credit terms. If the Supplier does decline to give the Customer further credit, then that decision does not affect the credit terms which apply to any amounts the Customer then owes to the Supplier.</li>
        <li style="margin:0 0 10px 0">Where the Customer fails to comply with any of the terms of these Conditions or in respect of any obligation to pay money to Supplier when due, suffers an insolvency event or makes any misrepresentation to the Supplier, they can pay their account on time. In that case, the balance of the Customer's account to the Supplier will become due and payable immediately.</li>
        <li style="margin:0 0 10px 0">The Customer agrees:
          <ul>
            <li style="margin:0 0 10px 0">Payment is to be made in full without any deduction or setoff of the total amount due and payable by the Customer, on delivery of the Goods and Services provided, or, if credit terms are offered, <span>by the 15th or 30th day (28th day for February) of the month of invoice by automatic direct debit.</span></li>
            <li style="margin:0 0 10px 0">Pay any stamp duty assessed on this document or fee to register or maintain any security interest held by the Supplier regarding Goods or Services supplied to the Customer.</li>
            <li style="margin:0 0 10px 0">Advise the Supplier in writing of any insolvency event, any change in its name, ownership or control, or any steps taken to sell an asset or assets (separately or together having a value being greater than 20% in value of its gross assets) as soon as practicable and not later than within two business days of such event, change or step occurring.<br>
              The Customer acknowledges that the Customer remains liable to pay for all Goods and/or Services supplied by the Supplier despite any such event.</li>
          </ul>
        </li>
      </ol>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">4. Overdue Accounts</p>
      <ol>
        <li style="margin:0 0 10px 0">Any amount not paid by the due date will incur interest at a rate of 5% above the Reserve Bank of Australia Cash Rate calculated daily and compounded monthly, but in no circumstances will the interest charged exceed 20% per annum.</li>
        <li style="margin:0 0 10px 0">The Customer agrees to pay all costs and incurred by the Supplier in connection with the recovery of overdue amounts and enforcing the charge in clause 4(c) expenses including legal costs, commissions paid by the Supplier or its Related Bodies Corporate to any commercial or mercantile agent and dishonor fees</li>
        <li style="margin:0 0 10px 0"><b>SECURITY AND CHARGE:</b> As security for any amounts due to the Supplier from time to time, the Customer charges all of its legal and equitable interest (both present and future) of whatsoever nature held in any and all Real Property and Other Property to the Supplier.</li>
        <li style="margin:0 0 10px 0">Without limiting the generality of the charge in clause 4(c), the Customer agrees, on request by the Supplier, to execute any documents and do all things reasonably required by the Supplier (including if a beneficial owner, as beneficial owner under the Property Law Act 1969 (WA) and Electronic Conveyancing Act 2014 (WA) or of any similar implied term under the applicable governing law) to perfect the charge given in clause 4 (c) including registering mortgage security over any Real Property. The Customer appoints the Supplier to be the Customer's lawful attorney to execute and register such documents and take all such steps. The Customer indemnifies the Supplier on an indemnity basis against all costs and expenses incurred by the Supplier in preparing and registering the mortgage documents.</li>
        <li style="margin:0 0 10px 0">The Customer consents unconditionally to the Supplier lodging a caveat or caveats noting its interest in any Real Property.</li>
        <li style="margin:0 0 10px 0">The Customer consents unconditionally to the Supplier lodging a mortgage, lien, or other registerable interest against any Other Property.</li>
        <li style="margin:0 0 10px 0">The Customer consents to a fixed and floating charge being placed over the Customer's corporate entity.</li>
        <li style="margin:0 0 10px 0">The Customer consents unconditionally to the Supplier to provide payment security in favour of the Supplier over all monies owed by third parties to the Customer. The Supplier may unconditionally claim direct payment from any third party owing monies to the Customer, only to the maximum extent of monies owed by the Customer to the Supplier, including all expenses due under these T&amp;C</li>
        <li style="margin:0 0 10px 0">A statement in writing signed by an authorised officer of the Supplier setting out the money due or owing to the Supplier at the date of the statement shall be sufficient evidence of the amount so due or owing until the contrary is proven.</li>
      </ol>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">5. Retention of title</p>
      <ol>
        <li style="margin:0 0 10px 0">The Supplier retains legal and equitable title in any Goods supplied to the Customer until payment in full for or in connection with the supply of the relevant Goods has been received by the Supplier. Until payment in full has been received, the following terms apply.</li>
        <li style="margin:0 0 10px 0">Notwithstanding that title in the Goods remains with the Supplier until payment has been received in full, the Customer may sell such goods or use the Goods in a manufacturing or construction process in the ordinary course of the Customer's business. As between the Customer and the purchaser of any Goods, the Customer sells as principal and not as an Agent of the Supplier. The proceeds of the sale of each item of Goods must be held by the Customer in a separate fund on trust for the Supplier, and the Customer is under a duty to account to the Supplier for such proceeds. The creation of, or failure of, any trust shall not in any way limit the obligation of the Customer to pay all amounts to the Supplier for Goods supplied.</li>
        <li style="margin:0 0 10px 0">Until Goods are sold or used in any process, the Customer must keep the Goods safe and free from deterioration, destruction, loss, or harm, clearly designate the Goods as the property of the Supplier, store them in such a way they are clearly identified as the property of the Supplier and keep complete records, firstly, of the physical location of the Goods and, secondly, the ownership of the Goods by the Supplier.</li>
        <li style="margin:0 0 10px 0">The Supplier is irrevocably entitled at any time, and from time to time before the sale of any item of Goods by the Customer, to inspect or to recover and retake possession of such Goods and otherwise exercise all rights in relation to the Goods whether those rights are as owner and/or unpaid Seller or otherwise and whether those rights are conferred by common law, contract, statute or in any other way. To exercise such entitlement, the Supplier and its agents are irrevocably authorised by the Customer to enter any of the Customer's premises or vehicles or those of any third party. The Customer agrees to obtain the consent of any such third party to such entry by the Supplier and to indemnify the Supplier and its agents for any liability arising from any entry upon such third parties' premises or vehicles. The Supplier and its agents agree to take all reasonable care in removing the Goods from such premises or vehicles but, to the extent this liability may be disclaimed by law, are not liable for any damage or injury to such premises caused by the removal of the Goods.</li>
        <li style="margin:0 0 10px 0">This reservation of title and ownership is effective whether or not the Goods have been altered from their supplied form or commingled with other goods.</li>
      </ol>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">6. Security interest PPSA</p>
      <div>
        <ol>
          <li style="margin:0 0 10px 0">The retention of title arrangement described in clause 5 constitutes the grant of a purchase money security interest by the Customer in favour of the Supplier in respect of all present, and after-acquired Goods supplied to the Customer by the Supplier.</li>
          <li style="margin:0 0 10px 0">The Customer must immediately, if requested by the Supplier, sign any documents, provide all necessary information and do anything else required by the Supplier to ensure that the Supplier's purchase money security interest is perfected.</li>
          <li style="margin:0 0 10px 0">The Customer will not enter into any security agreement that permits any other person to have or register any security interest in respect of the Goods or any proceeds from the sale of the Goods until the Supplier has perfected its purchase money security interest.</li>
          <li style="margin:0 0 10px 0">For any Goods supplied that are not goods that are used predominately for personal, domestic, or household purposes, the parties agree to contract out of the application of ss 95, 118, 121(4), 130, 132(4),135, 142 or 143 of the PPSA about the Goods.</li>
          <li style="margin:0 0 10px 0">The Customer hereby waives any rights the Customer may otherwise have to:
            <ul>
              <li style="margin:0 0 10px 0">Receive any notices the Customer would otherwise be entitled to receive under ss 95, 118, 121, 130, 132 or 135</li>
              <li style="margin:0 0 10px 0">Apply to a Court for an order concerning the removal of an accession under section 97</li>
              <li style="margin:0 0 10px 0">Object to a proposal of the Customer to purchase or retain any collateral under ss 130 and 135</li>
              <li style="margin:0 0 10px 0">Receive a copy of a verification statement confirming registration of a financing statement, or a financing change statement, relating to any security interest the Supplier may have in Goods supplied to the Customer from time to time.</li>
            </ul>
          </li>
          <li style="margin:0 0 10px 0">For the purpose of this clause, "PPSA" means the Personal Property Securities Act 2009.</li>
        </ol>
        <p style="margin:0;padding:0;">The expressions "accession", "collateral", "financing statement", "financing change
          statement", "security agreement", "security interest," "perfected security interest" and
          "verification statement" have the meanings given to them under, or in the context of the
          PPSA. References to sections are to sections of the PPSA.</p>
      </div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">7. Risk</p>
      <div>Risk concerning any Goods passes to the Customer on delivery of the Goods. Delivery of Goods will be at the Supplier's premises when collecting the Goods by the Customer, its employees, agent or contractors. If the Supplier has expressly agreed to ship the Goods, risk in the Goods passes immediately on delivery of the Goods to the Customer's designated place of delivery by the Supplier or its agent. <br>
        <br>
        Any goods provided to the Supplier by a Customer for any purpose whatsoever is done so at the Customer's own risk. The Customer agrees to indemnify and hold harmless the Supplier for any damage or loss that may occur while the Customer's goods are in the custody of the Supplier.</div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">8. Unclaimed Goods</p>
      <div>The Customer has fourteen (14) days from the completion of any Goods Services supplied by the Supplier to pick up the Goods and Services from the Suppliers premises. Notification of the completed works and/or Services shall be deemed to commence from either or agreed to completion date specified on any valid purchase order, any change to this date agreed to in writing or, notification by email from the Supplier to the Customer's email address on their website. No other form of notice is valid. <br>
        <br>
        Any Goods owned by the Customer and/or Goods and Services provided by the Supplier that are not picked up within fourteen (14) days after completion of any works and Services supplied by the Supplier become the lawful property of the Supplier in lieu of payment. <br>
        <br>
        The Supplier reserves the right to either charge the Customer a daily holding fee of fifty dollars ($50.00) plus GST or dispose of the unclaimed Goods &amp; Services in any manner the Supplier sees fit. This includes selling the Goods &amp; Services for profit.</div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">9. Exclusion of implied terms</p>
      <div>The Customer may have the benefit of consumer guarantees under the Australian Consumer Law. Otherwise, to the maximum extent permitted by law, all terms, conditions, or warranties would be implied into Supplier Ts&amp;Cs or in connection with the supply of any Goods or Services by the Supplier under law or statute or custom or international convention are excluded.</div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">10. Limitation of liability</p>
      <div>To the maximum extent permitted by law and subject to clauses 9 and 11, the Supplier's total liability arising out of or in connection with its performance of its obligations pursuant to these Conditions, the Supplier Ts&amp;Cs, or arising out of or in connection with the supply of specific Goods or Services (including pursuant to or for breach of these Conditions, Supplier Ts&amp;Cs or repudiation thereof, under statute law, in equity or for tort, including negligent acts or omissions) is limited as follows:
        <ol>
          <li style="margin:0 0 10px 0">the Supplier shall have no liability to the Customer for any Consequential Loss;</li>
          <li style="margin:0 0 10px 0">the Supplier's total aggregate liability for loss, however arising, shall not exceed the GST exclusive aggregate price paid by the Customer to the Supplier for the specific Goods or Services that gave rise to the loss in question. The limitations and exclusions in this sub-clause 9 (b) do not apply to the extent that any Loss is directly attributable to:
            <ul>
              <li style="margin:0 0 10px 0">the personal injury or death caused by the Supplier's default, breach of these Conditions or the Supplier Ts&amp;Cs or negligence; or</li>
              <li style="margin:0 0 10px 0">fraud by the Supplier.</li>
            </ul>
          </li>
        </ol>
        <p style="margin:1rem!important 0 0 0;">Each party must take reasonable steps to mitigate any loss it suffers or incurs.</p>
      </div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">11. Limitation of liability under Competition and Consumer Act 2010 (CCA)</p>
      <div>
        <ol>
          <li style="margin:0 0 10px 0">The Customer must inspect the Goods on delivery and must within seven (7) days of delivery notify the Supplier in writing of any evident defect/damage, shortage in quantity, or failure to comply with the description or quote. The Customer must notify any other alleged defect in the Goods as soon as reasonably possible after any such defect becomes evident. Upon such notification, the Customer must allow the Supplier to inspect the Goods.</li>
          <li style="margin:0 0 10px 0">Under applicable State, Territory, and Commonwealth Law (including, without limiting the CCA), certain statutory implied guarantees and warranties (including, without limiting the statutory guarantees under the CCA) may be implied into these terms and conditions (Non-Excluded Guarantees).</li>
          <li style="margin:0 0 10px 0">The Supplier acknowledges that nothing in these terms and conditions purports to modify or exclude the Non-Excluded Guarantees.</li>
          <li style="margin:0 0 10px 0">Except as expressly set out in these terms and conditions or in respect of the Non-Excluded Guarantees, the Supplier makes no warranties or other representations under these terms and conditions, including but not limited to the quality or suitability of the Goods. The Supplier's liability in respect of these warranties is limited to the fullest extent permitted by law.</li>
          <li style="margin:0 0 10px 0">If the Customer is a consumer within the meaning of the CCA, the Supplier's liability is limited to the extent permitted by section 64A of Schedule 2.</li>
          <li style="margin:0 0 10px 0">If the Supplier is required to replace the Goods under this clause or the CCA but cannot do so, the Supplier may refund any money the Customer has paid for the Goods.</li>
          <li style="margin:0 0 10px 0">If the Customer is not a consumer within the meaning of the CCA, the Supplier's liability for any defect or damage in the Goods is
            <ul>
              <li style="margin:0 0 10px 0">limited to the powder coating manufacturer's warranty;</li>
              <li style="margin:0 0 10px 0">recoating at the Supplier's premises only;</li>
              <li style="margin:0 0 10px 0">otherwise negated absolutely.</li>
            </ul>
          </li>
          <li style="margin:0 0 10px 0">The Supplier shall not be liable for any defect or damage and shall not provide any warranty for:
            <ul>
              <li style="margin:0 0 10px 0">the Customer failing to maintain or store any Goods properly</li>
              <li style="margin:0 0 10px 0">transport, installation, uninstalling, fixing, or removal of any Goods;</li>
              <li style="margin:0 0 10px 0">the Customer using the Goods for any purpose other than that for which they were designed;</li>
              <li style="margin:0 0 10px 0">the Customer continuing the use of any Goods after any defect became apparent or should have become apparent to a reasonably prudent operator or user;</li>
              <li style="margin:0 0 10px 0">the Customer failing to follow any instructions, guidelines, care and maintenance provided by the Supplier</li>
              <li style="margin:0 0 10px 0">fair wear and tear, any accident, or act of God.</li>
            </ul>
          </li>
        </ol>
      </div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">12. GST</p>
      <div>If the Supplier has any liability to pay Goods and Services Tax (GST) on the supply of any Goods or Services to the Customer, the Customer must pay to the Supplier an amount equivalent to the GST liability of the Supplier at the same time as the consideration is paid for the Goods or Services (unless the consideration for that supply is explicitly expressed to be GST inclusive).</div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">13. Privacy disclosure and consent</p>
      <div>
        <p style="margin:0;padding:0;">The Customer authorises Supplier to:</p>
        <ul>
          <li style="margin:0 0 10px 0">obtain credit information about its personal, consumer, and commercial creditworthiness from any their Accountant, Bank, or trade referee disclosed in this document and from any other credit provider or credit reporting agency to assess this application for credit, or in connection with any guarantee given by the Customer</li>
          <li style="margin:0 0 10px 0">use, disclose, or exchange with other credit providers information about the Customer's credit arrangements to assess this application for credit, monitor creditworthiness and collect overdue accounts, and</li>
          <li style="margin:0 0 10px 0">disclose the contents of any credit report on the Customer to the Supplier and its Related Bodies Corporate and any of their solicitors and mercantile agents</li>
        </ul>
        <p style="margin:1rem!important 0 0 0;">Supplier complies with the privacy principles imposed by law about the collection and disclosure of information regarding individuals. For more details on the way the Supplier manages personal information, refer to www.pilbarapowdercoatings.com.au.</p>
      </div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">14. Direct Debit Authority</p>
      <div>The Customer agrees to sign the Irrevocable Direct Debit Authority to ensure payment of their account under clause 3(c)(i). This Irrevocable Direct Debit Authority is to remain in full force and effect while the term of this Credit Agreement exists and beyond until all outstanding monies are paid.</div>
      <p style="text-transform:uppercase;font-weight:600;margin:3rem 0 0 0;padding:0">15. Governing law</p>
      <div>These Conditions are governed by and are to be interpreted according to the laws in force in the state of Western Australia and the parties submit to the non-exclusive jurisdiction of the courts operating in Western Australia. The operation of the United Nations Convention on Contracts for the Sale of International Goods is hereby excluded.</div>
    </div>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">ACKNOWLEDGMENT</p>
    <p style="font-weight:600;font-size:1.2em;margin:1rem 0 0 0;padding:0;">Acknowledgment</p>
    <p style="margin:5px 0 0 0;padding:0;">The Customer acknowledges that the Goods it will acquire from the Supplier will be obtained for either the purpose of re-supply (whether or not in an altered form or as part of some other manufacture) or for the purpose of using them up or transforming them in trade or commerce in the course of a process of production or manufacture or in the course of repairing or treating other goods or fixtures on land.</p>
    <p style="font-weight:600;font-size:1.2em; margin-top:1rem!important;margin:1rem 0 0 0;padding:0;">Who must sign this Agreement on behalf of the Customer</p>
    <p style="margin:5px 0 0 0;padding:0;">For Companies:Where there is only one director for the Company, then that person must sign where there are two or more directors for the Company then 2 Directors or a Director + Company Secretary must sign; for a public or extensive reporting proprietary company, a duly authorised officer.<br>
      Sole Traders operating under their name or a business name:The individual.<br>
      Partnerships:All Partners of the Partnership.</p>
    <p style="margin:1rem!important 0 0 0;">Where this is not possible, please contact the Office - Pilbara Powder Coatings, 2015 Anderson Road, Karratha Industrial Estate WA (08) 9143 1837 or contactus@pilbarapowdercoatings.com.au</p>
    <p style="font-weight:600;font-size:1.2em; margin-top:1.25rem!important;margin:1rem 0 0 0;padding:0;">The Customer agrees to be bound by the Credit Account Terms and Conditions stated in Section 5.</p>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
      <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Signatory</p>
		
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-pos"><b>Position</b></label></div>
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-date"><b>Date</b></label></div>
		<p style="margin:0;padding:0;clear:both;"></p>
      
	  <?php $get_ack_signatory_data = $ci->db->query("SELECT * FROM wl_app_ack_signatory WHERE app_id='".$res['application_id']."'")->result_array();
	  for($ack_sig=0;$ack_sig<4;$ack_sig++){
	  ?>
      <div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_ack_signatory_data[$ack_sig]['ack_sig_print_name'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_ack_signatory_data[$ack_sig]['ack_sig_position'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <div>
              <div>
                <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" placeholder="" value="<?php echo @$get_ack_signatory_data[$ack_sig]['ack_sig_date'];?>">
                <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/></div>
            </div>
          </div>
        </div>
      </div>
      <?php }?>
      
      <p style="margin:0;padding:0;clear:both;"></p>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <div>
      <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
        <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Witness</p>
        <div><div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-pos"><b>Position</b></label></div>
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <label style="display:block" for="sign-date"><b>Date</b></label></div>
		<p style="margin:0;padding:0;clear:both;"></p>
          
        </div>
		<?php 
		$get_ack_witness_data = $ci->db->query("SELECT * FROM wl_app_ack_witness WHERE app_id='".$res['application_id']."'")->result_array();
		for($ack_wit=0;$ack_wit<4;$ack_wit++){
		?>
        <div>
          <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <div>
              <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_ack_witness_data[$ack_wit]['ack_wit_print_name']?>">
            </div>
          </div>
          <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <div>
              <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_ack_witness_data[$ack_wit]['ack_wit_position']?>">
            </div>
          </div>
          <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <div>
              <div>
                <div>
                  <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="" value="<?php echo @$get_ack_witness_data[$ack_wit]['ack_wit_date']?>">
                  <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/> </div>
				  
              </div>
            </div>
          </div>
        </div>
		<?php }?>
        
        
        <p style="margin:0;padding:0;clear:both;"></p>
      </div>
    </div>
    <p style="margin:0 0 45px 0;padding:0;clear:both;"></p>
    <hr>
    <p style="margin:0 0 35px 0;padding:0;clear:both;"></p>
    <div>
      <div style="float:left;">
        <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">PERSONAL GUARANTEE &amp; INDEMNITY FORM</h1>
        <p style="margin:0;padding:0;clear:both;margin-top:.5rem!important;font-size:1.4em;">PILBARA POWDER COATINGS</p>
        <p style="margin:15px 0 0 0;padding:0;">and its Related Body Corporate ABN 21 069 120 341</p>
      </div>
      <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
      <p style="margin:0;padding:0;clear:both;"></p>
    </div>
    <div style="border:1px solid #ddd;padding:1rem!important;padding:0;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> THIS IS AN IMPORTANT DOCUMENT. IF YOU DO NOT UNDERSTAND THIS DOCUMENT YOU SHOULD SEEK INDEPENDENT LEGAL ADVICE. <br>
      <br>
      <b>IMPORTANT:</b> As part of your application for credit, this Personal Guarantee and Indemnity Agreement must be completed and signed by all Directors, Sole Traders and Business Partners in the presence of Independent Witnesses. Spouses/Partners of all Directors, Sole Traders and Business Partners must sign as Guarantors in the presence of an independent witnesses where there is joint ownership of personal assets.</div>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="sign-print"><b>I/We, the Guarantor(s)</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="sign-print" placeholder="" value="<?php echo $res['guarantor_name'];?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="sign-print"><b>hereby request the supplier to supply</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="sign-print" placeholder="" value="<?php echo $res['supplier_to_supply'];?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="sign-print"><b>Trading as (if applicable)</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="sign-print" placeholder="" value="<?php echo $res['trade_applicable'];?>">
        </div>
      </div>
      <div style="padding:0;margin:0;padding:.5rem!important;">
        <label style="display:block" for="sign-print"><b>(the "Customer") of</b></label>
        <input type="email" style="display:block;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;width:28.8%;float:left;" id="sign-print" placeholder="" value="<?php echo $res['guarantor_address'];?>">
        <input type="email" style="display:block;width:12.4%;float:left;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;margin:0 0 0 30px;" id="sign-print" placeholder="" value="<?php echo $res['guarantor_state'];?>">
        <input type="email" style="display:block;width:12.4%;float:left;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;margin:0 0 0 20px;" id="sign-print" placeholder="" value="<?php echo $res['guarantor_zip_code'];?>">
      </div>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="padding:.25rem;">with Goods and Services on Credit.</p>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.25rem!important;">
      <p style="text-transform:uppercase;font-weight:600;margin:0;padding:0">Should the Supplier elect to supply Goods and/or Services:</p>
      <ol>
        <li style="margin:0 0 10px 0">I/We guarantee payment to the Supplier of the total price charged by the Supplier for Goods or Services supplied to the Customer from time to time, without any deduction or setoff whatsoever. I/We also guarantee payment of any other monies now or in the future owing by the Customer to the Supplier.</li>
        <li style="margin:0 0 10px 0">I/We indemnify the Supplier against all costs, losses, and expenses that the Supplier incurs due to any default by the Customer. I/We agree to pay any stamp duty assessed on this Guarantee.</li>
        <li style="margin:0 0 10px 0">My/Our guarantee and indemnity under this Guarantee is a continuing guarantee and will not be affected:
          <ul>
            <li style="margin:0 0 10px 0">if the Supplier grants any extension of time or other indulgences to the Customer or varies the terms of the Customer's account (even if this increases my/our liability under this Guarantee)</li>
            <li style="margin:0 0 10px 0">by the release of any of the Guarantors or if this Guarantee is or becomes unenforceable against one or more of the Guarantors.</li>
            <li style="margin:0 0 10px 0">any payment by the Customer being later avoided by law, whether or not I/we have been given notice of these matters.</li>
          </ul>
        </li>
        <li style="margin:0 0 10px 0">I/We agree that an application for credit made by the Customer is deemed to have been accepted from the date of the first invoice by the Supplier to the Customer and, without further notice to me/us, this Guarantee will extend to all liabilities from the Customer to that Supplier.</li>
        <li style="margin:0 0 10px 0">This Guarantee extends to credit given to the Customer in the future by a company which is not now, but at the time such credit is extended, a Related Bodies Corporate.</li>
        <li style="margin:0 0 10px 0">This Guarantee may be withdrawn by the Guarantor(s) on expiry of 14 days following written notice of withdrawal being delivered to the Supplier's Company Secretary at the Supplier's registered office. This Guarantee will continue in force in respect of all debt incurred up to the date of withdrawal.</li>
        <li style="margin:0 0 10px 0">I/We authorise the Supplier to do each of the things listed in clause 12 of the Credit Account Terms and Conditions about my/our personal credit matters.
          <ul>
            <li style="margin:0 0 10px 0">As security for the obligations and liabilities of the
              Guarantor(s), I/we charge for the due and punctual payment
              and performance of those obligations and liabilities, all of my/
              our legal and equitable interest (including as beneficial owner,
              both present and future) of whatsoever nature held in any and
              all Real Property or Other Property in favour of the Supplier.</li>
            <li style="margin:0 0 10px 0">Without limiting the generality of the charges in Clause 4, I/we agree on a request by the Supplier to execute any documents and do all things reasonably required by the Supplier to register mortgage security over any Real Property and Other Property. If the Guarantor(s) fails to deliver the requested documents, the Guarantor(s) hereby appoints Supplier to be the Guarantor's (s') lawful attorney to execute and register such documents. I/We indemnify the Supplier on an indemnity basis against all costs and expenses incurred by the Supplier, as the case may be in connection with the preparation and registration of such mortgage documents.</li>
            <li style="margin:0 0 10px 0">I/We consent unconditionally to the Supplier lodging a caveat or caveats noting its interest in any Real Property.</li>
            <li style="margin:0 0 10px 0">I/We consents unconditionally to the Supplier lodging a mortgage, lien, or other registerable interest against any Other Property.</li>
            <li style="margin:0 0 10px 0">I/We agree to advise the Supplier in writing of the occurrence of any Insolvency Event, any change in my/our name, ownership or control, or any step being taken to sell an asset or assets (separately or together having a value being greater than 20% in value of my/our gross assets) as soon as practicable and not later than within two business days of such event, change or step occurring.</li>
            <li style="margin:0 0 10px 0">If any payment made by or on behalf of the Customer is alleged to be void or voidable by any liquidator or like an officer of the Customer under any law related to insolvency, I/we indemnify the Supplier against any costs or losses it may incur in connection with such claim. This indemnity shall continue to apply notwithstanding any withdrawal under clause 6.</li>
            <li style="margin:0 0 10px 0">If the charge created by clause 8(a) is or becomes void or unenforceable, it may be severed from this Guarantee without affecting the Supplier's rights against the Guarantor(s).</li>
            <li style="margin:0 0 10px 0">If the Guarantor(s) is a trustee of a trust, the Guarantor(s) enters into this agreement in both the Guarantor's capacity and as trustee of that trust.</li>
            <li style="margin:0 0 10px 0">Any condition or agreement under this Guarantee by or in favour of two or more persons is deemed to bind them jointly and severally or be in favour of each of them severally. If the Guarantor comprises more than one person, the Supplier may at any time, and from time to time, proceed against any or all of them in respect of the Guarantor's obligations as the Supplier may choose in its absolute discretion, and the Supplier is not to be obliged to make any claim against all the persons comprising the Guarantor.</li>
            <li style="margin:0 0 10px 0">Until the whole of the Customer's obligations have been paid or satisfied in full, the Guarantor must not (except with the prior written consent of the Supplier) either directly or indirectly, and either before or after the winding up or bankruptcy of the Customer, or any person, take any steps to recover or enforce a right or claim against the Customer relating to any sum paid by the Guarantor to the Supplier under this Guarantee including without limitation proving or claiming in competition with the Supplier to diminish any distribution, dividend or payment which, but for the proof or claim, the Supplier would be entitled to receive according to the winding up or bankruptcy of the Customer.</li>
            <li style="margin:0 0 10px 0">The definitions in the Credit Account Terms and Conditions shall apply in this Guarantee, except that "Real Property" shall mean all real property owned by the Guarantor(s) now or in the future, solely or jointly. Also, singular words include the plural and vice versa, and references to any party to this Guarantee include that party's executors, administrators, substitutes, successors or permitted assigns.</li>
          </ul>
        </li>
      </ol>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="margin:3rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">PERSONAL GUARANTEE &amp; INDEMNITY FORM</p>
    <p style="font-weight:600;font-size:1.2em;margin:1rem 0 0 0;padding:0;">CERTIFICATE OF GUARANTEE � Executed as a Deed</p>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.25rem!important;"><b style="font-weight:600;font-size:1.2em;margin:1rem 0 .5rem 0;padding:0;">Acknowledgment:</b><br>
      By signing below as Guarantor(s), I/we certify that I/we understand the terms of this Guarantee. In particular, I/we understand that if the Customer fails to make any required payments to the Supplier, the Supplier may recover the amount of these payments from me/us. In such case, the Supplier may, amongst other recovery rights, take a charge over any Real Property. <br>
      <br>
      That I/We have had the opportunity of taking independent legal advice about the meaning and effect of this Guarantee. All Directors, Sole Traders, Business Partners and any other Guarantors complete, print and sign below as Guarantors in the presence of Independent Witnesses (not Spouses or Family Members).</div>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
      <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Guarantor</p>
      <div>
		  
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-pos"><b>Address</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-date"><b>Date</b></label></div>
			<p style="margin:0;padding:0;clear:both;"></p>
      </div>
	  <?php 
	  $get_cert_guarantor_data = $ci->db->query("SELECT * FROM wl_app_cert_guarantor WHERE app_id='".$res['application_id']."'")->result_array(); 
	  for($per_gua=0;$per_gua<4;$per_gua++){
	  ?>    
      
      <div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cert_guarantor_data[$per_gua]['per_gua_print_name'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cert_guarantor_data[$per_gua]['per_gua_address'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <div>
              <div>
                <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="" value="<?php echo @$get_cert_guarantor_data[$per_gua]['per_gua_date'];?>">
                <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/> </div>
            </div>
          </div>
        </div>
        <p style="margin:0;padding:0;clear:both;"></p>
      </div>
	  <?php }?>
	  
    </div>
    <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
      <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Witness <span>(Executed by Independent Witnesses)</span></p>
      <div>
		  <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-pos"><b>Address</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-date"><b>Date</b></label></div>
			<p style="margin:0;padding:0;clear:both;"></p>        
      </div>
      
	  <?php  $get_cert_witness_data = $ci->db->query("SELECT * FROM wl_app_cert_witness WHERE app_id='".$res['application_id']."'")->result_array();
	  		for($per_wit=0;$per_wit<4;$per_wit++){
	   ?>
      <div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cert_witness_data[$per_wit]['per_wit_print_name'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cert_witness_data[$per_wit]['per_wit_address'];?>">
          </div>
        </div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
          <div>
            <div>
              <div>
                <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="" value="<?php echo @$get_cert_witness_data[$per_wit]['per_wit_date'];?>">
                <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/> </div>
            </div>
          </div>
        </div>
      </div>
      <?php }?>
      <p style="margin:0;padding:0;clear:both;"></p>
    </div>
  </div>
  <p style="margin:0 0 45px 0;padding:0;clear:both;"></p>
  <hr>
  <p style="margin:0 0 35px 0;padding:0;clear:both;"></p>
  <div>
    <div style="float:left;">
      <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">LIMITED WARRANTY</h1>
      <p style="margin:0;padding:0;clear:both;">PILBARA POWDER COATINGS</p>
      <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate ABN 21 069 120 341</p>
    </div>
    <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
    <p style="margin:0;padding:0;clear:both;"></p>
  </div>
  <div style="float:left;width:50%;">
    <label style="display:block" for="sign-print"><b>I/We, the Customer(s)</b></label>
    <input type="email" style="display:block;width:92%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="sign-print" placeholder="" value="<?php echo $res['limited_customer_name']?>">
  </div>
  <div style="float:right;width:50%;">
    <label style="display:block" for="sign-print"><b>Trading as (if applicable)</b></label>
    <input type="email" style="display:block;width:92%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="sign-print" placeholder="" value="<?php echo $res['limited_business_name']?>">
  </div>
  <p style="margin:20px 0 0 0;padding:0;clear:both;"></p>
  <div style="padding:0;margin:0;padding:.5rem 0;">
    <label style="display:block" for="sign-print"><b>of</b></label>
    <input type="email" style="display:block;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;width:46%;float:left;" id="sign-print" placeholder="" value="<?php echo $res['limited_address'];?>">
    <input type="email" style="display:block;width:20.7%;float:left;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;margin:0 0 0 30px;" id="sign-print" placeholder="" value="<?php echo $res['limited_state'];?>">
    <input type="email" style="display:block;width:20.7%;float:left;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;margin:0 0 0 20px;" id="sign-print" placeholder="" value="<?php echo $res['limited_zip_code']?>">
  </div>
  <p style="margin:0;padding:0;clear:both;"></p>
  <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:1rem;"><b>Terms and Conditions of this Limited Warranty:</b><br>
    <ol>
      <li style="margin:0 0 10px 0">This Limited Warranty is implied and expressed to the extent of the manufacturer of the powder coating product used. These manufacturers are Axalta, Interpon, Dulux and OxyTech or any other manufacturer whose powder coating products Pilbara Powder Coatings may use.</li>
      <li style="margin:0 0 10px 0">Manufacturer Warranties specified in Clause 1 above can be found at the following websites:
        <p style="margin:0;padding:0;clear:both;"></p>
        <div style="width:25%;float:left;">
          <p style="margin:0;padding:0;">Axlata</p>
          <p style="margin:0;padding:0;"><a href="https://www.axalta.com/" target="_blank" style="color:#007bff;text-decoration:none;">www.axalta.com</a></p>
        </div>
        <div style="width:25%;float:left;">
          <p style="margin:0;padding:0;">Interpon</p>
          <p style="margin:0;padding:0;"><a href="http://www.specifyinterpon.com.au/" target="_blank" style="color:#007bff;text-decoration:none;">www.specifyinterpon.com.au</a></p>
        </div>
        <div style="width:25%;float:left;">
          <p style="margin:0;padding:0;">Dulux</p>
          <p style="margin:0;padding:0;"><a href="https://duluxpowders.com.au/" target="_blank" style="color:#007bff;text-decoration:none;">www.duluxpowders.com.au</a></p>
        </div>
        <div style="width:25%;float:left;">
          <p style="margin:0;padding:0;">OxyTech</p>
          <p style="margin:0;padding:0;"><a href="https://www.oxytech.com.au/" target="_blank" style="color:#007bff;text-decoration:none;">www.oxytech.com.au</a></p>
        </div>
        <p style="margin:0;padding:0;clear:both;"></p>
      </li>
      <li style="margin:0 0 10px 0">No warranty is implied or expressed by the Supplier other than that of the Warranties of the manufacturers and their powder coating products, specified in Clause 1 above.</li>
      <li style="margin:0 0 10px 0">Pilbara Powder Coatings denies any expressed or implied liabilities that may arise from a successful Warranty claim. Any successful Warranty claim is strictly limited to the liability of the manufacturer's Warranty.</li>
      <li style="margin:0 0 10px 0">Where a manufacturer accepts a Warranty claim, at its sole discretion and in limited circumstances, the Supplier may agree to recoat the affected Goods at the Supplier's cost.</li>
      <li style="margin:0 0 10px 0">When the Supplier agrees to recoat any affected Goods under a manufacturer's Warranty, the recoating is to be done only at the Supplier's premises.</li>
      <li style="margin:0 0 10px 0">When the Supplier agrees to recoat any affected Goods under a manufacturer's Warranty, in no way whatsoever does this make Supplier liable under the manufacturer's Warranty for any claims that may arise from the Warranty claim.</li>
      <li style="margin:0 0 10px 0">Any Goods the Supplier agrees to recoat are to be recoated at the Supplier's premises only. The Customer must arrange for the Goods to be delivered to, and pick-up from, the Supplier's premises at the Customer's own expense.</li>
      <li style="margin:0 0 10px 0">The recoating of any Goods at the Supplier's discretion is limited to the recoating only at the Suppliers premises and excludes:
        <ul>
          <li style="margin:0 0 10px 0">transport of any Goods to and from the Supplier's premises;</li>
          <li style="margin:0 0 10px 0">uninstallation and/or installation of any Goods;</li>
          <li style="margin:0 0 10px 0">unfixing and/or fixing of any Goods;</li>
          <li style="margin:0 0 10px 0">anything other than the recoating of the Goods at the Supplier's premises.</li>
        </ul>
      </li>
      <li style="margin:0 0 10px 0">The Supplier may refuse to recoat any Goods where:
        <ul>
          <li style="margin:0 0 10px 0">previously repainted or repaired OEM finishes</li>
          <li style="margin:0 0 10px 0">failure due to pre-existing rust or rust originating from the Customer's Goods;</li>
          <li style="margin:0 0 10px 0">scratches, abrasions, or stone chips</li>
          <li style="margin:0 0 10px 0">damage caused by accidents.</li>
          <li style="margin:0 0 10px 0">damage to the paint film caused by improper care, abrasive detergents or waxes, acid rain, industrial emissions or fallout, or heavy-duty pressure washing.</li>
          <li style="margin:0 0 10px 0">failure resulting from misuse or abuse, including improper storage.</li>
          <li style="margin:0 0 10px 0">failures of systems or components containing products from other manufacturers not specified in Clause 1</li>
          <li style="margin:0 0 10px 0">failure of finishes/systems which have been applied outside manufacturers recommendations</li>
          <li style="margin:0 0 10px 0">failure of Goods that have not been applied fo their specific use</li>
          <li style="margin:0 0 10px 0">there is fair wear and tear, any accident, or act of God.</li>
        </ul>
      </li>
      <li style="margin:0 0 10px 0">This Warranty sets forth the entire limited Warranty by the Supplier. Except as otherwise expressly set forth herein, the Supplier makes no representations or warranties, either expressed or implied, with respect to any Goods and Services provided by the Customer. Without limitation that the Goods and Services will increase productivity, financial performance, efficiency or profitability, the Customer agrees that any implied warranty of merchantability or fitness for a particular use is expressly denied.</li>
      <li style="margin:0 0 10px 0">In no event shall any Goods representation or Warranties received by the Customer from the Supplier create any more Warranty than that of the manufacturer's Warranty. Where a term in this Limited Warranty conflicts with the manufacturer's Warranty, the manufacturer's Warranty shall prevail with all liabilities and obligations resting with the manufacturer.</li>
      <li style="margin:0 0 10px 0">Notwithstanding any provision of this Warranty or a manufacturer's Warranty or any incentive document to the contrary, in no event shall the Supplier be liable to the Customer for:
        <ul>
          <li style="margin:0 0 10px 0">any punitive, exemplary, or other special damages arising under or relating to any Warranty or the subject matter thereof;</li>
          <li style="margin:0 0 10px 0">any indirect, incidental, or consequential damages (including, without limitation, loss of use, income, profits or anticipated profits, business or business opportunity, savings or business reputation) arising under or relating to any Warranty agreement or any incentive document or the subject matter hereof, regardless of whether such damages are based in contract, breach of warranty, tort, negligence or any other theory, irrespective of whether such party of, knew of, or should have known of the possibility of such damages</li>
          <li style="margin:0 0 10px 0">the remedies set forth herein shall be the sole and exclusive remedy for any breach of a manufacturer's warranty;</li>
          <li style="margin:0 0 10px 0">no event shall aggregate the liability to the Customer for each claim arising from a manufacturer's Warranty or subject thereof, regardless of whether such claims are based in contract, breach of warranty, tort, negligence, or any other theory exceeding that specified in the manufacturer's Warranty;</li>
          <li style="margin:0 0 10px 0">This section shall survive any termination or expiration of a manufacturer's Warranty.</li>
        </ul>
      </li>
      <li style="margin:0 0 10px 0">This Warranty Agreement shall be governed by, and construed, and enforced according to the laws of the state of Western Australia and the Commonwealth of Australia. Each party consents and submits to the exclusive jurisdiction of and service of process by the Courts of Western Australia.</li>
    </ol>
  </div>
  <p style="font-weight:600;font-size:1.2em; margin-top:1.25rem!important;margin:1rem 0 0 0;padding:0;">CERTIFICATE OF GUARANTEE � Executed as a Deed</p>
  <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:1rem;"><b>Acknowledgment:</b><br>
    The Customer acknowledges that this is a Limited Warranty to the extent of the terms set out above. This Warranty Agreement may be executed in counterparts with the same force and effect as if executed in a complete document. Facsimile or electronically transmitted signatures shall have the same force and effect as original signatures. <br>
    <br>
    The Customer acknowledges, understands and agrees to this Limited Warranty. The signatory/ies below hereby confirm they have the authority to sign this agreement and all agreements on behalf of the Customer.</div>
  <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
    <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Signatory</p><div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-pos"><b>Address</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-date"><b>Date</b></label></div>
			<p style="margin:0;padding:0;clear:both;"></p>    
    
	<?php  $get_cirt_gua_data = $ci->db->query("SELECT * FROM wl_app_cirt_gua_signatory WHERE app_id='".$res['application_id']."'")->result_array();
	for($cirt_gua=0;$cirt_gua<4;$cirt_gua++){
	 ?>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cirt_gua_data[$cirt_gua]['cirt_gua_print_name']; ?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" placeholder="" value="<?php echo @$get_cirt_gua_data[$cirt_gua]['cirt_gua_address']; ?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <div>
            <div>
              <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="" value="<?php echo @$get_cirt_gua_data[$cirt_gua]['cirt_gua_date']; ?>">
              <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/> </div>
          </div>
        </div>
      </div>
    </div>
	<?php }?>
    <p style="margin:0;padding:0;clear:both;"></p>
  </div>
  <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:.5rem!important;">
    <p style="font-weight:600;font-size:1.1em;color:#80280b;text-decoration:none;text-transform:uppercase!important;text-align:center!important;padding:.5rem!important;margin:0;">Witness</p>
    <div>
		<div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-print"><b>Print Name</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-pos"><b>Address</b></label></div>
        <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
            <label style="display:block" for="sign-date"><b>Date</b></label></div>
			<p style="margin:0;padding:0;clear:both;"></p>
      
    </div>
    
    <?php 
	 $get_cirt_gua_witness_data = $ci->db->query("SELECT * FROM wl_app_cirt_gua_witness WHERE app_id='".$res['application_id']."'")->result_array();
	 for($cirt_wit=0;$cirt_wit<4;$cirt_wit++){
	?>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="witn-print" placeholder="" value="<?php echo @$get_cirt_gua_witness_data[$cirt_wit]['cirt_wit_print_name']?>">
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="witn-pos" placeholder="" value="<?php echo @$get_cirt_gua_witness_data[$cirt_wit]['cirt_wit_address']?>">
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <input type="email" style="display:block;width:79%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="witn-date" placeholder="" value="<?php echo @$get_cirt_gua_witness_data[$cirt_wit]['cirt_wit_date']?>">
        <img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""/> </div>
    </div>
	<?php }?>
    <p style="margin:0;padding:0;clear:both;"></p>
  </div>
  <p style="margin:0 0 45px 0;padding:0;clear:both;"></p>
  <hr>
  <p style="margin:0 0 35px 0;padding:0;clear:both;"></p>
  <div>
    <div style="float:left;">
      <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">DIRECT DEBIT AUTHORITY</h1>
      <p style="margin:0;padding:0;clear:both;">PILBARA POWDER COATINGS</p>
      <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate ABN 21 069 120 341</p>
    </div>
    <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
    <p style="margin:0;padding:0;clear:both;"></p>
  </div>
  <div style="border-radius:10px;border:1px solid #ddd;padding:1rem!important;margin-top:1rem!important; background-color:#f8f9fa!important;">
    <p style="margin:0;padding:0;border-radius:5px; text-align:center!important;">Request and Authority to debit the account named below to pay<br>
      <b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b></p>
    <hr>
    <p style="font-weight:600;font-size:1.2em;color:#80280b;text-decoration:none;text-transform:uppercase!important;padding:.5rem!important;margin-top:1rem!important;margin:0;">Request and Authority to debit</p>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Account Number</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_account_number']?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Surname or Company name</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_company_name']?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Given names or ABN/ARBN</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_abn']?>">
        </div>
      </div>
    </div>
    <div style="padding:.5rem;">request and authorise <b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b> to arrange, through its own financial institution, a debit to your nominated account for any amount due and payable by you.<br>
      <br>
      This debit or charge will be made through the Bulk Electronic Clearing System Framework (BECS) from your account held at the financial institution you have nominated below and will be subject to the terms and conditions of the Direct Debit Request Service Agreement.</div>
    <p style="font-weight:600;font-size:1.2em;color:#80280b;text-decoration:none;text-transform:uppercase!important;padding:.25rem!important;margin-top:1rem!important;margin:0;">Name and address of financial institution at which account is held</p>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Financial institution name</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_institution_name']?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Address</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_address']?>">
        </div>
      </div>
    </div>
    <p style="margin:0;padding:0;clear:both;"></p>
    <p style="font-weight:600;font-size:1.2em;color:#80280b;text-decoration:none;text-transform:uppercase!important;padding:.25rem!important;margin-top:1rem!important;margin:0;">Account to be debited</p>
    <div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Name/s on account</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_account_name']?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>BSB number (Must be 6 Digits)</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['dir_bsb_number']?>">
        </div>
      </div>
      <div style="padding:0;margin:0;width:31.8%;float:left;padding:.5rem!important;">
        <div>
          <label style="display:block" for="customer"><b>Account number</b></label>
          <input type="email" style="display:block;width:64;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer"  value="<?php echo $res['dir_account_dbt_number']?>">
        </div>
      </div>
    </div>
    <div style="padding:.5rem!important;margin-top:.5rem!important;">
      <p style="font-weight:600;font-size:1.1em;margin:0;padding:0;">Acknowledgment</p>
      <p style="margin:.25rem 0 0 0">By signing and/or providing us with a valid instruction in respect to your Direct Debit Request, you have understood and agreed to the terms and conditions governing the debit arrangements between you and <b>Reefwind Pty Ltd t/a Pilbara Powder Coatings</b>, as set out in this Request and the Credit Application signed and agreed to by you (refer Section 5, Clause 3(c)(i) and 14).</p>
    </div>
  </div>
  <p style="padding:.25rem !important;margin:.5rem!important 0 0 0;">
    <input name="" type="submit" value="Submit" style="font-size:1em;text-transform:uppercase;padding:10px 30px;border-radius:5px;color:#fff;background-color:#343a40;border-color:#343a40;display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">
  </p>
  <p style="margin:1rem!important 0 0 0;"><b>Email -</b><br>
    the completed form to <a href="mailto:accounts@pilbarapowdercoating.com.au">accounts@pilbarapowdercoating.com.au</a></p>
  <p style="margin:1rem!important 0 0 0;"><b>Post -</b><br>
    the completed form to Pilbara Powder Coating, 2015 Anderson Road, Karratha Industrial Estate WA 6714</p>
  <p style="margin:1rem!important 0 0 0;"><b>In person -</b><br>
    2015 Anderson Road, Karratha Industrial Estate WA</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
	<?php  
}


function order_form_inline_html_view($res)
{
	$ci=CI();
	$site_details=get_db_single_row('tbl_admin','*',' and  admin_id =1');
	?>
	
	<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body style="font-size:14px; color:#333; margin:15px auto; padding:5px; font-family:Arial, Helvetica, sans-serif; min-width:1230px;width:1230px;line-height:22px;">
<div style="background:#fff;padding:20px;border-radius:10px;margin-bottom:20px;box-shadow:0 0 10px #ccc;">
  <div style="float:left;margin:20px 0 0 0; width:60%;">
    <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">PILBARA POWDER COATINGS</h1>
    <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341</p>
  </div>
  <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
  <p style="margin:0;padding:0;clear:both;"></p>
  <div style="border:1px solid #ddd;padding:20px 20px 20px 20px;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>
  <p style="margin:0;padding:0;clear:both;"></p>
<div style="float:left;width:48%;">
	<p style="font-size:1.8em;text-decoration: none;margin:2rem 0 0 0;padding:0;">Purchase Order</p>
	<p style="margin:1.5rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Customer</p>
	<div style="margin:15px 0 0 0;">
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Name</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['first_name'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Address</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['address'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Suburb, State Postcode</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['state'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Phone</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['phone'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
          <label style="display:block" for="customer"><b>Email</b></label>
          <input type="email" style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $res['email'];?>">
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
    </div>
  </div>	
  
<div style="float:right;width:48%;padding:22px 0 0 0;">
	<div style="padding:0;margin:0;width:44%;float:left;padding:.5rem!important;">
                  <input type="email" style="display:block;width:75%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="Date" value="<?php echo $res['purchase_date']?>"><img src="<?php echo theme_url();?>images/calander.png" style="float:left;margin:5px 0 0 15px;" alt=""> </div>
	<div style="padding:0;margin:0;width:50%;float:right;padding:.5rem!important;">
                  <input type="email" style="display:block;width:94%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left;" id="dtl-pos" placeholder="PO" value="<?php echo $res['po_text'];?>"></div><p style="margin:0;padding:0;clear:both;"></p>
	<p style="margin:.4rem 0 0 0;padding:0;background-color:#343a40!important;padding:.7rem!important;text-transform:uppercase!important;color:#fff;text-decoration:none;font-size:1.4em;font-weight:600;border-radius:5px;">Ship To</p>
	<div style="margin:15px 0 0 0;">
      <div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
	    <textarea style="display:block;width:95.5%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;height: 342px;"><?php echo $res['ship_to'];?></textarea>
      </div>
      <p style="margin:0 0 20px 0;padding:0;clear:both;"></p>
    </div>
  </div>	
  <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
<p style="margin:0 0 45px 0;padding:0;clear:both;"></p>	
<div>
	<table width="100%" border="1" cellpadding="3" cellspacing="3" rules="all" style="border: 1px solid #ddd; font-size:20px !important;">
  <tbody>
    <tr>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600; height:60px;">Contact</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;height:60px;">Ship Via</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;height:60px;">Delivery</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;height:60px;">Shipping Terms</td>
    </tr>
    <tr>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out; height:35px;" id="customer" value="<?php echo $res['contact_no'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out; height:35px;" id="customer" value="<?php echo $res['ship_via'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out; height:35px;" id="customer" value="<?php echo $res['delivery'];?>"></td>
      <td style="padding:.75rem;"><input type="email" style="display:block;width:90%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out; height:35px;" id="customer" value="<?php echo $res['shipping_term'];?>"></td>
    </tr>
    </tbody>
</table>
</div>
<p style="margin:0 0 65px 0;padding:0;clear:both;"></p>	
	<table width="100%" border="1" rules="all" style="border: 1px solid #ddd;">
  <tbody>
    <tr>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:18%">Item</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;">Description</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:20%">M2</td>
      <td style="padding:.75rem;background-color:#343a40;color:#fff;font-size:16px;font-weight:600;width:20%">Total (ex. GST)</td>
    </tr>
	<?php 
	$get_order_detial_data = $ci->db->query("SELECT * FROM wl_orders_products WHERE orders_id='".$res['order_id']."'")->result_array();
	if(is_array($get_order_detial_data) && !empty($get_order_detial_data))
	{
		foreach($get_order_detial_data as $key=>$val){
	?>
    <tr>
      <td style="padding:15px;"><p style="border:5px; font-size:25px;"><input type="email" style="display:block;width:87%;padding:.375rem .75rem;font-size:40px;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['item_name']?>"></p></td>
      <td style="padding:15px;"><p style="border:5px; font-size:25px;"><input type="email" style="display:block;width:94.6%;padding:.375rem .75rem;font-size:40px;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['description']?>"></p></td>
      <td style="padding:15px;"><p style="border:5px; font-size:25px;"><input type="email" style="display:block;width:88%;padding:.375rem .75rem;font-size:40px;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['m2']?>"></p></td>
      <td style="padding:15px;background:#f8f9fa"><p style="border:5px; font-size:25px;"><input type="email" style="display:block;width:88%;padding:.375rem .75rem;font-size:40px;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" value="<?php echo $val['item_price']?>"></p></td>
    </tr>
	<?php }
	}?>
	
    </tbody>
</table>
<p style="margin:0 0 45px 0;padding:0;clear:both;"></p>	
	<table width="100%" border="1" rules="all" style="border: 1px solid #ddd;">
  <tbody>
    <tr>
      <td width="66%">&nbsp;</td>
      <td style="padding:.5rem 0"><table width="100%" border="0">
  <tbody>
    <tr>
      <td style="padding:.15rem .75rem;width:60%;font-size:16px">Subtotal</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer15"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">GST</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer14" value="<?php echo $res['gst']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">Shipping</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer13" value="<?php echo $res['shipping']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px">Other</td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer12" value="<?php echo $res['other']?>"></td>
      </tr>
    <tr>
      <td style="padding:.15rem .75rem;font-size:16px"><strong>Total</strong></td>
      <td style="padding:.15rem .75rem;"><input type="email" style="display:block;width:81%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;" id="customer" <?php echo $res['total_price'];?>></td>
    </tr>
    </tbody>
</table>
</td>
    </tr>
    </tbody>
</table>
  <p style="margin:0;padding:0;clear:both;"></p>
	<p style="margin:10px 0 15px 0;padding:0;">(Please use the drawing sheet below for detail works)</p>
	<p style="font-weight:600;font-size:1.1em;margin:0;padding:0;">Comments or Special Instructions</p>
	<div style="padding:0;margin:0;width:100%;margin:.5rem 0 0 0">
	    <textarea rows="6" style="display:block;width:98%;padding:.375rem .75rem;font-size:.8rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;"><?php echo $res['comment'];?></textarea>
      </div>
  <p style="margin:0 0 45px 0;padding:0;clear:both;"></p>
  <div>
    <div style="float:left;margin:20px 0 0 0;">
    <h1 style="font:25px/25px Arial, Helvetica, sans-serif;color:#454545;margin:0;padding:0;">PILBARA POWDER COATINGS</h1>
    <p style="margin:10px 0 0 0;padding:0;">and its Related Body Corporate Reefwind Pty Ltd ABN 21 069 120 341

</p>
  </div>
    <p style="float:right;margin:0;padding:0;"><img src="<?php echo theme_url();?>images/ppc-pdf.jpg" width="200" alt=""></p>
    <p style="margin:0;padding:0;clear:both;"></p>
	  <div style="border:1px solid #ddd;padding:1rem!important;padding:0;margin-top:1rem!important;border-radius:10px;color:#dc3545!important;"><b>WARNING:</b> This is a binding order form committing you to purchase the details you have outlined below. When you submit this Order Form, you agree to all terms and conditions of sale, which is outlined in our website and Credit Application for clients with credit accounts. If you do not have a credit account, a deposit may be required with the balance or full payment due upon completion of works prior to pick up.</div>
  </div>
	<p style="font-weight:600;font-size: 1.6em;text-transform: uppercase!important;margin-top: 1.5rem!important; margin: 0;padding:0;">DRAWING SHEET</p>
	<div style="border:#333 5px solid; height:600px;padding: 1rem;margin-top: .5rem;"></div>
	<p style="margin:.5rem!important 0 0 0;">
    <input name="" type="submit" value="Submit" style="font-size:1em;text-transform:uppercase;padding:10px 30px;border-radius:5px;color:#fff;background-color:#007bff;border-color:#007bff;display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">
  </p>
</div>
</body>
</html>
	<?php
	
}