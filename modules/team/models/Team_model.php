<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Team_model extends MY_Model{ 

	 public function get_team($limit='10',$offset='0',$param=array()){

		$status			    =   @$param['status'];
		$orderby		    =   @$param['orderby'];	
		$latest_team		    =   @$param['latest_team'];
		$where		        =   @$param['where'];

		$keyword = $this->db->escape_str($this->input->get_post('keyword',TRUE));

	    if($status!=''){
			$this->db->where("wl_team.status",$status);
		}

		if($latest_team!=''){
			$this->db->where("wl_team.latest_team",$latest_team);
		}

	    if($where!=''){
			$this->db->where($where);
		}

		if($keyword!=''){
			$this->db->where("(wl_team.team_title LIKE '%".$keyword."%' OR wl_team.publisher LIKE '%".$keyword."%' )");
		}

		if($orderby!=''){
			 $this->db->order_by($orderby);
		}else{
		  $this->db->order_by('wl_team.team_id','desc');		
		}

		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wl_team.*',FALSE);
		$this->db->from('wl_team');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;	
	}

}


?>