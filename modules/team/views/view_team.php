<?php $this->load->view("top_application");?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   } </script>
<?php navigation_breadcrumb('Meet Our Team');?>
<div class="container">
<div class="mid_area">
<div class="cms_area">
<div class="pl-1 pr-1">
<h1 class="mb-2">Meet Our Team</h1>
 <?php echo form_open("",'id="myform" method="post" '); ?>
      <input type="hidden" name="per_page" value="<?php echo $this->config->item('per_page');?>">
      <input type="hidden" name="offset" id="pg_offset" value="0">
      <?php echo form_close();?>
    <div class="w-100 mt-5 mb-4">
    <?php
      if(is_array($res) && !empty($res)){ ?>
    <ul class="float" id="prodListingContainer">
 	<?php		  
		$data['res'] = $res;            
       $this->load->view('team/load_team_view',$data);
	   ?>    
<!-- end -->
       </ul>
    <div class="clearfix"></div>
    <?php
		}else{
		 echo '<div class="mt-3 text-center"><strong>No record found !</strong></div>';
		}
		?>
    </div>

<div class="clearfix"></div>
</div>
</div>
</div>
</div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>