<?php
$this->load->model(array('news/news_model'));
$param         = array('status'=>'1','latest_news'=>'1');	
$res_array     = $this->news_model->get_news(4,0,$param);

if(is_array($res_array) && !empty($res_array))
{
?>
<div style="background:#003f80; padding:15px 0; text-align:center;">
<div class="container">
	<p class="white fs36 weight300 mb5 text-uppercase">Latest News</p>
    <div id="owl-example3">
    <?php
  foreach($res_array as $val)
  {
	  $news_links = site_url($val['friendly_url']);
	  
	  if($val['alt_tag']){
		  $alt_tag=escape_chars(ucfirst($val['alt_tag']));
	  }else{
		  $alt_tag=escape_chars(ucfirst($val['news_title']));
	  }
		  $post_date=@explode(" ",getDateFormat($val["recv_date"],10));
	  ?>
    <div class="item">
    <p class="orange fs16 b"><a href="<?php echo $news_links;?>" title="<?php echo escape_chars(ucfirst($val['news_title']));?>"><?php echo char_limiter($val['news_title'],70); ?></a></p>
    <p class="fs11 white"><?php echo getDateFormat($val['recv_date'],1);?></p>
    <p class="fs13 white mt5"><?php echo char_limiter($val['news_description'],300); ?></p>
    <!--<p class="mt5 text-center orange"><a href="news-details.htm" title="Read More">Read More <span class="fa fa-caret-right ml1 fs16 mt2"></span></a></p>-->
    </div>
     <?php } ?>   
    
    </div>
</div>
</div>

 <?php
}
?>
