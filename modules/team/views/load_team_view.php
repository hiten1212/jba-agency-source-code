<?php
if(is_array($res) && !empty($res) ){ 
  foreach($res as $val){  
	$portfolio_links = site_url($val['friendly_url']);
	if($val['alt_tag']){
		$alt_tag=ucfirst($val['alt_tag']);
	}else{
		$alt_tag=ucfirst($val['team_title']);
	}	
  ?>
  <li class="listpager">
    <div class="team-area">
    <div class="team-box">
    <figure><a href="javascript:void(0);">
    <img src="<?php echo get_image('team',$val['team_image'],'227','227','R') ?>" alt=""/>
    </a></figure>
    </div>
    <div class="team-txt-area">
    <p><a href="javascript:void(0);"><?php echo $val["team_title"];?></a></p>
    <p><?php echo $val["designation"];?></p>
    </div>
    </div>
  </li> 
  
  <?php
 }
}
?>