<?php
class Team extends Public_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model(array('team/team_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->page_section_ct = 'team';
	}

	public function index(){

		 $this->page_section_ct = 'team'; 
		// $this->config->set_item('per_page','2');
		$record_per_page        = (int) $this->input->post('per_page');
		$offset = (int) $this->input->post('offset');
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$base_url      =  "team";
		$param = array('status'=>'1');
		$res_array              = $this->team_model->get_team($config['per_page'],$offset,$param);
		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct'] = $total_rows;
		$data['record_per_page'] = $config['per_page'];
		$data['title'] = 'Blogs';
		$data['res'] = $res_array;
		$data['base_url'] = $base_url;
		$data['frm_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		if($ajx_req===TRUE){
		  $this->load->view('team/load_team_view',$data);
		}
		else{
		  $this->load->view('team/view_team',$data);
		}
	}		

	public function details(){

		$this->page_section_ct = 'team';
		//$id = (int) $this->uri->rsegment(3);
		$id= (int) $this->meta_info['entity_id'];
		$param     = array('wl_team.status'=>'1','where'=>"wl_team.team_id ='$id' ");
		$res       = $this->team_model->get_team(1,0,$param);
		if(is_array($res) && !empty($res)){

			$data['title'] = 'Blogs';
		    $data['res'] = $res;
		    $this->load->view('team/team_details_view',$data);	
		}else{
			redirect('team', ''); 

		}

	}

	public function download_pdf(){
		
	  $id = (int) $this->uri->segment(3);
	  $param     = array('status'=>'1','where'=>"team_id ='$id' ");
	  $res_topic       = $this->team_model->get_team(1,0,$param);	

	  if(is_array($res_topic) && !empty($res_topic)){

		if($res_topic['team_pdf']!='' && file_exists(UPLOAD_DIR."/team/pdf/".$res_topic['team_pdf']))
		{
			$this->load->helper('download');
			$data = file_get_contents(UPLOAD_DIR."/team/pdf/".$res_topic['team_pdf']);
			$name = $res_topic['team_pdf'];
			force_download($name, $data); 
		}
	  }
	}


}

/* End of file pages.php */



