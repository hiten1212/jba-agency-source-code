<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php $segment=3; if($cat_id){ echo category_breadcrumbs($cat_id,$segment); }else{ echo navigation_breadcrumb("Categories"); }
$cat_name="Category";
if($cat_id){
	$cat_name=ucwords($parentres['category_name']);
}?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>

<div class="container">
<div class="mid_area">
<h1><?php echo $cat_name;?></h1>
<div class="w-100 mt-3 mb-4">
<div class="catgory_listing">


<?php
if(is_array($res) && !empty($res) ){
	echo form_open("",'id="myform" method="post" ');?>
  <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
     <input type="hidden" name="offset" value="0">
     <?php echo form_close();?>
	 
<ul  id="prodListingContainer">
<?php $data = array('res'=>$res);
      $this->load->view('category/category_data',$data);?>
</ul>
<?php if($totalProduct > $record_per_page){?>
<div class="mt-3 text-center" id="loadingdiv"><img src="<?php echo theme_url() ?>images/loader.gif" alt=""></div>
<?php }?>
<div class="clearfix"></div>
<?php
	}else{
	   ?>
	   <div class="clearfix"></div>
	   <p class="mt-3 text-center"><strong><?php echo $this->config->item('no_record_found');?></strong></p>
	   <?php
	}?>
    
    <?php if(is_array($parentres) && !empty($parentres)){?>
<div class="fs13 text-center mt-2"><?php echo $parentres['category_description'];?></div>
<?php }?>

</div>
</div>
</div>
</div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>