<?php
if(is_array($res) && !empty($res) ){
	$counter=0;
	foreach($res as $val)
	{
		$link_url = base_url().$val['friendly_url'];
		$cateImage=get_image('category',$val["category_image"],'286','201');
		?>	
        
        <li class="listpager">
<div class="cat_out_sect">
<div class="cat_area">
<figure><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><img src="<?php echo $cateImage;?>" alt="<?php echo $val['category_alt'];?>" class="img-fluid"/></a></figure>
</div>
<div class="cat_txt_area">
<a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a>
</div>
</div>
</li>	   
		<?php
	}
}?>