<?php
      $condtion_array = array(
      'field' =>"category_id, category_name,category_image,category_alt, friendly_url",
      'condition'=>"AND parent_id = '0' AND status='1' AND is_featured='1'",
      'order'=>'sort_order',
      'debug'=>FALSE
      );

      $condtion_array['offset'] = 0;
      $condtion_array['limit'] = 9;
      $level_0_res = $this->category_model->getcategory($condtion_array);
      $total_categories = $this->category_model->total_rec_found;
      if(is_array($level_0_res) && !empty($level_0_res)){?>
<div class="categories_area">
  <div class="container">
    <p class="heading">Our Activities</p>
    <div id="owl-categories" class="owl-carousel owl-theme mt-3">	
	<?php	
	foreach($level_0_res as $val)
	{
		$link_url = base_url().$val['friendly_url'];
		$cateImage=get_image('category',$val["category_image"],'285','299');
		?>
      <div class="item">
        <div class="cate_box">
          <div class="cate_pic"><span><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><img src="<?php echo $cateImage;?>" alt="<?php echo $val['category_alt'];?>"></a></span></div>
          <div class="cate_name cat-bg1">
            <p><a href="<?php echo $link_url;?>" title="<?php echo $val['category_name'];?>"><?php echo $val['category_name'];?></a></p>
          </div>
        </div>
      </div>
      <?php }?>
      
    </div>
    <p class="mt-4 text-center"><a href="<?php echo site_url('category');?>" class="viewmore_btn">See More Activities</a></p>
  </div>
</div>
<?php }?>