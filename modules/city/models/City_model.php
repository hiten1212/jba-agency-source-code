<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class City_model extends MY_Model


{





	public function __construct()


	{


		parent::__construct();


	}


	


	public function get_citys($param=array(),$offset='0',$limit='100')


	 {


		$status			    =   @$param['status'];


		$is_featured			    =   @$param['is_featured'];


		$orderby		    =   @$param['orderby'];	


		$where		        =   @$param['where'];


		$keyword       		=   @$param['keyword'];


		


		if($status!='')


		{


			$this->db->where("status",$status);


		}


		if($is_featured!='')


		{


			$this->db->where("is_featured",$is_featured);


		}


		


		if($where!='')


		{


			$this->db->where($where);


		}


		


		if($orderby!='')


		{


			 $this->db->order_by($orderby);


			


		}else


		{


		  $this->db->order_by('city_name','asc');


		}


		


		$this->db->limit($limit,$offset);


		$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE);		


		$this->db->from('wl_city');


		$q=$this->db->get();


		//echo_sql();


		$result = $q->result_array();	


		$result = ($limit=='1') ? @$result[0]: $result;	


		return $result;


	}


	


	public function get_city_by_id($id)


	{


		$id = applyFilter('NUMERIC_GT_ZERO',$id);


		


		if($id>0)


		{


			$condtion = "status !='2' AND city_id=$id";


			$fetch_config = array(


														'condition'=>$condtion,							 					 


														'debug'=>FALSE,


														'return_type'=>"array"							  


													 );


			$result = $this->find('wl_city',$fetch_config);


			return $result;


		}


	}


}


// model end here