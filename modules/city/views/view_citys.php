<?php $this->load->view("top_application");
echo navigation_breadcrumb("Partnership");?>

<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>

<div class="container mid_area">
<h1>Partnership</h1>
<?php
			if(is_array($res) && !empty($res) ){
			  echo form_open("",'id="myform" method="post" ');?>
			  <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
			  <input type="hidden" name="offset" value="0">
			  <?php echo form_close(); ?>
        
<ul class="client-logo">
     <?php $data = array('res'=>$res);
         $this->load->view('city/city_data',$data);?>    
      
</ul>
<?php if($totalProduct > $record_per_page){?>
<p class="text-center" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p><?php } ?>
 <?php   
			}else{
			  ?>
			  <div class="mt-3 text-center"><strong>No record found !</strong></div>
			  <?php
			}?> 
</div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>