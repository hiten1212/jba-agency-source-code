<?php
if ( ! function_exists('get_root_level_city_dropdown'))
{
	function get_root_level_city_dropdown($parent,$selectId="")
	{
		$ci = CI();
		$selId =( $selectId!="" ) ? $selectId : "";
		$var="";
		echo $sql="SELECT * FROM wl_city WHERE  status='1' ORDER BY city_name ASC ";
		$query=$ci->db->query($sql);
		$num_rows     =  $query->num_rows();
		
		if ($num_rows > 0  )
		{
			foreach( $query->result_array() as $row )
			{
				$city_name=ucwords(strtolower($row['city_name']));
				$sel=( $selectId==$row['city_id'] ) ? "selected='selected'" : "";
				$var .= '<option value="'.$row['city_id'].'" '.$sel.'>'.$city_name.'  </option>';
			}
		}
		return $var;
	}
}

