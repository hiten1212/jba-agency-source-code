<?php
class Gallery extends Public_Controller
{

	
	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('gallery/gallery_model'));
                 $this->load->model(array('pages/pages_model'));
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");
		$this->load->helper(array('query_string', 'scroll_pagination/scroll_pagination'));
		//$this->load->helper(array('category/category','products/product'));	 
	}

	public function index()
	{	
		$record_per_page         = (int) $this->input->post('per_page') ? $this->input->post('per_page'): $this->config->item('per_page');
		
		$parent_segment          = (int) $this->uri->segment(3);		
		$page_segment            = find_paging_segment();		
		$config['per_page']		 = 1000;//( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');		
		$offset                 =  (int) $this->uri->segment($page_segment,0);			
		$base_url               = "gallery/index/pg/";

		$portfolioId = $this->session->userdata('portfolioId');

		$portfolioParam = $this->session->userdata('portfolioParam');


		if($portfolioId!='')
		{
			$param = array('status'=>'1','type'=>'1','parent_id'=>$portfolioId,'orderby'=>'sort_order asc','type' => array(1,2));
		}
		else
		{
		  $param = array('status'=>'1','type'=>'1','parent_id_not'=>'0','orderby'=>'sort_order asc','type' => array(1,2));
		  
		}

		/*$param = array('status'=>'1','type'=>'1','parent_id_not'=>'0','orderby'=>'sort_order asc','type' => array(1,2));*/
		/*$param = array('status'=>'1','type'=>'1','parent_id'=>'1','orderby'=>'sort_order asc','type' => array(1,2));*/

		$res_array = $this->gallery_model->get_all_gallery($config['per_page'],$offset,$param);



                 foreach($res_array as &$resarray){
                    $checkparent  = $this->gallery_model->parentstatus($resarray['parent_id']);
                    
                    $resarray['parent_status'] = $checkparent['status'];
                }
//echo_sql();		
		$config['total_rows'] = $data['total_rec'] = get_found_rows();
		
		$data['scroll_pagination']    = scroll_pagination($base_url,$config['total_rows'],$config['per_page'],$config['per_page']);			
	    //$data['page_links']      = front_pagination("$base_url",$config['total_rows'],$config['per_page'],$page_segment);	
		$data['record_per_page'] = $record_per_page;		
	    $data['title'] = "Photo Albums";
		$data['res'] = $res_array;
		$data['portfolioId'] = $portfolioId;
		$data['portfolioParam'] = $portfolioParam;

                $allcms_pages = $this->pages_model->get_allcmspages();
                $data['allcms_pages']       = $allcms_pages;  					
		$this->load->view('gallery/view_gallery',$data);
		
	}

	public function getphoto()
	{
	 $photoId = $this->input->post('photoId');

	 $this->session->set_userdata('photoId', $photoId);
	}

	public function category_portfolio()
	{
	  $portfolioId = $this->input->post('portfolioid');

	  $this->session->set_userdata('portfolioId', $portfolioId);

	  $data = array(
       'confirm'  => 1,
      );
    
      echo json_encode($data);
	}

	public function unset_category_portfolio()
	{
	  $portfolioId = $this->input->post('portfolioid');

	  $this->session->set_userdata('portfolioId');

	  $data = array(
       'confirm'  => 1,
      );
    
      echo json_encode($data);
	}

	public function unset_category_portfolio_old()
	{
	  $portfolioId = $this->input->post('portfolioid');

	  $this->session->unset_userdata('portfolioId');

	  $this->session->set_userdata('portfolioParam', $portfolioId);

	  $data = array(
       'confirm'  => 1,
      );
    
      echo json_encode($data);
	}
	
	
	public function details()
	{
		$id     = (int) $this->meta_info['entity_id'];

		$photo_id = $this->session->userdata('photoId');


		//$id =  (int) $this->uri->segment(3);	
		$res               = $this->gallery_model->get_gallery_by_id($id);

		
		$record_per_page         = (int) $this->input->post('per_page') ? $this->input->post('per_page'): $this->config->item('per_page');
		
		if(array_key_exists('entity_id',$this->meta_info) && $this->meta_info['entity_id'] > 0 )
		{
			$parent_segment         = (int) $this->meta_info['entity_id'];
		}
		else
		{
			$parent_segment         = (int) $this->uri->segment(3);
		}


		
		$page_segment           = find_paging_segment();		
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');		
		$offset                 =  (int) $this->uri->segment($page_segment,0);	
				
		$parent_id              = ( $parent_segment > 0 ) ?  $parent_segment : '0';
		$base_url               = ( $parent_segment > 0 ) ?  "gallery/details/$parent_id/pg/" : "gallery/details/pg/";

		/*$param = array('status'=>'1','parent_id'=>$parent_id,'gallery_id'=>$photo_id,'orderby'=>'sort_order asc');*/

		$param = array('status'=>'1','parent_id'=>$parent_id,'orderby'=>'sort_order asc');


		/*$param = array('status'=>'1','parent_id'=>$parent_id,'orderby'=>'sort_order asc');	*/			
		$res_array = $this->gallery_model->get_gallery($config['per_page'],$offset,$param);

		$selected_index = array_search($photo_id, array_column($res_array, 'gallery_id'));

		if($selected_index==0)
		{
		  $gallery_res_array = $res_array;		
		}
		else
		{
		  if($res_array_off = array_search($selected_index, array_keys($res_array))) 
		  {
		    $gallery_res_array = array_merge(array_slice($res_array, $res_array_off, null, true), array_slice($res_array, 0, $res_array_off, true));
		  }
		}

		$config['total_rows'] = $data['total_rec'] = get_found_rows();	
		
		$data['scroll_pagination']    = scroll_pagination($base_url,$config['total_rows'],$config['per_page'],$config['per_page']);		
	    //$data['page_links']      = front_pagination("$base_url",$config['total_rows'],$config['per_page'],$page_segment);
	
				
		$data['record_per_page'] = $record_per_page;
		$data['gallery_title']=$res['gallery_title'];
		$data['description']=$res['description'];
		$data['res'] = $gallery_res_array;
		$allcms_pages = $this->pages_model->get_allcmspages();
		$data['allcms_pages']       = $allcms_pages;

		$this->load->view('gallery/view_gallery_details',$data);

	}
	
}
?>