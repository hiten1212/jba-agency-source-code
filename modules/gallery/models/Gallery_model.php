<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gallery_model extends MY_Model
 {

		 
	 public function get_gallery($limit='10',$offset='0',$param=array())
	 {		
		
		$status			    =   @$param['status'];	
		$type			    =   @$param['type'];	
		$parent_id			=   @$param['parent_id'];
		$gallery_id			=   @$param['gallery_id'];
		$parent_id_not			=   @$param['parent_id_not'];	
		$id			        =   @$param['id'];	
		$orderby			=	@$param['orderby'];
		$keyword            =	trim($this->input->get_post('keyword',TRUE));	
		$keyword            =   $this->db->escape_str($keyword);

			
	    if($status!='')
		{
			$this->db->where("status","$status");
		}
		 if($type!='')
		{
			$this->db->where("type","$type");
		}
		 if($id!='')
		{
			$this->db->where("gallery_id","$id");
		}
		if($parent_id_not!=''){
		$this->db->where(array('parent_id !='=> $parent_id_not));
			}
			else{
		if($parent_id!='')
		{
			$this->db->where("parent_id","$parent_id");
		}
		else
		{
			$this->db->where("parent_id",0);
		}
		}

		if($status!='')
		{
			$this->db->where("status","$status");
		}

		if($gallery_id!='')
		{
			$this->db->like('gallery_id',$gallery_id);
		}
		
		if($orderby!='')
		{
			$this->db->order_by($orderby);

		}
		else
		{
			$this->db->order_by('gallery_id',"DESC");
		}
		
		if($limit!="")
		$this->db->limit($limit,$offset);
	
		$this->db->select('SQL_CALC_FOUND_ROWS*',FALSE);
		$this->db->from('wl_gallery');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
				
	}

         public function get_all_gallery($limit='10',$offset='0',$param=array())
	 {
		
		$status			    =   @$param['status'];	
		$type			    =   @$param['type'];	
		$parent_id			=   @$param['parent_id'];
		$parent_id_not	    =   @$param['parent_id_not'];	
		$id			        =   @$param['id'];	
		$orderby			=	@$param['orderby'];
		$keyword            =	trim($this->input->get_post('keyword',TRUE));	
		$keyword            =   $this->db->escape_str($keyword);
        $type               =   @$param['type'];
               
        if(!empty($type))
		{ 
			$this->db->where_in("type",$type);
		}
			
	    if($status!='')
		{
			$this->db->where("status","$status");
		}
		 if($id!='')
		{
			$this->db->where("gallery_id","$id");
		}
		if($parent_id_not!=''){
		$this->db->where(array('parent_id !='=> $parent_id_not));
			}
			else{
		 if($parent_id!='')
		{
			$this->db->where("parent_id","$parent_id");
		}else{
			$this->db->where("parent_id",0);
		}
		}
		if($keyword!='')
		{
			$this->db->like('gallery_title',"$keyword");
			
		}
		
		if($orderby!='')
		{
			$this->db->order_by($orderby);

		}
		else
		{
			$this->db->order_by('gallery_id',"DESC");
		}
		
		if($limit!="")
		$this->db->limit($limit,$offset);
	
		$this->db->select('SQL_CALC_FOUND_ROWS*',FALSE);
		$this->db->from('wl_gallery');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->result_array();	
		$result = ($limit=='1') ? @$result[0]: $result;	
		return $result;
				
	}	  
		  
	
	public function get_gallery_by_id($id)
	{
		$id = applyFilter('NUMERIC_GT_ZERO',$id);
		
		if($id>0)
		{
			$condtion = "status !='2' AND gallery_id=$id";
			$fetch_config = array(
														'condition'=>$condtion,							 					 
														'debug'=>FALSE,
														'return_type'=>"array"							  
													 );
			$result = $this->find('wl_gallery',$fetch_config);
			return $result;
		}
	}

            public function parentstatus($parent_id){
                $this->db->where('gallery_id',$parent_id);
                $this->db->select('status',FALSE);
		$this->db->from('wl_gallery');
		$q=$this->db->get();
		//echo_sql();
		$result = $q->row_array();
		return $result;
        }
	 
}
?>