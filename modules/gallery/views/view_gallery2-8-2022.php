<?php $this->load->view('top_application');?>
<script type="text/javascript">function serialize_form(){ return $('#myform').serialize(); }</script>
<?php echo navigation_breadcrumb("Portfolio");?>
<!-- Breadcrum Starts-->
<!-- Inner Content-->
<!-- MIDDLE STARTS -->
<div class="innerlogo"><img src="<?php echo theme_url();?>images/logo.png"></div>




<div class="details_portfolio">
    <div class="container">
        <div class="row">
            <div  class="col-md-12 col-xs-12 col-sm-12">
                <div class="portfolio_div"  style="background: <?php echo $this->admin_info->portfolio_list_background!='' ? $this->admin_info->portfolio_list_background:'#000';  ?> !important">
                   
                    <div class="portfolio-content"> 
                        <ul class="portfolio-categ filter">
						<li class="all"><a href="#" >All</a></li>
						<?php  $condtion_array = array(
   'field' =>"gallery_id,gallery_title,parent_id,friendly_url,(SELECT COUNT(gallery_id) FROM wl_gallery AS b WHERE b.parent_id=a.gallery_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'orderby'=>'sort_order',
   'debug'=>FALSE
   );
   $offset = 0;
   $limit = 100;
   
   $param = array('status'=>'1','parent_id'=>'0','orderby'=>'sort_order asc');
	$res_products               = $this->gallery_model->get_gallery($limit,$offset,$param);  
  //echo_sql();
   $total_page_products	= get_found_rows();
   if($total_page_products > 0){
?>

<?php $i=1;
	foreach($res_products as $val){
	 $link_url = site_url($val['friendly_url']);
	 //$link_url = site_url().'#'.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
  <li class="cat-item-<?php echo $val['gallery_id'];?>"><a  href="#" title="<?php echo $val['gallery_title'];?>"><?php echo $val['gallery_title'];?></a></li>
<?php $i++;}
if($total_page_products >6)
{
?>
<?php /*<li class="cat-item-7"><a  href="<?php echo base_url('gallery');?>" title="View All &raquo;"><strong>View More</strong></a></li>*/?>
<?php } ?>
                          
                           
   <?php } ?>
                          </ul>
                        <ul class="portfolio-area row"> 
						<?php 
							foreach($res as $val){
          // $cls = ( $ix==0 )	 ? "cat-w fl" : "cat-w fl ml10" ;				
        $id=$val['gallery_id'];
		$parent_id=$val['parent_id'];
        $total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
		$friendly_url = get_db_field_value('wl_gallery','friendly_url'," AND `gallery_id` = '$parent_id' AND status='1'");
		
		
        $link_url = base_url().$friendly_url;		   
        ?>
                           <li class="portfolio_item portfolio-item2 col-md-4 col-sm-4 col-xs-6" data-id="id-<?php echo $val['parent_id']-1;?>" data-type="cat-item-<?php echo $val['parent_id'];?>"> 
                              <div class="image-block">
                                    <a class="image-zoom" href="<?php echo $link_url;?>" title="">
                                        <img src="<?php echo get_image('gallery',$val['gallery_image'],'234','178','R'); ?>" /> 
                                    </a>
                                </div>
                            </li>
							<?php } ?>
                            

                         
                            
                        </ul>
                
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MIDDLE ENDS -->
<script>
jQuery(document).ready(function(e) {
  jQuery('[id ^="per_page"]').live('change',function(){
		$("[id ^='per_page'] option[value=" + jQuery(this).val() + "]").attr('selected', 'selected'); jQuery('#myform').submit();
	});
});
</script>
<?php $this->load->view('project_footer');
// echo $this->uri->rsegment(1);
?>
<?php //echo front_record_per_page('per_page1', 'per_page', 'myform');;?>
<script src="<?php echo theme_url(); ?>js/jquery.min1.js"></script>
<script src="<?php echo theme_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo theme_url(); ?>js/owl.carousel.min.js"></script>


<script src="<?php echo theme_url(); ?>js/main.js"></script>


<script src="<?php echo theme_url(); ?>js/jquery.quicksand.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.easing.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/script.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.prettyphoto.js" type="text/javascript"></script> 


<script>



function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}



</script>

<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>

<script src="<?php echo resource_url();?>Scripts/script.int.dg.js"></script>
</body>
</html>

