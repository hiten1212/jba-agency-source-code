<?php $this->load->view("top_application");?>
<?php //$this->load->view('banner/inner_page_top_banner');?>
<?php 
$seg = $this->uri->segment(1);
if($seg=='interpon' || $seg=='axalta' || $seg=='dulux' || $seg=='oxytech')
{
	echo navigation_breadcrumb($content['page_name'],array('Case Studies'=>'case-studies'));
}
else
{
	echo navigation_breadcrumb($gallery_title,array('Portfolio'=>'gallery'));
	//echo navigation_breadcrumb($gallery_title);
}
?>
<!-- MIDDLE STARTS -->

<div class="innerlogo"><img src="<?php echo theme_url();?>images/logo.png"></div>



<div class="details_portfolio">
    <div class="container">
        <div class="row">
            <div  class="col-md-12 col-xs-12 col-sm-12" style="background: <?php echo $this->admin_info->portfolio_detail_background!='' ? $this->admin_info->portfolio_detail_background:'#000';  ?> !important">
          <?php /*<div class="portfolio_div"   style="background: <?php echo $this->admin_info->portfolio_detail_background!='' ? $this->admin_info->portfolio_detail_background:'#000';  ?> !important">*/?>    
<?php
     if(is_array($res) && !empty($res)){
         echo form_open('',array("name"=>"myform","id"=>"myform"));?>
                <div class="portfolio_div" >
                 
     
                    <div class="innerslider">
                    
   
   <?php   foreach($res as $val){ ?>
                        <div>
                            
                            <img src="<?php echo get_image('gallery',$val['gallery_image'],'1020','500','R'); ?>">
                            
                            
                            <div class="details_portfolio">
                                <h3 style="margin-bottom: 0px;"><?php echo $val['gallery_title'];?></h3><br>
                                <div><?php echo $val['description'];?></div>
                            </div>
                        </div>
<?php }?>
                        
     
                        
                    </div>
                </div>
 <?php }?>
<!--</div>-->

            </div>
        </div>
    </div>
</div>
<!-- MIDDLE ENDS -->
<?php $this->load->view("bottom_application");?>