<?php
$gallery=$this->db->query("select * from wl_gallery where status='1' and parent_id='0' order by rand()")->result_array();
$gallery_total=$this->db->query("select * from wl_gallery where status='1' and parent_id='0'")->num_rows();
?>
<?php
if($gallery_total>0)
{			
?><!--Our Gallery1-->
<div class="gallery_area">
<div class="container">
<p class="heading white text-center">Our Gallery</p>
<div id="owl-gallery" class="owl-carousel owl-theme mt-1">
<?php 	$feat_counter = 0;
		foreach($gallery as $galleryval){
			?>
<div class="item">
<div class="gal_pic">
<p class="gal_expand"><a href="<?php echo get_image('gallery',$galleryval['gallery_image'],'368','368','R'); ?>" data-fancybox="images" title="<?php echo $galleryval['gallery_title'];?>"><i class="fas fa-expand"></i></a></p>
<span><img src="<?php echo get_image('gallery',$galleryval['gallery_image'],'220','220','R'); ?>" alt="<?php echo $galleryval['gallery_title'];?>"></span>
</div>
</div>
<?php }?>

</div>
<p class="mt-2 text-center"><a href="<?php echo site_url('gallery');?>" class="readmore" title="View All">View All</a></p>
</div>
</div>
<!--Our Gallery End-->
<?php }?>
