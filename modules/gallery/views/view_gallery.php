
<?php $this->load->view('top_application');?>
<script type="text/javascript">
function serialize_form(){ return $('#myform').serialize(); }
</script>
<?php echo navigation_breadcrumb("Portfolio");?>
<!-- Breadcrum Starts-->
<!-- Inner Content-->
<!-- MIDDLE STARTS -->
<div class="innerlogo"  style="background: <?php echo $this->admin_info->logo_background!='' ? $this->admin_info->logo_background:'';  ?>"><img src="<?php echo $this->admin_info->header_logo!='' ? base_url().'uploaded_files/logo/'.$this->admin_info->header_logo:theme_url()."images/logo.png" ?>"></div>


<div class="details_portfolio">
    <div class="container">
        <div class="row">
            <div  class="col-md-12 col-xs-12 col-sm-12">
                <div class="portfolio_div"  style="background: <?php echo $this->admin_info->portfolio_list_background!='' ? $this->admin_info->portfolio_list_background:'#000';  ?> !important">
                   
                    <div class="portfolio-content"> 
                        <ul class="portfolio-categ filter">
            <?php
              if($portfolioId=='')
              {
                $all_active_class = 'active';
              }
              else
              {
                $all_active_class = '';
              }
            ?>
						<li class="all <?php echo $all_active_class; ?>"><a href="#" class="ctg_glry_tile_sub_list" data-portfolioid="">All</a></li>
						<?php  $condtion_array = array(
   'field' =>"gallery_id,gallery_title,parent_id,friendly_url,(SELECT COUNT(gallery_id) FROM wl_gallery AS b WHERE b.parent_id=a.gallery_id ) AS total_subcategories",
   'condition'=>"AND parent_id = '0' AND status='1'",
   'orderby'=>'sort_order',
   'debug'=>FALSE
   );
   $offset = 0;
   $limit = 100;
   
   $param = array('status'=>'1','parent_id'=>'0','orderby'=>'sort_order asc');
	$res_products = $this->gallery_model->get_gallery($limit,$offset,$param);
  //echo_sql();
   $total_page_products	= get_found_rows();
            // echo "<pre>";
            // print_r($total_page_products);
            // exit;
   if($total_page_products > 0){
              // echo $this->db->last_query();
              // exit;
?>


<?php $i=1;
	foreach($res_products as $val){
	 $link_url = site_url($val['friendly_url']);
	 //$link_url = site_url().'#'.$val['friendly_url'];
	 $total_subcategories = $val['total_subcategories'];
	 ?>
   <?php 
   if($portfolioId=='')
   {
     ?>
       <li class="cat-item-<?php echo $val['gallery_id'];?>"><a href="javscript:void(0);" title="<?php echo $val['gallery_title'];?>" data-portfolioid="<?php echo $val['gallery_id'];?>"><?php echo $val['gallery_title'];?></a></li>
     <?php
   }
   else
   {
    if($portfolioId == $val['gallery_id'])
    {
      $active_class = 'active';
    }
    else
    {
      $active_class = '';
    }
    ?>
     <li class="cat-item-<?php echo $val['gallery_id'];?> <?php echo $active_class; ?>"><a href="javscript:void(0);" class="ctg_glry_tile_sub_list" title="<?php echo $val['gallery_title'];?>" data-portfolioid="<?php echo $val['gallery_id'];?>"><?php echo $val['gallery_title'];?></a></li>
    <?php
   }
   ?>
<?php $i++;}
if($total_page_products >6)
{
?>
<?php /*<li class="cat-item-7"><a  href="<?php echo base_url('gallery');?>" title="View All &raquo;"><strong>View More</strong></a></li>*/?>
<?php } ?>
                          
                           
   <?php } ?>
                          </ul>

                          
                        <ul class="portfolio-area row"> 
						<?php 
							foreach($res as $val){
          // $cls = ( $ix==0 )	 ? "cat-w fl" : "cat-w fl ml10" ;				
        $id=$val['gallery_id'];
		$parent_id=$val['parent_id'];
        $total_photo=count_record('wl_gallery',"`status` = '1' AND `parent_id` = '$id'");
		$friendly_url = get_db_field_value('wl_gallery','friendly_url'," AND `gallery_id` = '$parent_id' AND status='1'");
		
		
        $link_url = base_url().$friendly_url;		   
        ?>
				<?php if($val['parent_status'] != 0) {?>
                           <li class="portfolio_item portfolio-item2 col-md-4 col-sm-6 col-xs-12" data-id="id-<?php echo $val['parent_id']-1;?>" data-type="cat-item-<?php echo $val['parent_id'];?>" data-photoId="<?php echo $id; ?>">
                              <div class="image-block">
                                    <a class="image-zoom" href="<?php echo $link_url;?>" title="">
                                        <?php if ($val['type'] ==1) { ?>
                                                <img src="<?php echo get_image('gallery', $val['gallery_image'], '', '', 'R'); ?>" />
                                                <?php } else if($val['type'] ==2) {?>
                                                <img src="<?php echo get_image('gallery', $val['video_thumbnail'], '', '', 'R'); ?>" />
                                                <?php } ?>
                                    </a>
                                </div>
                            </li>
				<?php } ?>

							<?php } ?>
                            

                         
                            
                        </ul>
                
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MIDDLE ENDS -->


<?php $this->load->view('project_footer');
// echo $this->uri->rsegment(1);
?>
<?php //echo front_record_per_page('per_page1', 'per_page', 'myform');;?>

<script>
jQuery(document).ready(function(e) {
  jQuery('[id ^="per_page"]').live('change',function(){

    $("[id ^='per_page'] option[value=" + jQuery(this).val() + "]").attr('selected', 'selected'); jQuery('#myform').submit();
  });
});
</script>


<script src="<?php echo theme_url(); ?>js/jquery.min1.js"></script>
<!-- <script src="<?php //echo theme_url(); ?>js/jquery.min.js"></script> -->
<script src="<?php echo theme_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo theme_url(); ?>js/owl.carousel.min.js"></script>


<!-- <script src="<?php //echo base_url(); ?>js/common.js"></script> -->

<script src="<?php echo theme_url(); ?>js/main.js"></script>

<script src="<?php echo theme_url(); ?>js/jquery.quicksand.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.easing.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/script.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/jquery.prettyphoto.js" type="text/javascript"></script> 


<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>

<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}


jQuery('.start_prjct_class').live('click',function(){
 jQuery('#myModal').modal('show');
});

</script>

<script src="<?php echo resource_url();?>Scripts/script.int.dg.js"></script>
</body>
</html>

