<?php
class Broucher extends Public_Controller{

	public function __construct(){
		
		parent::__construct();
		$this->load->model(array('broucher/broucher_model'));
	}

	public function index(){

		$record_per_page         = (int) $this->input->post('per_page');
		$page_segment            =  find_paging_segment();
		$config['per_page']	     =  ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		$offset                 = (int) $this->input->post('offset');
		$base_url                =   "broucher/index/pg/";
		$param= array('status'=>'1');
		$res_array              = $this->broucher_model->get_brouchers($param,$offset,$config['per_page']);
		//echo_sql();
		$config['total_rows'] = $data['totalProduct']	= get_found_rows();
		//$data['page_links']      = front_pagination($base_url,$config['total_rows'],$config['per_page'],$page_segment);

		$data['record_per_page'] = $config['per_page'];
		$data['res'] = $res_array;
		$data['frm_url'] = $base_url;

		if($this->input->is_ajax_request()){
			$this->load->view('broucher/broucher_data',$data);
		}
		else{
			$this->load->view('broucher/view_brouchers',$data);
		}

	}
	
	
	public function download_pdf(){
		
		$pId=(int)$this->uri->segment(3,0);		
		if($pId > 0){			
			$pdf_name='broucher_doc';
			$file=get_db_field_value('wl_brouchers', $pdf_name, array("broucher_id"=>$pId));
			if($file !="" && @file_exists(UPLOAD_DIR."/brouchers/".$file)){
				$this->load->helper('download');
				$data = file_get_contents(UPLOAD_DIR."/brouchers/".$file);
				$name = $file;
				force_download($name, $data);
			}else{
				redirect('broucher', '');
			}
		}else{
			redirect('broucher', '');
		}
	}



}