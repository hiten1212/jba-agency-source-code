<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb("Download PDF");?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize(); }</script>

<div class="container">
<div class="mid_area">
<h1>Download PDF</h1>
<div class="container">
  <div class="app_container">
              <div class="d-none bg-1 d-sm-none d-md-block b ttu gray fs14 bt">
                <div class="row">
                  <div class="col-12 col-md-1"> S.No. </div>
                  <div class="col-12 col-md-6"> PDF Details </div>
                </div>
              </div>
              <!-- row 1 -->
              <?php
  if(is_array($res) && !empty($res) ){
	  echo form_open("",'id="myform" method="post" ');?>
	  <input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page');?>">
	  <input type="hidden" name="offset" value="0">
	  <?php echo form_close();?>

 <div  id="prodListingContainer">
             <?php $data = array('res'=>$res);
	     			$this->load->view('broucher/broucher_data.php',$data);?>
              </div>
			  <?php if($totalProduct > $record_per_page){?>
<div class="ext-center mt-4" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></div>
<?php }?> 
              
           
	<?php
  }else{
	  ?>
	  <div class="b mt15 text-center"><strong>No record found !</strong></div>
	  <?php
  }?>
              
              
            </div>
</div>

</div>
</div>

<div class="clearfix"></div>

<?php $this->load->view("ajax_paging");
$this->load->view("bottom_application");?>