<?php
class Careers extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('careers/careers_model'));
		$this->load->library(array('Dmailer'));
		$this->form_validation->set_error_delimiters("<div class='required red fs12'>","</div>");
	}

	public function index()
	{
		$this->page_section_ct  = 'product';
		$offset                 = (int) $this->input->post('offset');
		$curr_symbol            = display_symbol();
		$posted_data            = is_array($this->input->post()) ? array_filter($this->input->post()) : array();

		if($this->input->post()){
			$posted_data= array_filter($this->input->post());
			unset($posted_data['submit']);
		}

		$condition               = array();
		$cat_res = '';
		//$record_per_page        = (int) $this->input->post('per_page');
		$record_per_page        = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');

		$category_id 			= $this->input->post('category_id') ? (int) $this->input->post('category_id') : $this->uri->rsegment(3);

		$page_segment           = find_paging_segment();

		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');

		$base_url      = ( $category_id!='' ) ?   "careers/index/$category_id/pg/" : "careers/index/pg/";
		
		$page_title              = "Career";	
		$where                   = "wlj.status ='1' ";
		
		$keyword			=   trim($this->input->post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);

		
		if($keyword){
			$condition['keyword'] = $keyword;
			$where.=" AND (wlj.job_title LIKE '%".$keyword."%' OR  wlj.job_code LIKE '%".$keyword."%' )";
		}
				
		$condition['where'] = $where;		
		$res_array               =  $this->careers_model->get_careers($config['per_page'],$offset,$condition);		
		$config['total_rows'] = $data['totalProduct']	=  get_found_rows();

	  	$data['frm_url'] = $base_url;
		
		$data['record_per_page'] = $record_per_page;
		$data['heading_title'] = $page_title;
		$data['res']           = $res_array;
		$data['cat_res'] = $cat_res;
		
		//----------------------------Send Enquiry---------------------------------
			if($this->input->post('action')=='Career') {				
				$this->job_apply();
			}
			#-----------------------------Send Enquiry----------------------#

		if($this->input->is_ajax_request())
		{
			$this->load->view('careers/careers_data',$data);
		}
		else
		{
			$this->load->view('careers/view_careers_listing',$data);
		}

	}	

	public function detail()
	{		
		$this->meta_info['entity_id'];
		$data['unq_section'] = "Career";
		$jobid = (int) $this->meta_info['entity_id'];
		$option = array('jobid'=>$jobid,'status'=>1);
		$res =  $this->careers_model->get_careers(1,0,$option);
				
		if(is_array($res)&& !empty($res) ){
			$data['res']       = $res;			
			$this->load->view('careers/view_careers_details',$data);
		}else
		{
			redirect('careers', '');
		}

	}
	
	public function job_apply(){

		$this->form_validation->set_rules('first_name1','First Name','trim|alpha|required|max_length[30]');	
		$this->form_validation->set_rules('last_name1','Last Name','trim|alpha|max_length[30]');		
		$this->form_validation->set_rules('mobile_number1','Mobile Number','trim|required|numeric|min_length[10]|max_length[15]');
		$this->form_validation->set_rules('email1','Email','trim|required|valid_email|max_length[80]');		
				
		//$this->form_validation->set_rules('message1','Message','trim|max_length[8500]');
		$this->form_validation->set_rules('resume','Resume',"file_required|file_allowed_type[document]");
		//$this->form_validation->set_rules('verification_code1','Verification code','trim|required|valid_captcha_code');
		$data['page_error'] = "";
		
		$job_id    = $this->input->post('jId'); 
		
		if($this->form_validation->run()==TRUE){			
			
			$uploaded_file = "";
			if( !empty($_FILES) && $_FILES['resume']['name']!='' ){

				$this->load->library('upload');
				$uploaded_data =  $this->upload->my_upload('resume','resumes');
				if( is_array($uploaded_data)  && !empty($uploaded_data) ){
					$uploaded_file = $uploaded_data['upload_data']['file_name'];
					$filepath = UPLOAD_DIR.'/resume/'.$uploaded_file;
				}
			}

			$posted_data=array(
			
			'first_name'    => $this->input->post('first_name1'),
			'last_name'    => $this->input->post('last_name1'),				
			'email'         => $this->input->post('email1'),			
			'mobile_number'  => $this->input->post('mobile_number1'),			
			'message'       => $this->input->post('message1'),
			'apply_for'       => $this->input->post('job_title'),			
			'type'       => 4,
			'product_id'       => $job_id,
			'resume'       => $uploaded_file,
			'receive_date'     =>$this->config->item('config.date.time')
			);
			//trace($posted_data);
			//exit;
			$posted_data = $this->security->xss_clean($posted_data);
			$this->careers_model->safe_insert('wl_enquiry',$posted_data,FALSE);
			/* Send  mail to user */
			$content    =  get_content('wl_auto_respond_mails','4');
			$subject    =  str_replace('{site_name}',$this->admin_info->company_name,$content->email_subject);
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$name = $sender_name = ucwords($this->input->post('first_name1').' '.$this->input->post('last_name1'));
			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{sender_name}',$name,$body);
			$body			=	str_replace('{email}',$this->input->post('email1'),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number1'),$body);
			$body			=	str_replace('{applying_for}',$this->input->post('job_title'),$body);
			//$body			=	str_replace('{experience}',$this->input->post('relevant_experience'),$body);
			//$body			=	str_replace('{salary}',$this->input->post('expected_salary'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->admin_info->company_name,$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->input->post('email1'),
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			if($filepath){
					$mail_conf['attachment']=$filepath;
				}

			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to user */
			/* Send  mail to admin */
			$body       =  $content->email_content;
			$verify_url = "<a href=".base_url().">Click here </a>";
			$name = 'Admin';
			$body			=	str_replace('{mem_name}',$name,$body);
			$body			=	str_replace('{sender_name}',$name,$body);
			$body			=	str_replace('{email}',$this->input->post('email1'),$body);
			$body			=	str_replace('{phone}',$this->input->post('phone_number1'),$body);
			$body			=	str_replace('{mobile}',$this->input->post('mobile_number1'),$body);
			$body			=	str_replace('{applying_for}',$this->input->post('job_title'),$body);
			//$body			=	str_replace('{experience}',$this->input->post('relevant_experience'),$body);
			//$body			=	str_replace('{salary}',$this->input->post('expected_salary'),$body);
			$body			=	str_replace('{admin_email}',$this->admin_info->admin_email,$body);
			$body			=	str_replace('{site_name}',$this->admin_info->company_name,$body);
			$body			=	str_replace('{url}',base_url(),$body);
			$body			=	str_replace('{link}',$verify_url,$body);

			$mail_conf =  array(
			'subject'=>$subject,
			'to_email'=>$this->admin_info->admin_email,
			'from_email'=>$this->admin_info->admin_email,
			'from_name'=> $this->config->item('site_name'),
			'body_part'=>$body
			);
			if($filepath){
					$mail_conf['attachment']=$filepath;
				}
			$this->dmailer->mail_notify($mail_conf);
			/* End send  mail to admin */
			$data['page_error'] = "";
			$this->session->set_userdata(array('msg_type'=>'success'));
			$this->session->set_userdata(array('msg_type_convs_request'=>'success_contect'));
			$this->session->set_flashdata('success', 'Your job has been applied successfully. We will get back to you soon.');
			redirect('pages/thanks', 'refresh');
		}else{
			$posted_data_str=$this->input->post('action');
			if(strlen($posted_data_str) && $posted_data_str=="Submit"){
				$data['page_error'] = "validation error";
			}
		}		
		$data['page_heading'] = "Apply Job";		
		$data['job_id'] = $job_id;
		$data['job_title']="";
		if($job_id > 0){
			$option = array('jobid'=>$job_id,'status'=>1);
			$res =  $this->careers_model->get_careers(1,0,$option);
			//trace($res);
			
			$data['job_title']=$res['job_title'];
		}				
		//$this->load->view('view_careers_apply',$data);
	}
	
	
	
	
}