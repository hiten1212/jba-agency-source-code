<?php $this->load->view("top_application");
?>
<!-- TREE --> 
<nav aria-label="breadcrumb" class="bg-black d-none d-md-block">
<div class="container">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>careers">Career</a></li>
<li class="breadcrumb-item active" aria-current="page"><?php echo $res['job_title'];?></li>
</ol>
</div>
</nav>
<!-- TREE ENDS --> 

<div class="container mid_area">
<div class="cms_area">
<h1><?php echo $res['job_title'];?></h1>

<div class="inr_list_inr">
<p class="fs20 yellow2"><?php echo $res['job_title'];?></p>
<p class="mt2 fs14"><b>Location :</b> <?php echo $res['location'];?></p>
<hr>
<p class="mt2 fs14"><b>Description :</b><br>
<?php echo $res['jobs_description'];?></p>
<div class="cb"></div>
<hr>
<div class="mt10"><a href="<?php echo base_url(); ?>careers/job_apply/<?php echo $res['jobs_id'];?>" class="view_btn forgot" data-fancybox="" data-type="iframe" title="Apply Job">Apply Job</a></div>
</div>

</div>
</div>
<?php
$this->load->view("ajax_paging");
$this->load->view("bottom_application");?>
