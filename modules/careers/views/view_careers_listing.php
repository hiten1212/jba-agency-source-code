<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<?php echo navigation_breadcrumb('Careers');?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   }</script>

  

<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1>Careers</h1>
<div class="w-100 mt-3 mb-4">
<div class="row">
<div class="my_data col-lg-6">
<div class="caree_lft">
<?php 
if(is_array($res) && !empty($res)){
	echo form_open("",'id="myform" method="post" ');
	?>
<input type="hidden" name="per_page" value="<?php echo $this->input->get_post('per_page',TRUE);?>">
<input type="hidden" name="offset" value="0">
<?php echo form_close();?>


<p class="fs18 black weight600 text-uppercase bb pb-2">Job Details :- </p>
<div class="row" id="prodListingContainer">
	    <?php $data = array('res'=>$res);
	    $this->load->view('careers/careers_data',$data);?>	   
       </div>
      <div class="clearfix"></div>  
<?php if($totalProduct > $record_per_page){?>
	   <p class="mt20 mb20 text-center" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p>
	   <?php 
	   }  

}else{
	  ?>
       <p class="mt20 mb20 text-center">No record(s) Found.</p>
	  <?php
  }?> 


</div>
</div>
<!-- end -->


<div class="col-lg-6">
 <?php
 			echo error_message();
 			echo form_open_multipart('careers#jobs','id="comment_form"');?>
            <input type="hidden" name="action" id="action" value="Career" />
            <input type="hidden" name="jId" id="jId" value="" />
<div class="app_frm" id="a1">
<p class="fs20 gray weight600 text-uppercase pb-2 bb mb-4">Apply Form :-</p>

<div class="form_sect">
<p>Applying For <span class="red"></span></p>
<p><input type="text" name="job_title" id="job_title" value="<?php echo set_value('job_title');?>"  placeholder="Applying For"  readonly="readonly">
<?php echo form_error('job_title');?></p>
</div>
<!-- end -->

<div class="form_sect">
<p>Name * <span class="red">*</span></p>
<p><input type="text" name="first_name1" id="first_name" value="<?php echo set_value('first_name1');?>" placeholder="First Name *">
<?php echo form_error('first_name1');?></p>
</div>

<div class="form_sect">
<p>Email ID <span class="red">*</span></p>
<p><input type="text" name="email1" id="email" value="<?php echo set_value('email1');?>" placeholder="Email *"><?php echo form_error('email1');?></p>
</div>
<!-- end -->


<div class="form_sect">
<p>Mobile No. <span class="red">*</span></p>
<p><input type="text" name="mobile_number1" id="mobile_number" placeholder="Mobile Number *" value="<?php echo set_value('mobile_number1');?>"><?php echo form_error('mobile_number1');?></p>
</div>
<!-- end -->


<!--<div class="form_sect">
<p>Applying For <span class="red">*</span></p>
<p><input name="" type="text" placeholder="Applying For"></p>
</div>-->
<!-- end -->


<div class="form_sect">
<p>Resume <span class="red">*</span></p>
<p><input type="file" name="resume" id="resume" placeholder="Resume"><?php echo form_error('resume');?></p>
<span class="black fs15">Only upload doc,docx,pdf,rtf &amp; File Size (5MB)</span>
</div>
<!-- end -->


<div class="form_sect">
<div class="enter_cod"><input name="verification_code1" id="verification_code" autocomplete="off" type="text" placeholder="Enter Code *"></div>
<div class="captc_cod"> <img src="<?php echo site_url('captcha/normal');?>"  alt="" title="" id="captchaimage" > &nbsp; <a href="javascript:void(0);" title="Change Verification Code" onClick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal'); ?>/<?php echo uniqid(time()); ?>'+Math.random(); document.getElementById('verification_code1').focus();"><img src="<?php echo theme_url(); ?>images/ref2.png" alt=""></a>
  <?php echo form_error('verification_code1');?></div>
</div>
<!-- enter code -->


<div class="app_butt">
<input type="submit" name="submit" id="submit" value="Submit">
</div>


<div class="clearfix"></div>
</div>
<?php echo form_close();?>
</div>
<!-- end -->



</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<script>
function put_job(v,j){	
	$("#jId").val(v);
	$("#job_title").val(j);
}
</script>
<?php 
$this->load->view("bottom_application");?>