<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>siriuscoffee</title>
<link rel="shortcut icon" href="favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&family=Lobster+Two:wght@700&family=Open+Sans:wght@300;400;600;700;800&family=Oswald:wght@300;400;500;600;700&family=Poppins:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo theme_url();?>css/conditional_ak.css">
<script async src="https://kit.fontawesome.com/30d3c2bc83.js" crossorigin="anonymous"></script>
</head>
<body>

<div class="popup_w">
<h1>Apply Now</h1>
<?php
 echo error_message();
 echo form_open_multipart('careers/job_apply/'.$job_id,'id="comment_form"');?>
<div class="form-row"> 

<div class="form-group col-12">
<p class="fs14"><label for="name"><b>Name</b></label></p>
<p ><input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name');?>" placeholder="Name *" class="form-control">
<?php echo form_error('first_name');?></p>
</div>

<div class="form-group mt-2 col-12">
<p class="fs14"><label for="your_email"><b>Email ID</b></label></p>
<p><input type="text" name="email" id="email" value="<?php echo set_value('email');?>" placeholder="Email *" class="form-control">
<?php echo form_error('email');?></p>
</div>

<div class="form-group mt-2  col-12">
<p class="fs14"><label for="mobile"><b>Mobile No.</b></label></p>
<p><input type="text" name="mobile_number" id="mobile_number" value="<?php echo set_value('mobile_number');?>" placeholder="Mobile Number *" class="form-control">
<?php echo form_error('mobile_number');?></p>
</div>

<div class="form-group mt-2  col-12">
<p class="fs14"><label for="resume"><b>Upload Resume</b></label></p>
<p><input type="file" name="resume" class="form-control" id="customFile">    
    <?php echo form_error('resume');?>
<span class="black fs15">Only upload doc,docx,pdf,rtf files</span>  </p>
</div>


<div class="form-group mt-2 col-12">
<input name="verification_code" id="verification_code" type="text" placeholder="Enter code *" class="float-left form-control" style="width:90px;" autocomplete="off">

<img src="<?php echo site_url('captcha/normal'); ?>" class="float-left" alt="" id="captchaimage"> <a href="javascript:viod(0);" title="Change Verification Code"><img src="<?php echo theme_url();?>images/ref2.png" alt="Refresh" onclick="document.getElementById('captchaimage').src='<?php echo site_url('captcha/normal');?>/<?php echo uniqid(time());?>'+Math.random(); document.getElementById('verification_code').focus();" class="float-left ml5"></a></p>

<p class="clearfix"></p>
  <?php echo form_error('verification_code');?>
 
 <div class="clearfix"></div>
</div>
<p class="col-12">
<input name="submit" type="submit" value="Submit" class="view_btn3">
  </p>
  </div>
<?php echo form_close();?>


</div>

</body>
</html>