<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Careers_model extends MY_Model
{

	public function get_careers($limit='10',$offset='0',$param=array())
	{

		$category_id		=   @$param['category_id'];
		$country_id		=   @$param['country_id'];
		$status			    =   @$param['status'];
		$jobid			=   @$param['jobid'];
		$orderby			=	@$param['orderby'];
		$orderbyn			=	@$param['orderbyn'];
		$where			    =	@$param['where'];
		$jwhere			    =	@$param['jwhere'];
		$keyword			=   trim($this->input->get_post('keyword2',TRUE));
		$keyword			=   $this->db->escape_str($keyword);
		

		//trace($where);

		if($where!='')
		{
			$this->db->where($where);

		}				
		if($category_id!='')
		{
			$this->db->where("FIND_IN_SET( '".$category_id."',wlj.category_links )");
		}
		if($subcat_id!='')
		{
			$this->db->where("FIND_IN_SET( '".$subcat_id."',wlj.category_links )");
		}
		if($country_id!='')
		{
			$this->db->where("wlj.country_id  ","$country_id");
		}
		if($jobid!='')
		{
			$this->db->where("wlj.jobs_id  ","$jobid");
		}
		if($status!='')
		{
			$this->db->where("wlj.status","$status");
		}

		$search_prod_type=$this->input->get_post('search_prod_type',TRUE);

		if($search_prod_type=='is_featured')
		{
			$this->db->where("wlj.is_featured",'1');
		}
		if($search_prod_type=='is_new')
		{
			$this->db->where("wlj.is_new",'1');
		}
		if($search_prod_type=='is_hot')
		{
			$this->db->where("wlj.is_hot",'1');
		}
		if($search_prod_type=='is_trending')
		{
			$this->db->where("wlj.is_trending",'1');
		}


		if($keyword!='')
		{
			$this->db->where("(wlj.job_title LIKE '%".$keyword."%' OR wlj.job_code LIKE '%".$keyword."%' )");
		}

		if($this->input->get_post("sortbyprice")!=""){
			$orderby = $this->input->get_post("sortbyprice");
		}
		if($this->input->get_post("sortbyname")!=""){
			$orderbyn = $this->input->get_post("sortbyname");
		}

		if($orderby!='')
		{
			$orderby=$this->db->escape_str($orderby);
			if($orderby=="lth"){
				$this->db->order_by('price ','asc');
			}
			if($orderby=="htl"){
				$this->db->order_by('price ','desc');
			}
		}

		if($orderbyn!='')
		{
			$orderbyn=$this->db->escape_str($orderbyn);
			if($orderbyn=="az"){
				$this->db->order_by('job_title ','asc');
			}
			if($orderbyn=="za"){
				$this->db->order_by('job_title ','desc');
			}
		}

		if(empty($orderby) && empty($orderbyn))
		{
			$this->db->order_by('wlj.jobs_id ','desc');
		}

	  $this->db->group_by("wlj.jobs_id");
	  if($limit!="")
		$this->db->limit($limit,$offset);
		$this->db->select('SQL_CALC_FOUND_ROWS wlj.*,wlj.status as status,wljm.media,wljm.media_type,wljm.is_default',FALSE);
		$this->db->from('wl_jobs as wlj');
		$this->db->where('wlj.status !=','2');
		$this->db->join('wl_jobs_media AS wljm','wlj.jobs_id=wljm.jobs_id'.$jwhere,'left');
		//$this->db->join('wl_jobs_media AS wljm','wlj.jobs_id=wljm.jobs_id','left');
		
		$q=$this->db->get();
		$result = $q->result_array();
		$result = ($limit=='1') ? @$result[0]: $result;
		return $result;

	}

	public function get_careers_media($limit='4',$offset='0',$param=array())
	{

		$default			    =   @$param['default'];
		$jobid			    =   @$param['jobid'];
		$media_type				=   @$param['media_type'];
		 $where  				=   @$param['where'];
		//$color_id=$this->uri->segment('6');
		if( is_array($param) && !empty($param) )
		{
			$this->db->select('SQL_CALC_FOUND_ROWS *',FALSE);
			$this->db->limit($limit,$offset);
			$this->db->from('wl_jobs_media');
			$this->db->where('jobs_id',$jobid);

			if($default!='')
			{
				$this->db->where('is_default',$default);
			}
			if($media_type!='')
			{
				$this->db->where('media_type',$media_type);
			}
			if($where!=''){			
				$this->db->where($where);			
			}

			$q=$this->db->get();
			//echo_sql();
			$result = $q->result_array();
			$result = ($limit=='1') ? $result[0]: $result;
			return $result;

		 }

	}

}