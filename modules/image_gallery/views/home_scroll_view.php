<?php $this->load->model(array('image_gallery/image_gallery_model'));
$param = array('status'=>'1','type'=>'1'); 
$res_array     = $this->image_gallery_model->get_image_gallery(12,0,$param);

if(is_array($res_array) && !empty($res_array)){	?>

<!-- our gallery section start -->
<div class="gall_bg">
<div class="container">
<h3>Our Gallery</h3>
<div class="w-100 mt-3 mb-3">
<div id="gall_scroll" class="owl-carousel owl-theme">

 <?php
	   $p=1;
	   $para='';
	   foreach($res_array as $val){
		   $act_cls = $p==1 ? 'active' : '';
		   $b=$p-1;  
		   $big_img=base_url()."uploaded_files/image_gallery/".$val["gallery_file"];
		    ?>
<div class="item">
<div class="img-gall-out">
<div class="effect">
<p><a href="<?php echo $big_img;?>" data-fancybox="<?php echo theme_url(); ?>images"><i class="fas fa-expand"></i></a></p>
<p><a href="<?php echo site_url('image_gallery'); ?>"><?php echo $val["title"];?></a></p>
</div>
<div class="gall-area">
<p class="gall-title"><span><?php echo $val["title"];?></span></p>
<figure><img src="<?php echo get_image('image_gallery',$val["gallery_file"],286,261,'AR')?>" alt="" class="img-fluid"/></figure>
</div>
</div>
</div>
<?php } ?>
<!-- end -->

</div>
</div>
<div class="clearfix"></div>
<p class="text-center mt-5"><a href="<?php echo site_url('image-gallery'); ?>" title="View More" class="vew_mor1">View More</a></p>
<div class="clearfix"></div>
</div>
</div>
<?php } ?>
<!-- our gallery section end -->










