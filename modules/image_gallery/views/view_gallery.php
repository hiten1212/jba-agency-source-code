<?php $this->load->view("top_application");?>
<?php $this->load->view('banner/inner_page_top_banner');?>
<script type="text/javascript">function serialize_form() { return $('#myform').serialize();   } </script>
<?php navigation_breadcrumb($title);?>

<div class="container">
<div class="mid_area">
<div class="cms_area">
<h1 class="float-left"><?php echo $title;?></h1><p class="float-right"><a href="<?php echo base_url('video-gallery');?>" class="btn btn-sm btn-success">Video Gallery</a></p>
<p class="clearfix"></p>
<div class="category_list">
<?php if(is_array($res) && !empty($res)){?>
		  
  <?php echo form_open("",'id="myform" method="post" ');?>
		<input type="hidden" name="per_page" value="<?php echo $this->config->item('per_page');?>">
<input type="hidden" name="offset" id="pg_offset" value="0">
<?php echo form_close();?>
  <ul class="float_1" id="xlistContainer">    
  
  <?php  if(is_array($res) && !empty($res)){ $data['res'] = $res;	  ?>
	<?php $this->load->view('image_gallery/load_gallery_view',$data);?>  
	<?php
}else{
 print_no_record(0);
}
?>        
		  </ul>
		  <p class="clearfix"></p>

<?php if($totalProduct > $record_per_page){?>
<p class="text-center mt-4" id="loadingdiv"><img src="<?php echo theme_url(); ?>images/loader.gif" alt=""></p><?php } ?>
<?php
	}else{
	   print_no_record(0);
	}
	?>



<p class="clearfix"></p>
</div>



</div>
</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
$.extend(gObj,{
			  'actual_count':<?php echo $total_rows;?>,
			  'listingContainer':'#xlistContainer',
			  'itemContainer':'.xitemContainer',
			  'req_url':'<?php echo $base_url;?>',
			  'data_frm' : '#myform'
	  });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/developers/js/pager.js"></script>
<?php $this->load->view("bottom_application");?>