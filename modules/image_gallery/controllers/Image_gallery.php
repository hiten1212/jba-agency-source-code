<?php
class image_gallery extends Public_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model(array('image_gallery/image_gallery_model','video_gallery/video_gallery_model'));
		$this->load->helper('category/category');
	    $this->form_validation->set_error_delimiters("<div class='required'>","</div>");

		$this->page_section_ct = 'events';
	}


	public function index(){
		
		$album_id =  (int) $this->uri->segment(3);		
		$this->page_section_ct = 'image_gallery';					 

		//$record_per_page        = (int) $this->input->post('per_page');	
		$record_per_page    = (int) $this->input->post('per_page')? $this->input->post('per_page'): $this->config->item('per_page');	

		$offset = (int) $this->input->post('offset');
		
		$config['per_page']		= ( $record_per_page > 0 ) ? $record_per_page : $this->config->item('per_page');
		
		$base_url      =  "image_gallery";			

		$act = $this->input->post('action');
		$param = array('status'=>'1','type'=>'1','album_id'=>$album_id);		

		if($act=="is_exclusive"){	
			$param = array('status'=>'1','type'=>'1','is_exclusive'=>'1'); 
		}

		if($act=="is_latest"){	
			$param = array('status'=>'1','type'=>'1','is_latest'=>'1'); 
		}
		
				
		$res_array = $this->image_gallery_model->get_image_gallery($config['per_page'],$offset,$param);		

		$total_rows	= get_found_rows();	
		$data['total_rows']	= $data['totalProduct']= $total_rows;	
		
		$param2 = array('status'=>'1','type'=>'2');		
		$res_video= $this->video_gallery_model->get_video_gallery($config['per_page'],$offset,$param2);		
		
		$data['record_per_page'] = $record_per_page;
		$data['title'] = 'Image Gallery';
		$data['res'] = $res_array; 
		$data['res_video'] = $res_video; 
		$data['base_url'] = $base_url;
		$ajx_req = $this->input->is_ajax_request();
		
		if($ajx_req===TRUE){
		  $this->load->view('image_gallery/load_gallery_view',$data);
		}
		else{
		  $this->load->view('image_gallery/view_gallery',$data);
		}	   
		
	}		

	

	public function details()

	{	

		$this->page_section_ct = 'events';

	

		$id = (int) $this->uri->rsegment(3);	

		//$id= (int) $this->meta_info['entity_id'];	

		$param     = array('tbl_events.status'=>'1','where'=>"tbl_events.events_id ='$id' ");	

		$res       = $this->events_model->get_events(1,0,$param);	

		

		if(is_array($res) && !empty($res))

		{			

			$data['title'] = 'Events ';

		    $data['res'] = $res; 			

		    $this->load->view('events/events_details_view',$data);

		

			

		}else

		{

			redirect('events', ''); 

			

		}

		

	}



	public function download_pdf()

	{

	  $id = (int) $this->uri->segment(3);



	  $param     = array('status'=>'1','where'=>"events_id ='$id' ");	



	  $res_topic       = $this->events_model->get_events(1,0,$param);	

		  



	  if(is_array($res_topic) && !empty($res_topic))

	  {

		if($res_topic['events_pdf']!='' && file_exists(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']))

		{

			$this->load->helper('download');

			$data = file_get_contents(UPLOAD_DIR."/events/pdf/".$res_topic['events_pdf']);

			$name = $res_topic['events_pdf'];

			force_download($name, $data); 

		}

	  }

	  

	}

	

	

}

